//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CanIfr.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the CanIfr class.
//! @details
//!
//*!****************************************************************************

#ifndef CANIFR_H
#define CANIFR_H

//******************************************************************************
// Includes
//******************************************************************************
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "Loggable.h"
#include "CanSocket.h"
#include "Mutex.h"

//******************************************************************************
// Class Declaration
//******************************************************************************
/*! ***************************************************************************************
 * @struct t_canFrameFD
 * @brief	Structure contains canfd_frame packet and timestamp information
 * ****************************************************************************************
 */
typedef struct
{
    canfd_frame	Frame;
	char time_msg[32];

} t_canFrameFD;

/*! ***************************************************************************************
 * @struct t_canFrame
 * @brief	Structure contains can_frame packet and timestamp information
 * ****************************************************************************************
 */
typedef struct
{
    can_frame	Frame;
    char time_msg[32];

} t_canFrame;

/*! ****************************************************************************************
 * @class CanIfr
 * @brief The CanIfr class: wrapper class for the execution of socket operation
 * *****************************************************************************************
 */
class CanIfr : public Loggable
{
    public:
        CanIfr();
        ~CanIfr();

        /*! ***************************************************************************************
         * \brief readFrame
         * \brief wrapper ro readCAN socket method in order to retrieve can_frame message
         * \param pFrame [out] can_frame data received
         * \return number of byte received upon success, -1 in case of error
         * ***************************************************************************************
         */
        int readFrame(can_frame * pFrame);

		/*! ***************************************************************************************
         *  @brief			readFrameFD
         *  @brief			split data of canfd_frame to put information in canID and pBuff parameters
         *  @param nCanID [out]	can ID received
         *  @param npBuff [out]	payLoad data received
		 *  @return			number of byte received upon success, -1 in case of error
		 *  @details		-
		 *  ***************************************************************************************
		 */
        int  readFrameFD( int * nCanID, uint8_t * npBuff);

		/*! ***************************************************************************************
         *  @brief			readFrameFDWithTimeStamp
         *  @brief			fill t_canFrameFD structure with canfd_frame and timestamp information
         *  @param pCanFrame [out]	CAN packet frame with timestamp of the reception
		 *  @return			number of byte received upon success, -1 in case of error
		 *  @details		-
		 *  ***************************************************************************************
		 */
        int  readFrameFDWithTimeStamp(t_canFrameFD *pCanFrame);

        /*! ***************************************************************************************
         *  @brief			readFrameWithTimeStamp
         *  @brief			fill t_canFrame structure with can_frame and timestamp information
         *  @param pCanFrame [out]	CAN packet frame with timestamp of the reception
         *  @return			number of byte received upon success, -1 in case of error
         *  @details		-
         *  ***************************************************************************************
         */
        int readFrameWithTimeStamp(t_canFrame * pCanFrame);

		/*! ***************************************************************************************
         *  @brief			sendFrameFD
         *  @brief			performs writeCan operation using canfd_frame struct
         *  @param nCanID [in]		ID to send
         *  @param npBuff [in]		payload to Send
         *  @param nSize [in]		number of data to send
		 *  @return			0 upon success, -1 otherwise
		 *  @details		-
		 *  ***************************************************************************************
		 */
        int sendFrameFD( int nCanID, uint8_t * npBuff, uint8_t nSize );

        /*! ***************************************************************************************
         *  @brief			sendFrame
         *  @brief			performs writeCan operation using can_frame struct
         *  @param nCanID [in]		ID to send
         *  @param npBuff [in]		payload to Send
         *  @param nSize [in]		number of data to send
         *  @return			0 upon success, -1 otherwise
         *  @details		-
         *  ***************************************************************************************
         */
        int sendFrame( int nCanID, uint8_t * npBuff, uint8_t nSize );

		/*! ***************************************************************************************
         *  @brief			init
		 *  @brief			set the CAN socket interface.
         *  @param idListSFF	pointer to the list of CAN ID to filter
         *  @param sizeListSFF	the size of the list
         *  @param idListEFF 	pointer to a second list of CAN ID to filter
         *  @param sizeListEFF  size of the list
		 *  @return			0 upon success, -1 otherwise
		 *  @details
		 *  ***************************************************************************************
		 */
		bool init(uint16_t * idListSFF, uint16_t sizeListSFF, uint16_t *idListEFF, uint16_t sizeListEFF);

		/*! ***************************************************************************************
         *  @brief			setInterfaceName
		 *  @brief			set the name of can interface to use
         *  @param pCanNameInterface [in]		-
		 *  @return			0 upon success, -1 otherwise
		 *  @details		-
		 *  ***************************************************************************************
		 */
        void setInterfaceName(const char * pCanNameInterface);

    private:
        CanSocket m_CanSocket;
        char m_sCanInterfaceName[1024];

};

#endif // CANIFR_H
