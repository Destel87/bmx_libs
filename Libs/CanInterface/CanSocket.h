//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CanSocket.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the CanSocket class.
//! @details
//!
//*!****************************************************************************

#ifndef CANSOCKET_H
#define CANSOCKET_H

//******************************************************************************
// Includes
//******************************************************************************
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <vector>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <linux/if_link.h>
#include <linux/rtnetlink.h>
#include <linux/netlink.h>

#include "Mutex.h"

//******************************************************************************
// Class Declaration
//******************************************************************************
using namespace std;

#define MAXFILTERS 32

/*! ***************************************************************************************
 * @class CanSocket
 * @brief The CanSocket class: opens a socket file descriptor for the performing of read
 *		  and write operation.
 * ****************************************************************************************
 */
class CanSocket
{

		/* Methods */

	public:

		/*! ***************************************************************************************
         *  @brief			CanSocket Default constructor
		 *  @details 		Sets the socket to 0
		 *  ***************************************************************************************
		 */
		CanSocket();

		/*! ***************************************************************************************
         *  @brief			CanSocket	Default destructor
		 *  @details		Closes the socket and sets it to 0
		 *  ***************************************************************************************
		 */
		~CanSocket();

		/*! ***************************************************************************************
         *  @brief			open
		 *  @brief			Creates the socket
		 *  @return			0 upon success, -1 otherwise
		 *  @details		Calls socket()
		 *  ***************************************************************************************
		 */
		int open( void );

		/*! ***************************************************************************************
         *  @brief			setInterface
		 *  @brief			set the interface for the socket in use
         *  @param interfaceName     set the name of interface to use
		 *  @return			0 upon success, -1 otherwise
		 *  @details
		 *  ***************************************************************************************
		 */
		int setInterface(const char * interfaceName);

		/*! ***************************************************************************************
         *  @brief			bindToInterface
		 *  @brief			link the interface to peripheral interface
		 *  @return			0 upon success, -1 otherwise
		 *  ***************************************************************************************
		 */
		int bindToInterface( void );

		/*! ***************************************************************************************
         *  @brief			setFDFrames
		 *  @brief			set flexible data format frame
         *  @param nFDEnable flag to enable/disabel FD frame
		 *  @return			0 upon success, -1 otherwise
		 *  ***************************************************************************************
		 */
		int setFDFrames( int nFDEnable );

		/*! ***************************************************************************************
          *  @brief			setReceiveTimeOut
		  *  @brief				set the blocking timeout of read socket function
          *  @param nMsec [in]	millisec to wait after which unlocking occurs
		  *  @return			0 upon success, -1 otherwise
		  *  @details
		  *  **************************************************************************************
		  */
		int setReceiveTimeOut( uint32_t nMsec );

		/*! ***************************************************************************************
          *  @brief			setSendTimeOut
		  *  @brief				set the blocking timeout of write socket function
          *  @param nMsec [in] millisec to wait after which unlocking occurs
		  *  @return			0 upon success, -1 otherwise
		  *  @details
		  *  ***************************************************************************************
		  */
		int setSendTimeOut( uint32_t nMsec );

		/*! ***************************************************************************************
          *  @brief			setReceiveNetworkStackBufferSize
		  *  @brief				set the queue of receive buffer of the socket interface
          *  @param nNumByte [in] size of the queue
		  *  @return			0 upon success, -1 otherwise
		  *  @details
		  *  **************************************************************************************
		  */
		int setReceiveNetworkStackBufferSize( uint32_t nNumByte );

		/*! ***************************************************************************************
          *  @brief			setSendNetworkStackBufferSize
		  *  @brief				set the queue of send buffer of the socket interface
          *  @param nNumByte [in] size of the queue
		  *  @return			0 upon success, -1 otherwise
		  *  @details
		  *  **************************************************************************************
		  */
		int setSendNetworkStackBufferSize( uint32_t nNumByte);

		/*! ***************************************************************************************
          *  @brief			setNotBlockingMode
		  *  @brief				set not blocking working mode of the socket interface
          *  @param nEnable [in] CAN_BLOCKING_MODE_ON, CAN_BLOCKING_MODE_OFF
		  *  @return			0 upon success, -1 otherwise
		  *  @details			-
		  *  **************************************************************************************
		  */
		int setNotBlockingMode( int nEnable);

		/*! ***************************************************************************************
          *  @brief			getDescriptor
		  *  @brief				return the int number of the file descriptor
		  *  @return			m_sockFD
		  *  @details			-
		  *  **************************************************************************************
		  */
		int getDescriptor( void );

		/*! ***************************************************************************************
          *  @brief			createFilters
		  *  @brief				create and initialize n filter to set in the socket interface
          *  @param nNumFilter [in] number of filters to create
		  *  @return			0 upon success, -1 otherwise
		  *  @details
		  *  **************************************************************************************
		  */
		int createFilters( uint8_t nNumFilter );

		/*! ***************************************************************************************
          *  @brief			setFilter
		  *  @brief				set canID and canMAsk datas of the filter
          *  @param nCanId [in]	Id number to filter
          *  @param nCanMask [in] Mask to apply at the ID specified
		  *  @return			0 upon success, -1 otherwise
		  *  @details			-
		  *  **************************************************************************************
		  */
		int setFilter(uint32_t nCanId, uint32_t nCanMask);

		/*! ***************************************************************************************
          *  @brief			getFilterDescriptionString
          *  @param sFiltersString [out] the buffer where the filter string is copied
		  *  **************************************************************************************
		  */
		void getFilterDescriptionString(char* sFiltersString);

		/*! ***************************************************************************************
          *  @brief			writeFDCan
		  *  @brief				send to CAN socket interface a canfd_frame structure
          *  @param pCanFDframe [in] pointer to a canfd_frame
		  *  @return			0 upon success, -1 otherwise
		  *  @details			-
		  *  **************************************************************************************
		  */
		int writeFDCan( canfd_frame * pCanFDframe );

        /*! ***************************************************************************************
          *  @brief			writeCAN
          *  @brief				send to CAN socket interface a can_frame structure
          *  @param pCanFrame [in] pointer to a canfd_frame
          *  @return			0 upon success, -1 otherwise
          *  @details			-
          *  **************************************************************************************
          */
        int writeCan(can_frame * pCanFrame);

		/*! ***************************************************************************************
          *  @brief			readFDCAN
		  *  @brief				read the socket can interface and put the data inside a canfd_frame
		  *						structure when the data are ready
          *  @param pCanFDframe [out] canfd_frame data storage structure
		  *  @return			0 upon success, -1 otherwise
		  *  @details			if timeout is set, the function exits after the timeout with a
		  *						specified errno
		  *  **************************************************************************************
		  */
        int readFDCAN( canfd_frame * pCanFDframe );

        /*! ***************************************************************************************
          *  @brief			readCAN
          *  @brief				read the socket can interface and put the data inside a can_frame
          *						structure when the data are ready
          *  @param pCanFrame [out] can_frame data storage structure
          *  @return			0 upon success, -1 otherwise
          *  @details			if timeout is set, the function exits after the timeout with a
          *						specified errno
          *  **************************************************************************************
          */
        int readCAN(can_frame * pCanFrame);

		/*! ***************************************************************************************
          *  @brief			readFDCANwithTimeStamp
		  *  @brief				read the socket can interface and put the data inside a canfd_frame
		  *						structure when the data are ready. Read also timestamp of the
		  *						socket operation with ioctl function (SIOCGSTAMP) and put the info
		  *						in the timeval specified structure.
          *  @param pCanFDframe [out] canfd_frame data storage structure
          *  @param ptimeStamp [out] struct timeval with timestamp information
		  *  @return			0 upon success, -1 otherwise
		  *  @details			if timeout is set, the function exits after the timeout with a
		  *						specified errno
		  *  **************************************************************************************
		  */
        int readFDCANwithTimeStamp( canfd_frame * pCanFDframe, struct timeval * ptimeStamp );

        /*! ***************************************************************************************
          *  @brief			readCANwithTimeStamp
          *  @brief				read the socket can interface and put the data inside a can_frame
          *						structure when the data are ready. Read also timestamp of the
          *						socket operation with ioctl function (SIOCGSTAMP) and put the info
          *						in the timeval specified structure.
          *  @param pcanframe [out] canfd_frame data storage structure
          *  @param ptimeStamp [out] struct timeval with timestamp information
          *  @return			0 upon success, -1 otherwise
          *  @details			if timeout is set, the function exits after the timeout with a
          *						specified errno
          *  **************************************************************************************
          */
        int readCanWithTimeStamp(can_frame * pcanframe, struct timeval * ptimeStamp);

		/*! ***************************************************************************************
          *  @brief			isOpen
		  *  @brief				if m_sockFD is > -1 the socket is open.
		  *  @return			0 upon success, 1 otherwise
		  *  **************************************************************************************
		  */
		int isOpen( void );

		/*! ***************************************************************************************
         * @brief           enableDebugPrint
         * @brief               enable/disable debug printf messages
		 * @param bDebugPrintEnable	if true enables debug lines, false otherwise
		 * ****************************************************************************************
		 */
		void enableDebugPrint(bool bDebugPrintEnable);

        /*! ***************************************************************************************
         * @brief           setError
         * @brief               set CAN_ERR_MASK error mask to the socket in use
         * @return              0 upon success, 1 otherwise
         * ****************************************************************************************
         */
        int setError(void);

        /*! ***************************************************************************************
         * @brief           setOnlyWriteMode
         * @brief               It disables receive operations on the usage socket
         * ***************************************************************************************
         */
        void setOnlyWriteMode(void);

        /*!
         * @brief checkSocketWriteBuffer
         * @brief
         * @return
         */
        int checkSocketWriteBuffer(void);

        /*!
         * @brief checkError
         * @brief
         * @return
         */
        int checkError( void );

        /* Attributes */
    public:

        static const int CAN_FORMAT_FD_ON = 1;
        static const int CAN_FORMAT_FD_OFF = 0;
        static const int CAN_BLOCKING_MODE_ON = 0;
        static const int CAN_BLOCKING_MODE_OFF = 1;

    private:

        int                     m_sockCanfd;
        struct ifreq            m_ifreq;
        struct sockaddr_can 	m_sockaddr_can;
        string                  m_interfaceName;
        uint8_t                 m_nNumFilter;
        struct canfd_frame      m_canFDFrameTx;
        can_frame               m_canFrameTx;
        struct canfd_frame      m_canFDFrameRx;
        can_frame               m_canFrameRx;
        struct can_filter*      m_pCanFilter;
        uint8_t					m_maxNumFilters;
        vector<can_filter>      m_pFilter;

        struct iovec            m_iovec;
        struct msghdr           m_msghdr;

        Mutex                   m_MutexWrite;
        Mutex                   m_MutexRead;
        bool					m_bDebugPrintEnable;

};

#endif // CANSOCKET_H
