//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CanSocket.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the CanSocket class.
//! @details
//!
//*!****************************************************************************

#include <sys/select.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <libsocketcan.h>

#include "CanSocket.h"
/* Linux-specific socket ioctls */
#define SIOCINQ		FIONREAD
#define SIOCOUTQ	TIOCOUTQ        /* output queue size (not sent + not acked) */
#define SIOCOUTQNSD	0x894B		/* output queue size (not sent only) */

CanSocket::CanSocket()
{
    m_sockCanfd = -1;
	m_nNumFilter = 0;
	m_pFilter.clear();
	m_pCanFilter = NULL;
	m_maxNumFilters = MAXFILTERS;
	m_bDebugPrintEnable = false;
    memset(&m_canFrameTx, 0, sizeof(can_frame));
    memset(&m_canFrameRx, 0, sizeof(can_frame));

}

CanSocket::~CanSocket()
{
	m_pFilter.clear();
    close(m_sockCanfd);
	delete(m_pCanFilter);
}

int CanSocket::open( void )
{
	int sock;
	//open a socket CAN
	//PF is an extension of AF_CAN
	//SOCK_RAW: directly access lower level protocols(jump middle layer protocol)
	//CAN_RAW: customize CAN format frame
	sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if ( sock < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::open ERR <%s>\n", strerror(errno));
        }
		return -1;
	}
    m_sockCanfd = sock;
	return 0;
}

int CanSocket::setInterface( const char *interfaceName )
{
	int res;

	if (isOpen() == 0)
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::setInterface ERR <%s>\n", strerror(errno));
        }
		return -1;
	}

    m_interfaceName = interfaceName;

	strcpy(m_ifreq.ifr_name, interfaceName);
    res = ioctl(m_sockCanfd, SIOCGIFINDEX, &m_ifreq);
	if ( res < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::setInterface ERR <%s>\n", strerror(errno));
        }
		return -1;
	}

	m_sockaddr_can.can_family = AF_CAN;
	m_sockaddr_can.can_ifindex = m_ifreq.ifr_ifindex;

	// these settings are static and can be held out of the hot path
    m_iovec.iov_base = &m_canFrameRx;
	m_msghdr.msg_name = &m_sockaddr_can;
	m_msghdr.msg_iov = &m_iovec;
	m_msghdr.msg_iovlen = 1;

	return 0;
}

int CanSocket::bindToInterface( void )
{
	int ret;

	if (isOpen() == 0)
	{
		if ( m_bDebugPrintEnable )
        {
			printf( " ERR CanSocket::bindToInterface FD not setted ");
        }
		return -1;
	}

    ret = bind(m_sockCanfd, (struct sockaddr *)&m_sockaddr_can, sizeof(m_sockaddr_can));
	if ( ret < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::bindToInterface ERR <%s>\n", strerror(errno));
        }
		return -1;
	}

	return 0;
}

int CanSocket::writeCan(can_frame * pCanFrame)
{
    int nBytes = 0;

    if (isOpen() == 0)
    {
        if ( m_bDebugPrintEnable )
        {
            printf( "CanSocket::writeCAN ERR FD not setted\n");
        }
        return -1;
    }

    m_MutexWrite.lock();
    // Use 'select' to wait for socket to be ready for writing:
    struct timeval tv = {0,0};
    fd_set fds;
    fd_set *rfds, *wfds;
    int n, so_error = 0;
    unsigned so_len;

    FD_ZERO(&fds);
    FD_SET(m_sockCanfd, &fds);
    tv.tv_sec = 0;
    tv.tv_usec = 200;

    rfds = NULL;
    wfds = &fds;
    TEMP_FAILURE_RETRY (n = select (m_sockCanfd + 1, rfds, wfds, NULL, &tv));
    switch (n)
    {
        case 0:
            if ( m_bDebugPrintEnable )
            {
                printf("CanSocket::writeCAN-->wait timed out\n");
            }
            n = -1;
            break;

        case -1:
            if ( m_bDebugPrintEnable )
            {
                printf("CanSocket::writeCAN-->error during wait\n");
            }
            n = -1;
            break;

        default:
            // select tell us that sock is ready, test it
            so_len = sizeof(so_error);
            so_error = 0;
            getsockopt (m_sockCanfd, SOL_SOCKET, SO_ERROR, &so_error, &so_len);
            errno = so_error;
            if (so_error != 0)
            {
                printf("CanSocket::writeCAN--> error socket %s\n", strerror(errno));
                n = -1;
            }
            break;
    }

    if(n == -1)
    {
        // error condition
        m_MutexWrite.unlock();
        return -1;
    }

    nBytes = write(m_sockCanfd, pCanFrame, CAN_MTU);
//    printf("CanSocket::send performed <%i>,  <%s>\n", errno, strerror(errno));
    if (( nBytes < 0 ) || (nBytes != CAN_MTU))
    {
        if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::writeCAN ERR <%s>\n", strerror(errno));
        }
        m_MutexWrite.unlock();
        return -1;
    }

    m_MutexWrite.unlock();
    return 0;
}

int CanSocket::writeFDCan(canfd_frame * pCanFDframe)
{
    int nBytes = 0;

    if (isOpen() == 0)
    {
        if ( m_bDebugPrintEnable )
        {
                printf( "CanSocket::writeCAN ERR FD not setted\n");
        }
        return -1;
    }


    m_MutexWrite.lock();

    // Use 'select' to wait for socket to be ready for writing:
    struct timeval tv = {0,0};
    fd_set fds;
    fd_set *rfds, *wfds;
    int n, so_error;
    unsigned so_len;

    FD_ZERO(&fds);
    FD_SET(m_sockCanfd, &fds);
    tv.tv_sec = 0;
    tv.tv_usec = 200;

    rfds = NULL;
    wfds = &fds;
    TEMP_FAILURE_RETRY (n = select (m_sockCanfd + 1, rfds, wfds, NULL, &tv));
    switch (n)
    {
        case 0:
            printf("wait timed out\n");
            //RBB return
            break;

        case -1:
            printf("error during wait\n");
            //RBB to inserto return
            break;

        default:
            // select tell us that sock is ready, test it
            so_len = sizeof(so_error);
            so_error = 0;
            getsockopt (m_sockCanfd, SOL_SOCKET, SO_ERROR, &so_error, &so_len);
            errno = so_error;
            if (so_error != 0)
            {
                printf("error socket %s\n", strerror(errno));
            }
            break;
    }


    nBytes = write( m_sockCanfd, pCanFDframe, CAN_MTU);
    if (( nBytes < 0 ) || (nBytes != CAN_MTU))
    {
        if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::writeFDCAN ERR (%d) <%s>\n", errno, strerror(errno));
        }
        nBytes = -2;
    }
    m_MutexWrite.unlock();
    return nBytes;
}

int CanSocket::readCAN(can_frame * pCanFrame)
{
    int res = 0;

    if (isOpen() == 0)
    {
        if ( m_bDebugPrintEnable )
        {
            printf( "CanSocket::readCAN ERR FD not setted ");
        }
        return -1;
    }

    m_MutexRead.lock();

    // these settings may be modified by recvmsg()
    m_iovec.iov_len			= sizeof(m_canFrameRx);
    m_msghdr.msg_namelen	= sizeof(m_sockaddr_can);
    //msg.msg_controllen	= sizeof(ctrlmsg);
    m_msghdr.msg_flags		= 0;

    res = read(m_sockCanfd, pCanFrame, CAN_MTU);
    if ( res < 0 )
    {
        if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::readCAN ERR <%s>\n", strerror(errno));
        }
        res = -1;
    }

    m_MutexRead.unlock();

    return res;
}

int CanSocket::readFDCAN(canfd_frame * pCanFDframe)
{
	int res = 0;

	if (isOpen() == 0)
	{
		if ( m_bDebugPrintEnable )
        {
			printf( "CanSocket::readCAN ERR FD not setted ");
        }
		return -1;
	}

	m_MutexRead.lock();

	// these settings may be modified by recvmsg()
    m_iovec.iov_len			= sizeof(m_canFDFrameRx);
	m_msghdr.msg_namelen	= sizeof(m_sockaddr_can);
	//msg.msg_controllen	= sizeof(ctrlmsg);
	m_msghdr.msg_flags		= 0;

    res = read(m_sockCanfd, pCanFDframe, CANFD_MTU);
	if ( res < 0 )
	{
		if ( m_bDebugPrintEnable )
            printf("CanSocket::readCAN ERR <%s>\n", strerror(errno));
		res = -1;
	}

	m_MutexRead.unlock();

	return res;
}

int CanSocket::readFDCANwithTimeStamp(canfd_frame * pCanFDframe, struct timeval * ptimeStamp)
{
	int res = 0;

	if ( isOpen() == 0 )
	{
		if ( m_bDebugPrintEnable )
        {
            printf( "CanSocket::readCAN ERR FD not set");
        }
		return -1;
	}

	m_MutexRead.lock();

    res = read(m_sockCanfd, pCanFDframe, CANFD_MTU);
    if ( res > 0 )
	{
        if(ptimeStamp)
        {
            struct timeval tv;
            ioctl(m_sockCanfd, SIOCGSTAMP, &tv);
            memcpy(ptimeStamp, &tv, sizeof(tv));
        }
    }

	m_MutexRead.unlock();

	return res;
}

int CanSocket::readCanWithTimeStamp(can_frame * pcanframe, struct timeval * ptimeStamp)
{
    int res = 0;

    if ( isOpen() == 0 )
    {
        if ( m_bDebugPrintEnable )
        {
            printf( "CanSocket::readCAN ERR FD not set");
        }
        return -1;
    }

    m_MutexRead.lock();

    res = read(m_sockCanfd, pcanframe, CAN_MTU);
    if ( res > 0 )
    {
        if(ptimeStamp)
        {
            struct timeval tv;
            ioctl(m_sockCanfd, SIOCGSTAMP, &tv);
            memcpy(ptimeStamp, &tv, sizeof(tv));
        }
    }

    m_MutexRead.unlock();
    return res;
}

int CanSocket::setFDFrames( int nFDEnable )
{
	int res;
    res = setsockopt(m_sockCanfd, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &nFDEnable, sizeof(nFDEnable));
	if ( res < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::setFDFrames ERR <%s>\n", strerror(errno));
        }
		return -1;
	}
	return 0;
}

int CanSocket::setReceiveTimeOut( uint32_t nMsec )
{
	int res;
	int nSecTmp;
	int nMsecTmp;
	int nUsecTmp;
	struct timeval tv;

	if (nMsec >= 1000)
	{
        nSecTmp = nMsec / 1000;
        nMsecTmp = nMsec % 1000;
        nUsecTmp = nMsecTmp * 1000;
	}
	else
	{
		tv.tv_sec = 0;
        nUsecTmp = nMsec * 1000;
	}

	tv.tv_sec = nSecTmp;
	tv.tv_usec = nUsecTmp;

    res = setsockopt(m_sockCanfd, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&tv, sizeof(struct timeval));
	if ( res < 0)
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::setReceiveTimeOut ERR <%s>\n", strerror(errno));
        }
		return -1;
	}

	return 0;
}

int CanSocket::setSendTimeOut( uint32_t nMsec )
{
	int res;
	int nSecTmp;
	int nMsecTmp;
	int nUsecTmp;
	struct timeval tv;

	if ( nMsec >= 1000 )
	{
		nSecTmp = nMsec / 1000;
		nMsecTmp = nMsec % 1000;
		nUsecTmp = nMsecTmp * 1000;
	}
	else
	{
		tv.tv_sec = 0;
		nUsecTmp = nMsec * 1000;
	}

	tv.tv_sec = nSecTmp;
	tv.tv_usec = nUsecTmp;

    res = setsockopt(m_sockCanfd, SOL_SOCKET, SO_SNDTIMEO, (struct timeval *)&tv, sizeof(struct timeval));
	if ( res < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
			printf("CanSocket::setSendTimeOut ERR <%s>\n", strerror(errno));
        }
		return -1;
	}

	return 0;
}

int CanSocket::setReceiveNetworkStackBufferSize( uint32_t nNumByte )
{
	int res;
	int nN1Tmp = nNumByte;
	int nN2Tmp = sizeof(int);

    getsockopt(m_sockCanfd, SOL_SOCKET, SO_RCVBUF, &res, (socklen_t *)&nN2Tmp);
    if ( m_bDebugPrintEnable )
    {
        printf("CanSocket::setReceiveNetworkStackBufferSizeOld = %d\n", res);
    }

    nN1Tmp = nNumByte;
    res = setsockopt(m_sockCanfd, SOL_SOCKET, SO_RCVBUF, &nN1Tmp, nN2Tmp);
	if ( res < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::setReceiveNetworkStackBufferSize ERR <%s>\n", strerror(errno));
        }
		return -1;
	}

    getsockopt(m_sockCanfd, SOL_SOCKET, SO_RCVBUF, &res, (socklen_t *)&nN2Tmp);
    if ( m_bDebugPrintEnable )
    {
        printf("CanSocket::setReceiveNetworkStackBufferSizeNew = %d\n", res);
    }
    return 0;
}

int CanSocket::setSendNetworkStackBufferSize( uint32_t nNumByte )
{
	int res;
	int nN1Tmp = nNumByte;
	int nN2Tmp = sizeof(int);

/* only for testing ...
    socklen_t socklen;
    int       optval;
    optval = 0;
    socklen = 4;

    res = getsockopt(m_sockCanfd, SOL_SOCKET, SO_SNDBUF, &optval, &socklen);
    if ( m_bDebugPrintEnable )
    {
        printf("socket TX original buffer size = %d\n", optval);
    }
*/
    res = setsockopt(m_sockCanfd, SOL_SOCKET, SO_SNDBUF, &nN1Tmp, nN2Tmp);
	if ( res < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
			printf("CanSocket::setSendNetworkStackBufferSize ERR <%s>\n", strerror(errno));
        }
		return -1;
	}


/* ... only for testing
    res = getsockopt(m_sockCanfd, SOL_SOCKET, SO_SNDBUF, &optval, &socklen);
    if ( m_bDebugPrintEnable )
    {
        printf("socket TX original buffer size = %d\n", optval);
    }
*/

	return 0;
}

int CanSocket::setNotBlockingMode( int nEnable )
{
	int res;
    res = ioctl(m_sockCanfd, (int)FIONBIO, (char *)nEnable);
	if ( res < 0 )
	{
		if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::setNotBlockingMode ERR <%s>\n", strerror(errno));
        }
		return -1;
	}
	return 0;
}

int CanSocket::getDescriptor( void )
{
    return m_sockCanfd;
}

int CanSocket::createFilters(uint8_t nNumFilter)
{
	if ( m_pCanFilter )
    {
		delete m_pCanFilter;
        m_pCanFilter = 0;
    }

	m_pCanFilter = new can_filter[nNumFilter];

	if ( !m_pCanFilter )
    {
		return -1;
    }

	/* ****************************************************************************************************************
	 *  Set a default to indesiderable not filtering setup:
	 * if can_mask and can:id are set to 0x00, socket does not filter nothing!
	 * ****************************************************************************************************************
	 */
    for (uint8_t i = 0; i < nNumFilter; i++)
	{
		m_pCanFilter[i].can_mask = CAN_SFF_MASK;
	}

	m_maxNumFilters = nNumFilter;

	return 0;
}

int CanSocket::setFilter(uint32_t nCanId, uint32_t nCanMask)
{
    if ( m_sockCanfd < 0 )
	{
		return -1;
	}

	if ( m_nNumFilter < m_maxNumFilters )
	{
		m_pCanFilter[m_nNumFilter].can_id =  nCanId;
		m_pCanFilter[m_nNumFilter].can_mask = nCanMask;
		m_nNumFilter++;

		uint32_t nSizeStruct = sizeof(can_filter)*m_nNumFilter;
        int res = setsockopt(m_sockCanfd, SOL_CAN_RAW, CAN_RAW_FILTER, &m_pCanFilter, nSizeStruct);
		if (res < 0)
		{
			if ( m_bDebugPrintEnable )
            {
                printf("CanSocket::setFilter ERR <%s>\n", strerror(errno));
            }
			return -1;
		}
	}
	else
	{
		if ( m_bDebugPrintEnable )
        {
			printf("CanSocket::setFilter ERR <%s>\n",strerror(errno));
        }
		return -1;
	}

	return 0;
}

void CanSocket::getFilterDescriptionString(char* sFiltersString)
{
    *sFiltersString = 0;
    for( uint8_t i = 0; i < m_nNumFilter; i++ )
	{
		char sTmp[256];
		sprintf(sTmp,
				"Filter[%d]={id=0x%x;mask=0x%x} ",
				i,
				m_pCanFilter[i].can_id,
				m_pCanFilter[i].can_mask);
		strcat(sFiltersString, sTmp);
	}
}

int CanSocket::isOpen( void )
{
    return (m_sockCanfd > -1) ? 1 : 0;
}

void CanSocket::enableDebugPrint(bool bDebugPrintEnable)
{
	m_bDebugPrintEnable = bDebugPrintEnable;
}

int CanSocket::setError()
{
    can_err_mask_t err_mask = CAN_ERR_MASK;
    int res = setsockopt(m_sockCanfd, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof(err_mask));
    if (res < 0)
    {
        if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::setFilter MASK <%s>\n", strerror(errno));
        }
        return -1;
    }

    return 0;
}

void CanSocket::setOnlyWriteMode()
{
    shutdown(m_sockCanfd, SHUT_RD);
}

int CanSocket::checkSocketWriteBuffer()
{
    // wait for can TX queue to become empty
    int queued = 0;
    ioctl(m_sockCanfd, TIOCOUTQ, &queued);
    while (queued > 0)
    {
        ioctl(m_sockCanfd, TIOCOUTQ, &queued);

        if ( m_bDebugPrintEnable )
        {
            printf("CanSocket::checkSocketWriteBuffer--> queued %i \n", queued);
        }

        usleep(100);
    }
    return 0;
}

int CanSocket::checkError( void )
{
    int error = 0;
    socklen_t len = sizeof (error);
    int retval = getsockopt (m_sockCanfd, SOL_SOCKET, SO_ERROR, &error, &len);
    if (retval != 0)
    {
        // there was a problem getting the error code
        fprintf(stderr, "error getting socket error code: %s\n", strerror(retval));
        return -1;
    }

    if (error != 0)
    {
        // socket has a non zero error status
        fprintf(stderr, "socket error: %s\n", strerror(error));
        return -1;
    }
    return 0;
}
