QT          -= core gui
TARGET      = CanInterface
TEMPLATE    = lib
CONFIG      += staticlib

QMAKE_CXXFLAGS_DEBUG -= -O1
QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG -= -O3
QMAKE_CXXFLAGS_DEBUG += -O0

LIBS += -pthread

# internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a


SOURCES +=\
    $$PWD/CanIfr.cpp \
    $$PWD/CanSocket.cpp \

HEADERS += \
    $$PWD/CanIfr.h \
    $$PWD/CanSocket.h \
