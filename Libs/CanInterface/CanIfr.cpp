//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CanIfr.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the CanIfr class.
//! @details
//!
//*!****************************************************************************

//******************************************************************************
// Includes
//******************************************************************************
#include "CanIfr.h"


//******************************************************************************
// Class Implementation
//******************************************************************************
CanIfr::CanIfr()
{
	m_sCanInterfaceName[0] = 0;
}

CanIfr::~CanIfr()
{
    /* Nothing to do yet */
}

bool CanIfr::init(uint16_t * idListSFF, uint16_t sizeListSFF, uint16_t * idListEFF, uint16_t sizeListEFF)
{
	if(sizeListSFF == 0) return true;

    int res;

    /* ********************************************************************************************
     * Open socket
     * ********************************************************************************************
     */
    res = m_CanSocket.open();
	if ( res < 0 )
    {
        log(LOG_ERR, "Cannot open socket" );
        return false;
    }

    /* ********************************************************************************************
     * Set interface
     * ********************************************************************************************
     */
	res = m_CanSocket.setInterface(m_sCanInterfaceName);
    if (res < 0)
    {
       log(LOG_ERR, "Cannot set interface %s", "can0");
       return false;
    }

    /* ********************************************************************************************
     * Set Stack Buffer Queue
     * ********************************************************************************************
     */
    uint32_t       bufsize = 400 * 1024;
    res = m_CanSocket.setSendNetworkStackBufferSize(bufsize);
    if (res < 0)
    {
        log(LOG_ERR, "Cannot set setSendNetworkStackBufferSize");
        return false;
    }

    /* ********************************************************************************************
     * Unset FD format frames
     * ********************************************************************************************
     */
    res = m_CanSocket.setFDFrames(CanSocket::CAN_FORMAT_FD_OFF);
    if ( res < 0 )
    {
        log(LOG_ERR, "Cannot set FD format frame");
        return false;
    }

    /* ********************************************************************************************
     * Set Error Mask
     * ********************************************************************************************
     */
    res = m_CanSocket.setError();
    if ( res < 0 )
    {
        log(LOG_ERR, "Cannot set FD error mask");
        return false;
    }

	/* ********************************************************************************************
	 * Create and set Filters
	 * ********************************************************************************************
	 */
	uint16_t usNumTotFilter = sizeListSFF + sizeListEFF;

	 res = m_CanSocket.createFilters(usNumTotFilter);
	 if ( res < 0 )
	 {
		log(LOG_ERR, "Cannot create filters");
		return false;
	 }

	 for (unsigned int i = 0; i < sizeListSFF; i++)
	 {
		 res = m_CanSocket.setFilter(*idListSFF, CAN_SFF_MASK);
		 if ( res < 0 )
		 {
			 log(LOG_ERR, "Cannot set SFF Filter 0x%x", *idListSFF);
			 return false;
		 }

		 idListSFF++;
	 }

	 for (unsigned int i = 0; i < sizeListEFF; i++)
	 {
		 res = m_CanSocket.setFilter(*idListEFF, CAN_EFF_MASK);
		 if ( res < 0 )
		 {
			 log(LOG_ERR, "Cannot set EXT Filter 0x%x", *idListEFF);
			 return false;
		 }

		 idListEFF++;
	 }

	 log(LOG_INFO, "Can Interface configured with the following filters");
	 char sFilters[1024];
	 m_CanSocket.getFilterDescriptionString(sFilters);
	 log(LOG_INFO, "%s", sFilters);

    /* ********************************************************************************************
     * Bind to interface
     * ********************************************************************************************
     */
    res = m_CanSocket.bindToInterface();
    if ( res < 0 )
    {
        log(LOG_ERR, "Cannot bind socket to interface" );
        return false;
    }

	int nCanSocket;
	nCanSocket = m_CanSocket.getDescriptor();

    m_CanSocket.enableDebugPrint(true);

	log(LOG_INFO, "%s can interface up with socket number %d", m_sCanInterfaceName, nCanSocket);

    return true;
}

void CanIfr::setInterfaceName(const char *pCanNameInterface)
{
	snprintf(m_sCanInterfaceName, 1023, "%s", pCanNameInterface);
}

int CanIfr::readFrame(can_frame * pFrame)
{
    int nReceivedBytes = 0;

    nReceivedBytes = m_CanSocket.readCAN(pFrame);

    return nReceivedBytes;
}

int CanIfr::readFrameWithTimeStamp(t_canFrame * pCanFrame)
{
    int nReceivedBytes = 0;
    timeval timeStamp;
    nReceivedBytes = m_CanSocket.readCanWithTimeStamp(&pCanFrame->Frame, &timeStamp);
    if (nReceivedBytes > 0)
    {
        time_t timeStampSec = timeStamp.tv_sec;
        int	curr_msec = timeStamp.tv_usec / 1000;

        sprintf(pCanFrame->time_msg, "%.19s.%03d", ctime(&timeStampSec), curr_msec);

    }

    return nReceivedBytes;
}

int CanIfr::readFrameFD( int * nCanID, uint8_t * npBuff)
{
    int nReceivedBytes = 0;
    int res = 0;
    canfd_frame canFrame;

    nReceivedBytes = m_CanSocket.readFDCAN(&canFrame);
    if (nReceivedBytes < 0)
    {
        return -1;
    }
    else
    {
        *nCanID = canFrame.can_id;
        memcpy(npBuff, canFrame.data, canFrame.len);
        res = canFrame.len;
    }

    return res;
}

int CanIfr::readFrameFDWithTimeStamp(t_canFrameFD * pCanFrame)
{
	int nReceivedBytes = 0;
    struct timeval timeStamp;

    nReceivedBytes = m_CanSocket.readFDCANwithTimeStamp(&pCanFrame->Frame, &timeStamp);
    if (nReceivedBytes > 0)
	{
		time_t timeStampSec = timeStamp.tv_sec;
		int	curr_msec = timeStamp.tv_usec / 1000;

		sprintf(pCanFrame->time_msg, "%.19s.%03d", ctime(&timeStampSec), curr_msec);
	}

	return nReceivedBytes;
}

int CanIfr::sendFrameFD( int nCanID, uint8_t * npBuff, uint8_t nSize )
{
    canfd_frame canFrame;
    int res = 0;

    canFrame.can_id = nCanID;
    canFrame.len = nSize;
    memcpy(canFrame.data, npBuff, nSize);

    res = m_CanSocket.writeFDCan(&canFrame);

    return res;
}

int CanIfr::sendFrame( int nCanID, uint8_t * npBuff, uint8_t nSize )
{
    can_frame canFrame;
    int res = 0;

    canFrame.can_id = nCanID;
    canFrame.can_dlc = nSize;
    memcpy(canFrame.data, npBuff, nSize);

    if (m_CanSocket.checkError() == 0)
    {
       res = m_CanSocket.writeCan(&canFrame);
    }

    return res;
}



