QT          -= core gui
TARGET      = GpioInterface
TEMPLATE    = lib
CONFIG      += staticlib

HEADERS += \
	$$PWD/GpioInterface.h \
	$$PWD/GpioInterfaceDeclarations.h

SOURCES += \
	$$PWD/GpioInterface.cpp

# internal libraries inclusion

unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../Config/ -lConfig
INCLUDEPATH += $$PWD/../Config
DEPENDPATH += $$PWD/../Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Config/libConfig.a
