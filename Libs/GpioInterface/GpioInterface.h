//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    GpioInterface.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the GpioInterface class.
//! @details
//!
//*!****************************************************************************

#ifndef GPIO_INTERFACE_H
#define GPIO_INTERFACE_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

#include "Loggable.h"
#include "Config.h"
#include "Mutex.h"
#include "GpioInterfaceDeclarations.h"


/*! ***********************************************************************************************
 * @class	GpioInterface
 * @brief	This class implements a Freescale model Linux GPIO interface.
 *			It is possible to manage up to 16 GPIO.
 *			A configuration file must be read in which there must be the following general keys:
 *			- gpio.baseDir=/Sysfs_path/						# eg. /sys/class/gpio/
 *			- gpio.exportFile=filename_to_export_gpio		# eg. export
 *			- gpio.unexportFile=filename_to_release_gpio	# es. unexport
 *
 *			If a GPIO is needed the following keys must be present
 *			- gpio.1.id=27			# GPIO identifier, obtained with the formula id = (i-1)*32 + j
 *									# where "i" is the bank and "j" is the index whitin the bank
 *									# example: GPIO3[16] = [(3-1)*32 + 16] = 80
 *			- gpio.1.direction=out	# can be "in" or "out"
 *			- gpio.1.defaultValue=1	# default value if it configured as "out"
 *
 *			After a GpioInterface is created, the method init() must be called with the configuration
 *			file. init() initializes a dynamic array with size equal to the size of adjacent gpio
 *			correctly configured in the file.
 *			The class contains a logger.
 * ************************************************************************************************
 */

typedef struct
{
	int nId;                // identifier (as per config file, obtained with the formula)
	int nDirection;         // direction
	int nDefaultValue;      // defaultValue
	int nValue;             // the last value read or written on the GPIO
	int nFD;				// file descriptor
	Mutex nMutex;			// provides write operations protection
	struct pollfd* pPollFD;	// a file descriptor allocated only if the gpio must receive irqs

} gpio;

class GpioInterface : public Loggable
{

    public:

		/*! ***************************************************************************************
		 * void constructor
		 * ****************************************************************************************
		 */
		GpioInterface();

		/*! ***************************************************************************************
		 * default destructor
		 * ****************************************************************************************
		 */
		virtual ~GpioInterface();

		/*! ***************************************************************************************
		 * Initializes the interface as per configuration file.
		 * configuration file must be written as in the brief of the class.
		 * @param	sConfigFileName  configuration file name
		 * ****************************************************************************************
		 */
		bool init(const char* sConfigFileName);

		/*! ***************************************************************************************
		 * Asks the kernel to export control of the specified GPIO to userspace
		 * @param	nGpioIndex GPIO id
		 * @return	true upon success, false otherwise (error is logged)
		 * ****************************************************************************************
		 */
		bool exportGpio(int nGpioIndex);

		/*! ***************************************************************************************
		 * Reverses the effect of exporting the specified GPIO to userspace
		 * @param	nGpioIndex GPIO id
		 * @return	true upon success, false otherwise (error is logged)
		 * ****************************************************************************************
		 */
		bool closeAndUnExportGpio(int nGpioIndex);

		/*! ***************************************************************************************
		 * Sets the specified GPIO as input by writing on the device the proper flag
		 * @param	nGpioIndex GPIO id
		 * @return	true upon success, false otherwise (error is logged)
		 * ****************************************************************************************
		 */
		bool writeDirectionIn(int nGpioIndex);

		/*! ***************************************************************************************
		 * Sets the specified GPIO as output by writing on the device the proper flag
		 * @param	nGpioIndex GPIO id
		 * @return	true upon success, false otherwise (error is logged)
		 * ****************************************************************************************
		 */
		bool writeDirectionOut(int nGpioIndex);

		/*! ***************************************************************************************
		 * Writes on the specified GPIO the desired value
		 * @param	nGpioIndex GPIO id
		 * @param	nValue value to write can be 0 or 1
		 * @return	true upon success, false otherwise (error is logged)
		 * ****************************************************************************************
		 */
		bool writeValue(int nGpioIndex, int nValue);

		/*! ***************************************************************************************
		 * Read the value of the specified GPIO
		 * @param	nGpioIndex GPIO id
		 * @return	the last value read by using readValue method, 2 upon error
		 * ****************************************************************************************
		 */
		int readValue(int nGpioIndex);

		/*! ***************************************************************************************
		 * Returns the last value read by using readValue method from the specified GPIO
		 * @param	nGpioIndex GPIO id
		 * @return	the last value read by using readValue method, 2 upon error
		 * ****************************************************************************************
		 */
		int getValue(int nGpioIndex);

		/*! ***************************************************************************************
		 * Blocking waits until a rising or a falling edge is seen on the specified gpio. The gpio
		 * must be configured a input and with hasIrq=1 in the config file. Moreover the type of
		 * edge must be specified with the key edge=rising or edge=falling.
		 * @param	nGpioIndex GPIO id
         * @param nMsecTimeout the msecs timeout
		 * @return	true if an interrupt has occurred, false on timeout
		 * ****************************************************************************************
		 */
		int waiForIrq(int nGpioIndex, int nMsecTimeout = -1);

		/*! ***************************************************************************************
		 *  Prints the configuration on the logger
		 * ****************************************************************************************
		 */
		void printConfiguration(void);

		/*! ***************************************************************************************
		 * @brief closeInterface close and unexport all GPIOs
		 * ****************************************************************************************
		 */
		void closeInterface(void);


	private:

		/*! ***************************************************************************************
		 *  Sets the GPIO direction
		 *  @param nGpioIndex GPIO id
		 *  @param nDirection can only be GPIO_DIR_IN, or GPIO_DIR_OUT
		 *  @return true upon success, false otherwise (error is logged)
		 * ****************************************************************************************
		 */
		bool GpioWriteDirection(int nGpioIndex, int nDirection);

		/*! ***************************************************************************************
		 *  Reads configuration file and stores configuration structure inside the class
		 *  @param sConfigFileName the complete path of the configuration file
		 *  @return true upon success, false otherwise (error is logged)
		 * ****************************************************************************************
		 */
		bool readConfigFile(const char* sConfigFileName);

		/*! ***************************************************************************************
		 *  Reset the interface and free all the resources
		 * ****************************************************************************************
		 */
		void reset(void);

    private:

        bool m_bConfigOk;
        Config m_Config;
        char m_sBaseDir[1024];
        char m_sExportFile[1024];
        char m_sUnExportFile[1024];
        int m_nNumGpio;
        gpio* m_pGpioArray;

};


#endif // GPIO_INTERFACE_H
