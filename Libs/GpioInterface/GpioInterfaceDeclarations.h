//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    GpioInterfaceDeclarations.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the defines for the GpioInterface class.
//! @details
//!
//*!****************************************************************************

#ifndef GPIOINTERFACEDECLARATIONS_H
#define GPIOINTERFACEDECLARATIONS_H

// Configuration keys
#define CFG_GPIO_BASE_DIR               1001
#define CFG_GPIO_EXPORT_FILE            1002
#define CFG_GPIO_UNEXPORT_FILE          1003

#define CFG_GPIO_ID_1                   1
#define CFG_GPIO_ID_2                   2
#define CFG_GPIO_ID_3                   3
#define CFG_GPIO_ID_4                   4
#define CFG_GPIO_ID_5                   5
#define CFG_GPIO_ID_6                   6
#define CFG_GPIO_ID_7                   7
#define CFG_GPIO_ID_8                   8
#define CFG_GPIO_ID_9                   9
#define CFG_GPIO_ID_10                  10
#define CFG_GPIO_ID_11                  11
#define CFG_GPIO_ID_12                  12
#define CFG_GPIO_ID_13                  13
#define CFG_GPIO_ID_14                  14
#define CFG_GPIO_ID_15                  15
#define CFG_GPIO_ID_16                  16

#define CFG_GPIO_DIRECTION_1            101
#define CFG_GPIO_DIRECTION_2            102
#define CFG_GPIO_DIRECTION_3            103
#define CFG_GPIO_DIRECTION_4            104
#define CFG_GPIO_DIRECTION_5            105
#define CFG_GPIO_DIRECTION_6            106
#define CFG_GPIO_DIRECTION_7            107
#define CFG_GPIO_DIRECTION_8            108
#define CFG_GPIO_DIRECTION_9            109
#define CFG_GPIO_DIRECTION_10           110
#define CFG_GPIO_DIRECTION_11           111
#define CFG_GPIO_DIRECTION_12           112
#define CFG_GPIO_DIRECTION_13           113
#define CFG_GPIO_DIRECTION_14           114
#define CFG_GPIO_DIRECTION_15           115
#define CFG_GPIO_DIRECTION_16           116

#define CFG_GPIO_DEFAULT_VALUE_1        201
#define CFG_GPIO_DEFAULT_VALUE_2        202
#define CFG_GPIO_DEFAULT_VALUE_3        203
#define CFG_GPIO_DEFAULT_VALUE_4        204
#define CFG_GPIO_DEFAULT_VALUE_5        205
#define CFG_GPIO_DEFAULT_VALUE_6        206
#define CFG_GPIO_DEFAULT_VALUE_7        207
#define CFG_GPIO_DEFAULT_VALUE_8        208
#define CFG_GPIO_DEFAULT_VALUE_9        209
#define CFG_GPIO_DEFAULT_VALUE_10       210
#define CFG_GPIO_DEFAULT_VALUE_11       211
#define CFG_GPIO_DEFAULT_VALUE_12       212
#define CFG_GPIO_DEFAULT_VALUE_13       213
#define CFG_GPIO_DEFAULT_VALUE_14       214
#define CFG_GPIO_DEFAULT_VALUE_15       215
#define CFG_GPIO_DEFAULT_VALUE_16       216

#define CFG_GPIO_HAS_IRQ_1				301
#define CFG_GPIO_HAS_IRQ_2				302
#define CFG_GPIO_HAS_IRQ_3				303
#define CFG_GPIO_HAS_IRQ_4				304
#define CFG_GPIO_HAS_IRQ_5				305
#define CFG_GPIO_HAS_IRQ_6				306
#define CFG_GPIO_HAS_IRQ_7				307
#define CFG_GPIO_HAS_IRQ_8				308
#define CFG_GPIO_HAS_IRQ_9				309
#define CFG_GPIO_HAS_IRQ_10				310
#define CFG_GPIO_HAS_IRQ_11				311
#define CFG_GPIO_HAS_IRQ_12				312
#define CFG_GPIO_HAS_IRQ_13				313
#define CFG_GPIO_HAS_IRQ_14				314
#define CFG_GPIO_HAS_IRQ_15				315
#define CFG_GPIO_HAS_IRQ_16				316

#define CFG_GPIO_EDGE_1					401
#define CFG_GPIO_EDGE_2					402
#define CFG_GPIO_EDGE_3					403
#define CFG_GPIO_EDGE_4					404
#define CFG_GPIO_EDGE_5					405
#define CFG_GPIO_EDGE_6					406
#define CFG_GPIO_EDGE_7					407
#define CFG_GPIO_EDGE_8					408
#define CFG_GPIO_EDGE_9					409
#define CFG_GPIO_EDGE_10				410
#define CFG_GPIO_EDGE_11				411
#define CFG_GPIO_EDGE_12				412
#define CFG_GPIO_EDGE_13				413
#define CFG_GPIO_EDGE_14				414
#define CFG_GPIO_EDGE_15				415
#define CFG_GPIO_EDGE_16				416

#define DEF_GPIO_BASE_DIR				"/sys/class/gpio/"
#define DEF_GPIO_EXPORT_FILE			"export"
#define DEF_GPIO_UNEXPORT_FILE			"unexport"
#define DEF_GPIO_HAS_IRQ_VAL			"0"
#define DEF_GPIO_EDGE_VAL				"rising"

#define GPIO_DIR_IN                     0
#define GPIO_DIR_OUT                    1

#endif // GPIOINTERFACEDECLARATIONS_H
