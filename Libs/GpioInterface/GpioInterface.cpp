//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    GpioInterface.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the GpioInterface class.
//! @details
//!
//*!****************************************************************************

#include "GpioInterface.h"

/*! ***********************************************************************************************
 * MACROS
 * ************************************************************************************************
 */
#define SAFE_DELETE(p) { if( p != 0 ) { delete p; p = 0; } }
#define SAFE_DELETE_ARRAY(ar) { if( ar != 0 ) { delete [] ar; ar = 0; } }

/*! ***********************************************************************************************
 * STATIC FUNCTIONS
 * ************************************************************************************************
 */
bool check_file_exists(const char* sFilename)
{
	int nFd;
	nFd = open(sFilename, O_WRONLY);
	if ( nFd < 0 )
	{
		return false;
	}
	else
	{
		close(nFd);
		return true;
	}
}

/*! ***********************************************************************************************
 * Class implementation
 * ************************************************************************************************
 */
GpioInterface::GpioInterface()
{
    m_bConfigOk = false;
	m_sBaseDir[0] = '\0';
	m_sExportFile[0] = '\0';
	m_sUnExportFile[0] = '\0';
    m_nNumGpio = 0;
    m_pGpioArray = NULL;
}


GpioInterface::~GpioInterface()
{
	reset();
}


bool GpioInterface::init(const char* sConfigFileName)
{
    m_bConfigOk = true;
	log(LOG_INFO, "GpioInterface initialization start");

	// Parse configuration file and store configuration structure
	if ( readConfigFile(sConfigFileName) == false )
	{
		log(LOG_ERR, "Unable to read config file");
		m_bConfigOk = false;
		return false;
	}

	// Initialization of GpioInterface general keys
	log(LOG_DEBUG, "Inizialization of general config keys for GpioInterface");
	strncpy(m_sBaseDir, m_Config.GetString(CFG_GPIO_BASE_DIR), 1023);
	sprintf(m_sExportFile, "%s%s", m_sBaseDir, m_Config.GetString(CFG_GPIO_EXPORT_FILE));
	if (check_file_exists(m_sExportFile) == false)
	{
		log(LOG_ERR, "Export file <%s> does not exist", m_sExportFile);
		m_bConfigOk = false;
		return false;
	}
	sprintf(m_sUnExportFile, "%s%s", m_sBaseDir, m_Config.GetString(CFG_GPIO_UNEXPORT_FILE));
	if (check_file_exists(m_sExportFile) == false)
	{
		log(LOG_ERR, "Unexport file <%s> does not exist", m_sUnExportFile);
		m_bConfigOk = false;
		return false;
	}
	log(LOG_DEBUG, "\texport-file: <%s>", m_sExportFile);
	log(LOG_DEBUG, "\tunexport-file: <%s>", m_sUnExportFile);

	// Initialization of each GPIO
	int nTemp = 0;
	for ( int i=CFG_GPIO_ID_1; i<=CFG_GPIO_ID_16; i++ )
	{
		nTemp = m_Config.GetInt(i);
		if ( nTemp == 0 )
			break;
		else
			m_nNumGpio++;
	}
	log(LOG_DEBUG, "Configuring %d GPIOs", m_nNumGpio);
	m_pGpioArray = new gpio[m_nNumGpio];

	// Reset all the values
	for ( int i=0; i<m_nNumGpio; i++)
	{
		m_pGpioArray[i].nId = -1;
		m_pGpioArray[i].nDirection = 1;
		m_pGpioArray[i].nFD = -1;
		m_pGpioArray[i].nDefaultValue = 0;
		m_pGpioArray[i].nValue = 0;
		m_pGpioArray[i].pPollFD = NULL;;
	}

	for ( int i=0; i<m_nNumGpio; i++ )
	{
		// Initializes IDs
		m_pGpioArray[i].nId = m_Config.GetInt(CFG_GPIO_ID_1+i);
		if (m_pGpioArray[i].nId < 0)
		{
			log(LOG_ERR, "Initialization error for gpio %d, id out of range (%d)", i+1, m_pGpioArray[i].nId);
			reset();
			return false;
		}

		// Export
		exportGpio(i+1);

		// Sets direction
		if ( strcmp(m_Config.GetString(CFG_GPIO_DIRECTION_1+i), "in") == 0 )
		{
			if ( writeDirectionIn(i+1) == false )
			{
				reset();
				return false;
			}
		}
		else
		{
			if ( writeDirectionOut(i+1) == false )
			{
				reset();
				return false;
			}
		}

		// Opens the file descriptor and holds it
		char sBuffer[1024];
        if(snprintf(sBuffer, sizeof(sBuffer), "%sgpio%d/value", m_sBaseDir, m_pGpioArray[i].nId) < 0)
            return false;

		int nFlagsforFD = O_RDONLY;
		if ( m_pGpioArray[i].nDirection == GPIO_DIR_OUT )
			nFlagsforFD = O_WRONLY;
		else if ( m_Config.GetInt(CFG_GPIO_HAS_IRQ_1+i) == 1 )
			nFlagsforFD = O_RDWR;

		// if the gpio is setup for interrupt listening the edge is set
        if ( m_Config.GetInt(CFG_GPIO_HAS_IRQ_1 + i) == 1 )
		{
			char sEdgeFile[1024];

            if(snprintf(sEdgeFile, sizeof(sEdgeFile), "%sgpio%d/edge", m_sBaseDir, m_pGpioArray[i].nId) < 0)
                return false;

			int nEdgeFd = open(sEdgeFile, O_WRONLY);
			if ( nEdgeFd <= 0 )
			{
				log(LOG_ERR, "Initialization error for gpio %d, unable to open file <%s>", i+1, sEdgeFile);
				reset();
				return false;
			}
			else
			{
				lseek(nEdgeFd, 0, SEEK_SET);
				write(nEdgeFd, m_Config.GetString(CFG_GPIO_EDGE_1+i), strlen(m_Config.GetString(CFG_GPIO_EDGE_1+i)));
				close(nEdgeFd);
			}
		}

		// Open the gpio and holds the file descriptor
		int nFd = open(sBuffer, nFlagsforFD);
		if ( nFd <= 0 )
		{
			log(LOG_ERR, "Initialization error for gpio %d, unable to open file <%s>", i+1, sBuffer);
			reset();
			return false;
		}
		m_pGpioArray[i].nFD = nFd;

		// If the gpio is configured to listen to interrupts then fill its pollfd structure,
		if ( m_Config.GetInt(CFG_GPIO_HAS_IRQ_1+i) == 1 )
		{
			if ( m_pGpioArray[i].nDirection == GPIO_DIR_OUT )
			{
				log(LOG_WARNING, "Misconfiguration for gpio %d, has Irq but is set as output", i+1);
			}
			else
			{
				m_pGpioArray[i].pPollFD = new struct pollfd;
				m_pGpioArray[i].pPollFD->fd = m_pGpioArray[i].nFD;
				m_pGpioArray[i].pPollFD->events = POLLPRI;

				char buf[8];
				lseek(m_pGpioArray[i].pPollFD->fd, 0, SEEK_SET);    /* consume any prior interrupt */
				read(m_pGpioArray[i].pPollFD->fd, buf, sizeof buf);
                poll(m_pGpioArray[i].pPollFD, 1, 100);				/* wait for interrupt */
				lseek(m_pGpioArray[i].pPollFD->fd, 0, SEEK_SET);    /* consume interrupt */
			}

		}

		// Writes default value for output gpios
		if ( m_pGpioArray[i].nDirection == GPIO_DIR_OUT )
		{
			m_pGpioArray[i].nDefaultValue = m_Config.GetInt(CFG_GPIO_DEFAULT_VALUE_1+i);

//			int liValue = readValue(i+1);
//			if ( liValue != m_pGpioArray[i].nDefaultValue )
//			{
				if ( writeValue(i+1, m_pGpioArray[i].nDefaultValue) ==  false )
				{
					reset();
					return false;
				}
//			}
		}
    }

	log(LOG_INFO, "GpioInterface initialization succesfully done");

	return true;

}


bool GpioInterface::exportGpio(int nGpioIndex)
{
	if ( (nGpioIndex < 1) || (nGpioIndex > m_nNumGpio) )
	{
		log(LOG_ERR, "Impossible to export gpio %d (not in the configuration list)", nGpioIndex);
		return false;
	}

	// Check if already exported
	unsigned int nId = (unsigned int)m_pGpioArray[nGpioIndex-1].nId;
	char sBuffer[1024] = "";
	int nLen = snprintf(sBuffer, sizeof(sBuffer), "%sgpio%u/direction", m_sBaseDir, nId);
	if (check_file_exists(sBuffer) == true)
	{
		log(LOG_INFO, "The specified gpio <%s> has been already exported", sBuffer);
		return true;
	}
	int nFd = open(m_sExportFile, O_WRONLY);
	if ( nFd < 0 )
	{
		log(LOG_ERR, "Impossible to export gpio %d, unable to open <%s>", nGpioIndex, m_sExportFile);
		return false;
	}

	char sBuff[16] = "";
	nLen = snprintf(sBuff, sizeof(sBuff), "%u", nId);
	ssize_t nRetWrite = write(nFd, sBuff, nLen);
	close(nFd);
	if ( nRetWrite < 1 )
	{
		log(LOG_ERR, "Impossible to export gpio %d, unable to write to <%s>", nGpioIndex, m_sExportFile);
		return false;
	}
	return true;
}


bool GpioInterface::closeAndUnExportGpio(int nGpioIndex)
{

	if ( (nGpioIndex < 1) || (nGpioIndex > m_nNumGpio) )
	{
		log(LOG_ERR, "Impossible to un-export gpio %d (not in the configuration list)", nGpioIndex);
		return false;
	}

	// Check if already un-exported
	unsigned int nId = (unsigned)m_pGpioArray[nGpioIndex-1].nId;
	char sBuffer[1024];
	int nLen = snprintf(sBuffer, sizeof(sBuffer), "%sgpio%u/direction", m_sBaseDir, nId);
	if ( check_file_exists(sBuffer) == false )
	{
		log(LOG_INFO, "The specified gpio <%sgpio%u> has been already un-exported", m_sBaseDir, nId);
		return true;
	}

	// Before the un-export operation we must ensure to close the gpio file descriptor
	if ( m_pGpioArray[nGpioIndex-1].nFD < 0 )
		log(LOG_WARNING, "It seems the file descriptor associated to gpio[%d] has been already closed", nGpioIndex);
	else
	{
		if (close(m_pGpioArray[nGpioIndex-1].nFD) != 0)
		{
			log(LOG_ERR, "Unable to close file descriptor associated to gpio[%d]", nGpioIndex);
		}
		else
		{
			log(LOG_INFO, "File descriptor of gpio[%d] correctly closed", nGpioIndex);
		}
	}

	// Un-export
	int nFd = open(m_sUnExportFile, O_WRONLY);
	if (nFd < 0)
	{
		log(LOG_ERR, "Impossible to un-export gpio %d, unable to open <%s>", nGpioIndex, m_sUnExportFile);
		return false;
	}
	char sBuff[16] = "";
	nLen = snprintf(sBuff, sizeof(sBuff), "%u", nId);
	ssize_t nRetWrite = write(nFd, sBuff, nLen);
	close(nFd);
	if ( nRetWrite < 1 )
	{
		log(LOG_ERR, "Impossible to un-export gpio %d, unable to write to <%s>", nGpioIndex, m_sUnExportFile);
		return false;
	}

	return true;
}


bool GpioInterface::writeDirectionOut(int nGpioIndex)
{
	return GpioWriteDirection(nGpioIndex, GPIO_DIR_OUT);
}


bool GpioInterface::writeDirectionIn(int nGpioIndex)
{
	return GpioWriteDirection(nGpioIndex, GPIO_DIR_IN);
}


bool GpioInterface::writeValue(int nGpioIndex, int nValue)
{

	if ( m_bConfigOk == false )
	{
		log(LOG_ERR, "Impossible to write to gpio[%d] (configuration error)", nGpioIndex);
		return false;
	}
	if ( (nGpioIndex < 1) || (nGpioIndex > m_nNumGpio) )
	{
		log(LOG_ERR, "Impossible to write to gpio[%d] (not in the list)", nGpioIndex);
		return false;
	}
	if ( (nValue != 0) && (nValue != 1) )
	{
		log(LOG_ERR, "Impossible to write to gpio[%d] (invalid value to set %d)", nGpioIndex, nValue);
		return false;
	}

	char sWriteBuffer[2] = {'0', 0 };
	if (nValue) sWriteBuffer[0] = '1';
	int nRetWrite = -1;
	m_pGpioArray[nGpioIndex-1].nMutex.lock();
	lseek(m_pGpioArray[nGpioIndex-1].nFD, 0, SEEK_SET);
	nRetWrite = write(m_pGpioArray[nGpioIndex-1].nFD, sWriteBuffer, sizeof(sWriteBuffer));
	m_pGpioArray[nGpioIndex-1].nMutex.unlock();

	if ( nRetWrite <= 0 )
	{
		m_pGpioArray[nGpioIndex-1].nValue = -1;
		log(LOG_ERR, "Impossible to write gpio[%d]=%d, unable to write on file", m_pGpioArray[nGpioIndex-1].nId, nValue);
		return false;
	}

	m_pGpioArray[nGpioIndex-1].nValue = nValue;
	log(LOG_DEBUG_PARANOIC+2, "Set gpio[%d]=%d", m_pGpioArray[nGpioIndex-1].nId, nValue);
	return true;
}


int GpioInterface::readValue(int nGpioIndex)
{
	if ( m_bConfigOk == false )
	{
		log(LOG_ERR, "Impossible to read from gpio[%d] (configuration error)", nGpioIndex);
		return 2;
	}
	if ( (nGpioIndex < 1) || (nGpioIndex > m_nNumGpio) )
	{
		log(LOG_ERR, "Impossible to read from gpio[%d] (not in the list)", nGpioIndex);
		return 2;
	}
	if ( m_pGpioArray[nGpioIndex-1].nDirection == GPIO_DIR_OUT )
	{
		log(LOG_WARNING, "Impossible to read from gpio[%d] (configured as out)", nGpioIndex);
	}

	char sReadBuffer[8] = {0};
	lseek(m_pGpioArray[nGpioIndex-1].nFD, 0, SEEK_SET);
	int nRetRead = read(m_pGpioArray[nGpioIndex-1].nFD, sReadBuffer, sizeof(sReadBuffer));
	if (nRetRead <= 0)
	{
		log(LOG_ERR, "Impossible to read from gpio[%d], unable to read file", m_pGpioArray[nGpioIndex-1].nId);
		return 2;
	}
	//int nRetValue = -1;
	//sscanf(sReadBuffer,"%u", &nRetValue);
	int nRetValue = ( sReadBuffer[0] == '1' );

	m_pGpioArray[nGpioIndex-1].nValue = nRetValue;
	log(LOG_DEBUG_PARANOIC+2, "Read gpio[%d]=%d", m_pGpioArray[nGpioIndex-1].nId, m_pGpioArray[nGpioIndex-1].nValue);
	return m_pGpioArray[nGpioIndex-1].nValue;
}


int GpioInterface::getValue(int nGpioIndex)
{
	if ( m_bConfigOk == false )
	{
		log(LOG_ERR, "Impossible to get value of gpio[%d] (configuration error)", nGpioIndex);
		return 2;
	}
	if ( (nGpioIndex < 1) || (nGpioIndex > m_nNumGpio) )
	{
		log(LOG_ERR, "Impossible to get value of gpio[%d] (not in the list)", nGpioIndex);
		return 2;
	}
	return m_pGpioArray[nGpioIndex-1].nValue;
}


int GpioInterface::waiForIrq(int nGpioIndex, int nMsecTimeout)
{
	if ( m_bConfigOk == false )
	{
		log(LOG_ERR, "Impossible to get value of gpio[%d] (configuration error)", nGpioIndex);
		return 2;
	}
	if ( ( nGpioIndex < 1 ) || ( nGpioIndex > m_nNumGpio ) )
	{
		log(LOG_ERR, "Impossible to get value of gpio[%d] (not in the list)", nGpioIndex);
		return 2;
	}
	if ( m_pGpioArray[nGpioIndex-1].pPollFD == NULL )
	{
		log(LOG_ERR, "The specified gpio[%d] is not configured as interrupt listener", nGpioIndex);
		return 2;
	}

	// Wait for an irq until timeout exceed
	int ret = poll(m_pGpioArray[nGpioIndex-1].pPollFD, 1, nMsecTimeout);
//	log(LOG_INFO, "irq=%d", ret);
	//if ( ret > 0 )
	//{
		// Read gpio
		char sReadBuffer[8] = {0};
		lseek(m_pGpioArray[nGpioIndex-1].nFD, 0, SEEK_SET);
		int nRetRead = read(m_pGpioArray[nGpioIndex-1].nFD, sReadBuffer, sizeof(sReadBuffer));
		if (nRetRead <= 0)
		{
			log(LOG_ERR, "Impossible to read from irq gpio[%d, unable to read file", m_pGpioArray[nGpioIndex-1].nId);
		}
		int nReadValue = ( sReadBuffer[0] == '1' );
		log(LOG_DEBUG_PARANOIC, "Read pin after irq value=%d", nReadValue);
	//}
	//else
	//{
	//	log(LOG_INFO, "Irq -> timeout ");
	//}
	return ret > 0 && (m_pGpioArray[nGpioIndex-1].pPollFD->revents & POLLPRI) != 0;
}


void GpioInterface::printConfiguration(void)
{
	if ( m_bConfigOk == false )
	{
		log(LOG_DEBUG, "Impossible to show configuration (configuration error)");
		return;
	}

    for (int i = 0; i < m_nNumGpio; i++)
	{
		log(LOG_DEBUG, "GPIO %d:    id=%d", i, m_pGpioArray[i].nId);
		if ( m_pGpioArray[i].nDirection == GPIO_DIR_OUT )
		{
			log(LOG_DEBUG, "           dir=out");
			log(LOG_DEBUG, "           defaultValue=%d", m_pGpioArray[i].nDefaultValue);
		}
		else
		{
			log(LOG_DEBUG, "           dir=in");
			log(LOG_DEBUG, "           defaultValue=N/A");
			if ( m_pGpioArray[i].pPollFD != NULL )
			{
				log(LOG_DEBUG, "           hasIrq=1");
				log(LOG_DEBUG, "           edge=%s", m_Config.GetString(CFG_GPIO_EDGE_1+i));
			}
		}
	}
}

void GpioInterface::closeInterface(void)
{
	reset();
}

bool GpioInterface::GpioWriteDirection(int nGpioIndex, int nDirection)
{

	if ( (nGpioIndex < 1) || (nGpioIndex > m_nNumGpio) )
	{
		log(LOG_ERR, "Impossible to write direction for gpio %d (not in the configuration list)", nGpioIndex);
		return false;
	}
	if ( (nDirection != GPIO_DIR_IN) && (nDirection != GPIO_DIR_OUT) )
	{
		log(LOG_ERR, "Impossible to write direction for gpio %d (direction not valid <%d>)", nGpioIndex, nDirection);
		return false;
	}

    unsigned nId = m_pGpioArray[nGpioIndex-1].nId;
	char sBuffer[1024] = "";

    if(snprintf(sBuffer, sizeof(sBuffer), "%sgpio%u/direction", m_sBaseDir, nId) < 0)
    {
        return false;
    }

	int nFd = open(sBuffer, O_WRONLY);
	if ( nFd < 0 )
	{
		log(LOG_ERR, "Impossible to write direction for gpio %d (unable to open file <%s>)", nGpioIndex, sBuffer);
		return false;
	}
	if ( nDirection == GPIO_DIR_OUT )
	{
		m_pGpioArray[nGpioIndex-1].nDirection = GPIO_DIR_OUT;
		write(nFd, "out", 4);
	}
	else
	{
		m_pGpioArray[nGpioIndex-1].nDirection = GPIO_DIR_IN;
		write(nFd, "in", 3);
	}
	close(nFd);
	return true;
}


bool GpioInterface::readConfigFile(const char* sConfigFileName)
{

	// Reads configuration file
	config_key_t config_keys[] =
	{

		// General config keys
		{ CFG_GPIO_BASE_DIR, (char*)"gpio.baseDir", (char*)"GENERAL", T_string, (char*)DEF_GPIO_BASE_DIR, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EXPORT_FILE, (char*)"gpio.exportFile", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EXPORT_FILE, DEFAULT_ACCEPTED},
		{ CFG_GPIO_UNEXPORT_FILE, (char*)"gpio.unexportFile", (char*)"GENERAL", T_string, (char*)DEF_GPIO_UNEXPORT_FILE, DEFAULT_ACCEPTED},

		// Single gpio configuration keys
		{ CFG_GPIO_ID_1, (char*)"gpio.1.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_1, (char*)"gpio.1.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_1, (char*)"gpio.1.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_1, (char*)"gpio.1.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_1, (char*)"gpio.1.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_2, (char*)"gpio.2.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_2, (char*)"gpio.2.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_2, (char*)"gpio.2.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_2, (char*)"gpio.2.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_2, (char*)"gpio.2.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_3, (char*)"gpio.3.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_3, (char*)"gpio.3.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_3, (char*)"gpio.3.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_3, (char*)"gpio.3.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_3, (char*)"gpio.3.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_4, (char*)"gpio.4.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_4, (char*)"gpio.4.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_4, (char*)"gpio.4.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_4, (char*)"gpio.4.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_4, (char*)"gpio.4.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_5, (char*)"gpio.5.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_5, (char*)"gpio.5.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_5, (char*)"gpio.5.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_5, (char*)"gpio.5.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_5, (char*)"gpio.5.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_6, (char*)"gpio.6.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_6, (char*)"gpio.6.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_6, (char*)"gpio.6.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_6, (char*)"gpio.6.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_6, (char*)"gpio.6.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_7, (char*)"gpio.7.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_7, (char*)"gpio.7.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_7, (char*)"gpio.7.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_7, (char*)"gpio.7.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_7, (char*)"gpio.7.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_8, (char*)"gpio.8.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_8, (char*)"gpio.8.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_8, (char*)"gpio.8.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_8, (char*)"gpio.8.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_8, (char*)"gpio.8.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_9, (char*)"gpio.9.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_9, (char*)"gpio.9.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_9, (char*)"gpio.9.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_9, (char*)"gpio.9.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_9, (char*)"gpio.9.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_10, (char*)"gpio.10.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_10, (char*)"gpio.10.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_10, (char*)"gpio.10.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_10, (char*)"gpio.10.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_10, (char*)"gpio.10.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_11, (char*)"gpio.11.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_11, (char*)"gpio.11.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_11, (char*)"gpio.11.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_11, (char*)"gpio.11.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_11, (char*)"gpio.11.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_12, (char*)"gpio.12.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_12, (char*)"gpio.12.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_12, (char*)"gpio.12.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_12, (char*)"gpio.12.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_12, (char*)"gpio.12.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_13, (char*)"gpio.13.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_13, (char*)"gpio.13.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_3, (char*)"gpio.13.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_13, (char*)"gpio.13.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_13, (char*)"gpio.13.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_14, (char*)"gpio.14.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_14, (char*)"gpio.14.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_14, (char*)"gpio.14.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_14, (char*)"gpio.14.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_14, (char*)"gpio.14.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_15, (char*)"gpio.15.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_15, (char*)"gpio.15.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_15, (char*)"gpio.15.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_15, (char*)"gpio.15.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_15, (char*)"gpio.15.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

		{ CFG_GPIO_ID_16, (char*)"gpio.16.id", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DIRECTION_16, (char*)"gpio.16.direction", (char*)"GENERAL", T_string, (char*)"out", DEFAULT_ACCEPTED},
		{ CFG_GPIO_DEFAULT_VALUE_16, (char*)"gpio.16.defaultValue", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_GPIO_HAS_IRQ_16, (char*)"gpio.16.hasIrq", (char*)"GENERAL", T_int, (char*)DEF_GPIO_HAS_IRQ_VAL, DEFAULT_ACCEPTED},
		{ CFG_GPIO_EDGE_16, (char*)"gpio.16.edge", (char*)"GENERAL", T_string, (char*)DEF_GPIO_EDGE_VAL, DEFAULT_ACCEPTED},

	};

	int num_keys = sizeof(config_keys) / sizeof(config_key_t);

	if ( !m_Config.Init(config_keys, num_keys, sConfigFileName) )
	{
		char error[1024];
		Config::GetLastError(error, 1024);
		log(LOG_ERR, "Impossible to read parameter from <%s> (error: <%s>)", sConfigFileName, error);
		m_Config.Free();
		return false;
	}

	return true;

}


void GpioInterface::reset(void)
{

	m_bConfigOk = false;
	for (int i=0; i<m_nNumGpio; i++)
	{
		closeAndUnExportGpio(i+1);
		SAFE_DELETE(m_pGpioArray[i].pPollFD);
	}
	SAFE_DELETE_ARRAY(m_pGpioArray);

	m_sBaseDir[0] = '\0';
	m_sExportFile[0] = '\0';
	m_sUnExportFile[0] = '\0';

}
