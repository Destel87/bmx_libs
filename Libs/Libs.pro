TEMPLATE = subdirs
SUBDIRS = \
    Config \
    Log \
    Thread \
    TimeStamp \
    ProcessManager \
    GpioInterface \
    I2CDevice \
    SPIInterface \
    CanInterface \
    DataCollector \
    IPC \
    Scheduler \
    SerialInterface \
    StaticSingleton \
    WebServer \
    HttpClient \
    XmlParser \
    I2CInterface \
    USBInterface \
    StateMachine \
    TARUtilities \
    UdpClient \
    SectionTimeCalc \
    EventRecorder \
    FolderManager

CONFIG += ordered
