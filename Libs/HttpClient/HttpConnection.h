//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HttpConnection.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the HttpConnection class.
//! @details
//!
//*!****************************************************************************

#ifndef HTTPCONNECTION_H
#define HTTPCONNECTION_H

#include <curl/curl.h>
#include <string>
#include <map>
#include <cstdlib>

#include "HttpClient.h"

/*! *******************************************************************************************************************
*  @struct HttpRequestInfo
*  @brief						holds some diagnostics information about a request
*  @var HttpRequestInfo::m_dTotalTimeSec the total time of the last request in seconds, i.e. the time of previous transfer.
*								See CURLINFO_TOTAL_TIME
*  @var HttpRequestInfo::m_dNameLookupTimeSec the time spent in DNS lookup in  seconds, i.e. time from start until
*								name resolving completed.
*								See CURLINFO_NAMELOOKUP_TIME
*  @var HttpRequestInfo::m_dConnectTimeSec the time it took until Time from start until remote host or proxy completed.
*								See CURLINFO_CONNECT_TIME
*  @var HttpRequestInfo::m_dAppConnectTimeSec the time from start until SSL/SSH handshake completed.
*								See CURLINFO_APPCONNECT_TIME
*  @var HttpRequestInfo::m_dPreTransferTimeSec contains the total time from start until just before the transfer begins.
*								See CURLINFO_PRETRANSFER_TIME
*  @var HttpRequestInfo::m_dStartTransferTimeSec contains the total time from start until just when the first byte is received.
*								See CURLINFO_STARTTRANSFER_TIME
*  @var HttpRequestInfo::m_dRedirectTime contains the total time taken for all redirect steps before the final transfer.
*								See CURLINFO_REDIRECT_TIME
*  @var HttpRequestInfo::m_liRedirectCount Member 'redirectCount' contains the number of redirects followed.
*								See CURLINFO_REDIRECT_COUNT
*
* *********************************************************************************************************************
*/
typedef struct
{
	double m_dTotalTimeSec;
	double m_dNameLookupTimeSec;
	double m_dConnectTimeSec;
	double m_dAppConnectTimeSec;
	double m_dPreTransferTimeSec;
	double m_dStartTransferTimeSec;
	double m_dRedirectTime;
	int m_liRedirectCount;
} HttpRequestInfo;


/*! *******************************************************************************************************************
*  @struct HttpInfo
*  @brief							holds some diagnostics information about the connection object it came from
*  @var m_strBaseUrl				the base URL for the connection object
*  @var m_headers					the HeaderFields map
*  @var m_liTimeout					the configured timeout
*  @var m_bFollowRedirects			whether or not to follow redirects
*  @var m_liMaxRedirects			the maximum number of redirect to follow (-1 unlimited)
*  @var m_basicAuth					information about basic auth,
*  @var m_basicAuth::strUserName	the basic auth username
*  @var m_basicAuth::strPassword	the basic auth password
*  @var m_strCertPath				the certificate file path
*  @var m_strCertType				the certificate type
*  @var m_strKeyPath				the SSL key file path
*  @var m_strKeyPassword			the SSWebClientL key password
*  @var m_strCustomUserAgent		the custom user agent
*  @var m_strUriProxy				the HTTP proxy address
*  @var m_strLastRequest			metrics about the last request
*
* *********************************************************************************************************************
*/
typedef struct
{
	std::string m_strBaseUrl;
	HeaderFields m_headers;
	int m_liTimeout;
	bool m_bFollowRedirects;
	int m_liMaxRedirects;
	bool m_bNoSignal;
	struct
	{
		std::string strUserName;
		std::string strPassword;
	} m_basicAuth;
	std::string m_strCertPath;
	std::string m_strCertType;
	std::string m_strKeyPath;
	std::string m_strKeyPassword;
	std::string m_strCustomUserAgent;
	std::string m_strUriProxy;
	std::string m_strUnixSocketPath;
	HttpRequestInfo m_strLastRequest;
} HttpInfo;

/*! *******************************************************************************************************************
 * @brief The HttpConnection class is a connection object for advanced usage, providing the standard HTTP methods
 * ********************************************************************************************************************
 */
class HttpConnection
{
	public:

		/*! ***********************************************************************************************************
		 * @brief HttpConnection	constructor for the Connection object
		 * @param m_strBaseUrl		base URL for the connection to use
		 * ************************************************************************************************************
		 */
		explicit HttpConnection(const std::string& m_strBaseUrl);

		/*! ***********************************************************************************************************
		 * @brief ~HttpConnection	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~HttpConnection();

		/*! ***********************************************************************************************************
		 * @brief SetBasicAuth		configure basic auth setting username and password for basic auth
		 * @param strUserName		basic auth username
         * @param sPassword			basic auth password
		 * ************************************************************************************************************
		 */
		void setBasicAuth(const std::string& strUserName, const std::string& sPassword);

		/*! ***********************************************************************************************************
		 * @brief SetTimeout		set timeout for connection
		 * @param liTimeoutSeconds	timeout in seconds
		 * ************************************************************************************************************
		 */
		void setTimeout(int liTimeoutSeconds);

		/*! ***********************************************************************************************************
		 * @brief SetNoSignal		switch off curl signals for connection (see CURLOPT_NONSIGNAL).
		 *							By default signals are used, except when timeout is given.
		 * @param bNoSignal			if set to true switches signals off
		 * ************************************************************************************************************
		 */
		void setNoSignal(bool bNoSignal);

		/*! ***********************************************************************************************************
		 * @brief FollowRedirects	configure whether to follow redirects on this connection
		 * @param bFollow			boolean whether to follow redirects
		 * @param liMaxRedirects	int indicating the maximum number of redirect to follow (-1 unlimited, default)
		 * ************************************************************************************************************
		 */
		void followRedirects(bool bFollow, int liMaxRedirects = -1l);

		/*! ***********************************************************************************************************
		 * @brief setUserAgent		set custom user agent for connection. This gets prepended to the
		 *							default ClientName/WEBCLIENT_VERSION string
		 * @param strUserAgent		custom userAgent prefix
		 * ************************************************************************************************************
		 */
		void setUserAgent(const std::string& strUserAgent);

		/*! ***********************************************************************************************************
		 * @brief setCAInfoFilePath	set custom Certificate Authority (CA) path
		 * @param strCaInfoFilePath	The path to a file holding the certificates used to verify the peer with
		 *							See CURLOPT_CAINFO
		 * ************************************************************************************************************
		 */
		void setCAInfoFilePath(const std::string& strCaInfoFilePath);

		/*! ***********************************************************************************************************
		 * @brief setCertPath		set certificate path. See CURLOPT_SSLCERT
		 * @param strCertPath		to certificate file
		 * ************************************************************************************************************
		 */
		void setCertPath(const std::string& strCertPath);

		/*! ***********************************************************************************************************
		 * @brief setCertType		set certificate type See CURLOPT_SSLCERTTYPE
		 * @param strCertType		certificate type (e.g. "PEM" or "DER")
		 * ************************************************************************************************************
		 */
		void setCertType(const std::string& strCertType);

		/*! ***********************************************************************************************************
		 * @brief setKeyPath		set key path. See CURLOPT_SSLKEY. Default format is PEM
		 * @param strKeyPath		path to key file
		 * ************************************************************************************************************
		 */
		void setKeyPath(const std::string& strKeyPath);

		/*! ***********************************************************************************************************
		 * @brief setKeyPassword	set key password. See CURLOPT_KEYPASSWD.
		 * @param strKeyPassword	key password
		 * ************************************************************************************************************
		 */
		void setKeyPassword(const std::string& strKeyPassword);

		/*! ***********************************************************************************************************
		 * @brief setProxy			set HTTP proxy address and port. See CURLOPT_PROXY.
         * @param strUriProxy		address with port number
		 * ************************************************************************************************************
		 */
		void setProxy(const std::string& strUriProxy);

		/*! ***********************************************************************************************************
		 * @brief setUnixSocketPath	set custom Unix socket path for connection.
		 *							See https://curl.haxx.se/libcurl/c/CURLOPT_UNIX_SOCKET_PATH.html
		 *							See CURLOPT_UNIX_SOCKET_PATH
		 * @param strUnixSocketPath	path to Unix socket (ex: /var/run/docker.sock)
		 * ************************************************************************************************************
		 */
		void setUnixSocketPath(const std::string& strUnixSocketPath);

		/*! ***********************************************************************************************************
		 * @brief getUserAgent		get the user agent to add to the request
		 * @return user agent as std::string
		 * ************************************************************************************************************
		 */
		std::string getUserAgent(void);

		/*! ***********************************************************************************************************
		 * @brief getInfo			get diagnostic information about the connection object
		 * @return Connection::Info struct
		 * ************************************************************************************************************
		 */
		HttpInfo getInfo(void);

		/*! ***********************************************************************************************************
		 * @brief setHeaders		set the custom headers map. This will replace the currently configured headers
		 *							with the provided ones. If you want to add additional headers, use AppendHeader()
		 * @param headers to set
		 * ************************************************************************************************************
		 */
		void setHeaders(HeaderFields headers);

		/*! ***********************************************************************************************************
		 * @brief getHeaders		get all custom headers set on the connection
		 * @returns a HeaderFields map containing the custom headers
		 * ************************************************************************************************************
		 */
		HeaderFields getHeaders();

		/*! ***********************************************************************************************************
		 * @brief appendHeader		append a header to the internal map
		 * @param strKey			key for the header field
		 * @param strValue			value for the header field
		 * ************************************************************************************************************
		 */
		void appendHeader(const std::string& strKey, const std::string& strValue);

		/*! ***********************************************************************************************************
		 * @brief get				HTTP GET method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse get(const std::string& strUrl);

		/*! ***********************************************************************************************************
		 * @brief post				HTTP POST method
		 * @param strUrl			url to query
		 * @param strData			HTTP POST body
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse post(const std::string& strUrl, const std::string& strData);

		/*! ***********************************************************************************************************
		 * @brief put				HTTP PUT method
		 * @param strUrl			url to query
		 * @param strData			HTTP PUT body
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse put(const std::string& strUrl, const std::string& strData);

		/*! ***********************************************************************************************************
		 * @brief patch				HTTP PATCH method
		 * @param strUrl			url to query
		 * @param strData			HTTP PATCH body
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse patch(const std::string& strUrl, const std::string& strData);

		/*! ***********************************************************************************************************
		 * @brief del				HTTP DELETE method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse del(const std::string& strUrl);

		/*! ***********************************************************************************************************
		 * @brief head				HTTP HEAD method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse head(const std::string& strUrl);

		/*! ***********************************************************************************************************
		 * @brief options			HTTP OPTIONS method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse options(const std::string& strUrl);

    private:
        /*! ***********************************************************************************************************
         * @brief performCurlRequest	helper function to get called from the actual request methods to prepare
         *								the curlHandle for transfer with generic options. This method performs
         *								the request	and records some stats from the last request, then resets
         *								the handle with curl_easy_reset to its default state.
         *								This will keep things like connections and session ID intact but makes
         *								sure you can change parameters on the object for another request.
         * @param strUri				URI to query
         * @return response struct that gets filled with results
         * ************************************************************************************************************
         */
        HttpResponse performCurlRequest(const std::string& strUri);

    private:

        CURL* m_pCurlHandle;
        std::string m_strBaseUrl;
        HeaderFields m_headerFields;
        int m_liTimeoutSeconds;
        bool m_bFollowRedirects;
        int m_liMaxRedirects;
        bool m_bNoSignal;
        struct
        {
            std::string m_strUserName;
            std::string m_strPassword;
        } m_basicAuth;
        std::string m_strCustomUserAgent;
        std::string m_strCaInfoFilePath;
        HttpRequestInfo m_lastRequest;
        std::string m_strCertPath;
        std::string m_strCertType;
        std::string m_strKeyPath;
        std::string m_strKeyPassword;
        std::string m_strUriProxy;
        std::string m_strUnixSocketPath;

};

#endif // HTTPCONNECTION_H
