//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HttpClientHelpers.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the general functions of the HttpCient lib.
//! @details
//!
//*!****************************************************************************

#ifndef HTTPCLIENTHELPERS_H
#define HTTPCLIENTHELPERS_H

#include <string>
#include <cctype>
#include <algorithm>
#include <functional>

/*! *******************************************************************************************************************
 * @struct UploadObject
 * @brief This structure represents the payload to upload on POST requests
 * @var UploadObject::data	Member 'data' contains the data to upload
 * @var UploadObject::length	Member 'length' contains the length of the data to upload
 * ********************************************************************************************************************
*/
typedef struct
{
	const char* data;
	size_t length;
} UploadObject;

/*! *******************************************************************************************************************
 * @brief write callback function for libcurl
 * @param ptr returned data of size (size*nmemb)
 * @param size size parameter
 * @param nmemb memblock parameter
 * @param userdata pointer to user data to save/work with return data
 * @return (size * nmemb)
 * ********************************************************************************************************************
 */
size_t write_callback(void *ptr, size_t size, size_t nmemb, void *userdata);

/*! *******************************************************************************************************************
 * @brief header callback for libcurl
 * @param ptr data returned (header line)
 * @param size of data
 * @param nmemb memblock
 * @param userdata pointer to user data object to save headr data
 * @return size * nmemb;
 * ********************************************************************************************************************
 */
size_t header_callback(void *ptr, size_t size, size_t nmemb, void *userdata);

/*! *******************************************************************************************************************
 * @brief read callback function for libcurl
 * @param ptr data pointer of max size (size*nmemb) to write data to
 * @param size size parameter
 * @param nmemb memblock parameter
 * @param userdata pointer to user data to read data from
 * @return (size * nmemb)
 * ********************************************************************************************************************
 */
size_t read_callback(void *ptr, size_t size, size_t nmemb, void *userdata);

/*! *******************************************************************************************************************
 * @brief ltrim trim a string from start
 * @param s	string to trim
 * @return the string trimmed
 * ********************************************************************************************************************
 */
static inline std::string &ltrim(std::string &s)
{
	// NOLINT
	s.erase(s.begin(), std::find_if(s.begin(), s.end(),	std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

/*! *******************************************************************************************************************
 * @brief rtrim trim a string from end
 * @param s	string to trim
 * @return the string trimmed
 * ********************************************************************************************************************
 */
static inline std::string &rtrim(std::string &s)
{
	// NOLINT
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

/*! *******************************************************************************************************************
 * @brief trim trim a string from both ends
 * @param s	string to trim
 * @return the string trimmed
 * ********************************************************************************************************************
 */
static inline std::string &trim(std::string &s)
{
	// NOLINT
	return ltrim(rtrim(s));
}

#endif // HTTPCLIENTHELPERS_H
