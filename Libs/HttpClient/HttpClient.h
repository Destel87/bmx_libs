//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HttpClient.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the HttpClient class.
//! @details
//!
//*!****************************************************************************

#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H


#include <string>
#include <map>
#include <cstdlib>

#define HTTPCLIENT_VERSION	"1.0.0"

/*! *******************************************************************************************************************
 * @brief HeaderFields container of http headers that are couples of name=value
 * ********************************************************************************************************************
 */
typedef std::map<std::string, std::string> HeaderFields;


/*! *******************************************************************************************************************
 * @struct HttpResponse
 * @brief This structure		represents the HTTP response data
 * @var HttpResponse::code		Member 'code' contains the HTTP response code
 * @var HttpResponse::body		Member 'body' contains the HTTP response body
 * @var HttpResponse::headers	Member 'headers' contains the HTTP response headers
 * ********************************************************************************************************************
*/
typedef struct
{
	int code;
	std::string body;
	HeaderFields headers;
} HttpResponse;


/*! *******************************************************************************************************************
 * @brief The HttpClient class implements a HTTP REST client with standard HTTP methods
 * ********************************************************************************************************************
 */
class HttpClient
{

	public:

		/*! ***********************************************************************************************************
		 * @brief HttpClient		void constructor
		 * ************************************************************************************************************
		 */
		HttpClient();

		/*! ***********************************************************************************************************
		 * @brief ~HttpClient		default destructor
		 * ************************************************************************************************************
		 */
		virtual ~HttpClient();

		/*! ***********************************************************************************************************
		 * @brief init				global init function, performs a curl_global_init(),
		 *							must be called before using any other method of this class
		 * @return					0 upon successfull initialization, 1 otherwise
		 * ************************************************************************************************************
		 */
		int init();

		/*! ***********************************************************************************************************
		 * @brief disable			global disable function, performs a curl_global_cleanup(),
		 *							must be called before the destructor gets called
		 * ************************************************************************************************************
		 */
		void disable();

		/*! ***********************************************************************************************************
		 * @brief get				HTTP GET method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse get(const std::string& strUrl);

		/*! ***********************************************************************************************************
		 * @brief post				HTTP POST method
		 * @param strUrl			url to query
		 * @param strContentType	content type as string
		 * @param strData			HTTP POST body
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse post(const std::string& strUrl, const std::string& strContentType, const std::string& strData);

		/*! ***********************************************************************************************************
		 * @brief put				HTTP PUT method
		 * @param strUrl			url to query
		 * @param strContentType	content type as string
		 * @param strData			HTTP PUT body
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse put(const std::string& strUrl, const std::string& strContentType, const std::string& strData);

		/*! ***********************************************************************************************************
		 * @brief patch				HTTP PATCH method
		 * @param strUrl			url to query
		 * @param strContentType	content type as string
		 * @param strData			HTTP PATCH body
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse patch(const std::string& strUrl, const std::string& strContentType, const std::string& strData);

		/*! ***********************************************************************************************************
		 * @brief del				HTTP DELETE method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse del(const std::string& strUrl);

		/*! ***********************************************************************************************************
		 * @brief head				HTTP HEAD method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse head(const std::string& strUrl);

		/*! ***********************************************************************************************************
		 * @brief options			HTTP OPTIONS method
		 * @param strUrl			url to query
		 * @return					HttpResponse struct
		 * ************************************************************************************************************
		 */
		HttpResponse options(const std::string& strUrl);

        /*! ***********************************************************************************************************
         * @brief transferFile		transfer a file from a server to a client using curl methods
         * @param sourcePath the input file path on the server (http://<ip.address>/folder/filename)
         * @param destinationPath the output file path on the client
         * @param strError the error string (empty if OK)
         * @return	true if OK
         * ************************************************************************************************************
         */
        bool transferFile(const std::string sourcePath, const std::string destinationPath, std::string strError);
};

#endif // HTTPCLIENT_H
