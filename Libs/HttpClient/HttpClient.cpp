//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HttpClient.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implmentation for the HttpClient class.
//! @details
//!
//*!****************************************************************************

#include <curl/curl.h>
#include <curl/easy.h>

#include "HttpClient.h"
#include "HttpConnection.h"

// function used to write file during transfer from gateway
size_t writeDataToFile(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t written;

    written = fwrite(ptr, size, nmemb, stream);
    return written;
}

HttpClient::HttpClient()
{

}

HttpClient::~HttpClient()
{

}

int HttpClient::init()
{
	CURLcode res = curl_global_init(CURL_GLOBAL_ALL);
	if ( res == CURLE_OK )
		return 0;
	else
		return 1;
}

void HttpClient::disable()
{
	curl_global_cleanup();
}

HttpResponse HttpClient::get(const std::string& strUrl)
{
	HttpResponse ret;
	HttpConnection *conn = new HttpConnection("");
	ret = conn->get(strUrl);
	delete conn;
	return ret;
}

HttpResponse HttpClient::post(const std::string& strUrl, const std::string& strContentType, const std::string& strData)
{
	HttpResponse ret;
	HttpConnection *conn = new HttpConnection("");
	conn->appendHeader("Content-Type", strContentType);
	ret = conn->post(strUrl, strData);
	delete conn;
	return ret;
}

HttpResponse HttpClient::put(const std::string& strUrl, const std::string& strContentType, const std::string& strData)
{
	HttpResponse ret;
	HttpConnection *conn = new HttpConnection("");
	conn->appendHeader("Content-Type", strContentType);
	ret = conn->put(strUrl, strData);
	delete conn;
	return ret;
}

HttpResponse HttpClient::patch(const std::string& strUrl, const std::string& strContentType, const std::string& strData)
{
	HttpResponse ret;
	HttpConnection *conn = new HttpConnection("");
	conn->appendHeader("Content-Type", strContentType);
	ret = conn->patch(strUrl, strData);
	delete conn;
	return ret;
}

HttpResponse HttpClient::del(const std::string& strUrl)
{
	HttpResponse ret;
	HttpConnection *conn = new HttpConnection("");
	ret = conn->del(strUrl);
	delete conn;
	return ret;
}

HttpResponse HttpClient::head(const std::string& strUrl)
{
	HttpResponse ret;
	HttpConnection *conn = new HttpConnection("");
	ret = conn->head(strUrl);
	delete conn;
	return ret;
}

HttpResponse HttpClient::options(const std::string& strUrl)
{
	HttpResponse ret;
	HttpConnection *conn = new HttpConnection("");
	ret = conn->options(strUrl);
	delete conn;
	return ret;
}


bool HttpClient::transferFile(const std::string sourcePath, const std::string destinationPath, std::string strError)
{
    CURL *curl;
    FILE *fp;
    CURLcode res;

    strError.clear();

    curl_version_info_data * vinfo = curl_version_info(CURLVERSION_NOW);

    if(vinfo->features & CURL_VERSION_SSL)
    {
        printf("CURL: SSL enabled\n");
    }
    else
    {
        printf("CURL: SSL not enabled\n");
    }

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(destinationPath.c_str(), "wb");

        // Setup the https:// verification options. Note we
        // do this on all requests as there may be a redirect
        // from http to https and we still want to verify
        curl_easy_setopt(curl, CURLOPT_URL, sourcePath.c_str());
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_CAINFO, "./ca-bundle.crt");
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_easy_setopt(curl, CURLOPT_REDIR_PROTOCOLS, CURLPROTO_ALL);
        curl_easy_setopt (curl, CURLOPT_VERBOSE, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeDataToFile);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);

        char *url = NULL;
        curl_easy_getinfo(curl, CURLINFO_REDIRECT_URL, &url);
        if(url)
          printf("Redirect to: %s\n", url);

        curl_easy_cleanup(curl);

        fclose(fp);

        if ( res != CURLE_OK )
        {
            switch (res)
            {
                case CURLE_OPERATION_TIMEDOUT:
                    strError.assign("Operation Timeout.");
                break;
                case CURLE_SSL_CERTPROBLEM:
                    strError.assign(curl_easy_strerror(res));
                break;
                default:
                    strError.assign("Failed to query.");
            }
        }
    }
    else
    {
        res = CURLE_FAILED_INIT;
        strError.assign("Operation Timeout.");
    }

    return (res == CURLE_OK);
}

