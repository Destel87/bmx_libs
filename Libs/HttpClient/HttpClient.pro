QT		-= core gui
TARGET		= HttpClient
TEMPLATE	= lib
CONFIG		+= staticlib

# internal libraries inclusion
INCLUDEPATH += $$PWD/../Log

HEADERS += \
    HttpConnection.h \
    HttpClient.h \
    HttpClientHelpers.h

SOURCES	+= \
    HttpConnection.cpp \
    HttpClient.cpp \
    HttpClientHelpers.cpp


unix:!macx: LIBS += -lcurl
