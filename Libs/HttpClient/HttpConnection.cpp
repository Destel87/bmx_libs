//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HttpConnection.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the HttpConnection class.
//! @details
//!
//*!****************************************************************************

#include "HttpConnection.h"
#include <curl/curl.h>

#include <cstring>
#include <string>
#include <iostream>
#include <map>
#include <stdexcept>
#include <utility>

#include "HttpClient.h"
#include "HttpClientHelpers.h"


HttpResponse HttpConnection::performCurlRequest(const std::string& strUri)
{
	// init return type
	HttpResponse ret = {};

	std::string url = std::string(m_strBaseUrl + strUri);
	std::string headerString;
	CURLcode res = CURLE_OK;
	curl_slist* headerList = NULL;

	// set query URL
	curl_easy_setopt(m_pCurlHandle, CURLOPT_URL, url.c_str());
	// set callback function
	curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEFUNCTION, write_callback);
	// set data object to pass to callback function
	curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEDATA, &ret);
	// set the header callback function
	curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERFUNCTION, header_callback);
	// callback object for headers
	curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERDATA, &ret);

	// set http headers
	for (HeaderFields::const_iterator it = m_headerFields.begin(); it != m_headerFields.end(); ++it)
	{
		headerString = it->first;
		headerString += ": ";
		headerString += it->second;
		headerList = curl_slist_append(headerList, headerString.c_str());
	}
	curl_easy_setopt(m_pCurlHandle, CURLOPT_HTTPHEADER, headerList);

	/* keep alive
	curl_easy_setopt(m_pCurlHandle,  CURLOPT_TCP_KEEPALIVE, 1L);
	curl_easy_setopt(m_pCurlHandle,  CURLOPT_TCP_KEEPIDLE, 120L);
	curl_easy_setopt(m_pCurlHandle,  CURLOPT_TCP_KEEPINTVL, 60L);
	*/

	// set basic auth if configured
	if (m_basicAuth.m_strUserName.length() > 0)
	{
		std::string authString = std::string(m_basicAuth.m_strUserName + ":" + m_basicAuth.m_strPassword);
		curl_easy_setopt(m_pCurlHandle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_easy_setopt(m_pCurlHandle, CURLOPT_USERPWD, authString.c_str());
	}
	// set user agent
	curl_easy_setopt(m_pCurlHandle, CURLOPT_USERAGENT, getUserAgent().c_str());

	// set timeout
	if (m_liTimeoutSeconds)
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_TIMEOUT, m_liTimeoutSeconds);
		// dont want to get a sig alarm on timeout
		curl_easy_setopt(m_pCurlHandle, CURLOPT_NOSIGNAL, 1);
	}
	// set follow redirect
	if (m_bFollowRedirects == true)
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(m_pCurlHandle, CURLOPT_MAXREDIRS, static_cast<int64_t>(m_liMaxRedirects));
	}

	if (m_bNoSignal)
	{
		// multi-threaded and prevent entering foreign signal handler (e.g. JNI)
		curl_easy_setopt(m_pCurlHandle, CURLOPT_NOSIGNAL, 1);
	}

	// if provided, supply CA path
	if (!m_strCaInfoFilePath.empty())
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_CAINFO, m_strCaInfoFilePath.c_str());
	}

	// set cert file path
	if (!m_strCertPath.empty())
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_SSLCERT, m_strCertPath.c_str());
	}

	// set cert type
	if (!m_strCertType.empty())
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_SSLCERTTYPE, m_strCertType.c_str());
	}
	// set key file path
	if (!m_strKeyPath.empty())
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_SSLKEY, m_strKeyPath.c_str());
	}
	// set key password
	if (!m_strKeyPassword.empty())
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_KEYPASSWD, m_strKeyPassword.c_str());
	}

	// set web proxy address
	if (!m_strUriProxy.empty())
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_PROXY, m_strUriProxy.c_str());
		curl_easy_setopt(m_pCurlHandle, CURLOPT_HTTPPROXYTUNNEL, 1L);
	}

	// set Unix socket path, if requested
	if (!m_strUnixSocketPath.empty())
	{
		curl_easy_setopt(m_pCurlHandle, CURLOPT_UNIX_SOCKET_PATH, m_strUnixSocketPath.c_str());
	}

	res = curl_easy_perform(m_pCurlHandle);
	if ( res != CURLE_OK )
	{
		switch (res)
		{
			case CURLE_OPERATION_TIMEDOUT:
				ret.code = res;
				ret.body = "Operation Timeout.";
			break;
			case CURLE_SSL_CERTPROBLEM:
				ret.code = res;
				ret.body = curl_easy_strerror(res);
			break;
			default:
				ret.body = "Failed to query.";
				ret.code = -1;
		}
	}
	else
	{
		int64_t http_code = 0;
		curl_easy_getinfo(m_pCurlHandle, CURLINFO_RESPONSE_CODE, &http_code);
		ret.code = static_cast<int>(http_code);
	}

	curl_easy_getinfo(m_pCurlHandle, CURLINFO_TOTAL_TIME, &m_lastRequest.m_dTotalTimeSec);
	curl_easy_getinfo(m_pCurlHandle, CURLINFO_NAMELOOKUP_TIME, &m_lastRequest.m_dNameLookupTimeSec);
	curl_easy_getinfo(m_pCurlHandle, CURLINFO_CONNECT_TIME, &m_lastRequest.m_dConnectTimeSec);
	curl_easy_getinfo(m_pCurlHandle, CURLINFO_APPCONNECT_TIME, &m_lastRequest.m_dAppConnectTimeSec);
	curl_easy_getinfo(m_pCurlHandle, CURLINFO_PRETRANSFER_TIME, &m_lastRequest.m_dPreTransferTimeSec);
	curl_easy_getinfo(m_pCurlHandle, CURLINFO_STARTTRANSFER_TIME, &m_lastRequest.m_dStartTransferTimeSec);
	curl_easy_getinfo(m_pCurlHandle, CURLINFO_REDIRECT_TIME, &m_lastRequest.m_dRedirectTime);
	curl_easy_getinfo(m_pCurlHandle, CURLINFO_REDIRECT_COUNT, &m_lastRequest.m_liRedirectCount);
	// free header list
	curl_slist_free_all(headerList);
	// reset curl handle
	curl_easy_reset(m_pCurlHandle);
	return ret;
}

HttpConnection::HttpConnection(const std::string& baseUrl) :  m_headerFields(), m_lastRequest()
{
	m_pCurlHandle = curl_easy_init();
	if ( !m_pCurlHandle )
	{
		throw std::runtime_error("Couldn't initialize curl handle");
	}
	m_strBaseUrl = baseUrl;
	m_liTimeoutSeconds = 0;
	m_bFollowRedirects = false;
	m_liMaxRedirects = -1l;
	m_bNoSignal = false;
}

HttpConnection::~HttpConnection()
{
	if ( m_pCurlHandle )
	{
		curl_easy_cleanup(m_pCurlHandle);
	}
}

void HttpConnection::setBasicAuth(const std::string& strUserName, const std::string& sPassword)
{
	m_basicAuth.m_strUserName = strUserName;
	m_basicAuth.m_strPassword = sPassword;
}


void HttpConnection::setTimeout(int liTimeoutSeconds)
{
	m_liTimeoutSeconds = liTimeoutSeconds;
}

void HttpConnection::setNoSignal(bool bNoSignal)
{
	m_bNoSignal = bNoSignal;
}

void HttpConnection::followRedirects(bool bFollow, int liMaxRedirects)
{
	m_bFollowRedirects = bFollow;
	m_liMaxRedirects = liMaxRedirects;
}

void HttpConnection::setUserAgent(const std::string& strUserAgent)
{
	m_strCustomUserAgent = strUserAgent;
}

void HttpConnection::setCAInfoFilePath(const std::string& strCaInfoFilePath)
{
	m_strCaInfoFilePath = strCaInfoFilePath;
}

void HttpConnection::setCertPath(const std::string& strCertPath)
{
	m_strCertPath = strCertPath;
}

void HttpConnection::setCertType(const std::string& strCertType)
{
	m_strCertType = strCertType;
}

void HttpConnection::setKeyPath(const std::string& strKeyPath)
{
	m_strKeyPath = strKeyPath;
}

void HttpConnection::setKeyPassword(const std::string& strKeyPassword)
{
	m_strKeyPassword = strKeyPassword;
}

void HttpConnection::setProxy(const std::string& strUriProxy)
{
	std::string uriProxyUpper = strUriProxy;
	// check if the provided address is prefixed with "http"
	std::transform(uriProxyUpper.begin(), uriProxyUpper.end(), uriProxyUpper.begin(), ::toupper);
	if ((strUriProxy.length() > 0) && (uriProxyUpper.compare(0, 4, "HTTP") != 0))
	{
		m_strUriProxy = "http://" + strUriProxy;
	}
	else
	{
		m_strUriProxy = strUriProxy;
	}
}

void HttpConnection::setUnixSocketPath(const std::string& strUnixSocketPath)
{
	m_strUnixSocketPath = strUnixSocketPath;
}

std::string HttpConnection::getUserAgent(void)
{
	std::string prefix;
	if (m_strCustomUserAgent.length() > 0)
	{
		prefix = m_strCustomUserAgent + " ";
	}
	return std::string(prefix + "WebClient/" + HTTPCLIENT_VERSION);
}

HttpInfo HttpConnection::getInfo(void)
{
	HttpInfo ret;
	ret.m_strBaseUrl = m_strBaseUrl;
	ret.m_headers = getHeaders();
	ret.m_liTimeout = m_liTimeoutSeconds;
	ret.m_bFollowRedirects = m_bFollowRedirects;
	ret.m_liMaxRedirects = m_liMaxRedirects;
	ret.m_bNoSignal = m_bNoSignal;
	ret.m_basicAuth.strUserName = m_basicAuth.m_strUserName;
	ret.m_basicAuth.strPassword = m_basicAuth.m_strPassword;
	ret.m_strCustomUserAgent = m_strCustomUserAgent;
	ret.m_strLastRequest = m_lastRequest;
	ret.m_strCertPath = m_strCertPath;
	ret.m_strCertType = m_strCertType;
	ret.m_strKeyPath = m_strKeyPath;
	ret.m_strKeyPassword = m_strKeyPassword;
	ret.m_strUriProxy = m_strUriProxy;
	ret.m_strUnixSocketPath = m_strUnixSocketPath;
	return ret;
}

void HttpConnection::setHeaders(HeaderFields headers)
{
#if __cplusplus >= 201103L
	m_headerFields = std::move(headers);
#else
	headerFields = headers;
#endif
}

HeaderFields HttpConnection::getHeaders()
{
	return m_headerFields;
}

void HttpConnection::appendHeader(const std::string& strKey, const std::string& strValue)
{
	m_headerFields[strKey] = strValue;
}

HttpResponse HttpConnection::get(const std::string& strUrl)
{
	return performCurlRequest(strUrl);
}

HttpResponse HttpConnection::post(const std::string& strUrl, const std::string& strData)
{
	// Now specify we want to POST data
	curl_easy_setopt(m_pCurlHandle, CURLOPT_POST, 1L);
	// set post fields
	curl_easy_setopt(m_pCurlHandle, CURLOPT_POSTFIELDS, strData.c_str());
	curl_easy_setopt(m_pCurlHandle, CURLOPT_POSTFIELDSIZE, strData.size());

	return performCurlRequest(strUrl);
}

HttpResponse HttpConnection::put(const std::string& strUrl, const std::string& strData)
{
	// initialize upload object
	UploadObject up_obj;
	up_obj.data = strData.c_str();
	up_obj.length = strData.size();

	// Now specify we want to PUT data
	curl_easy_setopt(m_pCurlHandle, CURLOPT_PUT, 1L);
	curl_easy_setopt(m_pCurlHandle, CURLOPT_UPLOAD, 1L);
	// set read callback function
	curl_easy_setopt(m_pCurlHandle, CURLOPT_READFUNCTION, read_callback);
	// set data object to pass to callback function
	curl_easy_setopt(m_pCurlHandle, CURLOPT_READDATA, &up_obj);
	// set data size
	curl_easy_setopt(m_pCurlHandle, CURLOPT_INFILESIZE, static_cast<int64_t>(up_obj.length));
	return performCurlRequest(strUrl);
}

HttpResponse HttpConnection::patch(const std::string& strUrl, const std::string& strData)
{
	// initialize upload object
	UploadObject up_obj;
	up_obj.data = strData.c_str();
	up_obj.length = strData.size();

	// we want HTTP PATCH
	const char* http_patch = "PATCH";

	// set HTTP PATCH METHOD
	curl_easy_setopt(m_pCurlHandle, CURLOPT_CUSTOMREQUEST, http_patch);
	curl_easy_setopt(m_pCurlHandle, CURLOPT_UPLOAD, 1L);
	// set read callback function
	curl_easy_setopt(m_pCurlHandle, CURLOPT_READFUNCTION, read_callback);
	// set data object to pass to callback function
	curl_easy_setopt(m_pCurlHandle, CURLOPT_READDATA, &up_obj);
	// set data size
	curl_easy_setopt(m_pCurlHandle, CURLOPT_INFILESIZE, static_cast<int64_t>(up_obj.length));
	return performCurlRequest(strUrl);
}

HttpResponse HttpConnection::del(const std::string& strUrl)
{
	// we want HTTP DELETE
	const char* http_delete = "DELETE";
	// set HTTP DELETE METHOD
	curl_easy_setopt(m_pCurlHandle, CURLOPT_CUSTOMREQUEST, http_delete);
	return performCurlRequest(strUrl);
}

HttpResponse HttpConnection::head(const std::string& strUrl)
{
	// we want HTTP HEAD
	const char* http_head = "HEAD";
	// set HTTP HEAD METHOD
	curl_easy_setopt(m_pCurlHandle, CURLOPT_CUSTOMREQUEST, http_head);
	curl_easy_setopt(m_pCurlHandle, CURLOPT_NOBODY, 1L);
	return performCurlRequest(strUrl);
}

HttpResponse HttpConnection::options(const std::string& strUrl)
{
	// we want HTTP OPTIONS
	const char* http_options = "OPTIONS";
	// set HTTP HEAD METHOD
	curl_easy_setopt(m_pCurlHandle, CURLOPT_CUSTOMREQUEST, http_options);
	curl_easy_setopt(m_pCurlHandle, CURLOPT_NOBODY, 1L);
	return performCurlRequest(strUrl);
}
