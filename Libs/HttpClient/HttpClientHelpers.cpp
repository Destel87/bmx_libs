//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HttpClientHelpers.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation of the general functions of the HttpClient lib.
//! @details
//!
//*!****************************************************************************


#include <cstring>

#include "HttpClientHelpers.h"
#include "HttpClient.h"

size_t write_callback(void *data, size_t size, size_t nmemb, void *userdata)
{
	HttpResponse* r;
	r = reinterpret_cast<HttpResponse*>(userdata);
	r->body.append(reinterpret_cast<char*>(data), size*nmemb);
	return (size * nmemb);
}


size_t header_callback(void *data, size_t size, size_t nmemb, void *userdata)
{
	HttpResponse* r;
	r = reinterpret_cast<HttpResponse*>(userdata);
	std::string header(reinterpret_cast<char*>(data), size*nmemb);
	size_t seperator = header.find_first_of(':');
	if ( std::string::npos == seperator )
	{
		// roll with non seperated headers...
		trim(header);
		if (0 == header.length())
		{
			return (size * nmemb);  // blank line;
		}
		r->headers[header] = "present";
	}
	else
	{
		std::string key = header.substr(0, seperator);
		trim(key);
		std::string value = header.substr(seperator + 1);
		trim(value);
		r->headers[key] = value;
	}
	return (size * nmemb);
}


size_t read_callback(void *data, size_t size, size_t nmemb, void *userdata)
{
	// get upload struct
	UploadObject* u;
	u = reinterpret_cast<UploadObject*>(userdata);
	// set correct sizes
	size_t curl_size = size * nmemb;
	size_t copy_size = (u->length < curl_size) ? u->length : curl_size;
	// copy data to buffer
	std::memcpy(data, u->data, copy_size);
	// decrement length and increment data pointer
	u->length -= copy_size;
	u->data += copy_size;
	// return copied size
	return copy_size;
}
