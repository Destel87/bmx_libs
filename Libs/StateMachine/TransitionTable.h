//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TransitionTable.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the TransitionTable class.
//! @details
//!
//*!****************************************************************************

#ifndef TRANSITIONTABLE_H
#define TRANSITIONTABLE_H

#include <vector>
#include <string>

using namespace std;

class TransitionTableEntry;

class TransitionTable
{
	public:

		TransitionTable(string strId);
		virtual ~TransitionTable();
		string getId(void);
		int isAllowed(int liActualState, int liTargetState);
		void addRow(TransitionTableEntry* pRow);
		void addChild(TransitionTable* pChild);
		void print(void);


    private:

        string m_strId;
        vector<TransitionTableEntry*> m_rows;
        vector<TransitionTable*> m_children;

};

#endif // TRANSITIONTABLE_H
