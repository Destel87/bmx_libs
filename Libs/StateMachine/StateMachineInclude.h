//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    StateMachineInclude.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the defines for the StateMachine lib.
//! @details
//!
//*!****************************************************************************

#ifndef STATEMACHINEINCLUDE_H
#define STATEMACHINEINCLUDE_H

#define STATE_UNDEFINED		999999999

#define	TRANSITION_ALLOWED						0
#define	TRANSITION_NOT_ALLOWED					1
#define	TRANSITION_TABLE_UNDEFINED				2
#define	TARGET_STATE_MACHINE_UNDEFINED			3
#define	TARGET_STATE_MACHINE_ID_UNKNOWN			4


#endif // STATEMACHINEINCLUDE_H
