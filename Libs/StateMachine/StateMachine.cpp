//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    StateMachine.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the StateMachine class.
//! @details
//!
//*!****************************************************************************

#include <iostream>

#include "StateMachine.h"
#include "TransitionTable.h"

StateMachine::StateMachine(string strId, int liInitialStateValue, bool bHasMutex)
{
	m_strId.assign(strId);
	m_liActualState = liInitialStateValue;
	m_children.clear();
	m_pTransitionTable = 0;
	m_bHasMutex = bHasMutex;
}

StateMachine::~StateMachine()
{
	for ( auto it = m_children.begin(); it != m_children.end(); ++it )
	{
		//cout << "Destructor of SM " << m_strId << " -> delete child SM " << (*it)->getId() << endl;
		delete(*it);
	}
}

string StateMachine::getId(void)
{
	return m_strId;
}

void StateMachine::setId(const string& strId)
{
	m_strId.assign(strId);
}

int StateMachine::getState() const
{
	return m_liActualState;
}

void StateMachine::setState(int liActualState)
{
	m_liActualState = liActualState;
}

void StateMachine::addChild(StateMachine* pChild)
{
	m_children.push_back(pChild);
}

vector<StateMachine*>* StateMachine::getChildren(void)
{
	return &m_children;
}

bool StateMachine::setTransitionTable(TransitionTable* pTT)
{
	if ( pTT == 0 )
	{
		return false;
	}
	if ( pTT->getId().compare(m_strId) != 0 )
	{
		return false;
	}
	m_pTransitionTable = pTT;
	return true;
}

void StateMachine::enableMutex(bool bEnableMutex)
{
	m_bHasMutex = bEnableMutex;
}

void StateMachine::printTransitionTable(void)
{
	if ( m_pTransitionTable == 0 )
	{
		cout << "SM " << m_strId << " error: transition table not set" << endl;
		return;
	}
	cout << "SM " << m_strId << endl;
	m_pTransitionTable->print();
}

void StateMachine::print(void)
{
	/* ****************************************************************************************************************
	 *  Print out this state machine
	 * ****************************************************************************************************************
	 */
	cout << "SM " << m_strId << " ActualState = " << m_liActualState << endl;

	/* ****************************************************************************************************************
	 *  Print out the children
	 * ****************************************************************************************************************
	 */
	if ( m_children.size() > 0 )
	{
		int liCnt = 1;
		for ( auto it = m_children.begin(); it != m_children.end(); ++it )
		{
			StateMachine* p = *it;
			cout << "CHILD n. " << liCnt << " OF " << m_strId << ": ";
			p->print();
			liCnt++;
		}
	}
}

int StateMachine::check(StateMachine* pTarget)
{

	if ( m_pTransitionTable == 0 )
	{
		//cout << "CheckAndSet: transition table not define for this SM " << m_strId << endl;
		return TRANSITION_TABLE_UNDEFINED;
	}

	if ( pTarget == 0 )
	{
		//cout << "CheckAndSet: target state machine undefined (null pointer)" << endl;
		return TARGET_STATE_MACHINE_UNDEFINED;
	}

	// First we try to apply the change to this state machine
	if ( pTarget->getId().compare(m_strId) != 0 )
	{
		//cout << "CheckAndSet: target id " << pTarget->getId() << "unknown" << endl;
		return TARGET_STATE_MACHINE_ID_UNKNOWN;
	}


	int liRetValue = m_pTransitionTable->isAllowed(m_liActualState, pTarget->getState());
	if ( liRetValue == TRANSITION_ALLOWED )
	{
		// If we get here the father allowed the transition, now we go through the children leaves
		vector<StateMachine*>* pTargetChildren = pTarget->getChildren();
		if ( pTargetChildren->size() > 0 )
		{
			for ( auto it = pTargetChildren->begin(); it != pTargetChildren->end(); ++it )
			{
				StateMachine* pTargetChild = *it;
				for ( auto itt = m_children.begin(); itt != m_children.end(); ++itt )
				{
					StateMachine* pThisChild = *itt;
					if ( pThisChild->getId().compare(pTargetChild->getId()) == 0 )
					{
						liRetValue = pThisChild->check(pTargetChild);
						if ( liRetValue == TRANSITION_NOT_ALLOWED )
						{
							return liRetValue;
						}
					}
				}
			}
		}
	}
//	else
//	{
//		cout << "CheckAndSet: SM " << m_strId << " not allowed the specified transition" << endl;
//	}

	return liRetValue;
}

void StateMachine::set(StateMachine* pTarget)
{
	m_liActualState = pTarget->getState();
	vector<StateMachine*>* pTargetChildren = pTarget->getChildren();
	if ( pTargetChildren->size() > 0 )
	{
		for ( auto it = pTargetChildren->begin(); it != pTargetChildren->end(); ++it )
		{
			StateMachine* pTargetChild = *it;
			for ( auto itt = m_children.begin(); itt != m_children.end(); ++itt )
			{
				StateMachine* pThisChild = *itt;
				if ( pThisChild->getId().compare(pTargetChild->getId()) == 0 )
				{
					pThisChild->setState(pTargetChild->getState());
				}
			}
		}
	}
}

int StateMachine::checkAndSet(StateMachine* pTarget)
{
	if ( m_bHasMutex ) m_mutex.lock();
	int liRetValue = check(pTarget);
	if ( liRetValue == TRANSITION_ALLOWED ) set(pTarget);
	if ( m_bHasMutex ) m_mutex.unlock();
	return liRetValue;
}
