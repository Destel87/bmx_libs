QT -= core gui
TARGET = StateMachine
TEMPLATE = lib
CONFIG += staticlib

#internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a

HEADERS +=	\
			StateMachineInclude.h \
			StateMachine.h \
			TransitionTableEntry.h \
			TransitionTable.h

SOURCES +=	StateMachine.cpp \
			TransitionTableEntry.cpp \
			TransitionTable.cpp

