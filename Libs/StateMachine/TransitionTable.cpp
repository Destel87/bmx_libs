//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TransitionTable.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the TransitionTable class.
//! @details
//!
//*!****************************************************************************

#include <iostream>
#include "TransitionTable.h"
#include "TransitionTableEntry.h"
#include "StateMachineInclude.h"

TransitionTable::TransitionTable(string strId)
{
	m_strId.assign(strId);
}

TransitionTable::~TransitionTable()
{
	for ( auto it = m_rows.begin(); it != m_rows.end(); ++it )
	{
		//cout << "Destructor of TT " << m_strId << " -> delete TTentry " <<  (*it)->getId() << endl;
		delete(*it);
	}
	for ( auto it = m_children.begin(); it != m_children.end(); ++it )
	{
		//cout << "Destructor of TT " << m_strId << " -> delete child TT " << (*it)->getId() << endl;
		delete(*it);
	}
}

string TransitionTable::getId(void)
{
	return m_strId;
}

int TransitionTable::isAllowed(int liActualState, int liTargetState)
{
	int liRetValue = TRANSITION_NOT_ALLOWED;
	for ( auto it = m_rows.begin(); it != m_rows.end(); ++it )
	{
		TransitionTableEntry* p = *it;
		if ( ( p->getStartState() == liActualState ) && ( p->getTargetState() == liTargetState ) )
		{
			liRetValue = TRANSITION_ALLOWED;
			break;
		}
	}
	return liRetValue;
}

void TransitionTable::addRow(TransitionTableEntry* pRow)
{
	m_rows.push_back(pRow);
}

void TransitionTable::addChild(TransitionTable* pChild)
{
	m_children.push_back(pChild);
}

void TransitionTable::print(void)
{

	/* ****************************************************************************************************************
	 *  Prints out this transition table
	 * ****************************************************************************************************************
	 */
	cout << "TT " << m_strId << endl;
	cout << "ROW\tSTART\tTARGET" << endl;
	for (auto it = m_rows.begin(); it != m_rows.end(); ++it)
	{
		TransitionTableEntry* p = *it;
		cout << p->getId() << "\t" << p->getStartState() << "\t" << p->getTargetState() << "\t";
		vector<TransitionTableEntry*>* pChildren = p->getChildrenPreconditions();
		if ( pChildren->size() != 0 )
		{
			cout << "Preconditions = { ";
			for (auto itt = pChildren->begin(); itt != pChildren->end(); ++itt)
			{
				TransitionTableEntry* p = *itt;
				cout << p->getId() << " ";
			}
			cout << "}";
		}
		cout <<endl;
	}
	cout << endl;

	/* ****************************************************************************************************************
	 *  Prints out the children
	 * ****************************************************************************************************************
	 */
	if ( m_children.size() > 0 )
	{
		int liCnt = 1;
		for (auto it = m_children.begin(); it != m_children.end(); ++it)
		{
			TransitionTable* p = *it;
			cout << "CHILD n. " << liCnt << " OF " << m_strId << endl;
			p->print();
			liCnt++;
		}
	}
}
