//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TransitionTableEntry.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the TransitionTableEntry class.
//! @details
//!
//*!****************************************************************************

#ifndef TRANSITIONTABLEENTRY_H
#define TRANSITIONTABLEENTRY_H

#include <string>
#include <vector>

using namespace std;

class TransitionTableEntry
{

	public:

		TransitionTableEntry();
		virtual ~TransitionTableEntry();
		void setId(string strId);
		string getId(void);
		void setStartState(int liStartState);
		int getStartState(void);
		void setTargetState(int liTargetState);
		int getTargetState(void);
		void setActionFunction(bool(*pActionFunc)(void*));
		void addPreconditon(TransitionTableEntry* pChildPrecondition);
		vector<TransitionTableEntry*>* getChildrenPreconditions(void);
		void printPreconditions(void);

    private:

        string m_strId;
        int m_liStartState;
        int m_liTargetState;
        bool (*m_pActionFunc)(void*);
        vector<TransitionTableEntry*> m_childrenPreconditions;

};

#endif // TRANSITIONTABLEENTRY_H
