//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TransitionTableEntry.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the TransitionTableEntry class.
//! @details
//!
//*!****************************************************************************

#include "TransitionTableEntry.h"
#include "StateMachineInclude.h"
#include <iostream>

TransitionTableEntry::TransitionTableEntry()
{
	m_strId.clear();
	m_liStartState = STATE_UNDEFINED;
	m_liTargetState = STATE_UNDEFINED;
	m_pActionFunc = 0;
	m_childrenPreconditions.clear();
}

TransitionTableEntry::~TransitionTableEntry()
{

}

void TransitionTableEntry::setId(string strId)
{
	m_strId.assign(strId);
}

string TransitionTableEntry::getId(void)
{
	return m_strId;
}

void TransitionTableEntry::setStartState(int liStartState)
{
	m_liStartState = liStartState;
}

int TransitionTableEntry::getStartState(void)
{
	return m_liStartState;
}

void TransitionTableEntry::setTargetState(int liTargetState)
{
	m_liTargetState = liTargetState;
}

int TransitionTableEntry::getTargetState(void)
{
	return m_liTargetState;
}

void TransitionTableEntry::setActionFunction(bool (*pActionFunc)(void*))
{
	m_pActionFunc = pActionFunc;
}

void TransitionTableEntry::addPreconditon(TransitionTableEntry* pChildPrecondition)
{
	m_childrenPreconditions.push_back(pChildPrecondition);
}

vector<TransitionTableEntry*>* TransitionTableEntry::getChildrenPreconditions(void)
{
	return &m_childrenPreconditions;
}

void TransitionTableEntry::printPreconditions(void)
{
	cout << "TransitionTableEntry ID = " << m_strId << " Preconditions = { ";
	for (auto it = m_childrenPreconditions.begin(); it != m_childrenPreconditions.end(); ++it)
	{
		TransitionTableEntry* p = *it;
		cout << p->getId() << " ";
	}
	cout << "}" << endl;
}
