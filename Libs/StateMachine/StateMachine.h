//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    StateMachine.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the StateMachine class.
//! @details
//!
//*!****************************************************************************

#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <string>
#include <vector>
#include "StateMachineInclude.h"
#include "Mutex.h"

using namespace std;

class TransitionTable;

class StateMachine
{

	public:	

		StateMachine(string strId, int liInitialStateValue, bool bHasMutex = false);
		virtual ~StateMachine();
		string getId(void);
		void setId(const string& strId);
		int getState() const;
		void setState(int liActualState);
		vector<StateMachine*>* getChildren(void);
		void addChild(StateMachine* pStateMachine);
		bool setTransitionTable(TransitionTable* pTT);
		void enableMutex(bool bEnableMutex);
		void printTransitionTable(void);
		void print(void);
		int checkAndSet(StateMachine* pTarget);

    private:

        int check(StateMachine* pTarget);
        void set(StateMachine* pTarget);

    private:

        string m_strId;
        int m_liActualState;
        vector<StateMachine*> m_children;
        TransitionTable* m_pTransitionTable;
        Mutex m_mutex;
        bool m_bHasMutex;

};

#endif // STATEMACHINE_H
