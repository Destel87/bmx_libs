//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TimeStampInclude.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the defines for the TimeStampInclude class.
//! @details
//!
//*!****************************************************************************

#ifndef TIMESTAMPINCLUDE_H
#define TIMESTAMPINCLUDE_H

#define SEC_PER_MIN		60ULL
#define MSEC_PER_SEC	1000ULL
#define USEC_PER_MSEC	1000ULL
#define NSEC_PER_USEC	1000ULL
#define NSEC_PER_MSEC	1000000ULL
#define USEC_PER_SEC	1000000ULL
#define NSEC_PER_SEC	1000000000ULL
#define FSEC_PER_SEC	1000000000000000ULL

#endif // TIMESTAMPINCLUDE_H
