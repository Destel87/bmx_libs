//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TimeWait.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the TimeWait class.
//! @details
//!
//*!****************************************************************************

#include <string>

#include "TimeWait.h"

/*! ***********************************************************************************************
 * Local functions
 * ************************************************************************************************
 */
static inline void tsnorm(struct timespec *ts)
{
	while ((unsigned long long)ts->tv_nsec >= NSEC_PER_SEC)
	{
		ts->tv_nsec -= NSEC_PER_SEC;
		ts->tv_sec++;
	}
}

/*! ***********************************************************************************************
 * Class implementation
 * ************************************************************************************************
 */
TimeWait::TimeWait(bool bDebug)
{
	sem_init(&m_sem, 0 ,0);
	m_bDebug = bDebug;
}


signed int TimeWait::waitSeconds(int seconds)
{
	signed int result;

	sem_init(&m_sem, 0 ,0);
	clock_gettime(CLOCK_REALTIME, &m_tv);

	m_tv.tv_sec += seconds;

	result = sem_timedwait(&m_sem, &m_tv);

	if ( m_bDebug && ( result != 0 ) )
		resultInterpreter(result);

	return result;
}

signed int TimeWait::waitMilliSeconds(int milliseconds)
{
	signed int result;
	unsigned long long nsecValue;

	sem_init(&m_sem, 0 ,0);
	clock_gettime(CLOCK_REALTIME, &m_tv);

	nsecValue = ( milliseconds % MSEC_PER_SEC ) * NSEC_PER_MSEC;

	// Avoid wrap around
	if((m_tv.tv_nsec+nsecValue)<NSEC_PER_SEC)
	{
		m_tv.tv_sec += milliseconds/MSEC_PER_SEC;
		m_tv.tv_nsec += nsecValue;
	}
	else
	{
		m_tv.tv_sec += (milliseconds/MSEC_PER_SEC +1);
		m_tv.tv_nsec += nsecValue;
		m_tv.tv_nsec %= NSEC_PER_SEC;
	}

	result = sem_timedwait(&m_sem, &m_tv);

	if ( m_bDebug && ( result != 0 ) )
		resultInterpreter(result);

	return result;
}

/*
//signed int TimeWait::waitMicroSeconds(int microseconds)
unsigned long long TimeWait::waitMicroSeconds(int microseconds)
{
	//signed int result;
	unsigned long long result;
	unsigned long long int nsecValue;
	struct timespec tv;

	unsigned long long time0;
	unsigned long long  time1;
	unsigned long long  time2;

	sem_init(&m_sem, 0 ,0);
	clock_gettime(CLOCK_REALTIME, &m_tv);

	time0 = m_tv.tv_sec*1000000000;
	time0+=m_tv.tv_nsec;

	nsecValue = (microseconds % MICROSEC_IN_SEC)*NANOSEC_IN_MICROSEC;

	// Avoid wrap around
	if((m_tv.tv_nsec+nsecValue)<NANOSEC_IN_SEC)
	{
		m_tv.tv_sec += microseconds/MICROSEC_IN_SEC;
		m_tv.tv_nsec += nsecValue;
	}
	else
	{
		m_tv.tv_sec += (microseconds/MICROSEC_IN_SEC +1);
		m_tv.tv_nsec += nsecValue;
		m_tv.tv_nsec %= NANOSEC_IN_SEC;
	}

	clock_gettime(CLOCK_REALTIME, &tv);
	time1 =tv.tv_sec*1000000000;
	time1+=tv.tv_nsec;

	// It is the time needed to evaluate the duration of operation+clock_gettime
	//time2 = time1-time0;


	result = sem_timedwait(&m_sem, &m_tv);

	// To evaluate the duration of sem_timedwait
	clock_gettime(CLOCK_REALTIME, &m_tv);

	time0 = m_tv.tv_sec*1000000000;
	time0+=m_tv.tv_nsec;
	//printf("Difference 1 %llu\n", time2 );
	//printf("Difference 2 %llu\n", time0-time1 );

	result = time0-time1;

	printf("%u\n", result);
	//if (result!=0)
	//    resultInterpretator(result);

	return result;
}
*/
signed int TimeWait::waitMicroSeconds(int microseconds)
{
	signed int result;
	unsigned long long int nsecValue;

	sem_init(&m_sem, 0 ,0);
	clock_gettime(CLOCK_REALTIME, &m_tv);

	nsecValue = (microseconds % USEC_PER_SEC) * NSEC_PER_USEC;

	// Avoid wrap around
	if((m_tv.tv_nsec+nsecValue)<NSEC_PER_SEC)
	{
		m_tv.tv_sec += microseconds / USEC_PER_SEC;
		m_tv.tv_nsec += nsecValue;
	}
	else
	{
		m_tv.tv_sec += ( microseconds / USEC_PER_SEC + 1 );
		m_tv.tv_nsec += nsecValue;
		m_tv.tv_nsec %= NSEC_PER_SEC;
	}
	result = sem_timedwait(&m_sem, &m_tv);

	if ( m_bDebug && ( result != 0 ) )
		resultInterpreter(result);

	return result;
}


void TimeWait::resultInterpreter(signed int waitResult)
{
	if ( waitResult == (-1) )
	{
		if ( errno == ETIMEDOUT )
			printf("sem_timedwait() timed out\n");  // the timeout has already expired by
		// the time of the call, and the semaphore
		// could not be locked immediately. It's regular.
		else
			perror("sem_timedwait");
	}
	else
		printf("sem_timedwait() succeeded\n");
}

signed int TimeWait::waitNanoSecondsClock(unsigned long long int nNanoSeconds)
{

	// Save actual time
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	// Calculate time to wait in nanoseconds
	struct timespec next;
	next = now;
	int nSecToAdd = nNanoSeconds / NSEC_PER_SEC;
	int nNanoSecToAdd = nNanoSeconds % NSEC_PER_SEC;
	next.tv_sec += nSecToAdd;
	next.tv_nsec += nNanoSecToAdd;
	tsnorm(&next);

	// Sleep for the remaining time using a monotonic clock
	return clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &next, NULL);

}

signed int TimeWait::waitMicroSecondsClock(unsigned long long int nMicroSeconds)
{
	return(waitNanoSecondsClock(nMicroSeconds * NSEC_PER_USEC));
}

signed int TimeWait::waitMilliSecondsClock(unsigned long long int nMilliSeconds)
{
	return(waitNanoSecondsClock(nMilliSeconds * NSEC_PER_MSEC));
}

signed int TimeWait::waitSecondsClock(unsigned long long int nSeconds)
{
	return(waitNanoSecondsClock(nSeconds * NSEC_PER_SEC));
}
