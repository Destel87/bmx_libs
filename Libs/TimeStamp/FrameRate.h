//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    FrameRate.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the FrameRate class.
//! @details
//!
//*!****************************************************************************

#ifndef FRAMERATE_H
#define FRAMERATE_H

#include "TimeStamp.h"

class FrameRate
{

	public:

		FrameRate();
		FrameRate(float fTargetFramerate);
		virtual ~FrameRate();
		void init(float fTargetFramerate);
		void setStartOfCycle(void);
		float getRealFps(void);
		void setStartOfFrame(void);
		void waitEndOfFrame(void);

    private:

        unsigned int m_uTargetFrameMillisec;	// Duration of a frame in milliseconds
        TimeStamp m_startTS;					// Timestamp of the previous frame
        TimeStamp m_endTS;						// Timestamp of the current frame
        unsigned int m_uTotalFramesDuration;	// Duration of all the frames in milliseconds
        unsigned int m_nNumFrames;				// Total number of frames passed

};

#endif // FRAMERATE_H
