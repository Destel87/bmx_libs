QT			-= core gui
TARGET		= TimeStamp
TEMPLATE	= lib
CONFIG		+= staticlib

QMAKE_CXXFLAGS += -std=c++0x -pthread
QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3
QMAKE_CXXFLAGS += -O0

LIBS += -pthread

unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a

HEADERS += \
	TimeStamp.h \
	FrameRate.h \
	TimeMeter.h \
    TimeWait.h \
    TimeStampInclude.h

SOURCES += \
	TimeStamp.cpp \
	FrameRate.cpp \
	TimeMeter.cpp \
    TimeWait.cpp
