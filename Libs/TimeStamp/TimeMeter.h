//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TimeMeter.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the TimeMeter class.
//! @details
//!
//*!****************************************************************************

#ifndef TIMEMETER_H
#define TIMEMETER_H

#include <time.h>
#include <stdint.h>

#include "Mutex.h"
#include "TimeStampInclude.h"


class TimeMeter
{

	public:

		TimeMeter();
		virtual ~TimeMeter();
		void start(void);
		void stop(void);
		void reset(void);
		void setStart(struct timespec* startTS);
		void setEnd(struct timespec* endTS);
		struct timespec getStart(void) const;
		struct timespec getEnd(void) const;
		uint64_t getMin(void) const;
		uint64_t getMax(void) const;
		uint64_t getDurationNanoSec(void) const;

	private:

		void calculateDifference(void);

    private:

        struct timespec m_startTS;
        struct timespec m_endTS;
        uint64_t m_nDiff;
        uint64_t m_nTMin;
        uint64_t m_nTMax;
        Mutex m_Mutex;

};

#endif // TIMEMETER_H
