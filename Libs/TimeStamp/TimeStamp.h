//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TimeStamp.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the TimeStamp class.
//! @details
//!
//*!****************************************************************************

#ifndef __TIMESTAMP_H__
#define __TIMESTAMP_H__

#include <stdint.h>
#include <string.h>
#include <time.h>
#include "TimeStampInclude.h"


typedef union elem64bit_tag
{
	uint64_t ull64;
	int64_t ll64;
} elem64_bit;

/*! ***********************************************************************************************************
 * @brief getFormattedTimeStamp Retrieves the formatted timestamp from a read of gettimeofday system function
 * @param strOutTimeStamp	out vector timestamp
 * ***********************************************************************************************************
 */
void getFormattedTimeStamp(char *strOutTimeStamp);

class TimeStamp
{

	public:

		/*! ***********************************************************************************************************
		 * @brief				void constructor
		 * ************************************************************************************************************
		 */
		TimeStamp();

		/*! ***********************************************************************************************************
         * @brief	Builds a timestamp from an unsigned long long number that contains nano-seconds
         * @param	ullNS value with whom m_nTS will be initialized
		 * ************************************************************************************************************
		 */
        TimeStamp(unsigned long long ullNS);

		/*! ***********************************************************************************************************
         * @brief	Builds a timestamp from a timespec struct that shall be already initialized
         * @param	pTimespec pointer to a timespec struct from wich the timestamp shall be built
		 * ************************************************************************************************************
		 */
        TimeStamp(struct timespec* pTimespec);

        /*! ***********************************************************************************************************
         * @brief				Copy constructor
         * ************************************************************************************************************
         */
        TimeStamp(const TimeStamp& c);

        /*! ***********************************************************************************************************
		 * @brief				Default destructor
		 * ************************************************************************************************************
		 */
		virtual ~TimeStamp();

		/*! ***********************************************************************************************************
		 * @brief	operator +	Add to this timestamp the timestamp passed as parameter and returns the result
		 * @param	b			reference to timestamp to add
		 * @return				the result from the addition
		 * ************************************************************************************************************
		 */
		TimeStamp operator + (const TimeStamp& b);

		/*! ***********************************************************************************************************
		 * @brief	operator +=	Add to this timestamp the timestamp passed as parameter and returns this
		 * @param	b			reference to timestamp to add
		 * @return				a reference to this TimeStamp after addition
		 * ************************************************************************************************************
		 */
		TimeStamp& operator += (const TimeStamp& b);

		/*! ***********************************************************************************************************
		 * @brief	operator -	Subtracts to this timestamp the timestamp passed as parameter and returns the result
		 * @param	b			reference to timestamp to subtract
		 * @return				the result from the addition
		 * ************************************************************************************************************
		 */
		TimeStamp operator - (const TimeStamp& b);

		/*! ***********************************************************************************************************
		 * @brief	operator -=	Subtract to this timestamp the timestamp passed as parameter and returns this
		 * @param	b			reference to timestamp to subtract
		 * @return				a reference to this TimeStamp after subtraction
		 * ************************************************************************************************************
		 */
		TimeStamp& operator -= (const TimeStamp& b);

		/*! ***********************************************************************************************************
		 * @brief	save		Updates the value of this timestamp by calling the clock_gettime system method
		 * ************************************************************************************************************
		 */
		void save(void);

		/*! ***********************************************************************************************************
		 * @brief	get			Returns the value of the timestamp
		 * @return				the value of the timestamp
		 * ************************************************************************************************************
		 */
		unsigned long long get(void);

		/*! ***********************************************************************************************************
		 * @brief	get			Returns the value of the timestamp
		 * @return				the value of the timestamp
		 * ************************************************************************************************************
		 */
		 long long getSigned(void);

		/*! ***********************************************************************************************************
		 * @brief	set			Sets the value of the timestamp using the number passed as parameter
		 * @param	ullNanoSecs	value used to set the timestamp in nanoseconds
		 * ************************************************************************************************************
		 */
		void set(unsigned long long ullNanoSecs);

		/*! ***********************************************************************************************************
         * Update the value of the timestamp with the timespec ----timeval---- passed as parameter
		 * @param		pTimespec	pointer to a timespec struct from wich the timestamp shall be updated
		 * @return		true		upon success (pTimespec shall be a valid pointer), false otherwise
		 * ************************************************************************************************************
		 */
        bool timespecToTS(struct timespec* pTimespec);

		/*! ***********************************************************************************************************
		 * Converts the timestamp in a timespec structure filling the members of the timespec passed as parameter
		 * @param[out]	pTimespec	pointer to a timespec struct to be filled with the timestamp value
		 * @return					true upon success (pTimespec shall be a valid pointer), false otherwise
		 * ************************************************************************************************************
		 */
        bool tsToTimespec(struct timespec* pTimespec);

		/*! ***********************************************************************************************************
		 * Retrieves the seconds value from the timestamp
		 * @return	value of the seconds field in the timestamp
		 * ************************************************************************************************************
		 */
		unsigned int getSecsFromTimestamp(void);

		/*! ***********************************************************************************************************
		 * Retrieves the microseconds value from the timestamp
		 * @return	value of the microseconds field in the timestamp
		 * ************************************************************************************************************
		 */
		unsigned int getMicroSecsFromTimestamp(void);

		/*! ***********************************************************************************************************
		 * Retrieves the milliseconds value from the timestamp
		 * @return	value of the milliseconds field in the timestamp
		 * ************************************************************************************************************
		 */
		unsigned int getMilliSecsFromTimestamp(void);

        /*! ***********************************************************************************************************
         * Retrieves the nanoseconds value from the timestamp - all the ns
         * @return	value of the nanoseconds field in the timestamp
         * ************************************************************************************************************
         */
        unsigned int getNanoSecsFromTimestamp(void);

		/*! ***********************************************************************************************************
		 * Converts the timestamp in a time_t type and returns it
		 * @return	value of time_t that represents the conversion of the timestamp
		 * ************************************************************************************************************
		 */
		time_t getTimeFromTimestamp(void);

		/*! ***********************************************************************************************************
		 * Formats the timestamp in a datestring of type Daaaammdd_Hhhmmss_Mnnn and returns it
         * @param pOutTimeStampString [out]	value of time_t that represents the conversion of the timestamp
		 * ************************************************************************************************************
		 */
		void getDateStringFromTimestamp(char* pOutTimeStampString);

		/*! ***********************************************************************************************************
		 * Converts a date string of tipe Daaaammdd_Hhhmmss_Mxxx or Daaaammdd_Hhhmmss (without milliseconds)
		 * in TimeStamp
		 * @param	pStringaTimestamp the input date string
		 * @return	the TimeStamp extracted from the string (0 if string is not valid)
		 * ************************************************************************************************************
		 */
		static TimeStamp getTimestampFromDateString(const char* pStringaTimestamp);

		/*! ***********************************************************************************************************
		 * Searches ro a timestamp in the header of a jpeg image and if it is found returns it
		 * @param	imgbuffer the jpeg frame buffer
		 * @param	buffer_size the size of the jpeg frame buffer
		 * @return	the TimeStamp found (0 if not found, or not valid)
		 * ************************************************************************************************************
		 */
		static TimeStamp getTimestampFromJpeg(const unsigned char* imgbuffer, unsigned int buffer_size);

    private:

        elem64_bit m_ullNanoSeconds;	// a timestamp is an unsigned long long that contains nanoseconds

};

#endif // __TIMESTAMP_H__
