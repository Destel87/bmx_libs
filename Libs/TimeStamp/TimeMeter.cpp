//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TimeMeter.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the TimeMeter class.
//! @details
//!
//*!****************************************************************************

#include <math.h>
#include "TimeMeter.h"

TimeMeter::TimeMeter()
{
	reset();
}

TimeMeter::~TimeMeter()
{

}

void TimeMeter::start(void)
{
	clock_gettime(CLOCK_MONOTONIC, &m_startTS);
	m_Mutex.lock();
	m_endTS = m_startTS;
	m_Mutex.unlock();
}

void TimeMeter::stop(void)
{
	clock_gettime(CLOCK_MONOTONIC, &m_endTS);
	m_Mutex.lock();
	calculateDifference();
	m_nTMin = fmin(m_nTMin, m_nDiff);
	m_nTMax = fmax(m_nTMax, m_nDiff);
	m_Mutex.unlock();
}

void TimeMeter::reset(void)
{
	m_Mutex.lock();
	m_startTS.tv_sec = 0;
	m_startTS.tv_nsec = 0;
	m_endTS.tv_sec = 0;
	m_endTS.tv_nsec = 0;
	m_nDiff = 0;
	m_nTMin = UINT64_MAX;
	m_nTMax = 0;
	m_Mutex.unlock();
}

void TimeMeter::setStart(struct timespec* startTS)
{
	m_Mutex.lock();
	m_startTS.tv_sec = startTS->tv_sec;
	m_startTS.tv_nsec = startTS->tv_nsec;
	m_Mutex.unlock();
}

struct timespec TimeMeter::getStart(void) const
{
	return m_startTS;
}

void TimeMeter::setEnd(struct timespec* endTS)
{
	m_Mutex.lock();
	m_endTS.tv_sec = endTS->tv_sec;
	m_endTS.tv_nsec = endTS->tv_nsec;
	calculateDifference();
	m_nTMin = fmin(m_nTMin, m_nDiff);
	m_nTMax = fmax(m_nTMax, m_nDiff);
	m_Mutex.unlock();
}

struct timespec TimeMeter::getEnd(void) const
{
	return m_endTS;
}

uint64_t TimeMeter::getDurationNanoSec(void) const
{
	return m_nDiff;
}

uint64_t TimeMeter::getMin(void) const
{
	return m_nTMin;
}

uint64_t TimeMeter::getMax(void) const
{
	return m_nTMax;
}


void TimeMeter::calculateDifference(void)
{
	uint64_t nSecDiff = m_endTS.tv_sec - m_startTS.tv_sec;
	uint64_t nNanoSecDiff = m_endTS.tv_nsec - m_startTS.tv_nsec;
	m_nDiff = (nSecDiff * NSEC_PER_SEC) + nNanoSecDiff;
}
