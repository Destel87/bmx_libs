//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TimeStamp.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the TimeStamp class.
//! @details
//!
//*!****************************************************************************

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stdlib.h>
#include <linux/types.h>
#include <sys/time.h>

#include "TimeStamp.h"


void getFormattedTimeStamp(char *strOutTimeStamp)
{
	struct timeval curr_time;
	time_t curr_sec;
	int curr_msec;
	char time_msg[32];

	gettimeofday(&curr_time, NULL);
	curr_sec = curr_time.tv_sec;
	curr_msec = curr_time.tv_usec / 1000;
	sprintf(time_msg, "%.19s.%03d", ctime(&curr_sec), curr_msec);

	memcpy(strOutTimeStamp, &time_msg, sizeof(time_msg));
}



TimeStamp::TimeStamp()
{
	struct timespec tv;
	clock_gettime(CLOCK_REALTIME, &tv);
	m_ullNanoSeconds.ull64 = ( (unsigned long long)tv.tv_sec * NSEC_PER_SEC ) + tv.tv_nsec;
}


TimeStamp::TimeStamp(unsigned long long ullNS)
{
	m_ullNanoSeconds.ull64 = ullNS;
}

TimeStamp::TimeStamp(struct timespec* pTimespec)
{
	m_ullNanoSeconds.ull64 = ( (unsigned long long)pTimespec->tv_sec * NSEC_PER_SEC ) + pTimespec->tv_nsec;
}

TimeStamp::TimeStamp(const TimeStamp& c)
{
	m_ullNanoSeconds = c.m_ullNanoSeconds;
}

TimeStamp::~TimeStamp()
{
    // Nothing to be done here yet...
}

TimeStamp TimeStamp::operator + (const TimeStamp& b)
{
	TimeStamp ts;
	ts = m_ullNanoSeconds.ull64 + b.m_ullNanoSeconds.ull64;
	return ts;
}


TimeStamp& TimeStamp::operator += (const TimeStamp& b)
{
	m_ullNanoSeconds.ull64 += b.m_ullNanoSeconds.ull64;
	return *this;
}

TimeStamp TimeStamp::operator - (const TimeStamp& b)
{
	TimeStamp ts;
	ts = m_ullNanoSeconds.ull64 - b.m_ullNanoSeconds.ull64;
	return ts;
}

TimeStamp& TimeStamp::operator-=(const TimeStamp& b)
{
	m_ullNanoSeconds.ull64 -= b.m_ullNanoSeconds.ull64;
	return *this;
}

void TimeStamp::save(void)
{
	struct timespec tv;
	clock_gettime(CLOCK_REALTIME, &tv);
	m_ullNanoSeconds.ull64 = ((unsigned long long)tv.tv_sec * NSEC_PER_SEC) + tv.tv_nsec;
}

unsigned long long TimeStamp::get(void)
{
	return m_ullNanoSeconds.ull64;
}

long long TimeStamp::getSigned(void)
{
	return m_ullNanoSeconds.ll64;
}

void TimeStamp::set(unsigned long long ullNanoSecs)
{
	m_ullNanoSeconds.ull64 = ullNanoSecs;
}

bool TimeStamp::timespecToTS(struct timespec* pTimespec)
{
	if ( pTimespec == NULL )
		return false;
	m_ullNanoSeconds.ull64 = ((unsigned long long)pTimespec->tv_sec * NSEC_PER_SEC) + (unsigned long long)pTimespec->tv_nsec;
	return true;
}

bool TimeStamp::tsToTimespec(struct timespec* pTimespec)
{
	if ( pTimespec == NULL )
		return false;
	pTimespec->tv_sec =  (int32_t)( m_ullNanoSeconds.ull64 / NSEC_PER_SEC );
	pTimespec->tv_nsec = (int32_t)( m_ullNanoSeconds.ull64 % NSEC_PER_SEC );
	//printf("TS: tutto=%llu sec=%d nsec=%d\n", m_ullNanoSeconds.ull64, (long int)pTimespec->tv_sec, (long int)pTimespec->tv_nsec);
	return true;
}

unsigned int TimeStamp::getSecsFromTimestamp(void)
{
	return (unsigned int)(m_ullNanoSeconds.ull64 / NSEC_PER_SEC);
}

unsigned int TimeStamp::getMilliSecsFromTimestamp(void)
{
	return (unsigned int)( (m_ullNanoSeconds.ull64 / NSEC_PER_MSEC) % 1000 );
}

unsigned int TimeStamp::getMicroSecsFromTimestamp(void)
{
	return (unsigned int)( (m_ullNanoSeconds.ull64 / NSEC_PER_USEC) % 1000 );
}

unsigned int TimeStamp::getNanoSecsFromTimestamp(void)
{
	return (unsigned int)( m_ullNanoSeconds.ull64 % 1000 );
}

time_t TimeStamp::getTimeFromTimestamp(void)
{
	return (time_t)getSecsFromTimestamp();
}

TimeStamp TimeStamp::getTimestampFromDateString(const char* pStringaTimestamp)
{

	char year[10], month[10], day[10], hour[10], min[10], sec[10], msec[10];
	struct tm tmTimestamp;
	time_t tTimestampSec;
	int nTimestampMsec;

	strcpy(year, "");
	strcpy(month, "");
	strcpy(day, "");
	strcpy(hour, "");
	strcpy(min, "");
	strcpy(sec, "");
	strcpy(msec, "");
	memset(&tmTimestamp, 0, sizeof(struct tm));
	tTimestampSec = 0;
	nTimestampMsec = 0;

	// Accepts strings of type D20070612_H110814_M600 or D20070612_H110814
	TimeStamp nRetValue((unsigned long long)0);
	int len = strlen(pStringaTimestamp);
	if ( (len != 22) && (len != 17) )
	{
		return nRetValue;
	}

	// Builds a string with the hour
	memset(&year[0], 0, 10);
	strncpy(year, pStringaTimestamp + 1, 4);
	year[4] = 0;
	memset(&month[0], 0, 10);
	strncpy(month, pStringaTimestamp + 5, 2);
	month[2] = 0;
	memset(&day[0], 0, 10);
	strncpy(day, pStringaTimestamp + 7, 2);
	day[2] = 0;
	memset(&hour[0], 0, 10);
	strncpy(hour, pStringaTimestamp + 11, 2);
	hour[2] = 0;
	memset(&min[0], 0, 10);
	strncpy(min, pStringaTimestamp + 13, 2);
	min[2] = 0;
	memset(&sec[0], 0, 10);
	strncpy(sec, pStringaTimestamp + 15, 2);
	sec[2] = 0;
	if (len == 22)
	{
		memset(&msec[0], 0, 10);
		strncpy(msec, pStringaTimestamp + 19, 3);
		msec[3] = 0;
	}

	/*int    tm_sec   seconds [0,61]
	int    tm_min   minutes [0,59]
	int    thour  hour [0,23]
	int    tm_mday  day of month [1,31]
	int    tm_mon   month of year [0,11]
	int    tyear  years since 1900
	int    tm_wday  day of week [0,6] (Sunday = 0)
	int    tm_yday  day of year [0,365]
	int    tm_isdst daylight savings flag*/

	tmTimestamp.tm_year = atoi(year)-1900;
	tmTimestamp.tm_mon = atoi(month)-1;
	tmTimestamp.tm_mday = atoi(day);
	tmTimestamp.tm_hour = atoi(hour);
	tmTimestamp.tm_min = atoi(min);
	tmTimestamp.tm_sec = atoi(sec);
	tmTimestamp.tm_isdst = -1;

	tTimestampSec = mktime(&tmTimestamp);
	if ( tTimestampSec == -1)
	{
		// Impossible to build timestamp
		return nRetValue;
	}

	nTimestampMsec = 0;
	if (len == 22)
	{
		if (strcmp(msec, "000") != 0)
		{
			nTimestampMsec = atoi(msec);
			if ( nTimestampMsec < 0)
			{
				// Impossible to build timestamp
				return nRetValue;
			}
		}
	}

	// Everything ok
	unsigned long long n = 0;
	n = ((unsigned long long)tTimestampSec * NSEC_PER_SEC) +
		((unsigned long long)nTimestampMsec * NSEC_PER_MSEC);
	nRetValue.set(n);
	return nRetValue;

}

void TimeStamp::getDateStringFromTimestamp(char* pOutTimeStampString)
{
	// Retrieves seconds and milliseconds
	time_t ts_sec = (time_t)getSecsFromTimestamp();

	//int ts_millisec = getMilliSecsFromTimestamp(t % TICKS_PER_SEC );
	int ts_millisec = (int)getMilliSecsFromTimestamp();

	// Converts seconds in tm structue
	struct tm Tm;
	localtime_r(&ts_sec, &Tm);

	//Builds the ret string Daaaammdd_Hhhmmss_Mxxx)
	sprintf(pOutTimeStampString, "D%04d%02d%02d_H%02d%02d%02d_M%03d",
			Tm.tm_year + 1900,
			Tm.tm_mon + 1,
			Tm.tm_mday,
			Tm.tm_hour,
			Tm.tm_min,
			Tm.tm_sec,
			ts_millisec);
}

TimeStamp TimeStamp::getTimestampFromJpeg(const unsigned char* imgbuffer, unsigned int buffer_size)
{

	TimeStamp nRetValue((unsigned long long)0);
	if ( !imgbuffer )
	{
		return nRetValue;
	}

	if ( buffer_size < 200 )
	{
		return nRetValue;
	}

	// Searches for the field time and converts it in TimeStamp
	// ---------------
	// timestamp (15 byte in the field "JFIF" after the first FE FF)
	// FF FE
	// 00 0F
	// 0A
	// 01
	// 4A 12 6F A4        seconds from EPOC
	// 38                 cents of second
	// 4A 12 6F A4        same fields repeated
	// 38
	// 01                 unuseful

	// Step 1, searches JFIF header
	int i = 0;
	bool bfound=false;
	for (i=0;i<100;i++)
	{
		if (imgbuffer[i]=='J')
		{
			if ((imgbuffer[i+1]=='F')&&
				(imgbuffer[i+2]=='I')&&
				(imgbuffer[i+3]=='F'))
			{
				// found JFIF
				bfound = true;
				//printf ("JFIF Torvato %d\n", i);
				break;
			}
		}
	}

	if ( !bfound )
	{
		return nRetValue;
	}

	unsigned long long n = 0;
	// Searches for the beginning of the timestamp
	while (i < 100)
	{
		if ((imgbuffer[i]==0xFF)&&(imgbuffer[i+1]==0xFE))
		{
			// header found
			//printf ("%X\n", imgbuffer[i+6]);
			n = imgbuffer[i+6];
			n = n << 8;
			n += imgbuffer[i+7];
			n = n << 8;
			n += imgbuffer[i+8];
			n = n << 8;
			n += imgbuffer[i+9];

			n *= NSEC_PER_SEC;
			n +=  imgbuffer[i+10]*10000;
			break;
		}
		i++;
	}

	nRetValue.set(n);
	return nRetValue;

}
