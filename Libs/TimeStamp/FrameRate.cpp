//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    FrameRate.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the FrameRate class.
//! @details
//!
//*!****************************************************************************

#include <stdlib.h>
#include <unistd.h>

#include "FrameRate.h"
#include "TimeStamp.h"


/*! *******************************************************************************************************************
 * FrameRate class implementation
 * ********************************************************************************************************************
 */
FrameRate::FrameRate()
{
	m_uTargetFrameMillisec = 0;
	m_startTS.set(0);
	m_endTS.set(0);
	m_uTotalFramesDuration = 0;
	m_nNumFrames = 0;
}

FrameRate::FrameRate(float fTargetFramerate)
{
	m_uTargetFrameMillisec = (int)(1000 / fTargetFramerate);
	setStartOfCycle();
}

FrameRate::~FrameRate()
{

}

void FrameRate::init(float fTargetFramerate)
{
	m_uTargetFrameMillisec = (int)(1000 / fTargetFramerate);
	setStartOfCycle();
}

void FrameRate::setStartOfCycle(void)
{
	m_startTS.save();
	m_endTS = m_startTS;
	m_uTotalFramesDuration = 0;
	m_nNumFrames = 0;
}

float FrameRate::getRealFps(void)
{
	float total_frame_time_sec;
	// Performs the calculation of the total amount of seconds passed
	total_frame_time_sec = (float)(m_uTotalFramesDuration / 1000);
	return (float)m_nNumFrames / total_frame_time_sec;
}

void FrameRate::setStartOfFrame(void)
{
	m_startTS.save();
}

void FrameRate::waitEndOfFrame(void)
{
	TimeStamp frame_duration;
	unsigned int frame_millisec;
	int wait_millisec;
	// Save the actual timestamp
	m_endTS.save();
	// Calculates the difference between this and the previous one
	frame_duration = m_endTS - m_startTS;
	frame_millisec = frame_duration.getMilliSecsFromTimestamp();
	// Updates the total time
	m_uTotalFramesDuration += frame_millisec;
	// Calculates how long we must wait
	wait_millisec = m_uTargetFrameMillisec - frame_millisec;
	//printf("Target millisec = %d, frame _millisec = %d\n", pFrameRate->frame_target_millisec, frame_millisec);
	//printf("Wait %d millisec\n", wait_millisec);
	if (wait_millisec > 0)
	{
		usleep(wait_millisec * 1000);
		m_uTotalFramesDuration += wait_millisec;
	}
	// Increases the frame counter
	m_nNumFrames++;
	// Check for an overflow
	if (m_uTotalFramesDuration > 0xefffffff)
	{
		m_uTotalFramesDuration = 0;
		m_nNumFrames = 0;
	}
	// Sets the beginning of the next frame
	m_startTS.save();
}
