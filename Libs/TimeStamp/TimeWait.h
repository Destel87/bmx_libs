//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TimeWait.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the TimeWait class.
//! @details
//!
//*!****************************************************************************

#ifndef TIMEWAIT_H
#define TIMEWAIT_H

#include <time.h>
#include <semaphore.h>

#include "TimeStampInclude.h"

class TimeWait
{
	public:

		/*! ************************************************************************
		 * @brief Default constructor
		 ** ***********************************************************************/
		TimeWait(bool bDebug=false);

		/*! ************************************************************************
		 * @brief  Wait on a semaphore the time (in milliseconds) wanted
		 * @param  milliseconds: time to wait in milliseconds
		 * @return exit function code
		 ** ***********************************************************************/
		signed int waitMilliSeconds(int milliseconds);

		/*! ************************************************************************
		 * @brief  Wait on a semaphore the time (in seconds) wanted
		 * @param  seconds: time to wait in seconds
		 * @return exit function code
		 ** ************************************************************************/
		signed int waitSeconds(int seconds);

		/*! ************************************************************************
		 * @brief  Wait on a semaphore the time (in microseconds) wanted
		 * @param  microseconds: time to wait in microseconds
		 * @return exit function code
		 ** ************************************************************************/
		signed int waitMicroSeconds(int microseconds);

		/*! ************************************************************************
		 * @brief	Print a string containing an explanation of the result
		 *			obtained by waitSeconds, waitMilliSeconds or waitMicroSeconds
		 * @param	waitResult: result of one of the waiting functions
		 ** ***********************************************************************/
		void resultInterpreter(signed int waitResult);

		signed int waitNanoSecondsClock(unsigned long long nNanoSeconds);
		signed int waitMicroSecondsClock(unsigned long long int nMicroSeconds);
		signed int waitMilliSecondsClock(unsigned long long int nMilliSeconds);
		signed int waitSecondsClock(unsigned long long int nSeconds);

    private:
        struct timespec m_tv;
        sem_t m_sem;
        bool m_bDebug;

};

#endif // TIMEWAIT_H
