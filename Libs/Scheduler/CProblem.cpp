//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CProblem.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the CProblem class.
//! @details
//!
//*!****************************************************************************

#include "CProblem.h"

CProblem::CProblem()
{

}

CProblem::~CProblem()
{

}

bool CProblem::loadWorkPlan(t_WorkPlan& pWorkPlan, t_ErrorData& pErrorData)
{
	return Problem_loadWorkPlan(&m_Problem, &pWorkPlan, &pErrorData);
}

void CProblem::startConfig(int64_t lTimeOffset)
{
	Problem_startConfig(&m_Problem, lTimeOffset);
}


t_Block* CProblem::addBlock(const char* cEventTAG, int iEventTAG,
						 int iSectionID, int iSlotID, int iEventID, int iResourceID,
						 int iActionID, int64_t lLength, bool planned, int64_t plannedTime,
						 t_ErrorData& pErrorData)
{
	t_Block* pRetValue = 0;
	pRetValue = Problem_addBlock(&m_Problem, cEventTAG, iEventTAG, iSectionID, iSlotID, iEventID,
								 iResourceID, iActionID, lLength, planned, plannedTime, &pErrorData);
	return pRetValue;
}

void CProblem::setSlotPriority(int iSectionID, int iSlotID, int iPriority)
{
	Problem_setSlotPriority(&m_Problem, iSectionID, iSlotID, iPriority);
}


t_PrecedenceDef* CProblem::addPrecedence(int iSection, int iSlot, int iEvent,
										 int iPredSection, int iPredSlot, int iPredEvent,
										 int64_t lMinDelay, int64_t lMaxDelay,
										 t_ErrorData& pErrorData)
{
	t_PrecedenceDef* pRetValue = 0;
	pRetValue = Problem_addPrecedence(&m_Problem, iSection, iSlot, iEvent, iPredSection, iPredSlot,
									  iPredEvent, lMinDelay, lMaxDelay, &pErrorData);
	return pRetValue;
}

t_PrecedenceDef* CProblem::addStartingConstraint(int iSection, int iSlot, int iEvent,
												 int64_t lMinDelay, int64_t lMaxDelay,
												 t_ErrorData& pErrorData)
{
	t_PrecedenceDef* pRetValue = 0;
	pRetValue = Problem_addStartingConstraint(&m_Problem, iSection, iSlot, iEvent, lMinDelay, lMaxDelay, &pErrorData);
	return pRetValue;
}

bool CProblem::addScheduledProblem(t_TimeLine& pTimeLine, t_ErrorData& pErrorData)
{
	bool bRetValue = false;
	bRetValue = Problem_addScheduledProblem(&m_Problem, &pTimeLine, &pErrorData);
	return bRetValue;
}

bool CProblem::addScheduledProblemWithOffset(t_TimeLine& pTimeLine, int64_t lOffset, t_ErrorData& pErrorData)
{
	bool bRetValue = false;
	bRetValue = Problem_addScheduledProblemWithOffset(&m_Problem, &pTimeLine, lOffset, &pErrorData);
	return bRetValue;
}

bool CProblem::endConfig(t_ErrorData& pErrorData)
{
	bool bRetValue = false;
	bRetValue = Problem_endConfig(&m_Problem, &pErrorData);
	return bRetValue;
}

bool CProblem::addResourceConflict(int iResourceID1, int iActionID1,
								   int iResourceID2, int iActionID2,
								   t_ErrorData& pErrorData)
{
	bool bRetValue = false;
	bRetValue = Problem_addResourceConflict(&m_Problem, iResourceID1, iActionID1, iResourceID2, iActionID2, &pErrorData);
	return bRetValue;
}

bool CProblem::calcBlockAbsoluteTimes(t_ErrorData& pErrorData)
{
	bool bRetValue = false;
	bRetValue = Problem_calcBlockAbsoluteTimes(&m_Problem, &pErrorData);
	return bRetValue;
}

void CProblem::setStartJitter(int64_t lStartJitter)
{
	Problem_setStartJitter(&m_Problem, lStartJitter);
}

t_ProblemSlot* CProblem::getProblemSlot(int iSectionID, int iSlotID)
{
	t_ProblemSlot* pRetValue = 0;
	pRetValue = Problem_getProblemSlot(&m_Problem, iSectionID, iSlotID);
	return pRetValue;
}

t_Block* CProblem::findBlock(int iSectionID, int iSlotID, int iEventID)
{
	t_Block* pRetValue = 0;
	pRetValue = Problem_findBlock(&m_Problem, iSectionID, iSlotID, iEventID);
	return pRetValue;
}

bool CProblem::testResourceConflict(t_Block& pBlock1, t_Block& pBlock2)
{
	bool bRetValue = false;
	bRetValue = Problem_testResourceConflict(&m_Problem, &pBlock1, &pBlock2);
	return bRetValue;
}

t_Problem* CProblem::getProblem(void)
{
	return &m_Problem;
}

int CProblem::getBlockCount(void)
{
	return m_Problem.iBlockCount;
}

t_Block* CProblem::getBlockByIndex(int iIndex)
{
	if ( ( iIndex >= NBLK ) || ( iIndex < 0 ) )
		return 0;
	t_Block* pRetValue = &m_Problem.blocks[iIndex];
	return pRetValue;
}

