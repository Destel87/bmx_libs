//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SchedulerThread.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the SchedulerThread class.
//! @details
//!
//*!****************************************************************************

#include <string.h>
#include <vector>

#include "SchedulerThread.h"
#include "TimeWait.h"
#include "TimeStamp.h"
#include "CProblemIo.h"

// linked to the enum defined in the schedulerinclude.h file
static std::vector<std::string> _eventsString =
{
	"E_RESET",
	"E_START_PROTO",
	"E_FLUO_READ",
	"E_END_PROTO",
	"E_TRAY_MOVE_BCR",
	"E_RAW_STRIP_READ",
	"E_TOWER_MOVE_SPR",
	"E_RAW_SPR_READ",
	"E_READSEC_SEND",
	"E_SET_RUNNING_STATE",
	"E_SH_MOVE_FOR_READING",
	"E_RESTART_PROTO",
	"E_TOWER_ACCESS_RESTORE",
	"E_TRAY_ACCESS_RESTORE",
	"E_MEASURE_ON_AIR",
	"E_MEASURE_OFF_AIR",
	"E_AUTOCHECK_AIR",
	"E_SH_MOVE_SS_READING",
	"E_SH_READ_SS",
	"E_TOWER_FORCE_SPR",

	"E_START_READ_SEC",
	"E_GET_READY_READ_SEC",
	"E_CHECK_STRIP_PRESENCE",
	"E_DECODE_DM",
	"E_DECODE_BC",
	"E_CHECK_SAMPLE0_PRESENCE",
	"E_CHECK_SAMPLE3_PRESENCE",
	"E_CHECK_CONE_PRESENCE",
	"E_RESCHEDULE_EVENT",
	"E_SET_DEFAULT_READ_SEC",
	"E_SEND_REPLY_READSEC",

	"E_DUMMY_1",
	"E_DUMMY_2",
	"E_NUM_EVENTS"
};

// --------------------------------------------------------------------

SchedulerThread::SchedulerThread()
{
    m_schedulerSem.create(SCHED_SEM_SCHEDULER_NAME, SCHED_SEM_SCHEDULER_INIT_VALUE);
    m_timeLineActiveSem.create(SCHED_SEM_TIMELINE_ACTIVE_NAME, SCHED_SEM_TIMELINE_ACTIVE_INIT_VALUE);
    m_timeLineExecSem.create(SCHED_SEM_TIMELINE_EXEC_NAME, SCHED_SEM_TIMELINE_EXEC_INIT_VALUE);
    m_timeLineSwapSem.create(SCHED_SEM_TIMELINE_SWAP_NAME, SCHED_SEM_TIMELINE_SWAP_INIT_VALUE);

    p_block = 0;
    p_prec = 0;

    memset((void*)&ts, 0, sizeof(ts));

    memset(&currTimeLine, 0, sizeof(currTimeLine));
    memset(&nextTimeLine, 0, sizeof(nextTimeLine));
    memset(&purgeTimeLine, 0, sizeof(purgeTimeLine));

    event_idx = CURRENT_EVENT_INDEX;	// era volatile
    zero_time = 0;
    DeltaScheduler = 0;
    endReSched_time = 0;
    lastStartTime = 0;

    iLastSectionID = 0;
    iLastSlotID = 0;
    iLastEventID = 0;

    memset(&prec, 0, sizeof(prec));
    memset(&precReadSec, 0, sizeof(precReadSec));

    temp_eventcnt = 0;

    memset((void*)&te, 0, sizeof(te));
    memset((void*)SectionInvolvedDeleted, 0, sizeof(SectionInvolvedDeleted));

    for(int i = 0; i < E_NUM_EVENTS; i++)
    {
        m_ptrEventFunction[i] = 0;
    }
}

SchedulerThread::~SchedulerThread()
{
    m_schedulerSem.closeAndUnlink();
    m_timeLineActiveSem.closeAndUnlink();
    m_timeLineExecSem.closeAndUnlink();
    m_timeLineSwapSem.closeAndUnlink();
}

void stepFn(int phase, int64_t timeout, bool solutionFound, bool* interruptionRequested)
{

	(void)solutionFound;

	//TimeStamp now;
	//int64_t illNowMsec = now.getSigned() / NSEC_PER_MSEC;
	//struct timespec sched_time;
	//SchedulerThread tmp;
	if ( timeout > 0 )
	{
		//clock_gettime(CLOCK_REALTIME, &sched_time); // gets the current system time
		TimeStamp now;
		int64_t illNowMsec = now.getSigned() / NSEC_PER_MSEC;
		switch ( phase )
		{
			case NEW_PHASE_STEP:
			{
				//tmp.numCycles = 0;
				SchedulerThread::getInstance()->setSchedulationNumCycles(0);
				//sched_stime=(BMX_LONG)(sched_time.tv_sec * (BMX_LONG)INTERNAL_TIME_FACTOR) +
				//		(BMX_LONG)(sched_time.tv_nsec / (1000000000L / (BMX_LONG)INTERNAL_TIME_FACTOR));
				//tmp.sched_stime = illNowMsec;
				SchedulerThread::getInstance()->setSchedulationStartTime(illNowMsec);
				SchedulerThread::getInstance()->log(LOG_DEBUG_PARANOIC, "SCHED_STEP_FN: START Time=%lld",
													SchedulerThread::getInstance()->getSchedulationStartTime());
			}
			break;
			case EXECUTE_PHASE_STEP:
			{
				//tmp.numCycles++;
				uint32_t ulNumCycles = SchedulerThread::getInstance()->getSchedulationNumCycles();
				ulNumCycles++;
				SchedulerThread::getInstance()->setSchedulationNumCycles(ulNumCycles);
				//sched_ltime=(BMX_LONG)(sched_time.tv_sec * (BMX_LONG)INTERNAL_TIME_FACTOR) +
				//		(BMX_LONG)(sched_time.tv_nsec / (1000000000L / (BMX_LONG)INTERNAL_TIME_FACTOR));
				//tmp.sched_ltime = illNowMsec;
				SchedulerThread::getInstance()->setSchedulationLastTime(illNowMsec);
				if ( ulNumCycles == 1 )
				{
					SchedulerThread::getInstance()->log(LOG_DEBUG_PARANOIC,
														"SCHED_STEP_FN: First Cycle Time:%lld", illNowMsec);
				}
				//tmp.sched_difftime = ( tmp.sched_ltime - tmp.sched_stime );
				int64_t illSchedulationDiffTime = illNowMsec - SchedulerThread::getInstance()->getSchedulationStartTime();
				if ( illSchedulationDiffTime > timeout )
					*interruptionRequested = true;
			}
			break;
			case END_PHASE_STEP:
			{
				//sched_ltime=(BMX_LONG)(sched_time.tv_sec * (BMX_LONG)INTERNAL_TIME_FACTOR) +
				//		(BMX_LONG)(sched_time.tv_nsec / (1000000000L / (BMX_LONG)INTERNAL_TIME_FACTOR));
				//tmp.sched_ltime = illNowMsec;
				SchedulerThread::getInstance()->setSchedulationLastTime(illNowMsec);
				SchedulerThread::getInstance()->log(LOG_DEBUG_PARANOIC,
													"SCHED_STEP_FN: END Time: %lld Cycles:%d",
													illNowMsec,
													SchedulerThread::getInstance()->getSchedulationNumCycles());
			}
			break;
		}
	}
}

bool SchedulerThread::popTimeline(void)
{
    int n = (int)currTimeLine.iEvents;
    if ( n <= 0 )
    {
        // empty FIFO Timeline
        return false;
    }

    for ( int i = 0; i < n - 1; i++ )
    {
        // Move every event one position before
        memcpy( &(currTimeLine.PlannedEvent[i]), &(currTimeLine.PlannedEvent[i+1]), sizeof(t_PlannedEvent) );
    }
    currTimeLine.iEvents--;
    return true;
}

void SchedulerThread::mergeTimeline(void)
{

	log(LOG_DEBUG, "SCHED_MERGE_TL: start");

#if PRINT_MERGED_TIMELINE
	log(LOG_DEBUG, "SCHED_MERGE_TL: merging window: %lld %lld", zero_time, zero_time + (int64_t)T_SCHEDULING);
	log(LOG_DEBUG, "SCHED_MERGE_TL: current timeline start");
	for(unsigned int i = 0; i < currTimeLine.iEvents; i++)
	{
		if ( i == event_idx )
			log(LOG_DEBUG, "SCHED_MERGE_TL: <<<<");
		else
			log(LOG_DEBUG, "SCHED_MERGE_TL: <---");

		log(LOG_DEBUG, "SCHED_MERGE_TL: Event=%d, StartTime=%llu, Tag=%d, %s",
			i,
			currTimeLine.PlannedEvent[i].lPlannedStarTime,
			currTimeLine.PlannedEvent[i].iEventTAG,
			currTimeLine.PlannedEvent[i].cEventTAG);
	}
	log(LOG_DEBUG, "SCHED_MERGE_TL: current timeline end");
#endif

	/*! ***************************************************************************************************************
	 * Check if one or more events fron the "curr" timeline must be merged into the "next" timeline.
	 * Do it if it is the case
	 * ****************************************************************************************************************
	 */
#if PRINT_MERGED_TIMELINE
	log(LOG_DEBUG, "SCHED_MERGE_TL: timelines must be merged");
#endif

	/*! ***************************************************************************************************************
	 * Initially sets both the start and end times for all sections to the "next" values.
	 * It will be amended lately. Sets the "changed" memo to "unchanged", too.
	 * ****************************************************************************************************************
	 */
	bool changed[NST];
	t_TimeLine mergeTimeLine;
	for(int i = 0; i < SCT_NUM_TOT_SECTIONS; i++)
	{
		/*! ***********************************************************************************************************
		 * Sets values for a single section
		 * ************************************************************************************************************
		 */
		mergeTimeLine.lStartSection[i] = nextTimeLine.lStartSection[i];
		mergeTimeLine.lEndSection[i] = nextTimeLine.lEndSection[i];
		changed[i] = false;
	}

	mergeTimeLine.iEvents = 0; // no events initially into the temporary timeline
	event_idx = 0;

	// changed  currTimeLine in nextTimeLine
	TimeStamp now;
	uint64_t ullNowMsec = now.get() / NSEC_PER_MSEC;
	endReSched_time = (int64_t)ullNowMsec;

	log(LOG_DEBUG, "SCHED_MERGE_TL: endReSchedule=%lld", endReSched_time );
	log(LOG_DEBUG, "SCHED_MERGE_TL: lastStartTime=%lld", lastStartTime);
	log(LOG_DEBUG, "SCHED_MERGE_TL: mergeStateA=%d", SectionInvolvedDeleted[SCT_A_ID]);
	log(LOG_DEBUG, "SCHED_MERGE_TL: mergeStateB=%d", SectionInvolvedDeleted[SCT_B_ID]);
	int iSectionID;
	while ( event_idx < (int32_t)nextTimeLine.iEvents )
	{
		if ( ( nextTimeLine.PlannedEvent[event_idx].lPlannedStarTime ) >= (uint64_t)lastStartTime )
		{
			if ( nextTimeLine.PlannedEvent[event_idx].lPlannedStarTime == (uint64_t)lastStartTime )
			{
				//check last EventID, SectionId, SlotId in order to search the last running event
				if ( (nextTimeLine.PlannedEvent[event_idx].iSectionID == (unsigned int)iLastSectionID) &&
					 (nextTimeLine.PlannedEvent[event_idx].iSlotID == (unsigned int)iLastSlotID) &&
					 (nextTimeLine.PlannedEvent[event_idx].iEventTAG == iLastEventID))
				{
					// last event in running
					event_idx++;
					continue;
				}
			}
			// skip section deleted during the rescheduling
			iSectionID = nextTimeLine.PlannedEvent[event_idx].iSectionID;
			if ( SectionInvolvedDeleted[iSectionID] == 2 )
			{
				// the section is deleted during the rescheduling, skip this one
				event_idx++;
				continue;
			}

			/*! *******************************************************************************************************
			 * Only events not crossing the zero time boundary shall be merged
			 * ********************************************************************************************************
			 */
			mergeTimeLine.PlannedEvent[mergeTimeLine.iEvents] = nextTimeLine.PlannedEvent[event_idx];
			iSectionID = nextTimeLine.PlannedEvent[event_idx].iSectionID;

#if PRINT_MERGED_TIMELINE
			log(LOG_DEBUG, "SCHED_MERGE_TL: <--- Event=%d, StartTime=%llu, Tag=%d, %s", event_idx,
					mergeTimeLine.PlannedEvent[mergeTimeLine.iEvents].lPlannedStarTime,
					mergeTimeLine.PlannedEvent[mergeTimeLine.iEvents].iEventTAG,
					mergeTimeLine.PlannedEvent[mergeTimeLine.iEvents].cEventTAG);
#endif

			if ( !changed[iSectionID] )
			{
				/*! ***************************************************************************************************
				 * The start time for the section must be updated, do it
				 * ****************************************************************************************************
				 */
				mergeTimeLine.lStartSection[iSectionID] = nextTimeLine.PlannedEvent[event_idx].lPlannedStarTime;
				changed[iSectionID] = true;
			}

			mergeTimeLine.iEvents++;   // increment the temporary timeline event counter

		}
		else
		{

#if PRINT_MERGED_TIMELINE
			log(LOG_DEBUG, "SCHED_MERGE_TL: X--- Event=%d, StartTime=%llu, Tag=%d, %s", event_idx,
					nextTimeLine.PlannedEvent[event_idx].lPlannedStarTime,
					nextTimeLine.PlannedEvent[event_idx].iEventTAG,
					nextTimeLine.PlannedEvent[event_idx].cEventTAG);
#endif

		}
		event_idx++;
	}

	currTimeLine = mergeTimeLine;
	event_idx = 0;

#if PRINT_MERGED_TIMELINE
	log(LOG_DEBUG, "SCHED_MERGE_TL: merged timeline start");
	for(unsigned int i = 0; i < mergeTimeLine.iEvents; i++)
	{
		log(LOG_DEBUG, "SCHED_MERGE_TL: Event=%d, StartTime=%llu, Tag=%d, %s", i, mergeTimeLine.PlannedEvent[i].lPlannedStarTime,
				mergeTimeLine.PlannedEvent[i].iEventTAG, mergeTimeLine.PlannedEvent[i].cEventTAG);
	}
	log(LOG_DEBUG, "SCHED_MERGE_TL: merged timeline end");
#endif  // PRINT_MERGED_TIMELINE

#if PRINT_NEW_CURR_TIMELINE
	log(LOG_DEBUG, "SCHED_MERGE_TL: New current timeline start");
	for(unsigned int i = 0; i < currTimeLine.iEvents; i++)
	{
		log(LOG_DEBUG, "SCHED_MERGE_TL: Event=%d, StartTime=%llu, Tag=%d, %s",
			i,
			currTimeLine.PlannedEvent[i].lPlannedStarTime,
			currTimeLine.PlannedEvent[i].iEventTAG,
			currTimeLine.PlannedEvent[i].cEventTAG);
	}
	log(LOG_DEBUG, "SCHED_MERGE_TL: new current timeline end");
#endif  // PRINT_MERGED_TIMELINE

}

void SchedulerThread::problemInit(t_ErrorData& rErrorData)
{
	int sectionInvolved;

	/*! ***************************************************************************************************************
	 * Waits for getting the full control on the scheduler subsystem until the problem solution is found.
	 * The timeline execution must be left free to run in the meantime.
	 * ****************************************************************************************************************
	 */
	m_schedulerSem.wait();

	TimeStamp now;
	uint64_t ullNowMsec = now.get() / NSEC_PER_MSEC;
	zero_time = (int64_t)ullNowMsec;

	// search section involved
	for ( int i = SCT_A_ID; i < SCT_NUM_TOT_SECTIONS; i++ )
	{
		sectionInvolved = 0;
		searchSectionInvolved(i, &sectionInvolved);
		SectionInvolvedDeleted[i] = sectionInvolved;
	}

	int TaskSemLock = m_timeLineActiveSem.getValue();
	if ( TaskSemLock < 0 )
	{

		/*! ***********************************************************************************************************
		 * The timeline execution task is pending on the lock semaphore. Just initialize the scheduling subsystem
		 * ************************************************************************************************************
		 */
		if ( DeltaScheduler <= T_SCHEDULING )
		{
			DeltaScheduler = T_SCHEDULING;
		}
		log(LOG_DEBUG, "SCHED_PROBLEM_INIT: DeltaScheduler=%lld", DeltaScheduler);
		m_problem.startConfig(zero_time + DeltaScheduler);

	}
	else
	{

		/*! ***********************************************************************************************************
		 * The timeline execution task exists. Initialize the scheduling subsystem.
		 * Then add the not yet completed preceeding activities
		 * ************************************************************************************************************
		 */
		if ( DeltaScheduler <= T_SCHEDULING )
		{
			DeltaScheduler = T_SCHEDULING;
		}

		log(LOG_DEBUG, "SCHED_PROBLEM_INIT: DeltaScheduler=%lld", DeltaScheduler);
		m_problem.startConfig((int64_t)(zero_time+DeltaScheduler));
		if ( lastStartTime == 0 ) // rescheduling before first event is running
		{
			log(LOG_DEBUG, "SCHED_PROBLEM_INIT: AddScheduledProblemWithOffset zero_time=%lld", zero_time);
			m_problem.addScheduledProblemWithOffset(currTimeLine, zero_time, rErrorData);
		}
		else
		{
			log(LOG_DEBUG, "SCHED_PROBLEM_INIT: AddScheduledProblemWithOffset lastStartTime=%lld",lastStartTime);
			m_problem.addScheduledProblemWithOffset(currTimeLine, lastStartTime, rErrorData);
		}

	}

	/*! ***************************************************************************************************************
	 * All the event counters must be initialized now
	 * ****************************************************************************************************************
	 */
	prec.section_id = SCT_NONE_ID;
	prec.slot_id = SCT_SLOT_NONE_ID;
	for(int i = 0; i < SCT_NUM_TOT_SECTIONS + 1; i++ )
	{
		// Blanks both counter and lenght for preceding events
		prec.event_cnts[i] = -1;
	}
	memset(&precReadSec, 0, sizeof(precReadSec));

}

int SchedulerThread::closeAndRun(t_ErrorData& rErrorData)
{
    int  iRetValue = -1;

#if PRINT_PROBLEM
	/*! ***************************************************************************************************************
	 * Display the current problem
	 * ****************************************************************************************************************
	 */
	log(LOG_DEBUG, "SCHED_CLOSE_RUN: problem start");
	int iBlockCount = m_problem.getBlockCount();
	for(int i = 1; i < iBlockCount; i++)
	{
		t_Block* pBlock = m_problem.getBlockByIndex(i);
		log(LOG_DEBUG, "SCHED_CLOSE_RUN: Block=%d %s Planned=%d", i, pBlock->cEventTAG, pBlock->planned);
	}
	log(LOG_DEBUG, "SCHED_CLOSE_RUN: problem end");
#endif

	/*! ***************************************************************************************************************
	 * Perform the scheduling activity
	 * ****************************************************************************************************************
	 */
#if PRINT_TIMELINE
	TimeStamp now;
	uint64_t ullNowMsec = now.get() / NSEC_PER_MSEC;
	log(LOG_DEBUG, "SCHED_CLOSE_RUN: scheduling start=%lld", ullNowMsec);
#endif

	bool bHasScheduled = false;
	if ( m_problem.endConfig(rErrorData) == true )
	{
		CProblemIo problemIo;
		problemIo.exportProblemOnXmlFile(m_problem.getProblem(), "./Problem.xml");
#if PRINT_PROBLEM
        log(LOG_DEBUG, "SCHED_CLOSE_RUN: problem exported");
#endif
        CScheduler s;
		bHasScheduled = s.scheduleProblem(m_problem.getProblem(), &nextTimeLine, &rErrorData, stepFn);
	}

#if PRINT_TIMELINE
	now.save();
	ullNowMsec = now.get() / NSEC_PER_MSEC;
	log(LOG_DEBUG, "SCHED_CLOSE_RUN: scheduling end=%lld", ullNowMsec);
#endif

	/*! ***************************************************************************************************************
	 * Display the newly computed timeline
	 * ****************************************************************************************************************
	 */
	if( bHasScheduled == true )
	{
        log(LOG_INFO, "SCHED_CLOSE_RUN: timeline start");
		for(int i = 0; i < (int)nextTimeLine.iEvents; i++)
		{
			log(LOG_DEBUG, "SCHED_CLOSE_RUN: [Event=%d], STime=%llu, Tag=%d, %s, SctId=%d, SltID=%d, EvtID=%d",
				i,
				nextTimeLine.PlannedEvent[i].lPlannedStarTime,
				nextTimeLine.PlannedEvent[i].iEventTAG,
				nextTimeLine.PlannedEvent[i].cEventTAG,
				nextTimeLine.PlannedEvent[i].iSectionID,
				nextTimeLine.PlannedEvent[i].iSlotID,
				nextTimeLine.PlannedEvent[i].iEventID);
		}
		log(LOG_DEBUG, "SCHED_CLOSE_RUN: timeline end");


		/*! ***********************************************************************************************************
		 * The scheduling activity had been successfully completed.
		 * Start or restart the execution of the just filled timeline.
		 * ************************************************************************************************************
		 */
		rErrorData.iErrorCode = 0;
		iRetValue = tlineExecStart(); // start/restart the execution of the new timeline

		/*! ***********************************************************************************************************
		 * Scheduling activity completed. Stops accumulating the idle time and release the scheduling subsystem.
		 * ************************************************************************************************************
		 */
		iRetValue = m_schedulerSem.release();

	}
	else
	{
		// Diagnostic message
		log(LOG_DEBUG, "SCHED_CLOSE_RUN: Scheduling error %d: %s", rErrorData.iErrorCode, rErrorData.pErrorMessage);
        m_schedulerSem.release();
	}

	return iRetValue;

}

int SchedulerThread::tlineExecStart(void)
{

	int iRetValue = SEM_FAILURE;

	if ( nextTimeLine.iEvents > 0 )
	{

		/*! ***********************************************************************************************************
		 * One event at least exists in the next timelime, process it
		 * ************************************************************************************************************
		 */
		iRetValue = m_timeLineSwapSem.wait(); // gets the control over the timeline
		log(LOG_DEBUG, "SCHED_EXEC_START: %d events found on next timeline", nextTimeLine.iEvents);
		int iActiveTimeLineSemValue = m_timeLineActiveSem.getValue();
		log(LOG_DEBUG, "SCHED_EXEC_START: activeTimeLineSemValue = %d", iActiveTimeLineSemValue);
		if ( iActiveTimeLineSemValue < 0 )
		{
			// Thread is pending on semaphore
			currTimeLine = nextTimeLine;
			event_idx = 0;
			m_timeLineActiveSem.release(); // release the control of the timeline
			log(LOG_DEBUG, "SCHED_EXEC_START: timeline starts");
		}
		else
		{
			/*! *******************************************************************************************************
			 * The timeline execution thread is pending, waiting for the next time slot.
			 * Release the semaphore for a possible update of the delay.
			 * ********************************************************************************************************
			 */
			log(LOG_DEBUG, "SCHED_EXEC_START: timeline already running, merge and resync");
			mergeTimeline();
			iActiveTimeLineSemValue = m_timeLineActiveSem.getValue();
			log(LOG_DEBUG, "SCHED_EXEC_START: timeline MERGED (iActiveTimeLineSemValue=%d)", iActiveTimeLineSemValue);
			m_timeLineActiveSem.release(); // during merge the active_tline_sem was released no activity in the scheduler
			log(LOG_DEBUG, "SCHED_EXEC_START: starts after merge");
			iRetValue = m_timeLineExecSem.release();
			// forcedly unlocks the current timeline wait in order to re-read the new timeline
			iActiveTimeLineSemValue = 1;
		}

		/*! ***********************************************************************************************************
		 * Building of the new timeline is completed.
		 * Updates the deadlines and release the control over the timeline
		 * ************************************************************************************************************
		 */
		setLastTimeUse();
		if ( iActiveTimeLineSemValue >= 0 )
		{
			iRetValue = m_timeLineSwapSem.release();
		}
	}
	else
	{
		log(LOG_DEBUG, "SCHED_EXEC_START: empty timeLine, nothing to do");
		iRetValue = SEM_SUCCESS;
	}

	return iRetValue;
}

int SchedulerThread::tlineExecRelease(void)
{
	int iRetValue = SEM_SUCCESS;
#ifdef IMPROVEMENT_SCHEDULER
	iRetValue = m_timeLineExecSem.release();
#endif
	return iRetValue;
}

int SchedulerThread::closeAndAbort(void)
{
	int iRetValue = m_schedulerSem.release();
	return iRetValue;
}

struct precedence SchedulerThread::getLastPrec(void)
{
	return prec;
}

void SchedulerThread::setSectionInvolved(uint16_t section_id, uint16_t involved)
{
	SectionInvolvedDeleted[section_id] = involved & 0x3;
}

int SchedulerThread::searchSectionInvolved(uint16_t section_id, int *pSectionInvolved)
{
	int e;

	*pSectionInvolved = 0;
	e = m_timeLineSwapSem.wait();

	/*! ***************************************************************************************************************
	 * Copy from the "curr" to the "purge" timeline all entries but the ones containing the involved resource
	 * ****************************************************************************************************************
	 */
	while( event_idx < (int)currTimeLine.iEvents )
	{
		/*! ***********************************************************************************************************
		 * If the resource matches, rise the flag for the associates section, too
		 * ************************************************************************************************************
		 */
		if ( section_id == currTimeLine.PlannedEvent[event_idx].iSectionID )
		{
			*pSectionInvolved = 1;
			break;
		}
		event_idx++;
	}
	e = m_timeLineSwapSem.release();
	return e ;
}

void SchedulerThread::setLastTimeUse(void)
{
	TimeStamp now;
	int64_t ullNowMsec = now.getSigned() / NSEC_PER_MSEC;

	log(LOG_DEBUG, "SCHED_SET_LAST_TIME_USED: start=%lld", ullNowMsec);

	/*! ***************************************************************************************************************
	 * Scans the whole timeline to get the maximum time for which a rack is needed.
	 * The master search key are the events associated to the pipettor resource.
	 * ****************************************************************************************************************
	 */
	for ( uint16_t i = 0; i < currTimeLine.iEvents; i++ )
	{
		/*! ***********************************************************************************************************
		 * Check the single event into the timeline for any possible "use time" associated action
		 * ************************************************************************************************************
		 */
		switch ( currTimeLine.PlannedEvent[i].iEventTAG )
		{
			case   E_END_PROTO:
			{
				/*! ***************************************************************************************************
				 * End of protocol reached, save the associated time.
				 * ****************************************************************************************************
				 */
				lSectionLastTime[currTimeLine.PlannedEvent[i].iSectionID] = currTimeLine.PlannedEvent[i].lPlannedStarTime;
			}
			break;
		}
	}
	now.save();
	ullNowMsec = now.getSigned() / NSEC_PER_MSEC;
	log(LOG_DEBUG, "SCHED_SET_LAST_TIME_USED: end=%lld", ullNowMsec);
}


int SchedulerThread::workerThread()
{

	log(LOG_INFO, "SCHED_TIMELINE: thread started");
	while ( isRunning() )
	{
		for (int i = SCT_A_ID; i < SCT_NUM_TOT_SECTIONS; i++ )
		{
			SectionInvolvedDeleted[i] = 0;
		}

		// scheduler is now empty
		// TODO
		//Instr_reset_status(INIT_SCHED_PROGR);
		//Vn_counter_OnFile();//20120405 in order to store counters on file...
		//VN_update_endurance_runtime(VN_ENDURANCE_STORE_ONFILE);

		log(LOG_INFO, "SCHED_TIMELINE: currTimeLine.iEvents = %d", currTimeLine.iEvents);
		while (1)
		{
			m_timeLineActiveSem.wait();
			if ( currTimeLine.iEvents > 0 )
			{
				log(LOG_INFO, "SCHED_TIMELINE: scheduler active");
				break;
			}
			else
			{
				log(LOG_INFO, "SCHED_TIMELINE: scheduler Idle");
			}
		}
		// TODO
		//Vn_save_idle_time();
		lastStartTime = (int64_t)0;
		iLastSectionID = SCT_NONE_ID;
		iLastSlotID = SCT_SLOT_NONE_ID;
		iLastEventID = -1;
		// TODO
		//Instr_set_status(INIT_SCHED_PROGR);

		while ( 1 )
		{
			/*! *******************************************************************************************************
			 * Waits on semaphore until the absolute time for the next event into the current timeline is reached
			 * ********************************************************************************************************
			 */
			volatile uint64_t ullPlannedStartTimeMSec = currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].lPlannedStarTime;
			m_timeLineSwapSem.release();
			int iWaitResult = m_timeLineExecSem.wait(ullPlannedStartTimeMSec, true);
			m_timeLineSwapSem.wait(); // gets the control over the timeline
			if ( iWaitResult == SEM_SUCCESS )
			{
				/*! ***************************************************************************************************
				 * A timeline re-read has been forced by the merging or purging function.
				 * Just read new current event and waits.
				 * ****************************************************************************************************
				 */
				#if PRINT_MERGED_TIMELINE
				log(LOG_INFO, "SCHED_TIMELINE: timeline updated");
				#endif

				#ifdef IMPROVEMENT_SCHEDULER
				clock_gettime(CLOCK_REALTIME, &task_clock_time); // gets the current system time
				tline_task_time = (int64_t)(task_clock_time.tv_sec * (int64_t)INTERNAL_TIME_FACTOR) +
								(int64_t)(task_clock_time.tv_nsec / (1000000000L / (int64_t)INTERNAL_TIME_FACTOR) );
								// system time for the current scheduling

				if (tline_task_time < p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].lPlannedStarTime)
				{
					log(LOG_INFO, "SCHED_TIMELINE: Timeline try to recover time for Sk_tline_exec");
					int64_t diff_tline=p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].lPlannedStarTime-tline_task_time;
					log(LOG_INFO, "SCHED_TIMELINE: [Task tline diff_tline] : %llu", diff_tline);
					for ( int idxev=0; idxev<p_currTimeLine->iEvents; idxev++ )
					{
						p_currTimeLine->PlannedEvent[idxev].lPlannedStarTime-=diff_tline;
					}
				}
				#endif

				if ( currTimeLine.iEvents == 0 )
				{

					/*! ***********************************************************************************************
					 * The timeline had been totally purged, exits the execution loop
					 * ************************************************************************************************
					 */
					// TODO
					//VN_update_endurance_runtime(VN_ENDURANCE_STORE_ONFILE); //the scheduler is now empty
					break;
				}
			}
			else if ( iWaitResult == SEM_TIMEOUT )
			{

				/*! ***************************************************************************************************
				 * Timeout expired, gets the control on the timeline and execute the current event
				 * ****************************************************************************************************
				 */
				// Retrieve system time
				TimeStamp now;
				uint64_t ullNowMsec = now.get() / NSEC_PER_MSEC;
				log(LOG_INFO, "SCHED_TIMELINE: time-out expired t=%llu", ullNowMsec);
				log(LOG_INFO, "SCHED_TIMELINE: Event=%d, PlannedStartTime=%llu msec, Tag=%d:\"%s\"",
					event_idx,
					currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].lPlannedStarTime,
					currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].iEventTAG,
					currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].cEventTAG);

				// Pop the current PlannedEvent from the top of the queue;
				lastStartTime  = currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].lPlannedStarTime;
				iLastSectionID = currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].iSectionID;
				iLastSlotID    = currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].iSlotID;
				iLastEventID   = currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].iEventTAG;

				int64_t illInterEventsDelayMsec = (int64_t)ullNowMsec - lastStartTime;
				log(LOG_INFO, "SCHED_TIMELINE: delay with respect to planned time = %lld msec", illInterEventsDelayMsec);

				SectionInvolvedDeleted[iLastSectionID] = 1;

//				int errSection = 0;

				std::string eventStr;
				eventStr.assign(_eventsString.at(iLastEventID));

				switch(iLastEventID)
				{
					case E_DUMMY_1:
					case E_DUMMY_2:
					case E_START_PROTO:
					case E_END_PROTO:
					case E_SH_MOVE_FOR_READING:
					case E_RESTART_PROTO:
					case E_FLUO_READ:
					case E_MEASURE_ON_AIR:
					case E_MEASURE_OFF_AIR:
					case E_START_READ_SEC:		// start read sec
					case E_GET_READY_READ_SEC:	// start read sec
					case E_CHECK_STRIP_PRESENCE:
					case E_DECODE_BC:
					case E_DECODE_DM:
					case E_CHECK_CONE_PRESENCE:
					case E_RESCHEDULE_EVENT:
					case E_CHECK_SAMPLE0_PRESENCE:
					case E_CHECK_SAMPLE3_PRESENCE:
					case E_SET_DEFAULT_READ_SEC:
					case E_SEND_REPLY_READSEC:
					{
						// call the associated function, if any
						if(m_ptrEventFunction[iLastEventID])
						{
							log(LOG_INFO, "SCHED: %s_%d section %c", eventStr.c_str(), iLastEventID, 'A' + iLastSectionID);
							m_ptrEventFunction[iLastEventID](iLastSectionID, iLastSlotID, iLastEventID);
						}
						else
						{
							log(LOG_INFO, "SCHED: %s_%d NO ACTION !", eventStr.c_str(), iLastEventID);
						}
					}
					break;

#if 0
					case E_SH_MOVE_FOR_READING:
					{
						Sh_move_for_reading(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
					}break;
					case E_RESTART_PROTO:
					{
						Section_protocol_restart(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
					}break;

					case E_FLUO_READ:
					{
						Sh_read_section(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
					}
					break;

					case E_MEASURE_ON_AIR:
					{
						Sh_readAir_section(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,MEASURE_AIR_ACTIVE);
					}break;

					case E_MEASURE_OFF_AIR:
					{
						Sh_readAir_section(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,MEASURE_CHECK_AIR);
					}break;

					case E_AUTOCHECK_AIR:
					{
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_AUTOCHECK_AIR\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
						Sh_readAir_section(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,MEASURE_AIR_AUTO_CHECK);
					}break;

					case E_SH_MOVE_SS_READING :
					{
						//printf( "AutoCalibration DSP > move for reading SS\n");
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_SH_MOVE_SS_READING\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
						Sh_move_for_reading_solidstandard();
					}break;

					case E_SH_READ_SS:
					{
						//printf( "AutoCalibration DSP > Reading SS\n");
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_SH_READ_SS\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
						Sh_AutoCalibSS_section(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
					}break;


					case E_TRAY_MOVE_EJECT:				// section tray move for tip eject
					{
						char ans_buf[64];
						//Section_answer_purge(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						errSection=Section_move_to_coded_pos(NO_WAIT_END,p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															 SECTION_TRAY_ID, '7', ans_buf,NULL);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_TRAY_MOVE_EJECT\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_TRAY_MOVE_BCR:
					{
						// section tray move for barcode reading
						char ans_buf[64];
						//Section_answer_purge(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						errSection=Section_move_to_coded_pos(NO_WAIT_END,p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															 SECTION_TRAY_ID, 'N', ans_buf,NULL);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_TRAY_MOVE_BCR\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_TOWER_ACCESS_RESTORE:
					{
						// restore tower position for operator access
						//Section_answer_purge(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						errSection=Section_move_to_coded_pos(NO_WAIT_END,p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															 SECTION_TOWER_ID, '8', ans_buf,NULL);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_TOWER_ACCESS_RESTORE\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_TRAY_ACCESS_RESTORE:
					{
						// restore tray position for operator access
						//Section_answer_purge(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						errSection=Section_move_to_coded_pos(NO_WAIT_END,p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															 SECTION_TRAY_ID, 'O', ans_buf,NULL);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_TRAY_ACCESS_RESTORE\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_RAW_STRIP_READ:
					{
						uint16_t err_code;
						Sh_section_raw_strip_read(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,&err_code);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_RAW_STRIP_READ\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_TOWER_MOVE_SPR:
					{
						// move the tower for SPR reading
						errSection=Section_move_to_coded_pos(NO_WAIT_END,p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															 SECTION_TOWER_ID, '7', ans_buf,NULL);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_TOWER_MOVE_SPR\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;

					case E_TOWER_FORCE_SPR:
					{
						// move the tower to force the SPR position
						errSection=Section_move_to_coded_pos(NO_WAIT_END,p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															 SECTION_TOWER_ID, '1', ans_buf,NULL);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_TOWER_FORCE_SPR\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_RAW_SPR_READ:
					{
						// perform a full spr reading
						//1.2.3D012 @MAD
						prevPhase = Section_CheckAndSetPipettorUsage( p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID, SMT_Reserved, SMT_2BStarted);
						//cannot be stopped in the while because the cancel use the timeline semaphore
						Section_raw_read_spr(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_RAW_SPR_READ\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_READSEC_SEND:
					{
						Error e;
						//Section_answer_purge(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						e = Readsec_compose_and_send(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						//1.2.2.D022 2013-05-10 unlock Waste/disposable if possible
						SetUnlockRack4Waste();
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_READSEC_SEND\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_SET_RUNNING_STATE:
					{
						// set the running state for the section
						//@MAD not used
						Section_set_state(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID, S_S_RUNNING);
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_SET_RUNNING_STATE\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
					}break;
					case E_INCUBATE_START:
					{
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_INCUBATE_START\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
						//check if it is needed to add
						/*Section_strip_runstate(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
														  p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSlotID,RWFL_PREPROC_INCRUNNING_ONSCT);*/
						Section_set_state(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID, S_S_SECTIONINCUBATE);
						/// get the incubate base linked to the event of the scheduler
						e=SectionIncubateBase_getfbm(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
													 p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSlotID,
													 p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iEventID,&IncubateBase);
						if (e==Success)
						{
							//Set state Incubate Context status
							Section_SetStateIncubateContext(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															&IncubateBase,
															&tline_task_time,
															SECTION_INC_CFG_STARTED);

							Incubate_section_send(ETHBUFFPROT_DATANOTSTORE,
												  p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
												  &IncubateBase,
												  NULL,
												  INCUBATE_ASYNC);
						}
					}break;
					case E_INCUBATE_END:
					{
#ifdef REPORT_OPERATION_SCHEDULE
						printf("Sct%d E_INCUBATE_END\n",p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
#endif
						/// get the incubate base linked to the event of the scheduler
						e=SectionIncubateBase_getfbm(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
													 p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSlotID,
													 p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iEventID,&IncubateBase);
						if (e==Success)
						{
							//Set state Incubate Context status
							Section_SetStateIncubateContext(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
															&IncubateBase,
															&tline_task_time,
															SECTION_INC_CFG_COMPLETED);
							Incubate_section_send(ETHBUFFPROT_DATANOTSTORE,
												  p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
												  &IncubateBase,
												  NULL,
												  INCUBATE_ASYNC);
						}
						//FCA20120102 verify if exist another preproc operation in the timeline for the section
						//			that involves the pipettor
						checkexistActionPreproc = CheckExistInTimeLinePreproc(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						//FCA20180216 check if an incubate event has been scheduled on the scheduler in order to keep the status RUNNING
						//		or change to WAIT_IN_PROTO
						checkexistIncPreproc = CheckExistInTimeLineIncubateEvent(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID);
						if ((checkexistActionPreproc==1)||(checkexistIncPreproc==1))
						{
							Section_strip_runstate(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
												   p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSlotID,RWFL_PREPROC_END);
							//other events have been scheduled
							Section_set_state(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID, S_S_RUNNING);
						}else
						{
							//check if it is needed to manage the function
							//Section_CheckAndSetPipettorUsage in order to align with old version
							// Last preproc for this section,it is possible to change in wait_in_proto
							////////////////////////////////////////////////////
							printf("Section%c : RWFL_PROTO_INIT\n",SECTION_ID2CH(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID));
							Section_strip_runstate(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID,
												   p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSlotID,RWFL_PROTO_INIT);
							//other events have been scheduled
							Section_set_state(p_currTimeLine->PlannedEvent[CURRENT_EVENT_INDEX].iSectionID, S_S_SECTIONWAITPROTO);
						}
					}break;
#endif // #if 0
					default:
					{
						/* nothing to do*/
					}break;
				}

				/*! ***************************************************************************************************
				 * Execution of the event related action completed. Setup the next one.
				 * ****************************************************************************************************
				 */
				#ifdef FIFO_TIMELINE
//				if ( errSection == 0 )
//				{
					DeltaScheduler = (int64_t)currTimeLine.PlannedEvent[CURRENT_EVENT_INDEX].lLength;
					log(LOG_INFO, "SCHED_TIMELINE: DeltaScheduler=%lld", DeltaScheduler);
					if(popTimeline() == false)
					{
						log(LOG_INFO, "SCHED_TIMELINE: popTimeLine Fail");
					}
//				}
//				else
//				{
//					DeltaScheduler = 0;
//				}

				if ( currTimeLine.iEvents <= 0 )
				{
					/*! ***********************************************************************************************
					 * The execution of the last event has been started, exits the loop
					 * ************************************************************************************************
					 */
					currTimeLine.iEvents = 0;
					break;
				}
				#else
				event_idx++;
				if ( event_idx == currTimeLine.iEvents )
				{
					/*! ***********************************************************************************************
					 * The execution of the last event has been started, exits the loop
					 * ************************************************************************************************
					 */
					break;
				}
				if ( event_idx > currTimeLine.iEvents )
				{
					/*! ***********************************************************************************************
					 * Incorrect value event_idx loop
					 * ************************************************************************************************
					 */
					log(LOG_INFO, "Sk_tline_exec :event_idx > p_currTimeLine->iEvents");
					break;
				}
				#endif

			}

		}

		/*! ***********************************************************************************************************
		 * Timeline execution completed. Release resources and enters the idle state
		 **************************************************************************************************************
		 */
		log(LOG_INFO, "TimeLine scan completed, wait for new events...");
//		Vn_set_idle_time();
		m_timeLineSwapSem.release();
	}

	return 0;

}

void SchedulerThread::beforeWorkerThread()
{

}

void SchedulerThread::afterWorkerThread()
{

}

bool SchedulerThread::registerFunction(uint8_t event_id, int (*ptrFunction)(int, int, int))
{
	if(event_id >= E_NUM_EVENTS) return false;

	m_ptrEventFunction[event_id] = ptrFunction;
	return true;
}

int SchedulerThread::addDummyEvent(uint8_t event_id, uint16_t section_id, uint16_t prev_section_id,
								   uint16_t prev_slot_id, int (*ptrFunction)(int section, int slot, int event),
								   t_ErrorData& rErrorData, bool flag, bool addPrecedence)
{
	int iFailureCode = SCHED_SUCCESS;
	char buf[64];
	int64_t event_time;

	sprintf(buf, "S%d_dummy_%d", section_id, event_id);
	temp_eventcnt = prec.event_cnts[section_id];
	prec.event_cnts[section_id]++;

	switch(event_id)
	{
		case E_DUMMY_1:
			event_time = (int64_t)T_DUMMY_1;
		break;
		case E_DUMMY_2:
			event_time = (int64_t)T_DUMMY_2;
		break;
	}
	m_ptrEventFunction[event_id] = ptrFunction;

	p_block = m_problem.addBlock(buf,
								 event_id, // E_DUMMY_1,
								 section_id,
								 SCT_SLOT_NONE_ID,
								 prec.event_cnts[section_id],
								 R_SCAN_HEAD,
								 A_NONE,
								 event_time,
								 flag,
								 (int64_t)T_NOPLANNEDTIME,
								 rErrorData);
	if ( p_block == NULL )
	{
		iFailureCode = rErrorData.iErrorCode;
		return iFailureCode;
	}

	if ( ( addPrecedence == true ) && ( prec.event_cnts[section_id] > 0 ) )
	{
		p_prec = m_problem.addPrecedence(section_id,
										 SCT_SLOT_NONE_ID,
										 prec.event_cnts[section_id],
										 prev_section_id,
										 prev_slot_id,
										 temp_eventcnt,
										 (int64_t)D_MIN_ZERO,
										 (int64_t)D_MAX_ZERO,
										 rErrorData);
	}
	prec.section_id = section_id;
	prec.slot_id = SCT_SLOT_NONE_ID;
	return iFailureCode;
}

int SchedulerThread::addPrecedenceHead(uint16_t section_id, uint64_t minDelay,
									   uint64_t maxDelay, t_ErrorData& rErrorData)
{

	p_prec = m_problem.addPrecedence(precReadSec[section_id].section_id,
									 precReadSec[section_id].slot_id,
									 precReadSec[section_id].event_lastcntID,
									 -1,
									 -1,
									 -1,
									 minDelay,
									 maxDelay,
									 rErrorData);
	if(p_prec != NULL)
	{
		return SCHED_SUCCESS;
	}

	return rErrorData.iErrorCode;
}

int SchedulerThread::addSectionReadEvent(uint16_t section_id, uint16_t prev_section_id,
										uint16_t prev_slot_id, t_ErrorData& rErrorData,
										uint32_t * JgapsAbs, bool addPrecedence,
										uint64_t minDelay, uint64_t maxDelay,
										uint8_t measureAir)
{
	int iFailureCode = SCHED_SUCCESS;
	char buf[64];
	int64_t delay, tmpJgaps;
	uint8_t Jcnt, eventId;


	sprintf(buf, "S%d_proto_start", section_id);
	temp_eventcnt = prec.event_cnts[section_id];
	prec.event_cnts[section_id]++;

	p_block = m_problem.addBlock(buf,
								 E_START_PROTO,
								 section_id,
								 SCT_SLOT_NONE_ID,
								 prec.event_cnts[section_id],
								 R_NONE,
								 A_START_PROTO,
								 (int64_t)T_START_PROTO,
								 false,
								 (int64_t)T_NOPLANNEDTIME,
								 rErrorData);
	if ( p_block == NULL )
	{
		iFailureCode = rErrorData.iErrorCode;
		return iFailureCode;
	}

	precReadSec[section_id].section_id = section_id;
	precReadSec[section_id].slot_id = SCT_SLOT_NONE_ID;
	precReadSec[section_id].event_lastcntID = prec.event_cnts[section_id];

	/*......................................................................
	 . Precedence must be added for all but the first event.                .
	 .......................................................................*/
	if ( ( addPrecedence == true ) && ( prec.event_cnts[section_id] > 0 ) )
	{
		p_prec = m_problem.addPrecedence(section_id,
										 SCT_SLOT_NONE_ID,
										 prec.event_cnts[section_id],
										 prev_section_id,
										 prev_slot_id,
										 temp_eventcnt,
										 minDelay,
										 maxDelay,
										 rErrorData);
	}
	prec.section_id = section_id;
	prec.slot_id = SCT_SLOT_NONE_ID;

	/*..........................................................................
	. Insert the J reading points.                                             .
	...........................................................................*/
	for(Jcnt = 0; Jcnt < J_MAXMIN_VECTOR_SIZE; Jcnt++, JgapsAbs++)
	{
		if(*JgapsAbs != 0)
		{
			/*..................................................................
		   . A reading must be performed for the currently processed Jx.      .
		   . All steps (excluding J7) are performed according to a same skeleton.    .
		   ..................................................................*/
			if(Jcnt < (J_MAXMIN_VECTOR_SIZE - 1))
			{
				/*..............................................................
			   . Generic (J0..J6) protocol step. Start inserting the scan     .
			   . head move for next reading.                                  .
			   ...............................................................*/
				sprintf(buf, "S%dJ%d_sh_move_for_reading", section_id, Jcnt);
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf,
											 E_SH_MOVE_FOR_READING,
											 section_id,
											 SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id],
											 R_SCAN_HEAD,
											 A_FLUO_READ,
											 (int64_t)T_SH_MOVE_FOR_READING,
											 false,
											 (int64_t)T_NOPLANNEDTIME,
											 rErrorData);
				if ( p_block == NULL )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if(prec.event_cnts[section_id] > 0)
				{
					/*..............................................................
				   . Precedence must be added for all but the first event.        .
				   ...............................................................*/
					tmpJgaps = (int64_t)(*JgapsAbs);
					delay = tmpJgaps - ((int64_t)T_SH_MOVE_FOR_READING);
					p_prec = m_problem.addPrecedence(section_id,
													 SCT_SLOT_NONE_ID,
													 prec.event_cnts[section_id],
													 section_id,
													 SCT_SLOT_NONE_ID,
													 temp_eventcnt,
													 delay,
													 delay,
													 rErrorData);
				}

				/*..............................................................
			   . The scan head is already in position, Insert the fluorescence .
			   . reading step.                                                .
			   ...............................................................*/
				sprintf(buf, "S%dJ%d_read_fluo", section_id, Jcnt);
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf,
											 E_FLUO_READ,
											 section_id,
											 SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id],
											 R_SCAN_HEAD,
											 A_FLUO_READ,
											 (int64_t)T_FLUO_READ,
											 false,
											 (int64_t)T_NOPLANNEDTIME,
											 rErrorData);
				if ( p_block == NULL )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if(prec.event_cnts[section_id] > 0)
				{
					/*..............................................................
				   . Precedence must be added for all but the first event.        .
				   ...............................................................*/
					p_prec = m_problem.addPrecedence(section_id,
													 SCT_SLOT_NONE_ID,
													 prec.event_cnts[section_id],
													 section_id,
													 SCT_SLOT_NONE_ID,
													 temp_eventcnt,
													 (int64_t)D_MIN_ZERO,
													 (int64_t)D_MAX_ZERO,
													 rErrorData);
				}

				// Protocol restart
				sprintf(buf, "S%dJ%d_restart_proto", section_id, Jcnt);
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf,
											 E_RESTART_PROTO,
											 section_id,
											 SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id],
											 R_NONE,
											 A_NONE,
											 (int64_t)T_RESTART_PROTO,
											 false,
											 (int64_t)T_NOPLANNEDTIME,
											 rErrorData);
				if ( p_block == NULL )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if(prec.event_cnts[section_id] > 0)
				{
					/*..............................................................
				   . Precedence must be added for all but the first event.        .
				   ...............................................................*/
					delay = T_READING_WINDOW - T_FLUO_READ;
					p_prec = m_problem.addPrecedence(section_id,
													 SCT_SLOT_NONE_ID,
													 prec.event_cnts[section_id],
													 section_id,
													 SCT_SLOT_NONE_ID,
													 temp_eventcnt,
													 delay,
													 delay,
													 rErrorData);
				}
			}
			else
			{
				// this is J7, protocol final reading

				/*..............................................................
			   . The scan head is already in position, Insert the fluorescence .
			   . reading step.                                                .
			   ...............................................................*/
				sprintf(buf, "S%dJA_sh_move_for_reading", section_id);
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf,
											 E_SH_MOVE_FOR_READING,
											 section_id,
											 SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id],
											 R_SCAN_HEAD,
											 A_AIR_READ,
											 (int64_t)T_SH_MOVE_FOR_READING,
											 false,
											 (int64_t)T_NOPLANNEDTIME,
											 rErrorData);


				if ( p_block == NULL )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if(prec.event_cnts[section_id] > 0)
				{
					/*..............................................................
				   . Precedence must be added for all but the first event.        .
				   ...............................................................*/
					tmpJgaps = (int64_t)(*JgapsAbs);
					delay = tmpJgaps - ((BMX_LONG)T_SH_MOVE_FOR_READING);

					p_prec = m_problem.addPrecedence(section_id,
													 SCT_SLOT_NONE_ID,
													 prec.event_cnts[section_id],
													 section_id,
													 SCT_SLOT_NONE_ID,
													 temp_eventcnt,
													 delay,
													 delay,
													 rErrorData);
				}

				sprintf(buf, "S%dJA_read_air", section_id);
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

                (void)measureAir;
				/* no need to differentiate
				switch(measureAir)
				{
					case MEASURE_AIR_ACTIVE:
						eventId = E_MEASURE_ON_AIR;
					break;
					default:
						eventId = E_MEASURE_OFF_AIR;
					break;
				}
				*/
				eventId = E_MEASURE_ON_AIR;

				p_block = m_problem.addBlock(buf,
											 eventId,
											 section_id,
											 SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id],
											 R_SCAN_HEAD,
											 A_AIR_READ,
											 (int64_t)T_FLUO_READ,
											 false,
											 (int64_t)T_NOPLANNEDTIME,
											 rErrorData);


				if ( p_block == NULL )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if(prec.event_cnts[section_id] > 0)
				{
					/*..............................................................
				   . Precedence must be added for all but the first event.        .
				   ...............................................................*/
					p_prec = m_problem.addPrecedence(section_id,
													 SCT_SLOT_NONE_ID,
													 prec.event_cnts[section_id],
													 section_id,
													 SCT_SLOT_NONE_ID,
													 temp_eventcnt,
													 (int64_t)D_MIN_ZERO,
													 (int64_t)D_MAX_ZERO,
													 rErrorData);
				}

				/*..............................................................
			   . Special (J7) end of protocol step.                           .
			   ...............................................................*/
				sprintf(buf, "S%dJ%d_end_proto", section_id, Jcnt);
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf,
											 E_END_PROTO,
											 section_id,
											 SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id],
											 R_NONE,
											 A_END_PROTO,
											 (int64_t)T_END_PROTO,
											 false,
											 (int64_t)T_NOPLANNEDTIME,
											 rErrorData);


				if ( p_block == NULL )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if(prec.event_cnts[section_id] > 0)
				{
					/*..............................................................
				   . Precedence must be added for all but the first event.        .
				   ...............................................................*/
					delay = T_READING_WINDOW - T_FLUO_READ;
					p_prec = m_problem.addPrecedence(section_id,
													 SCT_SLOT_NONE_ID,
													 prec.event_cnts[section_id],
													 section_id,
													 SCT_SLOT_NONE_ID,
													 temp_eventcnt,
													 delay,
													 delay,
													 rErrorData);
				}
			}
		}
	}

	return iFailureCode;
}

//int SchedulerThread::addReadSecEvent(uint16_t section_id, uint16_t prev_section_id,
//									 uint16_t prev_slot_id, t_ErrorData& rErrorData,
//									 bool addPrecedence, int liDMNum, int liBCNum,
//									 int liSubstrateNum, int liSample0Num, int liSample3Num,
//									 int liConeAbsenceNum)
//{
//	precReadSec[section_id].section_id = section_id;
//	precReadSec[section_id].slot_id = SCT_SLOT_NONE_ID;
//	precReadSec[section_id].event_lastcntID = prec.event_cnts[section_id];

//	string buf("");
//	int iFailureCode = SCHED_SUCCESS;

//	// the cone absence detection is performed only after a failure in the read of the dtmx,
//	// therefore there is no need to init everything again.
//	if ( liConeAbsenceNum == 0 )
//	{
//		/*.................. Start ReadSec ...................*/

//		buf = "S" + to_string(section_id) + "_readSec_start";
//		temp_eventcnt = prec.event_cnts[section_id];
//		prec.event_cnts[section_id]++;

//		p_block = m_problem.addBlock(buf.c_str(), E_START_READ_SEC, section_id, SCT_SLOT_NONE_ID,
//									 prec.event_cnts[section_id], R_NONE, A_NONE, (int64_t)T_START_READSEC,
//									 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//		if ( p_block == nullptr )
//		{
//			iFailureCode = rErrorData.iErrorCode;
//			return iFailureCode;
//		}


//		/*......................................................................
//		 . Precedence must be added for all but the first event.                .
//		 .......................................................................*/
//		if ( ( addPrecedence == true ) && ( prec.event_cnts[section_id] > 0 ) )
//		{
//			p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//											 prev_section_id, prev_slot_id, temp_eventcnt, (int64_t)D_MIN_ZERO,
//											 (int64_t)D_MAX_ZERO, rErrorData);
//		}
//		prec.section_id = section_id;
//		prec.slot_id = SCT_SLOT_NONE_ID;

//		/*....... Move Components to Default Position ........*/
//		buf = "S" + to_string(section_id) + "_readSec_getReady";
//		temp_eventcnt = prec.event_cnts[section_id];
//		prec.event_cnts[section_id]++;

//		p_block = m_problem.addBlock(buf.c_str(), E_GET_READY_READ_SEC, section_id, SCT_SLOT_NONE_ID,
//									 prec.event_cnts[section_id], R_SCAN_HEAD, A_NONE, (int64_t)T_MOVE_READSEC,
//									 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//		if ( p_block == nullptr )
//		{
//			iFailureCode = rErrorData.iErrorCode;
//			return iFailureCode;
//		}

//		/*......................................................................
//		 . Precedence must be added for all but the first event.                .
//		 .......................................................................*/
//		if ( prec.event_cnts[section_id] > 0 )
//		{
//			p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//											 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//											 (int64_t)D_MAX_ZERO, rErrorData);
//		}
//	}

//	if ( liBCNum > 0 )
//	{
//		if ( liSubstrateNum > 0 )
//		{
//			/*.................. Read Substrate ..................*/

//			buf = "S" + to_string(section_id) + "_readSec_check_strip_presence";
//			temp_eventcnt = prec.event_cnts[section_id];
//			prec.event_cnts[section_id]++;

//			// Calculate the correct slot of time dedicated to this action
//			int64_t llTimeNeeded = T_SH_READ_SUBSTRATE + ( liSubstrateNum-1 ) * T_SH_READ_SUBSTRATE / 3;
//			p_block = m_problem.addBlock(buf.c_str(), E_CHECK_STRIP_PRESENCE, section_id, SCT_SLOT_NONE_ID,
//										 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ, llTimeNeeded,
//										 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//			if ( p_block == nullptr )
//			{
//				iFailureCode = rErrorData.iErrorCode;
//				return iFailureCode;
//			}

//			if ( prec.event_cnts[section_id] > 0 )
//			{
//				/*..............................................................
//			   . Precedence must be added for all but the first event.        .
//			   ...............................................................*/
//				p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//												 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//												 (int64_t)D_MAX_ZERO, rErrorData);
//			}
//		}

//		/*.................. Decode barcode ..................*/
//		buf = "S" + to_string(section_id) + "_readSec_decode_BC";
//		temp_eventcnt = prec.event_cnts[section_id];
//		prec.event_cnts[section_id]++;

//		// Calculate the correct slot of time dedicated to this action
//		int64_t llTimeNeeded = T_CAMERA_READ_BC + T_CAMERA_READ_BC * ( liBCNum-1 ) * 5 / 12;
//		p_block = m_problem.addBlock(buf.c_str(), E_DECODE_BC, section_id, SCT_SLOT_NONE_ID,
//									 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ, llTimeNeeded,
//									 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//		if ( p_block == nullptr )
//		{
//			iFailureCode = rErrorData.iErrorCode;
//			return iFailureCode;
//		}

//		if ( prec.event_cnts[section_id] > 0 )
//		{
//			/*..............................................................
//		   . Precedence must be added for all but the first event.        .
//		   ...............................................................*/
//			p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//											 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//											 (int64_t)D_MAX_ZERO, rErrorData);
//		}
//	}

//	if ( liDMNum > 0 )
//	{
//		/*................. Decode data matrix .................*/
//		buf = "S" + to_string(section_id) + "_readSec_decode_DM";
//		temp_eventcnt = prec.event_cnts[section_id];
//		prec.event_cnts[section_id]++;

//		// Calculate the correct slot of time dedicated to this action
//		int64_t llTimeNeeded = T_CAMERA_READ_DM + ( liDMNum-1 ) * T_CAMERA_READ_DM * 6 / 9;
//		p_block = m_problem.addBlock(buf.c_str(), E_DECODE_DM, section_id, SCT_SLOT_NONE_ID,
//									 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ, llTimeNeeded,
//									 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//		if ( p_block == nullptr )
//		{
//			iFailureCode = rErrorData.iErrorCode;
//			return iFailureCode;
//		}

//		if ( prec.event_cnts[section_id] > 0 )
//		{
//			/*..............................................................
//			. Precedence must be added for all but the first event.        .
//			...............................................................*/
//			p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//											 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//											 (int64_t)D_MAX_ZERO, rErrorData);
//		}
//	}

//	if ( liSample0Num > 0 )
//	{
//		/*...................... Check X0 ......................*/
//		buf = "S" + to_string(section_id) + "_readSec_check_presence_W0";
//		temp_eventcnt = prec.event_cnts[section_id];
//		prec.event_cnts[section_id]++;

//		// Calculate the correct slot of time dedicated to this action
//		int64_t llTimeNeeded = T_CAMERA_DETECT_SAMPLE + ( liSample0Num-1 ) * T_CAMERA_DETECT_SAMPLE / 2;
//		p_block = m_problem.addBlock(buf.c_str(), E_CHECK_SAMPLE0_PRESENCE, section_id, SCT_SLOT_NONE_ID,
//									 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ,
//									 llTimeNeeded, false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//		if ( p_block == nullptr )
//		{
//			iFailureCode = rErrorData.iErrorCode;
//			return iFailureCode;
//		}

//		if ( prec.event_cnts[section_id] > 0 )
//		{
//			/*..............................................................
//			. Precedence must be added for all but the first event.        .
//			...............................................................*/
//			p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//											 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//											 (int64_t)D_MAX_ZERO, rErrorData);
//		}
//	}

//	if ( liSample3Num > 0 )
//	{
//		/*...................... Check X3 ......................*/
//		buf = "S" + to_string(section_id) + "_readSec_check_presence_W3";
//		temp_eventcnt = prec.event_cnts[section_id];
//		prec.event_cnts[section_id]++;

//		// Calculate the correct slot of time dedicated to this action
//		int64_t llTimeNeeded = T_CAMERA_DETECT_SAMPLE + ( liSample3Num-1 ) * T_CAMERA_DETECT_SAMPLE / 2;
//		p_block = m_problem.addBlock(buf.c_str(), E_CHECK_SAMPLE3_PRESENCE, section_id, SCT_SLOT_NONE_ID,
//									 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ,
//									 llTimeNeeded, false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//		if ( p_block == nullptr )
//		{
//			iFailureCode = rErrorData.iErrorCode;
//			return iFailureCode;
//		}

//		if ( prec.event_cnts[section_id] > 0 )
//		{
//			/*..............................................................
//			. Precedence must be added for all but the first event.        .
//			...............................................................*/
//			p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//											 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//											 (int64_t)D_MAX_ZERO, rErrorData);
//		}
//	}

//	if ( liConeAbsenceNum > 0 )
//	{
//		/*................. Check Spr presence .................*/
//		buf = "S" + to_string(section_id) + "_readSec_check_presence_cone";
//		temp_eventcnt = prec.event_cnts[section_id];
//		prec.event_cnts[section_id]++;

//		// Calculate the correct slot of time dedicated to this action
//		int64_t llTimeNeeded = T_CAMERA_CHECK_CONE_ABS + (liConeAbsenceNum-1) * T_CAMERA_CHECK_CONE_ABS * 2 / 3;
//		p_block = m_problem.addBlock(buf.c_str(), E_CHECK_CONE_PRESENCE, section_id, SCT_SLOT_NONE_ID,
//									 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ,
//									 llTimeNeeded, false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//		if ( p_block == nullptr )
//		{
//			iFailureCode = rErrorData.iErrorCode;
//			return iFailureCode;
//		}

//		if ( prec.event_cnts[section_id] > 0 )
//		{
//			/*..............................................................
//			. Precedence must be added for all but the first event.        .
//			...............................................................*/
//			p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//											 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//											 (int64_t)D_MAX_ZERO, rErrorData);
//		}
//	}

//	buf = "S" + to_string(section_id) + "_readSec_move_default";
//	temp_eventcnt = prec.event_cnts[section_id];
//	prec.event_cnts[section_id]++;

//	p_block = m_problem.addBlock(buf.c_str(), E_SET_DEFAULT_READ_SEC, section_id, SCT_SLOT_NONE_ID,
//								 prec.event_cnts[section_id], R_SCAN_HEAD, A_NONE, (int64_t)T_MOVE_READSEC,
//								 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//	if ( p_block == nullptr )
//	{
//		iFailureCode = rErrorData.iErrorCode;
//		return iFailureCode;
//	}
//	if ( prec.event_cnts[section_id] > 0 )
//	{
//		p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//										 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//										 (int64_t)D_MAX_ZERO, rErrorData);
//	}

//	// Send reply at the end
//	buf = "S" + to_string(section_id) + "_send_readsec_reply";
//	temp_eventcnt = prec.event_cnts[section_id];
//	prec.event_cnts[section_id]++;

//	p_block = m_problem.addBlock(buf.c_str(), E_SEND_REPLY_READSEC, section_id, SCT_SLOT_NONE_ID,
//								 prec.event_cnts[section_id], R_NONE, A_NONE, (int64_t)T_SEND_REPLY_READSEC,
//								 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
//	if ( p_block == nullptr )
//	{
//		iFailureCode = rErrorData.iErrorCode;
//		return iFailureCode;
//	}

//	if ( prec.event_cnts[section_id] > 0 )
//	{
//		/*..............................................................
//		. Precedence must be added for all but the first event.        .
//		...............................................................*/
//		p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
//										 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
//										 (int64_t)D_MAX_ZERO, rErrorData);
//	}

//	return iFailureCode;
//}

#define READ_SEC_START		0
#define READ_SEC_SUBSTR		1
#define READ_SEC_BC			2
#define READ_SEC_DM			3
#define READ_SEC_SAMPLE0	4
#define READ_SEC_SAMPLE3	5
#define READ_SEC_CONE_ABS	6
#define READ_SEC_REPLY		7
#define READ_SEC_RESCHEDULE_EVENT	8
#define READ_SEC_END		9



int SchedulerThread::addReadSecEvent(uint16_t section_id, uint16_t prev_section_id,
									 uint16_t prev_slot_id, t_ErrorData& rErrorData,
									 bool addPrecedence, bool bStart, int liDMNum, int liBCNum,
									 int liSubstrateNum, int liSample0Num, int liSample3Num,
									 int liConeAbsenceNum)
{
	precReadSec[section_id].section_id = section_id;
	precReadSec[section_id].slot_id = SCT_SLOT_NONE_ID;
	precReadSec[section_id].event_lastcntID = prec.event_cnts[section_id];

	string buf("");
	int iFailureCode = SCHED_SUCCESS;

	unsigned char ucCase;
	if ( bStart )						ucCase = READ_SEC_START;
	else if ( liSubstrateNum > 0 )		ucCase = READ_SEC_SUBSTR;
	else if ( liBCNum > 0 )				ucCase = READ_SEC_BC;
	else if ( liDMNum > 0 )				ucCase = READ_SEC_DM;
	else if ( liSample0Num > 0 )		ucCase = READ_SEC_SAMPLE0;
	else if ( liSample3Num > 0 )		ucCase = READ_SEC_SAMPLE3;
	else if ( liConeAbsenceNum > 0 )	ucCase = READ_SEC_CONE_ABS;
	else								ucCase = READ_SEC_REPLY;

	while ( ucCase != READ_SEC_END )
	{
		switch ( ucCase )
		{
			case READ_SEC_START:
			{
				/*.................. Start ReadSec ...................*/

				buf = "S" + to_string(section_id) + "_readSec_start";
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf.c_str(), E_START_READ_SEC, section_id, SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id], R_NONE, A_NONE, (int64_t)T_START_READSEC,
											 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
				if ( p_block == nullptr )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				/*......................................................................
				 . Precedence must be added for all but the first event.                .
				 .......................................................................*/
				if ( ( addPrecedence == true ) && ( prec.event_cnts[section_id] > 0 ) )
				{
					p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
													 prev_section_id, prev_slot_id, temp_eventcnt, (int64_t)D_MIN_ZERO,
													 (int64_t)D_MAX_ZERO, rErrorData);
				}
				prec.section_id = section_id;
				prec.slot_id = SCT_SLOT_NONE_ID;

				/*....... Move Components to Default Position ........*/
				buf = "S" + to_string(section_id) + "_readSec_getReady";
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf.c_str(), E_GET_READY_READ_SEC, section_id, SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id], R_SCAN_HEAD, A_NONE, (int64_t)T_MOVE_READSEC,
											 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
				if ( p_block == nullptr )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				/*......................................................................
				 . Precedence must be added for all but the first event.                .
				 .......................................................................*/
				if ( prec.event_cnts[section_id] > 0 )
				{
					p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
													 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
													 (int64_t)D_MAX_ZERO, rErrorData);
				}
				ucCase = READ_SEC_SUBSTR;
			}
			break;

			case READ_SEC_SUBSTR:
			{
				if ( liSubstrateNum > 0 )
				{
					/*.................. Read Substrate ..................*/

					buf = "S" + to_string(section_id) + "_readSec_check_strip_presence";
					temp_eventcnt = prec.event_cnts[section_id];
					prec.event_cnts[section_id]++;

					// Calculate the correct slot of time dedicated to this action
					int64_t llTimeNeeded = T_SH_READ_SUBSTRATE + ( liSubstrateNum-1 ) * T_SH_READ_SUBSTRATE / 3;
					p_block = m_problem.addBlock(buf.c_str(), E_CHECK_STRIP_PRESENCE, section_id, SCT_SLOT_NONE_ID,
												 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ, llTimeNeeded,
												 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
					if ( p_block == nullptr )
					{
						iFailureCode = rErrorData.iErrorCode;
						return iFailureCode;
					}

					if ( prec.event_cnts[section_id] > 0 )
					{
						/*..............................................................
					   . Precedence must be added for all but the first event.        .
					   ...............................................................*/
						p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
														 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
														 (int64_t)D_MAX_ZERO, rErrorData);
					}

					// If the only operation is substrate read, then perform reply,
					// otherwise break and reschedule only the slot necessaries
					if ( liBCNum + liDMNum + liSample0Num + liSample3Num + liConeAbsenceNum == 0 )
					{
						ucCase = READ_SEC_REPLY;
					}
					else
					{
						ucCase = READ_SEC_RESCHEDULE_EVENT;
					}
				}
				else
				{
					ucCase = READ_SEC_BC;
				}
			}
			break;

			case READ_SEC_BC:
			{
				if ( liBCNum > 0 )
				{
					/*.................. Decode barcode ..................*/
					buf = "S" + to_string(section_id) + "_readSec_decode_BC";
					temp_eventcnt = prec.event_cnts[section_id];
					prec.event_cnts[section_id]++;

					// Calculate the correct slot of time dedicated to this action
					int64_t llTimeNeeded = T_CAMERA_READ_BC + T_CAMERA_READ_BC * ( liBCNum-1 ) * 5 / 12;
					p_block = m_problem.addBlock(buf.c_str(), E_DECODE_BC, section_id, SCT_SLOT_NONE_ID,
												 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ, llTimeNeeded,
												 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
					if ( p_block == nullptr )
					{
						iFailureCode = rErrorData.iErrorCode;
						return iFailureCode;
					}

					if ( prec.event_cnts[section_id] > 0 )
					{
						/*..............................................................
					   . Precedence must be added for all but the first event.        .
					   ...............................................................*/
						p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
														 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
														 (int64_t)D_MAX_ZERO, rErrorData);
					}
				}
				ucCase = READ_SEC_DM;
			}
			break;

			case READ_SEC_DM:
			{
				if ( liDMNum > 0 )
				{
					/*................. Decode data matrix .................*/
					buf = "S" + to_string(section_id) + "_readSec_decode_DM";
					temp_eventcnt = prec.event_cnts[section_id];
					prec.event_cnts[section_id]++;

					// Calculate the correct slot of time dedicated to this action
					int64_t llTimeNeeded = T_CAMERA_READ_DM + ( liDMNum-1 ) * T_CAMERA_READ_DM * 3 / 4;
					p_block = m_problem.addBlock(buf.c_str(), E_DECODE_DM, section_id, SCT_SLOT_NONE_ID,
												 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ, llTimeNeeded,
												 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
					if ( p_block == nullptr )
					{
						iFailureCode = rErrorData.iErrorCode;
						return iFailureCode;
					}

					if ( prec.event_cnts[section_id] > 0 )
					{
						/*..............................................................
						. Precedence must be added for all but the first event.        .
						...............................................................*/
						p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
														 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
														 (int64_t)D_MAX_ZERO, rErrorData);
					}
				}
				ucCase = READ_SEC_SAMPLE0;
			}
			break;

			case READ_SEC_SAMPLE0:
			{
				if ( liSample0Num > 0 )
				{
					/*...................... Check X0 ......................*/
					buf = "S" + to_string(section_id) + "_readSec_check_presence_W0";
					temp_eventcnt = prec.event_cnts[section_id];
					prec.event_cnts[section_id]++;

					// Calculate the correct slot of time dedicated to this action
					int64_t llTimeNeeded = T_CAMERA_DETECT_SAMPLE + ( liSample0Num-1 ) * T_CAMERA_DETECT_SAMPLE / 2;
					p_block = m_problem.addBlock(buf.c_str(), E_CHECK_SAMPLE0_PRESENCE, section_id, SCT_SLOT_NONE_ID,
												 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ,
												 llTimeNeeded, false, (int64_t)T_NOPLANNEDTIME, rErrorData);
					if ( p_block == nullptr )
					{
						iFailureCode = rErrorData.iErrorCode;
						return iFailureCode;
					}

					if ( prec.event_cnts[section_id] > 0 )
					{
						/*..............................................................
						. Precedence must be added for all but the first event.        .
						...............................................................*/
						p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
														 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
														 (int64_t)D_MAX_ZERO, rErrorData);
					}
				}
				ucCase = READ_SEC_SAMPLE3;
			}
			break;

			case READ_SEC_SAMPLE3:
			{
				if ( liSample3Num > 0 )
				{
					/*...................... Check X3 ......................*/
					buf = "S" + to_string(section_id) + "_readSec_check_presence_W3";
					temp_eventcnt = prec.event_cnts[section_id];
					prec.event_cnts[section_id]++;

					// Calculate the correct slot of time dedicated to this action
					int64_t llTimeNeeded = T_CAMERA_DETECT_SAMPLE + ( liSample3Num-1 ) * T_CAMERA_DETECT_SAMPLE / 2;
					p_block = m_problem.addBlock(buf.c_str(), E_CHECK_SAMPLE3_PRESENCE, section_id, SCT_SLOT_NONE_ID,
												 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ,
												 llTimeNeeded, false, (int64_t)T_NOPLANNEDTIME, rErrorData);
					if ( p_block == nullptr )
					{
						iFailureCode = rErrorData.iErrorCode;
						return iFailureCode;
					}

					if ( prec.event_cnts[section_id] > 0 )
					{
						/*..............................................................
						. Precedence must be added for all but the first event.        .
						...............................................................*/
						p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
														 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
														 (int64_t)D_MAX_ZERO, rErrorData);
					}
				}
				ucCase = READ_SEC_CONE_ABS;
			}
			break;

			case READ_SEC_CONE_ABS:
			{
				if ( liConeAbsenceNum > 0 )
				{
					/*................. Check Spr presence .................*/
					buf = "S" + to_string(section_id) + "_readSec_check_presence_cone";
					temp_eventcnt = prec.event_cnts[section_id];
					prec.event_cnts[section_id]++;

					// Calculate the correct slot of time dedicated to this action
					int64_t llTimeNeeded = T_CAMERA_CHECK_CONE_ABS + (liConeAbsenceNum-1) * T_CAMERA_CHECK_CONE_ABS * 2 / 3;
					p_block = m_problem.addBlock(buf.c_str(), E_CHECK_CONE_PRESENCE, section_id, SCT_SLOT_NONE_ID,
												 prec.event_cnts[section_id], R_SCAN_HEAD, A_RAW_STRIP_READ,
												 llTimeNeeded, false, (int64_t)T_NOPLANNEDTIME, rErrorData);
					if ( p_block == nullptr )
					{
						iFailureCode = rErrorData.iErrorCode;
						return iFailureCode;
					}

					if ( prec.event_cnts[section_id] > 0 )
					{
						/*..............................................................
						. Precedence must be added for all but the first event.        .
						...............................................................*/
						p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
														 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
														 (int64_t)D_MAX_ZERO, rErrorData);
					}

					// this is always the last check, so send readsec reply after it
//					ucCase = READ_SEC_REPLY;
//					break;
				}
				ucCase = READ_SEC_RESCHEDULE_EVENT;
			}
			break;

			case READ_SEC_REPLY:
			{
				buf = "S" + to_string(section_id) + "_readSec_move_default";
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf.c_str(), E_SET_DEFAULT_READ_SEC, section_id, SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id], R_SCAN_HEAD, A_NONE, (int64_t)T_MOVE_READSEC,
											 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
				if ( p_block == nullptr )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if ( prec.event_cnts[section_id] > 0 )
				{
					p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
													 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
													 (int64_t)D_MAX_ZERO, rErrorData);
				}

				// Send reply at the end
				buf = "S" + to_string(section_id) + "_send_readsec_reply";
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				p_block = m_problem.addBlock(buf.c_str(), E_SEND_REPLY_READSEC, section_id, SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id], R_NONE, A_NONE, (int64_t)T_SEND_REPLY_READSEC,
											 false, (int64_t)T_NOPLANNEDTIME, rErrorData);
				if ( p_block == nullptr )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if ( prec.event_cnts[section_id] > 0 )
				{
					/*..............................................................
					. Precedence must be added for all but the first event.        .
					...............................................................*/
					p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
													 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
													 (int64_t)D_MAX_ZERO, rErrorData);
				}
				ucCase = READ_SEC_END;
			}
			break;

			case READ_SEC_RESCHEDULE_EVENT:
				/*.......... reschedule events if needed ..........*/
				buf = "S" + to_string(section_id) + "_readSec_reschedule_events";
				temp_eventcnt = prec.event_cnts[section_id];
				prec.event_cnts[section_id]++;

				// Calculate the correct slot of time dedicated to this action
				p_block = m_problem.addBlock(buf.c_str(), E_RESCHEDULE_EVENT, section_id, SCT_SLOT_NONE_ID,
											 prec.event_cnts[section_id], R_NONE, A_NONE,
											 T_RESCHEDULE_EVENT, false, (int64_t)T_NOPLANNEDTIME, rErrorData);
				if ( p_block == nullptr )
				{
					iFailureCode = rErrorData.iErrorCode;
					return iFailureCode;
				}

				if ( prec.event_cnts[section_id] > 0 )
				{
					/*..............................................................
					. Precedence must be added for all but the first event.        .
					...............................................................*/
					p_prec = m_problem.addPrecedence(section_id, SCT_SLOT_NONE_ID, prec.event_cnts[section_id],
													 section_id, SCT_SLOT_NONE_ID, temp_eventcnt, (int64_t)D_MIN_ZERO,
													 (int64_t)D_MAX_ZERO, rErrorData);
				}
				ucCase = READ_SEC_END;
			break;

			default:
				ucCase = READ_SEC_END;
			break;
		}

	}

	return iFailureCode;
}

int SchedulerThread::removeSection(uint8_t section_id)
{
	int    i;
	int    iSectionID;

	/*..........................................................................
	  . Initially sets both the start and end times for all sections to zero.    .
	  . Values will be changed late if needed.                                   .
	  ...........................................................................*/
	for(i = 0; i < SCT_NUM_TOT_SECTIONS; i++)
	{
		/*......................................................................
		  . Sets values for a single section.                                    .
		  .......................................................................*/
		purgeTimeLine.lStartSection[i] = 0;
		purgeTimeLine.lEndSection[i] = 0;
	}

	/*..........................................................................
	  . Copy from the "curr" to the "purge" timeline all entries but the ones    .
	  . containing the involved section.                                         .
	  ...........................................................................*/
	purgeTimeLine.iEvents = 0; // no events initially into the purged timeline
	event_idx = 0;

	// gets the current system time
	clock_gettime(CLOCK_REALTIME, &te);
	// system time for the current scheduling
	endReSched_time = (BMX_LONG)(te.tv_sec * (BMX_LONG)SCHED_TIME_FACTOR) +
			(BMX_LONG) (te.tv_nsec / (1000000000L / (BMX_LONG)SCHED_TIME_FACTOR) );

	log(LOG_INFO, "Endtline_remove_Section%c=%lld\n",('A' + section_id), endReSched_time );

	while(event_idx < currTimeLine.iEvents)
	{
		if((currTimeLine.PlannedEvent[event_idx].lPlannedStarTime) >= lastStartTime)
		{
			iSectionID = currTimeLine.PlannedEvent[event_idx].iSectionID;
			SectionInvolvedDeleted[section_id] = 2;
			if(iSectionID != section_id)
			{
				/*..................................................................
				  . The section ID for the current event does not match the id for    .
				  . the section to be removed, Copy the event unchanged.             .
				  ...................................................................*/
				purgeTimeLine.PlannedEvent[purgeTimeLine.iEvents] = currTimeLine.PlannedEvent[event_idx];
				if(purgeTimeLine.lStartSection[iSectionID] == 0)
				{
					/*..............................................................
					  . First event for the current section, set both the start and  .
					  . the end times to the current values.                         .
					  ...............................................................*/
					purgeTimeLine.lStartSection[iSectionID] = currTimeLine.PlannedEvent[event_idx].lPlannedStarTime;
					purgeTimeLine.lEndSection[iSectionID] =
							currTimeLine.PlannedEvent[event_idx].lPlannedStarTime +
							currTimeLine.PlannedEvent[event_idx].lLength;
				}
				else if(purgeTimeLine.lEndSection[iSectionID] <
						currTimeLine.PlannedEvent[event_idx].lPlannedStarTime +
						currTimeLine.PlannedEvent[event_idx].lLength)
				{
					/*..............................................................
					  . The end of the last event for the current section must be    .
					  . updated, do it.                                              .
					  ...............................................................*/
					purgeTimeLine.lEndSection[iSectionID] =
							currTimeLine.PlannedEvent[event_idx].lPlannedStarTime +
							currTimeLine.PlannedEvent[event_idx].lLength;
				}

				purgeTimeLine.iEvents++;   // increment the temporary timeline event counter
			}
		}
		event_idx++;
	}  // (while)

	/*..........................................................................
	  . All "curr" events have been processed. Copy the "purge" timeline back    .
	  . into the "curr" one and resynchronize it.                                .
	  ...........................................................................*/
	currTimeLine = purgeTimeLine;
	event_idx = 0;

#if PRINT_REMOVE_SECTION_TIMELINE
	log(LOG_INFO, "---- REMOVE SECTION New current timeline ----\n");
	for(i = 0; i < currTimeLine.iEvents; i++)
	{
		log(LOG_INFO, "     Event=%d, StartTime=%llu, Tag=%d, %s\n",
				i,
				currTimeLine.PlannedEvent[i].lPlannedStarTime,
				currTimeLine.PlannedEvent[i].iEventTAG,
				currTimeLine.PlannedEvent[i].cEventTAG);
	}
	log(LOG_INFO, "---- New current timeline end ----\n");
#endif
	return 0;
}

uint64_t SchedulerThread::getSectionEndTime(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return lSectionLastTime[section];
}

