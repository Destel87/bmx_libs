/*! ****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SchedulerThread.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the SchedulerThread class.
//! @details
//!
//! ****************************************************************************
!*/

#ifndef SCHEDULERTHREAD_H
#define SCHEDULERTHREAD_H

#include "StaticSingleton.h"
#include "Semaphore.h"
#include "Thread.h"
#include "Loggable.h"

#include "SchedulerInclude.h"

class SchedulerThread :
		public StaticSingleton<SchedulerThread>,
		public Thread,
		public Loggable
{

    public:

        /*! ***********************************************************************************************************
         * @brief SchedulerThread	void constructor, initializes internal members with default values
         * ************************************************************************************************************
         */
        SchedulerThread();

        /*! ***********************************************************************************************************
         * @brief ~SchedulerThread	default destructor, free resources
         * ************************************************************************************************************
         */
        virtual ~SchedulerThread();

        /*! ***********************************************************************************************************
         * @brief problemInit	Initialize the scheduling problem.
         * @param rErrorData
         * ************************************************************************************************************
         */
        void problemInit(t_ErrorData& rErrorData);

        /*! ***********************************************************************************************************
         * @brief closeAndRun	stops the problem insertion and start running the scheduling activity based on
         *						the currently existing problems.
         * @param rErrorData
         * @return
         * ************************************************************************************************************
         */
        int closeAndRun(t_ErrorData& rErrorData);

        /*! ***********************************************************************************************************
         * @brief closeAndAbort abort the problem currently being built by releasig the m_schedulerSem semaphore.
         * @return SEM_SUCCESS or SEM_FAILURE depending on the releasing operation outcome
         * ************************************************************************************************************
         */
        int closeAndAbort(void);

        /*! ***********************************************************************************************************
         * @brief getLastPrec	returns the structure containing the last values used to define a precedence
         *						between blocks
         * @return the current value of member variable prec that holds the last configured precedence
         * ************************************************************************************************************
         */
        struct precedence getLastPrec(void);

        /*! ***********************************************************************************************************
         * @brief addSectionReadEvent	insert all the steps required to perform the protocol readings
         * @param section_id	the section identifier (0-1)
         * @param prev_section_id
         * @param prev_slot_id
         * @param rErrorData the error structure
         * @param JgapsAbs the pointer to the reading time
         * @param addPrecedence to insert the first precedence
         * @param minDelay
         * @param maxDelay
         * @param measureAir flag to activate the air measure at the end
         * @return 0 in case of success, error code otherwise
         * ************************************************************************************************************
         */
        int addSectionReadEvent(uint16_t section_id, uint16_t prev_section_id,
                                            uint16_t prev_slot_id, t_ErrorData& rErrorData,
                                            uint32_t * JgapsAbs, bool addPrecedence,
                                            uint64_t minDelay, uint64_t maxDelay,
                                            uint8_t measureAir);

		int addReadSecEvent(uint16_t section_id, uint16_t prev_section_id,
							uint16_t prev_slot_id, t_ErrorData& rErrorData, bool addPrecedence, bool bStart,
							int liDMNum, int liBCNum, int liSubstrateNum, int liSample0Num,
							int liSample3Num, int liConeAbsenceNum);


        /*! ***********************************************************************************************************
         * @brief addPrecedenceHead	insert the timing precedence read from the constraint
         * @param section_id	the section identifier (0-1)
         * @param rErrorData the error structure
         * @param minDelay
		 * @param maxDelay
         * @return 0 in case of success, error code otherwise
         * ************************************************************************************************************
         */
        int addPrecedenceHead(uint16_t section_id, uint64_t minDelay,
                                               uint64_t maxDelay, t_ErrorData& rErrorData);

        /*! ***********************************************************************************************************
         * @brief registerFunction	set the callback function linked to a specific event
         * @param event_id	the event identifier
         * @param ptrFunction the function pointer (function with 3 arguments)
         * @return true in case of success, false otherwise
         * ************************************************************************************************************
         */
        bool registerFunction(uint8_t event_id,  int (*ptrFunction)(int section, int slot, int event));

        /*! ***********************************************************************************************************
         * @brief removeSection	remove from the timeline the events of the specific section
         * @param section_id	the section identifier (0-1)
         * @return 0 in case of success, error code otherwise
         * ************************************************************************************************************
         */
        int removeSection(uint8_t section_id);


        /*! ***********************************************************************************************************
         * @brief data setters and getters
         * ************************************************************************************************************
         */
        int64_t getSchedulationStartTime(void) { return sched_stime; }
        int64_t getSchedulationLastTime(void) { return sched_ltime; }
        int64_t getSchedulationDiffTime(void) { return sched_difftime; }
        uint32_t getSchedulationNumCycles(void) { return numCycles; }
        void setSchedulationStartTime(int64_t illSchedStartTimeMsec) { sched_stime = illSchedStartTimeMsec; }
        void setSchedulationLastTime(int64_t illSchedLastTimeMsec) { sched_ltime = illSchedLastTimeMsec; }
        void setSchedulationDiffTime(int64_t illSchedDiffTimeMsec) { sched_difftime = illSchedDiffTimeMsec; }
        void setSchedulationNumCycles(uint32_t ulNumCycles) { numCycles = ulNumCycles; }

        void setSectionInvolved(uint16_t section_id, uint16_t involved);
        uint64_t getSectionEndTime(uint8_t section);


        /*! ***********************************************************************************************************
         * @brief addDummyEvent	insert a test dummy event
         * @param event_id the id of the event
         * @param section_id	the section identifier (0-1)
         * @param prev_section_id
         * @param prev_slot_id
         * @param rErrorData the error structure
         * @param addPrecedence to insert the first precedence
         * @param flag
         * @param ptrFunction pointer to the event callback function
         * @return 0 in case of success, error code otherwise
         * ************************************************************************************************************
         */
        int addDummyEvent(uint8_t event_id, uint16_t section_id, uint16_t prev_section_id, uint16_t prev_slot_id,
                          int (*ptrFunction)(int section, int slot, int event),
                          t_ErrorData& rErrorData, bool flag, bool addPrecedence);

    protected:

        int workerThread();
        void beforeWorkerThread();
        void afterWorkerThread();

    private:

		friend void stepFn(int phase, int64_t timeout, bool solutionFound, bool* interruptionRequested);

		/*! ***********************************************************************************************************
		 * @brief popTimeline moves up of one position every event in the currTimeline
		 * ************************************************************************************************************
		 */
		bool popTimeline(void);

		/*! ***********************************************************************************************************
		 * @brief mergeTimeline merges the current timeline with the supposedly new event list
		 * ************************************************************************************************************
		 */
		void mergeTimeline(void);
		int tlineExecStart(void);
		int tlineExecRelease(void);

		int searchSectionInvolved(uint16_t section_id, int *pSectionInvolved);

		/*! ***********************************************************************************************************
		 * @brief setLastTimeUse	browse the currentTimeline and fills the rack data pool with the last time for
		 *							which a single positions is expected to be used by a single slot/section.
		 **************************************************************************************************************
		 */
		void setLastTimeUse(void);

    private:

        Semaphore m_schedulerSem;
        Semaphore m_timeLineActiveSem;
        Semaphore m_timeLineExecSem;
        Semaphore m_timeLineSwapSem;

        CProblem m_problem;

        t_Block* p_block;
        t_PrecedenceDef* p_prec;
        struct timespec ts;                  // current system time

        t_TimeLine currTimeLine;
        t_TimeLine nextTimeLine;
        t_TimeLine purgeTimeLine;

        uint16_t event_idx;
        int64_t zero_time;				// system time for the current scheduling
        int64_t DeltaScheduler;
        uint64_t endReSched_time;
        uint64_t lastStartTime;

        int iLastSectionID;
        int iLastSlotID;
        int iLastEventID;

        struct precedence prec;
        struct precedence precReadSec[SCT_NUM_TOT_SECTIONS];
        int temp_eventcnt;
        struct timespec te;                  // current system time

        int SectionInvolvedDeleted[SCT_NUM_TOT_SECTIONS];
        uint64_t lSectionLastTime[SCT_NUM_TOT_SECTIONS];

        // Step function
        int64_t sched_stime;
        int64_t sched_ltime;
        int64_t sched_difftime;
        uint32_t numCycles;

        // callback function associated to each event defined by user
        int (* m_ptrEventFunction[E_NUM_EVENTS])(int section, int slot, int event);

};

#endif // SCHEDULERTHREAD_H
