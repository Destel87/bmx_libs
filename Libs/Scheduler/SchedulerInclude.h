//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SchedulerInclude.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the defines for the Scheduler lib.
//! @details
//!
//*!****************************************************************************

#ifndef SCHEDULERINCLUDE_H
#define SCHEDULERINCLUDE_H

#include <stdint.h>
#include <time.h>

#include "CProblem.h"
#include "CScheduler.h"

// for the Sections and Slots defines
#include "../../../../mbfw/CommonInclude.h"

/*! *******************************************************************************************************************
 * Resource definitions
 * ********************************************************************************************************************
 */
#define R_NONE						0	// none resource (must be 0)
#define R_SCAN_HEAD					1	// scan head

/*! *******************************************************************************************************************
 * General defines
 * ********************************************************************************************************************
 */

#define SCHED_TIME_FACTOR		1000

#define SCHED_SUCCESS				0
#define SCHED_ERROR					1

#define NOLOCK_SEM_TIMELINE			0	// if rescheduling is being performed by the scheduler itself avoids a deadlock
#define LOCK_SEM_TIMELINE			1
#define FIFO_TIMELINE
#define NO_IMPROVEMENT_SCHEDULER
#define REPORT_OPERATION_SCHEDULE
#define ASPIRATION_ENABLE
#define NEWADDPRECEDENCE_			// enables function problem_newaddprecedence to apply constraints filtering
#define PROBLEM_ADDPRECEDENCE_DBG_	// enables debug to add.precedence

#define PRINT_MERGED_TIMELINE		1
#define PRINT_NEW_CURR_TIMELINE		1
#define PRINT_PROBLEM				1
#define PRINT_TIMELINE				1

#define MEASURE_AIR_ACTIVE			1

/*! *******************************************************************************************************************
 * Activity definitions
 * ********************************************************************************************************************
 */
// Scan head + camera
#define A_NONE					0	// none specific activity (must be 0)
#define A_FLUO_READ				1	// fluorescence reading
#define A_RAW_STRIP_READ		2	// strip barcode reading
#define A_AIR_READ				3	// air reading

// TODO camera activities...
// None resource
#define A_NONE					0	// none specific activity (must be 0)
#define A_START_PROTO			1	// protocol execution start
#define A_END_PROTO				2	// protocol execution end

/*! *******************************************************************************************************************
 * Event unique tags definition
 * ********************************************************************************************************************
 */

// linked to the string vector defined in the schedulerthread.cpp file
enum scheduler_events_tag
{
	E_RESET	= 0,			// reset section
	E_START_PROTO,			// start protocol on section
	E_FLUO_READ,			// fluorescence reading on section
	E_END_PROTO,			// end of protocol on section
	E_TRAY_MOVE_BCR,		// section tray move for barcode reading
	E_RAW_STRIP_READ,		// barcode and background raw read
	E_TOWER_MOVE_SPR,		// tower move for SPR reading
	E_RAW_SPR_READ,			// pipettor move and spr read
	E_READSEC_SEND,			// send all bcodes data
	E_SET_RUNNING_STATE,	// set running state for a section
	E_SH_MOVE_FOR_READING,	// scan head move before reading
	E_RESTART_PROTO,		// restart protocol
	E_TOWER_ACCESS_RESTORE,	// restore tower for operator access
	E_TRAY_ACCESS_RESTORE,	// restore tray for operator access
	E_MEASURE_ON_AIR,		// reading on air operation
	E_MEASURE_OFF_AIR,		// reading off air operation
	E_AUTOCHECK_AIR,		// reading air operation in CheckSanity
	E_SH_MOVE_SS_READING,	// move for reading solid standard
	E_SH_READ_SS,			// Sh reading Solid Standard
	E_TOWER_FORCE_SPR,		// Force the SPR position before the reading

	E_START_READ_SEC,		// start read sec
	E_GET_READY_READ_SEC,	// move tray and spr to home position
	E_CHECK_STRIP_PRESENCE,
	E_DECODE_DM,
	E_DECODE_BC,
	E_CHECK_SAMPLE0_PRESENCE,
	E_CHECK_SAMPLE3_PRESENCE,
	E_CHECK_CONE_PRESENCE,
	E_RESCHEDULE_EVENT,
	E_SET_DEFAULT_READ_SEC,
	E_SEND_REPLY_READSEC,

	E_DUMMY_1,
	E_DUMMY_2,
	E_NUM_EVENTS
};

/*! *******************************************************************************************************************
 * Timing definitions (ms)
 * ********************************************************************************************************************
 */
//#define T_SCHEDULING			3000	// maximum total time allowed for scheduling
// A.T changed from 3000 to 1000 because of faster processor and simpler events list to be scheduler (no pipettor)
#define T_SCHEDULING			1000	// maximum total time allowed for scheduling
#define T_SCHED_CANCEL			15000	// maximum total time allowed for scheduling during a cancel
#define T_NONE					0		// no time for none activity
#define T_NOPLANNEDTIME			0		// none planned time (will be zero)
#define T_FLUO_READ				10500	// fluo reading     // 4000->8000->10000 (MODULAR)
#define T_START_PROTO			10		// protocol execution execution start
#define T_END_PROTO				10		// protocol execution execution end
#define T_FLUO_READ_CALIBSS		5000	// fluo reading
#define T_RAW_STRIP_READ		16000	// head positioning + barcode reading
#define T_TOWER_MOVE_SPR		8500	// tower move for spr reading
#define T_RAW_SPR_READ			10000	// spr datamatrix read
#define T_READSEC_SEND			100		// send all bcodes data  // 100->10
#define T_TRAY_MOVE_BCR			5500	// move tray for bcr
#define T_SET_RUNNING_STATE		10		// set running state for a section
#define T_SH_MOVE_FOR_READING	1500	// scan head move before reading (ex 3500 VIDAS3)
#define T_RESTART_PROTO			1		// protocol restart after Jx reached
#define T_READING_WINDOW		12000	// reading fixed window
#define T_TOWER_ACCESS_RESTORE	2500	// tower restore position for operator access
#define T_TRAY_ACCESS_RESTORE	4000	// tray restore position for operator access
#define T_TOWER_FORCE_SPR		8500	// tower move for force SPR position
#define T_INITPUMP_AFTEREJECT	5000	// adding time to reset the pump after eject
#define T_TRAY_CHECK_HOME		8000	// time to check home of the tray
#define T_TRAY_MOVE_INCUBATE	5000	// move tray for incubate

#define T_START_READSEC			20
//#define T_SH_READ_SUBSTRATE		8000	// time to read substrate of a section
//#define T_CAMERA_READ_DM		15000	// time to decode data matrix
//#define T_CAMERA_READ_BC		15000	// time to decode barcode
//#define T_CAMERA_DETECT_SAMPLE	15000	// time to read sample presence

#define T_SH_READ_SUBSTRATE		3000	// time to read substrate of a section
#define T_CAMERA_READ_BC		6000	// time to decode barcode
#define T_CAMERA_READ_DM		8000	// time to decode data matrix
#define T_CAMERA_CHECK_CONE_ABS	10000	// time to check spr presence
#define T_CAMERA_DETECT_SAMPLE	4500	// time to read sample presence
#define T_SEND_REPLY_READSEC	100
#define T_RESCHEDULE_EVENT		500

#define T_MOVE_READSEC			4000

// NOTE NOTE NOTE : T_SH_MOVE_FOR_READING + T_FLUO_READ <= 12000 (12 secs)

#define T_DUMMY_1				1000
#define T_DUMMY_2				2000

/*! *******************************************************************************************************************
 * Precedence delays
 * ********************************************************************************************************************
 */
#define D_MIN_ZERO 0
#define D_MAX_ZERO 0

/*! *******************************************************************************************************************
 * Scheduler members and utilities
 * *******************************************************************************************************************
 */
#define SCHED_SEM_SCHEDULER_NAME				"/sem-scheduler-sem"
#define SCHED_SEM_SCHEDULER_INIT_VALUE			1
#define SCHED_SEM_TIMELINE_ACTIVE_NAME			"/sem-timeline-active-sem"
#define SCHED_SEM_TIMELINE_ACTIVE_INIT_VALUE	0
#define SCHED_SEM_TIMELINE_EXEC_NAME			"/sem-timeline-exec-sem"
#define SCHED_SEM_TIMELINE_EXEC_INIT_VALUE		0
#define SCHED_SEM_TIMELINE_SWAP_NAME			"/sem-timeline-swap-sem"
#define SCHED_SEM_TIMELINE_SWAP_INIT_VALUE		1
#define CURRENT_EVENT_INDEX						0

/*! *******************************************************************************************************************
 * Types definition
 * ********************************************************************************************************************
 */
struct precedence
{
	uint16_t   section_id;
	uint16_t   slot_id;
	int        event_lastcntID;  //  contains the last eventID
	int        event_num;        //  future use
	int        event_cnts[SCT_NUM_TOT_SECTIONS + 1];
};


#endif // SCHEDULERINCLUDE_H
