//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CScheduler.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the CScheduler class.
//! @details
//!
//*!****************************************************************************

#include "CScheduler.h"
#include "problem_solver.cpp"

CScheduler::CScheduler()
{

}

CScheduler::~CScheduler()
{

}

bool CScheduler::scheduleProblem(t_Problem* pProblem, t_TimeLine* pTimeLine, t_ErrorData* pErrorData, StepFn step)
{
    return Scheduler_scheduleProblem(pProblem, pTimeLine, pErrorData, step);
}

