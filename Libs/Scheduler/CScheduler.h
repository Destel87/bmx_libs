//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CScheduler.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the CScheduler class.
//! @details
//!
//*!****************************************************************************

#ifndef CSCHEDULER_H
#define CSCHEDULER_H

#include <stdint.h>
#include "Scheduler/scheduler.h"

class CScheduler
{
	public:
		CScheduler();
		virtual ~CScheduler();
		bool scheduleProblem(t_Problem* pProblem, t_TimeLine* pTimeLine, t_ErrorData* pErrorData, StepFn step);
};

#endif // CSCHEDULER_H
