//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CProblemIo.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the CProblemIo class.
//! @details
//!
//*!****************************************************************************

#include <stdio.h>
#include <string.h>
#include "CProblemIo.h"

CProblemIo::CProblemIo()
{

}

CProblemIo::~CProblemIo()
{

}

bool CProblemIo::exportProblemOnXmlFile(t_Problem* pProblem, const char* sFileName)
{
	if ( pProblem == 0 )
		return false;
	size_t n = strlen(sFileName);
	if ( n <= 1 )
		return false;

	// Check if file can be opened
	FILE* pFile = fopen (sFileName , "w");
	if ( pFile == NULL )
		return false;
	else
	  fclose (pFile);

	bool bRetValue = ProblemIO_export(pProblem, sFileName);
	return bRetValue;
}
