//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CProblem.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the CProblem class.
//! @details
//!
//*!****************************************************************************

#ifndef CPROBLEM_H
#define CPROBLEM_H

#include "StaticSingleton.h"
#include "Scheduler/problem.h"

/*! *******************************************************************************************************************
 * @brief	The CProblem class is a wrapper for function and data structures contained in problem.h/.cpp file.
 *			It provides functionalities to setup and solve schedulation problem of events involving shared resources.
 *			For further info please check documentation in @file problem.h and @file problem.cpp files
 * ********************************************************************************************************************
 */

class CProblem
{

	public:

		CProblem();

		virtual ~CProblem();

		bool loadWorkPlan(t_WorkPlan& pWorkPlan, t_ErrorData& pErrorData);

		void startConfig(int64_t lTimeOffset);

		t_Block* addBlock(const char* cEventTAG, int iEventTAG,
						  int iSectionID, int iSlotID, int iEventID, int iResourceID, int iActionID,
						  int64_t lLength, bool planned, int64_t plannedTime,
						  t_ErrorData& pErrorData);

		void setSlotPriority(int iSectionID, int iSlotID, int iPriority);

		t_PrecedenceDef* addPrecedence(int iSection, int iSlot, int iEvent,
									   int iPredSection, int iPredSlot, int iPredEvent,
									   int64_t lMinDelay, int64_t lMaxDelay,
									   t_ErrorData& pErrorData);

		t_PrecedenceDef* addStartingConstraint(int iSection, int iSlot, int iEvent, int64_t lMinDelay, int64_t lMaxDelay, t_ErrorData& pErrorData);

		bool addScheduledProblem(t_TimeLine& pTimeLine, t_ErrorData& pErrorData);

		bool addScheduledProblemWithOffset(t_TimeLine& pTimeLine, int64_t lOffset, t_ErrorData& pErrorData);

		bool endConfig(t_ErrorData& pErrorData);

		bool addResourceConflict(int iResourceID1, int iActionID1, int iResourceID2, int iActionID2, t_ErrorData& pErrorData);

		bool calcBlockAbsoluteTimes(t_ErrorData& pErrorData);

		void setStartJitter(int64_t lStartJitter);

		t_ProblemSlot* getProblemSlot(int iSectionID, int iSlotID);

		t_Block* findBlock(int iSectionID, int iSlotID, int iEventID);

		bool testResourceConflict(t_Block& pBlock1, t_Block& pBlock2);

		t_Problem* getProblem(void);

		int getBlockCount(void);

		t_Block* getBlockByIndex(int iIndex);

    private:

        t_Problem m_Problem;

};

#endif // CPROBLEM_H
