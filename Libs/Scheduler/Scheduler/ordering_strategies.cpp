/*
 * ordering_strategies.c
 *
 *  Created on: 19/mag/2010
 *      Author: jacopo
 */

#include "ordering_strategies.h"

OrderingStrategyFn fg_orderingStrategies[] =
{ minIntervalStrategy, maxIntervalStrategy };

const char* fg_osNames[] =
{ "Minimum interval", "Maximum interval" };


int OrderingStrategies_getOrderingStrategyCount()
{
	return sizeof(fg_orderingStrategies) / sizeof(OrderingStrategyFn);
}

OrderingStrategyFn OrderingStrategies_getOrderingStrategy(int index)
{
	return fg_orderingStrategies[index];
}

const char* OrderingStrategies_getOrderingStrategyName(int index)
{
	return fg_osNames[index];
}

bool maxIntervalStrategy(t_Interval* pLeftInterval, t_Interval* pRightInterval,
		int iBlockIndex, int jBlockIndex)
{
	return pLeftInterval->right - pLeftInterval->left >= pRightInterval->right
			- pRightInterval->left;
}

bool minIntervalStrategy(t_Interval* pLeftInterval, t_Interval* pRightInterval,
		int iBlockIndex, int jBlockIndex)
{
	return pLeftInterval->right - pLeftInterval->left <= pRightInterval->right
			- pRightInterval->left;

}
