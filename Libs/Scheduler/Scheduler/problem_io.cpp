/*
 * problem_io.c
 *
 *  Created on: 30/ago/2011
 *      Author: jacopo
 */

#include <stdio.h>
#include <string.h>
#include "problem_io.h"

typedef struct struct_Attribute
{
	char name[30];
	char value[30];
} t_Attribute;

typedef struct struct_Tag
{
	char name[30];
	t_Attribute attributes[20];
	int iAttributeCount;

} t_Tag;

t_Tag* initTag(const char* name);
void addAttribute(t_Tag* pTag, const char* name, const char* value);
void addLongAttribute(t_Tag* pTag, const char* name, BMX_LONG value);
void addIntAttribute(t_Tag* pTag, const char* name, int value);
void addBoolAttribute(t_Tag* pTag, const char* name, bool value);
void startTag(FILE* pFile, t_Tag* pTag, bool end);
void endTag(FILE* pFile, const char* name);
void exportBlock(FILE* pFile, t_Block* pBlock);
void exportPrecedence(FILE* pFile, t_PrecedenceDef* pPrecedence);

//-----------------------------------------------------------

static char fg_line[1000];

static t_Tag fg_tag;

bool ProblemIO_export(t_Problem* pProblem, const char* fileName)
{
	int i;
	FILE* pFile;
	t_Tag* pProblemTag;

	pFile = fopen(fileName, "w");
	fprintf(pFile, "<?xml version=\"1.0\"?>\n");
	pProblemTag = initTag("problem");
	addLongAttribute(pProblemTag, "time-offset", pProblem->lTimeOffset);
	startTag(pFile, pProblemTag, false);

	for (i = 1; i < pProblem->iBlockCount; i++)
	{
		exportBlock(pFile, pProblem->blocks + i);
	}

	for (i = 0; i < pProblem->iPrecedenceCount; i++)
	{
		exportPrecedence(pFile, pProblem->precedenceDefs + i);
	}

	endTag(pFile, "problem");

	fclose(pFile);
	return true;
}

void exportBlock(FILE* pFile, t_Block* pBlock)
{
	t_Tag* pBlockTag;

	pBlockTag = initTag("event");
	addAttribute(pBlockTag, "name", pBlock->cEventTAG);
	addIntAttribute(pBlockTag, "event-tag", pBlock->iEventTAG);
	addIntAttribute(pBlockTag, "section-id", pBlock->iSectionID);
	addIntAttribute(pBlockTag, "slot-id", pBlock->iSlotID);
	addIntAttribute(pBlockTag, "event-id", pBlock->iEventID);
	addIntAttribute(pBlockTag, "resource-id", pBlock->iResourceID);
	addIntAttribute(pBlockTag, "action-id", pBlock->iActionID);
	addLongAttribute(pBlockTag, "length", pBlock->lLength);
	addBoolAttribute(pBlockTag, "planned", pBlock->planned);
	addLongAttribute(pBlockTag, "planned-time", pBlock->lPlannedTime);

	startTag(pFile, pBlockTag, true);
}

void exportPrecedence(FILE* pFile, t_PrecedenceDef* pPrecedence)
{
	t_Tag* pPrecedenceTag;

	pPrecedenceTag = initTag("precedence");
	addIntAttribute(pPrecedenceTag, "section-id", pPrecedence->iSectionId);
	addIntAttribute(pPrecedenceTag, "slot-id", pPrecedence->iSlotId);
	addIntAttribute(pPrecedenceTag, "event-id", pPrecedence->iEventId);
	addIntAttribute(pPrecedenceTag, "prev-section-id",
			pPrecedence->iPrevSectionId);
	addIntAttribute(pPrecedenceTag, "prev-slot-id", pPrecedence->iPrevSlotId);
	addIntAttribute(pPrecedenceTag, "prev-event-id", pPrecedence->iPrevEventId);
	addLongAttribute(pPrecedenceTag, "left-jitter", pPrecedence->interval.left);
	addLongAttribute(pPrecedenceTag, "right-jitter",
			pPrecedence->interval.right);

	startTag(pFile, pPrecedenceTag, true);

}

t_Tag* initTag(const char* name)
{
	strcpy(fg_tag.name, name);
	fg_tag.iAttributeCount = 0;
	return &fg_tag;
}

void addAttribute(t_Tag* pTag, const char* name, const char* value)
{
	t_Attribute* pAttr;

	pAttr = pTag->attributes + pTag->iAttributeCount;
	strcpy(pAttr->name, name);
	strcpy(pAttr->value, value);
	++pTag->iAttributeCount;
}

void addLongAttribute(t_Tag* pTag, const char* name, BMX_LONG value)
{
	t_Attribute* pAttr;

	pAttr = pTag->attributes + pTag->iAttributeCount;
	strcpy(pAttr->name, name);
	sprintf(fg_line, "%lld", value);
	strcpy(pAttr->value, fg_line);
	++pTag->iAttributeCount;
}

void addIntAttribute(t_Tag* pTag, const char* name, int value)
{
	t_Attribute* pAttr;

	pAttr = pTag->attributes + pTag->iAttributeCount;
	strcpy(pAttr->name, name);
	sprintf(fg_line, "%d", value);
	strcpy(pAttr->value, fg_line);
	++pTag->iAttributeCount;
}

void addBoolAttribute(t_Tag* pTag, const char* name, bool value)
{
	addAttribute(pTag, name, value ? "true" : "false");
}

void startTag(FILE* pFile, t_Tag* pTag, bool end)
{
	int i;

	fg_line[0] = 0;
	for (i = 0; i < pTag->iAttributeCount; ++i)
	{
		t_Attribute* pAttr = pTag->attributes + i;
		strcat(fg_line, pAttr->name);
		strcat(fg_line, "=\"");
		strcat(fg_line, pAttr->value);
		strcat(fg_line, "\" ");
	}
	if (end)
		strcat(fg_line, "/");

	fprintf(pFile, "<%s %s>\n", pTag->name, fg_line);
}

void endTag(FILE* pFile, const char* name)
{
	fprintf(pFile, "</%s>\n", name);
}

