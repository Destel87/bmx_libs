/*
 * workplan.h
 *
 *  Created on: 06/nov/2010
 *      Author: jacopo
 */

#ifndef WORKPLAN_H_
#define WORKPLAN_H_

#include "common.h"

/*!     Priority levels used into SectionNV structure                            */
#define SEC_PLEV_NOPRI    0    // NO  priority set
#define SEC_PLEV_LOW      1	   // LOW priority
#define SEC_PLEV_MED      2	   // MEDIUM priority
#define SEC_PLEV_HIGH     3	   // HIGH priority
/*!     Status of section used into SectionNV structure                          */
#define SEC_STAT_EMPTY    0	   // EMPTY section
#define SEC_STAT_TBSCHED  1	   // TO BE SCHEDULED
#define SEC_STAT_SCHED    2	   // SCHEDULED
#define SEC_STAT_INEXEC   3	   // in EXECUTION
#define SEC_STAT_COMPLETE 4    // Test COMPLETE
#define SEC_STAT_ERROR    5    // ERROR


/*! \struct t_ConstrainingEvent

 Struttura per eventi vincolati. \n
 E' utilizzata per sincronizzare due eventi su due diverse sezioni o su due diversi
 slots. L'evento di riferimento sara' sempre un evento precedente nel tempo; non e'
 previsto il caso contrario.
 */
/*********************************************************************************/
typedef struct _struct_ConstrainingEvent
{
	int iEventID; //!< ID of the reference event. Se negativo si riferisce al tempo iniziale delle sezioni
	//!<
	unsigned int iSectionID; //!< ID of the section (related to the reference event).
	//!<
	unsigned int iSlotID; //!< ID of the slot (related to the reference event).
	//!<
	BMX_ULONG lMinDelay; //!< Minimum delay time computed from the reference event [s/10].
	//!<
	BMX_ULONG lMaxDelay; //!< Maximum delay time computed from the reference event [s/10].
	//!<
} t_ConstrainingEvent;

/*********************************************************************************/
/*! \struct t_Event

 Definisce il singolo evento di impegno continuativo della testa di lettura o
 del pipettatore.
 */
/*********************************************************************************/
typedef struct _struct_Event
{
	char cEventTAG[50]; //!< String used to identify the event.
	//!< This parameter is used only for debug purposes
	//!< and shall be copied in the output event.
	//!<
	unsigned int iSectionID; //!< ID of the section.
	//!<
	unsigned int iSlotID; //!< ID of the slot.
	//!<
	unsigned int iEventID; //!< ID of the event.
	//!<
	unsigned int iResourceID; //!< Classification of the event
	//!< \arg 0, 1, ..., 99        NO resource engaged (used for syncronization purposes)
	//!< \arg 100, 101, ..., 199  Reading Event (optical head resource)
	//!< \arg 200, 201, ..., 299  Pre-treatment Event (APU resource). \n
	//!<
	unsigned int iActionID;

	BMX_ULONG lLength; //!< Temporal legth of the event (duration) (expressed in in s/10).
	//!<
	BMX_ULONG lPlannedStarTime; //!< output of the scheduler.\n
	//!< Scheduled Start Time (expressed in s/10).
	//!< Note: this parameters is used by the scheduler in case of re-run
	//!< when the status of the section is set to 2 (in execution)
	//!<
	unsigned int iConstrainingSize; //!< Size of the pointer EventConstraint (i.e. number of reference events).
	//!<
	t_ConstrainingEvent *constrainingEvent; //!< Pointer to one or more different events (references). \n
	//!< This information is used in order to synchronize
	//!< the event with previous ones.
	//!<
} t_Event;

/*********************************************************************************/
/*! \struct t_SectionNV

 Struttura contenente gli eventi temporali classificati per sezione
 */
/*********************************************************************************/
typedef struct _struct_SectionNV
{
	unsigned int iPriorityLevel; //!< Priority level of the Assay in set in the job list:
	//!< \arg \b 0 - NO priority set
	//!< \arg \b 1 - LOW priority
	//!< \arg \b 2 - MEDIUM priority
	//!< \arg \b 3 - HIGHT priority \n
	//!<

	unsigned int iStatus; //!< Status of the section:
	//!< \arg \b 0 - EMPTY (sezione vuota )
	//!< \arg \b 1 - TO BE SCHEDULED (deve essere schedulata)
	//!< \arg \b 2 - SCHEDULED  (schedulata in attesa di essere eseguita)
	//!< \arg \b 3 - in EXECUTION
	//!< \arg \b 4 - Test COMPLETED
	//!< \arg \b 5 - ERROR \n
	//!< Note: \n
	//!< status = 0 e status = 3 devono essere ignorati; \n
	//!< status = 2 risorsa bloccata;\n
	//!< status = 1 sezione da schedulare
	//!<
	unsigned int iSlotsUsed; //!< Number of slots used.
	//!<
	unsigned int iEventsPerSlot[NSL + 1]; //!< Number of events planned for each slots and section. \n
	//!<
	t_Event Event[NSL + 1][NEV]; //!< Array containing the structure that characterize each event

	int iConstraints;
//!<
} t_SectionNV;

/**
 * Definisce un conflitto tra risorse differenti quando eseguono determinate
 * azioni
 */
typedef struct _struct_resourceConflict
{
	int iResourceID1;
	int iActionID1;
	int iResourceID2;
	int iActionID2;
} t_ResourceConflict;

/*********************************************************************************/
/*! \struct t_WorkPlan

 Struttura contenente informazioni sul piano di lavoro completo della macchina
 (4 sezioni).
 */
/*********************************************************************************/
typedef struct _struct_WorkPlan
{
	BMX_ULONG lT_instrument; //!< Instrument timer (\a absolute \a time) [expressed in s/10]. \n
	//!< This value is used as reference for the scheduler and it is
	//!< referred to the intrument power on
	//!< (the parameter lT_instrument is set to zero at the boot of the instrument ).
	//!<
	unsigned int iT_Delay; //!< Total delay for Scheduling [expressed in s/10].\n
	//!< Note:\n
	//!< Questo delay e' calcolato a partide dall'istante in cui il controllo
	//!< torna al supervisor. La partenza del primo evento non potra' essere schedulato
	//!< per tempi inferiori a iT_Delay
	//!<
	t_SectionNV Section[NST]; //!< Structure containing information about sections.

	/**Array di t_ResourceConflict che definisce i conflitti tra risorse differenti*/
	t_ResourceConflict resourceConflicts[NRES*NACT*NRES*NACT];

	/**Numero di elementi nell'array resourceConflicts*/
	unsigned int iResourceConflictCount;

	//!<
} t_WorkPlan;

#endif /* WORKPLAN_H_ */
