/*
 * timeline.h
 *
 *  Created on: 06/nov/2010
 *      Author: jacopo
 */

#ifndef TIMELINE_H_
#define TIMELINE_H_

#include "common.h"

/*********************************************************************************/
/*! \struct t_PlannedEvent

 OUTPUT Event. \n
 Definisce l'output per il singolo evento. I tempi negli eventi in uscita sono espressi
 in decimi di secondo e si riferiscono al timer di sistema.
 */
/*********************************************************************************/
typedef struct _struct_PlannedEvent
{
	char cEventTAG[50]; //!< String used to identify the event.
	//!< (used only for debug purposes)
	int iEventTAG; //!< Integer used to identify the event.
	//!<
	unsigned int iSectionID; //!< ID of the section.
	//!<
	unsigned int iSlotID; //!< ID of the slot.
	//!<
	unsigned int iEventID; //!< ID of the event.

	///////////////////////////////////////////////////////////////////////////
	// NOTA: la documentazione di iResourceID non e' piu' valida! Vedere la
	// documentazione di Problem_addBlock
	///////////////////////////////////////////////////////////////////////////
	unsigned int iResourceID; //!< Classification of the event
	//!< \arg 0, 1, ..., 99        NO resource engaged (used for syncronization purposes)
	//!< \arg 100, 101, ..., 199  Reading Event (optical head resource)
	//!< \arg 200, 201, ..., 299  Pre-treatment Event (APU resource). \n
	//!<
	unsigned int iActionID;

	BMX_ULONG lLength; //!< Temporal legth of the event in decimi di secondo
	//!<
	BMX_ULONG lPlannedStarTime; //!< output of the scheduler\n.
	//!< Scheduled Start Time (expressed in s/10)
	//!<
} t_PlannedEvent;

/*********************************************************************************/
/*! \struct t_TimeLine

 Struttura di OUTPUT.\n
 La struttura e' organizzata come sequenza temporale di eventi
 */
/*********************************************************************************/
typedef struct _struct_TimeLine
{
	BMX_ULONG lStartSection[NST]; //!< Scheduled Start Time for each section. \n
	//!< This value is equal to the PlannedReading(0)
	//!< of the section 0.
	//!<
	BMX_ULONG lEndSection[NST]; //!< Scheduled End Time for each section. \n
	//!<
	unsigned int iEvents; //!< Number of events contained in the output structure
	//!<
	t_PlannedEvent PlannedEvent[NOUT]; //!< Array containing the structure that characterize each event
	//!<

} t_TimeLine;

#endif /* TIMELINE_H_ */
