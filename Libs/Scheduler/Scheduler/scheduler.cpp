/*
 * scheduler.c
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

//#include <INTEGRITY.h>
//#include <bsp.h>
#include <unistd.h>

#include "scheduler.h"
#include "problem_solver.h"
#include "startingjitter_strategies.h"
#include "ordering_strategies.h"
#include "datatypes.h"

typedef struct struct_Phase
{
	StartingJitterStrategyFn pStartingJitterStrategy;

	OrderingStrategyFn pOrderingStrategy;

	BMX_LONG timeout;

	bool stopOnSolution;

} t_Phase;

static time_t lastSysTime;

static unsigned long numCycles;
/**
 * Implementazione di default della stepFn, che usa time.
 */
void Scheduler_defaultStepFn(int phase, BMX_LONG timeout, bool solutionFound,
		bool* interruptionRequested)
{
	if (timeout > 0)
	{
		BMX_LONG diff;
		switch (phase)
		{
            case NEW_PHASE_STEP:
                time(&lastSysTime);
                numCycles=0;
                break;
            case EXECUTE_PHASE_STEP:
                
                numCycles++;
                /*if (numCycles>=timeout)
                    *interruptionRequested = true;*/

                diff = (BMX_LONG) difftime(time(0), lastSysTime) * 1000;
                if (diff > timeout)
                    *interruptionRequested = true;
                break;
            case END_PHASE_STEP:
                break;
		}
	}
}

/**true quando viene richiesto l'interruzione di fase*/
static bool fg_bInterruptionRequested;

/**true se viene trovata una soluzione*/
static bool fg_bSolutionFound;

/**Array delle soluzioni*/
static BMX_LONG fg_lSolution[NBLK];

/**Array degli indici ordinati dei blocchi della soluzione*/
static int fg_iOrderedSolutionIndexes[NBLK];

/**Array dei tempi di inizio degli eventi*/
static BMX_LONG fg_lEventStartValues[NBLK];

/**Pointer a funzione di controllo dell'algoritmo*/
static StepFn fg_stepFn;

/**Fase correntemente eseguita*/
static t_Phase* fg_pPhase;

/**Fasi definite dallo scheduler*/
static t_Phase fg_phases[] =
#if 0 // @@@@ 2011-05-27
{
{ relaxedJitterStrategy, minIntervalStrategy, 10000, false },//{ relaxedJitterStrategy, minIntervalStrategy, 2000, false },
{ halfMinLaneJitterStrategy, maxIntervalStrategy, 5000, true },//{ halfMinLaneJitterStrategy, maxIntervalStrategy, 1500, true },
{ minLaneJitterStrategy, maxIntervalStrategy, 5000, true },//{ minLaneJitterStrategy, maxIntervalStrategy, 1500, true },
{ relaxedJitterStrategy, maxIntervalStrategy, 10000, false },//{ relaxedJitterStrategy, maxIntervalStrategy, 2000, true },
{ relaxedJitterStrategy, maxIntervalStrategy, 0, true } };//{ relaxedJitterStrategy, maxIntervalStrategy, 100, true } };
#else
{
{ relaxedJitterStrategy, minIntervalStrategy, 6500, false }, //2000,  false 6000 20110916
//{ halfMinLaneJitterStrategy, maxIntervalStrategy, 7500, true }, //1500
//{ minLaneJitterStrategy, maxIntervalStrategy, 7500, true },     //1500
//{ relaxedJitterStrategy, maxIntervalStrategy, 2000, true },     //2000
{ relaxedJitterStrategy, maxIntervalStrategy, 1000, true } }; //500
#endif // @@@@ 2011-05-27

// sum of timeout are <= T_SCHEDULING 8000 -> maximum total time allowed for scheduling see scheduler.c


bool Scheduler_cancelRequested()
{
	fg_stepFn(EXECUTE_PHASE_STEP, fg_pPhase->timeout, fg_bSolutionFound,
			&fg_bInterruptionRequested);
	return fg_bInterruptionRequested;
}

void Scheduler_solutionNotify(t_Problem* pProblem, t_Matrix* pMatrix)
{
	if (!fg_bSolutionFound || ProblemSolver_compareSolution(pProblem, pMatrix,
			fg_lSolution) < 0)
	{
		fg_bSolutionFound = true;
		Matrix_getTimes(pMatrix, fg_lSolution);

		//printf("\nSolution found!\n");

		//Setto il flag di stop se la fase prevede che se ho una soluzione debba fermarsi
		if (fg_pPhase->stopOnSolution)
			fg_bInterruptionRequested = true;
	}
}

int Scheduler_compare_indexes_fn(const void *a, const void *b)
{
	int index1;
	int index2;

	//i valori sono gli indici
	index1 = *((const int*) a);
	index2 = *((const int*) b);
	return fg_lEventStartValues[index1] < fg_lEventStartValues[index2] ? -1
			: fg_lEventStartValues[index1] > fg_lEventStartValues[index2] ? 1 : 0;
}

void Scheduler_createOrderedSolutionIndexes(t_Problem* pProblem)
{
	int i;
	int size;

	size = pProblem->iBlockCount;
	for (i = 0; i < size; i++)
	{
		fg_iOrderedSolutionIndexes[i] = i;
		fg_lEventStartValues[i] = fg_lSolution[i] - pProblem->blocks[i].lLength;
	}

	//ordino gli indici per tempi (non ordino t*)
	qsort(fg_iOrderedSolutionIndexes + 1, size - 1, sizeof(int),
			Scheduler_compare_indexes_fn);
}

void Scheduler_copySolution(t_Problem* pProblem, t_TimeLine* pTimeLine)
{
	int i;
	int j;
	int k;
	BMX_LONG lMin;
	BMX_LONG lMax;
	BMX_LONG lEndTime;
	BMX_LONG lStartTime;
	t_PlannedEvent* pPlannedEvent;
	t_Block* pBlock;
	t_ProblemSlot* pProblemSlot;

	pTimeLine->iEvents = pProblem->iBlockCount - 1;

	//creo l'array degli indici ordinati dei blocchi in base ai tempi della soluzione
	Scheduler_createOrderedSolutionIndexes(pProblem);

	//riempio i planned events saltando t*
	for (i = 1; i < pProblem->iBlockCount; i++)
	{
		pPlannedEvent = pTimeLine->PlannedEvent + (i - 1);
		pBlock = pProblem->blocks + fg_iOrderedSolutionIndexes[i];
		strcpy(pPlannedEvent->cEventTAG, pBlock->cEventTAG);
		pPlannedEvent->iEventTAG = pBlock->iEventTAG;
		pPlannedEvent->iEventID = pBlock->iEventID;
		pPlannedEvent->iResourceID = pBlock->iResourceID;
		pPlannedEvent->iActionID = pBlock->iActionID;
		pPlannedEvent->iSectionID = pBlock->iSectionID;
		pPlannedEvent->iSlotID = pBlock->iSlotID;
		pPlannedEvent->lLength = pBlock->lLength;
		pPlannedEvent->lPlannedStarTime
				= fg_lSolution[fg_iOrderedSolutionIndexes[i]] - pBlock->lLength
						+ pProblem->lTimeOffset;
	}

	//riempio lStartSection e lEndSection
	for (i = 0; i < NST; ++i)
	{
		//mi calcolo il max e min dei blocchi contenuti in NSL + 1 slots (1 sezione)
		lMin = BMX_LONG_MAX;
		lMax = BMX_LONG_MIN;
		for (j = 0; j < NSL + 1; j++)
		{
			pProblemSlot = pProblem->problemSlots + (i * (NSL + 1) + j);
			for (k = 0; k < pProblemSlot->iBlockCount; ++k)
			{
				pBlock = pProblemSlot->pBlocks[k];
				lEndTime = fg_lSolution[pBlock->iIndex];
				lStartTime = lEndTime - pBlock->lLength;
				if (lStartTime < lMin)
					lMin = lStartTime;
				if (lEndTime > lMax)
					lMax = lEndTime;
			}
		}
		//se max non si e' modificata significa che la sezione non contiene eventi
		if (lMax > BMX_LONG_MIN)
		{
			pTimeLine->lStartSection[i] = lMin + pProblem->lTimeOffset;
			pTimeLine->lEndSection[i] = lMax + pProblem->lTimeOffset;
		}
		else
		{
			pTimeLine->lStartSection[i] = 0;
			pTimeLine->lEndSection[i] = 0;
		}

	}
}

bool Scheduler_scheduleWorkPlan(t_WorkPlan* pWorkPlan, t_TimeLine* pTimeLine,
		t_ErrorData* pErrorData, StepFn step)
{
	t_Problem problem;

	//si carica il problema
	if (!Problem_loadWorkPlan(&problem, pWorkPlan, pErrorData))
		return false;

	return Scheduler_scheduleProblem(&problem, pTimeLine, pErrorData, step);
}

bool Scheduler_scheduleProblem(t_Problem* pProblem, t_TimeLine* pTimeLine,
		t_ErrorData* pErrorData, StepFn step)
{
	int iSize;
	int i;
	int iResponse;
	t_ProblemSolver solver;

	iSize = sizeof(fg_phases) / sizeof(t_Phase);
	fg_bSolutionFound = false;
    fg_stepFn = ((step == 0) ? Scheduler_defaultStepFn : step);

	iResponse = INTERRUPTED;

	//si itera sulle varie fasi
	for (i = 0; i < iSize && iResponse != ERROR && !fg_bSolutionFound; i++)
	{
		fg_pPhase = fg_phases + i;
//        printf("Phase %d of %d - Timeout=%lld\n", (i + 1), iSize, fg_pPhase->timeout);

		fg_bInterruptionRequested = false;

		solver.pCancelRequested = Scheduler_cancelRequested;
		solver.pStartingJitterStrategy = fg_pPhase->pStartingJitterStrategy;
		solver.pOrderingStrategy = fg_pPhase->pOrderingStrategy;
		solver.pSolutionNotify = Scheduler_solutionNotify;

		//chiamo lo step
		fg_stepFn(NEW_PHASE_STEP, fg_pPhase->timeout, fg_bSolutionFound,
				&fg_bInterruptionRequested);

		//chiama lo scheduler
		iResponse = ProblemSolver_solve(&solver, pProblem, pErrorData);

//        printf("\n>>> Scheduler_scheduleProblem : found:%d Step:%d iResponse:%d numCycles:%ld\n", fg_bSolutionFound,i,iResponse,numCycles);
        
		//chiamo lo step
		fg_stepFn(END_PHASE_STEP, fg_pPhase->timeout, fg_bSolutionFound,
				&fg_bInterruptionRequested);

	}

	//se ho finito le fasi e non ho trovato soluzione, allora
	//ho errore di soluzione non trovata
	if (!fg_bSolutionFound && iResponse != ERROR)
	{
		pErrorData->iErrorCode = NO_SOLUTION_FOUND_ERROR;
        strcpy(pErrorData->pErrorMessage, "No solution found");
		iResponse = ERROR;
//        printf("\nSchedule failed\n");
    }
	else
	{
		//si copia la soluzione nei risultati
		Scheduler_copySolution(pProblem, pTimeLine);
//        printf("\nSchedule completed\n");
    }

    return (iResponse != ERROR);
}

inline const char* Scheduler_getVersion()
{
#ifdef DEBUG
	return VER_DBG;
#else
	return VERSION;
#endif
}

inline int Scheduler_getSectionCount()
{
	return NST;
}

inline int Scheduler_getSlotPerSectionCount()
{
	return NSL;
}

inline int Scheduler_getMaxEventPerSlotCount()
{
	return NEV;
}

inline int Scheduler_getMaxConstraintPerEventCount()
{
	return NCNS;
}

inline int Scheduler_getResourceCount()
{
	return NRES;
}

inline int Scheduler_getMaxActionPerResourceCount()
{
	return NACT;
}
