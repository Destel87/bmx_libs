/*
 * problem_io.h
 *
 *  Created on: 30/ago/2011
 *      Author: jacopo
 */

#ifndef PROBLEM_IO_H_
#define PROBLEM_IO_H_

#include "problem.h"

/**
 * Esporta il problema in un formato XML
 * @param pProblem Problema da esportare
 * @param fileName Nome del file
 * @return true se l'export va a buon fine, false altrimenti
 */
bool ProblemIO_export(t_Problem* pProblem, const char* fileName);

#endif /* PROBLEM_IO_H_ */
