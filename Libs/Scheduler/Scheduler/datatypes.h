#ifndef DATATYPES_H_
#define DATATYPES_H_

#include <stdint.h>

#define BMX_LONG int64_t
#define BMX_ULONG uint64_t
#define BMX_LONG_MAX INT64_MAX
#define BMX_LONG_MIN INT64_MIN

#endif
