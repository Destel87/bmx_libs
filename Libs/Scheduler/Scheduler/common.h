/*
 * common.h
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdbool.h>
#include <limits.h>
#include "datatypes.h"
#include <stdio.h>

#include "../../../../mbfw/CommonInclude.h"

/**Numero di versione*/
#define VERSION "2.06"
#define VER_DBG "2.06 debug"

//******************************************************************
//*	Codici di ritorno
//********************************************************************
#define OK			0
#define INTERRUPTED	1
#define ERROR		2

//******************************************************************
//*	Codici di errore
//********************************************************************

/**Errore interno non verificabile*/
#define INTERNAL_ERROR 1

/**
 * Errore che si verifica se ci sono vincoli in conflitto (il problema non e' valido)
 * Il campo data della struttura di errore è un puntatore a t_Block che individua
 * il blocco in conflitto
 */
#define CONFLICTING_CONSTRAINTS_ERROR 2

/**Errore che si verifica quando non viene trovata alcuna soluzione al problema*/
#define NO_SOLUTION_FOUND_ERROR 3

/**Errore che si verifica quando si specifica un id di un evento non esistente */
#define EVENT_NOT_FOUND_ERROR 4

/**Errore che si verifica quando ci sono troppi eventi in totale*/
#define TOO_MANY_EVENTS_ERROR 5

/**Errore che si verifica quando ci sono troppi eventi per uno slot*/
#define TOO_MANY_SLOT_EVENTS_ERROR 6

/**Errore che si verifica quando ci sono troppe precedenze per un blocco o in totale*/
#define TOO_MANY_PRECEDENCES_ERROR 7

/**Errore che si verifica quando si specifica una sezione il cui ID e' troppo grande*/
#define TOO_HIGH_SECTION_ID 8

/**Errore che si verifica quando si specifica uno slot il cui ID e' troppo grande*/
#define TOO_HIGH_SLOT_ID 9

/**Errore che si verifica quando si specifica una risorsa il cui ID e' troppo grande*/
#define TOO_HIGH_RESOURCE_ID 10

/**Errore che si verifica quando si specifica una azione il cui ID e' troppo grande*/
#define TOO_HIGH_ACTION_ID 11

/**Errore che si verifica se si inseriscono due eventi con lo stesso ID (sectionID + slotID + eventID)*/
#define DUPLICATE_ID_ERROR 12


//******************************************************************
//*	Macro di utilita'
//********************************************************************
#define min(a,b) (a) < (b) ? (a) : (b)
#define max(a,b) (a) > (b) ? (a) : (b)

//******************************************************************
//*	Macro per il dimensionamento dei vettori
//********************************************************************
/*!     Total number of sections of the instrument                               */
#define NST          SCT_NUM_TOT_SECTIONS

/*!     Total number of Slot for each section                                    */
#define NSL          SCT_NUM_TOT_SLOTS

/*!     Total number of temporal events for each slot                            */
#define NEV          29//<-- 20120718 ..26//20120704 <--25//20120418

/**Numero totale di vincoli di precedenza*/
#define NCNS         5

/**Numero totale di risorse */
#define NRES         2

/**Numero totale di azioni possibili per risorsa*/
#define NACT		 4

/*!     Total number of temporal events in output                                */
#define NOUT         NST * (NSL+1) * NEV

/**Definisco la dim massima dei blocchi come la dim massima degli eventi piu' il blocco a tempo 0*/
#define NBLK (NOUT + 1)

/**Dimensione massima della matrice delle differenze*/
#define NMTX NBLK * NBLK

/**Numero totale di slot (NSL + 1) poiche' c'e' lo slot fittizio*/
#define NTSL (NSL + 1) * NST

/**Massima profondita' dell'albero di ricerca*/
#define NDPTH NEV * NEV * NTSL * (NTSL - 1) / 2

/*!     Resource IDs                                                             */
#define RESID_SYNC_BASE   0    // Base of sync IDs
#define RESID_READ_BASE   100  // Base of reading IDs
#define RESID_APU_BASE    200  // Base of pre-treatment (APU) IDs
/**
 * Struttura che definisce un errore
 */
typedef struct struct_ErrorData
{
	/**Codice dell'errore*/
	short iErrorCode;

	/**Messaggio errore*/
    char pErrorMessage[128];

	/**
	 * Dati aggiuntivi dell'errore. Cambiano in valore e tipo a seconda del tipo
	 * di errore. Consultare la documentazione dei codici di errore per sapere
	 * il significato
	 */
	int data;

} t_ErrorData;

/**
 * Definisce un intervallo temporale
 */
typedef struct struct_Interval
{

	/**Estremo sinistro dell'intervallo*/
	BMX_LONG left;

	/**Estremo destro dell'intervallo*/
	BMX_LONG right;

} t_Interval;

#endif /* COMMON_H_ */
