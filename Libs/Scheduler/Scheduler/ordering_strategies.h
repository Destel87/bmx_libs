/*
 * ordering_strategies.h
 *
 *  Created on: 19/mag/2010
 *      Author: jacopo
 */

#ifndef ORDERING_STRATEGIES_H_
#define ORDERING_STRATEGIES_H_

#include "problem_solver.h"

/**
 * Riporta il numero di ordering strategy disponibili
 */
int OrderingStrategies_getOrderingStrategyCount();

/**
 * Riporta la strategia di ordinamento intervallo dato l'indice
 */
OrderingStrategyFn OrderingStrategies_getOrderingStrategy(int index);

/**
 * Riporta il nome dell'ordering strategy dato l'indice
 */
const char* OrderingStrategies_getOrderingStrategyName(int index);



bool maxIntervalStrategy(t_Interval* pLeftInterval,
			t_Interval* pRightInterval, int iBlockIndex, int jBlockIndex);

bool minIntervalStrategy(t_Interval* pLeftInterval,
			t_Interval* pRightInterval, int iBlockIndex, int jBlockIndex);


#endif /* ORDERING_STRATEGIES_H_ */
