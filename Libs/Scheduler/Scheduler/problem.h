/*
 * problem.h
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#ifndef PROBLEM_H_
#define PROBLEM_H_

#include "block.h"
#include "workplan.h"
#include "timeline.h"

/**
 * Definisce gli eventi in una sezione
 */
typedef struct struct_ProblemSlot
{
	/**Numero di eventi nella sezione*/
	int iBlockCount;

	/**Priorita' della sezione*/
	int iPriority;

	/**Eventi della sezione*/
	t_Block* pBlocks[NEV];

} t_ProblemSlot;

/**
 * Rappresenta una definizione di precedenza
 */
typedef struct struct_PrecedenceDef
{
	int iSectionId;
	int iSlotId;
	int iEventId;
	int iPrevSectionId;
	int iPrevSlotId;
	int iPrevEventId;
	t_Interval interval;

} t_PrecedenceDef;

/**
 * Definisce un problema di scheduling da risolvere
 */
typedef struct struct_Problem
{

	/** Numero di blocchi nell'array blocks */
	int iBlockCount;

	/** Numero totale di precedenze registrate */
	int iPrecedenceCount;

	/** Array di blocchi che definiscono il problema */
	t_Block blocks[NBLK];

	/** Array di definizioni di precedenze */
	t_PrecedenceDef precedenceDefs[NCNS * NBLK];

	/** Tutti gli slot. Serve per la ricerca per sezione e slot */
	t_ProblemSlot problemSlots[NTSL];

	/** Time offset del problema */
	BMX_LONG lTimeOffset;

	/**Matrice di conflitti risorsa-azione*/
	bool conflictMatrix[NRES * NACT	* NRES * NACT];

} t_Problem;

/**
 * Inizializza e carica il problema da un work plan. Questa funzione inizializza
 * il problema, per cui non e' necessario invocare la funzione Problem_initialize
 * @param pProblem Problema di inizializzare
 * @param pWorkPlan Work plan da caricare.
 * @param pErrorData Error data
 * @return false se si verifica un errore, true altrimenti
 */bool Problem_loadWorkPlan(t_Problem* pProblem, t_WorkPlan* pWorkPlan,
		t_ErrorData* pErrorData);

/**
 * Inizializza la struttura dati del problema. Questa funzione deve essere chiamata
 * prima di qualsiasi altra che involve un'istanza di problema
 *
 * @param pProblem Problema da inizializzare
 * @param lTimeOffset Offset assoluto del tempo. Deve tener conto anche del tempo
 * 					  di calcolo della macchina
 */
void Problem_startConfig(t_Problem* pProblem, BMX_LONG lTimeOffset);

/**
 * Aggiunge un blocco al problema. Questa funzione puo' essere chiamata solo
 * dopo aver chiamato una volta la funzione Problem_startConfig.
 *
 * @param pProblem Problema a cui aggiungere un blocco
 * @param cEventTAG Tag dell'evento
 * @param iEventTAG Altro Tag numerico dell'evento.
 * @param iSectionID Id della sezione. Le sezioni devono essere numerate sequenzialmente a partire da 0
 * @param iSlotID Id dello slot. Gli slot devono essere numerati sequenzialmente a partire da 0.
 * 		  Uno slot a scelta deve identificare gli eventi associati a tutta la sezione
 * @param iEventID Id dell'evento. La numerazione degli eventi e' del tutto libera. L'unica assunzione
 *        che si fa e' che non esistano eventi con lo stesso all'interno dello stesso slot
 * @param iResourceID Id della risorsa. Le risorse devono essere numerate a partire da 1. Lo 0
 *        identifica l'assenza di risorsa
 * @param iActionID Id dell'azione. Le azioni devono essere numerate a partire da 0
 * @param lLength Lunghezza dell'evento
 * @param planned Se vale true il blocco e' gia' schedulato
 * @param plannedTime Considerato solo se planned e' true. Rappresenta il tempo di
 *        schedulazione calcolato in precedenza. E' il tempo assoluto (comprende il tempo di macchina)
 * @param pErrorData Struttura per la memorizzazione dell'eventuale errore
 * @return il blocco nell'array dei blocchi del problema o null se si verifica un errore
 */
t_Block* Problem_addBlock(t_Problem* pProblem, const char* cEventTAG, int iEventTAG,
		int iSectionID, int iSlotID, int iEventID, int iResourceID,
		int iActionID, BMX_LONG lLength, bool planned, BMX_LONG plannedTime,
		t_ErrorData* pErrorData);

/**
 * Imposta la priorita' di uno slot. Questo metodo puo' essere chiamato in qualsiasi
 * momento, non necessariamente dopo Problem_startConfig.
 * @param pProblem Problema
 * @param iSectionID Indice della sezione
 * @param iSlotID Indice dello slot
 * @param iPriority Priorita' da impostare allo slot
 */
void Problem_setSlotPriority(t_Problem* pProblem, int iSectionID, int iSlotID,
		int iPriority);

/**
 * Aggiunge una vincolo di precedenza tra un blocco e un altro blocco. Il vincolo
 * deve essere interpretato in questo modo: se al blocco i aggiungo un vincolo
 * di precedenza col blocco j con un intervallo di tempo minT,maxT significa che il blocco
 * i deve seguire il blocco j con un tempo minimo pari a minT e un tempo massimo pari a maxT
 * Questa funzione puo' essere chiamata solo dopo aver chiamato una volta
 * la funzione Problem_startConfig. Non e' invece necessario aver inserito i blocchi a cui
 * la precedence si riferisce prima della chiamata di questa funzione. In effetti questa
 * funzione non aggiunge la precedenza vera e propria, bensi' una sua definizione.
 * Al momento della chiamata alla funzione Problem_endConfig le precedenze vengono
 * risolte e correttamente inserite.
 *
 * @param pProblem Problema in oggetto
 * @param iSection Id della sezione del blocco a cui aggiungere il vincolo
 * @param iSlot Id dello slot del blocco a cui aggiungere il vincolo
 * @param iEvent Id dell'evento del blocco a cui aggiungere il vincolo
 * @param iPredSection Id della sezione del blocco che ha precedenza
 * @param iPredSlot Id dello slot del blocco che ha precedenza
 * @param iPredEvent Id dell'evento del blocco che ha precedenza
 * @param lMinDelay Valore minimo dell'intervallo di precedenza
 * @param lMaxDelay Valore massimo dell'intervallo di precedenza
 * @param pErrorData Struttura per la memorizzazione dell'eventuale errore
 * @return La struttura che identifica la definizione della precedenza o null se si verifica un errore
 */
t_PrecedenceDef* Problem_addPrecedence(t_Problem* pProblem, int iSection,
		int iSlot, int iEvent, int iPredSection, int iPredSlot, int iPredEvent,
		BMX_LONG lMinDelay, BMX_LONG lMaxDelay, t_ErrorData* pErrorData);

/**
 * Aggiunge una vincolo tra un blocco e il tempo di inizio della schedulazione. Il vincolo
 * deve essere interpretato in questo modo: se aggiungo un vincolo
 * con l'inizio della traccia con un intervallo di tempo minT,maxT significa che il blocco
 * deve iniziare dopo un tempo minimo pari a minT e un tempo massimo pari a maxT dall'
 * inizio della schedulazione (o rischedulazione)
 * Questa funzione puo' essere chiamata solo dopo aver chiamato una volta
 * la funzione Problem_startConfig. Non e' invece necessario aver inserito il blocco a cui
 * la precedence si riferisce prima della chiamata di questa funzione. In effetti questa
 * funzione non aggiunge la precedenza vera e propria, bensi' una sua definizione.
 * Al momento della chiamata alla funzione Problem_endConfig le precedenze vengono
 * risolte e correttamente inserite.
 *
 * @param pProblem Problema in oggetto
 * @param iSection Id della sezione del blocco a cui aggiungere il vincolo
 * @param iSlot Id dello slot del blocco a cui aggiungere il vincolo
 * @param iEvent Id dell'evento del blocco a cui aggiungere il vincolo
 * @param lMinDelay Valore minimo dell'intervallo di precedenza
 * @param lMaxDelay Valore massimo dell'intervallo di precedenza
 * @param pErrorData Struttura per la memorizzazione dell'eventuale errore
 * @return La struttura che identifica la definizione della precedenza o null se si verifica un errore
 */
t_PrecedenceDef* Problem_addStartingConstraint(t_Problem* pProblem,
		int iSection, int iSlot, int iEvent, BMX_LONG lMinDelay,
		BMX_LONG lMaxDelay, t_ErrorData* pErrorData);

/**
 * Come Problem_addScheduledProblemWithOffset con offset pari all'offset del problema
 * @param pProblem Problema
 * @param pTimeLine Timeline
 * @param pErrorData Struttura di errore
 * @return false se si verifica un errore, true altrimenti
 */bool Problem_addScheduledProblem(t_Problem* pProblem, t_TimeLine* pTimeLine,
		t_ErrorData* pErrorData);

/**
 * Questa funzione permette di aggiungere rapidamente un problema precedentemente
 * schedulato ad un nuovo problema. Si passanto il nuovo problema e la timeline
 * contenente i risultati della prima schedulazione. Al problema pProblem verranno
 * aggiunti soltanto gli eventi il cui tempo di fine e' maggiore del tempo di
 * offset passato. Questa funzione puo' essere chiamata
 * solo dopo aver chiamato una volta la funzione Problem_startConfig.
 * La funzione puo' essere chiamata indifferentemente prima o dopo aver inserito
 * i nuovi eventi del nuovo problema.
 * @param pProblem Nuovo problema a cui aggiungere gli eventi schedulati precedentemente
 * @param pTimeLine Time line contenente le soluzioni della schedulazione del
 * problema precedente
 * @param lOffset Offset di tempo prima del quale gli eventi vengono scartati
 * @param pErrorData Struttura per la memorizzazione dell'eventuale errore
 * @return true se la funzione va a buon fine, false se si verifica un errore.
 */bool Problem_addScheduledProblemWithOffset(t_Problem* pProblem,
		t_TimeLine* pTimeLine, BMX_LONG lOffset, t_ErrorData* pErrorData);

/**
 * Da chiamare dopo aver inserito tutti i blocchi e le precedenze. Esegue
 * le operazioni finali di calcolo del problema, quali l'aggiunta delle
 * precedenze fittizie sul blocco a tempo 0.
 *
 * @param pProblem Problema
 * @param pErrorData Struttura per la memorizzazione dell'eventuale errore
 * @return true se il metodo va a buon fine, false altrimenti
 */bool Problem_endConfig(t_Problem* pProblem, t_ErrorData* pErrorData);

/**
 * Aggiunge la definizione di un conflitto tra la risorsa 1 e la risorsa 2
 * quando eseguono rispettivamente l'azione1 e l'azione2.
 * Tramite questa definizione, due risorse sono in conflitto quando eseguono
 * le azioni specificate.
 * @param pProblem Problema a cui aggiungere la definizione
 * @param iResourceID1 ID della risorsa 1
 * @param iActionID1 ID dell'azione 1
 * @param iResourceID2 ID della risorsa 2
 * @param iActionID2 ID dell'azione 2
 * @param pErrorData Struttura per gli errori
 * @return false se si verifica errore, true altrimenti
 */bool Problem_addResourceConflict(t_Problem* pProblem, int iResourceID1,
		int iActionID1, int iResourceID2, int iActionID2,
		t_ErrorData* pErrorData);

///////////////////////////////////////////////////////////////////////////////
// Seguono funzioni di package di Problem
///////////////////////////////////////////////////////////////////////////////

/**
 * Calcola gli absolute times di ogni blocco associato al problema
 * @param pProblem Problema
 * @param pErrorData Struttura per il ritorno dei messaggi d'errore. La struttura viene
 * 					 modificata solo se la funzione riporta false
 * @return true se non ci sono errori, false altrimenti
 */bool Problem_calcBlockAbsoluteTimes(t_Problem* pProblem,
		t_ErrorData* pErrorData);

/**
 * Imposta lo start jitter per tutte le sezioni/slot. Dopo questa funzione
 * gli absolute time di tutti i blocchi viene azzerato per cui e' necessario
 * utilizzare la funzione Problem_calcAbsoluteTimes per inizializzarli
 * @param pProblem Problema
 * @param lStartJitter Start jitter
 */
void Problem_setStartJitter(t_Problem* pProblem, BMX_LONG lStartJitter);

/**
 * Cerca il ProblemSlot corrispondente alla sezione e slot specificati
 * @param pProblem Problema
 * @param iSectionID Id sezione
 * @param iSlotID Id slot
 */
t_ProblemSlot* Problem_getProblemSlot(t_Problem* pProblem, int iSectionID,
		int iSlotID);

/**
 * Trova il blocco con sezione, slot e event specificati. Riporta null
 * se non trovato
 * @param pProblem Problema
 * @param iSectionID Id sezione
 * @param iSlotID Id slot
 * @param iEventID Id evento
 * @return Blocco cercato o null
 */
t_Block* Problem_findBlock(t_Problem* pProblem, int iSectionID, int iSlotID,
		int iEventID);

/**
 * Riporta true se i due blocchi utilizzano risorse che entrano in conflitto
 * @param pProblem Problema
 * @param pBlock1 Primo blocco
 * @param pBlock2 Secondo blocco
 * @return true se i blocchi entrano in conflitto, false altrimenti
 */bool Problem_testResourceConflict(t_Problem* pProblem, t_Block* pBlock1,
		t_Block* pBlock2);

#endif /* PROBLEM_H_ */
