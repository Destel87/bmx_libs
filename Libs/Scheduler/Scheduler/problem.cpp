/*
 * problem.c
 *
 *  Created on: 14/mag/2010
 *      Author: jacopo
 */

#include <string.h>

#include "problem.h"


#include <unistd.h>

#define _SCHEDULER_DEBUG

void Problem_startConfig(t_Problem* pProblem, BMX_LONG lTimeOffset)
{
	int i;
	t_Block* pStartBlock;
	t_ErrorData errorData;

	// aggiungo il blocco iniziale che rappresenta t*
	pProblem->iBlockCount = 0;
	pProblem->iPrecedenceCount = 0;
	pProblem->lTimeOffset = lTimeOffset;
	memset(pProblem->conflictMatrix, 0, sizeof(pProblem->conflictMatrix));

	pStartBlock = Problem_addBlock(pProblem, "", 0, -1, -1, -1, 0, 0, 0, false,
			0, &errorData);
	pStartBlock->absoluteTime.left = 0;
	pStartBlock->absoluteTime.right = 0;
	pStartBlock->absoluteTimeSet = true;

	//inizializzo le strutture accessorie per la ricerca
	for (i = 0; i < NTSL; ++i)
	{
		pProblem->problemSlots[i].iBlockCount = 0;
	}

}

t_Block* Problem_addBlock(t_Problem* pProblem, const char* cEventTAG,
		int iEventTAG, int iSectionID, int iSlotID, int iEventID,
		int iResourceID, int iActionID, BMX_LONG lLength, bool planned,
		BMX_LONG plannedTime, t_ErrorData* pErrorData)
{
	int iIndex;
	t_Block* pBlock;
	t_Block* pStartBlock;
	t_ProblemSlot* pProblemSlot;
	BMX_LONG relTime;

#ifdef SCHEDULER_DEBUG
	fprintf(console,"AddBlock: tag[%s].sct[%d].slot[%d].event[%d].dur[%lld]\n",
						cEventTAG,iSectionID,iSlotID,iEventID,lLength);
#endif
	//errore se supero il numero totale di blocchi
	if (pProblem->iBlockCount >= NOUT)
	{
		pErrorData->iErrorCode = TOO_MANY_EVENTS_ERROR;
		strcpy(pErrorData->pErrorMessage, "Too many total events");
		return 0;
	}

	//errore se id sezione troppo grande
	if (iSectionID >= NST)
	{
		pErrorData->iErrorCode = TOO_HIGH_SECTION_ID;
		strcpy(pErrorData->pErrorMessage, "Too high section ID");
		return 0;
	}

	//errore se id slot troppo grande (si ammette NSL perche' rappresenta la sezione unica)
	if (iSlotID > NSL)
	{
		pErrorData->iErrorCode = TOO_HIGH_SLOT_ID;
		strcpy(pErrorData->pErrorMessage, "Too high slot ID");
		return 0;
	}

	//errore se la risorsa ha id troppo elevato (si ammette NRES poiche' la risorsa 0 significa niente risorsa)
	if (iResourceID > NRES)
	{
		pErrorData->iErrorCode = TOO_HIGH_RESOURCE_ID;
		strcpy(pErrorData->pErrorMessage, "Too high resource ID");
		return 0;
	}

	//errore se l'azione ha id troppo elevato
	if (iActionID >= NACT)
	{
		pErrorData->iErrorCode = TOO_HIGH_ACTION_ID;
		strcpy(pErrorData->pErrorMessage, "Too high action ID");
		return 0;
	}

	//errore se il blocco ha id duplicato (solo se non e' lo starting block)
	if (iSectionID >= 0 && Problem_findBlock(pProblem, iSectionID, iSlotID,
			iEventID) != 0)
	{
		pErrorData->iErrorCode = DUPLICATE_ID_ERROR;
		strcpy(pErrorData->pErrorMessage, "Duplicate id");
		return 0;
	}

	//individuo il blocco su cui scrivere
	pBlock = pProblem->blocks + pProblem->iBlockCount;
	if (cEventTAG != 0)
		strcpy(pBlock->cEventTAG, cEventTAG);
	else
		pBlock->cEventTAG[0] = 0;
	pBlock->iEventTAG = iEventTAG;
	pBlock->iSectionID = iSectionID;
	pBlock->iSlotID = iSlotID;
	pBlock->iEventID = iEventID;
	pBlock->iResourceID = iResourceID;
	pBlock->iActionID = iActionID;
	pBlock->lLength = lLength;
	pBlock->absoluteTime.left = 0;
	pBlock->absoluteTime.right = 0;
	pBlock->absoluteTimeSet = false;
	pBlock->iPrecedenceCount = 0;
	pBlock->planned = planned;
	pBlock->lPlannedTime = plannedTime;
	pBlock->iIndex = pProblem->iBlockCount;

	//se e' un blocco planned devo aggiungere una precedenza col blocco t*
	if (planned)
	{
		//devo traslare il tempo indietro del tempo assoluto
		relTime = plannedTime - pProblem->lTimeOffset;
		pStartBlock = pProblem->blocks;
		Block_addPrecedence(pBlock, pStartBlock, relTime, relTime, false);
	}

	//incremento il conto dei blocchi
	++pProblem->iBlockCount;

	//aggiorno la struttura accessoria per le ricerche solo se non e' t*
	if (iSectionID >= 0)
	{
		iIndex = iSectionID * (NSL + 1) + iSlotID;
		pProblemSlot = pProblem->problemSlots + iIndex;
		if (pProblemSlot->iBlockCount >= NEV)
		{
			pErrorData->iErrorCode = TOO_MANY_SLOT_EVENTS_ERROR;
			strcpy(pErrorData->pErrorMessage, "Too many events for a slot");
			return 0;
		}
		pProblemSlot->pBlocks[pProblemSlot->iBlockCount] = pBlock;
		pProblemSlot->iBlockCount++;
		pProblemSlot->iPriority = 0;
	}
	return pBlock;
}

void Problem_setSlotPriority(t_Problem* pProblem, int iSectionID, int iSlotID,
		int iPriority)
{
	int iIndex;
	t_ProblemSlot* pProblemSlot;

	iIndex = iSectionID * (NSL + 1) + iSlotID;
	pProblemSlot = pProblem->problemSlots + iIndex;
	pProblemSlot->iPriority = iPriority;

}

t_PrecedenceDef* Problem_addPrecedence(t_Problem* pProblem, int iSection,
		int iSlot, int iEvent, int iPredSection, int iPredSlot, int iPredEvent,
		BMX_LONG lMinDelay, BMX_LONG lMaxDelay, t_ErrorData* pErrorData)
{
	t_PrecedenceDef* pPrecedenceDef;

	//errore se supero il numero totale di precedenze
	if (pProblem->iPrecedenceCount >= NCNS * NBLK)
	{
		pErrorData->iErrorCode = TOO_MANY_PRECEDENCES_ERROR;
		strcpy(pErrorData->pErrorMessage, "Too many total precedences");
		return 0;
	}

	pPrecedenceDef = pProblem->precedenceDefs + pProblem->iPrecedenceCount;

	pPrecedenceDef->iSectionId = iSection;
	pPrecedenceDef->iSlotId = iSlot;
	pPrecedenceDef->iEventId = iEvent;
	pPrecedenceDef->iPrevSectionId = iPredSection;
	pPrecedenceDef->iPrevSlotId = iPredSlot;
	pPrecedenceDef->iPrevEventId = iPredEvent;
	pPrecedenceDef->interval.left = lMinDelay;
	pPrecedenceDef->interval.right = lMaxDelay;
	++pProblem->iPrecedenceCount;
	/////////////////
	#ifdef SCHEDULER_DEBUG
		fprintf(console,"AddPrec : sct[%d].slot[%d].event[%d]->prevsct[%d].[%d].[%d]->min[%lld].max[%lld]\n",
					iSection,iSlot,iEvent,iPredSection,iPredSlot,iPredEvent,lMinDelay,lMaxDelay);
	#endif
	return pPrecedenceDef;
}

t_PrecedenceDef* Problem_addStartingConstraint(t_Problem* pProblem,
		int iSection, int iSlot, int iEvent, BMX_LONG lMinDelay,
		BMX_LONG lMaxDelay, t_ErrorData* pErrorData)
{
	return Problem_addPrecedence(pProblem, iSection, iSlot, iEvent, -1, -1, -1,
			lMinDelay, lMaxDelay, pErrorData);
}

bool Problem_addScheduledProblem(t_Problem* pProblem, t_TimeLine* pTimeLine,
		t_ErrorData* pErrorData)
{
	return Problem_addScheduledProblemWithOffset(pProblem, pTimeLine,
			pProblem->lTimeOffset, pErrorData);
}

bool Problem_addScheduledProblemWithOffset(t_Problem* pProblem,
		t_TimeLine* pTimeLine, BMX_LONG lOffset, t_ErrorData* pErrorData)
{
	unsigned int i;

	t_PlannedEvent* pPlannedEvent;

	//recupero tutte le soluzioni del problema precedente
	for (i = 0; i < pTimeLine->iEvents; ++i)
	{
		pPlannedEvent = pTimeLine->PlannedEvent + i;

		//devo aggiungere soltanto i blocchi la cui fine e' oltre il tempo di inizio
		//del nuovo problema
		if (pPlannedEvent->lPlannedStarTime + pPlannedEvent->lLength > lOffset)
		{
			if (Problem_addBlock(pProblem, pPlannedEvent->cEventTAG,
					pPlannedEvent->iEventTAG, pPlannedEvent->iSectionID,
					pPlannedEvent->iSlotID, pPlannedEvent->iEventID,
					pPlannedEvent->iResourceID, pPlannedEvent->iActionID,
					pPlannedEvent->lLength, true,
					pPlannedEvent->lPlannedStarTime, pErrorData) == 0)
				return false;
		}
	}

	return true;
}

bool Problem_endConfig(t_Problem* pProblem, t_ErrorData* pErrorData)
{
	int i;
	int j;
	t_Block* pBlock;
	t_Block* pStartBlock;
	t_Block* pPrevBlock;
	t_PrecedenceDef* pPrecDef;
	t_Precedence* pPrecedence;
	BMX_LONG lMaxTime;

	//inserisco tutte le precedenze dalle definizioni di precedenza
	for (i = 0; i < pProblem->iPrecedenceCount; ++i)
	{
		pPrecDef = pProblem->precedenceDefs + i;
		pBlock = Problem_findBlock(pProblem, pPrecDef->iSectionId,
				pPrecDef->iSlotId, pPrecDef->iEventId);

        if (pBlock == NULL)
		{
			printf("Precedence Block == NULL\n");
			pErrorData->iErrorCode = EVENT_NOT_FOUND_ERROR;
			strcpy(pErrorData->pErrorMessage, "Invalid problem: next event not found in constraint");
			return false;
		}

		//controllo che non siano troppe precedenze
		if (pBlock->iPrecedenceCount >= NCNS)
		{
			pErrorData->iErrorCode = TOO_MANY_PRECEDENCES_ERROR;
			strcpy(pErrorData->pErrorMessage, "Too many precedences in a block");
			return false;
		}

		//le precedenze si aggiungono solo per i blocchi non planned
		if (!pBlock->planned)
		{
			pPrevBlock = Problem_findBlock(pProblem, pPrecDef->iPrevSectionId,
					pPrecDef->iPrevSlotId, pPrecDef->iPrevEventId);

			if (pPrevBlock == NULL)
			{
				pErrorData->iErrorCode = EVENT_NOT_FOUND_ERROR;
				strcpy(pErrorData->pErrorMessage, "Invalid problem: previous event not found in constraint");
				return false;
			}
			Block_addPrecedence(pBlock, pPrevBlock, pPrecDef->interval.left,
					pPrecDef->interval.right, false);
		}
	}

	//devo calcolare un sicuro maggiorante (come somma di tutti i vincoli e lunghezze dei blocchi)
	//mi serve per aggiungere i vincoli iniziale dello starting jitter
	lMaxTime = 0;
	for (i = 1; i < pProblem->iBlockCount; ++i)
	{
		pBlock = pProblem->blocks + i;
		lMaxTime += pBlock->lLength;
		for (j = 0; j < pBlock->iPrecedenceCount; ++j)
		{
			pPrecedence = pBlock->precedences + j;
			lMaxTime += pPrecedence->jitter.right;
		}
	}

	//aggiungo ai blocchi senza precedenza un vincolo sull'evento iniziale fittizio.
	for (i = 1; i < pProblem->iBlockCount; ++i)
	{
		pBlock = pProblem->blocks + i;
		if (pBlock->iPrecedenceCount == 0)
		{
			pStartBlock = pProblem->blocks;
			//imposto una precedenza con t* di un valore alto. in un secondo tempo verranno impostati
			//al loro valore corretto (startJitterStrategy) ma solo per i blocchi non planned
			//Nota che non si deve impostare un valore stretto a 0 poiche' potrebbe esserci un evento successivo
			//che dipende in maniera stretta da questo e che e' vincolato ad un altro evento che lo porta a
			//stare piu' in avanti nel tempo. Questo blocco viene quindi "trascinato" in avanti
			//dal secondo blocco rendendo il problema non risolvibile.
			Block_addPrecedence(pBlock, pStartBlock, 0, lMaxTime, true);

		}
	}

	return true;
}

bool Problem_calcBlockAbsoluteTimes(t_Problem* pProblem,
		t_ErrorData* pErrorData)
{
	int i;

	//prima di tutto si calcolano i tempi assoluti dati i vincoli
	//si parte dal blocco 1, il primo e' gia' impostato
	for (i = 1; i < pProblem->iBlockCount; ++i)
	{
		if (!Block_calcAbsoluteTime(pProblem->blocks + i, pErrorData))
			return false;
	}

	return true;
}

void Problem_setStartJitter(t_Problem* pProblem, BMX_LONG lStartJitter)
{
	int i;
	int j;
	t_Block* pBlock;
	t_Precedence* pPrec;

	//parto da 1 poiche' salto il primo blocco relativo a t*
	for (i = 1; i < pProblem->iBlockCount; ++i)
	{
		pBlock = pProblem->blocks + i;
		// azzero l'absolute time perché deve essere ricalcolato
		pBlock->absoluteTime.left = -1;
		pBlock->absoluteTime.right = -1;
		for (j = 0; j < pBlock->iPrecedenceCount; ++j)
		{
			pPrec = pBlock->precedences + j;

			//se la precedence e' di tipo startJitter e il blocco attuale non e' planned
			//imposto il jitter
			if (pPrec->bStartJitter && !pBlock->planned)
			{
				pPrec->jitter.left = 0;
				pPrec->jitter.right = lStartJitter;
			}
		}
	}
}

bool loadProblem(t_Problem* pProblem, t_WorkPlan* pWorkPlan,
		t_ErrorData* pErrorData)
{
	int iSection;
	int iSlot;
	int iEvent;
	int iEventCount;
	bool planned;
	BMX_LONG lStartTime;
	t_SectionNV* pSection;
	t_Event* pEvent;
	t_ProblemSlot* pProblemSlot;
	t_Block* pBlock;

	//carico per primi i blocchi
	for (iSection = 0; iSection < NST; iSection++)
	{
		pSection = pWorkPlan->Section + iSection;

		//TODO verificare la scelta di questi due status
		if (pSection->iStatus == SEC_STAT_TBSCHED || pSection->iStatus
				== SEC_STAT_SCHED || pSection->iStatus == SEC_STAT_INEXEC)
		{
			//TODO verificare il significato di iSlotsUsed (se c'e' pretratt e protoc vale 4?)
			//tra l'altro se uso la 2a e 3a sezione anziche la 1a e 2a?
			for (iSlot = 0; iSlot < NSL + 1; iSlot++)
			{
				iEventCount = pSection->iEventsPerSlot[iSlot];
				for (iEvent = 0; iEvent < iEventCount; iEvent++)
				{
					//e' uno starting block se non ha vincoli all'indietro
					pEvent = &pSection->Event[iSlot][iEvent];

					//TODO verificare i valori di test per iStatus
					planned = pSection->iStatus != SEC_STAT_TBSCHED;

					//lo start time deve tener conto del tempo dello strumento
					//e del delay di calcolo
					lStartTime = planned ? pEvent->lPlannedStarTime : 0;

					pBlock = Problem_addBlock(pProblem, pEvent->cEventTAG, 0,
							pEvent->iSectionID, pEvent->iSlotID,
							pEvent->iEventID, pEvent->iResourceID,
							pEvent->iActionID, pEvent->lLength, planned,
							lStartTime, pErrorData);
					if (pBlock == 0)
						return false;
				}

				//aggiorno le priorita'
				pProblemSlot
						= Problem_getProblemSlot(pProblem, iSection, iSlot);
				pProblemSlot->iPriority = pSection->iPriorityLevel;
			}
		}
	}

	return true;
}

bool loadPrecedences(t_Problem* pProblem, t_WorkPlan* pWorkPlan,
		t_ErrorData* pErrorData)
{
	int iSection;
	int iSlot;
	int iEvent;
	unsigned int iConst;
	int iEventCount;
	t_SectionNV* pSection;
	t_Event* pEvent;
	t_ConstrainingEvent *pConstEvent;

	//Carico le precedenze
	for (iSection = 0; iSection < NST; iSection++)
	{
		pSection = pWorkPlan->Section + iSection;
		if (pSection->iStatus == SEC_STAT_TBSCHED || pSection->iStatus
				== SEC_STAT_SCHED) //TO BE SCHEDULED, SCHEDULED, IN EXECUTION
		{
			for (iSlot = 0; iSlot < NSL + 1; iSlot++)
			{
				iEventCount = pSection->iEventsPerSlot[iSlot];
				for (iEvent = 0; iEvent < iEventCount; iEvent++)
				{
					pEvent = &pSection->Event[iSlot][iEvent];
					for (iConst = 0; iConst < pEvent->iConstrainingSize; ++iConst)
					{
						pConstEvent = pEvent->constrainingEvent + iConst;
						if (pConstEvent->iEventID >= 0)
						{
							//il vincolo e' verso un altro evento
							if (Problem_addPrecedence(pProblem, iSection,
									iSlot, iEvent, pConstEvent->iSectionID,
									pConstEvent->iSlotID,
									pConstEvent->iEventID,
									pConstEvent->lMinDelay,
									pConstEvent->lMaxDelay, pErrorData) == 0)
								return false;
						}
						else
						{
							//il vincolo e' rispetto al tempo iniziale
							if (Problem_addStartingConstraint(pProblem,
									iSection, iSlot, iEvent,
									pConstEvent->lMinDelay,
									pConstEvent->lMaxDelay, pErrorData) == 0)
								return false;
						}
					}
				}
			}
		}
	}
	return true;
}

bool loadResourceConflicts(t_Problem* pProblem, t_WorkPlan* pWorkPlan,
		t_ErrorData* pErrorData)
{
	unsigned int i;
	t_ResourceConflict* pResConfl;

	for (i = 0; i < pWorkPlan->iResourceConflictCount; ++i)
	{
		pResConfl = pWorkPlan->resourceConflicts + i;
		if (!Problem_addResourceConflict(pProblem, pResConfl->iResourceID1,
				pResConfl->iActionID1, pResConfl->iResourceID2,
				pResConfl->iActionID2, pErrorData))
			return false;
	}

	return true;
}

bool Problem_loadWorkPlan(t_Problem* pProblem, t_WorkPlan* pWorkPlan,
		t_ErrorData* pErrorData)
{
	//si inizializza il problema
	Problem_startConfig(pProblem,
			pWorkPlan->lT_instrument + pWorkPlan->iT_Delay);

	//si caricano gli eventi
	if (!loadProblem(pProblem, pWorkPlan, pErrorData))
		return false;

	//si caricano i vincoli
	if (!loadPrecedences(pProblem, pWorkPlan, pErrorData))
		return false;

	if (!loadResourceConflicts(pProblem, pWorkPlan, pErrorData))
		return false;

	//si finalizza la configurazione
	return Problem_endConfig(pProblem, pErrorData);
}

t_ProblemSlot* Problem_getProblemSlot(t_Problem* pProblem, int iSectionID,
		int iSlotID)
{
	return pProblem->problemSlots + (iSectionID * (NSL + 1) + iSlotID);
}

t_Block* Problem_findBlock(t_Problem* pProblem, int iSectionID, int iSlotID,
		int iEventID)
{
	int i;
	t_ProblemSlot* pProblemSlot;
	t_Block* pBlock;

	//se sectionId e' negativo sto cercando il blocco iniziale t*
	if (iSectionID < 0)
		return pProblem->blocks;

	pProblemSlot = Problem_getProblemSlot(pProblem, iSectionID, iSlotID);

	for (i = 0; i < pProblemSlot->iBlockCount; ++i)
	{
		pBlock = pProblemSlot->pBlocks[i];
		if (pBlock->iEventID == iEventID)
			return pBlock;
	}

	return 0;
}

inline int Problem_getConflictMatrixRow(int iResourceID, int iActionID)
{
	return (iResourceID - 1) * NACT + iActionID;
}

inline int Problem_getConflictMatrixIndex(int iResourceID1, int iActionID1,
		int iResourceID2, int iActionID2)
{
	return Problem_getConflictMatrixRow(iResourceID1, iActionID1) * NRES * NACT
			+ Problem_getConflictMatrixRow(iResourceID2, iActionID2);
}

bool Problem_testResourceConflict(t_Problem* pProblem, t_Block* pBlock1,
		t_Block* pBlock2)
{
	if (pBlock1->iResourceID == 0 || pBlock2->iResourceID == 0)
		return false;
	if (pBlock1->iResourceID == pBlock2->iResourceID)
		return true;

	return pProblem->conflictMatrix[Problem_getConflictMatrixIndex(
			pBlock1->iResourceID, pBlock1->iActionID, pBlock2->iResourceID,
			pBlock2->iActionID)];
}

bool Problem_addResourceConflict(t_Problem* pProblem, int iResourceID1,
		int iActionID1, int iResourceID2, int iActionID2,
		t_ErrorData* pErrorData)
{
	pProblem->conflictMatrix[Problem_getConflictMatrixIndex(iResourceID1,
			iActionID1, iResourceID2, iActionID2)] = true;

	return true;
}

