/*
 * problem_solver.h
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#ifndef PROBLEM_SOLVER_H_
#define PROBLEM_SOLVER_H_

#include "common.h"
#include "matrix.h"
#include "problem.h"

/**
 * Definisce un pointer a funzione che riporta true se e' stata richiesta l'interruzione del calcolo
 * @return true se e' stato richiesta l'interruzione del calcolo
 */
typedef bool (*CancelRequestedFn)(void);

/**
 * Pointer a funzione per la notifica di una soluzione.
 * @param pProblem Problema associato
 * @param pMatrix Matrice soluzione
 */
typedef void (*SolutionNotifyFn)(t_Problem*, t_Matrix*);

/**
 * Pointer a funzione per il calcolo degli starting jitter
 * @param pProblem Problema
 * @param pBaseMatrix Matrice di base
 * @return starting jitter
 */
typedef BMX_LONG (*StartingJitterStrategyFn)(t_Problem* pProblem,
		t_Matrix* pBaseMatrix);

/**
 * Pointer a funzione per l'ordinamento degli intervalli di conflitto da
 * selezionare nella ricerca delle soluzioni.
 * @param pLeftInterval Left interval di validita' del conflitto
 * @param pRightInterval Right interval di validita' del conflitto
 * @param iBlockIndex Indice del blocco i che crea conflitto
 * @param jBlockIndex Indice del blocco j che crea conflitto
 * @return true se sono ordinati correttamente (prima si seleziona left, poi right),
 *   	   false se vanno invertiti
 */
typedef bool (*OrderingStrategyFn)(t_Interval* pLeftInterval,
		t_Interval* pRightInterval, int iBlockIndex, int jBlockIndex);

/**
 * Definisce un vincolo aggiunto all'albero di ricerca
 */
typedef struct structMatrixConstraint
{

	/**Intervallo destro e sinistro del vincolo*/
	t_Interval intervals[2];

	/**Numero di intervalli validi in intervals*/
	int intervalCount;

	/**Indice dell'intervallo corrente*/
	int currentIntervalIndex;

	/**Indice i nella matrice*/
	int iIndex;

	/**Indice j nella matrice*/
	int jIndex;

} t_MatrixConstraint;

/**
 * Definisce un solutore di un problema Problem
 */
typedef struct struct_ProblemSolver
{
	CancelRequestedFn pCancelRequested;

	SolutionNotifyFn pSolutionNotify;

	StartingJitterStrategyFn pStartingJitterStrategy;

	OrderingStrategyFn pOrderingStrategy;

} t_ProblemSolver;

/**
 * Esegue il solver sul problema passato
 * @param pProblemSolver Solver del problema. La struttura deve essere inizializzata
 * @param pProblem Problema. La struttura deve essere inizializzata
 * @param pErrorData Struttura per il ritorno dei messaggi d'errore. La struttura viene
 * 					 modificata solo se la funzione riporta false
 * @return Codice di ritorno OK, INTERRUPTED, ERROR
 */
int ProblemSolver_solve(t_ProblemSolver* pProblemSolver, t_Problem* pProblem,
		t_ErrorData* pErrorData);

/**
 * Calcola la matrice di base, tenendo conto solo dei vincoli di sequenza.
 * Non viene impostato il jitter iniziale
 * @param pProblem problema di cui calcolare la base matrix
 * @param pErrorData Error data
 * @return La matrice di base o null se si verifica un errore
 */
t_Matrix* ProblemSolver_calcBaseMatrix(t_Problem* pProblem,
		t_ErrorData* pErrorData);

/**
 * Compara la matrice con le soluzioni precedenti. Riporta un valore <0 se la matrice
 * ha una valutazione migliore rispetto a quella delle soluzioni
 * @param pProblem Problema
 * @param pMatrix Matrice da confrontare con le soluzioni precedenti
 * @param lSolutions Array di soluzioni precedenti
 */
int ProblemSolver_compareSolution(t_Problem* pProblem, t_Matrix* pMatrix,
		BMX_LONG* lSolutions);

#endif /* PROBLEM_SOLVER_H_ */
