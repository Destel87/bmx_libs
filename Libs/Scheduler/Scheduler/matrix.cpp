/*
 * matrix.c
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */
#include <string.h>
#include <stdio.h>
#include "matrix.h"
#include "datatypes.h"

inline BMX_LONG Matrix_getValueImpl(t_Matrix* pMatrix, int i, int j)
{
	return pMatrix->values[i * pMatrix->iVarCount + j];
}

inline void Matrix_setValueImpl(t_Matrix* pMatrix, int i, int j,
		BMX_LONG lValue)
{
	pMatrix->values[i * pMatrix->iVarCount + j] = lValue;
}

void Matrix_simplifyMatrix(t_Matrix* pMatrix)
{
	int i;
	int j;
	int k;
	int iIndex;
	int jIndex;
	int kIndex;
	int size;

	//TODO Verificare che questo metodo, se chiamato piu' volte aggiunga le nuove variabili dipendenti
	//senza modificare quelle presenti

	size = pMatrix->iNormIndexesCount;
	for (iIndex = 0; iIndex < size - 1; iIndex++)
	{
		i = pMatrix->normIndexes[iIndex];
		for (jIndex = iIndex + 1; jIndex < size; jIndex++)
		{
			j = pMatrix->normIndexes[jIndex];
			if (Matrix_getValueImpl(pMatrix, i, j)
					== -Matrix_getValueImpl(pMatrix, j, i))
			{
				//aggiungo la dipendenza
				pMatrix->depIndexesMap[i] = j;

				for (kIndex = 0; kIndex < size; kIndex++)
				{
					k = pMatrix->normIndexes[kIndex];
					if (k != i && k != j)
					{
						if (Matrix_getValueImpl(pMatrix, k, j)
								> Matrix_getValueImpl(pMatrix, k, i)
										+ Matrix_getValueImpl(pMatrix, i, j))
						{
							Matrix_setValueImpl(
									pMatrix,
									k,
									j,
									Matrix_getValueImpl(pMatrix, k, i)
											+ Matrix_getValueImpl(pMatrix, i, j));
						}
						if (Matrix_getValueImpl(pMatrix, j, k)
								> Matrix_getValueImpl(pMatrix, j, i)
										+ Matrix_getValueImpl(pMatrix, i, k))
						{
							Matrix_setValueImpl(
									pMatrix,
									j,
									k,
									Matrix_getValueImpl(pMatrix, j, i)
											+ Matrix_getValueImpl(pMatrix, i, k));
						}
					}
				}

				for (k = 0; k < i; k++)
				{
					if (pMatrix->depIndexesMap[k] == i)
					{
						pMatrix->depIndexesMap[k] = j;
						//aggiorno il coefficiente di relazione tra tk e tj
						Matrix_setValueImpl(
								pMatrix,
								k,
								j,
								Matrix_getValueImpl(pMatrix, i, j)
										+ Matrix_getValueImpl(pMatrix, k, i));
					}
				}
				break;
			}
		}
	}

	//ricostruisco il vettore delle variabili indipendenti
	pMatrix->iNormIndexesCount = 0;
	for (i = 0; i < pMatrix->iVarCount; i++)
		if (pMatrix->depIndexesMap[i] == i)
			pMatrix->normIndexes[pMatrix->iNormIndexesCount++] = i;

//		printf("Variabili indipendenti: %d:\n", pMatrix->iNormIndexesCount);
//		for (i = 0; i < pMatrix->iNormIndexesCount; i++)
//			printf("%d,  ", pMatrix->normIndexes[i]);
//
//		printf("\nVariabili dipendenti:\n");
//
//		for (i = 0; i < pMatrix->iVarCount; i++)
//			if (pMatrix->depIndexesMap[i] != i)
//				printf("%d -> %d\n", i, pMatrix->depIndexesMap[i]);
}

inline void Matrix_initialize(t_Matrix* pMatrix, t_Problem* pProblem)
{
	int i;
	int j;
	BMX_LONG lValue;

	pMatrix->iVarCount = pProblem->iBlockCount;
	pMatrix->iNormIndexesCount = pProblem->iBlockCount;
	for (i = 0; i < pProblem->iBlockCount; ++i)
	{
		pMatrix->normIndexes[i] = i;
		pMatrix->depIndexesMap[i] = i;
	}

	//inizializzo la matrice dai valori del problema
	// riempio la matrice con i valori dati dalle differenze dei tempi assoluti
	for (i = 0; i < pProblem->iBlockCount; ++i)
	{
		for (j = 0; j < pProblem->iBlockCount; ++j)
		{
			//i valori sulla diagonale valgono 0
			if (i == j)
				lValue = 0;
			else
			{
				//gli altri vengono calcolati come differenze degli intervalli dei
				//tempi assoluti delle variabili: [a,b] - [c,d] = [a-d,b-c]
				lValue = pProblem->blocks[i].absoluteTime.right
						- pProblem->blocks[j].absoluteTime.left;
			}

			Matrix_setValueImpl(pMatrix, i, j, lValue);
		}
	}

}

inline void Matrix_setValue(t_Matrix* pMatrix, int i, int j, BMX_LONG lValue)
{
	int h;
	int k;

	h = pMatrix->depIndexesMap[i];
	k = pMatrix->depIndexesMap[j];

	Matrix_setValueImpl(
			pMatrix,
			h,
			k,
			lValue + Matrix_getValueImpl(pMatrix, j, k)
					- Matrix_getValueImpl(pMatrix, i, h));
}

inline BMX_LONG Matrix_getValue(t_Matrix* pMatrix, int i, int j)
{
	int h;
	int k;

	/* Per calcolare un valore Ti-Tj si considera il seguente calcolo:
	 * - si considerano gli indici h,k dati da h=depIndexesMap[i] e k=depIndexesMap[j]
	 *   dove h=i se i e' indipendente e k=j se j e' indipendente per costruzione.
	 * - quindi Bij = Bhk + Bih - Bjk (si noti che Bih = 0 se i=h o, equivalentemente,
	 *   Bjk = 0 se j=k)*/

	h = pMatrix->depIndexesMap[i];
	k = pMatrix->depIndexesMap[j];

	return Matrix_getValueImpl(pMatrix, h, k)
			+ Matrix_getValueImpl(pMatrix, i, h)
			- Matrix_getValueImpl(pMatrix, j, k);
}

void Matrix_normalize(t_Matrix* pMatrix)
{
	int i;
	int j;
	int k;
	int iIndex;
	int jIndex;
	int kIndex;
	int iVarCount;
	BMX_LONG lValue;

	iVarCount = pMatrix->iNormIndexesCount;
	for (kIndex = 0; kIndex < iVarCount; ++kIndex)
	{
		k = pMatrix->normIndexes[kIndex];
		for (iIndex = 0; iIndex < iVarCount; ++iIndex)
		{
			i = pMatrix->normIndexes[iIndex];
			for (jIndex = 0; jIndex < iVarCount; ++jIndex)
			{
				j = pMatrix->normIndexes[jIndex];
				lValue =
						min( Matrix_getValueImpl( pMatrix, i, j ), Matrix_getValueImpl( pMatrix, i, k )
								+ Matrix_getValueImpl( pMatrix, k, j ) );
				Matrix_setValueImpl(pMatrix, i, j, lValue);
			}
		}
	}
}

void Matrix_normalizeAfterChange(t_Matrix* pMatrix, int i, int j)
{
	int k;
	int l;
	int kIndex;
	int lIndex;
	int iVarCount;
	BMX_LONG sum;

	//gli indici i e j vanno riadattati ai veri indici modificati da setValue
	i = pMatrix->depIndexesMap[i];
	j = pMatrix->depIndexesMap[j];

	iVarCount = pMatrix->iNormIndexesCount;
	for (kIndex = 0; kIndex < iVarCount; ++kIndex)
	{
		k = pMatrix->normIndexes[kIndex];
		sum = Matrix_getValueImpl(pMatrix, k, i)
				+ Matrix_getValueImpl(pMatrix, i, j);
		if (k != i && k != j && Matrix_getValueImpl(pMatrix, k, j) > sum)
		{
			Matrix_setValueImpl(pMatrix, k, j, sum);
			for (lIndex = 0; lIndex < iVarCount; ++lIndex)
			{
				l = pMatrix->normIndexes[lIndex];
				sum = Matrix_getValueImpl(pMatrix, k, i)
						+ Matrix_getValueImpl(pMatrix, i, j)
						+ Matrix_getValueImpl(pMatrix, j, l);
				if (l != i && l != j && l != k
						&& Matrix_getValueImpl(pMatrix, k, l) > sum)
				{
					Matrix_setValueImpl(pMatrix, k, l, sum);
				}
			}
		}
	}

	for (lIndex = 0; lIndex < iVarCount; ++lIndex)
	{
		l = pMatrix->normIndexes[lIndex];
		sum = Matrix_getValueImpl(pMatrix, i, j)
				+ Matrix_getValueImpl(pMatrix, j, l);
		if (l != i && l != j && Matrix_getValueImpl(pMatrix, i, l) > sum)
			Matrix_setValueImpl(pMatrix, i, l, sum);
	}

}

void Matrix_addConstraint(t_Matrix* pMatrix, int i, int j, BMX_LONG lLeftValue,
		BMX_LONG lRightValue, bool bNormalize)
{
	if (lRightValue < Matrix_getValue(pMatrix, i, j))
	{
		Matrix_setValue(pMatrix, i, j, lRightValue);
		if (bNormalize)
			Matrix_normalizeAfterChange(pMatrix, i, j);
	}
	if (-lLeftValue < Matrix_getValue(pMatrix, j, i))
	{
		Matrix_setValue(pMatrix, j, i, -lLeftValue);
		if (bNormalize)
			Matrix_normalizeAfterChange(pMatrix, j, i);
	}
}

inline void Matrix_copyInto(t_Matrix* pDestMatrix, t_Matrix* pSourceMatrix)
{
	memcpy(
			pDestMatrix->values,
			pSourceMatrix->values,
			pSourceMatrix->iVarCount * pSourceMatrix->iVarCount
					* sizeof(BMX_LONG));
	pDestMatrix->iVarCount = pSourceMatrix->iVarCount;

	//copio le informazioni di raggruppamento delle variabili
	pDestMatrix->iNormIndexesCount = pSourceMatrix->iNormIndexesCount;
	memcpy(pDestMatrix->normIndexes, pSourceMatrix->normIndexes,
			pSourceMatrix->iNormIndexesCount * sizeof(int));
	memcpy(pDestMatrix->depIndexesMap, pSourceMatrix->depIndexesMap,
			pSourceMatrix->iVarCount * sizeof(int));

}

inline void Matrix_getTimeInterval(t_Matrix* pMatrix, int varIndex,
		t_Interval* pInterval)
{
	pInterval->left = -Matrix_getValue(pMatrix, 0, varIndex);
	pInterval->right = Matrix_getValue(pMatrix, varIndex, 0);
}

inline void Matrix_getTimeDifferenceInterval(t_Matrix* pMatrix, int i, int j,
		t_Interval* pInterval)
{
	pInterval->left = -Matrix_getValue(pMatrix, j, i);
	pInterval->right = Matrix_getValue(pMatrix, i, j);
}

BMX_LONG Matrix_getWorstTime(t_Matrix* pMatrix)
{
	int i;
	BMX_LONG lValue;
	BMX_LONG lRet;

	lRet = BMX_LONG_MAX;
	for (i = 0; i < pMatrix->iVarCount; ++i)
	{
		lValue = Matrix_getValue(pMatrix, 0, i);
		if (lValue < lRet)
			lRet = lValue;
	}

	return -lRet;
}

BMX_LONG getWorstTime(BMX_LONG* times, int size)
{
	int i;
	BMX_LONG lValue;
	BMX_LONG lRet;

	lRet = 0;
	for (i = 0; i < size; ++i)
	{
		lValue = times[i];
		if (lValue > lRet)
			lRet = lValue;
	}

	return lRet;
}

inline BMX_LONG Matrix_getTime(t_Matrix* pMatrix, int varIndex)
{
	return -Matrix_getValue(pMatrix, 0, varIndex);
}

BMX_LONG Matrix_getTimeSum(t_Matrix* pMatrix)
{
	int i;
	BMX_LONG lRet;

	lRet = 0;
	for (i = 0; i < pMatrix->iVarCount; i++)
	{
		lRet += Matrix_getTime(pMatrix, i);
	}
	return lRet;
}

BMX_LONG getTimeSum(BMX_LONG* times, int size)
{
	int i;
	BMX_LONG lRet;

	lRet = 0;
	for (i = 0; i < size; i++)
	{
		lRet += times[i];
	}
	return lRet;
}

inline int compareTo(BMX_LONG v1, BMX_LONG v2)
{
	return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

int Matrix_compareTo(t_Matrix* pMatrix, BMX_LONG* times)
{
	int iRet;

	//si comparano il tempo peggiore.

	iRet = compareTo(Matrix_getWorstTime(pMatrix),
			getWorstTime(times, pMatrix->iVarCount));

	//a parita di tempi peggiori si sceglie quella con la somma dei tempi piu' piccola
	if (iRet == 0)
		return compareTo(Matrix_getTimeSum(pMatrix),
				getTimeSum(times, pMatrix->iVarCount));
	return iRet;
}

void Matrix_getTimes(t_Matrix* pMatrix, BMX_LONG* times)
{
	int i;

	for (i = 0; i < pMatrix->iVarCount; i++)
	{
		times[i] = Matrix_getTime(pMatrix, i);
	}
}

bool Matrix_validate(t_Matrix* pMatrix)
{
	int i;

	for (i = 0; i < pMatrix->iVarCount; i++)
		if (Matrix_getValueImpl(pMatrix, i, i) != 0)
			return false;
	return true;
}
