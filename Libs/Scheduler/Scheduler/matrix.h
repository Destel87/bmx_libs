/*
 * matrix.h
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include "common.h"
#include "problem.h"



/**
 * Definisce la matrice delle differenze sui tempi assoluti delle variabili
 */
typedef struct struct_Matrix
{

	/**Numero di variabili (numero di righe o di colonne)*/
	int iVarCount;

	/**Array di valori*/
	BMX_LONG values[NMTX];

	//Dalla versione 2.0
	/**
	 * Il vettore normIndexes contiene gli indici nella matrice delle variabili
	 * indipendenti. E' un vettore sequenziale di dimensione iNormIndexesCount che
	 * viene ricorstruito ogni volta che si introduce una variabile dipendente.
	 * Il vettore depIndexesMap e' una mappa che associa ad ogni indice di variabile
	 * dipendente l'indice della variabile da cui dipende. La sua dimensione e' pari
	 * alla dimensione della matrice e i valori contenuti negli indici delle variabili
	 * indipendenti e' inizializzato al proprio indice.
	 * Ogni volta che si individua una nuova variabile dipendente ( (Ti-Tj)=-(Tj-Ti) con
	 * i e j variabili entrambe indipendenti) si inserisce in depIndexesMap[i] il valore j.
	 * In questo modo la mappa depIndexesMap contiene, per ogni indice, l'indice stesso
	 * per le variabili indipendenti, o l'indice della variabile da cui dipende
	 * nel caso in cui sia dipendente.
	 * Dopo di che si ricostruisce normIndexes inserendo sequenzialmente gli indici k
	 * per cui depIndexesMap[k] = k (variabili indipendenti).
	 * Per normalizzare si utilizzano solo gli indici di normIndexes.
	 * Per calcolare un valore Ti-Tj si considera il seguente calcolo:
	 * - si considerano gli indici h,k dati da h=depIndexesMap[i] e k=depIndexesMap[j]
	 *   dove h=i se i e' indipendente e k=j se j e' indipendente.
	 * - quindi Bij = Bhk + Bih - Bjk (si noti che Bih = 0 se i=h o, equivalentemente,
	 *   Bjk = 0 se j=k)
	 *  Allo stesso modo si procede per impostare il valore Bij. In questo caso
	 *  il valore reale che verra' impostato è Bhk, calcolato dalla precedente
	 *  equazione.
	 *  Quando si normalizza parzialmente, si considerano gli indici h,k anziche'
	 *  i e j.
	 */

	/** Array di indici delle righe e colonne da normalizzare*/
	int normIndexes[NBLK];

	/** Numero di elementi nel vettore normIndexes*/
	int iNormIndexesCount;

	/**Mappa indice della variabile dipendente - indice della variabile indipendente*/
	int depIndexesMap[NBLK];

} t_Matrix;

/**
 * Inizializza la matrice tramite la sua dimensione
 * @param pMatrix Matrice da inizializzare
 * @param pProblem Problema associato da cui inizializzare i valori della matrice
 */
void Matrix_initialize(t_Matrix* pMatrix, t_Problem* pProblem);

/**
 * Imposta il valore della matrice agli indici specificati
 * @param pMatrix Matrice da inizializzare
 * @param i Indice riga della matrice
 * @param j Indice colonna della matrice
 * @param lValue Valore da impostare
 */
void Matrix_setValue(t_Matrix* pMatrix, int i, int j, BMX_LONG lValue);

/**
 * Riporta il valore della matrice agli indici specificati
 * @param pMatrix Matrice da inizializzare
 * @param i Indice riga della matrice
 * @param j Indice colonna della matrice
 * @return Valore contenuto nella matrice
 */
BMX_LONG Matrix_getValue(t_Matrix* pMatrix, int i, int j);

/**
 * Normalizza la matrice tramite l'algoritmo di Floyd-Warshall
 * @param pMatrix matrice da normalizzare
 */
void Matrix_normalize(t_Matrix* pMatrix);

/**
 * Normalizza la matrice tramite Floyd-Warshall.
 * Versione ottimizzata a seguito di un cambio del valore
 * in i,j
 */
void Matrix_normalizeAfterChange(t_Matrix* pMatrix, int i, int j);

/**
 * Calcola la riduzione della matrice in base agli eventi dipendenti
 * @param pMatrix Matrice da semplificare
 */
void Matrix_simplifyMatrix(t_Matrix* pMatrix);

/**
 * Aggiunge un vincolo del tipo leftValue<=Ti-Tj<=rightValue alla matrice
 * @param pMatrix Matrice
 * @param i Indice di riga
 * @param j Indice di colonna
 * @param lLeftValue Valore di sinistra del vincolo
 * @param lRightValue Valore di destra del vincolo
 * @param bNormalize se true la matrice viene parzialmente normalizzata
 */
void Matrix_addConstraint(t_Matrix* pMatrix, int i, int j, BMX_LONG lLeftValue,
		BMX_LONG lRightValue, bool bNormalize);

/**
 * Copia il contenuto della matrice pSourceMatrix in pDestMatrix.
 * @param pDestMatrix Matrice di destinazione
 * @param pSourceMatrix Matrice di provenienza
 */
void Matrix_copyInto(t_Matrix* pDestMatrix, t_Matrix* pSourceMatrix);

/**
 * Riporta nell'intervallo passato i tempi massimi e minimi assoluti associati
 * alla variabile di indice varIndex (prima colonna e -prima riga della matrice)
 * @param pMatrix matrice
 * @param iVarIndex Indice della variabile
 * @param pInterval Intervallo da inizializzare
 */
void Matrix_getTimeInterval(t_Matrix* pMatrix, int iVarIndex,
		t_Interval* pInterval);

/**
 * Riporta nell'intervallo passato la differenza dei tempi delle variabili
 * associate ai due indici
 * @param pMatrix Matrice
 * @param i Indice di riga
 * @param j Indice di colonna
 * @param pInterval Intervallo da inizializzare
 */
void Matrix_getTimeDifferenceInterval(t_Matrix* pMatrix, int i, int j,
		t_Interval* pInterval);

/**
 * Riporta il tempo associato ad una variabile
 * @param pMatrix Matrice dei tempi
 * @param varIndex Indice della variabile
 * @return Tempo associato ad una variabile
 */
BMX_LONG Matrix_getTime(t_Matrix* pMatrix, int varIndex);

/**
 * Compara qualitativamente una matrice con un array di tempi. Se la matrice passata come primo argomento
 * rappresenta una soluzione "migliore" dei tempi passati come secondo argomento, la funzione restituisce -1.
 * Se sono equivalenti restituisce 0. Se e' peggiore restituisce 1.
 * @param pMatrix Matrice 1
 * @param times Vettore dei tempi da confrontare con la matrice
 * @return -1, 0 o 1 a secondo se la matrice e' migliore, uguale o peggiore dei tempi passati
 */
int Matrix_compareTo(t_Matrix* pMatrix, BMX_LONG* times);

/**
 * Riporta il tempo peggiore tra tutte le variabili. In pratica corrisponde alla
 * lunghezza totale della schedulazione
 * @param pMatrix Matrice
 * @return Tempo totale della schedulazione
 */
BMX_LONG Matrix_getWorstTime(t_Matrix* pMatrix);

/**
 * Inizializza l'array times con in valori dei tempi della matrice passata
 * @param pMatrix Matrice
 * @param times Array da inizializzare coi tempi
 */
void Matrix_getTimes(t_Matrix* pMatrix, BMX_LONG* times);

/**
 * Valida la matrice, controllando se non contiene vincoli in conflitto
 * @param pMatrix Matrice
 * @return true se e' valida, false altrimenti
 */
bool Matrix_validate(t_Matrix* pMatrix);


#endif /* MATRIX_H_ */
