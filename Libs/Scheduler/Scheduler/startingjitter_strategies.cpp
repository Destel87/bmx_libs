/*
 * startingjitter_strategies.c
 *
 *  Created on: 19/mag/2010
 *      Author: jacopo
 */

#include "startingjitter_strategies.h"
#include "datatypes.h"

StartingJitterStrategyFn fg_sjStrategies[] =
{ relaxedJitterStrategy, minLaneJitterStrategy, halfMinLaneJitterStrategy };

const char* fg_sjNames[] =
{ "Relaxed starting jitter", "Minimum lane starting jitter",
		"Half lane starting jitter" };

int StartingJitterStrategies_getStartingJitterStrategyCount()
{
	return sizeof(fg_sjStrategies) / sizeof(StartingJitterStrategyFn);
}

StartingJitterStrategyFn StartingJitterStrategies_getStartingJitterStrategy(
		int index)
{
	return fg_sjStrategies[index];
}

const char* StartingJitterStrategies_getStartingJitterStrategyName(int index)
{
	return fg_sjNames[index];
}

BMX_LONG halfMinLaneJitterStrategy(t_Problem* pProblem, t_Matrix* pBaseMatrix)
{
	return minLaneJitterStrategy(pProblem, pBaseMatrix) / 2;
}
BMX_LONG minLaneJitterStrategy(t_Problem* pProblem, t_Matrix* pBaseMatrix)
{
	int i;
	int j;
	int iSize;
	BMX_LONG lMin;
	BMX_LONG lMaxSlot;
	BMX_LONG lVal;
	t_Block* pBlock;
	t_ProblemSlot* pProblemSlot;

	iSize = NTSL;
	lMin = BMX_LONG_MAX;
	for (i = 0; i < iSize; ++i)
	{
		pProblemSlot = pProblem->problemSlots + i;

		if (pProblemSlot->iBlockCount > 0)
		{
			lMaxSlot = 0;
			for (j = 0; j < pProblemSlot->iBlockCount; ++j)
			{
				pBlock = pProblemSlot->pBlocks[j];
				lVal = Matrix_getTime(pBaseMatrix, pBlock->iIndex);
				if (lVal > lMaxSlot)
					lMaxSlot = lVal;
			}
			if (lMaxSlot < lMin)
				lMin = lMaxSlot;
		}
	}

	return lMin;

}
BMX_LONG relaxedJitterStrategy(t_Problem* pProblem, t_Matrix* pBaseMatrix)
{
	int i;
	int j;
	int iSize;
	BMX_LONG lRet;
	BMX_LONG lMaxBlock;
	BMX_LONG lVal;
	t_ProblemSlot* pProblemSlot;
	t_Block* pBlock;

	iSize = NTSL;
	lRet = 0;
	for (i = 0; i < iSize; ++i)
	{
		pProblemSlot = pProblem->problemSlots + i;

		lMaxBlock = 0;
		for (j = 0; j < pProblemSlot->iBlockCount; ++j)
		{
			pBlock = pProblemSlot->pBlocks[j];
			lVal = Matrix_getTime(pBaseMatrix, pBlock->iIndex);
			if (lVal > lMaxBlock)
				lMaxBlock = lVal;
		}

		lRet += lMaxBlock;
	}

	return lRet;
}
