/*
 * block.h
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#ifndef BLOCK_H_
#define BLOCK_H_

#include "common.h"

typedef struct struct_Block t_Block;

/**
 * Struttura che wrappa un t_ConstrainingEvent e che aggiunge dati
 * per la soluzione del problema di scheduling
 */
typedef struct struct_Precedence
{
	t_Interval jitter;

	/**Puntatore al blocco su cui ho precedenza*/
	t_Block* pPrevious;

	/**
	 * Se vale true la precedenza e' aggiunta automaticamente dallo schedulatore
	 * e rappresenta lo starting jitter da modificarsi a seconda dell'euristica
	 * di calcolo dello starting jitter
	 */
	bool bStartJitter;

} t_Precedence;

/**
 * Struttura che wrappa i dati di un t_Event e aggiunge dati per la soluzione
 * del problema di scheduling
 */
struct struct_Block
{
	char cEventTAG[32];

	int iEventTAG;

	int iSectionID;

	int iSlotID;

	int iEventID;

	int iResourceID;

	int iActionID;

	BMX_LONG lLength;

	/**Vale true se il blocco e' gia' stato schedulato in precedenza*/
	bool planned;

	BMX_LONG lPlannedTime;

	/**Memorizza il tempo assoluto associato al blocco*/
	t_Interval absoluteTime;

	/**true se l'absolute time e' stato calcolato*/
	bool absoluteTimeSet;

	/**Vettore di precedenze*/
	t_Precedence precedences[NCNS];

	/**Numero di precedenze usato. Puo' esssere maggiore del numero definito in pEvent */
	int iPrecedenceCount;

	/**Indice assunto dal blocco nell'array del problema*/
	int iIndex;

	/**pointer a funzione personalizzata dall'utente*/
	int (*ptrFnc)(int param1, int param2);

};

/**
 * Calcola il tempo assoluto di ogni blocco in base ai vincoli di sequenza.
 * Ogni blocco basa il proprio calcolo sul valore dei blocchi precedenti, per
 * cui, se non gia' calcolati, applica questa funzione ricorsivamente anche
 * ai blocchi precedenti
 *
 * @param pBlock Blocco di cui calcolare l'intervallo di tempo assoluto
 * @param pErrorData Struttura per il ritorno dei messaggi d'errore. La struttura viene
 * 					 modificata solo se la funzione riporta false
 * @return true se non ci sono errori, false altrimenti
 */
bool Block_calcAbsoluteTime(t_Block* pBlock, t_ErrorData* pErrorData);

/**
 * Aggiunge una precedenza al blocco
 * @param pBlock Blocco a cui aggiungere una precedenza
 * @param pPrevBlock Blocco che precede
 * @param lMinJitter Valore minimo del jitter
 * @param lMaxJitter Valore massimo del jitter
 * @param bStartJitter Se vale true la precedenza e' uno starting jitter
 * @return La precenza creata
 */
t_Precedence* Block_addPrecedence(t_Block* pBlock, t_Block* pPrevBlock,
		BMX_LONG lMinJitter, BMX_LONG lMaxJitter, bool bStartJitter);

#endif /* BLOCK_H_ */
