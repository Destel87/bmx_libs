/*
 * problem_solver.c
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#include <string.h>

#include "problem_solver.h"
#include "datatypes.h"

//*************************************************
// Definizioni locali
//*************************************************

/**
 * Inizializza il problem solver, calcolando gli intervalli assoluti di
 * ogni variabile, e la matrice di base con le differenze tra i tempi assoluti.
 *
 * @return true se non ci sono errori, false altrimenti
 */bool ProblemSolver_initialize(t_Problem* pProblem, t_Matrix* pMatrix,
		t_ErrorData* pErrorData);

/**
 * Genera i vincoli di sequenza, ossia i vincoli dati dalle precedenze tra eventi.
 */
void ProblemSolver_generateSequenceConstraints(t_Problem* pProblem,
		t_Matrix* pMatrix);

/**
 * Calcola i nuovi indici dei blocchi che entrano in conflitto, a partire dagli
 * indici dei blocchi passati nelle vaiabili i e j. Riporta i valori dei nuovi indici
 * direttamente in i e j, se esistono, e riporta true. Riporta false se non ci sono piu'
 * blocchi in conflitto
 * @param i Variabile in ingresso/uscita dell'indice i
 * @param j Variabile in ingresso/uscita dell'indice j
 *
 */bool ProblemSolver_searchNewIndexes(int* i, int *j);

/**
 * Algoritmo di generazione dei conflitti
 * @return Codice di ritorno OK, INTERRUPTED, ERROR
 */
int ProblemSolver_generate(void);

/**
 * Ricrea la matrice di runtime a partire da quella di base
 * usando i vincoli salvati. Normalizza completamente prima di ritornare
 * @param count Numero di elementi del vettore da inserire
 */
void ProblemSolver_recreateMatrixFromConstraints(int count);

//**************************************************************


/**Matrice contenente la soluzione di base*/
static  t_Matrix fg_baseMatrix;

/**Matrice di calcolo*/
static  t_Matrix fg_runtimeMatrix;

/**Lista di vincoli aggiunti alla matrice*/
static  t_MatrixConstraint fg_matrixConstraints[NDPTH];

static  t_ProblemSolver* fg_pProblemSolver;

static  t_Problem* fg_pProblem;

static  t_ErrorData* fg_pErrorData;

int ProblemSolver_solve(t_ProblemSolver* pProblemSolver, t_Problem* pProblem,
		t_ErrorData* pErrorData)
{
	//	int i;
	//	int j;
	BMX_LONG startJitter;

	fg_pProblemSolver = pProblemSolver;
	fg_pProblem = pProblem;
	fg_pErrorData = pErrorData;

	// inizializzo il problem solver. Ancora i jitter iniziali sono impostati a valori
	// molto alti
	if (ProblemSolver_initialize(pProblem, &fg_baseMatrix, pErrorData))
	{
		//normalizzo
		Matrix_simplifyMatrix(&fg_baseMatrix);
		Matrix_normalize(&fg_baseMatrix);

		//verifico se la matrice e' valida
		if (!Matrix_validate(&fg_baseMatrix))
		{
			pErrorData->iErrorCode = CONFLICTING_CONSTRAINTS_ERROR;
			strcpy(pErrorData->pErrorMessage, "Illegal model. Some constraints are conflicting.");
			pErrorData->data = 0;
			return ERROR;
		}

		//calcolo e imposto i jitter iniziali con la strategy, in base
		//alla matrice dei vincoli normalizzata
		startJitter = pProblemSolver->pStartingJitterStrategy(pProblem,
				&fg_baseMatrix);
		Problem_setStartJitter(pProblem, startJitter);

		//reinizializzo il problema poiche' cambia tutto con i nuovi jitter
		if (ProblemSolver_initialize(pProblem, &fg_baseMatrix, pErrorData))
		{
			//genero i vincoli di sequenza e normalizzo
			ProblemSolver_generateSequenceConstraints(pProblem, &fg_baseMatrix);
			Matrix_simplifyMatrix(&fg_baseMatrix);
			Matrix_normalize(&fg_baseMatrix);
			Matrix_simplifyMatrix(&fg_baseMatrix);

			//verifico se la matrice e' valida
			if (!Matrix_validate(&fg_baseMatrix))
			{
				pErrorData->iErrorCode = CONFLICTING_CONSTRAINTS_ERROR;
				strcpy(pErrorData->pErrorMessage, "Illegal model. Some constraints are conflicting.");
				pErrorData->data = 0;
				return ERROR;
			}

			//semplifico la matrice eliminando le variabili dipendenti
//			Matrix_simplifyMatrix(&fg_baseMatrix);

			//copio la matrice di base in quella di runtime
			Matrix_copyInto(&fg_runtimeMatrix, &fg_baseMatrix);

			//inizio la procedura di ricerca
			return ProblemSolver_generate();
		}
	}
	return ERROR;
}

t_Matrix* ProblemSolver_calcBaseMatrix(t_Problem* pProblem,
		t_ErrorData* pErrorData)
{
	if (ProblemSolver_initialize(pProblem, &fg_baseMatrix, pErrorData))
	{
		//genero i vincoli di sequenza
		ProblemSolver_generateSequenceConstraints(pProblem, &fg_baseMatrix);
		Matrix_normalize(&fg_baseMatrix);

		//verifico se la matrice e' valida
		if (!Matrix_validate(&fg_baseMatrix))
		{
			pErrorData->iErrorCode = CONFLICTING_CONSTRAINTS_ERROR;
			strcpy(pErrorData->pErrorMessage, "Illegal model. Some constraints are conflicting.");
			pErrorData->data = 0;
			return 0;
		}
		return &fg_baseMatrix;
	}

	return 0;
}

bool ProblemSolver_initialize(t_Problem* pProblem, t_Matrix* pMatrix,
		t_ErrorData* pErrorData)
{

	//Calcolo innanzitutto i tempi assoluti dei blocchi
	if (!Problem_calcBlockAbsoluteTimes(pProblem, pErrorData))
		return false;

	Matrix_initialize(pMatrix, pProblem);

	return true;
}

void ProblemSolver_generateSequenceConstraints(t_Problem* pProblem,
		t_Matrix* pMatrix)
{
	int i;
	int j;
	t_Block* pBlock;
	t_Block* pPrevBlock;
	t_Block* pOtherBlock;
	t_Precedence* pPrecedence;

	for (i = 0; i < pProblem->iBlockCount; ++i)
	{
		pBlock = pProblem->blocks + i;
		for (j = 0; j < pBlock->iPrecedenceCount; ++j)
		{
			pPrecedence = pBlock->precedences + j;
			pPrevBlock = pPrecedence->pPrevious;
			Matrix_addConstraint(pMatrix, pBlock->iIndex, pPrevBlock->iIndex,
					pBlock->lLength + pPrecedence->jitter.left,
					pBlock->lLength + pPrecedence->jitter.right, false);
		}
	}

	//vincoli di priorita'
	for (i = 0; i < NTSL; i++)
	{
		t_ProblemSlot* pProblemSlot = pProblem->problemSlots + i;
		if (pProblemSlot->iBlockCount > 0 && pProblemSlot->iPriority > 0)
		{
			for (j = 0; j < NTSL; j++)
			{
				if (i != j)
				{
					t_ProblemSlot* pOtherProblemSlot = pProblem->problemSlots + j;
					if (pOtherProblemSlot->iBlockCount > 0
							&& pOtherProblemSlot->iPriority > 0
							&& pOtherProblemSlot->iPriority
									!= pProblemSlot->iPriority)
					{
						pBlock = pProblemSlot->pBlocks[0];
						pOtherBlock = pOtherProblemSlot->pBlocks[0];
						if (pProblemSlot->iPriority
								> pOtherProblemSlot->iPriority)
						{
							Matrix_setValue(pMatrix, pOtherBlock->iIndex,
									pBlock->iIndex, 0);
						}
						else
						{
							Matrix_setValue(pMatrix, pBlock->iIndex,
									pOtherBlock->iIndex, 0);
						}
					}
				}
			}
		}
	}

}

void ProblemSolver_generateMatrixConstraint(t_MatrixConstraint* pMc,
		int iBlockIndex, int jBlockIndex)
{
	bool hasLeft;
	bool hasRight;
	t_Block* iBlock;
	t_Block* jBlock;
	t_Interval conflictInterval;
	t_Interval goodInterval;
	t_Interval leftInterval;
	t_Interval rightInterval;

	//recupero i blocchi che entrano in conflitto
	iBlock = fg_pProblem->blocks + iBlockIndex;
	jBlock = fg_pProblem->blocks + jBlockIndex;

	//Calcolo l'intervallo Ti-Tj di conflitto tra i due blocchi
	conflictInterval.left = -jBlock->lLength;
	conflictInterval.right = iBlock->lLength;

	//Intervallo totale di validita' di Ti-Tj
	Matrix_getTimeDifferenceInterval(&fg_runtimeMatrix, iBlockIndex,
			jBlockIndex, &goodInterval);

	//Gli intervalli da esplorare sono quelli che rimangono dalla sottrazione tra
	//goodInterval con conflictInterval

	hasLeft = conflictInterval.left >= goodInterval.left;
	hasRight = conflictInterval.right <= goodInterval.right;

	if (hasLeft || hasRight)
	{
		//Mi calcolo gli intervalli che rimangono
		leftInterval.left = goodInterval.left;
		leftInterval.right = min( conflictInterval.left, goodInterval.right );
		rightInterval.left = max( conflictInterval.right, goodInterval.left );
		rightInterval.right = goodInterval.right;

		//adesso ordino gli intervalli a seconda se sono presenti
		//o se ce ne e' uno solo. Uso pFirstInterval e pLastInterval per
		//ordinarli

		//solo sinistra
		if (!hasRight)
		{
			pMc->intervals[0] = leftInterval;
			pMc->intervalCount = 1;
		}
		//solo destra
		else if (!hasLeft)
		{
			pMc->intervals[0] = rightInterval;
			pMc->intervalCount = 1;
		}
		//ci sono entrambi
		else
		{
			//si ordinano gli intervalli con la ordering strategy
			if (fg_pProblemSolver->pOrderingStrategy(&leftInterval,
					&rightInterval, iBlockIndex, jBlockIndex))
			{
				pMc->intervals[0] = leftInterval;
				pMc->intervals[1] = rightInterval;
			}
			else
			{
				pMc->intervals[0] = rightInterval;
				pMc->intervals[1] = leftInterval;
			}
			pMc->intervalCount = 2;
		}
	}
	else
	{
		//nessun intervallo
		pMc->intervalCount = 0;
	}

	//imposto l'indice corrente
	pMc->currentIntervalIndex = -1;
	pMc->iIndex = iBlockIndex;
	pMc->jIndex = jBlockIndex;
}

int ProblemSolver_generate()
{
	int iMatrixConstraintIndex;
	int iBlockIndex;
	int jBlockIndex;
	int iRet;
	bool bFullCreateMatrix;
	t_Interval* pInterval;

	iBlockIndex = 1;
	jBlockIndex = 1;
	iRet = OK;
	iMatrixConstraintIndex = 0;
	bFullCreateMatrix = false;

	//cerco i primi indici disponibili
	if (!ProblemSolver_searchNewIndexes(&iBlockIndex, &jBlockIndex))
	{
		//se non ci sono ho una soluzione banale pari a quella di base
		fg_pProblemSolver->pSolutionNotify(fg_pProblem, &fg_runtimeMatrix);
		return OK;
	}

	//calcolo i primi vincoli della matrice
	ProblemSolver_generateMatrixConstraint(fg_matrixConstraints, iBlockIndex,
			jBlockIndex);

	//adesso parte il ciclo di ricerca
	while (iMatrixConstraintIndex >= 0 && iRet == OK)
	{
		//interrompo se richiesto
		if (fg_pProblemSolver->pCancelRequested())
		{
			iRet = INTERRUPTED;
		}
		else
		{

			//si estrae il vincolo attuale
			t_MatrixConstraint* pConstraint = fg_matrixConstraints
					+ iMatrixConstraintIndex;

			//incremento il currentIntervalIndex
			pConstraint->currentIntervalIndex++;

			//ripristino gli indici dei blocchi in conflitto
			iBlockIndex = pConstraint->iIndex;
			jBlockIndex = pConstraint->jIndex;

			//se il vincolo contiene intervalli da inserire
			if (pConstraint->currentIntervalIndex < pConstraint->intervalCount)
			{
				//controllo se devo ricostruire la matrice dal principio
				if (bFullCreateMatrix)
				{
					//ricostruisco la matrice includendo il vincolo attuale
					ProblemSolver_recreateMatrixFromConstraints(
							iMatrixConstraintIndex + 1);
					bFullCreateMatrix = false;
				}
				else
				{
					//aggiungo il vincolo attuale nella matrice e normalizzo
					//semplificato
					pInterval = pConstraint->intervals
							+ pConstraint->currentIntervalIndex;
					Matrix_addConstraint(&fg_runtimeMatrix, iBlockIndex,
							jBlockIndex, pInterval->left, pInterval->right,
							true);
				}

				//determino i nuovi indici di conflitto
				if (ProblemSolver_searchNewIndexes(&iBlockIndex, &jBlockIndex))
				{
					//ho nuovi indici di conflitto
					//incremento l'indice del vincolo corrente
					iMatrixConstraintIndex++;

					//genero un nuovo vincolo
					ProblemSolver_generateMatrixConstraint(
							fg_matrixConstraints + iMatrixConstraintIndex,
							iBlockIndex, jBlockIndex);

				}
				//non ci sono piu' indici in conflitto. abbiamo una soluzione
				else
				{
					//notifico una nuova soluzione
					fg_pProblemSolver->pSolutionNotify(fg_pProblem,
							&fg_runtimeMatrix);

					//la prox volta che si va in profondita' la matrice deve essere
					//ricostruita completamente
					bFullCreateMatrix = true;

					//non decremento indice vincoli poiche' potrebbe esserci ancora
					//l'altro intervallo su questo indice
				}
			}
			else
			//il vincolo non contiene intervalli da inserire
			{
				//la prox volta che si va in profondita' la matrice deve essere
				//ricostruita completamente
				bFullCreateMatrix = true;

				//decremento indice vincoli
				iMatrixConstraintIndex--;
			}

		}
	}

	return iRet;
}

void ProblemSolver_recreateMatrixFromConstraints(int count)
{
	int i;
	t_MatrixConstraint* c;

	// ricostruisco la matrice dai vincoli
	Matrix_copyInto(&fg_runtimeMatrix, &fg_baseMatrix);

	for (i = 0; i < count; i++)
	{
		c = fg_matrixConstraints + i;
		Matrix_addConstraint(&fg_runtimeMatrix, c->iIndex, c->jIndex,
				c->intervals[c->currentIntervalIndex].left,
				c->intervals[c->currentIntervalIndex].right, false);
	}
	//normalizzo tutto insieme
	Matrix_normalize(&fg_runtimeMatrix);
}

bool ProblemSolver_canBeConflicting(int i, int j)
{
	BMX_LONG iLeft;
	BMX_LONG iRight;
	BMX_LONG jLeft;
	BMX_LONG jRight;
	t_Block* iBlock;
	t_Block* jBlock;
	t_Interval iInt;
	t_Interval jInt;

	iBlock = fg_pProblem->blocks + i;
	jBlock = fg_pProblem->blocks + j;
	if (i != j && Problem_testResourceConflict(fg_pProblem, iBlock, jBlock))
	{
		Matrix_getTimeInterval(&fg_runtimeMatrix, i, &iInt);
		Matrix_getTimeInterval(&fg_runtimeMatrix, j, &jInt);

		iLeft = iInt.left - iBlock->lLength;
		iRight = iInt.right;
		jLeft = jInt.left - jBlock->lLength;
		jRight = jInt.right;

		return (iLeft >= jLeft && iLeft < jRight) || (jLeft >= iLeft && jLeft
				< iRight);
	}
	return false;
}

bool ProblemSolver_searchNewIndexes(int* iIndex, int *jIndex)
{
	int i;
	int j;
	int iStart;
	int jStart;
	int varCount;
	int iResId;
	int jInit;
	t_Block* iBlock;
	t_Block* jBlock;

	iStart = *iIndex;
	jStart = *jIndex;
	varCount = fg_pProblem->iBlockCount;

	for (i = iStart; i < varCount - 1; i++)
	{
		iBlock = fg_pProblem->blocks + i;
		iResId = iBlock->iResourceID;
		if (iResId > 0)
		{
			//il secondo ciclo parte da jIndex solo al primo giro di i, poi parte da
			// i + 1 (TODO da ottimizzare e far partire dal primo evento dello
			// slot successivo a quello di iIndex)
			jInit = (i == iStart) ? (jStart + 1) : (i + 1);
			for (j = jInit; j < varCount; j++)
			{
				jBlock = fg_pProblem->blocks + j;

				//se possono entrare in conflitto in base alla runtime matrix
				if (ProblemSolver_canBeConflicting(i, j))
				{
					*iIndex = i;
					*jIndex = j;
					return true;
				}
			}
		}
	}
	return false;
}

int ProblemSolver_compareSolution(t_Problem* pProblem, t_Matrix* pMatrix,
		BMX_LONG* lSolutions)
{
	int i;
	int j;
	int k;
	int iRet;
	BMX_LONG minTimeMatrix;
	BMX_LONG minTimeSolution;

	t_ProblemSlot* pProblemSlot;
	t_Block* pBlock;

	iRet = Matrix_compareTo(pMatrix, lSolutions);

	//nel caso la matrice dia tempi uguali si ordinano per sezione il cui primo
	//evento ha tempo minore
	for (i = 0; iRet == 0 && i < NST; i++)
	{
		minTimeMatrix = BMX_LONG_MAX;
		minTimeSolution = BMX_LONG_MAX;
		for (j = 0; j < NSL + 1; j++)
		{
			pProblemSlot = pProblem->problemSlots + (i * (NSL + 1) + j);
			if (pProblemSlot->iBlockCount > 0)
			{
				for (k = 0; k < pProblemSlot->iBlockCount; k++)
				{
					pBlock = pProblemSlot->pBlocks[k];
					minTimeMatrix
							= min( minTimeMatrix, Matrix_getTime( pMatrix, pBlock->iIndex ) );
					minTimeSolution
							= min( minTimeSolution, lSolutions[ pBlock->iIndex ] );
				}

			}
		}

		iRet = minTimeMatrix < minTimeSolution ? -1 : minTimeMatrix
				> minTimeSolution ? 1 : 0;
	}

	return iRet;
}
