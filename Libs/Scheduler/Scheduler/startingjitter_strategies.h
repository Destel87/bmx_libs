/*
 * ordering_strategies.h
 *
 *  Created on: 19/mag/2010
 *      Author: jacopo
 */

#ifndef STARTINGJITTER_STRATEGIES_H_
#define STARTINGJITTER_STRATEGIES_H_

#include "problem_solver.h"

/**
 * Riporta il numero di starting jitter strategies disponibili
 */
int StartingJitterStrategies_getStartingJitterStrategyCount();

/**
 * Riporta la strategia di starting jitter dato l'indice
 */
StartingJitterStrategyFn StartingJitterStrategies_getStartingJitterStrategy(
		int index);

/**
 * Riporta il nome della starting jitter strategy dato l'indice
 */
const char* StartingJitterStrategies_getStartingJitterStrategyName(int index);

BMX_LONG halfMinLaneJitterStrategy(t_Problem* pProblem, t_Matrix* pBaseMatrix);
BMX_LONG minLaneJitterStrategy(t_Problem* pProblem, t_Matrix* pBaseMatrix);
BMX_LONG relaxedJitterStrategy(t_Problem* pProblem, t_Matrix* pBaseMatrix);

#endif /* STARTINGJITTER_STRATEGIES_H_ */
