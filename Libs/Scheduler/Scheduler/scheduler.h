/*
 * Problem.h
 *
 *  Created on: 12/mag/2010
 *      Author: jacopo
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "common.h"
#include "problem.h"
#include "workplan.h"
#include "timeline.h"

#define NEW_PHASE_STEP 1
#define EXECUTE_PHASE_STEP 2
#define END_PHASE_STEP 3

/**
 * Funzione di controllo del solver.
 * Alla funzione viene passato come ultimo argomento un pointer ad
 * un valore boolean che, se settato, fa terminare in modo graceful la fase
 * attualmente eseguita dall'algoritmo. I primi parametri della funzione servono
 * per informare il caller se siamo all'inizio di una fase o se stiamo eseguendo
 * una fase. All'inizio di ogni fase il primo parametro assume il valore NEW_PHASE_STEP
 * e il secondo rappresenta il timeout previsto per la fase. L'algoritmo si aspetta
 * che, una volta passato il tempo di timeout, la variabile boolean venga settata. Questo
 * provoca il passaggio alla fase successiva.
 * Durante l'esecuzione di una fase la funzione stepFn viene invocata varie volte con primo
 * parametro pari a EXECUTE_PHASE_STEP. Il secondo parametro e' sempre pari al timeout
 * della fase.
 * Alla fine della fase la funzione viene chiamata con parametro pari a END_PHASE_STEP
 * Questo puo' avvenire a seguito di una richiesta di interruzione della fase tramite
 * il settaggio del parametro boolean del metodo stepFn, o se effettivamente
 * l'algoritmo fa terminare la fase spentaneamente.
 * Il timeout passato alla funzione puo' essere 0 o negativo. In questo caso
 * per la fase non e' previsto un timeout, per cui la funzione non deve fare niente,
 * ma puo' sempre interrompere l'esecuzione se e' necessario annullare ad esempio
 * l'operazione<br>
 * Concettualmente la funzione stepFn e' stata pensata per essere utilizzata in
 * due modi:
 * <ul>
 * <li>
 * il primo metodo prevede l'utilizzo di un timer. Quando il primo parametro della funzione stepFn
 * vale NEW_PHASE_STEP, un timer deve essere installato pari al timeout specificato.
 * Scaduto il timer, la variabile boolean passata deve essere settata, o direttamente
 * dentro la funzione stepFn (chiamata varie volte durante l'esecuzione della fase) o
 * indipendentemente, in maniera asincrona rispetto al thread di calcolo (sul thread
 * del timer) memorizzando il pointer alla variabile boolean esternamente.
 * Bisogna prestare attenzione all'eliminazione dei timer, quando la funzione
 * termina una fase. Puo' accadere infatti che la funzione di scheduling faccia terminare
 * una fase prima che il timer installato termini. E' quindi necessario disinstallare
 * il timer quando viene chiamato il metodo stepFn con parametro END_PHASE_STEP,
 * e far ritornare la funzione solo quando il timer da disinstallare non e' effettivamente
 * terminato.
 * </li>
 * <li>
 * Il secondo metodo prevede l'utilizzo le funzioni di interrogazione del clock di sistema.
 * Quando il primo parametro della funzione stepFn vale NEW_PHASE_STEP, si memorizza
 * il tempo di sistema, e si controlla ad ogni chiamata successiva, quando il primo
 * parametro vale EXECUTE_PHASE_STEP. Se la differenza di tempo supera il timeout,
 * viene settata la variabile boolean.
 * </li>
 * </ul>
 * @param type Tipo di passo: uno tra i seguenti valori: NEW_PHASE_STEP, EXECUTE_PHASE_STEP,
 * 			   END_PHASE_STEP
 * @param timeout Timeout del passo. Se vale 0 o negativo il passo non prevede un timeout
 * @param solutionFound Impostata a true se lo scheduler ha gia' trovato una soluzione
 * @param pInterruptionRequested Pointer a variabile bool da settare se si desidera
 * 							   richiedere l'interruzione della fase
 */
typedef void(*StepFn)(int type, BMX_LONG timeout, bool solutionFound,
		bool* pInterruptionRequested);

/**
 * Esegue il calcolo dello scheduling, caricando il problema da pWorkPlan, e
 * inserendo i risultati in pTimeLine. Il processo viene controllato
 * attraverso il pointer a funzione passato come ultimo argomento.
 * L'algoritmo e' suddiviso in fasi che devono essere temporizzate esternamente
 * alla libreria (per permettere un controllo tramite timer o calcolo sul tempo
 * di sistema o con qualsiasi altro sistema arbitrario).
 * La funzione di controllo stepFn viene invocata ad ogni ciclo dell'algoritmo di
 * calcolo e permette un controllo fine dell'interruzione dell'algoritmo stesso.
 * Vedere la documentazione della funzione per ulteriori informazioni.
 * @param pWorkPlan Work plan
 * @param pTimeLine time line da riempire
 * @param pErrorData Struttura contenente l'eventuale errore
 * @param stepFn Pointer a funzione di controllo. Se viene passato null, viene usata
 * 		  un'implementazione di default che fa uso del time di sistema
 * @return true se l'algoritmo termina correttamente, false se ci sono errori
 * 		   (pErrorData contiene le informazioni sull'errore)
 */
bool Scheduler_scheduleWorkPlan(t_WorkPlan* pWorkPlan, t_TimeLine* pTimeLine,
		t_ErrorData* pErrorData, StepFn stepFn);

/**
 * Esegue il calcolo dello scheduling, utilizzando un problema gia' inizializzato, e
 * inserendo i risultati in pTimeLine.
 * Per inizializzare un problema si deve creare un'istanza della struttura t_Problem.
 * Poi si invoca la funzione Problem_startConfig(...) per iniziare la configurazione del
 * problema. Si chiama ripetutamente il metodo Problem_addBlock(...) per aggiungere
 * eventi agli slot delle sezioni e Problem_addPrecedence(...) per i vincoli
 * di precedenza tra eventi. Non e' necessario che siano inseriti gli eventi
 * con Problem_addBlock prima di specificare un vincolo tra essi con Problem_addPrecedence.
 * Al termine dell'inizializzazione del problema deve essere chiamata la funzione
 * Problem_endConfig(...) per terminare correttamente la configurazione. Il problema
 * cosi' creato puo' essere passato a questa funzione per essere schedulato.
 * Consultare la documentazione delle funzioni Problem_* per ulteriori informazioni.
 * Il processo viene controllato attraverso il pointer a funzione passato come ultimo argomento.
 * L'algoritmo e' suddiviso in fasi che devono essere temporizzate esternamente
 * alla libreria (per permettere un controllo tramite timer o calcolo sul tempo
 * di sistema o con qualsiasi altro sistema arbitrario).
 * La funzione di controllo stepFn viene invocata ad ogni ciclo dell'algoritmo di
 * calcolo e permette un controllo fine dell'interruzione dell'algoritmo stesso.
 * Vedere la documentazione della funzione per ulteriori informazioni.
 * @param pProblem Problema inizializzato
 * @param pTimeLine time line da riempire
 * @param pErrorData Struttura contenente l'eventuale errore
 * @param step Pointer a funzione di controllo. Se viene passato null, viene usata
 * 		  un'implementazione di default che fa uso del time di sistema
 * @return true se l'algoritmo termina correttamente, false se ci sono errori
 * 		   (pErrorData contiene le informazioni sull'errore)
 */
bool Scheduler_scheduleProblem(t_Problem* pProblem, t_TimeLine* pTimeLine,
		t_ErrorData* pErrorData, StepFn step);

/**
 * Riporta la versione dello scheduler
 */
const char* Scheduler_getVersion();

/**
 * @return Riporta Il numero di sezioni
 */
int Scheduler_getSectionCount();

/**
 * @return Riporta il numero di slot per sezione
 */
int Scheduler_getSlotPerSectionCount();

/**
 * @return Riporta il massimo numero di eventi per slot
 */
int Scheduler_getMaxEventPerSlotCount();

/**
 * @return Riporta il massimo numero di vincoli per evento
 */
int Scheduler_getMaxConstraintPerEventCount();

/**
 * @return Riporta il numero di risorse
 */
int Scheduler_getResourceCount();

/**
 * @return Riporta il massimo numero di azioni per risorsa
 */
int Scheduler_getMaxActionPerResourceCount();

#endif /* PROBLEM_H_ */
