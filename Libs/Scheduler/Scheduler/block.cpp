/*
 * block.c
 *
 *  Created on: 13/mag/2010
 *      Author: jacopo
 */

#include <string.h>

#include "block.h"
#include "datatypes.h"

bool Block_calcAbsoluteTime(t_Block* pBlock, t_ErrorData* pErrorData)
{
	int i;
	BMX_LONG lNewLeft;
	BMX_LONG lNewRight;
	t_Precedence* pPrecedence;
	t_Block* pPrevious;
	t_Interval* pPrevInterval;

	if (!pBlock->absoluteTimeSet)
	{
		pBlock->absoluteTime.left = BMX_LONG_MIN;
		pBlock->absoluteTime.right = BMX_LONG_MAX;

		//Se ho un blocco senza precedenze significa che non e' vincolato a niente,
		//nemmeno al t* e non ha senso.
		if (pBlock->iSectionID >= 0 && pBlock->iPrecedenceCount == 0)
		{
			pErrorData->iErrorCode = INTERNAL_ERROR;
			strcpy(pErrorData->pErrorMessage, "Illegal model. A block without constraints was found.");
			return false;
		}

		for (i = 0; i < pBlock->iPrecedenceCount; ++i)
		{
			pPrecedence = pBlock->precedences + i;
			pPrevious = pPrecedence->pPrevious;

			//inizializzo l'absolute time del previous se necessario
			if (!Block_calcAbsoluteTime(pPrevious, pErrorData))
				return false;

			pPrevInterval = &pPrevious->absoluteTime;

			//calcolo il nuovo intervallo rispetto al blocco
			//che precede e il jitter.
			lNewLeft = pPrevInterval->left + pPrecedence->jitter.left
					+ pBlock->lLength;
			lNewRight = pPrevInterval->right + pPrecedence->jitter.right
					+ pBlock->lLength;

			//scelgo l'intervallo piu' stringente tra quello attuale
			//e quello appena calcolato
			pBlock->absoluteTime.left
					= max( pBlock->absoluteTime.left, lNewLeft );
			pBlock->absoluteTime.right
					= min( pBlock->absoluteTime.right, lNewRight );

		}

		if (pBlock->absoluteTime.left > pBlock->absoluteTime.right)
		{
			pErrorData->iErrorCode = CONFLICTING_CONSTRAINTS_ERROR;
			strcpy(pErrorData->pErrorMessage, "Illegal model. Some constraints are conflicting.");
			pErrorData->data = (int) pBlock;
			return false;
		}
	}

	return true;
}

t_Precedence* Block_addPrecedence(t_Block* pBlock, t_Block* pPrevBlock,
		BMX_LONG lMinJitter, BMX_LONG lMaxJitter, bool bStartJitter)
{
	t_Precedence* pPrecedence;

	pPrecedence = pBlock->precedences + pBlock->iPrecedenceCount;
	pPrecedence->jitter.left = lMinJitter;
	pPrecedence->jitter.right = lMaxJitter;
	pPrecedence->pPrevious = pPrevBlock;
	pPrecedence->bStartJitter = bStartJitter;
	++pBlock->iPrecedenceCount;
	return pPrecedence;
}

