QT          -= core gui
TARGET      = Scheduler
TEMPLATE    = lib
CONFIG      += staticlib

DEFINES += SCHEDULER_LIBRARY
INCLUDEPATH += Scheduler

#internal libraries inclusion
INCLUDEPATH += $$PWD/../StaticSingleton

unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../TimeStamp
DEPENDPATH += $$PWD/../TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../TimeStamp/libTimeStamp.a


SOURCES += \
    CProblem.cpp \
    CProblemIo.cpp \
    CScheduler.cpp \
    Scheduler/matrix.cpp \
    SchedulerThread.cpp \
    Scheduler/block.cpp \
    Scheduler/ordering_strategies.cpp \
    Scheduler/problem.cpp \
    Scheduler/problem_io.cpp \
    Scheduler/problem_solver.cpp \
    Scheduler/scheduler.cpp \
    Scheduler/startingjitter_strategies.cpp \
    CMatrixImport.cpp

HEADERS +=\
    Scheduler/workplan.h \
    Scheduler/timeline.h \
    Scheduler/startingjitter_strategies.h \
    Scheduler/scheduler.h \
    Scheduler/problem.h \
    Scheduler/problem_solver.h \
    Scheduler/problem_io.h \
    Scheduler/ordering_strategies.h \
    Scheduler/matrix.h \
    Scheduler/datatypes.h \
    Scheduler/common.h \
    Scheduler/block.h \
    CProblem.h \
    SchedulerInclude.h \
    CProblemIo.h \
    CScheduler.h \
    SchedulerThread.h

