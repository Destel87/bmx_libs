//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ProcessManager.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the ProcessManager class.
//! @details
//!
//*!****************************************************************************

#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include <vector>
#include "Process.h"
#include "Loggable.h"
#include <string>
#include "Thread.h"

using namespace std;

typedef struct
{
	Process m_Process;
	bool m_bIsH24;	//if true Process manager always respawns m_Process
} process;

class ProcessManager : public Loggable, public Thread
{

	public:

		/*! ***********************************************************************************************************
		* Default constructor
		* ************************************************************************************************************
		*/
		ProcessManager();

		/*! ***********************************************************************************************************
		* Default destructor
		* ************************************************************************************************************
		*/
		virtual ~ProcessManager();

		/*! ***********************************************************************************************************
		* Initializes the vector of processes to be spawn and monitored
		* @param sConfigFile	the configuration file that contains the configuration keys
		*						necessary to setup each process
		* ************************************************************************************************************
		*/
		bool initFromConfigFile(const char* sConfigFile);

		/*! ***********************************************************************************************************
		 * @brief stopAll		stop all the processes while the thread is running and exit
		 * @return true if the processes have been correctly stopped and returned 1 within the timeout
		 * ************************************************************************************************************
		 */
		bool stop(int liTimeoutMsec = -1);

		int getRespawnCycleDurationMSec() const;
		void setRespawnCycleDurationMSec(int liRespawnCycleDurationMSec);

		int getProcIdByName(string procName);

	protected:

		int workerThread();
		void beforeWorkerThread();
		void afterWorkerThread();

    private:

        vector<process> m_pProcesses;
        bool m_bConfigOk;
        bool m_bSpawnModeIsParallel;			// if true the processes are executed and monitored
                                                // parallel, if false the processes are spawned sequential
        int m_liRespawnCycleDurationMSec;		// respawn loop duration in milliseconds
        int m_liMaxWaitProcessMSec;
        bool m_bMustStop;
        bool m_bAllAreStopped;


};

#endif // PROCESSMANAGER_H
