//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ProcessManagerInclude.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the defines for the ProcessManager lib.
//! @details
//!
//*!****************************************************************************

#ifndef PROCESSMANAGERINCLUDE_H
#define PROCESSMANAGERINCLUDE_H

/*! ***********************************************************************************************
 * ProcessManager class defines and macros
 * ************************************************************************************************
 */
#define MAX_NUM_PROCESSES						16
#define MAX_NUM_PROCESSES_FROM_CONFIG_FILE		8
#define CONFIG_KEYS_DISPLACEMENT_INTERPROCESS	10

/*! ***********************************************************************************************
 * ProcessManager class configuration keys
 * ************************************************************************************************
 */
#define CFG_PM_POOL_SPAWN_MODE			1000
#define CFG_PM_POOL_LIST				1001
#define CFG_PM_CYCLE_DURATION_SEC		1002
#define CFG_PM_MAX_PROCESS_EXIT_SEC		1003

#define CFG_PM_PROCESS_0_CMD			0
#define CFG_PM_PROCESS_1_CMD			1
#define CFG_PM_PROCESS_2_CMD			2
#define CFG_PM_PROCESS_3_CMD			3
#define CFG_PM_PROCESS_4_CMD			4
#define CFG_PM_PROCESS_5_CMD			5
#define CFG_PM_PROCESS_6_CMD			6
#define CFG_PM_PROCESS_7_CMD			7
#define CFG_PM_PROCESS_8_CMD			8
#define CFG_PM_PROCESS_9_CMD			9
#define CFG_PM_PROCESS_10_CMD			10
#define CFG_PM_PROCESS_11_CMD			11
#define CFG_PM_PROCESS_12_CMD			12
#define CFG_PM_PROCESS_13_CMD			13
#define CFG_PM_PROCESS_14_CMD			14
#define CFG_PM_PROCESS_15_CMD			15

#define CFG_PM_PROCESS_0_PREPEND_CMD	100
#define CFG_PM_PROCESS_0_ARGS			101
#define CFG_PM_PROCESS_0_BASE_DIR		102
#define CFG_PM_PROCESS_0_LIFETIME		103
#define CFG_PM_PROCESS_0_POLICY			104
#define CFG_PM_PROCESS_0_PRIORITY		105

#define CFG_PM_PROCESS_1_PREPEND_CMD	110
#define CFG_PM_PROCESS_1_ARGS			111
#define CFG_PM_PROCESS_1_BASE_DIR		112
#define CFG_PM_PROCESS_1_LIFETIME		113
#define CFG_PM_PROCESS_1_POLICY			114
#define CFG_PM_PROCESS_1_PRIORITY		115

#define CFG_PM_PROCESS_2_PREPEND_CMD	120
#define CFG_PM_PROCESS_2_ARGS			121
#define CFG_PM_PROCESS_2_BASE_DIR		122
#define CFG_PM_PROCESS_2_LIFETIME		123
#define CFG_PM_PROCESS_2_POLICY			124
#define CFG_PM_PROCESS_2_PRIORITY		125

#define CFG_PM_PROCESS_3_PREPEND_CMD	130
#define CFG_PM_PROCESS_3_ARGS			131
#define CFG_PM_PROCESS_3_BASE_DIR		132
#define CFG_PM_PROCESS_3_LIFETIME		133
#define CFG_PM_PROCESS_3_POLICY			134
#define CFG_PM_PROCESS_3_PRIORITY		135

#define CFG_PM_PROCESS_4_PREPEND_CMD	140
#define CFG_PM_PROCESS_4_ARGS			141
#define CFG_PM_PROCESS_4_BASE_DIR		142
#define CFG_PM_PROCESS_4_LIFETIME		143
#define CFG_PM_PROCESS_4_POLICY			144
#define CFG_PM_PROCESS_4_PRIORITY		145

#define CFG_PM_PROCESS_5_PREPEND_CMD	150
#define CFG_PM_PROCESS_5_ARGS			151
#define CFG_PM_PROCESS_5_BASE_DIR		152
#define CFG_PM_PROCESS_5_LIFETIME		153
#define CFG_PM_PROCESS_5_POLICY			154
#define CFG_PM_PROCESS_5_PRIORITY		155

#define CFG_PM_PROCESS_6_PREPEND_CMD	160
#define CFG_PM_PROCESS_6_ARGS			161
#define CFG_PM_PROCESS_6_BASE_DIR		162
#define CFG_PM_PROCESS_6_LIFETIME		163
#define CFG_PM_PROCESS_6_POLICY			164
#define CFG_PM_PROCESS_6_PRIORITY		165

#define CFG_PM_PROCESS_7_PREPEND_CMD	170
#define CFG_PM_PROCESS_7_ARGS			171
#define CFG_PM_PROCESS_7_BASE_DIR		172
#define CFG_PM_PROCESS_7_LIFETIME		173
#define CFG_PM_PROCESS_7_POLICY			174
#define CFG_PM_PROCESS_7_PRIORITY		175

#define CFG_PM_PROCESS_8_PREPEND_CMD	180
#define CFG_PM_PROCESS_8_ARGS			181
#define CFG_PM_PROCESS_8_BASE_DIR		182
#define CFG_PM_PROCESS_8_LIFETIME		183
#define CFG_PM_PROCESS_8_POLICY			184
#define CFG_PM_PROCESS_8_PRIORITY		185

#define CFG_PM_PROCESS_9_PREPEND_CMD	190
#define CFG_PM_PROCESS_9_ARGS			191
#define CFG_PM_PROCESS_9_BASE_DIR		192
#define CFG_PM_PROCESS_9_LIFETIME		193
#define CFG_PM_PROCESS_9_POLICY			194
#define CFG_PM_PROCESS_9_PRIORITY		195

#define CFG_PM_PROCESS_10_PREPEND_CMD	200
#define CFG_PM_PROCESS_10_ARGS			201
#define CFG_PM_PROCESS_10_BASE_DIR		202
#define CFG_PM_PROCESS_10_LIFETIME		203
#define CFG_PM_PROCESS_10_POLICY		204
#define CFG_PM_PROCESS_10_PRIORITY		205

#define CFG_PM_PROCESS_11_PREPEND_CMD	210
#define CFG_PM_PROCESS_11_ARGS			211
#define CFG_PM_PROCESS_11_BASE_DIR		212
#define CFG_PM_PROCESS_11_LIFETIME		213
#define CFG_PM_PROCESS_11_POLICY		214
#define CFG_PM_PROCESS_11_PRIORITY		215

#define CFG_PM_PROCESS_12_PREPEND_CMD	220
#define CFG_PM_PROCESS_12_ARGS			221
#define CFG_PM_PROCESS_12_BASE_DIR		222
#define CFG_PM_PROCESS_12_LIFETIME		223
#define CFG_PM_PROCESS_12_POLICY		224
#define CFG_PM_PROCESS_12_PRIORITY		225

#define CFG_PM_PROCESS_13_PREPEND_CMD	230
#define CFG_PM_PROCESS_13_ARGS			231
#define CFG_PM_PROCESS_13_BASE_DIR		232
#define CFG_PM_PROCESS_13_LIFETIME		233
#define CFG_PM_PROCESS_13_POLICY		234
#define CFG_PM_PROCESS_13_PRIORITY		235

#define CFG_PM_PROCESS_14_PREPEND_CMD	240
#define CFG_PM_PROCESS_14_ARGS			241
#define CFG_PM_PROCESS_14_BASE_DIR		242
#define CFG_PM_PROCESS_14_LIFETIME		243
#define CFG_PM_PROCESS_14_POLICY		244
#define CFG_PM_PROCESS_14_PRIORITY		245

#define CFG_PM_PROCESS_15_PREPEND_CMD	250
#define CFG_PM_PROCESS_15_ARGS			251
#define CFG_PM_PROCESS_15_BASE_DIR		252
#define CFG_PM_PROCESS_15_LIFETIME		253
#define CFG_PM_PROCESS_15_POLICY		254
#define CFG_PM_PROCESS_15_PRIORITY		255

#endif // PROCESSMANAGERINCLUDE_H
