//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Process.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the Process class.
//! @details
//!
//*!****************************************************************************

#ifndef PROCESS_H
#define PROCESS_H

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <sched.h>

#define IS_ALIVE   0
#define IS_RESTART 1
#define IS_DOWN    2

#define PROCESS_VERSION "1.0.0"
#define DEFAULT_PROCESS_PRIORITY	0

/*! ***********************************************************************************************
* @class    Process
* @brief    Implements an interface for a process. The user can interact with the process with
*			a set of features like:
*				- initialization of the process cmd line,
*				- start;
*				- stop;
*				- PID retrieving;
*				- priority setting;
*				- respawning;
*				- etc...
* *************************************************************************************************
*/
class Process
{
	public:

		/*! ***************************************************************************************
		* void constructor
		* *****************************************************************************************
		*/
		Process();

		/*! ***************************************************************************************
		* Default destructor
		* *****************************************************************************************
		*/
		virtual ~Process();

		/*! ***************************************************************************************
		* Checks if the process is alive
		* @return	true if the process is alive, false otherwise
		* *****************************************************************************************
		*/
		bool isAlive(void);

		/*! ***************************************************************************************
		* Initializes the process with its command line. This method does not start the process.
        * @param sCmdLine the command line of the process
        * @param nSchedPolicy the scheduling policy
        * @param nPriority the priority
		* @return	true if the command line has been correctly parsed
		* *****************************************************************************************
		*/
        bool init(const char * sCmdLine, int nSchedPolicy = SCHED_OTHER, int nPriority = DEFAULT_PROCESS_PRIORITY);

		/*! ***************************************************************************************
		* Completely clears the command line and all its arguments
		* *****************************************************************************************
		*/
		void clearCommandLine(void);

		/*! ***************************************************************************************
		* Starts the processs
		* @return	true if process is started and has its PID, false otherwise
		* *****************************************************************************************
		*/
		bool start(void);

		/*! ***************************************************************************************
		* Kills the process, without cleaning the argv list, so that can be restarted later
		* without having to reinitialize it. The process exit code can be retrieved with
		* the getRetCode() method
		* *****************************************************************************************
		*/
		void stop(void);

		/*! ***************************************************************************************
		* Kills the process and cleans the argv list
		* *****************************************************************************************
		*/
		void reset(void);

		/*! ***************************************************************************************
		* Checks that the process is alive and, if not, respawns it
		* @param	nMsecSleepBeforeRestart milliseconds to wait before the possible restart
		* @return	IS_ALIVE if the process was alive, IS_RESTART if a restart has occurred,
		*			IS_DOWN if the process was dead and a restart was not effective
		* *****************************************************************************************
		*/
		int respawn(int nMsecSleepBeforeRestart = 0);

		/*! ***************************************************************************************
		* Returns the PID of the process
		* @return	the PID of the process
		* *****************************************************************************************
		*/
		pid_t getPid() { return m_pid; }

		/*! ***************************************************************************************
		* Returns argc of the process
		* @return	argc value of the process
		* *****************************************************************************************
		*/
		int getArgC() { return m_argc; }

		/*! ***************************************************************************************
		* Returns argv of the process
		* @return	argv value of the process (that is a pointer to an array of pointers to
		*			null-terminated character strings)
		* *****************************************************************************************
		*/
		char** getArgV() { return m_argv; }

		/*! ***************************************************************************************
		* Returns argv value at the specified index
        * @param index the argument index
		* @return	argv[index] pointer to the string stored at the specified index of argv list
		* *****************************************************************************************
		*/
		char* getArgVIndex(int index) { return m_argv[index]; }

		/*! ***************************************************************************************
		* Waits for the end of the process for at maximum the specified timeout. Timeout countdown
		* granularity can be specified.
		* @param	nMsecTimeout maximum number of millisecond to wait fore the process end
		* @param	nMsecCountdownGranularity granularity in millisecond of the timeout countdown
		* @return	true if the process is no more alive, false otherwise
		* *****************************************************************************************
		*/
		bool waitProcess(int nMsecTimeout, int nMsecCountdownGranularity = 1);

		/*! ***************************************************************************************
		* Set the desired priority of the process. This method should be called before the start()
		* Priority ranges between -20 (maximum) and 19 (minimum), default (at creation) is 0.
		* @param	nPriority the desired priority of the process
		* *****************************************************************************************
		*/
		void setDesiredPriority(int nPriority) { m_nPriority = nPriority; }

		/*! ***************************************************************************************
		* Retrieves the exit code of the process.
		* @return	the exit code of the process
		* *****************************************************************************************
		*/
		unsigned int getRetCode() { return m_nRetCode; }

		/*! ***************************************************************************************
		* Builds the string containing the command line using the stored argv list.
		* @param[out]	sCmdLine the command line
		* @param[in]	nMaxCmdLineSize the length of sCmdLine allocated by the caller
		* @return		true if the command line is retrieved, i.e. is shorter than nMaxCmdLineSize
		* *****************************************************************************************
		*/
		bool buildCommandLine(char* sCmdLine, int nMaxCmdLineSize);

		/*! ***************************************************************************************
		* Returns the command line
		* @return		the command line of the process
		* *****************************************************************************************
		*/
		const char* getCommandLine(void) { return m_sCmdLine; }

        /*! ***************************************************************************************
        * Returns sets the command line
        * @param sCmdLine the command line of the process
        * *****************************************************************************************
        */
        void setCmdLineOnlyString(const char* sCmdLine);

        /*! ***************************************************************************************
        * Returns the policy value corrsponding to the input string
        * @param sSchedPolicy the policy as string
        * @param nSchedPolicy [out] the policy as integer
        * @return true if success, false otherwise
        * *****************************************************************************************
        */
        static bool strToIntSchedPolicyConversion(const char* sSchedPolicy, int &nSchedPolicy);

    public:

        static const int MAX_NUM_ARGS = 1024;

    private:

        pid_t m_pid;				// the PID
        char *m_argv[MAX_NUM_ARGS];	// the list of parameters, not longer than MAX_NUM_ARGS
        int m_argc;					// the number of parameters
        char m_sCmdLine[8192];		// the entire command line
        int m_nPriority;			// the desired priority
        int m_nPolicy;
        unsigned int m_nRetCode;	// the retcode once the process has stopped

};

#endif // PROCESS_H

