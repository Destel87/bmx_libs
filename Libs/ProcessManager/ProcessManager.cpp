//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ProcessManager.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the ProcessManager class.
//! @details
//!
//*!****************************************************************************

#include <string>
#include <sstream>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include <termios.h>

#include "ProcessManager.h"
#include "Config.h"
#include "ProcessManagerInclude.h"
#include "TimeWait.h"
#include "TimeMeter.h"
#include "TimeStamp.h"

/* ********************************************************************************************************************
 * MACROS
 * ********************************************************************************************************************
 */
#define SAFE_DELETE(p)							{ if( p != 0 ) { delete p; p = 0; } }
#define SAFE_DELETE_ARRAY(ar)					{ if( ar != 0 ) { delete [] ar; ar = 0; } }
#define DEFAULT_RESPAWN_CYCLE_DURATION_SEC		10
#define DEFAULT_MAX_WAIT_PROCESS_TO_EXIT_SEC	10

using namespace std;

/* ********************************************************************************************************************
 * ProcessManager class implementation
 * ********************************************************************************************************************
 */
int ProcessManager::getRespawnCycleDurationMSec() const
{
	return m_liRespawnCycleDurationMSec;
}

void ProcessManager::setRespawnCycleDurationMSec(int liRespawnCycleDurationMSec)
{
	m_liRespawnCycleDurationMSec = liRespawnCycleDurationMSec;
}

ProcessManager::ProcessManager()
{
	m_bConfigOk = false;
	m_pProcesses.reserve(MAX_NUM_PROCESSES);
	m_liRespawnCycleDurationMSec = DEFAULT_RESPAWN_CYCLE_DURATION_SEC * 1000;
	m_liMaxWaitProcessMSec = DEFAULT_MAX_WAIT_PROCESS_TO_EXIT_SEC * 1000;
	m_bMustStop = false;
	m_bAllAreStopped = false;
}

ProcessManager::~ProcessManager()
{
	m_pProcesses.clear();
}

bool ProcessManager::initFromConfigFile(const char* sConfigFileName)
{

	m_bConfigOk = false;

	/* ****************************************************************************************************************
	 * Read the configuration file and stores the number of processes well configured to be
	 * ordered inside the m_pProcesses vector
	 * ****************************************************************************************************************
	 */
	Config config;
	config_key_t config_keys[] =
	{

		{ CFG_PM_POOL_SPAWN_MODE, (char*)"pm.pool.spawnMode", (char*)"GENERAL",
						T_string, (char*)"parallel", DEFAULT_ACCEPTED},
		{ CFG_PM_POOL_LIST, (char*)"pm.pool.list", (char*)"GENERAL",
						T_string, (char*)"[]", DEFAULT_ACCEPTED},
		{ CFG_PM_CYCLE_DURATION_SEC, (char*)"pm.pool.respawn.cycleDurationSec", (char*)"GENERAL",
						T_int, (char*)"1", DEFAULT_ACCEPTED},
		{ CFG_PM_MAX_PROCESS_EXIT_SEC, (char*)"pm.pool.maxWaitProcessToExitSec", (char*)"GENERAL",
						T_int, (char*)"65", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_0_CMD, (char*)"pm.process.0.cmd", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_0_PREPEND_CMD, (char*)"pm.process.0.prependCmd", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_0_ARGS, (char*)"pm.process.0.args", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_0_BASE_DIR, (char*)"pm.process.0.baseDir", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_0_LIFETIME, (char*)"pm.process.0.lifeTime", (char*)"GENERAL",
						T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_0_POLICY, (char*)"pm.process.0.schedPolicy", (char*)"GENERAL",
						T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_0_PRIORITY, (char*)"pm.process.0.priority", (char*)"GENERAL",
						T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_1_CMD, (char*)"pm.process.1.cmd", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_1_PREPEND_CMD, (char*)"pm.process.1.prependCmd", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_1_ARGS, (char*)"pm.process.1.args", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_1_BASE_DIR, (char*)"pm.process.1.baseDir", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_1_LIFETIME, (char*)"pm.process.1.lifeTime", (char*)"GENERAL",
						T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_1_POLICY, (char*)"pm.process.1.schedPolicy", (char*)"GENERAL",
						T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_1_PRIORITY, (char*)"pm.process.1.priority", (char*)"GENERAL",
						T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_2_CMD, (char*)"pm.process.2.cmd", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_2_PREPEND_CMD, (char*)"pm.process.2.prependCmd", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_2_ARGS, (char*)"pm.process.2.args", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_2_BASE_DIR, (char*)"pm.process.2.baseDir", (char*)"GENERAL",
						T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_2_LIFETIME, (char*)"pm.process.2.lifeTime", (char*)"GENERAL",
						T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_2_POLICY, (char*)"pm.process.2.schedPolicy", (char*)"GENERAL",
						T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_2_PRIORITY, (char*)"pm.process.2.priority", (char*)"GENERAL",
						T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_3_CMD,			(char*)"pm.process.3.cmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_3_PREPEND_CMD,	(char*)"pm.process.3.prependCmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_3_ARGS,		(char*)"pm.process.3.args", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_3_BASE_DIR,	(char*)"pm.process.3.baseDir", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_3_LIFETIME,	(char*)"pm.process.3.lifeTime", (char*)"GENERAL", T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_3_POLICY,		(char*)"pm.process.3.schedPolicy", (char*)"GENERAL", T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_3_PRIORITY,	(char*)"pm.process.3.priority", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_4_CMD,			(char*)"pm.process.4.cmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_4_PREPEND_CMD,	(char*)"pm.process.4.prependCmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_4_ARGS,		(char*)"pm.process.4.args", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_4_BASE_DIR,	(char*)"pm.process.4.baseDir", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_4_LIFETIME,	(char*)"pm.process.4.lifeTime", (char*)"GENERAL", T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_4_POLICY,		(char*)"pm.process.4.schedPolicy", (char*)"GENERAL", T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_4_PRIORITY,	(char*)"pm.process.4.priority", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_5_CMD,			(char*)"pm.process.5.cmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_5_PREPEND_CMD,	(char*)"pm.process.5.prependCmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_5_ARGS,		(char*)"pm.process.5.args", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_5_BASE_DIR,	(char*)"pm.process.5.baseDir", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_5_LIFETIME,	(char*)"pm.process.5.lifeTime", (char*)"GENERAL", T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_5_POLICY,		(char*)"pm.process.5.schedPolicy", (char*)"GENERAL", T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_5_PRIORITY,	(char*)"pm.process.5.priority", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_6_CMD,			(char*)"pm.process.6.cmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_6_PREPEND_CMD,	(char*)"pm.process.6.prependCmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_6_ARGS,		(char*)"pm.process.6.args", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_6_BASE_DIR,	(char*)"pm.process.6.baseDir", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_6_LIFETIME,	(char*)"pm.process.6.lifeTime", (char*)"GENERAL", T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_6_POLICY,		(char*)"pm.process.6.schedPolicy", (char*)"GENERAL", T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_6_PRIORITY,	(char*)"pm.process.6.priority", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_7_CMD,			(char*)"pm.process.7.cmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_7_PREPEND_CMD,	(char*)"pm.process.7.prependCmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_7_ARGS,		(char*)"pm.process.7.args", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_7_BASE_DIR,	(char*)"pm.process.7.baseDir", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_7_LIFETIME,	(char*)"pm.process.7.lifeTime", (char*)"GENERAL", T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_7_POLICY,		(char*)"pm.process.7.schedPolicy", (char*)"GENERAL", T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_7_PRIORITY,	(char*)"pm.process.7.priority", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},

		{ CFG_PM_PROCESS_8_CMD,			(char*)"pm.process.8.cmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_8_PREPEND_CMD,	(char*)"pm.process.8.prependCmd", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_8_ARGS,		(char*)"pm.process.8.args", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_8_BASE_DIR,	(char*)"pm.process.8.baseDir", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_8_LIFETIME,	(char*)"pm.process.8.lifeTime", (char*)"GENERAL", T_string, (char*)"oneshot", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_8_POLICY,		(char*)"pm.process.8.schedPolicy", (char*)"GENERAL", T_string, (char*)"SCHED_OTHER", DEFAULT_ACCEPTED},
		{ CFG_PM_PROCESS_8_PRIORITY,	(char*)"pm.process.8.priority", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED}

	};
	int num_keys = sizeof(config_keys) / sizeof(config_key_t);
	if ( !config.Init(config_keys, num_keys, sConfigFileName) )
	{
		char sError[1024] = "\0";
		Config::GetLastError(sError, 1024);
		log(LOG_ERR, "ProcessManager unable to read config file \"%s\" (err: \"%s\")", sConfigFileName, sError);
		return 1;
	}

	log(LOG_INFO, "ProcessManager configuration using config file \"%s\"", sConfigFileName);

	if ( ( m_pLogger->getLevel() ) >= LOG_DEBUG )
		config.PrintParams();

	int nNumProcessReadFromConfigFile = 0;
	for ( int i = 0; i < MAX_NUM_PROCESSES_FROM_CONFIG_FILE; i++ )
	{
		char sCmd[1024] = "m_nRespawnCycleDurationSec\0";
		sprintf(sCmd, "%s", config.GetString(CFG_PM_PROCESS_0_CMD + i));
		if ( strlen(sCmd) > 0 )
		{
			nNumProcessReadFromConfigFile++;
		}
	}

	/* ****************************************************************************************************************
	 * Parse the pool list string that is something like this -> [1;3;0;...]
	 * and build an array of characters in which the element at index i contains
	 * the process id parsed at position i
	 * This array will be used after to build with the desired order
	 * the vector containing the processes to be monitored
	 * ****************************************************************************************************************
	 */
	string sPoolList;
	sPoolList.assign(config.GetString(CFG_PM_POOL_LIST));
	size_t cSquareBracketOpen = sPoolList.find_first_of('[');
	size_t cSquareBracketClose = sPoolList.find_first_of(']', 1);
	if ( ( cSquareBracketOpen == string::npos ) || ( cSquareBracketClose == string::npos ) )
	{
		log(LOG_ERR, "ProcessManager unable to configure, pool list string bad format <%s>", sPoolList.c_str());
		return false;
	}

	sPoolList = sPoolList.substr(cSquareBracketOpen+1, cSquareBracketClose - cSquareBracketOpen - 1);
	vector<string> tokens;
	string token;
	stringstream tokenStream(sPoolList);
	while ( getline(tokenStream, token, ';') )
	{
		tokens.push_back(token);
	}
	int nNumProcessInPoolList = (int)tokens.size();
	if ( nNumProcessInPoolList > nNumProcessReadFromConfigFile )
	{
		log(LOG_ERR, "ProcessManager unable to configure, pool list length and number of processes differ");
		return false;
	}

	unsigned char* pProcessIndex = new unsigned char[nNumProcessReadFromConfigFile];
	for ( int i = 0; i < nNumProcessInPoolList; i++ )
	{
		int n = 0;
		try
		{
			n = (unsigned char)stoi(tokens[i]);
		}
		catch( invalid_argument )
		{
			SAFE_DELETE_ARRAY(pProcessIndex);
			log(LOG_ERR, "ProcessManager unable to configure, pool list contains a not valid value");
			return false;
		}
		if ( ( n < 0 ) || ( n > nNumProcessReadFromConfigFile ) )
		{
			SAFE_DELETE_ARRAY(pProcessIndex);
			log(LOG_ERR, "ProcessManager unable to configure, pool list contains a not valid value");
			return false;
		}
		char sCmd[256] = "\0";
		sprintf(sCmd, "%s", config.GetString(CFG_PM_PROCESS_0_CMD + n));
		if (strlen(sCmd) == 0)
		{
			SAFE_DELETE_ARRAY(pProcessIndex);
			log(LOG_ERR,
				"ProcessManager unable to configure, pool list has an undefined reference to process at idx=%d", n);
			return false;
		}
		pProcessIndex[i] = (unsigned char)n;
	}

	// Now initializes the pProcesses vector with the right size and the correct order of processes
	m_pProcesses.resize(nNumProcessInPoolList);
	for (int i=0; i<nNumProcessInPoolList; i++ )
	{
		int nIndex = pProcessIndex[i];
		char sFullCmdLine[1024];
		char sCmd[256] = "\0";
		sprintf(sCmd, "%s", config.GetString(CFG_PM_PROCESS_0_CMD + nIndex));
		int nDisplacement = nIndex * CONFIG_KEYS_DISPLACEMENT_INTERPROCESS;
		char sPrepend[256] = "\0";
		sprintf(sPrepend, "%s", config.GetString(CFG_PM_PROCESS_0_PREPEND_CMD + nDisplacement));
		char sBaseDir[256] = "\0";
		sprintf(sBaseDir, "%s", config.GetString(CFG_PM_PROCESS_0_BASE_DIR + nDisplacement));
		char sArgs[256] = "\0";
		sprintf(sArgs, "%s", config.GetString(CFG_PM_PROCESS_0_ARGS + nDisplacement));
		sprintf(sFullCmdLine, "%s %s/%s %s", sPrepend, sBaseDir, sCmd, sArgs);

		int nSchedPolicy = SCHED_OTHER;
		int nPriority = 0;
		if ( Process::strToIntSchedPolicyConversion(config.GetString(CFG_PM_PROCESS_0_POLICY + nDisplacement),
													 nSchedPolicy) )
		{
			nPriority = config.GetInt(CFG_PM_PROCESS_0_PRIORITY + nDisplacement);
		}
		else
		{
			log(LOG_ERR,
				"ProcessManager unable to initialize sched policy for process <%s> at index=%d", sFullCmdLine, i);
			SAFE_DELETE_ARRAY(pProcessIndex);
			return false;
		}

		if ( m_pProcesses[i].m_Process.init(sFullCmdLine, nSchedPolicy, nPriority) == false )
		{
			log(LOG_ERR, "ProcessManager unable to initialize process <%s> at index=%d", sFullCmdLine, nIndex);
			SAFE_DELETE_ARRAY(pProcessIndex);
			return false;
		}
		if ( strcmp( config.GetString(CFG_PM_PROCESS_0_LIFETIME + nDisplacement), "h24" ) == 0 )
		{
			m_pProcesses[i].m_bIsH24 = true;
		}
		else
		{
			m_pProcesses[i].m_bIsH24 = false;
		}
	}

	for ( unsigned int i = 0; i < m_pProcesses.size(); i++ )
	{
		log(LOG_INFO, "ProcessManager configured process[%d]=%s isH24=%d", i,
			m_pProcesses[i].m_Process.getCommandLine(), m_pProcesses[i].m_bIsH24);
	}

	/* ****************************************************************************************************************
	 * Initializes the pool management, if parallel or sequential
	 * ****************************************************************************************************************
	 */
	if ( strcmp( config.GetString(CFG_PM_POOL_SPAWN_MODE), "parallel" ) == 0 )
	{
		m_bSpawnModeIsParallel = true;
		log(LOG_INFO, "ProcessManager configured for parallel management of pool");
	}
	else
	{
		m_bSpawnModeIsParallel = false;
		log(LOG_INFO, "ProcessManager configured for sequential management of pool");
	}

	m_liRespawnCycleDurationMSec = config.GetInt(CFG_PM_CYCLE_DURATION_SEC) * MSEC_PER_SEC;
	m_liMaxWaitProcessMSec = config.GetInt(CFG_PM_MAX_PROCESS_EXIT_SEC) * MSEC_PER_SEC;
	SAFE_DELETE_ARRAY(pProcessIndex);
	m_bConfigOk = true;
	return true;
}

bool ProcessManager::stop(int liTimeoutMsec)
{
	bool bRetValue = m_bAllAreStopped;
	m_bMustStop = true;

	if ( liTimeoutMsec > 0 )
	{
		int liSingleWaitMsec = liTimeoutMsec / 10;
		TimeWait waiter;
		TimeMeter m;
		m.start();
		for (int i = 0; i < 10; i++)
		{
			if ( m_bAllAreStopped == true )
			{
				return true;
			}
			waiter.waitMilliSeconds(liSingleWaitMsec);
		}
		m.stop();
		int liRemaningMsec = liTimeoutMsec - ( m.getDurationNanoSec() / NSEC_PER_MSEC );
		waiter.waitMilliSeconds(liRemaningMsec);
		bRetValue = m_bAllAreStopped;
	}
	return bRetValue;
}

void ProcessManager::beforeWorkerThread()
{
	if ( !m_pLogger)
	{
		m_bConfigOk = false;
	}
}

int ProcessManager::workerThread()
{
	if ( m_bConfigOk == false )
		return 1;

	/* ****************************************************************************************************************
	 * START THE TASKS SPAWNING MANAGEMENT
	 * ****************************************************************************************************************
	 */
	log(LOG_INFO, "ProcessManager: starting");
	char sSpawnManagement[32] = "SEQUENTIAL";
	if ( m_bSpawnModeIsParallel ) sprintf(sSpawnManagement, "PARALLEL");
	log(LOG_INFO, "ProcessManager: going %s mode", sSpawnManagement);

	/* ****************************************************************************************************************
	 * SEQUENTIAL SPAWNING
	 * In this case the spawning is done parsing the vector and, for each process:
	 *	-	if the expected lifetime is oneshot -> the end of the process is waited before the
	 *		spawning of the next process, a timeout can be set to switch to the next process
	 *		if the current spawned process seems not to reach an end
	 *	-	if the expected lifetime is h24 -> the process is spawned and we switch to the
	 *		spawning of the next one without any wait
	 * ****************************************************************************************************************
	 */
	bool bThereIsAtLeastOneH24 = false;
	if ( ! m_bSpawnModeIsParallel )
	{
		for ( int i = 0; i < (int)m_pProcesses.size(); i++ )
		{
			if ( m_pProcesses[i].m_bIsH24 == true )
				bThereIsAtLeastOneH24 = true;
			log(LOG_INFO, "ProcessManager: starting process \"%s\"",
				m_pProcesses[i].m_Process.getCommandLine());
			if ( !m_pProcesses[i].m_Process.start() )
			{
				log(LOG_ERR, "ProcessManager: unable to start process \"%s\"",
					m_pProcesses[i].m_Process.getCommandLine());
			}
			else
			{
				usleep(1000);
				if ( m_pProcesses[i].m_Process.isAlive() )
				{
					log(LOG_INFO, "ProcessManager: process \"%s\" started", m_pProcesses[i].m_Process.getCommandLine());
				}
				int liCntdownGranularityMSec = 1000;
				bool bWait = m_pProcesses[i].m_Process.waitProcess(m_liMaxWaitProcessMSec, liCntdownGranularityMSec);
				if ( ! bWait )
				{
					 log(LOG_ERR, "ProcessManager: process \"%s\" has not finished within the timeout=%d",
						 m_pProcesses[i].m_Process.getCommandLine(), m_liMaxWaitProcessMSec);
				}
				else
				{
					unsigned int nProcessRetode = m_pProcesses[i].m_Process.getRetCode();
					log(LOG_INFO, "ProcessManager: process \"%s\" finished with retcode=%d",
						m_pProcesses[i].m_Process.getCommandLine(), nProcessRetode);
				}
			}
		}
	}

	/* ****************************************************************************************************************
	 * PARALLEL SPAWN
	 * In this case the spawning is done parsing the vector and each process is spawned without
	 * waiting for its ending
	 * ****************************************************************************************************************
	 */
	if ( m_bSpawnModeIsParallel )
	{
		for ( int i = 0; i < (int)m_pProcesses.size(); i++ )
		{
			if ( m_pProcesses[i].m_bIsH24 == true )
				bThereIsAtLeastOneH24 = true;
			log(LOG_INFO, "ProcessManager: starting process \"%s\"",
				m_pProcesses[i].m_Process.getCommandLine());

			bool bStarted = m_pProcesses[i].m_Process.start();
			if ( ! bStarted )
			{
				log(LOG_ERR, "ProcessManager: unable to start process \"%s\"",
					m_pProcesses[i].m_Process.getCommandLine());
			}
			else
			{
				usleep(1000);
				if ( m_pProcesses[i].m_Process.isAlive() )
				{
					log(LOG_INFO, "ProcessManager: process \"%s\" started",
						m_pProcesses[i].m_Process.getCommandLine());
				}
			}
		}
	}

	/* ****************************************************************************************************************
	 * RESPAWNING THE H24s
	 * If some process in the pool is set to h24 and it is found dead then it is respawned
	 * This thread cycles infinitely until somebody kills it
	 * ****************************************************************************************************************
	 */
	TimeWait waiter;
	TimeMeter t;
	uint64_t ulliRemainingTimeToWait = 0;
	int nCounter = 0;
	while ( isRunning() || ( ! m_bMustStop ) )
	{
		t.start();
		if ( bThereIsAtLeastOneH24 )
		{
			log(LOG_DEBUG, "ProcessManager: respawning cycle num %d", nCounter);
			for ( int i = 0; i < (int)m_pProcesses.size(); i++ )
			{
				if ( m_pProcesses[i].m_bIsH24 )
				{
					if ( ! m_pProcesses[i].m_Process.isAlive() )
					{
						log(LOG_WARNING, "ProcessManager: found process \"%s\" dead, try to respawn...",
							m_pProcesses[i].m_Process.getCommandLine());
						if ( m_pProcesses[i].m_Process.respawn() == IS_DOWN )
						{
							log(LOG_WARNING, "ProcessManager: unable to respawn process \"%s\"",
								m_pProcesses[i].m_Process.getCommandLine());
						}
					}
					else
					{
						log(LOG_DEBUG_PARANOIC, "ProcessManager: \"%s\" is alive",
							m_pProcesses[i].m_Process.getCommandLine());
					}
				}
			}
		}
		else
		{
                        unsigned int nNumActiveProcesses = m_pProcesses.size();
                        for ( unsigned int i = 0; i < m_pProcesses.size(); i++ )
			{
				if ( ! m_pProcesses[i].m_Process.isAlive() )
				{
                                    nNumActiveProcesses--;
				}
			}
			if ( nNumActiveProcesses < m_pProcesses.size() )
			{
				log(LOG_INFO, "ProcessManager: some process is dead, exit");
				break;
			}
		}
		usleep(5000);
		t.stop();
		ulliRemainingTimeToWait = ( m_liRespawnCycleDurationMSec ) - ( t.getDurationNanoSec() / NSEC_PER_MSEC);
		waiter.waitMilliSecondsClock(ulliRemainingTimeToWait);
		nCounter++;
		if ( m_bMustStop )
		{
			log(LOG_DEBUG, "ProcessManager: stop received");
			break;
		}
	}

	/* ********************************************************************************************
	 * CLOSE ALL THE PROCESSES
	 * ********************************************************************************************
	 */
	int nNumActiveProcesses = m_pProcesses.size();
	while ( nNumActiveProcesses > 0 )
	{
		log(LOG_INFO, "ProcessManager: trying to stop %d processes", nNumActiveProcesses);
		for ( int i = 0; i < (int)m_pProcesses.size(); i++ )
		{
			bool b = m_pProcesses[i].m_Process.isAlive();
			if ( b == true )
			{
				log(LOG_INFO, "ProcessManager: stopping \"%s\" ", m_pProcesses[i].m_Process.getCommandLine());

#ifdef KILL_TEST_PROC
				//TODO maybe here we should send kill signal in order to stop the other process safety

				printf("<<<<<<<<<<<<<<<<Kill with SIGTERM\n");
				pid_t ubPidCurrent = m_pProcesses[i].m_Process.getPid();
				printf("<<<<<<<<<<<<<<<< PID current%d\n", ubPidCurrent);

				kill(ubPidCurrent, SIGTERM);

				usleep(10000000);
#else
				m_pProcesses[i].m_Process.stop();
				usleep(1000);
#endif
				if ( m_pProcesses[i].m_Process.isAlive() == false )
				{
					nNumActiveProcesses--;
					log(LOG_INFO, "ProcessManager: %d stopped \"%s\"",
						nNumActiveProcesses, m_pProcesses[i].m_Process.getCommandLine());
				}
			}
			else
			{
				nNumActiveProcesses--;
				log(LOG_INFO, "ProcessManager: %d stopped \"%s\"",
					nNumActiveProcesses, m_pProcesses[i].m_Process.getCommandLine());
			}
			if ( nNumActiveProcesses <= 0 )
			{
				break;
			}
		}
		if ( nNumActiveProcesses <= 0 )
		{
			m_bAllAreStopped = true;
			log(LOG_INFO, "ProcessManager: no more processes are running, exit");
			break;
		}
		usleep(10000);
	}

	return 0;
}

void ProcessManager::afterWorkerThread()
{
	log(LOG_DEBUG_PARANOIC, "ProcessManager: closing");
}


int ProcessManager::getProcIdByName(string procName)
{
	int pid = -1;

	// Open the /proc directory
	DIR *dp = opendir("/proc");
	if (dp != NULL)
	{
		// Enumerate all entries in directory until process found
		struct dirent *dirp;
		while (pid < 0 && (dirp = readdir(dp)))
		{
			// Skip non-numeric entries
			int id = atoi(dirp->d_name);
			if (id > 0)
			{
				// Read contents of virtual /proc/{pid}/cmdline file
				string cmdPath = string("/proc/") + dirp->d_name + "/cmdline";
				ifstream cmdFile(cmdPath.c_str());
				string cmdLine;
				getline(cmdFile, cmdLine);
				if (!cmdLine.empty())
				{
					// Keep first cmdline item which contains the program path
					size_t pos = cmdLine.find('\0');
					if (pos != string::npos)
						cmdLine = cmdLine.substr(0, pos);
					// Keep program name only, removing the path
					pos = cmdLine.rfind('/');
					if (pos != string::npos)
						cmdLine = cmdLine.substr(pos + 1);
					// Compare against requested process name
					if (procName == cmdLine)
						pid = id;
				}
			}
		}
	}

	closedir(dp);

	return pid;
}
