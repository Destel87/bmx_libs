QT          -= core gui
TARGET      = ProcessManager
TEMPLATE    = lib
CONFIG      += staticlib

HEADERS += \
	Process.h \
        ProcessManager.h \
        ProcessManagerInclude.h

SOURCES += \
	Process.cpp \
        ProcessManager.cpp

# internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../Config/ -lConfig
INCLUDEPATH += $$PWD/../Config
DEPENDPATH += $$PWD/../Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Config/libConfig.a

unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../TimeStamp
DEPENDPATH += $$PWD/../TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../TimeStamp/libTimeStamp.a

