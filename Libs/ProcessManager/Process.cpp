//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Process.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the Process class.
//! @details
//!
//*!****************************************************************************

#include "Process.h"

#include <sys/types.h>
#include <signal.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sched.h>


#define RET_CODE_ERROR				0xffffffff
#define DEFAULT_PROCESS_PRIORITY	0
// ritorna la differenza (in millisecondi) tra due timestamp
static int tv_diff_millisec(struct timeval t1, struct timeval t2)
{
	int t1_millisec = (int)( (long long)t1.tv_sec * 1000 + t1.tv_usec / 1000 );
	int t2_millisec = (int)( (long long)t2.tv_sec * 1000 + t2.tv_usec / 1000 );
	return (t1_millisec - t2_millisec);
}

static void msleep(int msec)
{
	usleep(msec * 1000);
}


Process::Process()
{
	m_pid = -1;
	m_argc = 0;
	memset(&m_argv[0], 0, sizeof(m_argv) );
	m_nRetCode = RET_CODE_ERROR;
	strcpy(m_sCmdLine, "");
	m_nPolicy = SCHED_OTHER;
	m_nPriority = DEFAULT_PROCESS_PRIORITY;
}

Process::~Process()
{
	stop();
	m_nRetCode = RET_CODE_ERROR;
	clearCommandLine();
}

bool Process::isAlive(void)
{
	if ( m_pid < 0 )
	{
		//printf("is alive returns false\n");fflush(stdout);
		return false;
	}
	bool ret = true;
	int rc;
	ret = ( waitpid(m_pid, &rc, WNOHANG) != m_pid );
	if ( !ret && WIFEXITED(rc) )
	{
		m_nRetCode = WEXITSTATUS(rc);
		//printf("m_nRetCode = %d\n----------------------------\n", m_nRetCode);fflush(stdout);
	}
	if ( !ret )
	{
		// Process is terminated
		//printf("Calling stop() inside isAlive\n");fflush(stdout);
		stop();
	}
	return ret;
}

bool Process::init(const char *sCmdLine, int nSchedPolicy, int nPriority)
{
	int offset_index;
	char cmd_tmp[8192];
	char* cp;

	//printf("CmdLine = <%s>\n", sCmdLine);
	if ( sCmdLine == NULL )
	{
		return false;
	}
	if ( ( (int)strlen(sCmdLine) <= 0 ) || ( strlen(sCmdLine) >= 8192 ) )
	{
		return false;
	}
	strcpy(cmd_tmp, sCmdLine);

	// To be sure...
	reset();

	m_argc = 0;
	offset_index = 0;
	// Parse the command line until a space char is found
	while ( (cmd_tmp[offset_index] == ' ') && (offset_index < (int)strlen(cmd_tmp)) )
	{
		offset_index++;
	}
	// Arguments parsing
	while ( offset_index < (int)strlen(cmd_tmp) )
	{
		char arg[8192];
		int arg_size;
		char* offset = &cmd_tmp[offset_index];
		// Searches for a space character
		cp = strchr(offset, ' ');
		if (cp == NULL)
		{
			// We reached the end of the cmd line, we look for the argv lenght
			arg_size = strlen(cmd_tmp) - offset_index;
		}
		else
		{
			arg_size = cp - offset;
		}
		if (arg_size == 0)
		{
			break;
		}

		// We have to save the arguments
		offset_index += arg_size;
		// Searches for a space character
		while ( (cmd_tmp[offset_index] == ' ') && (offset_index < (int)strlen(cmd_tmp)) )
		{
			offset_index++;
		}
		// Save the actual argv
		strncpy(arg, offset, arg_size);
		arg[arg_size] = 0;
		m_argv[m_argc] = strdup(arg);
		//printf("m_argv[%d] = %s\n", m_argc, m_argv[m_argc]);

		// Increments the number of arguments
		m_argc++;

	}
	//printf("m_argc = %d\n", m_argc);

	// Default priority is normal = 0
	if ( ( nSchedPolicy != SCHED_OTHER ) && ( nSchedPolicy != SCHED_FIFO ) && ( nSchedPolicy != SCHED_RR ) )
	{
		printf("Cannot set scheduling policy, unknown policy value %d", nSchedPolicy);
		return false;
	}
	m_nPolicy = nSchedPolicy;
	m_nPriority = 0;

	if ( m_nPolicy != SCHED_OTHER )
	{
		if ( ( nPriority < 1 ) || ( nPriority > 99 ) )
		{
			printf("Cannot set scheduling policy, priority out-of-range ");
			return false;
		}
		m_nPriority = nPriority;
	}

	// Save the command line
	strcpy(m_sCmdLine, sCmdLine);

	// Reset the process return code
	m_nRetCode = RET_CODE_ERROR;

	return true;

}

void Process::clearCommandLine(void)
{
	int i;
	for (i = 0; i < MAX_NUM_ARGS; i++)
	{
		if (m_argv[i] != NULL)
		{
			free(m_argv[i]);
			m_argv[i] = NULL;
		}
	}
	strcpy(m_sCmdLine, "");
}

bool Process::start(void)
{
	if ( isAlive() )
	{
		// Process is already alive
		return false;
	}

	if ( m_argc <= 0 )
	{
		// This process cannot be started because m_argv[m_argc==0] is the process executable name
		return true;
	}

//	char** argv = m_argv;
//	while (*argv != NULL)
//	{
//		printf("DEBUG: argv = %s\n",*argv);
//		argv++;
//	}

	pid_t temp_pid;
	temp_pid = fork();
	if ( temp_pid == 0 )
	{
		// Set process policy and priority
		struct sched_param schedp;
		memset(&schedp, 0, sizeof(schedp));
		schedp.sched_priority = m_nPriority;
		int nRetSetScheduler = sched_setscheduler(temp_pid, m_nPolicy, &schedp);
		if ( nRetSetScheduler < 0 )
		{
			printf("Process unable to set policy and priority\n");
			return false;
		}

		// Son has been created...
		if ( execv(m_argv[0], m_argv) < 0 )
		{
			//fprintf(stderr, "Start di %s failed\n", m_argv[0]);
			//exit(1);
			return false;
		}
	}

	// Control comes back to the father...
	if ( temp_pid <= 0 )
	{
		// Impossible to create the son
		return false;
	}

	// Save the PID
	m_pid = temp_pid;

	return true;

}

void Process::stop(void)
{
	if ( m_pid >= 0 )
	{
		int rc;
		kill(m_pid, SIGKILL);
		// Notifies the father that the child is dead (to avoid the defunct)
		//waitpid(m_pid, NULL, WNOHANG);
		waitpid(m_pid, &rc, 0);
		if ( m_nRetCode == RET_CODE_ERROR )
		{
			if ( WIFEXITED(rc) )
			{
				if ( m_nRetCode != RET_CODE_ERROR )
				{
					m_nRetCode = WEXITSTATUS(rc);
				}
			}
		}
		m_pid = -1;
	}
}

void Process::reset(void)
{
	stop();
	clearCommandLine();
	m_nRetCode = 0;
}

int Process::respawn(int nMsecSleepBeforeRestart)
{
	if ( isAlive() )
	{
		return IS_ALIVE;
	}
	if ( nMsecSleepBeforeRestart > 0 )
	{
		msleep(nMsecSleepBeforeRestart);
	}
	if ( start() )
	{
		return IS_RESTART;
	}
	return IS_DOWN;
}

bool Process::waitProcess(int nMsecTimeout, int nMsecCountdownGranularity)
{
	bool ret = false;
	// Executes many one-millisecond sleeps
	struct timeval start_tv, current_tv;
	gettimeofday(&start_tv, NULL);
	while ( true )
	{
		// Waits...
		usleep(nMsecCountdownGranularity * 1000);
		// Checks if the process is still alive
		if ( !isAlive() )
		{
			ret = true;
			stop();
			break;
		}
		// Checks if the timeout is over
		gettimeofday(&current_tv, NULL);
		int diff = tv_diff_millisec(current_tv, start_tv);
		if ( diff >= nMsecTimeout )
		{
			break;
		}
	}
	return ret;
}

bool Process::buildCommandLine(char* sCmdLine, int nMaxCmdLineSize)
{
	int i;
	strcpy(sCmdLine, "");
	for (i = 0; i < (int)m_argc; i++)
	{
		if ( (strlen(sCmdLine) + strlen(m_argv[i])) > (size_t)nMaxCmdLineSize )
		{
			return false;
		}
		strcat(sCmdLine, m_argv[i]);
		if ( i != ( m_argc - 1 ) )
		{
			strcat(sCmdLine, " ");
		}
	}
	return true;
}

void Process::setCmdLineOnlyString(const char* sCmdLine)
{
	strcpy(m_sCmdLine, sCmdLine);
	m_argc = 1;
	memset(&m_argv[0], 0, sizeof(m_argv) );
	strcpy(m_sCmdLine, sCmdLine);
	m_argv[0] = strdup(m_sCmdLine);
}

bool Process::strToIntSchedPolicyConversion(const char* sSchedPolicy, int &nSchedPolicy)
{
	nSchedPolicy = 99;
	if ( strcmp(sSchedPolicy, "SCHED_OTHER") == 0 )
	{
		nSchedPolicy = SCHED_OTHER;
	}
	else if ( strcmp(sSchedPolicy, "SCHED_FIFO") == 0 )
	{
		nSchedPolicy = SCHED_FIFO;
	}
	else if ( strcmp(sSchedPolicy, "SCHED_RR") == 0 )
	{
		nSchedPolicy = SCHED_RR;
	}
	else
	{
		return false;
	}
	return true;
}
