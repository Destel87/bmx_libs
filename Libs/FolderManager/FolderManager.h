//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    FolderManager.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the FolderManager class.
//! @details
//!
//*!****************************************************************************

#ifndef FOLDERMANAGER_H
#define FOLDERMANAGER_H

#include <string>
#include <string.h>
#include <vector>

using namespace std;

class FolderManager
{
	public:

		FolderManager();
		FolderManager(string& sAbsFileNamePath);
		virtual ~FolderManager();

		string getFileName(void);
		int loadFileName(string& sAbsFileNamePath);
		bool isDir(void);
		bool isFile(void);
		int makeDir(void);
		int makeDir(string& sPath);
		bool exists(void);
		int deleteFile(void);
		int deleteEmptyDir(void);
		int deleteDirRecursively(const char* dirName);
		int isDirEmpty(const string& sDirectory);
		int getElementsInDirectory(vector<string>& vsOut, const string& sDirectory);
		int listFiles(vector<FolderManager *> &listFiles, string sFilePath);
		int getFilesInDirectory(vector<string> &vsOut, const string &sDirectory);
		int getFilesInDirectoryFilter(vector<string> &vsOut, const string &sDirectory, const string &sType);
		string getFileNameWidthOutExt(const string& sFileName);
		void getFileLen(unsigned long &ullFileLen);
		void reset(void);
		bool readFile(string& sFileName, uint8_t* rgcFile, unsigned long lliFileSize);
		bool copyFile(string& strFileDest);

	private:

		string getFileExt(const string& sFileName);
		bool fileExists(void);
		bool dirExists(void);

    public:

        static const string m_fs;
        static const string m_crlf;
        static const char m_cfs;
        static const string  m_KeyTar;
        static const string  m_KeyTxt;
        static const string  m_KeyXml;

    private:

        string m_sFilename;
        string m_sAbsoluteFilename;
        string m_sFilePath;
        bool m_isaFile;

};

#endif // FOLDERMANAGER_H
