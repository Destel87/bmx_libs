//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    CRCFile.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the CRCFile class.
//! @details
//!
//*!****************************************************************************

#include "CRCFile.h"
#include "FolderManagerCommonInclude.h"
#include <iostream>
#include <fstream>

CRCFile::CRCFile(Log* pLogger)
{
	m_pLogger = pLogger;
    made_table=0;
}

CRCFile::~CRCFile()
{

}

int CRCFile::calculateCRCFile(const string& sFileName, unsigned long& ullCRCvalue)
{
	unsigned long ulTmpCrc;
	unsigned char ucBufferValue = 0;
	unsigned long ulFileLenght;

	ifstream pfile;
	ulTmpCrc = CRC32_INITVALUE;//FCA 20150519 fixed initial value crc32

	pfile.open(sFileName.c_str(), ios::in|ios::binary);

	if ( pfile.is_open() )
	{
		pfile.seekg(0, ios::end);
		ulFileLenght = pfile.tellg();
		pfile.seekg(0, ios::beg);

		while(1)
		{
			//load file in the buffer
			pfile.read((char*)&ucBufferValue, 1);

			if ( pfile.eof() )
			{
				ulTmpCrc ^= CRC32_XORVALUE;
				m_pLogger->log(LOG_INFO, "CRCFile::calculateCRCFile: CRC32file %s lenght = %d CRC = %08X", sFileName.c_str(), ulFileLenght, ulTmpCrc);
				break;
			}
			calc32crc((unsigned long*)&ulTmpCrc, ucBufferValue);
		}
		pfile.close();
	}
	else
	{
		m_pLogger->log(LOG_ERR, "CRCFile::calculateCRCFile: error file %s not exist",sFileName.c_str());
		return -1;
	}

	ullCRCvalue = ulTmpCrc;
	return 0;
}


int CRCFile::calculateCRCFileMaxAlloc(const string& sFileName, unsigned long& ulCRCvalue, unsigned long ulMaxAlloc)
{
	unsigned long ulTmpCrc = CRC32_INITVALUE;
	char *pcBuffVal;
	pcBuffVal = NULL;
	ulCRCvalue = 0;

	ifstream pfile;
	pfile.open(sFileName.c_str(),ios::in|ios::binary);

	if ( pfile.is_open() )
	{
		pfile.seekg(0, ios::end);
		int liFileLenght= pfile.tellg();
		pfile.seekg(0, ios::beg);

		if ( liFileLenght > 0 )
		{
			unsigned long ptMemFile = 0;
			unsigned long ulLastAlloc = 0;
			unsigned long ulTmpAlloc = ulMaxAlloc;

			while (1)
			{
				if ( (ptMemFile >= (unsigned long)liFileLenght) || pfile.eof() )
				{
					ulTmpCrc = ulTmpCrc ^ CRC32_XORVALUE;
					break;
				}

				if ( (liFileLenght-ptMemFile) > ulMaxAlloc )
				{
					ulTmpAlloc = ulMaxAlloc;
				}
				else
				{
					ulTmpAlloc = liFileLenght - ptMemFile;
				}

				// condition : last allocation not equal to request or pbuff == NULL
				if ( (ulLastAlloc != ulTmpAlloc) || (pcBuffVal == NULL) )
				{
					// condition pbuffval!=NULL lastalloc to change with new request
					if ( pcBuffVal != NULL )
					{
						delete[] pcBuffVal;
					}

					pcBuffVal = (char*)new char[ulTmpAlloc];
					ulLastAlloc = ulTmpAlloc;
				}

				pfile.read(pcBuffVal, ulTmpAlloc);
				ptMemFile += ulTmpAlloc;

				for (unsigned long idxCrc = 0; idxCrc < ulTmpAlloc; idxCrc++)
				{
					calc32crc((unsigned long*)&ulTmpCrc, (unsigned char)pcBuffVal[idxCrc]);
				}
			}

			if ( pcBuffVal != NULL )
			{
				delete[] pcBuffVal;
			}
		}
		pfile.close();
	}
	else
	{
		m_pLogger->log(LOG_ERR, "CRCFile::calculateCRCFileMaxAlloc: error file %s not exist", sFileName.c_str());
		return -1;
	}
	ulCRCvalue = ulTmpCrc;
	return 0;
}

unsigned long CRCFile::get32crc(const unsigned char* pBuf, int liLen)
{
	unsigned long liCrc;
	int liCounter;
	const unsigned char *p;

	liCounter = liLen;
	liCrc = CRC32_INITVALUE;
	p = pBuf;
	while( --liCounter >= 0 )
		liCrc = ((liCrc>>8) & 0x00FFFFFF) ^ crc32tab[ (liCrc^*p++) & 0xFF ];
	return( liCrc^CRC32_XORVALUE );
}

void CRCFile::calc32crc(unsigned long* pCrc32, unsigned char cVal)
{
	register unsigned long ulCrc;
	ulCrc = *pCrc32;
	*pCrc32 = ((ulCrc>>8) & 0x00FFFFFF) ^ crc32tab[ (ulCrc^cVal) & 0xFF ];
	*pCrc32 &=0xFFFFFFFF;
}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ CRC16FileMaxAlloc- Calculate CRC16File on entire file passed as parameters+
+					and returns value allocating slot of size maxAlloc		+
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
int CRCFile::CRC16FileMaxAlloc(const string &filename,unsigned short &_crc16value,unsigned long maxAlloc)
{
    int err = -1;
    unsigned short tmpcrc;
    char *pbuffVal;
    unsigned long tmpAlloc,lastAlloc,ptMemFile;
    unsigned long filelenght,idxcrc;
//	int fileread;
    ifstream pfile;
    tmpcrc=_crc16value;
    pbuffVal=NULL;
    pfile.open(filename.c_str(),ios::in|ios::binary);
    if (pfile.is_open())
    {
        pfile.seekg(0, ios::end);
        filelenght= pfile.tellg();
        pfile.seekg(0, ios::beg);
        if (filelenght>0)
        {
            ptMemFile=0;
            lastAlloc=0;
            tmpAlloc=maxAlloc;
            while (1)
            {
                if ((ptMemFile>=filelenght) || (pfile.eof()))
                {
                    err=SUCCESS;
                    break;
                }

                if ((filelenght-ptMemFile)>maxAlloc)
                {
                    tmpAlloc=maxAlloc;
                }else
                {
                    tmpAlloc=(filelenght-ptMemFile);
                }
                // condition : last allocation not equal to request
                //			   or pbuff==NULL
                if ((lastAlloc!=tmpAlloc)||(pbuffVal==NULL))
                {
                    // condition pbuffval!=NULL lastalloc to change with new request
                    if (pbuffVal!=NULL)
                    {
                        delete []pbuffVal;
                    }
                    pbuffVal=(char*)new char[tmpAlloc];
                    lastAlloc=tmpAlloc;
                    if (pbuffVal==NULL)
                    {
                        return -1;
#if (LOG_ON==1)
                        printf(" ### Error file %s allocate buffer ###\n",filename.c_str());
#endif
                        break;
                    }
                }
                pfile.read(pbuffVal, tmpAlloc);
                ptMemFile+=tmpAlloc;
                for (idxcrc=0; idxcrc<tmpAlloc; idxcrc++)
                {
                    crc16_seq((short*)&tmpcrc,(unsigned char)pbuffVal[idxcrc]);
                }
            }
            //////////////////////////////////
            if (pbuffVal!=NULL)
            {
                delete [] pbuffVal;
            }
        }//if (filelenght>0)
        pfile.close();
    }//if (pfile.is_open())
    else
    {
#if (LOG_ON==1)
        printf(" ### Error file %s not exist ###\n",filename.c_str());
#endif
    }
    crcvalue=tmpcrc;
    _crc16value=tmpcrc;
    return err;
}

/*
 * Should be called before any other crc function.
 */
void CRCFile::init_crc16(void)
{
    int i,j;
    short crc;
    if (!made_table) {
    for (i=0; i<256; i++) {
        crc = (i << 8);
        for (j=0; j<8; j++)
        crc = (crc << 1) ^ ((crc & 0x8000) ? POLY : 0);
        crc16_table[i] = crc & 0xFFFF;
    }
    made_table=1;
    }
}


/*
 * For a byte array whose accumulated crc value is stored in *crc, computes
* resultant crc obtained by appending m to the byte array
*/
void CRCFile::crc16_seq(short *crc, unsigned char m)
{
    if (!made_table)
    init_crc16();
    *crc = crc16_table[(((*crc) >> 8) ^ m) & 0xFF] ^ ((*crc) << 8);
    *crc &= 0xFFFF;
}


