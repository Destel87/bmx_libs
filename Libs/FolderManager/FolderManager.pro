#-------------------------------------------------
#
# Project created by QtCreator 2020-03-26T15:59:36
#
#-------------------------------------------------

QT       -= core gui

TARGET = FolderManager
TEMPLATE = lib
CONFIG += staticlib


SOURCES += FolderManager.cpp \
    CRCFile.cpp

HEADERS += FolderManager.h \
    FolderManagerCommonInclude.h \
    CRCFile.h

# internal libraries inclusion

unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a
