//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    FolderManager.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the FolderManager class.
//! @details
//!
//*!****************************************************************************

#include "FolderManager.h"

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#define READ_ONLY	"r"

const string FolderManager::m_fs = "/";
const string FolderManager::m_crlf = "\n";
const char FolderManager::m_cfs = '/';

const string FolderManager::m_KeyTar = ".tar";
const string FolderManager::m_KeyTxt = ".txt";
const string FolderManager::m_KeyXml = ".xml";

FolderManager::FolderManager()
{
	m_sFilename.clear();
	m_sAbsoluteFilename.clear();
	m_sFilePath.clear();
	m_isaFile = false;
}

FolderManager::FolderManager(string &sAbsFileNamePath)
{
	m_sFilename.clear();
	m_sAbsoluteFilename.clear();
	m_sFilePath.clear();
	m_isaFile = false;

	FolderManager::loadFileName(sAbsFileNamePath);
}

FolderManager::~FolderManager()
{

}

string FolderManager::getFileName()
{
	return m_sFilename;
}

int FolderManager::loadFileName(string& sAbsFileNamePath)
{
	if ( sAbsFileNamePath.empty() )		return -1;

	m_sAbsoluteFilename = sAbsFileNamePath;
	if ( isDir() )
	{
		m_sFilePath = sAbsFileNamePath;
		m_isaFile = false;
		return 2;
	}
	else
	{
		size_t usPos = sAbsFileNamePath.rfind(m_fs, sAbsFileNamePath.length());
		if ( usPos != string::npos )
		{
			m_sFilename = sAbsFileNamePath.substr(usPos + 1, sAbsFileNamePath.length() - usPos);
			m_sFilePath = sAbsFileNamePath.substr(0, usPos);
			m_isaFile = true;
			return 1;
		}
		// assign value of absolutefilenamepath to filename if it is only fileName
		if ( sAbsFileNamePath.length() > 0 )
		{
			m_sFilename = sAbsFileNamePath;
			m_isaFile = true;
			return 1;
		}
	}
	return -1;
}

bool FolderManager::isDir(void)
{
	struct stat statBuf;
	if ( stat(m_sAbsoluteFilename.c_str(), &statBuf) == -1)
	{
		// wrong path or sys error
		return false;
	}

	if ( S_ISDIR(statBuf.st_mode) )
	{
		return true;
	}
	return false;
}

bool FolderManager::isFile(void)
{
	struct stat statBuf;
	stat(m_sAbsoluteFilename.c_str(), &statBuf);

	if (S_ISREG(statBuf.st_mode))
	{
		return true;
	}
	return false;
}

int FolderManager::makeDir(void)
{
	int liRes = mkdir(m_sAbsoluteFilename.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
	return liRes;
}

int FolderManager::makeDir(string& sPath)
{
	int liRes = mkdir(sPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
	return liRes;
}

bool FolderManager::exists()
{
	if ( m_isaFile )
	{
		return fileExists();
	}
	return dirExists();
}

int FolderManager::deleteEmptyDir()
{
	int liRes = rmdir(m_sAbsoluteFilename.c_str());
	return liRes;
}

int FolderManager::deleteDirRecursively(const char* dirName)
{
	DIR* dir;
	dir = opendir(dirName);
	if (dir == NULL)
	{
		return -1;
	}

	int liRes = 0;

	struct dirent* entry;
	char path[256];

	while ( (entry = readdir(dir)) != NULL )
	{
		/* Skip the names "." and ".." as we don't want to recurse on them. */
		if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
		{
		   continue;
		}

		snprintf(path, (size_t) PATH_MAX, "%s/%s", dirName, entry->d_name);
		struct stat statBuf;
		if ( ! stat(path, &statBuf) )
		{
			// If it is a directory then go inside it, otherwise delete the item.
			if ( S_ISDIR(statBuf.st_mode) )
			{
				liRes = deleteDirRecursively(path);
			}
			else
			{
				liRes = unlink(path);
			}
		}
	}
	closedir(dir);
	if ( ! isDirEmpty(dirName) )
	{
		liRes = rmdir(dirName);
		return liRes;
	}

	return 0;
}

int FolderManager::deleteFile()
{
	int liRes = remove(m_sAbsoluteFilename.c_str());
	return liRes;
}

int FolderManager::listFiles(vector<FolderManager*>& vFiles, string sFilePath)
{
	vector<string> listFile;
	getFilesInDirectory(listFile, sFilePath);

	for (auto i = listFile.begin(); i != listFile.end(); ++i)
	{
		string sCurrFile = *i;
		FolderManager *pFile = new FolderManager(sCurrFile);
		if ( pFile != NULL )
		{
			vFiles.push_back(pFile);
		}
	}

	return ( vFiles.size() );
}

int FolderManager::isDirEmpty(const string& sDirectory)
{
	DIR* pDir;
	struct dirent* pEntry;
	struct stat statBuf;
	string sFileName;
	string sFullFileName;

	pDir = opendir(sDirectory.c_str());
	if ( pDir == 0 )		return -1;
	while ( (pEntry = readdir(pDir)) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = pEntry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		closedir(pDir);
		return 1;
	}
	closedir(pDir);
	return 0;
}

int FolderManager::getElementsInDirectory(vector<string>& vsOut, const string& sDirectory)
{
	vsOut.clear();

	DIR* pDir;
	struct dirent* pEntry;
	struct stat statBuf;
	string sFileName;
	string sFullFileName;

	pDir = opendir(sDirectory.c_str());
	if ( pDir == 0 )		return -1;
	while ( (pEntry = readdir(pDir)) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = pEntry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		vsOut.push_back(sFullFileName);
	}
	closedir(pDir);
	return 0;
}

int FolderManager::getFilesInDirectory(vector<string>& vsOut, const string& sDirectory)
{
	vsOut.clear();

	DIR* pDir;
	struct dirent* pEntry;
	struct stat statBuf;
	string sFileName;
	string sFullFileName;

	pDir = opendir(sDirectory.c_str());
	if ( pDir == 0 )		return -1;
	while ( (pEntry = readdir(pDir)) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = pEntry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		const bool bIsDirectory = ( statBuf.st_mode & S_IFDIR ) != 0;

		if ( bIsDirectory )
			continue;

		vsOut.push_back(sFullFileName);
	}
	closedir(pDir);
	return 0;
}

int FolderManager::getFilesInDirectoryFilter(vector<string>& vsOut, const string& sDirectory, const string& sType)
{
	DIR *dir;
	struct dirent* entry;
	struct stat statBuf;
	string sFileName("");
	string sFullFileName("");
	string sFileNameExt("");

	dir = opendir(sDirectory.c_str());
	if ( dir == 0 )		return -1;
	while ( (entry = readdir(dir) ) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = entry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		const bool bIsDir = ( statBuf.st_mode & S_IFDIR ) != 0;

		if ( bIsDir )
			continue;

		sFileNameExt = getFileExt(sFileName);

		if ( sFileNameExt.compare(sType) == 0 )
		{
			vsOut.push_back(sFullFileName);
		}
	}
	closedir(dir);
	return 0;
}

string FolderManager::getFileNameWidthOutExt(const string& sFileName)
{
	size_t usPos = sFileName.rfind('.', sFileName.length());

	if ( usPos != string::npos )
	{
		return (sFileName.substr(0, usPos));
	}
	return ("");
}

void FolderManager::getFileLen(unsigned long& ullFileLen)
{
	struct stat statbuf;
	ullFileLen = 0;

	if ( stat(m_sAbsoluteFilename.c_str(), &statbuf) != -1)
	{
		ullFileLen = statbuf.st_size;
	}
}

void FolderManager::reset()
{
	m_sFilename.clear();
	m_sAbsoluteFilename.clear();
	m_sFilePath.clear();
	m_isaFile = false;
}

bool FolderManager::readFile(string& sFileName, uint8_t* rgcFile, unsigned long lliFileSize)
{
	if ( rgcFile == 0 )	return false;
	if ( lliFileSize == 0 )	return false;

	ifstream file(sFileName);

	if ( file )
	{
		while ( file.read((char*)rgcFile, lliFileSize) ) {};

		file.close();
		return true;
	}

	return false;
}

bool FolderManager::copyFile(string& strTarDest)
{
	if ( strTarDest.empty() )	return false;

	// Check if the source exists
	if ( ! exists() )	return false;

	string strTmp(m_sAbsoluteFilename);
	string strDirDest(strTarDest.substr(0, strTarDest.rfind("/")));

    // check if the destination exists
    loadFileName(strDirDest);
	if ( ! exists() )	return false;
    m_sAbsoluteFilename = strTmp;

	ifstream  src(m_sAbsoluteFilename, std::ios::binary);
	ofstream  dst(strTarDest, std::ios::binary);

	dst << src.rdbuf();

	return true;
}

string FolderManager::getFileExt(const string& sFileName)
{
	string sFileExt("");
	size_t usPos = sFileName.rfind('.', sFileName.length());
	if ( usPos != string::npos)
	{
		sFileExt = sFileName.substr(usPos + 1, sFileName.length() - usPos);
	}
	return sFileExt;
}

bool FolderManager::fileExists(void)
{
	FILE *fb = fopen(m_sAbsoluteFilename.c_str(), READ_ONLY);

	if ( fb != NULL )
	{
		fclose(fb);
		return true;
	}
	else
	{
		return false;
	}
}

bool FolderManager::dirExists(void)
{
	struct stat statBuf;

	if ( stat(m_sAbsoluteFilename.c_str(), &statBuf) == 0 && S_ISDIR(statBuf.st_mode) )
	{
		return true;
	}
	else
	{
		return false;
	}
}
