//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    UdpClient.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the UdpClient class.
//! @details
//!
//*!****************************************************************************

#ifndef UDPCLIENT_H
#define UDPCLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>


#include "Loggable.h"

#define DEFAULT_UDP_LOCAL_PORT	 55400	// 8080

class  UdpClient : public Loggable
{

	public:

        /*! ***********************************************************************************************************
		 * @brief UdpClient				void constructor intializes internal members to 0
		 * ************************************************************************************************************
		 */
		UdpClient();

        /*! ***********************************************************************************************************
		 * @brief ~UdpClient				void destructor
		 * ************************************************************************************************************
		 */
		~UdpClient();

        /*! ***********************************************************************************************************
		 * @brief openSocket			to open the socket on the specific port
         * @param lport the udp local port
		 * @return true in case of success, false otherwise
		 * ************************************************************************************************************
		 */
		bool openSocket(int lport = DEFAULT_UDP_LOCAL_PORT);

        /*! ***********************************************************************************************************
		 * @brief sendMessage			to send a message via udp
		 * @param message				the message to send as char array
		 * @param length				the number of bytes
		 * ************************************************************************************************************
		 */
		void sendMessage(const char * message, int length);


	private:
		int m_sockfd;
		struct sockaddr_in	m_local_addr;
};

#endif // UDPCLIENT_H
