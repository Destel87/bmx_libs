//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    UdpClient.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the UdpClient class.
//! @details
//!
//*!****************************************************************************

// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <iostream>

#include <netdb.h>
#include <ifaddrs.h>
#include <linux/if_link.h>

#include "UdpClient.h"

#define ETH0_ETHERNET_NAME "eth0"

UdpClient::UdpClient()
{
	m_sockfd = -1;
	memset(&m_local_addr, 0, sizeof(m_local_addr));
}

UdpClient::~UdpClient()
{
	if(m_sockfd != -1)	close(m_sockfd);
}

bool UdpClient::openSocket(int lport)
{
	// Creating socket file descriptor
	if ( (m_sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 )
	{
		log(LOG_ERR, "UdpClient::openSocket: failed to open socket %s", strerror(errno));
		return false;
	}

	int one = 1;
	if(setsockopt(m_sockfd, SOL_SOCKET, SO_BROADCAST, &one, sizeof(int)) < 0)
	{
		log(LOG_ERR, "UdpClient::openSocket: failed to setopt socket %s", strerror(errno));
		return false;
	}


	struct ifaddrs *ifaddr, *ifa;
	int family; //, s, n;
	char host[NI_MAXHOST];

	if (getifaddrs(&ifaddr) == -1)
	{
		log(LOG_ERR, "UdpClient::openSocket: get address %s", strerror(errno));
		return false;
	}

	/* Walk through linked list, maintaining head pointer so we can free list later */

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
	{
		if (ifa->ifa_addr == NULL)	continue;

		family = ifa->ifa_addr->sa_family;
		// identify Net connection on main ethernet port
		if(strcmp(ifa->ifa_name, ETH0_ETHERNET_NAME) == 0 && family == AF_INET)
		{
			if(getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in),
					host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) != 0)
			{
				log(LOG_ERR, "UdpClient::openSocket: get nameinfo %s", strerror(errno));
				return false;
			}

			log(LOG_INFO, "Udp connection on %s address %s", ifa->ifa_name, host);
			break;
		}
	}
	freeifaddrs(ifaddr);

	// build the submask associated to the ip address (something like: "10.105.192.255")
	std::string strIpAddress;
	strIpAddress.assign(host);
	int posPoint = strIpAddress.find_last_of('.');
	if(posPoint < 0)
	{
		log(LOG_ERR, "UdpClient::openSocket: build IP %s", strIpAddress.c_str());
		return false;
	}
	strIpAddress.erase(posPoint + 1, std::string::npos);
	strIpAddress.append("255");

	// Filling local server information
	m_local_addr.sin_family = AF_INET;
	m_local_addr.sin_port = htons(lport);
	m_local_addr.sin_addr.s_addr = inet_addr(strIpAddress.c_str());

	log(LOG_INFO, "UdpClient::openSocket: OK %s", strIpAddress.c_str());
	return true;
}

void UdpClient::sendMessage(const char * message, int length)
{
	if(m_sockfd == -1) return;

	if(sendto(m_sockfd, message, length, 0,
		   (struct sockaddr *) &m_local_addr, sizeof(m_local_addr)) == -1)
	{
		log(LOG_ERR, "UdpClient::sendMessage: failed %s", strerror(errno));
	}

	log(LOG_DEBUG_PARANOIC, "UdpClient::sendMessage: %s", message);

}
