#-------------------------------------------------
#
# Project created by QtCreator 2019-11-26T15:11:16
#
#-------------------------------------------------

QT       -= core gui
TARGET = UdpClient
TEMPLATE = lib
CONFIG		+= staticlib


# internal libraries inclusion
INCLUDEPATH += $$PWD/../Log
INCLUDEPATH += $$PWD/../Thread

HEADERS += UdpClient.h

SOURCES += UdpClient.cpp

DISTFILES +=



