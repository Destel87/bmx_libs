//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SerialInterface.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the SerialInterface class.
//! @details
//!
//*!****************************************************************************

#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <sys/termios.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "Loggable.h"
#include "Mutex.h"

using namespace std;

/*! *****************************************************************************
 * @brief Represents a Serial device with basic functionalities of read and write
 *******************************************************************************/

class SerialInterface	:	public Loggable
{
	public:

		SerialInterface();

		virtual ~SerialInterface();


		/*! **********************************************************************************************************************
		 * @brief  initInterface initialize USB interface (CDC device class).
         * @param  strName name of the port to be used i.e. /dev/ttyACM0
		 * @param  iBaudrate baudrate of the communication.
		 * @param  iBits bit per word.
         * @param  strParity enable parity or not. Possible inputs: "N" = none, "E" = even, "O" = odd, "M" = mark, or "S" = space.
		 * @param  bHasRts hardware flow control.
		 * @param  bHasXon software flow control.
         * @param  liReadTimeoutMsec read timeout in msec.
		 * @return true if success, false otherwise.
		 * ***********************************************************************************************************************
		 */
        bool initInterface(string strName, int32_t iBaudrate, int32_t iBits, string strParity, bool bHasRts = 0,
                           bool bHasXon = 0, int32_t liReadTimeoutMsec = 0);

		/*! **********************************************************************************************************************
		 * @brief  Open serial communication and set the Parameters saved as members with the initSerial function.
		 * @return -2 if setattr does not work for some reason, -1 if initSerial is not called yet, 0 if succeeded, >0 with errno
		 *		   code due to the failure of open() function.
		 * ***********************************************************************************************************************
		 */
		int openInterface(void);

		/*! **********************************************************************************************************************
		 * @brief  setReadTimeout set time in which the system tries to read from USB channel.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool isOpen(void);

		/*! **********************************************************************************************************************
		 * @brief  readFromSerialTimeout reads num_chars bytes and save them in a buffer.
		 * @param  liTimeoutMsec time when the function monitors the file descriptor.
		 * @return -2 if port is not open, -1 if reading fails, 0 if EOF, number of byte read if success (>0).
		 * ***********************************************************************************************************************
		 */
		bool setReadTimeout(int32_t liTimeoutMsec);

		/*! ***********************************************************************
		* @brief readFromSerialTimeout reads num_chars bytes and save them in a buffer.
        * @param rgBuffer destination of char* characters read on serial.
        * @param uliBufferSize number of characters to be read (int32_t).
		* @param liTimeoutMsec time when the function monitors the file descriptor.
		* @param liOperationResult	if some error occurs, this parameter contains the value of ERRNO
		* @return -1 if reading fails, 0 if EOF, number of byte read if success (>0).
		**************************************************************************/
		int32_t readBufferWithTimeout(void* rgBuffer, uint32_t uliBufferSize, int32_t& liOperationResult, int32_t liTimeoutMsec = 0);

		/*! **********************************************************************************************************************
		 * @brief  Clean everything receiven until when is invoked.
		 * @return -2 if port is not open, -1 if select does not work, 0 in case of success.
		 * ***********************************************************************************************************************
		 */
		int32_t cleanAll(void);

		/*! **********************************************************************************************************************
		 * @brief  writeOnSerial write num_chars bytes from a buffer.
         * @param  rgBuffer where the string I want to write is stored.
         * @param uliBufferSize number of characters to be written  (int32_t).
         * @param liOperationResult	if some error occurs, this parameter contains the value of ERRNO
         * @return -1 if writing fails, number of byte written if success (>0).
		 * ***********************************************************************************************************************
		 */
		int32_t writeBuffer(void* rgBuffer, uint32_t uliBufferSize, int32_t& liOperationResult);

		/*! **********************************************************************************************************************
		 * @brief  close serial communication.
		 * @return -1 if port not open, otherwise result of the closing operation.
		 * ***********************************************************************************************************************
		 */
		int32_t closeInterface(void);

		/*! **********************************************************************************************************************
		 * @brief set the name of the port we want to use.
		 * @param sName port name.
		 * ***********************************************************************************************************************
		 */
		bool setPortName(string sName);

		/*! **********************************************************************************************************************
		 * @brief set the baudrate we want to use.
         * @param liBaudRate value.
		 * ***********************************************************************************************************************
		 */
		void setBaudrate(int32_t liBaudRate);

		/*! **********************************************************************************************************************
		 * @brief set or reset Hardware flow control.
		 * @param bHasRts hardware flow control.
		 * ***********************************************************************************************************************
		 */
		void setHardwareFlowControl(bool bHasRts);

		/*! **********************************************************************************************************************
		 * @brief set or reset Software flow control.
		 * @param bHasXon Software flow control.
		 * ***********************************************************************************************************************
		 */
		void setSoftwareFlowControl(bool bHasXon);

		/*! **********************************************************************************************************************
		 * @brief set parity.
		 * @param strParity can be "E" = even or "O" = odd.
		 * ***********************************************************************************************************************
		 */
		void setParity(string strParity);

		/*! **********************************************************************************************************************
		 * @brief set the data bits.
		 * @param liBits databits (usually 8).
		 * ***********************************************************************************************************************
		 */
		void setDataBits(int32_t liBits);

	private:

		/*! **********************************************************************************************************************
		 * @brief setParameters of the serial communication saved as members.
		 * ***********************************************************************************************************************
		 */
		bool setParameters(void);

		/*! **********************************************************************************************************************
		 * @brief noHang set the line status so that it will not kill our process if the line hangs up.
		 * ***********************************************************************************************************************
		 */
		void noHang(void);

		/*! **********************************************************************************************************************
		 * @brief closeHang set hangup on close on/off.
         * @param on on-off flag
		 * ***********************************************************************************************************************
		 */
		void closeHang(int32_t on);

    private:

        bool			m_bConfigOk;

        int32_t			m_liFD;         // serial file descriptor
        string			m_strName;
        int32_t			m_liBaudrate;
        int8_t			m_cBits;
        string			m_strParity;
        bool			m_bHasRts;     // Hardware Flow Control
        bool			m_bHasXon;     // Software Flow Control
        struct termios	m_stty;

        Mutex           m_mutexWrite;
        Mutex           m_mutexRead;

        int32_t			m_liReadTimeoutMsec;

};

#endif // SERIALINTERFACE_H
