//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Thread.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the Thread class.
//! @details
//!
//*!****************************************************************************

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <cxxabi.h>
#include "Thread.h"

const char* Thread::THREAD_VERSION = "1.0.0";

/*! ***********************************************************************************************
 *  MACROS
 *  ***********************************************************************************************
 */
#define SAFE_DELETE(p) { if (p) {delete p; p = NULL;} }

/*! ***********************************************************************************************
 *  GLOBAL VARIABLES
 *  ***********************************************************************************************
 */
static int rc = 0;

/*! ***********************************************************************************************
 *  Class implementation
 * ************************************************************************************************
 */
Thread::Thread() :
		m_pthread(0),
		m_thread_pid(0),
		m_bThreadRunning(false),
		m_bThreadDone(true),
		m_log_func(NULL),
		m_log_func_handler(NULL)
{
	strcpy(m_class_name, "Thread");
	pthread_mutex_init(&m_mutexThreadEnd, NULL);
	pthread_cond_init(&m_condThreadEnd, NULL);
	pthread_attr_init(&m_pthread_attr);

	memset(&m_sched_param, 0, sizeof(m_sched_param));
	m_sched_param.__sched_priority = 0;
	m_nSchedPolicy =  SCHED_OTHER;
}

Thread::~Thread()
{
	// Nothing to be done here yet...
}

bool Thread::joinThread(int msec)
{
	bool ret = false;
	struct timeval tv;
	struct timespec ts;

	gettimeofday(&tv, NULL);
	// increments
	tv.tv_sec += (msec / 1000);
	tv.tv_usec += (msec % 1000) * 1000;
	// builds timespec
	ts.tv_sec = tv.tv_sec;
	ts.tv_nsec = tv.tv_usec * 1000;
	pthread_mutex_lock(&m_mutexThreadEnd);
	int e = pthread_cond_timedwait(&m_condThreadEnd, &m_mutexThreadEnd, &ts);
	pthread_mutex_unlock(&m_mutexThreadEnd);
	// now join so we wait until thread has -really- finished
	if ( e != ETIMEDOUT )
	{
		pthread_attr_destroy(&m_pthread_attr);
		pthread_join(m_pthread, NULL);
		ret = true;
	}
	return ret;
}


void* Thread::staticThreadCallback(void* data)
{
	Thread *pThread = static_cast < Thread * >(data);
	pThread->m_thread_pid = (int)getpid();
	rc = pThread->threadMain();
	return (void*)(&rc);
}

int Thread::threadMain()
{

	int rc = 0;
	m_bThreadDone = false;
	m_bThreadRunning = true;

	try
	{
		beforeWorkerThread();
		rc = workerThread();
		afterWorkerThread();
	}
	catch (abi::__forced_unwind&)
	{
		threadLog(THREAD_LOG_WARNING, "Forced unwind");
		throw;
	}
	catch (const char *s)
	{
		threadLog(THREAD_LOG_WARNING, "Exception s=%s",s);
		throw;
	}
	catch(...)
	{
		threadLog(THREAD_LOG_WARNING, "Exception not managed");
		throw;
	}

	afterWorkerThread();

	m_bThreadDone = true;
	m_bThreadRunning = false;

	// signal waiting threads that this thread is about to terminate
	pthread_mutex_lock(&m_mutexThreadEnd);
	pthread_cond_broadcast(&m_condThreadEnd);
	pthread_mutex_unlock(&m_mutexThreadEnd);

	return (rc);

}

void Thread::stopThread(int msecTimeout)
{

	bool was_active = false;
	int die_wait = 0;

	if ( m_bThreadDone )
	{
		if ( m_pthread != 0 )
		{
			//was_active = true;
			pthread_attr_destroy(&m_pthread_attr);
			pthread_join(m_pthread, NULL);
			m_pthread = 0;
		}
		return;
	}
	if ( m_pthread != 0 )
	{
		was_active = true;
		m_bThreadRunning = false;
		if ( msecTimeout >= 0 )
		{
			int numWait = msecTimeout / 10;
			if ( numWait < 1 )
			{
				numWait = 1;
			}
			while ( (!m_bThreadDone) && (die_wait++ < 300) )
			{
				usleep(100);
			}
			if ( die_wait >= 300 )
			{
				if ( pthread_cancel(m_pthread) )
				{
					m_pthread = 0;
					threadLog(THREAD_LOG_WARNING, "Impossible to cancel thread");
					return;
				}
			}
		}
		pthread_attr_destroy(&m_pthread_attr);
		pthread_join(m_pthread, NULL);
	}

	m_pthread = 0;
	m_bThreadRunning = false;
	m_bThreadDone = true;

	if ( was_active )
	{
		threadLog(THREAD_LOG_DEBUG, "Thread canceled");
	}
}

bool Thread::startThread(bool detach)
{

    m_bThreadRunning = true;
	m_bThreadDone = false;
	m_thread_pid = 0;
	int err = 0;

    if (detach)
    {
		pthread_attr_setdetachstate(&m_pthread_attr, PTHREAD_CREATE_DETACHED);
	}

	err = pthread_create(&m_pthread, &m_pthread_attr, staticThreadCallback, this);
    if (err == 0)
    {
        // waits for the thread to memorize the pid for at most 30 msec
		int die_wait = 0;
        while ( (m_thread_pid == 0) && (die_wait++ < 30) )
        {
			usleep(100);
		}
	}

    if (err != 0)
    {
        threadLog(THREAD_LOG_ERR, "Impossible to create the thread: %s", strerror(errno));
		m_bThreadRunning = false;
		m_bThreadDone = true;
		return false;
	}

    threadLog(THREAD_LOG_DEBUG, "Thread %d succesfully created", m_thread_pid);

    return true;

}

#define LOGMSG_SIZE 1024

void Thread::threadLog(int level, const char fmt[], ...)
{

    if ( m_log_func == NULL )
    {
		return;
    }

    char tmp_logmsg[LOGMSG_SIZE], log_msg[LOGMSG_SIZE];
    va_list ap;

    va_start(ap, fmt);
    vsnprintf(tmp_logmsg, LOGMSG_SIZE, fmt, ap);
    va_end(ap);
	
    if(snprintf(log_msg, LOGMSG_SIZE, "%s %d: %s", m_class_name, getPid(), tmp_logmsg) < 0)
        return;

    (m_log_func)(level, m_log_func_handler, log_msg);
}


void Thread::setThreadLogFunction(thread_log_func_t func, void* handler)
{
	m_log_func = func;
	m_log_func_handler = handler;
}

const char* Thread::getVersion()
{
	return THREAD_VERSION;
}

bool Thread::getSchedPolicy(int &nSchedPolicy, int &nPriority)
{
	int s = pthread_attr_getschedparam(&m_pthread_attr, &m_sched_param);
	if ( s != 0 )
	{
		threadLog(THREAD_LOG_ERR, "Cannot get scheduling parameters with getschedparam");
		return false;
	}
	nPriority = m_sched_param.__sched_priority;

	s = pthread_attr_getschedpolicy(&m_pthread_attr, &m_nSchedPolicy);
	if ( s != 0 )
	{
		threadLog(THREAD_LOG_ERR, "Cannot get scheduling policy with getschedpolicy", nSchedPolicy);
		return false;
	}
	nSchedPolicy = m_nSchedPolicy;
	return true;
}

bool Thread::setSchedPolicy(int nSchedPolicy, int nPriority)
{

	if ( m_bThreadRunning )
	{
		threadLog(THREAD_LOG_DEBUG, "Cannot set scheduling policy because thread is already running");
		return false;
	}

	if ( ( nSchedPolicy != SCHED_OTHER ) && ( nSchedPolicy != SCHED_FIFO ) && ( nSchedPolicy != SCHED_RR ) )
	{
		threadLog(THREAD_LOG_DEBUG, "Cannot set scheduling policy, unknown policy value %d", nSchedPolicy);
		return false;
	}

	if ( nSchedPolicy != SCHED_OTHER )
	{
		if ( ( nPriority < 1 ) || ( nPriority > 99 ) )
		{
			threadLog(THREAD_LOG_DEBUG, "Cannot set scheduling policy, priority out-of-range");
			return false;
		}
		m_sched_param.__sched_priority = nPriority;
		m_nSchedPolicy = nSchedPolicy;
	}

	int n = pthread_attr_setinheritsched(&m_pthread_attr, PTHREAD_EXPLICIT_SCHED);
	if ( n != 0 )
	{
		threadLog(THREAD_LOG_DEBUG, "Unable to set thread inheritance flag with setinheritsched (err %d)", n);
		return false;
	}

	n = pthread_attr_setschedpolicy(&m_pthread_attr, m_nSchedPolicy);
	if ( n != 0 )
	{
		threadLog(THREAD_LOG_DEBUG, "Unable to set thread scheduling policy with setschedpolicy (err %d)", n);
		return false;
	}

	n = pthread_attr_setschedparam(&m_pthread_attr, &m_sched_param);
	if ( n != 0 )
	{
		threadLog(THREAD_LOG_DEBUG, "Unable to set thread parameters with setschedparam (err %d)", n);
		return false;
	}
	return true;
}

void Thread::getSchedPolicyInfoString(char* sSchedPolicyInfoString)
{
	sprintf( sSchedPolicyInfoString, "policy=%s, priority=%d",
			(m_nSchedPolicy == SCHED_FIFO)  ? "SCHED_FIFO" :
			(m_nSchedPolicy == SCHED_RR)    ? "SCHED_RR" :
			(m_nSchedPolicy == SCHED_OTHER) ? "SCHED_OTHER" :
			"???",
			m_sched_param.sched_priority);
}


bool Thread::setCpuAffinity(int nCpu)
{
	if ( m_bThreadRunning )
	{
		threadLog(THREAD_LOG_DEBUG, "Cannot set cpu affinity because thread is already running");
		return false;
	}
	cpu_set_t mask;
	CPU_ZERO(&mask); // Clears set, so that it contains no CPUs
	CPU_SET(nCpu, &mask);
	int nError = pthread_setaffinity_np(pthread_self(), sizeof(mask), &mask);
	if ( nError )
	{
		threadLog(THREAD_LOG_DEBUG, "Unable to set cpu affinity parameters with pthread_setaffinity_np (err %d)", nError);
		return false;
	}
	return true;
}


bool Thread::strToIntSchedPolicyConversion(const char* sSchedPolicy, int &nSchedPolicy)
{
	nSchedPolicy = 99;
	if ( strcmp(sSchedPolicy, "SCHED_OTHER") == 0 )
	{
		nSchedPolicy = SCHED_OTHER;
	}
	else if ( strcmp(sSchedPolicy, "SCHED_FIFO") == 0 )
	{
		nSchedPolicy = SCHED_FIFO;
	}
	else if ( strcmp(sSchedPolicy, "SCHED_RR") == 0 )
	{
		nSchedPolicy = SCHED_RR;
	}
	else
	{
		return false;
	}
	return true;
}
