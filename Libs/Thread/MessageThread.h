//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    MessageThread.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the MessageThread class.
//! @details
//!
//*!****************************************************************************

#ifndef MESSAGE_THREAD_H_
#define	MESSAGE_THREAD_H_

#include "Thread.h"

class ThreadMsgQueue;
class ThreadSyncEvent;

/*! ***********************************************************************************************
* @class    MessageThread
* @brief    Implements a thread that takes messages sent by the calling thread.
*           The developer that wants to use this mechanism must implement the method
*           <i>ParseMsg</i> to retrieve the message and doing something with it and the
*           method <i>PurgeMsg</i> to free the resources of those messages that cannot be retrieved
*           (for instance when the thread gets closed to empty the queue)
* *************************************************************************************************
*/
class MessageThread : protected Thread
{

	public:

		/*! ***************************************************************************************
		* void constructor
		* *****************************************************************************************
		*/
		MessageThread();

		/*! ***************************************************************************************
		* Default destructor
		* *****************************************************************************************
		*/
		virtual ~MessageThread();

		/*! ***************************************************************************************
		* Initializes the messages management system by starting the thread
        * @param	nMsecTimeout milliseonds to wait before the thread is started
		* @return	true if the thread is correctly starte, false otherwise
		* *****************************************************************************************
		*/
		bool init(int nMsecTimeout = 1000);

		/*! ***************************************************************************************
		* Stops the messages management system by stopping the thread and by releasing resources
		* @param	bImmediate if set to false the thread stops after the last message has been
		*			poped from the queue, if true the remaining messages are immediately
		*			thrown away
		* *****************************************************************************************
		*/
		void destroy(bool bImmediate = false);

		/*! ***************************************************************************************
		* Sends a message to the thread containing the message type and two parameters and
		* specifying also if it must be sent also if the queue is full
		* @param	nMessageType message type (specified by the user)
		* @param	param1 optional additional data of type void* to send together with the message
		* @param	param2 optional additional data of type int to send together with the message
		* @param	bForceSend indicates if the message must be sent also if the queue is full
		* @return	true if the message has succesfully been sent, false otherwise
		* *****************************************************************************************
		*/
		bool sendMsg(unsigned int nMessageType, void* param1 = NULL, int param2 = 0, bool bForceSend = false);

		/*! ***************************************************************************************
		* Increases the number of messages that is possible to send without definitively
		* increasing the queue maximum size. This operation can be executed when it is
        * temporarily necessary to send a big quantity of messages that could not be
		* normally sent due to the queue size limit.
        * After this operation the thread won't refuse the next nSize messages.
		* @param	nSize number of messages that can be sent more than the maximum queue size
		* *****************************************************************************************
		*/
		void increaseNoCheckSizeOffset(unsigned int nSize);

		/*! ***************************************************************************************
		* Retrieves the queue size
		* @return	the queue size
		* *****************************************************************************************
		*/
		int getQueueSize();

		/*! ***************************************************************************************
		* Retrieves the number of messages of the specified type in the queue
		* @param	nMessageType the type of the messages to count (0 for all the messages)
		* @return	number of messages found
		* *****************************************************************************************
		*/
		int countQueueElements(unsigned int nMessageType = 0);

		/*! ***************************************************************************************
		* Set the queue size
		* @param	nSize number of messages that are managed in the queue
		* *****************************************************************************************
		*/
		void setQueueSize(unsigned int nSize);

		/*! ***************************************************************************************
		* Retrieves the watchdog flag value
		* @return	the watchdog flag
		* *****************************************************************************************
		*/
		bool getWatchDogFlag();

		/*! ***************************************************************************************
		* Set the watchdog flag to false
		* *****************************************************************************************
		*/
		void cleanWatchdogFlag();


        /*! ***************************************************************************************
        * Minimum message type identifier value for messages that shall be used by the developer
        * in the implementation of the derived classes
        * *****************************************************************************************
        */
        static const unsigned int MSG_THREAD_USER = 1024;

    protected:

        /*! ***************************************************************************************
        * Message type that signals to stop the thread. It is sent in the stop function
        * *****************************************************************************************
        */
        static const unsigned int MSG_THREAD_STOP = 512;

        /*! ***************************************************************************************
        * Message type that signals to stop the thread and do not execute the parseMessage of the
        * messages that are still in the queue.
        * *****************************************************************************************
        */
        static const unsigned int MSG_THREAD_STOP_NO_PARSE = MSG_THREAD_STOP + 1;

        /*! ***************************************************************************************
        * Message type for a periodically sent message used to avoid that the thread remains
        * stopped for too long in the case that nobody is sending messages for too long,
        * in that case the watchdog will reach timeout
        * *****************************************************************************************
        */
        static const unsigned int MSG_THREAD_WATCHDOG = MSG_THREAD_STOP + 2;

        /*! ***************************************************************************************
		* Function executed by the thread to process a message poped from the queue.
        * This function must release all the resources allocated in the message
		* @param nMessageType message type (specified in the sendMsg)
		* @param param1 optional additional data of type void* (specified in the sendMsg)
		* @param param2 optional additional data of type int (specified in the sendMsg)
		* *****************************************************************************************
		*/
		virtual void parseMsg(unsigned int nMessageType, void* param1, int param2) = 0;

		/*! ***************************************************************************************
		* Function executed by the thread to free the resources allocated in a message once it has
		* been poped from the queue
		* @param nMessageType message type (specified in the sendMsg)
		* @param param1 optional additional data of type void* (specified in the sendMsg)
		* @param param2 optional additional data of type int (specified in the sendMsg)
		* *****************************************************************************************
		*/
		virtual void purgeMsg(unsigned int nMessageType, void* param1, int param2) = 0;

		/*! ***************************************************************************************
		* Function executed by the thread every time it processes a message
		* *****************************************************************************************
		*/
		virtual void setWatchdogFlag() { m_bWatchDogFlag = true; }

		/*! ***************************************************************************************
		* Overloaded function that represents the thread body that executes an infinite loop
		* that waits for a message to come and then processes it
		* @return the thread exit code
		* *****************************************************************************************
		*/
		int workerThread();

		/*! ***************************************************************************************
		 * Function that must be put inside the workerThread to verify that the thread has been
		 * correctly started
		 * *****************************************************************************************
		 */
		void sendStartThreadSync();

    private:

        // The message queue
        ThreadMsgQueue *m_pMsgQueue;
        // Synchronism to notify the thread start time
        ThreadSyncEvent *m_pStartThreadSync;
        // Flag to point out that the thread has sent an initial synchronism signal
        bool m_bStartThreadSyncSent;
        // Flag to point out that the thread has sent a message
        bool m_bMessageSent;
        // Flag that is set to true every thread loop with the setWatchdogFlag()
        bool m_bWatchDogFlag;


};

#endif // MESSAGE_THREAD_H_
