//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Mutex.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the Mutex class.
//! @details
//!
//*!****************************************************************************

#ifndef THREADMUTEX_H
#define THREADMUTEX_H

#include <pthread.h>

/*! ***********************************************************************************************
* @class	ThreadMutex
* @brief	Implements a mutex as a wrapper of the pthread mutex
* *************************************************************************************************
*/
class Mutex
{

	public:

		/*! ***************************************************************************************
		* void constructor
		* *****************************************************************************************
		*/
		Mutex();

		/*! ***************************************************************************************
		* Default destructor
		* *****************************************************************************************
		*/
		virtual ~Mutex();

		/*! ***************************************************************************************
		* Locks the mutex
		* *****************************************************************************************
		*/
		void lock(void);

		/*! ***************************************************************************************
		* Unlocks the mutex
		* *****************************************************************************************
		*/
		void unlock(void);

    private:

        pthread_mutex_t m_mutex;


};

#endif // THREADMUTEX_H
