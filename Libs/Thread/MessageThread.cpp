//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    MessageThread.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the MessageThread class.
//! @details
//!
//*!****************************************************************************

#include <string.h>
#include "ThreadMsg.h"
#include "ThreadMsgQueue.h"
#include "MessageThread.h"


/*! ***********************************************************************************************
 *  MACROS
 * ************************************************************************************************
 */
#define SAFE_DELETE(p) { if (p) {delete p; p = NULL;} }
#define msleep(ms) usleep(ms*1000)

/*! ***********************************************************************************************
 *  Class implementation
 * ************************************************************************************************
 */
MessageThread::MessageThread()
{
	strcpy(m_class_name, "MessageThread");
	m_pMsgQueue = new ThreadMsgQueue();
	m_pStartThreadSync = new ThreadSyncEvent();
	m_bStartThreadSyncSent = false;
	m_bMessageSent = false;
}

MessageThread::~MessageThread()
{
	// ParseMsg is not called because it's been already destroyed in the derived class
	// in which has necessarily been implemented
	destroy(true);
	SAFE_DELETE(m_pMsgQueue);
	SAFE_DELETE(m_pStartThreadSync);
}

bool MessageThread::init(int nMsecTimeout)
{

	// In order to verify if the thread has been started, the thread itself should send a sync
	// using m_pStartThreadSync, but in some old kernel version this mechanism do not work, so
	// the flag m_bStartThreadSyncSent is set to true here and here we must double check
	bool bSyncReceived = false;
	m_bStartThreadSyncSent = false;
	m_bWatchDogFlag = false;
	m_bMessageSent = false;

	if ( !startThread() )
	{
		return false;
	}
	if(nMsecTimeout <= 0)
	{
		nMsecTimeout = 1000;
	}

	m_bWatchDogFlag = false;

	// Waits for the thread to start to manage messages
	if (m_pStartThreadSync)
	{
		int wait_time, remain;
		wait_time = 200;
		remain = nMsecTimeout;
		if ( wait_time > remain )
		{
			wait_time = remain;
		}
		// Loop that waits for the sync to arrive
		while ( ( remain > 0 ) && !m_bStartThreadSyncSent )
		{
			threadLog(THREAD_LOG_DEBUG_PARANOIC, "init: remain = %d, wait_time = %d", remain, wait_time);
			if ( m_pStartThreadSync->wait(wait_time) )
			{
				threadLog(THREAD_LOG_DEBUG_PARANOIC, "init: sync message received");
				bSyncReceived = true;
				break;
			}
			remain -= wait_time;
			if ( remain <= 0 )
			{
				break;
			}
			if ( wait_time > remain )
			{
				wait_time = remain;
			}
		}
		// Verifies if the loop is started
		if ( !m_bStartThreadSyncSent && !bSyncReceived )
		{
			threadLog(THREAD_LOG_DEBUG, "init: stops the thread because the sync message has not been received yet");
			destroy(true);
			return false;
		}
	}

	threadLog(THREAD_LOG_DEBUG_PARANOIC, "Inizializzazione del message manager terminata");
	
	m_bMessageSent = false;

	return true;

}

void MessageThread::destroy(bool bImmediate)
{

	unsigned int msg_type;

	if ( !isRunning() )
	{
		return;
	}

	if (bImmediate)
	{
		msg_type = MSG_THREAD_STOP_NO_PARSE;
	}
	else
	{
		msg_type = MSG_THREAD_STOP;
	}
	// Stop messages must be sent even if the queueu is full
	sendMsg(msg_type, NULL, 0, true);

	// Stops the thread
    stopThread();
	
	m_bMessageSent = false;

}

bool MessageThread::sendMsg(unsigned int nMessageType, void *param1, int param2, bool bForceSend)
{
	if ( !isRunning() )
	{
		return false;
	}
	ThreadMsg *pMsg = new ThreadMsg(nMessageType, param1, param2);
	if (pMsg == NULL)
	{
		threadLog(THREAD_LOG_WARNING, "Impossible to allocate the message");
		return false;
	}
	if ( ! m_pMsgQueue->push(pMsg, bForceSend) )
	{
        SAFE_DELETE(pMsg);
		threadLog(THREAD_LOG_DEBUG, "Impossible to send message of type %d", nMessageType);
		return false;
	}
	m_bMessageSent = true;
	return true;
}

void MessageThread::increaseNoCheckSizeOffset(unsigned int nSize)
{
	m_pMsgQueue->increaseNoCheckSizeOffset(nSize);
}

int MessageThread::countQueueElements(unsigned int nMessageType)
{
	return m_pMsgQueue->countElements(nMessageType);
}

int MessageThread::getQueueSize()
{
	return m_pMsgQueue->getSize();
}

void MessageThread::setQueueSize(unsigned int nSize)
{
	m_pMsgQueue->setSize(nSize);
}

bool MessageThread::getWatchDogFlag()
{
	return m_bWatchDogFlag;
}

void MessageThread::cleanWatchdogFlag()
{
	m_bWatchDogFlag = false;
	// Sends a message to unlock a thread waiting for some message
	// because if no messages are coming for a while then the watchdog
	// flag would not be refreshed
	sendMsg(MessageThread::MSG_THREAD_WATCHDOG);
}


void MessageThread::sendStartThreadSync()
{
	if (m_pStartThreadSync)
	{
		// Signals the calling thread that this thread has been started
		m_pStartThreadSync->set();
		m_bStartThreadSyncSent = true;
	}
	else
	{
		threadLog(THREAD_LOG_WARNING, "workerThread: synchronism not sent");
	}
}

int MessageThread::workerThread()
{
	int rc = -1;
    ThreadMsg* pMsg;
	unsigned int type;
	void* param1;
	int param2;

	m_bStartThreadSyncSent = false;
	
	threadLog(THREAD_LOG_DEBUG_PARANOIC+1, "workerThread: start");

	if (m_pMsgQueue == NULL)
	{
		threadLog(THREAD_LOG_WARNING, "workerThread: Coda dei messaggi non allocata: esce dal thread");
		return -1;
	}

	// Signals the correct thread start
	sendStartThreadSync();
	threadLog(THREAD_LOG_DEBUG_PARANOIC, "workerThread: message processing is started");

	while ( isRunning() )
	{
		// Reset watchdog
		setWatchdogFlag();
		// Waits for a message for at most 200 msec
		if ( !m_pMsgQueue->wait(200) )
		{
			if (!m_bMessageSent)
			{
				continue;
			}
		}
		// Extracts all the messages from the queue
		while ( (pMsg = m_pMsgQueue->pop()) != NULL )
		{
			// if we get here the thread is alive
			setWatchdogFlag();
			// Retrieves the message data
			type = pMsg->getType();
			pMsg->getMsgParams(&param1, &param2);

            SAFE_DELETE(pMsg);

            if ( (type == MessageThread::MSG_THREAD_STOP_NO_PARSE) ||
                 (type == MessageThread::MSG_THREAD_WATCHDOG) )
			{
				// this messages is only intended to do a thread cycle then signal the watchdog
				rc = 0;
				continue;
			}

            threadLog(THREAD_LOG_DEBUG_PARANOIC + 2, "Received message of type %d", type);

			// parses the messages
			parseMsg(type, param1, param2);

            if (type == MessageThread::MSG_THREAD_STOP)
			{
				rc = 0;
				break;
			}
		}
		m_bMessageSent = false;
	}
	
	threadLog(THREAD_LOG_DEBUG_PARANOIC, "Empties the message queue");

	// pops the remeining messages
    pMsg = m_pMsgQueue->pop();
	while ( pMsg != NULL )
	{
		// Retrieves the message data
		type = pMsg->getType();
		pMsg->getMsgParams(&param1, &param2);
		// Releases the message resources
		purgeMsg(type, param1, param2);
        SAFE_DELETE(pMsg);
        // extracts, if present, the last message
		pMsg = m_pMsgQueue->pop();
	}

	// exits
	return rc;

}
