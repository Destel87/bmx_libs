//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ThreadSyncEvent.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the ThreadSyncEvent class.
//! @details
//!
//*!****************************************************************************

#ifndef THREAD_SYNC_H_
#define THREAD_SYNC_H_

#include <pthread.h>

/*! ***********************************************************************************************
* @class	ThreadSyncEvent
* @brief	Implements a mechanism to send events between two threads
* *************************************************************************************************
*/
class ThreadSyncEvent
{

	public:

		/*! ***************************************************************************************
		* void constructor
		* *****************************************************************************************
		*/
		ThreadSyncEvent();

		/*! ***************************************************************************************
		* Default destructor
		* *****************************************************************************************
		*/
		~ThreadSyncEvent();

		/*! ***************************************************************************************
		* Set the event. If another thread was waiting, it will be awaken, otherwise the action
		* has no effect.
		* *****************************************************************************************
		*/
		void set(void);

		/*! ***************************************************************************************
		* Waits for an event for the specified amount of time
        * @param	msec the amount of time in millisecond to wait
		* @return	true if the event has arrive within the timeout, false otherwise
		* *****************************************************************************************
		*/
		bool wait(int msec);

    private:

        pthread_cond_t m_condition;
        pthread_mutex_t m_mutex;
        bool m_bWaiting;

};

#endif  //  THREAD_SYNC_H_
