//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Mutex.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the Mutex class.
//! @details
//!
//*!****************************************************************************

#include "Mutex.h"
#include <stdio.h>

Mutex::Mutex()
{
	pthread_mutex_init(&m_mutex, NULL);
}


Mutex::~Mutex()
{
	pthread_mutex_destroy(&m_mutex);
}


void Mutex::lock(void)
{
	pthread_mutex_lock(&m_mutex);
}


void Mutex::unlock(void)
{
	pthread_mutex_unlock(&m_mutex);
}
