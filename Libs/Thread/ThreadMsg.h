//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ThreadMsg.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the ThreadMsg class.
//! @details
//!
//*!****************************************************************************

#ifndef THREADMSG_H
#define THREADMSG_H

/*! ***********************************************************************************************
* @class	ThreadMsg
* @brief	Implements a message made of a type and two parameters,
*			a <code>void*</code> and a <code>int</code>
* *************************************************************************************************
*/
class ThreadMsg
{

	public:

		/*! ***************************************************************************************
		* Constructor
		* @param nMessageType type of the message
		* @param msg1 optional parameter of type void*
		* @param msg2 optional parameter of type int
		* *****************************************************************************************
		*/
		ThreadMsg(unsigned int nMessageType, void *msg1 = nullptr, int msg2 = 0);

		/*! ***************************************************************************************
		* Destructor
		* *****************************************************************************************
		*/
		virtual ~ThreadMsg();

		/*! ***************************************************************************************
		* Retrieves the parameters of type <code>void*</code> and <code>int</code>
        * @param pFirstParam buffer that contains the parameter of type <code>void*</code>
        * @param pSecondParam buffer that contains the parameter of type <code>int</code>
		* *****************************************************************************************
		*/
		void getMsgParams(void** pFirstParam, int *pSecondParam);

		/*! ***************************************************************************************
		* Retrieves the message type
		* @return the message type
		* *****************************************************************************************
		*/
        unsigned int getType(void);


	protected:

		ThreadMsg* getNext(void) { return m_pNext; }
		void setNext (ThreadMsg* next) { m_pNext = next; }

    private:

        friend class ThreadMsgQueue;

        ThreadMsg* m_pNext;
        unsigned int m_nType;
        void* m_pFirstParam;
        int m_nSecondParam;
};

typedef void (*thread_purge_msg_function_t)(ThreadMsg* pMsg);


#endif // THREADMSG_H
