//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ThreadMsgQueue.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the ThreadMsgQueue class.
//! @details
//!
//*!****************************************************************************

#include "ThreadMsg.h"
#include "ThreadMsgQueue.h"


/*! ***********************************************************************************************
 *  MACROS
 * ************************************************************************************************
 */
#define QUEUE_SIZE_DEFAULT 10


/*! ***********************************************************************************************
 *  Class implementation
 * ************************************************************************************************
 */
ThreadMsgQueue::ThreadMsgQueue(void)
{
	m_pMessageQueue = NULL;
	// Sets the default queue size
	m_nSize = QUEUE_SIZE_DEFAULT;
	m_nNoCheckOffset = 0;
}

ThreadMsgQueue::~ThreadMsgQueue(void)
{
	purge();
	m_nSize = QUEUE_SIZE_DEFAULT;
	m_nNoCheckOffset = 0;
}

void ThreadMsgQueue::purge(thread_purge_msg_function_t *pFunc)
{
	ThreadMsg *p;
	m_msg_queue_mutex.lock();
	while (m_pMessageQueue != NULL)
	{
		p = m_pMessageQueue->getNext();
		if (pFunc)
		{
			// applies the purge function to the message
			(*pFunc)(p);
		}
		delete m_pMessageQueue;
		m_pMessageQueue = p;
	}
	m_msg_queue_mutex.unlock();
}

bool ThreadMsgQueue::push(ThreadMsg* pMsg, bool bForceSend)
{
	unsigned int count = 0;
	bool bRet = false;
	
	m_msg_queue_mutex.lock();
	if (m_pMessageQueue == NULL)
	{
		m_pMessageQueue = pMsg;
		bRet = true;
	}
	else
	{
		ThreadMsg *p = m_pMessageQueue;
		while (p->getNext() != NULL)
		{
	    	p = p->getNext();
			count++;
		}
		//printf("count = %d\n", count);
		unsigned int max = m_nSize + m_nNoCheckOffset;
		if ( (count <= max ) || bForceSend )
		{
            // there is still free places in the queue
			p->setNext(pMsg);
			bRet = true;
			// decrements the number of messages that can be added more than the queue size
			m_nNoCheckOffset = (m_nNoCheckOffset > 0) ? (m_nNoCheckOffset-1) : (0);
		}
	}
	m_msg_queue_mutex.unlock();

	// synch event that signal that a new message has been pushed
	m_sem_sync.set();

	return (bRet);

}

ThreadMsg *ThreadMsgQueue::pop(void)
{
	ThreadMsg *pRet = NULL;

	if (m_pMessageQueue == NULL)
	{
		return (NULL);
	}

	m_msg_queue_mutex.lock();
	if (m_pMessageQueue != NULL)
	{
		pRet = m_pMessageQueue;
		m_pMessageQueue = pRet->getNext();
	}
	m_msg_queue_mutex.unlock();
	if (pRet)
	{
		pRet->setNext(NULL);
	}
	return (pRet);
}

int ThreadMsgQueue::countElements(unsigned int nMessageType)
{
	int ret = 0;
	ThreadMsg *p;
	m_msg_queue_mutex.lock();
	p = m_pMessageQueue;
	while (p != NULL)
	{
		if ( (nMessageType == 0) || (p->getType() == nMessageType) )
		{
			ret++;
		}
		p = p->getNext();
	}
	m_msg_queue_mutex.unlock();
	return (ret);
}

void ThreadMsgQueue::increaseNoCheckSizeOffset(unsigned int nSize)
{
	m_msg_queue_mutex.lock();
	m_nNoCheckOffset += nSize;
	m_msg_queue_mutex.unlock();
}
