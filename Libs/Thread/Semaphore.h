//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Semaphore.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the Semaphore class.
//! @details
//!
//*!****************************************************************************

#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <semaphore.h>
#include <string>
#include <sys/stat.h>
#include <fcntl.h>

#define SEM_FAILURE	-1
#define SEM_SUCCESS	0
#define SEM_TIMEOUT	1

using namespace std;

/*! *******************************************************************************************************************
* @class    Semaphore
* @brief    Implements a named counting semaphore as an interface to the POSIX.1-2001,
*			POSIX.1-2008. semaphore.h library.
			Examples of wrapped methods are:
				- sem_open(3);
				- sem_close(3);
				- sem_getvalue(3);
				- sem_post(3);
				- sem_unlink(3);
				- sem_wait(3);
				- sem_timedwait(3);
				- sem_overview(7)
* *********************************************************************************************************************
*/
class Semaphore
{

	public:

		/*! ***********************************************************************************************************
		* void constructor
		* *************************************************************************************************************
		*/
		Semaphore();

		/*! ***********************************************************************************************************
		* default destructor
		* *************************************************************************************************************
		*/
		virtual ~Semaphore();

		/*! ***********************************************************************************************************
		 * @brief open				creates a new named POSIX semaphore
		 * @param sName				semaphore name of the form /somename; that is, a null-terminated string
		 *							of up to NAME_MAX-4 (i.e., 251) characters consisting of an initial slash,
		 *							followed by one or more characters, none of which are slashes.
		 * @param bDestroyIfExist	if at creation it is found that a semaphore already exists with name sName
		 *							and this parameter is set to true, then the semaphore gets destroyed and then
		 *							created again, otherwise, this object gets linked to the already existing semaphore
         * @param uiPermissions		this parameter specifies the owner, group and others permissions on the semaphore
		 *							as reported in <fcntl.h>
		 * @param uiInitialValue	the initial value of the semaphore
		 * @return					true upon succesfull operation, false otherwise
		 * ************************************************************************************************************
		 */
		bool create(string sName, uint32_t uiInitialValue = 0, bool bDestroyIfExist = true, mode_t uiPermissions = S_IRWXU | S_IRWXG | S_IROTH);

		/*! ***********************************************************************************************************
		 * @brief open				open an existing named POSIX semaphore
		 * @param sName				semaphore name of the form /somename; that is, a null-terminated string
		 *							of up to NAME_MAX-4 (i.e., 251) characters consisting of an initial slash,
		 *							followed by one or more characters, none of which are slashes.
		 * @param iOflag			O_RDWR, O_RDONLY or O_WRONLY can be specified
         * @param uiPermissions			this parameter specifies the owner, group and others permissions on the semaphore
		 *							as reported in <fcntl.h>
		 * @return					true upon succesfull operation, false otherwise
		 * ************************************************************************************************************
		 */
		bool open(string sName, int iOflag = O_RDWR, mode_t uiPermissions = S_IRWXU | S_IRWXG | S_IROTH);

		/*! ***********************************************************************************************************
		 * @brief getValue			retrieves the current value of the semaphore
		 * @return					the current semaphore value, INT32_MIN in case of errors
		 * ************************************************************************************************************
		 */
		int getValue(void);

		/*! ***********************************************************************************************************
		 * @brief wait				Locks (decrements) the semaphore. A timeout can be specified as a limit on the
		 *							amount of time that the call should block if the decrement cannot be immediately
		 *							performed. If the semaphore's value is greater than zero, then the decrement
		 *							proceeds, and the function returns, immediately. If the semaphore currently has
		 *							the value zero, then the call blocks until either it becomes possible to perform
		 *							the decrement (i.e., the semaphore value rises above zero), or the timeout expires,
		 *							or a signal handler interrupts the call.
         * @param ullTimeoutMSec		the timeout in milliseconds, if 0 is specified the timeout is considered infinite
		 * @param bAbsoluteWait		if set to true then ullTimeoutMsec is considered as the system absolute time at
		 *							which the wait will expire (must be greater than the actual system time), otherwise
		 *							ullTimeoutMsec is considered a relative time
		 * @return					0 upon decrement succeded, 1 upon timeout with no decrement, -1 upon error
		 * ************************************************************************************************************
		 */
		int wait(uint64_t ullTimeoutMSec = 0, bool bAbsoluteWait = false);

		/*! ***********************************************************************************************************
		 * @brief release			Unlocks (increments) the semaphore. If the semaphore's value consequently becomes
		 *							greater than zero, then another process or thread blocked in a wait() call will
		 *							be woken up and proceed to lock the semaphore.
		 * @return					0 on success; -1 on error and the value of the semaphore is left unchanged
		 * ************************************************************************************************************
		 */
		int release(void);

		/*! ***********************************************************************************************************
		 * @brief					enableErrorPrint enables or disables error printing on stderr if errors are
		 *							reported from some class method
		 * @param					bEnable if true enables error printing, false otherwise
		 * ************************************************************************************************************
		 */
		void enableErrorPrint(bool bEnable);

		/*! ***********************************************************************************************************
		 * @brief					close closes the semaphore
		 * @return					true if operation succeded, false otherwise and, if enabled the error printing
		 *							feature, prints error on stderr
		 * ************************************************************************************************************
		 */
		bool close(void);

		/*! ***********************************************************************************************************
		 * @brief	unlink unlinks the semaphore
		 * @return	true if operation succeded, false otherwise and, if enabled the error printing
		 *			feature, prints error on stderr
		 * ************************************************************************************************************
		 */
		bool unlink(void);

		/*! ***********************************************************************************************************
		 * @brief	closeAndUnlink closes and unlinks the semaphore
		 * @return	true if operation succeded, false otherwise and, if enabled the error printing
		 *			feature, prints error on stderr
		 * ************************************************************************************************************
		 */
		bool closeAndUnlink(void);

    private:

        bool createInternal(uint32_t uiInitialValue, mode_t uiPermissions);


    private:

        sem_t* m_pSem;
        string m_sName;
        int m_iValue;
        bool m_bPrintErrors;

};

#endif // SEMAPHORE_H
