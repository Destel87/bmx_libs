//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ThreadMsgQueue.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the ThreadMsgQueue class.
//! @details
//!
//*!****************************************************************************

#ifndef THREAD_MSG_QUEUE_
#define THREAD_MSG_QUEUE_

#include "Mutex.h"
#include "ThreadSyncEvent.h"


class ThreadMsg;

typedef void (*thread_purge_msg_function_t)(ThreadMsg* pMsg);


/*! ***********************************************************************************************
* @class	ThreadMsgQueue
* @brief	Implements a synchronized queue between two threads in which the first one does push
*			actions and the other one waits and is awaken when a new message has been pushed
* *************************************************************************************************
*/
class ThreadMsgQueue
{

	public:

		/*! ***************************************************************************************
		* void constructor
		* *****************************************************************************************
		*/
		ThreadMsgQueue(void);

		/*! ***************************************************************************************
		* Default destructor
		* *****************************************************************************************
		*/
		virtual ~ThreadMsgQueue(void);

		/*! ***************************************************************************************
		* Pushes a message in the queue
        * @param	pMsg message to push
		* @param	bForceSend indicates if the message must be sent also if the queue is full
		* @return	true if the message has succesfully been pushed, false otherwise
		* *****************************************************************************************
		*/
		bool push(ThreadMsg* pMsg, bool bForceSend = false);

		/*! ***************************************************************************************
		* Waits until a message has been pushed in the queue
		* @param	msec max number of milliseconds to wait
		* @return	true if a message is arrived before the timeout, false if timeout is reached
		* *****************************************************************************************
		*/
		bool wait(int msec = 100000)
		{
			if ( getSize() > 0 )
			{
				return true;
			}
			return (m_sem_sync.wait(msec));
		}

		/*! ***************************************************************************************
		* Removes the first message in the queue
		* @return	the removed message
		* *****************************************************************************************
		*/
		ThreadMsg* pop(void);

		/*! ***************************************************************************************
		* Set the queue size
		* @param	nSize number of messages that are managed in the queue
		* *****************************************************************************************
		*/
		void setSize(unsigned int nSize) { m_nSize = nSize; }

		/*! ***************************************************************************************
		* Retrieves the queue size
		* @return	the queue size
		* *****************************************************************************************
		*/
		int getSize() { return m_nSize; }

		/*! ***************************************************************************************
		* Retrieves the number of messages of the specified type in the queue
		* @param	nMessageType the type of the messages to count (0 for all the messages)
		* @return	number of messages found
		* *****************************************************************************************
		*/
		int countElements(unsigned int nMessageType = 0);

		/*! ***************************************************************************************
		* Increases the number of messages that is possible to send without definitively
		* increasing the queue maximum size. This operation can be executed when it is
        * temporarily necessary to send a big quantity of messages that could not be
		* normally sent due to the queue size limit.
        * After this operation the thread won't refuse the next nSize messages.
		* @param	nSize number of messages that can be sent more than the maximum queue size
		* *****************************************************************************************
		*/
		void increaseNoCheckSizeOffset(unsigned int nSize);

		/*! ***************************************************************************************
		* Empties the queue
		* @param	pFunc is an optional function that can be applied at every message to free the
		*			resources allocated by the user
		* *****************************************************************************************
		*/
		void purge(thread_purge_msg_function_t *pFunc = NULL);

    private:

        // pointer to the message queue
        ThreadMsg *m_pMessageQueue;
        // mutex to access the queue
        Mutex m_msg_queue_mutex;
        // the queue size
        unsigned int m_nSize;
        // number of messages that can be added on the queue size without
        // the control on the queue size
        unsigned int m_nNoCheckOffset;
        // synchronization semaphore
        ThreadSyncEvent m_sem_sync;


};

#endif // THREAD_MSG_QUEUE_
