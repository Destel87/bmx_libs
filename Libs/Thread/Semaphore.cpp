//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Semaphore.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the Semaphore class.
//! @details
//!
//*!****************************************************************************

#include "Semaphore.h"
#include <stdio.h>
#include "TimeStamp.h"

Semaphore::Semaphore()
{
	m_pSem = SEM_FAILED;
	m_iValue = INT32_MIN;
	m_sName.clear();
	m_bPrintErrors = false;
}

Semaphore::~Semaphore()
{
	close();
	//unlink();
}

bool Semaphore::createInternal(uint32_t uiInitialValue, mode_t uiPermissions)
{
	m_pSem = sem_open(m_sName.c_str(), O_CREAT, uiPermissions, uiInitialValue);
	if ( m_pSem == SEM_FAILED )
	{
		if ( m_bPrintErrors ) perror("Semaphore creation error");
		return false;
	}
	else
	{
		sem_getvalue(m_pSem, &m_iValue);
		if ( m_iValue != (int)uiInitialValue )
		{
			closeAndUnlink();
			if ( m_bPrintErrors ) perror("Semaphore creation error, bad initial value");
			return false;
		}
	}
	return true;
}

bool Semaphore::create(string sName, uint32_t uiInitialValue, bool bDestroyIfExist, mode_t uiPermissions)
{
	if ( ( sName.length() <= 0 ) || ( sName.at(0) != '/' ) ) // Name not compatible, it would fail later...
		return false;

	if ( m_pSem != 0 ) // Already opened...
		return false;

	m_sName.assign(sName);
	bool bRetValue = false;
	m_pSem = sem_open(m_sName.c_str(), O_RDWR);
	if ( m_pSem == SEM_FAILED )
	{
		bRetValue = createInternal(uiInitialValue, uiPermissions);
	}
	else
	{
		if ( bDestroyIfExist )
		{
			closeAndUnlink();
			bRetValue = createInternal(uiInitialValue, uiPermissions);
		}
		else
		{
			// Semaphore was already created earlier, no destroy has been requested so
			// retrieve its value and hold the descriptor
			sem_getvalue(m_pSem, &m_iValue);
			bRetValue = true;
		}
	}
	return bRetValue;
}


bool Semaphore::open(string sName, int iOflag, mode_t uiPermissions)
{
	if ( ( sName.length() <= 0 ) || ( sName.at(0) != '/' ) ) // Name not compatible, it would fail later...
		return false;

	if ( m_pSem != SEM_FAILED ) // Already opened...
		return false;

	if ( ( iOflag == O_CREAT ) || ( iOflag == O_EXCL ) ) // Only for creation
		return false;

	m_sName.assign(sName);
	m_iValue = 0;
	m_pSem = sem_open(m_sName.c_str(), iOflag, uiPermissions, 0);
	if ( m_pSem == SEM_FAILED )
	{
		if ( m_bPrintErrors ) perror("Semaphore open error");
		return false;
	}
	// Retrieve semaphore value and hold the descriptor
	sem_getvalue(m_pSem, &m_iValue);
	return true;

}

int Semaphore::getValue(void)
{
	if ( m_pSem == SEM_FAILED )
	{
		if ( m_bPrintErrors ) perror("Semaphore not opened, impossible to getValue");
		return INT32_MIN;
	}
	if ( sem_getvalue(m_pSem, &m_iValue) < 0 )
	{
		if ( m_bPrintErrors ) perror("Impossible to getValue");
		return INT32_MIN;
	}
	return m_iValue;
}

int Semaphore::wait(uint64_t ullTimeoutMSec, bool bAbsoluteWait)
{
	if ( m_pSem == SEM_FAILED )
	{
		if ( m_bPrintErrors ) perror("Semaphore not opened, impossible to wait");
		return SEM_FAILURE;
	}

	int nRetValue = -1;
	if ( ullTimeoutMSec == 0 )
	{
		if ( sem_wait(m_pSem) < 0 )
		{
			if ( m_bPrintErrors ) perror("Error from sem_wait");
			nRetValue = SEM_FAILURE;
		}
		else
		{
			nRetValue = SEM_SUCCESS;
		}
	}
	else
	{

		TimeStamp waitTs;
		if ( bAbsoluteWait )
		{
			waitTs.set(ullTimeoutMSec * NSEC_PER_MSEC);
		}
		else
		{
			TimeStamp difference( (unsigned long long int)(ullTimeoutMSec * NSEC_PER_MSEC) );
			waitTs += difference;
		}
		struct timespec ts;
		waitTs.tsToTimespec(&ts);
		int nSemWaitRetValue = sem_timedwait(m_pSem, &ts);
		if ( nSemWaitRetValue == -1 )
		{
			if ( errno == ETIMEDOUT )
			{
				if ( m_bPrintErrors ) perror("sem_timedwait() timed out");
				nRetValue = SEM_TIMEOUT;
			}
			else
			{
				if ( m_bPrintErrors ) perror("Error from sem_wait()");
				nRetValue = SEM_FAILURE;
			}
		}
		else
		{
			nRetValue = SEM_SUCCESS;
		}
	}
	return nRetValue;
}

int Semaphore::release(void)
{
	if ( m_pSem == SEM_FAILED )
	{
		if ( m_bPrintErrors ) perror("Semaphore not opened, impossible to release");
		return SEM_FAILURE;
	}
	if ( sem_post(m_pSem) == -1 )
	{
		if ( m_bPrintErrors ) perror("Error from sem_post()");
		return SEM_FAILURE;
	}
	sem_getvalue(m_pSem, &m_iValue);
	return SEM_SUCCESS;
}

void Semaphore::enableErrorPrint(bool bEnable)
{
	m_bPrintErrors = bEnable;
}

bool Semaphore::close(void)
{
	if ( !m_pSem )
		return true;
	int nRet = sem_close(m_pSem);
	if ( nRet < 0 )
	{
		if ( m_bPrintErrors )
			perror("Semaphore close error");
		return false;
	}
	return true;
}

bool Semaphore::unlink(void)
{
	if ( m_sName.length() <= 0 )
		return true;
	int nRet = sem_unlink(m_sName.c_str());
	if ( nRet < 0 )
	{
		if ( m_bPrintErrors )
			perror("Semaphore unlink error");
		return false;
	}
	return true;
}

bool Semaphore::closeAndUnlink(void)
{
	bool bRetValue = close();
	if ( bRetValue )
		bRetValue = unlink();
	return bRetValue;
}
