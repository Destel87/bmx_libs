QT		-= core gui
TARGET		= Thread
TEMPLATE	= lib
CONFIG		+= staticlib

QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3
QMAKE_CXXFLAGS += -O0

INCLUDEPATH += $$PWD/../TimeStamp

HEADERS += \
	$$PWD/Thread.h \
	$$PWD/MessageThread.h \
	$$PWD/ThreadMsgQueue.h \
	$$PWD/ThreadSyncEvent.h \
	$$PWD/Mutex.h \
	$$PWD/ThreadMsg.h \
        $$PWD/Semaphore.h

SOURCES += \
	$$PWD/Thread.cpp \
	$$PWD/MessageThread.cpp \
	$$PWD/ThreadMsgQueue.cpp \
	$$PWD/ThreadSyncEvent.cpp \
	$$PWD/Mutex.cpp \
	$$PWD/ThreadMsg.cpp \
        $$PWD/Semaphore.cpp

