//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ThreadSyncEvent.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the ThreadSyncEvent class.
//! @details
//!
//*!****************************************************************************

#include <errno.h>
#include <sys/time.h>
#include <stdio.h>

#include "ThreadSyncEvent.h"


/*! ***********************************************************************************************
 *  Local functions
 * ************************************************************************************************
 */
void get_current_time(unsigned int *sec, unsigned int *usec)
{
	struct timeval temp;
	gettimeofday(&temp, NULL);
	*sec = temp.tv_sec;
	*usec = temp.tv_usec;
}


/*! ***********************************************************************************************
 *  Class implementation
 * ************************************************************************************************
 */
ThreadSyncEvent::ThreadSyncEvent()
{
	pthread_cond_init(&m_condition, NULL);
	pthread_mutex_init(&m_mutex, NULL);
	m_bWaiting = false;
}


ThreadSyncEvent::~ThreadSyncEvent()
{
	pthread_cond_destroy(&m_condition);
	pthread_mutex_destroy(&m_mutex);
}

void ThreadSyncEvent::set(void)
{
	if ( !m_bWaiting )
	{
		return;
	}
	pthread_cond_signal(&m_condition);
}

bool ThreadSyncEvent::wait(int msec)
{
	struct timespec end_wait;
	unsigned int end_sec, end_usec, current_sec, current_usec;
	pthread_mutex_lock(&m_mutex);
	m_bWaiting = true;
	// Retrieves the actual time
	get_current_time(&current_sec, &current_usec);
	// Performs the calculation of the final time in which the wait shall return
	end_sec = current_sec + (msec / 1000);
	end_usec = current_usec + (msec % 1000)*1000;
	if (end_usec > 1000000)
	{
		end_sec += (end_usec / 1000000);
		end_usec = (end_usec % 1000000);
	}
	end_wait.tv_sec = end_sec;
	end_wait.tv_nsec = end_usec * 1000;
	int rc = pthread_cond_timedwait(&m_condition, &m_mutex, &end_wait);
	m_bWaiting = false;
	pthread_mutex_unlock(&m_mutex);

	if (rc == ETIMEDOUT)
	{
		printf("ETIMEDOUT\n");fflush(stdout);
	}
	else
	{
		printf("NO TIMEDOUT remain = %ld\n", (end_wait.tv_nsec) / 1000);fflush(stdout);
	}

	return (rc == 0);

	/*
	unsigned int current_sec, current_usec;
	struct timespec end_wait;
	//unsigned int end_sec, end_usec;

	(void) pthread_mutex_lock(&m_mutex); ////
	m_bWaiting = true;

	// recupera il tempo corrente
	//get_current_time(&current_sec, &current_usec);
	clock_gettime(CLOCK_REALTIME, &end_wait);
	// si calcola il tempo di fine della wait
	end_wait.tv_nsec += (msec % 1000) * 1000000;
	end_wait.tv_sec += msec / 1000 + end_wait.tv_nsec / 1000000000;
	int rc = pthread_cond_timedwait(&m_condition, &m_mutex, &end_wait);
	m_bWaiting = false;
	(void) pthread_mutex_unlock(&m_mutex); ////
	*/
}
