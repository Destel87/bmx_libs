//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Thread.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the Thread class.
//! @details
//!
//*!****************************************************************************

#ifndef THREAD_H_
#define	THREAD_H_

#include <pthread.h>

/*! *******************************************************************************************************************
* @typedef thread_log_func_t (int loglevel, void* pLogHandler, const char *message)
* function to be used in order to have a personalized log
* *********************************************************************************************************************
*/
typedef void (*thread_log_func_t)(int loglevel, void* pLogHandler, const char *message);

/*! *******************************************************************************************************************
* @class    Thread
* @brief    Implements a thread that executes a function defined by the developer
*           If a multithreaded architecture is desired, the developer must implement a class
*           derived from this one and must define the method WorkerThread, that tipically is
*           an infinite cycle. Is it possible to implement a method beforeWorkerThread that
*           is executed just before the WorkerThread and another one afterWorkerThread
*           that will be executed just after.
* *********************************************************************************************************************
*/
class Thread
{

	public:

		/*! ***************************************************************************************
		* void constructor
		* *****************************************************************************************
		*/
		Thread();

		/*! ***************************************************************************************
		* Default destructor
		* *****************************************************************************************
		*/
		virtual ~Thread();

		/*! ***************************************************************************************
		* Starts this thread and gives back control to the calling thread
		* @param	detach to be set as true if a detached (i.e. not joinable) thread is desired
		* @return	true in case of success, false otherwise
		* *****************************************************************************************
		*/
		virtual bool startThread(bool detach = false);

		/*! ***************************************************************************************
		* Stops the thread. This function sets to false a flag that should be periodically checked
		* inside the workerThread using the isRunning method, then waits for the thread to exit.
		* The duration of this method depends also upon how long the workThread takes to finish
		* once the flag has been set to false
        * @param	msecTimeout maximum amount in milliseconds to wait
		* @sa		isRunning, workerThread
		* *****************************************************************************************
		*/
		virtual void stopThread(int msecTimeout = 3000);

		/*! ***************************************************************************************
		* Checks whether the thread is in execution
		* @return	true if the thread is in execution, false otherwise or is waiting to be stopped
		* @sa		isStopped
		* *****************************************************************************************
		*/
		bool isRunning() { return m_bThreadRunning; }

		/*! ***************************************************************************************
		* Checks whether the thread is stopped
		* @return true if the thread is stopped, true otherwise
		* @sa		isRunning
		* *****************************************************************************************
		*/
		bool isStopped() { return (!m_bThreadRunning && m_bThreadDone); }

		/*! ***************************************************************************************
		* Returns the pid of the thread
		* @return	the pid of the thread if it is active, 0 otherwise
		* *****************************************************************************************
		*/
		pid_t getPid() const { return m_thread_pid; }

		/*! ***************************************************************************************
		* Memorizes in this class a personalized log function.
		* Once this feature is active it is possible to set in this class the function to be used
		* to save log messages and an optional log handler, i.e. a pointer to a structure or a
		* class to be used in this personalized function.
		* Everytime that this class must log something, the personalized function is called whose
		* arguments are: the log level, the handler and the string to be logged.
		* The personalized log function must be defined as following
		* <br> <code> typedef void (*thread_log_func_t)(int loglevel, void* pLogHandler,
		* const char *message); </code> <br>
		* It must be noticed that the class passes to this personalized function only
		* the messages that have a log level equal or major than the log level set with the method
		* setLogLevel.
		* @param	func personalized log function
		* @param	handler the log function handler
		* *****************************************************************************************
		*/
		void setThreadLogFunction(thread_log_func_t func, void* handler);

		/*! ***************************************************************************************
		* Returns a string containing the version of this class
		* @return	a pointer to a statically allocated string
		* *****************************************************************************************
		*/
		static const char *getVersion();

		/*! ***************************************************************************************
		* Returns the scheduling policy of the thread
		* @param[out]	nSchedPolicy the scheduling policy of the thread
		* @param[out]	nPriority the priority of the thread
		* @return		true if the parameters are correctly retrieved, false otherwise
		* *****************************************************************************************
		*/
		bool getSchedPolicy(int& nSchedPolicy, int& nPriority);

		/*! ***************************************************************************************
		* Sets the scheduling policy of the thread, it must be called before the startThread
		* method
		* @param	nSchedPolicy can be SCHED_OTHER, SCHED_FIFO or SCHED_RR
		* @param	nPriority can range in 1 (lowest priority) - 99 (highest priority)
		*			if SCHED_FIFO or SCHED_RR is specified, otherwise is forcedly set to 0
		* @return	true upon success, false otherwise
		* *****************************************************************************************
		*/
		bool setSchedPolicy(int nSchedPolicy, int nPriority = 0);

		/*! ***************************************************************************************
		* Retrieves the scheduling policy type and priority of the thread
		* @param[out]	sSchedPolicyInfoString (allocated by the caller) containing the info
		* *****************************************************************************************
		*/
		void getSchedPolicyInfoString(char* sSchedPolicyInfoString);

		/*! ***************************************************************************************
		* Sets the CPU affinity of the thread to the CPU specified in the argument.
		* If the call is successful, and the thread is not currently running on the
		* specified CPU, then it is migrated to one of those CPUs.
		* @param	nCpu the CPU to whom the thread should be assigned
		* @return	true upon success, false otherwise
		* *****************************************************************************************
		*/
		bool setCpuAffinity(int nCpu);

		static bool strToIntSchedPolicyConversion(const char* sSchedPolicy, int &nSchedPolicy);

	protected:

		/*! ***************************************************************************************
		* Waits for the end of the thread
		* @param	msec max number of milliseconds to wait (-1 is infinite)
		* @return	true if the thread has ended before the required time
		* *****************************************************************************************
		*/
		bool joinThread(int msec = 3000);

		/*! ***************************************************************************************
		* Main thread cycle.
		* This function has to be necessarily overloaded by defining the thread
		* main cycle. This method should also periodically check (with the isRunning method) if the
		* calling thread has requested a stop.
		* @return	the thread exit code
		* @sa		isRunning
		* *****************************************************************************************
		*/
		virtual int workerThread() = 0;

		/*! ***************************************************************************************
		* Initial operations of the thread.
		* This function gets automatically called before the main thread cycle is executed.
		* It is not necessary to overload it.
		* *****************************************************************************************
		*/
		virtual void beforeWorkerThread() {}

		/*! ***************************************************************************************
		* Exit operations of the thread.
		* This function gets automatically called after the main thread cycle is executed.
		* It is not necessary to overload it.
		* *****************************************************************************************
		*/
		virtual void afterWorkerThread() {}

		/*! ***************************************************************************************
		* Writes a log message
		* @param	level the log level of the message
		* @param	fmt the log message
		* @sa		setThreadLogFunction
		* *****************************************************************************************
		*/
		void threadLog(int level, const char fmt[], ...);

	private:

		static void *staticThreadCallback(void *param);
		int threadMain();

    protected:

        // the class name and version info
        char m_class_name[256];
        static const char* THREAD_VERSION;

        // log levels
        static const int THREAD_LOG_EMERG = 0;
        static const int THREAD_LOG_ALERT = 1;
        static const int THREAD_LOG_CRIT = 2;
        static const int THREAD_LOG_ERR = 3;
        static const int THREAD_LOG_WARNING = 4;
        static const int THREAD_LOG_NOTICE	= 5;
        static const int THREAD_LOG_INFO = 6;
        static const int THREAD_LOG_DEBUG = 7;
        static const int THREAD_LOG_DEBUG_PARANOIC	= 8;

    private:

        pthread_t m_pthread;
        pid_t m_thread_pid;
        pthread_mutex_t m_mutexThreadEnd;
        pthread_cond_t m_condThreadEnd;

        pthread_attr_t m_pthread_attr;		// contains the attributes of the thread (must be initialized before creation)
        struct sched_param m_sched_param;	// contains the last scheduling parameters set (default or explicitly)
        int m_nSchedPolicy;

        bool m_bThreadRunning;
        bool m_bThreadDone;

        // logging
        thread_log_func_t m_log_func;
        void* m_log_func_handler;

};

#endif // THREAD_H_
