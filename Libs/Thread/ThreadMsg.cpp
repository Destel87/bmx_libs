//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    ThreadMsg.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the ThreadMsg class.
//! @details
//!
//*!****************************************************************************

#include "ThreadMsg.h"

ThreadMsg::ThreadMsg(unsigned int nMessageType, void *msg1, int msg2)
{
	m_nType = nMessageType;
	m_pNext = nullptr;
	m_pFirstParam = msg1;
	m_nSecondParam = msg2;
}

ThreadMsg::~ThreadMsg()
{
	m_nType = 0;
	m_pNext = nullptr;
	m_pFirstParam = nullptr;
	m_nSecondParam = 0;
}

void ThreadMsg::getMsgParams(void** pFirstParam, int *pSecondParam)
{
	*pFirstParam = m_pFirstParam;
	*pSecondParam = m_nSecondParam;
}


unsigned int ThreadMsg::getType(void)
{
	return m_nType;
}
