#include <fcntl.h>           // For O_* constants
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>        // For mode constants
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/un.h>
#include "NetworkIPC.h"

#define SOCKET_CTRL_DEFAULT  "sockinstctrl"

#define SOCKET_PROGRESS_DEFAULT  "swupdateprog"

NetworkIPC::NetworkIPC()
{
    socketCtrlPath = NULL;
    socketProgressPath = NULL;
}

NetworkIPC::~NetworkIPC()
{

}

char* NetworkIPC::getCtrlSocket()
{

    if (!socketCtrlPath || !strlen(socketCtrlPath))
    {
        const char *tmpdir = getenv("TMPDIR");
        if (!tmpdir)
            tmpdir = "/tmp";

        asprintf(&socketCtrlPath, "%s/%s", tmpdir, SOCKET_CTRL_DEFAULT);

     }

    return socketCtrlPath;
}

char* NetworkIPC::getProgrSocket()
{

    if (!socketProgressPath || !strlen(socketProgressPath))
    {
        const char *tmpdir = getenv("TMPDIR");
        if (!tmpdir)
            tmpdir = "/tmp";

        asprintf(&socketCtrlPath, "%s/%s", tmpdir, SOCKET_PROGRESS_DEFAULT);

     }

    return socketCtrlPath;
}

int NetworkIPC::prepareIPC(void)
{

    int connfd;
    int ret;

    struct sockaddr_un servaddr;

    connfd = socket(AF_LOCAL, SOCK_STREAM, 0);
    if (connfd < 0) {
        return connfd;
    }
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sun_family = AF_LOCAL;

    strncpy(servaddr.sun_path, getCtrlSocket(), sizeof(servaddr.sun_path) - 1);

    ret = connect(connfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
    if (ret < 0) {
        close(connfd);
        return ret;
    }

    return connfd;
}

int NetworkIPC::getstatusIPCtimeout(int connfd, ipc_message *msg, unsigned int timeout_ms)
{
    ssize_t ret;
    fd_set fds;
    struct timeval tv;

    memset(msg, 0, sizeof(*msg));
    msg->magic = IPC_MAGIC;
    msg->type = GET_STATUS;
    ret = write(connfd, msg, sizeof(*msg));
    if (ret != sizeof(*msg))
        return -1;

    if (!timeout_ms) {
        ret = read(connfd, msg, sizeof(*msg));
    } else {
        FD_ZERO(&fds);
        FD_SET(connfd, &fds);

        //
        // Invalid the message
        // Caller should check it
        //
        msg->magic = 0;

        tv.tv_sec = 0;
        tv.tv_usec = timeout_ms * 1000;
        ret = select(connfd + 1, &fds, NULL, NULL, &tv);
        if (ret <= 0 || !FD_ISSET(connfd, &fds))
            return 0;
        ret = read(connfd, msg, sizeof(*msg));
    }
    return ret;
}

int NetworkIPC::connectProgressIPC(const char *socketpath, bool reconnect)
{
    struct sockaddr_un servaddr;
    int fd = socket(AF_LOCAL, SOCK_STREAM, 0);
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sun_family = AF_LOCAL;
    strncpy(servaddr.sun_path, socketpath, sizeof(servaddr.sun_path) - 1);

    /*
     * Check to get a valid socket
     */
    if (fd < 0)
        return -1;

    do {
        if (connect(fd, (struct sockaddr *) &servaddr, sizeof(servaddr)) == 0) {
            break;
        }
        if (!reconnect) {
            fprintf(stderr, "cannot communicate with SWUpdate via %s\n", socketpath);
            close(fd);
            return -1;
        }

        usleep(10000);
    } while (true);

    fprintf(stdout, "Connected to SWUpdate via %s\n", socketpath);
    return fd;
}

int NetworkIPC::getstatusIPC(ipc_message *msg)
{
    int ret;
    int connfd;

    connfd = prepareIPC();
    if (connfd < 0) {
        return -1;
    }
    ret = getstatusIPCtimeout(connfd, msg, 0);
    close(connfd);

    if (ret > 0)
        return 0;
    return -1;
}

int NetworkIPC::waitForComplete(void)
{
    RECOVERY_STATUS status = IDLE;
    ipc_message message;

    do
    {
        getstatusIPC(&message);

        if (status != (RECOVERY_STATUS)message.data.status.current)
        {
            getstatusIPC(&message);
        }
        else
        {
            sleep(1);
        }


        status = (RECOVERY_STATUS)message.data.status.current;
    }while (message.data.status.current != IDLE);

    return message.data.status.last_result;
}

int NetworkIPC::connectProgressIPCwithPath(const char *socketpath, bool reconnect)
{
    return connectProgressIPC(socketpath, reconnect);
}

int NetworkIPC::connectProgressIPCWrap(bool reconnect)
{
    return connectProgressIPC(getProgrSocket(), reconnect);
}

int NetworkIPC::receiveProgressIPC(int *connfd, struct progress_msg *msg)
{
    int ret = read(*connfd, msg, sizeof(*msg));
    if (ret != sizeof(*msg)) {
        fprintf(stdout, "Connection closing..\n");
        close(*connfd);
        *connfd = -1;
        return -1;
    }
    return ret;
}
