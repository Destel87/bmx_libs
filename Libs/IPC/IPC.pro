QT          -= core gui
TARGET      = IPC
TEMPLATE    = lib
CONFIG      += staticlib

LIBS += -pthread -lrt

# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../TimeStamp
DEPENDPATH += $$PWD/../TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../TimeStamp/libTimeStamp.a

HEADERS += \
    NetworkIPC.h \
    SharedMemory.h \
    MessageQueue.h


SOURCES += \
    NetworkIPC.cpp \
    SharedMemory.cpp \
    MessageQueue.cpp

