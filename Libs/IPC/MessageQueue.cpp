//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    MessageQueue.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the MessageQueue class.
//! @details
//!
//*!****************************************************************************

#include <fcntl.h>           // For O_* constants
#include <sys/stat.h>        // For mode constants
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "TimeStamp.h"
#include "MessageQueue.h"

MessageQueue::MessageQueue()
{
	m_bConfigOk = false;
	m_nDescriptor = -1;
	m_sName[0] = '\0';
	m_nMaxNumMessages = 0;
	m_nMessageSizeBytes = 0;
	m_nMessageBufferSize = 0;
	m_attrMsgQueue.mq_curmsgs = 0;
	m_attrMsgQueue.mq_flags = 0;
	m_attrMsgQueue.mq_maxmsg = 0;
	m_attrMsgQueue.mq_msgsize = 0;
	m_bEnableDebugPrintf = false;
}

MessageQueue::~MessageQueue()
{

}

bool MessageQueue::create(const char* sName, const int nFlags, const int nMaxNumMessages, const int nMessagesSizeBytes)
{

	size_t nStrLen = strlen(sName);
	if ( ( nStrLen < 2 ) || (sName[0] != '/') )
	{
		m_bConfigOk = false;
		return false;
	}

	if ( ( nFlags != O_CREAT ) && ( nFlags != O_RDONLY ) && ( nFlags != O_WRONLY ) && ( nFlags != O_RDWR) )
	{
		m_bConfigOk = false;
		return false;
	}

	if ( ( nMaxNumMessages < 1 ) || ( nMessagesSizeBytes < 8 ) )
	{
		m_bConfigOk = false;
		return false;
	}

	snprintf(m_sName, MAX_NAME_LENGTH_BYTES-1, "%s", sName);
	m_nMaxNumMessages = nMaxNumMessages;
	m_nMessageSizeBytes = nMessagesSizeBytes;
	m_nMessageBufferSize = m_nMessageSizeBytes + MESSAGE_SIZE_PADDING_NUMBYTES;

	m_nDescriptor = mq_open(m_sName, O_RDONLY | O_WRONLY | O_RDWR );
	if ( m_nDescriptor == -1 )
	{
		m_attrMsgQueue.mq_flags = 0;
		m_attrMsgQueue.mq_maxmsg = m_nMaxNumMessages;
		m_attrMsgQueue.mq_msgsize = m_nMessageSizeBytes;
		m_attrMsgQueue.mq_curmsgs = 0;
		m_nDescriptor = mq_open(m_sName, nFlags | O_CREAT, DEFAULT_QUEUE_PERMISSIONS, &m_attrMsgQueue);
		if ( m_nDescriptor == -1 )
		{
			if ( m_bEnableDebugPrintf )
			{
				char sError[512];
				sprintf(sError, "MessageQueue::create() unable to create message queue <%s>", m_sName);
				perror(sError);
			}
			m_bConfigOk = false;
			return false;
		}
	}
	else
	{
		if ( m_bEnableDebugPrintf )
		{
			printf((char*)"MessageQueue::create() <%s> (qd=%d) was already present\n", m_sName, m_nDescriptor);
		}
	}

	m_bConfigOk = true;
	return true;

}

bool MessageQueue::open(const char* sName, const int nFlags)
{
	size_t nStrLen = strlen(sName);
	if ( ( nStrLen < 2 ) || ( sName[0] != '/' ) )
	{
		m_bConfigOk = false;
		return false;
	}

	if ( ( nFlags != O_RDONLY ) && ( nFlags != O_WRONLY ) && ( nFlags != O_RDWR) )
	{
		m_bConfigOk = false;
		return false;
	}

	snprintf(m_sName, MAX_NAME_LENGTH_BYTES - 1, "%s", sName);
	m_nDescriptor = mq_open(m_sName, nFlags);
	if ( m_nDescriptor == -1 )
	{
		if ( m_bEnableDebugPrintf )
		{
			char sError[512];
			sprintf(sError, "MessageQueue::open() unable to open message queue <%s>", m_sName);
			perror(sError);
		}
		m_bConfigOk = false;
		return false;
	}
	else
	{
		int nRetGetAttr = mq_getattr(m_nDescriptor, &m_attrMsgQueue);
		if ( nRetGetAttr == -1 )
		{
			if ( m_bEnableDebugPrintf )
			{
				perror("MessageQueue::open() message queue opened but unable to retrieve attributes");
			}
			m_bConfigOk = false;
			return false;
		}
		m_nMaxNumMessages = m_attrMsgQueue.mq_maxmsg;
		m_nMessageSizeBytes = m_attrMsgQueue.mq_msgsize;
		if ( m_bEnableDebugPrintf )
		{
			printf((char*)"MessageQueue::open() <%s> (qd=%d) correctly opened size=%d currNumMsg=%ld singleMsgSize=%d\n",
				   m_sName, m_nDescriptor, m_nMaxNumMessages, m_attrMsgQueue.mq_curmsgs, m_nMessageSizeBytes);
		}
	}

	m_bConfigOk = true;
	return true;
}

int MessageQueue::retrieveCurrentNumMessages(void)
{
	if ( !m_bConfigOk )
		return -1;
	int nRetGetAttr = mq_getattr(m_nDescriptor, &m_attrMsgQueue);
//	printf("MessageQueue::retrieveCurrentNumMessages--> <%s>\n", strerror(errno));
	if ( nRetGetAttr == -1 )
	{
		if ( m_bEnableDebugPrintf )
		{
			perror("MessageQueue::retrieveCurrentNumMessages() unable to retrieve queue attributes ");
		}
		return -1;
	}
	return (int)m_attrMsgQueue.mq_curmsgs;
}

int MessageQueue::getName(char * sName )
{
	if (m_sName != NULL)
	{
		uint8_t ubLen = strlen( m_sName );
		memcpy(sName, m_sName, ubLen);
	}
	else
	{
		return -1;
	}
	return 0;
}

bool MessageQueue::empty(void)
{

	if ( !m_bConfigOk )
		return false;

	int nRetGetAttr = mq_getattr(m_nDescriptor, &m_attrMsgQueue);
	if ( nRetGetAttr == -1 )
	{
		if ( m_bEnableDebugPrintf )
		{
			perror("MessageQueue::empty() unable to retrieve queue attributes");
		}
		return false;
	}

	int nNumMessages = (int)m_attrMsgQueue.mq_curmsgs;
	if ( nNumMessages <= 0 )
	{
		if ( m_bEnableDebugPrintf )
		{
			printf("MessageQueue::empty() queue is empty\n");
		}
		return true;
	}

	char sInBuffer[m_nMessageBufferSize];
	if ( m_bEnableDebugPrintf )
	{
		printf("MessageQueue::empty() found %d messages\n", nNumMessages);
	}
	for (int i=0; i<nNumMessages; i++)
	{
		if ( mq_receive(m_nDescriptor, sInBuffer, m_nMessageBufferSize, NULL) == -1 )
		{
			if ( m_bEnableDebugPrintf )
			{
				perror("MessageQueue::empty() mq_receive");
			}
		}
		sInBuffer[m_nMessageBufferSize-1] = '\0';
		if ( m_bEnableDebugPrintf )
		{
			printf("MessageQueue::empty() message %d <%s>\n", i, sInBuffer);
		}
	}
	return true;

}

int MessageQueue::receive(char* sBuffer, const int nBufferSize, unsigned int* nMsgPriority, const int nTimeoutMilliSeconds)
{
	if ( !m_bConfigOk )
		return -1;

	int nNumBytesReceived = -1;
	if ( nTimeoutMilliSeconds > 0 )
	{
		TimeStamp nTS;
		unsigned long long nWait = nTS.get();
		nWait += (nTimeoutMilliSeconds*NSEC_PER_MSEC);
		nTS.set(nWait);
		struct timespec ts;
		nTS.tsToTimespec(&ts);

		nNumBytesReceived = (int)mq_timedreceive(m_nDescriptor, sBuffer, nBufferSize, nMsgPriority, &ts);
	}
	else
	{
		nNumBytesReceived = (int)mq_receive(m_nDescriptor, sBuffer, nBufferSize, nMsgPriority);
	}

	if ( ( nNumBytesReceived == -1 ) && ( m_bEnableDebugPrintf ) )
	{
		if ( errno == ETIMEDOUT )
		{
			perror("MessageQueue::receive() timeout on mq_timedreceive()");
		}
		else
		{
			perror("MessageQueue::receive() error from mq_receive()");
		}
	}
	return nNumBytesReceived;
}

bool MessageQueue::send(char* sBuffer, const int nBufferSize, const unsigned int nMsgPriority, const int nTimeoutMilliSeconds)
{
	if ( !m_bConfigOk )
		return false;

	if ( nTimeoutMilliSeconds > 0 )
	{
		TimeStamp nTS(nTimeoutMilliSeconds*NSEC_PER_MSEC);
		struct timespec ts;
		nTS.tsToTimespec(&ts);
		if ( (int)mq_timedsend(m_nDescriptor, sBuffer, nBufferSize, nMsgPriority, &ts) == -1 )
		{
			if ( m_bEnableDebugPrintf )
			{
				if ( errno == ETIMEDOUT )
				{
					perror("MessageQueue::send() timeout on mq_timedsend()");
				}
				else
				{
					perror("MessageQueue::send() error from mq_timedsend()");
				}
			}
			return false;
		}
	}
	else
	{
		if ( mq_send(m_nDescriptor, sBuffer, nBufferSize, nMsgPriority) == -1 )
		{
			if ( m_bEnableDebugPrintf )
			{
				perror("MessageQueue::send() unable to send message");
			}
			return false;
		}
	}
	return true;
}

bool MessageQueue::close(void)
{
	int d = mq_close(m_nDescriptor);
	if ( d == -1 )
	{
		if ( m_bEnableDebugPrintf )
		{
			perror("MessageQueue::close() error from mq_close()");
		}
		return false;
	}
	return true;
}

bool MessageQueue::unlink(void)
{
	int d = mq_unlink(m_sName);
	if ( d == -1 )
	{
		if ( m_bEnableDebugPrintf )
		{
			perror("MessageQueue::unlink() error from mq_unlink()");
		}
		return false;
	}
	return true;
}

bool MessageQueue::closeAndUnlink(void)
{
	bool bRet = close();
	if ( bRet )
	{
		bRet = unlink();
	}
	return bRet;
}

void MessageQueue::enableDebug(bool bEnable)
{
	m_bEnableDebugPrintf = bEnable;
}
