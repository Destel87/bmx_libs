//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    MessageQueue.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the MessageQueue class.
//! @details
//!
//*!****************************************************************************

#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H

#include <mqueue.h>

//default define
#define MAX_NAME_LENGTH_BYTES				256
#define DEFAULT_QUEUE_PERMISSIONS			0660
#define MESSAGE_SIZE_PADDING_NUMBYTES		16

class MessageQueue
{

	public:

		/*! ***********************************************************************************************************
		* void constructor
		* *************************************************************************************************************
		*/
		MessageQueue();

		/*! ***********************************************************************************************************
		* Default destructor
		* *************************************************************************************************************
		*/
		virtual ~MessageQueue();

		/*! ***********************************************************************************************************
		 * @brief						creates a Posix message queue
		 * @param sName					message queue name, must begin with '/' and must be non-zero length
		 * @param nFlags				can be O_RDONLY, or O_WRONLY or O_RDWR
		 * @param nMaxNumMessages		message queue size
		 * @param nMessagesSizeBytes	single message size in bytes
		 * @return						true upon succesfull creation, false otherwise or the queue already existed
		 * ************************************************************************************************************
		 */
		bool create(const char* sName, const int nFlags, const int nMaxNumMessages, const int nMessagesSizeBytes);

		/*! ***********************************************************************************************************
		 * @brief						tries to open the specified Posix message queue and initializes internal
		 *								members, the message queue must exist
		 * @param sName					message queue name, must begin with '/' and must be non-zero length
		 * @param nFlags				can be O_RDONLY, or O_WRONLY or O_RDWR
		 * @return						true upon success, false otherwise
		 * ************************************************************************************************************
		 */
		bool open(const char* sName, const int nFlags);

		/*! ***********************************************************************************************************
		 * @brief						retrieves the current number of messages in the queue
		 * @return						current number of messages in the queue (if minor than zero there was an error)
		 * ************************************************************************************************************
		 */
		int retrieveCurrentNumMessages(void);

		/*! ***********************************************************************************************************
		 * @brief						retrieve the set name of used message queue
		 * @param[out] sName			name of the message queue
		 * @return						0 upon success, -1 otherwise
		 * ************************************************************************************************************
		 */
		int getName(char* sName);

		/*! ***********************************************************************************************************
		 * @brief						empty the queue and discards all the messages found
		 * @return						true upon success, false otherwise
		 * ************************************************************************************************************
		 */
		bool empty(void);

		/*! ***********************************************************************************************************
		 * @brief						receive a message on the queue until the specified timeout has expired
         * @param sBuffer [out]			received buffer
		 * @param nBufferSize			received buffer size in bytes
         * @param nMsgPriority [out]	priority of the received message
		 * @param nTimeoutMilliSeconds	timeout in milliseconds, if 0 this method is blocking
		 * @return						the number of bytes received, if -1 an error has occured or timeout has expired
		 * ************************************************************************************************************
		 */
		int receive(char* sBuffer, const int nBufferSize, unsigned int* nMsgPriority, const int nTimeoutMilliSeconds = 0);

		/*! ***********************************************************************************************************
		 * @brief						tries to send a message on the queue until the specified timeout has expired
		 * @param sBuffer				message buffer to send
		 * @param nBufferSize			message buffer size in bytes
		 * @param nMsgPriority			priority of the message to send
		 * @param nTimeoutMilliSeconds	timeout in milliseconds, if 0 this method is blocking
		 * @return						true upon success, false otherwise
		 * ************************************************************************************************************
		 */
		bool send(char* sBuffer, const int nBufferSize, const unsigned int nMsgPriority = 0, const int nTimeoutMilliSeconds = 0);

		/*! ***********************************************************************************************************
		 * @brief						closes the message queue
		 * @return						true upon success, false otherwise
		 * ************************************************************************************************************
		 */
		bool close(void);

		/*! ***********************************************************************************************************
		 * @brief						destroys the message queue
		 * @return						true upon success, false otherwise
		 * ************************************************************************************************************
		 */
		bool unlink(void);

		/*! ***********************************************************************************************************
		 * @brief						closes and destroys the message queue
		 * @return						true upon success, false otherwise
		 * ************************************************************************************************************
		 */
		bool closeAndUnlink(void);

		/*! ***********************************************************************************************************
		 * @brief enableDebug			enable / disable debug messages using printf
		 * @param bEnable				true to enable messages, false to disable messages
		 * ************************************************************************************************************
		 */
		void enableDebug(bool bEnable);

    private:

        bool m_bConfigOk;
        mqd_t m_nDescriptor;					// descriptor initialized after open()
        char m_sName[MAX_NAME_LENGTH_BYTES];	// queue file name
        int m_nMaxNumMessages;					// queue size
        int m_nMessageSizeBytes;				// single message size
        int m_nMessageBufferSize;				// MESSAGE_SIZE_PADDING_NUMBYTES more than m_nMessageSizeBytes
        struct mq_attr m_attrMsgQueue;			// helper for internal operations
        bool m_bEnableDebugPrintf;				// to enable debug messages using printf

};

#endif // MESSAGEQUEUE_H
