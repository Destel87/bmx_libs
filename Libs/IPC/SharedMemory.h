//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SharedMemory.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the SharedMemory class.
//! @details
//!
//*!****************************************************************************

#ifndef SHAREDMEMORY_H
#define SHAREDMEMORY_H


class SharedMemory
{
	public:

		/*! ***************************************************************************************
		* void constructor
		* *****************************************************************************************
		*/
		SharedMemory();

		/*! ***************************************************************************************
		* Default destructor
		* *****************************************************************************************
		*/
		virtual ~SharedMemory();

};

#endif // SHAREDMEMORY_H
