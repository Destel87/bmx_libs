#ifndef NETWORKIPC_H
#define NETWORKIPC_H

#define IPC_MAGIC		0x14052001

/*
 * This is used to send back the result of an update.
 * It is strictly forbidden to change the order of entries.
 * New values should be put at the end without altering the order.
 */

typedef enum {
    IDLE,
    START,
    RUN,
    SUCCESS,
    FAILURE,
    DOWNLOAD,
    DONE,
    SUBPROCESS,
} RECOVERY_STATUS;

typedef enum {
    SOURCE_UNKNOWN,
    SOURCE_WEBSERVER,
    SOURCE_SURICATTA,
    SOURCE_DOWNLOADER,
    SOURCE_LOCAL
} sourcetype;

typedef enum {
    REQ_INSTALL,
    ACK,
    NACK,
    GET_STATUS,
    POST_UPDATE,
    SWUPDATE_SUBPROCESS,
    REQ_INSTALL_DRYRUN,
} msgtype;

enum {
    CMD_ACTIVATION,
    CMD_CONFIG
};

typedef union {
    char msg[128];
    struct {
        int current;
        int last_result;
        int error;
        char desc[2048];
    } status;
    struct {
        sourcetype source; /* Who triggered the update */
        int	cmd;	   /* Optional encoded command */
        int	timeout;     /* timeout in seconds if an aswer is expected */
        unsigned int len;    /* Len of data valid in buf */
        char	buf[2048];   /*
                      * Buffer that each source can fill
                      * with additional information
                      */
    } instmsg;
} msgdata;

typedef struct {
    int magic;	/* magic number */
    int type;
    msgdata data;
} ipc_message;

/*
 * Message sent via progress socket.
 * Data is sent in LE if required.
 */
struct progress_msg {
    unsigned int	magic;		/* Magic Number */
    RECOVERY_STATUS	status;		/* Update Status (Running, Failure) */
    unsigned int	dwl_percent;	/* % downloaded data */
    unsigned int	nsteps;		/* No. total of steps */
    unsigned int	cur_step;	/* Current step index */
    unsigned int	cur_percent;	/* % in current step */
    char		cur_image[256];	/* Name of image to be installed */
    char		hnd_name[64];	/* Name of running hanler */
    sourcetype	source;		/* Interface that triggered the update */
    unsigned int 	infolen;    	/* Len of data valid in info */
    char		info[2048];   	/* additional information about install */
};

class NetworkIPC
{
    public:
    /*! ***********************************************************************************************************
    * void constructor
    * *************************************************************************************************************
    */
    NetworkIPC();

    /*! ***********************************************************************************************************
    * Default destructor
    * *************************************************************************************************************
    */
    virtual ~NetworkIPC();

    int getstatusIPC(ipc_message *msg);

    int waitForComplete(void);

    int connectProgressIPCwithPath(const char *socketpath, bool reconnect);

    int connectProgressIPCWrap(bool reconnect);

    int receiveProgressIPC(int *connfd, struct progress_msg *msg);

    private:

        char *getCtrlSocket(void);

        char* getProgrSocket();

        int prepareIPC(void);

        int getstatusIPCtimeout(int connfd, ipc_message *msg, unsigned int timeout_ms);

        int connectProgressIPC(const char *socketpath, bool reconnect);

    private:

        char* socketCtrlPath;
        char* socketProgressPath;
};

#endif // NETWORKIPC_H
