//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    I2CDevice.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the I2CDevice class.
//! @details
//!
//*!****************************************************************************

#ifndef I2CDEVICE_H
#define I2CDEVICE_H

#include <stdint.h>

#include "Loggable.h"
#include "Mutex.h"


/*! ***********************************************************************************************
 * MACROS
 * ************************************************************************************************
 */
#define I2CDEVICE_VERSION "1.0.0"

/*! ***********************************************************************************************
* @class    I2CDevice
* @brief    Represents an I2C device with basic funcionalities of read and write
* *************************************************************************************************
*/
class I2CDevice : public Loggable
{
	public:

		/*! ***************************************************************************************
		 * void constructor
		 * ****************************************************************************************
		 */
		I2CDevice();

		/*! ***************************************************************************************
		 * default destructor
		 * ****************************************************************************************
		 */
		virtual ~I2CDevice();

		/*! ***************************************************************************************
		 * Initializes the class
		 * Tries to open the file and, upon success, retrieves the bus functionalities and set the
		 * device address. After this operation has succeded, the user can perform read/write
		 * operations.
         * @param	sBusFileName name of the file that represents the interface with the low level
		 *			driver (e.g. /dev/i2c-0)
		 * @param	nAddress i2c device address
         * @param bMustSwapAddress
         * @param bTenBitAddress
		 * @retval	true upon successful initialization, false otherwise
		 * ****************************************************************************************
		 */
        bool init(const char* sBusFileName, const int32_t nAddress = INT32_MAX, bool bMustSwapAddress = false,
                  bool bTenBitAddress = false);

		/*! ***************************************************************************************
		 * Set the device address
		 * @param	nAddress i2c device address
		 * @retval	true if the device address is a valid number, false otherwise
		 * ****************************************************************************************
		 */
		bool setAddress(int32_t nAddress);

		/*! ***************************************************************************************
		 * Close the device and frees the underlying driver
		 * ****************************************************************************************
		 */
		void reset(void);

		/*! ***************************************************************************************
		 * Performs one or more read operations on the device using the ioctl method and the
		 * i2c_rdwr_ioctl_data structure. It is useful when many bytes should be read in continuous
		 * reading mode (if the slave supports it, for example an eeprom) on a 7 bit address device
		 * @param[out] pReadBuffer	pointer to the buffer in which the read bytes are stored
		 * @param nReadBufferLen	length in bytes of the buffer
		 * @param nRegAddr			starting address from which the read operations should begin
		 * @return					true upon succesfull reading, false otherwise
		 * ****************************************************************************************
		 */
		bool multiRead(uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint8_t nRegAddr);

		/*! ***************************************************************************************
		 * The same as the previous method but for 10 bit address devices
         * @param[out] pReadBuffer	pointer to the buffer in which the read bytes are stored
         * @param nReadBufferLen	length in bytes of the buffer
         * @param nRegAddr			starting address from which the read operations should begin
         * @return					true upon succesfull reading, false otherwise
         * ****************************************************************************************
		 */
		bool multiRead(uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint16_t nRegAddr);

		/*! ***************************************************************************************
		 * Performs one or more write operations on the device using the ioctl method and the
		 * i2c_rdwr_ioctl_data structure. It is useful when many bytes should be written in
		 * continuous writing mode (if the slave supports it, for example an eeprom)
		 * on a 7 bit address device
         * @param pWriteBuffer		pointer to the buffer to write on the device
         * @param nWriteBufferLen	number of bytes to write
		 * @param nRegAddr			starting address from which the write operations should begin
		 * @return					true upon succesfull write, false otherwise
		 * ****************************************************************************************
		 */
		bool multiWrite(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint8_t nRegAddr);

		/*! ***************************************************************************************
		 * The same as the previous method but for 10 bit address devices
         * @param pWriteBuffer		pointer to the buffer to write on the device
         * @param nWriteBufferLen	number of bytes to write
         * @param nRegAddr			starting address from which the write operations should begin
         * @return					true upon succesfull write, false otherwise
         * ****************************************************************************************
		 */
		bool multiWrite(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint16_t nRegAddr);

		/*! ***************************************************************************************
		 * Performs one read operation on the device using the read method.
		 * It is useful when a single register should be read on a 7 bit address device.
		 * @param nRegAddr		register address that should be read
         * @param nData [out]	the content of the register read from the device
		 * @return				true upon succesfull reading, false otherwise
		 * ****************************************************************************************
		 */
		bool readRegister(uint8_t nRegAddr, uint8_t* nData);

		/*! ***************************************************************************************
		 * The same as the previous method but for 10 bit address devices
         * @param nRegAddr		register address that should be read
         * @param nData [out]	the content of the register read from the device
         * @return				true upon succesfull reading, false otherwise
         * ****************************************************************************************
		 */
		bool readRegister(uint16_t nRegAddr, uint8_t* nData);

		/*! ***************************************************************************************
		 * Performs one write operation on the device.
		 * It is useful when a single register should be written on a 7 bit address device.
		 * @param nRegAddr	register address that should be written
		 * @param nData		value to write in the register
		 * @return			true upon succesfull writing, false otherwise
		 * ****************************************************************************************
		 */
		bool writeRegister(uint8_t nRegAddr, uint8_t nData);

		/*! ***************************************************************************************
		 * The same as the previous method but for 10 bit address devices
         * @param nRegAddr	register address that should be written
         * @param nData		value to write in the register
         * @return			true upon succesfull writing, false otherwise
         * ****************************************************************************************
		 */
		bool writeRegister(uint16_t nRegAddr, uint8_t nData);

	private:

		void formatRegAddr(uint8_t* pRegAddrBuff, uint32_t nRegAddr, int32_t nRegAddrLen);

		bool multiReadGeneral(uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint32_t nRegAddr, int32_t nRegAddrLen);
		bool multiWriteGeneral(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint32_t nRegAddr, int32_t nRegAddrLen);

		bool singleWriteGeneral(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen);
		bool writeRegisterGeneral(uint8_t nData, uint32_t nRegAddr, int32_t nRegAddrLen);

		bool readRegisterGeneral(uint32_t nRegAddr, int32_t nRegAddrLen, uint8_t* nData);

    private:

        bool m_bConfigOK;
        char m_sBusFileName[1024];		// Device file name, e.g. /dev/i2c-0
        int32_t m_nFD;					// Device file discriptor
        int32_t m_nAddress;				// Device address
        bool m_bTenBitAddress;			// Device address
        bool m_bMustSwapAddress;
        Mutex m_nMutexWrite;			// Provides protection during write operations
        Mutex m_nMutexRead;				// Provides protection during read operations
        uint8_t m_pTxBuffer[256];

};

#endif // I2CDEVICE_H
