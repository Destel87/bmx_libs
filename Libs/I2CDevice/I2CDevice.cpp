//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    I2CDevice.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the I2CDevice class.
//! @details
//!
//*!****************************************************************************

#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <limits>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "I2CDevice.h"


/*! ***********************************************************************************************
 * STATIC FUNCTIONS
 * ************************************************************************************************
 */
bool check_if_file_exists(const char* sFilename)
{
	int32_t nFd;
	nFd = open(sFilename, O_WRONLY);
	if ( nFd < 0 )
	{
		return false;
	}
	else
	{
		close(nFd);
		return true;
	}
}

/*! ***********************************************************************************************
 * CLASS IMPLEMENTATION
 * ************************************************************************************************
 */
I2CDevice::I2CDevice()
{
	m_bConfigOK = false;
	m_sBusFileName[0] = 0;
	m_nFD = -1;
	m_nAddress = INT32_MAX;
	m_bMustSwapAddress = false;
	m_bTenBitAddress = false;
}

I2CDevice::~I2CDevice()
{
	reset();
}

bool I2CDevice::init(const char* sBusFileName, const int32_t nAddress, bool bMustSwapAddress, bool bTenBitAddress)
{
	m_bConfigOK = false;
	if (!check_if_file_exists(sBusFileName))
	{
		log(LOG_ERR, "Cannot open file %s", sBusFileName);
		return false;
	}

	m_nFD = open(sBusFileName, O_RDWR);
	if ( m_nFD < 0 )
	{
		log(LOG_ERR, "I2CDevice: can't' open file <%s>", sBusFileName);
		return false;
	}
	snprintf(m_sBusFileName, 1023, "%s", sBusFileName);

	// Retrieve bus functionalities
	unsigned long funcs;
	if (ioctl(m_nFD, I2C_FUNCS, &funcs) < 0)
	{
		log(LOG_ERR, "I2CDevice: cannot retrieve bus functionalities (%s)", strerror(errno));
		close(m_nFD);
		m_nFD = -1;
		return false;
	}

	// Eventually set slave address
	if ( nAddress != INT32_MAX )
	{
		if ( ioctl(m_nFD, I2C_SLAVE, nAddress) < 0 )
		{
			log(LOG_ERR, "I2CDevice: cannot set slave address %d (%s)", nAddress, strerror(errno));
			return false;
		}
		if ( bTenBitAddress )
		{
			if ( ioctl(m_nFD, I2C_TENBIT, 1) < 0 )
			{
				log(LOG_ERR, "I2CDevice: cannot set 10 bit address %d (%s)", nAddress, strerror(errno));
				return false;
			}
			m_bTenBitAddress = true;

		}
		m_nAddress = nAddress;
		m_bMustSwapAddress = bMustSwapAddress;
		m_bConfigOK = true;
		log(LOG_DEBUG_PARANOIC, "I2CDevice: set slave address to %#x", m_nAddress);
	}

	return true;

}

bool I2CDevice::setAddress(int32_t nAddress)
{
	if ( m_nFD < 0 )
	{
		log(LOG_ERR, "I2CDevice: file <%s> is not open", m_sBusFileName);
		m_bConfigOK = false;
		return false;
	}
	if ( nAddress != INT32_MAX )
	{
		if ( ioctl(m_nFD, I2C_SLAVE, nAddress) < 0 )
		{
			log(LOG_ERR, "I2CDevice: cannot set slave address %d (%s)", nAddress, strerror(errno));
			m_bConfigOK = false;
			return false;
		}
		m_nAddress = nAddress;
		m_bConfigOK = true;
		log(LOG_DEBUG_PARANOIC, "I2CDevice: slave address %d correctly set", m_nAddress);
	}
	return m_bConfigOK;
}

void I2CDevice::reset(void)
{
	m_bConfigOK = false;
	if ( m_nFD >= 0 )
	{
		if ( close(m_nFD) != 0 )
			log(LOG_ERR, "I2CDevice: unable to close file descriptor of device %s", m_sBusFileName);
		else
			log(LOG_INFO, "I2CDevice: file descriptor of device %s correctly closed", m_sBusFileName);
	}
	m_nFD = -1;
	m_nAddress = INT32_MAX;
}

bool I2CDevice::multiRead(uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint8_t nRegAddr)
{
	return multiReadGeneral(pReadBuffer, nReadBufferLen, nRegAddr, 1);
}

bool I2CDevice::multiRead(uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint16_t nRegAddr)
{
	return multiReadGeneral(pReadBuffer, nReadBufferLen, nRegAddr, 2);
}

bool I2CDevice::multiWrite(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint8_t nRegAddr)
{
	return multiWriteGeneral(pWriteBuffer, nWriteBufferLen, nRegAddr, 1);
}

bool I2CDevice::multiWrite(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint16_t nRegAddr)
{
	return multiWriteGeneral(pWriteBuffer, nWriteBufferLen, nRegAddr, 2);
}

void I2CDevice::formatRegAddr(uint8_t* pRegAddrBuff, uint32_t nRegAddr, int32_t nRegAddrLen)
{
	for (int32_t i=0; i<(int32_t)sizeof(uint32_t); i++)
		pRegAddrBuff[i] = 0;

	switch ( nRegAddrLen )
	{
		case 1:
			pRegAddrBuff[0] = nRegAddr & 0xFF;
		break;
		case 2:
			pRegAddrBuff[0] = nRegAddr >> 8;
			pRegAddrBuff[1] = nRegAddr & 0xFF;
		break;
		case 3:
			pRegAddrBuff[0] = nRegAddr >> 16;
			pRegAddrBuff[1] = nRegAddr >> 8;
			pRegAddrBuff[2] = nRegAddr & 0xFF;
		break;
		case 4:
			pRegAddrBuff[0] = nRegAddr >> 24;
			pRegAddrBuff[1] = nRegAddr >> 16;
			pRegAddrBuff[2] = nRegAddr >> 8;
			pRegAddrBuff[3] = nRegAddr & 0xFF;
		break;
		default:
		break;
	}

	if( m_bMustSwapAddress )
	{
		uint8_t uTmp  = 0;
		switch ( nRegAddrLen )
		{
			case 1:
			break;
			case 2:
				uTmp = pRegAddrBuff[0];
				pRegAddrBuff[0] = pRegAddrBuff[1];
				pRegAddrBuff[1] = uTmp;
			break;
			case 3:
			break;
			case 4:
				uTmp = pRegAddrBuff[0];
				pRegAddrBuff[0] = pRegAddrBuff[3];
				pRegAddrBuff[3] = uTmp;
				uTmp = pRegAddrBuff[1];
				pRegAddrBuff[1] = pRegAddrBuff[2];
				pRegAddrBuff[2] = uTmp;
			break;
		}
	}
}

bool I2CDevice::multiReadGeneral(uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint32_t nRegAddr, int32_t nRegAddrLen)
{
	if ( m_bConfigOK == false )
		return false;

	uint8_t pRegAddrBuff[4];
	formatRegAddr(pRegAddrBuff, nRegAddr, nRegAddrLen);

	uint16_t readFlags = 0;
	if (m_bTenBitAddress)
		readFlags = I2C_M_TEN;

	m_nMutexRead.lock();

	struct i2c_msg msgs[2];
	msgs[0].addr = static_cast<uint16_t>(m_nAddress);
	msgs[0].flags = 0;
	msgs[0].len = static_cast<uint16_t>(nRegAddrLen);
	msgs[0].buf = (uint8_t*)pRegAddrBuff;

	msgs[1].addr = static_cast<uint16_t>(m_nAddress);
	msgs[1].flags = I2C_M_RD | readFlags;
	msgs[1].len = static_cast<uint16_t>(nReadBufferLen);
	msgs[1].buf = (uint8_t*)pReadBuffer;

	struct i2c_rdwr_ioctl_data data;
	data.msgs = msgs;
	data.nmsgs = 2;

	int32_t nRet;
	do
	{
		nRet = ioctl(m_nFD, I2C_RDWR, &data);
	} while ( ( nRet < 0 ) && ( errno == EREMOTEIO || errno == EIO ) );

	m_nMutexRead.unlock();

	if ( nRet < 0 )
	{
		log(LOG_ERR, "I2CDevice: cannot read data");
		return false;
	}
	else
	{
		log(LOG_DEBUG_PARANOIC, "I2CDevice: ioctl read reported %d", nRet);
	}
	return true;
}

bool I2CDevice::multiWriteGeneral(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint32_t nRegAddr, int32_t nRegAddrLen)
{
	if ( m_bConfigOK == false )
		return false;

	uint8_t pRegAddrBuff[4];
	formatRegAddr(pRegAddrBuff, nRegAddr, nRegAddrLen);

	uint16_t writeFlags = 0;
	if (m_bTenBitAddress)
		writeFlags = I2C_M_TEN;

	m_nMutexWrite.lock();

	for (int i=0; i<(int)nRegAddrLen; i++)
	{
		m_pTxBuffer[i] = pRegAddrBuff[i];
	}

	for (int i=0; i<(int)nWriteBufferLen; i++)
	{
		m_pTxBuffer[i+nRegAddrLen] = pWriteBuffer[i];
	}

	struct i2c_msg msgs;
	msgs.addr = static_cast<uint16_t>(m_nAddress);
	msgs.flags = 0 | writeFlags;
	msgs.len = static_cast<uint16_t>(nRegAddrLen+nWriteBufferLen);
	msgs.buf = m_pTxBuffer;

	struct i2c_rdwr_ioctl_data data;
	data.msgs = &msgs;
	data.nmsgs = 1;

	int32_t nRet;
	do
	{
		nRet = ioctl(m_nFD, I2C_RDWR, &data);
	} while ( ( nRet < 0 ) && ( errno == EREMOTEIO || errno == EIO ) );

	m_nMutexWrite.unlock();

	if ( nRet < 0 )
	{
		log(LOG_ERR, "I2CDevice: cannot write data");
		return false;
	}
	else
	{
		log(LOG_DEBUG_PARANOIC, "I2CDevice: ioctl write reported %d", nRet);
	}
	return true;
}

bool I2CDevice::writeRegister(uint8_t nRegAddr, uint8_t nData)
{
	return writeRegisterGeneral(nData, nRegAddr, 1);
}

bool I2CDevice::writeRegister(uint16_t nRegAddr, uint8_t nData)
{
	return writeRegisterGeneral(nData, nRegAddr, 2);
}

bool I2CDevice::writeRegisterGeneral(uint8_t nData, uint32_t nRegAddr, int32_t nRegAddrLen)
{
	if ( m_bConfigOK == false )
		return false;
	return multiWriteGeneral(&nData, 1, nRegAddr, nRegAddrLen);
}

bool I2CDevice::singleWriteGeneral(uint8_t *pWriteBuffer, uint32_t nWriteBufferLen)
{
	if ( m_bConfigOK == false )
		return false;

	int32_t nRet = write(m_nFD, pWriteBuffer, nWriteBufferLen);

	if ( nRet != (int32_t)nWriteBufferLen )
	{
		log(LOG_ERR, "I2CDevice: cannot write data");
		return false;
	}
	else
	{
		log(LOG_DEBUG_PARANOIC, "I2CDevice: write reported %d", nRet);
	}
	return true;
}


bool I2CDevice::readRegister(uint8_t nRegAddr, uint8_t* nData)
{
	return readRegisterGeneral(nRegAddr, 1, nData);
}


bool I2CDevice::readRegister(uint16_t nRegAddr, uint8_t* nData)
{
	return readRegisterGeneral(nRegAddr, 2, nData);
}


bool I2CDevice::readRegisterGeneral(uint32_t nRegAddr, int32_t nRegAddrLen, uint8_t* nData)
{
	if ( m_bConfigOK == false )
		return false;
	return multiReadGeneral(nData, 1, nRegAddr, nRegAddrLen);
}
