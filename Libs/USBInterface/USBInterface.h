//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    USBInterface.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the USBInterface class.
//! @details
//!
//*!****************************************************************************

#ifndef CDCINTERFACE_H
#define CDCINTERFACE_H

#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <sys/termios.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "Mutex.h"
#include "Loggable.h"
#include "CommonInclude.h"


#define CAM_INTERFACE_DEFAULT_READ_TIMEOUT_MSEC         1

using namespace std;

/*! *****************************************************************************
 * @brief Represents a USB interface with basic functionalities of read and write
 *******************************************************************************/

class USBInterface : public Loggable
{
	public:

		USBInterface();

		virtual ~USBInterface();

		/*! **********************************************************************************************************************
		 * @brief  initInterface initialize USB interface (CDC device class).
         * @param  strName name of the port to be used i.e. /dev/ttyACM0
		 * @param  iBits bit per word.
         * @param  strParity enable parity or not. Possible inputs: "N" = none, "E" = even, "O" = odd, "M" = mark, or "S" = space.
		 * @param  bHasRts hardware flow control.
		 * @param  bHasXon software flow control.
         * @param  liReadTimeoutMsec read timeout in msec.
		 * @return true if success, false otherwise.
		 * ***********************************************************************************************************************
		 */
        bool initInterface(string strName, int32_t iBits = CAM_INTERFACE_DEFAULT_NUMBITSPERWORD,
                           string strParity = "N", bool bHasRts = 0, bool bHasXon = 0, int32_t liReadTimeoutMsec = 0);

		/*! **********************************************************************************************************************
		 * @brief  Open serial communication and set the Parameters saved as members with the initSerial function.
		 * @return -2 if setattr does not work for some reason, -1 if initSerial is not called yet, 0 if succeeded, >0 with errno
		 *		   code due to the failure of open() function.
		 * ***********************************************************************************************************************
		 */
		int openInterface(void);

		/*! **********************************************************************************************************************
		 * @brief  isOpen check if serial port is open or not.
		 * @return 0 if the port is open, -1 if not.
		 * ***********************************************************************************************************************
		 */
		bool isOpen(void);

		/*! **********************************************************************************************************************
		 * @brief  setReadTimeout set time in which the system tries to read from USB channel.
		 * @param  liTimeoutMsec time in msec.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool setReadTimeout(int32_t liTimeoutMsec);

		/*! **********************************************************************************************************************
		 * @brief  readFromSerialTimeout reads num_chars bytes and save them in a buffer.
         * @param  rgBuffer destination of char* characters read on serial.
         * @param  uliBufferSize number of characters to be read (int32_t).
		 * @param  liTimeoutMsec time when the function monitors the file descriptor.
		 * @param  liOperationResult	if some error occurs, this parameter contains the value of ERRNO
		 * @return -2 if port is not open, -1 if reading fails, 0 if EOF, number of byte read if success (>0).
		 * ***********************************************************************************************************************
		 */
		int32_t readBufferWithTimeout(void* rgBuffer, uint32_t uliBufferSize, int32_t& liOperationResult, int32_t liTimeoutMsec = 0);

		/*! **********************************************************************************************************************
		 * @brief  Clean everything received until when is invoked.
		 * @return -2 if port is not open, -1 if select does not work, 0 in case of success.
		 * ***********************************************************************************************************************
		 */
		int32_t cleanAll(void);

		/*! **********************************************************************************************************************
		 * @brief  writeOnSerial write num_chars bytes from a buffer.
         * @param  rgBuffer where the string I want to write is stored.
         * @param  uliBufferSize number of characters I want to write on serial.
         * @param  liOperationResult	if some error occurs, this parameter contains the value of ERRNO
         * @return -1 if writing fails, number of byte written if success (>0).
		 * ***********************************************************************************************************************
		 */
		int32_t writeBuffer(void* rgBuffer, uint32_t uliBufferSize, int32_t& liOperationResult);

		/*! **********************************************************************************************************************
		 * @brief  close serial communication.
		 * @return -1 if port not open, otherwise result of the closing operation.
		 * ***********************************************************************************************************************
		 */
		int32_t closeInterface(void);

		/*! **********************************************************************************************************************
		 * @brief set the name of the port we want to use.
		 * @param sName port name.
		 * ***********************************************************************************************************************
		 */
		bool setPortName(string sName);


		/*! **********************************************************************************************************************
		 * @brief set or reset Hardware flow control.
		 * @param bHasRts hardware flow control.
		 * ***********************************************************************************************************************
		 */
		void setHardwareFlowControl(bool bHasRts);

		/*! **********************************************************************************************************************
		 * @brief set or reset Software flow control.
		 * @param bHasXon Software flow control.
		 * ***********************************************************************************************************************
		 */
		void setSoftwareFlowControl(bool bHasXon);

		/*! **********************************************************************************************************************
		 * @brief set parity.
		 * @param strParity can be "E" = even or "O" = odd.
		 * ***********************************************************************************************************************
		 */
		void setParity(string strParity);

		/*! **********************************************************************************************************************
		 * @brief set the data bits.
		 * @param liBits databits (usually 8).
		 * ***********************************************************************************************************************
		 */
		void setDataBits(int32_t liBits);

	private:

		/*! **********************************************************************************************************************
		 * @brief setParameters of the serial communication saved as members.
		 * ***********************************************************************************************************************
		 */
		bool setParameters(void);


    private:

        bool			m_bConfigOk;

        int32_t			m_liFD;         // serial file descriptor
        string			m_strName;
        int8_t			m_cBits;
        string			m_strParity;
        bool			m_bHasRts;     // Hardware Flow Control
        bool			m_bHasXon;     // Software Flow Control
        struct termios	m_stty;

        Mutex           m_mutexWrite;
        Mutex           m_mutexRead;

        int32_t			m_liReadTimeoutMsec;

};

#endif // CDCINTERFACE_H
