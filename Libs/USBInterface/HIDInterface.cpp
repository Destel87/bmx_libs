//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HIDInterface.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the HIDInterface class.
//! @details
//!
//*!****************************************************************************

#include "HIDInterface.h"

HIDInterface::HIDInterface()
{
	m_bConfigOk = false;
	m_pDevice = 0x00;
	m_pLogger = 0x00;
}

HIDInterface::~HIDInterface()
{
	closeHID();
}

bool HIDInterface::init(int liVID, int liPID, Log* pLogger)
{
	if ( m_bConfigOk == false )
	{
		if ( hid_init() == -1 )
		{
			return false;
		}
	}

	m_bConfigOk = false;

	if ( pLogger == 0 )	return false;

	m_pLogger = pLogger;
	m_pDevice = hid_open(liVID, liPID, NULL);

	if ( ! m_pDevice )
	{
		m_pLogger->log(LOG_ERR, "HIDInterface::init: unable to open device");
		return false;
	}

	m_bConfigOk = true;
	return true;
}

bool HIDInterface::printHIDInfo(void)
{
	if ( ! m_bConfigOk )	return false;

	#define MAX_STR 255
	wchar_t wstr[MAX_STR];
	int liRes;

	// Read the Manufacturer String
	wstr[0] = 0x0000;
	liRes = hid_get_manufacturer_string(m_pDevice, wstr, MAX_STR);
	if ( liRes < 0 )
	{
		m_pLogger->log(LOG_ERR, "HIDInterface::printHIDInfo: Unable to read manufacturer string");
	}
	else
	{
		m_pLogger->log(LOG_INFO, "HIDInterface::printHIDInfo: Manufacturer String: %ls", wstr);
	}

	// Read the Product String
	wstr[0] = 0x0000;
	liRes = hid_get_product_string(m_pDevice, wstr, MAX_STR);
	if ( liRes < 0 )
	{
		m_pLogger->log(LOG_ERR, "HIDInterface::printHIDInfo: Unable to read product string");
	}
	else
	{
		m_pLogger->log(LOG_INFO, "HIDInterface::printHIDInfo: Product String: %ls", wstr);
	}

	// Read the Serial Number String
	wstr[0] = 0x0000;
	liRes = hid_get_serial_number_string(m_pDevice, wstr, MAX_STR);
	if ( liRes < 0 )
	{
		m_pLogger->log(LOG_ERR, "HIDInterface::printHIDInfo: Unable to read serial number string");
	}
	else
	{
		m_pLogger->log(LOG_INFO, "HIDInterface::printHIDInfo: Serial Number String: (%d) %ls", wstr[0], wstr);
	}

	printf("\n");

	// Read Indexed String 1
	wstr[0] = 0x0000;
	liRes = hid_get_indexed_string(m_pDevice, 1, wstr, MAX_STR);
	if ( liRes < 0 )
	{
		m_pLogger->log(LOG_ERR, "HIDInterface::printHIDInfo: Unable to read indexed string 1");
	}
	else
	{
		m_pLogger->log(LOG_INFO, "HIDInterface::printHIDInfo: Indexed String 1: %ls", wstr);
	}
	return true;
}

int HIDInterface::write(unsigned char *pBuff)
{
	if ( ! m_bConfigOk )	return false;

	int liRes = hid_write(m_pDevice, pBuff, 65);

	return liRes;
}

int HIDInterface::read(unsigned char *pBuff, int liMs)
{
	if ( ! m_bConfigOk )	return false;

	int liRes = hid_read_timeout(m_pDevice, pBuff, 65, liMs);

	return liRes;
}

int HIDInterface::sendFeatureReport(unsigned char* pBuff, int liSize)
{
	int result = hid_send_feature_report(m_pDevice, pBuff, liSize);

	return result;
}

void HIDInterface::closeHID()
{
	if ( m_bConfigOk )
	{
		// Perform HID close only if the device is open (otherwise gives a SIGABRT error)
		hid_close(m_pDevice);
	}

	hid_exit();
	m_bConfigOk = false;
}
