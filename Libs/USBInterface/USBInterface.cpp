//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    USBInterface.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the USBInterface class.
//! @details
//!
//*!****************************************************************************

#include <dirent.h>
#include <vector>

#include "USBInterface.h"
#include "TimeStamp.h"


USBInterface::USBInterface()
{
	m_bConfigOk = false;
	m_liFD = -1;         // serial file descriptor
	m_strName.clear();
	m_cBits = 0;
	m_strParity.clear();
	m_bHasRts = false;
	m_bHasXon = false;
	memset((void*)&m_stty, 0, sizeof(m_stty));
	m_liReadTimeoutMsec = CAM_INTERFACE_DEFAULT_READ_TIMEOUT_MSEC;
}


USBInterface::~USBInterface()
{
	closeInterface();
}


bool USBInterface::initInterface(string strName, int32_t iBits, string strParity,
								 bool bHasRts, bool bHasXon, int32_t liReadTimeoutMsec)
{
	m_strName.assign(strName);
	m_cBits = iBits;
	m_strParity.assign(strParity);
	m_bHasRts = bHasRts;
	m_bHasXon = bHasXon;
	bool bRetValue = setReadTimeout(liReadTimeoutMsec);
	if ( bRetValue == true )
		m_bConfigOk = true;
	else
		m_bConfigOk = false;
	return m_bConfigOk;
}



int USBInterface::openInterface(void)
{
	if ( !m_bConfigOk ) return -1;

	// make sure port is closed
	closeInterface();

	// Check if the device really exists, otherwise autodetect CDC device
	int liFileDescriptor = open( m_strName.c_str(), O_RDWR | O_NDELAY | O_NOCTTY );
	if ( liFileDescriptor > 0 )
	{
		close(liFileDescriptor);
	}
	else
	{
		DIR *dp;
		struct dirent *dirp;

		dp = opendir("/sys/class/tty");
		if ( dp != 0 )
		{
			while ( (dirp = readdir(dp) ) != 0 )
			{
				string sName = string(dirp->d_name);
				string sDefault("ttyACM");
				int liRes = sName.substr(0, sDefault.length()).compare(sDefault);

				if ( liRes == 0 )
				{
					m_strName = "/dev/" + sName;
					break;
				}
			}
			closedir(dp);
		}
	}

	/*! ***************************************************************************************************************
	 * Open the USB  port
	 *
	 * The O_NOCTTY flag tells UNIX that this program doesn't want to be the "controlling terminal" for that port.
	 * If you don't specify this then any input (such as keyboard abort signals and so forth) will affect your process.
	 *
	 * The O_NDELAY flag tells UNIX that this program doesn't care what state the DCD signal line is in,
	 * whether the other end of the port is up and running.
	 * If you do not specify this flag, your process will be put to sleep until the DCD signal line
	 * is the space voltage.
	 *
	 * If O_NONBLOCK is set, an open() for reading-only shall return without delay. An open() for writing-only shall
	 * return an error if no process currently has the file open for reading.
	 * ****************************************************************************************************************
	 */
	m_liFD = open( m_strName.c_str(), O_RDWR | O_NDELAY | O_NOCTTY );

	if ( m_liFD == -1 )
	{
		log(LOG_ERR, "USBInterface::openInterface: error %s", strerror(errno));
		return errno;
	}

	if ( !setParameters() )
	{
		log(LOG_ERR, "USBInterface::openInterface: error.");
		return -2;
	}
	return 0;
}


bool USBInterface::isOpen()
{
	return ( m_liFD > 0 ? true : false );
}


bool USBInterface::setReadTimeout(int32_t liTimeoutMsec)
{
	if ( liTimeoutMsec != m_liReadTimeoutMsec )
	{
		fd_set fdmask;
		struct timeval wait;

		if ( liTimeoutMsec > 0 )
		{
			wait.tv_sec = liTimeoutMsec / MSEC_PER_SEC ;
			wait.tv_usec = ( liTimeoutMsec % USEC_PER_MSEC ) * USEC_PER_MSEC;
		}
		else
		{
			wait.tv_sec = 0 ;
			wait.tv_usec = CAM_INTERFACE_DEFAULT_READ_TIMEOUT_MSEC;
		}

		// set socket descriptor for a select()
		FD_ZERO(&fdmask);
		FD_SET(m_liFD, &fdmask);

		//	 Select can return:
		//	 -1: an error was encountered.
		//	 0: the call timed out without any event ready for the sockets monitored
		//	 >0: is the number of sockets that have events pending
		int32_t nRetSelect = select(m_liFD + 1, &fdmask, (fd_set *)NULL, (fd_set *)NULL, &wait);
		if ( nRetSelect < 0 )
		{
			return false;
		}
	}
	return true;
}


int32_t USBInterface::readBufferWithTimeout(void *rgBuffer, uint32_t uliBufferSize, int32_t& liOperationResult, int32_t liTimeoutMsec)
{
	if ( !isOpen() ) return -2;

	if ( liTimeoutMsec != m_liReadTimeoutMsec )
	{
		setReadTimeout(liTimeoutMsec);
	}

	m_mutexRead.lock();
	int32_t nByte = read(m_liFD, rgBuffer, uliBufferSize);
	m_mutexRead.unlock();

	liOperationResult = 0;

	if ( nByte == -1 )
	{
		liOperationResult = errno;
	}

	return nByte;
}


int32_t USBInterface::cleanAll()
{
	if ( !isOpen() ) return -2;

	fd_set fdmask;
	struct timeval wait;
	wait.tv_sec = 0;
	wait.tv_usec = 10000;

	// set socket descriptor for a select()
	FD_ZERO(&fdmask);
	FD_SET(m_liFD, &fdmask);

	while (select(m_liFD + 1, &fdmask, (fd_set *) NULL, (fd_set *) NULL, &wait) == 0)
	{
		return (-1);
	}

	tcflush(m_liFD, TCIFLUSH);

	return (0);
}


int32_t USBInterface::writeBuffer(void *rgBuffer, uint32_t uliBufferSize, int32_t& liOperationResult)
{
	if ( !isOpen() ) return -2;

	m_mutexWrite.lock();
	int32_t nByte = write(m_liFD, rgBuffer, uliBufferSize);
	m_mutexWrite.unlock();

	liOperationResult = 0;
	if ( nByte == -1 )
	{
		liOperationResult = errno;
	}

	return nByte;
}


int32_t USBInterface::closeInterface()
{
	if ( isOpen() )
	{
		int32_t result = close(m_liFD);
		return result;
	}
	return -1;
}


bool USBInterface::setPortName(string sName)
{
	if ( sName.length() < 1 )
		return false;
	else
		m_strName.assign(sName);
	return true;
}


void USBInterface::setHardwareFlowControl(bool bHasRts)
{
	m_bHasRts = bHasRts;

	tcgetattr(m_liFD, &m_stty);
	if (bHasRts)
		m_stty.c_cflag |= CRTSCTS;
	else
		m_stty.c_cflag &= ~CRTSCTS;
	tcsetattr(m_liFD, TCSANOW, &m_stty);

}


void USBInterface::setSoftwareFlowControl(bool bHasXon)
{
	m_bHasXon = bHasXon;

	tcgetattr(m_liFD, &m_stty);
	if (bHasXon)
		m_stty.c_iflag |= IXON | IXOFF;
	else
		m_stty.c_iflag &= ~(IXON|IXOFF|IXANY);
	tcsetattr(m_liFD, TCSANOW, &m_stty);
}


void USBInterface::setParity(string strParity)
{
	m_strParity.assign(strParity);

	tcgetattr(m_liFD, &m_stty);
	m_stty.c_cflag &= ~(PARENB | PARODD);
	if ( strParity.at(0) == 'E' )
		m_stty.c_cflag |= PARENB;
	else if ( strParity.at(0) == 'O' )
		m_stty.c_cflag |= PARODD;
	tcsetattr(m_liFD, TCSANOW, &m_stty);
}


void USBInterface::setDataBits(int32_t liBits)
{
	m_cBits = liBits;

	tcgetattr(m_liFD, &m_stty);
	if (liBits == 7 && (m_strParity.at(0) == 'M' || m_strParity.at(0) == 'S'))
		liBits = 8;

	switch (liBits)
	{
		case 5:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS5;
			break;
		case 6:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS6;
			break;
		case 7:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS7;
			break;
		case 8:
		default:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS8;
			break;
	}
	tcsetattr(m_liFD, TCSANOW, &m_stty);
}


bool USBInterface::setParameters()
{
	tcgetattr(m_liFD, &m_stty);

	// Set data bits
	// We generate mark and space parity ourself.
	if (m_cBits == 7 && (m_strParity.at(0) == 'M' || m_strParity.at(0) == 'S'))
		m_cBits = 8;

	switch (m_cBits)
	{
		case 5:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS5;
			break;
		case 6:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS6;
			break;
		case 7:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS7;
			break;
		case 8:
		default:
			m_stty.c_cflag = (m_stty.c_cflag & ~CSIZE) | CS8;
			break;
	}

	// Set into raw, no echo mode
	m_stty.c_iflag = IGNBRK;
	m_stty.c_lflag = 0;
	m_stty.c_oflag = 0;
	m_stty.c_cflag |= CLOCAL | CREAD | HUPCL; // HUPCL messo ora
	m_stty.c_cc[VMIN] = 1;
	m_stty.c_cc[VTIME] = 5;

	// RTS/CTS or XOn/XOff control
	if (m_bHasRts)
		m_stty.c_cflag |= CRTSCTS;
	else
		m_stty.c_cflag &= ~CRTSCTS;

	if (m_bHasXon)
		m_stty.c_iflag |= IXON | IXOFF;
	else
		m_stty.c_iflag &= ~(IXON|IXOFF|IXANY);

	m_stty.c_cflag &= ~(PARENB | PARODD);
	if ( m_strParity.at(0) == 'E' )
		m_stty.c_cflag |= PARENB;
	else if ( m_strParity.at(0) == 'O' )
		m_stty.c_cflag |= PARODD;

	if (tcsetattr(m_liFD, TCSANOW, &m_stty) != 0)
	{
		return false;
	}

	return true;
}
