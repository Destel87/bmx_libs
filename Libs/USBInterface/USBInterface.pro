QT		-= core gui
TARGET		= USBInterface
TEMPLATE	= lib
CONFIG		+= staticlib

QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3
QMAKE_CXXFLAGS += -O0

# path inclusions
INCLUDEPATH += $$PWD/../../../

# internal libraries inclusion

unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../TimeStamp
DEPENDPATH += $$PWD/../TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../TimeStamp/libTimeStamp.a

LIBS += -lusb-1.0
LIBS += -ludev

SOURCES += \
    USBInterface.cpp \
    hid.c \
    HIDInterface.cpp

HEADERS += \
    USBInterface.h \
    hidapi.h \
    HIDInterface.h


