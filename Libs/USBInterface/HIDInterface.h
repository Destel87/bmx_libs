//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    HIDInterface.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the HIDInterface class.
//! @details
//!
//*!****************************************************************************

#ifndef HIDINTERFACE_H
#define HIDINTERFACE_H

#include <stdio.h>

#include "hidapi.h"
#include "Log.h"

#define FEATURE_REPORT_SIZE		9

class HIDInterface
{
	public:

		HIDInterface();

		virtual ~HIDInterface();

		bool init(int liVID, int liPID, Log* pLogger);

		bool printHIDInfo(void);

		int write(unsigned char* pBuff);

		int read(unsigned char *pBuff, int liMs);

		int sendFeatureReport(unsigned char *pBuff, int liSize = FEATURE_REPORT_SIZE);

		void closeHID(void);

    private:

        hid_device* m_pDevice;
        bool		m_bConfigOk;

    protected:

        Log*		m_pLogger;

};

#endif // HIDINTERFACE_H
