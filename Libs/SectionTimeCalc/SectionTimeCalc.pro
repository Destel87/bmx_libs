#-------------------------------------------------
#
# Project created by QtCreator 2020-01-17T19:32:30
#
#-------------------------------------------------

QT       -= core gui

TARGET = SectionTimeCalc
TEMPLATE = lib
CONFIG += staticlib

# internal libraries inclusion
INCLUDEPATH += $$PWD/../Log
INCLUDEPATH += $$PWD/../Thread

SOURCES += SectionTimeCalc.cpp \
    NV_Timecalc.cpp

HEADERS += SectionTimeCalc.h \
    TempiMotori.h \
    NV_Timecalc.h


unix {
    target.path = /usr/lib
    INSTALLS += target
}

