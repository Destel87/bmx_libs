/*********************************************************************************/
/*!  \file  NV_Timecalc.cpp
     \brief File per calcolo tempi

Program to calculate total run time and optical reading times of
protocols that run on the Vidas instrument. Times will be reported
in seconds from the start of the assay. These times will be used by
the scanner for scheduling optical readings to enhance the performance
of the Vidas instrument in running kinetic type assays.

File modificato per calcolare anche tempistica dei singoli atomi

        \author   Francesca Biagioni
        \version  1.6
        \date     4_Giu_2010
//////////////////////////////////////////////////////////////////		
//	MAD 01.02.000: 20170405 NefroCheck Project
//		IS7984 : fixed bug on loops prediction 
//	FCA 01.02.001: 20190109 : 
//		.set value TC_Old=0
//		.added check index of vectors/matrix in order 
//			to improve the safety access of storage memory
//		.added function HaltTaskTCalc in order to report the file and source code
//			of error
*/
/*********************************************************************************/

/*********************************************************************************/
/*                            INCLUDE                                            */
/*********************************************************************************/

#ifndef TRUE
#define TRUE  1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#include <stdlib.h>
#include <string.h>
#include "NV_Timecalc.h"
#include "TempiMotori.h"

#define REPORT_DBG 0 //debug report of commands

#if REPORT_DBG==1
#include "string.h"
#include <stdio.h>
#endif

static int TC_old = 0; // check of TimeCalcOld during loop (0:disable)

// Default structure requested for J calculation by TimeCalc
static STR_GlobalInfo  stGinf =
{
		0,                                // iiCa              NA
		0,                                // iiEsec            NA
		{                                 // piiRampatower[20] NA
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		},
		{                                 // piiRampapompa[20] NA
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		},
		{                                 // piiRampatray[20]  NA
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		},
		{ 0,0,0,0, },                     // piiVeltower[4]    NA
		{                                 // piiVelpompa[36]
				40,  80, 120, 160, 200, 240, 280, 320, 360, 360, 100, 100,
				100, 100, 100,  20, 100, 100, 100,  40, 100,  60,  80, 100,
				120, 140, 200, 100, 100, 100, 100, 100, 100, 100, 100, 100
		},
		{ 0,0,0,0 },                      // piiVeltray[4]     NA
		{ // K0   K1    K2    K3    K4    K5 piiPostower[10]
				0, 3050, 3250, 3730, 5794, 5794, 0, 0, 0, 0
		},
		{                                 // piiPospompa[40]
				0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
				10,  12,  14,  16,  18,  20,  25,  30,  35,  40,
				50,  60,  80, 100, 139, 185, 232, 278, 325, 371,
				417, 464, 510, 556, 603, 649, 300,   0,   0,   0
		},
		{                                // piiPostray[24]    NA
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		},
};


/*###############################################################################*/
/*###############################################################################*/
/*                            DECLARATION                                        */
/*###############################################################################*/
/*********************************************************************************/
/*                            DEFINE                                             */
/*********************************************************************************/
#define HALT_TASK_TCALC		0	//to halt task during tcalc
#define	REPORT_JGAP_TIME	0	//to report time jgap

#define CHECK_NRIGHEPOMPA	NRIGHEPOMPA
#define CHECK_NCOLPOMPA		36			//check dimension of matrix fg_llTmpompa
#define CHECK_NKTOWER		6			//movement from K0->K1->K2->K3->K4->K5
#define CHECK_LENPII		LENpiiPacc
#define CHECK_NSTEPRAMPA	NSTEPRAMPA
//////////////////
#define WELL_CUVETTE          10   // Tray fully out
//////////////////////////////////////
//1.3.16	Tray Macro	�X� VD8501 Immuno Assay Protocol Language
//< Pos >
//Pos.: 1 char between �0�-�9� or bigger than �A� with the number of the well to reach
#define MAX_POSTRAYWELL		  10	// max value of position tray to manage '0'-'9'-'A' as reported 
//////////////////////////////////////

#define table_cycles( a )   table_time((a))
/*********************************************************************************/
/*                            PRIVATE VARIABLE                                   */
/*********************************************************************************/
/*********************************************************************************/
/*! \struct selection_table

  Struttura contenente la corrispondenza lettera (0-9 e A-z) con la velocita'
  in Step/Sec
 */
/*********************************************************************************/
static struct
{
	char selector ;       //!< Carattere trovato in protocol
	//!<
	int  speed    ;       //!< Velocita' corrispondente a carattere
	//!<
	int time_count;       //!< Tempi in sec
	//!<
}
fg_selection_table[NUM_SELECTS] =
{
		{ '0',  40, 0 },   // 01  40
		{ '1',  80, 1 },   // 02  80
		{ '2', 120, 2 },   // 03 120
		{ '3', 160, 3 },   // 04 160
		{ '4', 200, 4 },   // 05 200
		{ '5', 240, 5 },   // 06 240
		{ '6', 280, 6 },   // 07 280
		{ '7', 320, 7 },   // 08 320
		{ '8', 360, 8 },   // 09 360
		{ '9', 360, 9 },   // 10 360 // was 400, changed to 360 16-Jul-91 DTM
		{ 'A', 100, 10 },  // 11 100
		{ 'B', 100, 12 },  // 12 100
		{ 'C', 100, 14 },  // 13 100
		{ 'D', 100, 16 },  // 14 100
		{ 'E', 100, 18 },  // 15 100
		{ 'F',  20, 20 },  // 16  20
		{ 'G', 100, 25 },  // 17 100
		{ 'H', 100, 30 },  // 18 100
		{ 'I', 100, 35 },  // 19 100
		{ 'J',  40, 40 },  // 20  40
		{ 'K', 100, 50 },  // 21 100
		{ 'L',  60, 60 },  // 22  60
		{ 'M',  80, 80 },  // 23  80
		{ 'N', 100, 100 }, // 24 100
		{ 'O', 120, 139 }, // 25 120
		{ 'P', 140, 185 }, // 26 140
		{ 'Q', 200, 232 }, // 27 200
		{ 'R', 100, 278 }, // 28 100
		{ 'S', 100, 325 }, // 29 100
		{ 'T', 100, 371 }, // 30 100
		{ 'U', 100, 417 }, // 31 100
		{ 'V', 100, 464 }, // 32 100
		{ 'W', 100, 510 }, // 33 100
		{ 'X', 100, 556 }, // 34 100
		{ 'Y', 100, 603 }, // 35 100
		{ 'Z', 100, 649 }  // 36 100
};

static STR_GlobalVar    *fg_pgstGvar;
static STR_GlobalInfo   *fg_pgstGinfo;

static          int     fg_tc_error = TC_CLOSE;
static const    char    *fg_tc_argptr;
static unsigned int     fg_gpuiTea[NMAXATOM];    // tempi effettivi degli atomi
static unsigned int     fg_gpuiTma[NMAXATOM];    // tempi misurati degli atomi
static          int     fg_giiComIO;             // Indice della tabella piiPacc[13]
												 // -1 ==> tempo non tabellato
static          int     fg_giiNj;                // Numero del comando J
static          int     fg_giiNa;                // N. di atomi presenti nell' attuale comando
static          int     fg_iiEsecU  = TRUE;              // True ==> Execute U command
static          long   fg_llCorpomp = 0L;       // millisecondi di correzione del tempo misurato della pompa

#ifdef DEBUG
int     giiInlp;              // Numero di loop di ripetizione degi atomi comandi M e W
int     giiAPozTray;          // Posizione attuale Tray
int     giiAk;                // Attuale posizione della torre
int     wellspace;            // L - reagent tray well interval
int     giiAStepTray;         // Posizione assoluta in Step del Tray
#else
static          int     fg_giiAk;                	// Attuale posizione della torre
static          int     fg_giiInlp;              	// Numero di loop di ripetizione degi atomi comandi M e W
static          int     fg_giiAPozTray;          // Posizione attuale Tray
static          int     fg_giiAStepTray;         	// Posizione assoluta in Step del Tray
static          int     fg_wellspace;            		// L - reagent tray well interval
#endif

/*********************************************************************************/
/*                            PRIVATE FUNCTION                                   */
/*********************************************************************************/
static int   tc_parse_num __ARGS   (( const char *, int));
static int   table_time __ARGS     (( const char * ));
static int   table_volume __ARGS   (( const char * ));
static int   table_speed __ARGS    (( const char * ));

static void  pump_move_time_mix    ( unsigned int );
static void  pump_move_time_wash   ( unsigned int );
static void  position_tower        ( unsigned int );
static int   calcrspeed            (int iiSpeed);
static int   calccvol              (int iiVol);
static void  incrNatom             (void);
static void  calctCmdIO            (void);
static int   calcchar              (const char *p);
static void  calcTime              (long llMsec);
static void  parse_it              (const char *p);
//static void  CalcTE                (int iiNstep, int iiVel, unsigned int *puiTt);
static void CalcTE(int iiNstep, float iiVel, unsigned int *puiTe);				// changed by bmx on 1st Feb 2011
#ifdef DEBUG
static void  movetray              (int iiNStepTray);
#endif

static void  LVacommand __ARGS(( void));
static void  LVbcommand __ARGS(( void));
static void  LVccommand __ARGS(( void));
static void  LVdcommand __ARGS(( void));
static void  LVecommand __ARGS(( void));
static void  LVfcommand __ARGS(( void));
static void  LVgcommand __ARGS(( void));
static void  LVhcommand __ARGS(( void));
static void  LVicommand __ARGS(( void));
static void  LVjcommand __ARGS(( void));
static void  LVkcommand __ARGS(( void));
static void  LVlcommand __ARGS(( void));
static void  LVmcommand __ARGS(( void));
static void  LVncommand __ARGS(( void));
static void  LVocommand __ARGS(( void));
static void  LVpcommand __ARGS(( void));
static void  LVqcommand __ARGS(( void));
static void  LVrcommand __ARGS(( void));
static void  LVscommand __ARGS(( void));
static void  LVtcommand __ARGS(( void));
static void  LVucommand __ARGS(( void));
static void  LVvcommand __ARGS(( void));
static void  LVwcommand __ARGS(( void));
static void  LVxcommand __ARGS(( void));
static void  LVycommand __ARGS(( void));
static void  LVzcommand __ARGS(( void));

static void HaltTaskTCalc(const char *_FILE,const int line,int error);

/*********************************************************************************/
/*                            PUBLIC FUNCTION                                    */
/*********************************************************************************/
/*********************************************************************************/
/*                            PUBLIC VARIABLE PER DEBUG                          */
/*********************************************************************************/
#ifdef DEBUG
int  giiNcom[NMAXDLP];                 // Numero comando (0...  25)
char gpccNcom[NMAXDLP][10];            // Nome del comando completo di parametri
char gpccNatom[NMAXDLP][NMAXATOM][10]; // Array di nomi degli atomi del comando
int  giiPerr;                          // Parametro dell' errore (e' una informazione
// aggiuntiva al codice dell' errore)
int  giiDlp;                           // N. di loop del comando D
int  giiNumComD;                       // N. di comando dentro il loop del comando D
#else
static int  fg_giiDlp;                    // N. di loop del comando D
static int  fg_giiNcom;                   // Numero comando (0...  25)
#endif
/*********************************************************************************/
/*                            PRIVATE CONST                                      */
/*********************************************************************************/
CommandTable  fg_cmd_table[NUM_CMDS] =
{
		/* A */   { LVacommand, 0, 1 },
		/* B */   { LVbcommand, 4, 1 },
		/* C */   { LVccommand, 0, 1 },
		/* D */   { LVdcommand, 1, 1 },
		/* E */   { LVecommand, 0, 1 },
		/* F */   { LVfcommand, 0, 1 },
		/* G */   { LVgcommand, 1, 3 },
		/* H */   { LVhcommand, 1, 1 },
		/* I */   { LVicommand, 0, 1 },
		/* J */   { LVjcommand, 1, 1 },
		/* K */   { LVkcommand, 1, 1 },
		/* L */   { LVlcommand, 3, 1 },
		/* M */   { LVmcommand, 2, 4 },
		/* N */   { LVncommand, 0, 1 },
		/* O */   { LVocommand, 0, 1 },
		/* P */   { LVpcommand, 1, 2 },
		/* Q */   { LVqcommand, 1, 1 },
		/* R */   { LVrcommand, 0, 6 },
		/* S */   { LVscommand, 1, 1 },
		/* T */   { LVtcommand, 1, 1 },
		/* U */   { LVucommand, 0, 4 },
		/* V */   { LVvcommand, 1, 1 },
		/* W */   { LVwcommand, 2, 4 },
		/* X */   { LVxcommand, 1, 1 },
		/* Y */   { LVycommand, 1, 1 },
		/* Z */   { LVzcommand, 1, 1 }
};

/*###############################################################################*/


/*###############################################################################*/
/*********************************************************************************/
/*                               FUNCTION CODE                                   */
/*********************************************************************************/
/********************************************************************************/
/* HaltTaskTCalc: function used to report an erro on line file in according		*/
/*			 with values passed as parameters									*/
/*	Parameters: 																*/
/*		_FILE	: pointer to const char contains the file name of source code	*/
/*		line	: storage variable contains the line number of source file		*/
/*		error	: error code to report											*/
/********************************************************************************/
static void HaltTaskTCalc(const char *_FILE,const int line,int error)
{
	int idy, idx;

	idx = 0;
	fg_tc_error = error;
	#if HALT_TASK_TCALC==1
	  printf("HaltTask : %d - %s \n",line,_FILE);
      while (1)
	  {
		if (idx == 1)
		{
			break;
		}
		for (idy = 0; idy < 100000; idy++);
	  }
	#endif
}


/*! \fn    err = TimeCalcJ_old(const char *Locals, unsigned  int *Jgap)
     \brief Calcola il tempo fra due J con precedente algoritmo
 */
/*********************************************************************************/
int TimeCalcJ_old(char *Locals, unsigned  int *Jgap)
{
	int          iiJ;
//	int          iiI;
	int          iiJJ;
	int          iiInd;
	int          iiAj;                 // Ultimo J sul quale e' stato eseguito il calcolo
	STR_Atomi    stAtmTcj;
	unsigned int uiTmjgap;             // tempo misurato fra due J
	unsigned int uiTmdlp;              // tempo misurato all' interno di un loop
	char         *pccLc;

	pccLc = Locals;
	if (fg_tc_error != TC_OPEN)
	{
		fg_tc_error = TC_ERR_NOOPEN;
		return(fg_tc_error);
	}
	// Variabili inizializzate solo se siamo in debug
#ifdef DEBUG
	for(iiJ = 0; iiJ < NMAXJGAP; ++iiJ)
	{
		Jgap[iiJ] = 0;
	}
	giiNumComD = 1;
#endif
//	iiI       = 0;
	iiInd     = 1;
	uiTmjgap  = 0;
	uiTmdlp   = 0;
	iiAj      = -1;
	while(1)
	{
#if REPORT_DBG==1
		char c = *(pccLc + iiInd);
#endif
		TimeCommand (pccLc + iiInd, &stAtmTcj);
		if (fg_tc_error)
			break;
#ifdef DEBUG
		if (giiNcom[0] == ('J' - 'A'))
#else
		if (fg_giiNcom == ('J' - 'A'))
#endif
		{
			// e' arrivato un J verifichiamo il numero e aggiorniamo l'array Jgap
			// azzera i J non presenti
			for(iiJ = iiAj; iiJ < fg_giiNj; ++iiJ)
			{
				if (iiJ < 0)
					continue;
				Jgap[iiJ] = 0;
			}
			// aggiorna il J selezionato
			Jgap[iiJ] = uiTmjgap + uiTmdlp;
			
			#if REPORT_DBG==1
				printf("%d: [%d]%c%d (%d)\n", (int)uiTmjgap,  (int)iiInd, c, (int)iiJ, (int)uiTmdlp);
			#endif
			iiAj     = fg_giiNj + 1;
			uiTmjgap = 0;
			uiTmdlp  = 0;
			// con comando J dentro un loop ci allarmiamo
			if (fg_giiDlp)
			{
				#ifdef DEBUG
					giiPerr  = giiDlp;
				#endif
				fg_tc_error = TC_ERR_DNELJ;
			}
		}
		else
		{
			#if REPORT_DBG==1
				printf("%d: [%d]%c = {", (int)uiTmjgap,  (int)iiInd, c);
			#endif
					
			for (iiJJ = 0; iiJJ < fg_giiInlp; ++iiJJ)
			{
				for (iiJ = 0; iiJ < stAtmTcj.iiNa; ++iiJ)
				{
					if (fg_giiDlp)
						uiTmdlp += (stAtmTcj.puiTma[iiJ] * (unsigned int)(fg_giiDlp));
					else
					{
						#if REPORT_DBG==1
							printf(" %d", (int)stAtmTcj.puiTma[iiJ]);
						#endif
						uiTmjgap += (stAtmTcj.puiTma[iiJ] + uiTmdlp);
						uiTmdlp   = 0;
					}
				}
			}
			#if REPORT_DBG==1
				printf("} (%d)\n", (int)uiTmdlp);
			#endif
		}
		iiInd += stAtmTcj.iiNarg;

		#if REPORT_DBG==1
			fflush(stdout);
		#endif
	}
	return(fg_tc_error);
}


/*********************************************************************************/
/*! \fn    TimeVer()
     \brief Versione del modulo NV_Timecalc
 */
/*********************************************************************************/
void TimeVer(char * versione)
{
	strcpy(versione, VERTIMECALC);
}

/*********************************************************************************/
/*! \fn     err = TimeClose()
     \brief Chiusura del modulo di calcolo
 */
/*********************************************************************************/
void TimeClose()
{
	fg_tc_error = TC_CLOSE;
}

/*********************************************************************************/
/*! \fn    iiErr = TimeOpen (STR_GlobalVar *strGlobalVars,
                              STR_GlobalInfo *strGloInfo, char *gbls)
     \brief Inizzializzazione variabili interne al modulo

     \iiErr     TC_ERR_OPEN     Errore apertura file
                TC_OPEN         OK
 */
/*********************************************************************************/
int TimeOpen (STR_GlobalVar *strGlobalVars /*, STR_GlobalInfo *strGloInfo*/ , char *gbls)
{
	// solo se eravamo chiusi accettiamo la open altrimenti il programma
	// chiamante dovra' prima fare la close
	if (fg_tc_error == TC_CLOSE)
	{
		fg_pgstGvar      = strGlobalVars;
		fg_pgstGinfo     = &stGinf;	// strGloInfo;
		fg_tc_error  = TC_OPEN;
		if (gbls != NULL)
		{
			//Valori di default dei comandi globals
			fg_pgstGvar->iiPumpWashSpeed  = 80;  // comando H wash speed
			fg_pgstGvar->iiPumpWashVolume = 0;   // comando Q Wash volume
			fg_pgstGvar->iiPumpMixSpeed   = 80;  // comando S Mix speed
			fg_pgstGvar->iiPumpMixVolume  = 0;   // comando V Mix volume
			fg_wellspace                  = 66;  // Comando L Tray size
			// __Controllo limitatori di stringa
			if(  ( *gbls++ != '*' ) || ( gbls[strlen(gbls)-1] != '*' )  )
				return(fg_tc_error = TC_ERR_GLOBAL_DELIM );
			//__Analisi della stringa protocollo
			if(fg_tc_error == TC_OPEN)
				parse_it(gbls);
		}
		// Inizializzazione variabili interne
		fg_giiNa         = 0;
		fg_giiAk         = 1;   // Al reset torre in K1
		fg_giiAPozTray   = WELL_CUVETTE;
		fg_giiAStepTray  = -5;
		fg_giiK5         = 2544;
		fg_giiDlp        = 0;   // Non siamo in loop
	}
	else
	{
		fg_tc_error = TC_ERR_OPEN;
	}
	return(fg_tc_error);
}

/*********************************************************************************/
/*! \fn    int  TimeCommand (char *p, unsigned int *puiTta, unsigned int *puiTma, int *iiNccom, int *iiNlp);
     \brief Seziona un comando in atomi

Riempe l' array iiTimeAtoms, con la durata dei singoli atomi che compongono
un comando.

  \arg    pccCommand    = Puntatore alla stringa del comando
  \arg    puiTea        = Durata effettiva di ogni singolo atomo in msec
  \arg    puiTma        = Durata misurata di ogni singolo atomo in msec
  \arg    iiNc          = Lunghezza del comando
                      = 0 ==> Comando terminato
  int  giiNa;                          // Numero di atomi generati dal comando

Dalla esecuzione di TimeCommand sono aggiornate le seguenti variabili globali
presenti solo nella fase di debug:
  char gpccNcom[10];                   // Nome del comando completo di parametri
  int  giiNcom;                        // Numero comando (0...  25)
  char gpccNatom[NMAXATOM][10];        // Array di nomi degli atomi del comando
  int  giiPerr;                        // Parametro dell' errore (e' una informazione
\arg    *iiNlp        = N. di loop di atomi nel comando

     \return  Codice di errore (1 - nnnn)
              == TC_OPEN ==> Tutto ok
              == -1      ==> Finita stringa comandi
 */
/*********************************************************************************/
int  TimeCommand(char *p, STR_Atomi *pstrAtm)
{
	char cmd;
	int i, j;

	fg_giiNa         = 0;
	pstrAtm->iiNa = 0;
	cmd       = *p;
	if((cmd == '*') || (cmd == '&') || (cmd == 0))
	{
		fg_tc_error = TC_PRTEND;
		return(fg_tc_error);
	}
	cmd = *p;
	p++;        // Point at argument (if any)
	fg_tc_argptr = p;
	i = cmd - 'A';    // __Copia prima lettera del protocollo in i
	//   e controlla sia un carattere accettabile
	if((i < 0) || (i >= NUM_CMDS))
	{
		// Invalid command letter (not A-Z)
		fg_tc_error = TC_ERR_BAD_CMD;
		return(fg_tc_error);
	}
	#ifdef DEBUG
		gpccNcom[giiNumComD - 1][0] = cmd;
		gpccNcom[giiNumComD - 1][1] = '\0';
		gpccNcom[giiNumComD - 1][2] = '\0';
		gpccNcom[giiNumComD - 1][3] = '\0';
		gpccNcom[giiNumComD - 1][4] = '\0';
		gpccNcom[giiNumComD - 1][5] = '\0';
		strncpy(&gpccNcom[giiNumComD - 1][1], tc_argptr, cmd_table[i].argcount);    // Copia in argbuf il numero di caratteri fissato
		// da argconut prendendoli da stringa protocollo
		giiNcom[giiNumComD - 1] = i;
	#else
		fg_giiNcom = i;
	#endif
	fg_giiInlp = 1;
	(fg_cmd_table[i].func)();
	for (j = 0; j < NMAXATOM; j++)
	{
		pstrAtm->puiTea[j] = fg_gpuiTea[j];
		pstrAtm->puiTma[j] = fg_gpuiTma[j];
	}
	pstrAtm->iiNa   = fg_giiNa;
	pstrAtm->iiNarg = (int)(fg_cmd_table[i].argcount) + 1;
	return(fg_tc_error);
}

/*********************************************************************************/
/*! \fn    static void parse_it ( const char *p )
     \brief Breve descrizione

Controlla ogni carattere della stringa:
\arg    se e' SPAZIO (isspace()) lo salta
\arg    se non e' un carattere tra A e Z da' ERR_BAD_CMD  \n

Poi scrive in 'i' il carattere del comando e cerca la funzione nella struttura
dei comandi (cmd_table[i].func)().

     \param     p        puntatore alla stringa di prtocollo
     \return    descrizione del valore restituito dalla funzione

 */
/*********************************************************************************/
static void parse_it(const char *p)
{
	char cmd, argbuf[8];
	int i;

	for(;;)
	{
		cmd = *p;

		if(  ( cmd == '*' ) || ( cmd == '&' )  )
		{
			// The delimiter.
			return;
		}

		p++;        // Point at argument (if any)
		fg_tc_argptr = p;

		i = cmd - 'A';    // __Copia prima lettera del protocollo in i
		//   e controlla sia un carattere accettabile
		if(  ( i < 0 ) || ( i >= NUM_CMDS )  )
		{
			// Invalid command letter (not A-Z)
			fg_tc_error = TC_ERR_BAD_CMD;
			return;
		}

		argbuf[0] = cmd;
		argbuf[1] = '\0';
		argbuf[2] = '\0';
		argbuf[3] = '\0';
		argbuf[4] = '\0';
		argbuf[5] = '\0';
		strncpy( &argbuf[1], fg_tc_argptr, fg_cmd_table[i].argcount );    // __Copia in argbuf il numero di caratteri fissato
		//   da argconut prendendoli da stringa protocollo
		// If we get here, we have a valid index.  Vector to the
		// routine which does the actual work.
		fg_giiNa   = 0;
		#ifdef DEBUG
			giiPerr = 0;
		#endif
		(fg_cmd_table[i].func)();
		p += fg_cmd_table[i].argcount;
		if( fg_tc_error != TC_OPEN )
		{
			// An error occured
			return;
		}
	}
}


/*********************************************************************************/
/*! \fn    static int tc_parse_num( const char *p, int count )
     \brief Breve descrizione

Prende il numero di caratteri pari a 'count' dalla stringa di protocollo 'p'
e li coverte in un valore intero.

     \param     p         puntatore a stringa protocollo rimanente
     \param     count     numero di elementi da prendere
     \return    Numero intero

 */
/*********************************************************************************/
static int tc_parse_num( const char *p, int count )
{
	char buf[16];
	

	strncpy( buf, p, (size_t)count );
	buf[count] = '\0';

	return(  atoi( buf )  );
}


/*-------------------------------------------------------------------*/
/*              Tabella per calcolo di acc del motore                */
/*-------------------------------------------------------------------*/
#define RAMP_COUNT    12

/*********************************************************************************/
/*! \fn    static float position_tower( unsigned int k )
     \brief Sposta Torre

Esegue lo spostamento della torre alla posizione indicata in kloc[k] se
la sua posizione attuale (towerlocation) e' diversa.

     \param     k   indice delle posizioni note di kloc
     \return    Tempo in decimi di secondo

 */
/*********************************************************************************/
static void position_tower( unsigned int k )
{
#ifdef DEBUG
	int iiStpold;
	int iiStpnew;
	static const char pccNmk[6][9] =
	{
			"TOWER_K0",
			"TOWER_K1",
			"TOWER_K2",
			"TOWER_K3",
			"TOWER_K4",
			"TOWER_K5"
	};
#endif

	if (k > 5)
	{
		fg_tc_error = TC_ERR_PTOWER;
		return;
	}
	//improvement of checking index memory
	if (fg_giiNa>=NMAXATOM)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NAMAX);
		return;
	}
	if (fg_giiAk>=CHECK_NKTOWER)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_PTOWER);
		return;
	}
	if (k>=CHECK_NKTOWER)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_PTOWER);
		return;
	}
	/////////////////
	// sistema gli atomi
	if (fg_giiAk != k)
	{
		fg_gpuiTea[fg_giiNa] = fg_llTetower[fg_giiAk][k];
		fg_gpuiTma[fg_giiNa] = fg_llTmtower[fg_giiAk][k];
#ifdef DEBUG
		strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TW);
		if (giiAk < 5)
			iiStpold = piiKn[giiAk];
		else
			iiStpold = giiK5;
		if (k < 5)
			iiStpnew = piiKn[k];
		else
			iiStpnew = giiK5;
		gpstExta->piiRelstp[giiNa]  = iiStpnew - iiStpold;
		strcpy(gpstExta->pccNatm[giiNa], pccNmk[k]);
		gpstExta->piiTypecom[giiNa] = 1;
		if (giiAk < k)
			gpstExta->pffVela[giiNa] = VELTOWERUPE;
		else
			gpstExta->pffVela[giiNa] = VELTOWERDOWNE;
#endif
		incrNatom();
		fg_giiAk = k;
	}
	/*#ifndef DEBUG
	else
	{
		gpuiTea[giiNa] = 0;
		gpuiTma[giiNa] = 0;
		incrNatom();
	}
	#endif*/
}

/*********************************************************************************/
/*! \fn    pump_move_time_mix( unsigned int steps )
     \brief Tempo Movimento POMPA

Calcola il tempo che la pompa impiega a Fill/Put Volume indicato

     \param     steps   Volume di liquido da muovere, se 0 prende il valore globale
     \return    Time to fill with selected volume

 */
/*********************************************************************************/
static void pump_move_time_mix(unsigned int steps)
{
	int iiCol, iiRig;

	iiRig   = calcrspeed(fg_pgstGvar->iiPumpMixSpeed);
	if (steps)
		iiCol = calccvol(steps);
	else
		iiCol = calccvol(fg_pgstGvar->iiPumpMixVolume);
	///////////////////////////////////////
	//improvement of checking index memory
	if (iiRig>=CHECK_NRIGHEPOMPA)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_RPUMP);
		return;
	}
	if (iiCol>=CHECK_NCOLPOMPA)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_CPUMP);
		return;
	}
	if (fg_giiNa>=NMAXATOM)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NAMAX);
		return;
	}
	////////////////
	fg_gpuiTma[fg_giiNa] = fg_llTmpompa[iiRig][iiCol] + fg_llCorpomp;
	fg_llCorpomp      = 0L;
	fg_gpuiTea[fg_giiNa] = fg_llTepompa[iiRig][iiCol];
#ifdef DEBUG
	strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_PM);
#endif
	incrNatom();
}

/*********************************************************************************/
/*! \fn    pump_move_time_wash( unsigned int steps )
     \brief Tempo Movimento POMPA

Calcola il tempo che la pompa impiega a Fill/Put Volume indicato

     \param     steps   Volume di liquido da muovere, se 0 prende il valore globale
     \return    Time to fill with selected volume

 */
/*********************************************************************************/
static void pump_move_time_wash(unsigned int steps)
{
	int iiCol, iiRig;

	iiRig   = calcrspeed(fg_pgstGvar->iiPumpWashSpeed);
	if (steps)
		iiCol = calccvol(steps);
	else
		iiCol = calccvol(fg_pgstGvar->iiPumpWashVolume);

	///////////////////////////////////////
	//improvement of checking index memory
	if (iiRig>=CHECK_NRIGHEPOMPA)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_RPUMP);
		return;
	}
	if (iiCol >= CHECK_NCOLPOMPA)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_CPUMP);
		return;
	}
	////////////////
	fg_gpuiTma[fg_giiNa] = fg_llTmpompa[iiRig][iiCol] + fg_llCorpomp;
	fg_llCorpomp      = 0L;
	fg_gpuiTea[fg_giiNa] = fg_llTepompa[iiRig][iiCol];
#ifdef DEBUG
	strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_PM);
#endif
	incrNatom();
}

/*********************************************************************************/
/*! \fn    static int table_time( const char *p )
     \brief Associa valore da S_TABLE

Returns the time value in the table array for the given letter.

     \param     p     puntatore a stringa protocollo
     \return    Tempo associato a quella velocita'

 */
/*********************************************************************************/
static int table_time( const char *p )
{
	unsigned int i;
	char c;


	c = *p;
	// lasciare la struttura attuale
	for(i = 0; i < NUM_SELECTS; i++)
	{
		if(c == fg_selection_table[i].selector)
		{
			return( fg_selection_table[i].time_count );
		}
	}

	// Selector not found
	fg_tc_error = TC_ERR_TIME_ARG;
	return( 0 );
}

/*********************************************************************************/
/*! \fn    static int table_volume( const char *p )
     \brief Ritorna il volume della pompa

 */
/*********************************************************************************/
static int table_volume(const char *p)
{
	unsigned int i;
	char c;

	c = *p;
	// lasciare la struttura attuale
	for( i=0; i<NUM_SELECTS; i++ )
	{
		if( c == fg_selection_table[i].selector )
		{
			return(*(fg_pgstGinfo->piiPospompa + i));
		}
	}

	// Selector not found
	fg_tc_error = TC_ERR_TIME_ARG;
	return( 0 );
}

/*********************************************************************************/
/*! \fn    calcchar( const char *p )
     \brief Ritorna il numero (0 - 35) relativo alla carattere

 */
/*********************************************************************************/
static int calcchar( const char *p )
{
	unsigned int i;
	char c;

	c = *p;
	for(i = 0; i < NUM_SELECTS; i++)
	{
		if(c == fg_selection_table[i].selector)
		{
			return(i);
		}
	}

	// Selector not found
	fg_tc_error = TC_ERR_TIME_ARG;
	return( 0 );
}

//#define table_volume( a )   table_time((a))    // The same table is used for time as well as volume.


/*********************************************************************************/
/*! \fn    static int table_speed( const char *p )
     \brief Breve descrizione

Returns the speed value in the table array for the given letter.

     \param     p     puntatore a stringa protocollo
     \return    Velocita' in step/sec

 */
/*********************************************************************************/
static int table_speed( const char *p )
{
	unsigned int i;
	char c;


	c = *p;

	// mettere struttura da file .ini
	for( i=0; i<NUM_SELECTS; i++ )
	{
		if( c == fg_selection_table[i].selector )
		{
			return(fg_pgstGinfo->piiVelpompa[i]);

		}
	}

	// Selector not found
	fg_tc_error = TC_ERR_SPEED_ARG;
	return( 0 );
}


/*********************************************************************************/
/*! \fn    int calcrspeed (int iiSpeed)
     \brief No Atomi in comando

Calcola la riga della velocit� per accedere alla matrice dei tempi
 */
/*********************************************************************************/
static int calcrspeed (int iiSpeed)
{
	int  iiRiga;

	for(iiRiga = 0; iiRiga < NRIGHEPOMPA; iiRiga++)
	{
		if (fg_piiAspeed[iiRiga] == iiSpeed)
			break;
	}
	if (iiRiga > (NRIGHEPOMPA - 1))
	{
		fg_tc_error = TC_ERR_SPEED;
		iiRiga   = 0;
	}
#ifdef DEBUG
	gpstExta->pffVela[giiNa] = pffVe[iiRiga];
#endif
	return(iiRiga);
}

/*********************************************************************************/
/*! \fn    calctCmdIO()
     \brief Calocola i tempi dei comandi I e O. Si esegue il calcolo se non abbiamo
            un tempo tabellato.
 */
/*********************************************************************************/
static void calctCmdIO()
{
	static float ffTe;
	static float ffTm;
  	///////////////////////////////////////
	//improvement of checking index memory
	if (fg_giiNa >= NMAXATOM)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NAMAX);
		return;
	}
	////////////////
	ffTe = 0.0;
	// si effettua il calcolo
	if (fg_giiComIO < 0)
	{ // Calcola il tempo teorico
		CalcTE(fg_wellspace, VELTRAYE, &fg_gpuiTea[fg_giiNa]);
		// Calcola il tempo misurato utilizzando la retta di correlazione
		ffTe = (float)(fg_gpuiTea[fg_giiNa]);
		ffTm = ffTe * fg_ffMtray + fg_ffNtray;
		fg_gpuiTma[fg_giiNa] = (unsigned int)(ffTm);
	}
	// per le misure che abbiamo si legge i tempi da tabella
	else
  	{
		///////////////////////////////////////
		//improvement of checking index memory
		if (fg_giiComIO>=CHECK_LENPII)//13)
		{
		  HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_TIME_ARG);
		  return;
		}
		/////////
		fg_gpuiTma[fg_giiNa] = fg_llTmComIO[fg_giiComIO];
		fg_gpuiTea[fg_giiNa] = fg_llTeComIO[fg_giiComIO];
	}
	///////////
#ifdef DEBUG
	strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TR);
#endif
	incrNatom();
}

/*********************************************************************************/
/*! \fn    CalcTE()
     \brief Calocola il tempo effettivo.
     Tempo teorico =  La somma dei tempi effettivi della durata di ogni impulso di clock
                      inviato ai driver dei motori, non aggiungiamo nessun ritardo
                      sia all' inizio che alla fine di ogni impulso.
     N.B Per velocita' superiori a quella massima VELMAXRAMP l'incubator non effettua
         la rampa (baco nel FW in assembler che noi replichiamo)
 */
/*********************************************************************************/
static void CalcTE(int iiNstep, float iiVel, unsigned int *puiTe)
{
	int   iiNsrampa;    // N. di step da eseguire nella rampa (parte da 1)
	int   iiNsvmax;     // N. step da eseguire alla velocit� massima (parte da 1)
	int   iiI;
	static float ffTe;         // Tempo effettivo

	iiNsvmax  = iiNstep;
	#ifdef DEBUG
	if (iiNsvmax<0)
	{
		printf("CalcTE :iiNsvmax [%d]\n",iiNsvmax);
	}
	#endif
	////////////////////
	iiNsrampa = 0;
	if (iiVel <= VELMAXRAMP)  // per simulare baco nel FW in assembler
	{
		for (iiI = 0; iiI < NSTEPRAMPA; iiI++)
		{
			if (fg_stRinc.iiVel[iiI] > iiVel)
				break;
			iiNsvmax--;
			iiNsrampa++;
			if ((iiI + 1) >= iiNstep)
				break;
		}
	}
	// tempo teorico = tempo di rampa + iiNsvmax volte il tempo alla massima velocita'
	ffTe = 0.0;
	if (iiNsrampa)
  	{
		///////////////////////////////////////
		//improvement of checking index memory
	    if (iiNsrampa>=CHECK_NSTEPRAMPA)
		{
		  HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_IDXRAMP);
		  return;
		}
		///////////
	   	ffTe = fg_stRinc.ffMsec[iiNsrampa - 1];
	}
	// ffTe  += ((float)(iiNsvmax) * 1000.0) / (float)(iiVel);
	ffTe  += ((float)(iiNsvmax) * 1000.0) / iiVel;					// changed by bmx on 1st Feb 2011
	*puiTe = (unsigned int)(ffTe);
}

/*********************************************************************************/
/*! \fn    int calccvol (int iiVol)
     \brief No Atomi in comando

Calcola la colonna dei volumi per accedere alla matrice dei tempi
 */
/*********************************************************************************/
static int calccvol (int iiVol)
{
	int  iiCol;

	for(iiCol = 0; iiCol < 36; iiCol++)
	{
		if (fg_piiAvol[iiCol] == iiVol)
			break;
	}
	if (iiCol > 35)
	{
		fg_tc_error = TC_ERR_VOL;
		iiCol   = 0;
	}
	return(iiCol);
}

/*********************************************************************************/
/*! \fn    calcTime(long llMsec)
     \brief Inserisce l'atomo di attesa

 */
/*********************************************************************************/
static void calcTime(long llMsec)
{
  //improvement of checking index memory
  if (fg_giiNa >= NMAXATOM)
  {
	 HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NAMAX);
	 return;
  }
  //////////
	fg_gpuiTma[fg_giiNa] = llMsec;
	fg_gpuiTea[fg_giiNa] = llMsec;
#ifdef DEBUG
	strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_T);
	strcpy(gpstExta->pccNatm[giiNa], "WAIT");
	gpstExta->piiRelstp[giiNa]  = llMsec;
	gpstExta->piiTypecom[giiNa] = 4;
#endif
	// effettua la correzione del tempo di movimento della pompa che verr� eseguito
	// dopo l' attesa. Se il tempo di attesa e' inferiore a 3 secondi la pompa sara'
	// piu' veloce di 1 msec, altrimenti e' piu' veloce di 2 msec
	if (llMsec < 3000L)
		fg_llCorpomp = -1L;
	else
		fg_llCorpomp = -2L;

	incrNatom();
}

/*********************************************************************************/
/*! \fn    static incrNatom()
     \brief Incrementa il numero di atomi

Incrementa l' indice del untatore al numero di atomo e setta l' errore se abbiamo
superato il massimo.
 */
/*********************************************************************************/
static void incrNatom()
{
	++fg_giiNa;
	if (fg_giiNa > (NMAXATOM - 1))
	{
		fg_giiNa    = (NMAXATOM - 1);
		fg_tc_error = TC_ERR_NAMAX;
	}
}

/*-------------------------------------------------------------------------------*/
/*-----------------------------****************----------------------------------*/
/*-----------------------------*** COMMANDS ***----------------------------------*/
/*-----------------------------****************----------------------------------*/
/*-------------------------------------------------------------------------------*/

/*********************************************************************************/
/*! \fn    static void LVacommand __ARGS(( void))
     \brief Definisce tipo di Analisi

Questo comando non ha effetti sull'esecuzione del protocollo, ne' atomi o
tempi.
 */
/*********************************************************************************/
static void LVacommand __ARGS(( void))
{
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
	fg_tc_error = TC_ERR_ACOM;
}

/*********************************************************************************/
/*! \fn    static void LVbcommand __ARGS(( void))
     \brief Definisce posizione del WellBottom
            Con questo comando si setta la posizione di K5.
            Utilizzando i parametri M e N delle due rette di correlazione si
            aggiorna l' ultima riga e l'ultima colonna della matrice llTmtower.
            Effettuando un calcolo teorico si aggiorna l' ultima riga e l'ultima
            colonna della matrice llTttower.
 */
/*********************************************************************************/
static void LVbcommand __ARGS(( void))
{
	int iiI;
	unsigned int uiTe;
	int iiNstp;
	int iiVelKnK5;
	int iiVelK5Kn;
	static float ffMK5Kn;
	static float ffNK5Kn;
	static float ffMKnK5;
	static float ffNKnK5;

#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
	fg_giiK5 = tc_parse_num(fg_tc_argptr, 4); // Leggi nuova posizione di K5
	// aggiorna la riga e colonna delle matrice
	for(iiI = 0; iiI < 5; iiI++)
	{
		if (fg_giiK5 < fg_piiKn[iiI])
		{
			iiVelK5Kn = VELTOWERDOWNE;
			ffMK5Kn   = fg_ffMtowerdwn;
			ffNK5Kn   = fg_ffNtowerdwn;
			iiVelKnK5 = VELTOWERUPE;
			ffMKnK5   = fg_ffMtowerup;
			ffNKnK5   = fg_ffNtowerup;
			iiNstp    = fg_piiKn[iiI] - fg_giiK5;
		}
		else
		{
			iiVelK5Kn = VELTOWERUPE;
			ffMK5Kn   = fg_ffMtowerup;
			ffNK5Kn   = fg_ffNtowerup;
			iiVelKnK5 = VELTOWERDOWNE;
			ffMKnK5   = fg_ffMtowerdwn;
			ffNKnK5   = fg_ffNtowerdwn;
			iiNstp    = fg_giiK5 - fg_piiKn[iiI];
		}
		CalcTE(iiNstp, iiVelK5Kn, &uiTe);
		fg_llTetower[5][iiI] = (long)(uiTe);
		fg_llTmtower[5][iiI] = (long)(ffMK5Kn * (float)(uiTe) + ffNK5Kn);
		CalcTE(iiNstp, iiVelKnK5, &uiTe);
		fg_llTetower[iiI][5] = (long)(uiTe);
		fg_llTmtower[iiI][5] = (long)(ffMKnK5 * (float)(uiTe) + ffNKnK5);
	}
}

/*********************************************************************************/
/*! \fn    static void LVccommand __ARGS(( void))
     \brief Definisce numero di Cicli

Questo comando non ha effetti sull'esecuzione del protocollo, ne' atomi o
tempi.
 */
/*********************************************************************************/
static void LVccommand __ARGS(( void))
{
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
	fg_tc_error = TC_ERR_CCOM;
}


/*********************************************************************************/
/*! \fn    static void dcommand __ARGS(( void))
     \brief Setta Ripetizioni di Comando successivo

Comando che non ha atomi ma che influisce su comando successivio.. GUARDA!!!
 */
/*********************************************************************************/
static void LVdcommand __ARGS(( void))
{
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
	if (fg_giiDlp)
		fg_tc_error = TC_ERR_DCOM;
	fg_giiDlp = table_cycles(fg_tc_argptr);
}

/*********************************************************************************/
/*! \fn    static void ecommand __ARGS(( void))
     \brief Espelli Liquido

Esegue 1 atomo:
\arg 1. La POMPA espelle quanto scritto in V (var globale per MixVolum)
 */
/*********************************************************************************/
static void LVecommand __ARGS(( void))
{
#ifdef DEBUG
	giiRelSpr = -(pgstGvar->iiPumpMixVolume);
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_EMPTY");
	gpstExta->piiRelstp[giiNa]  = giiRelSpr;
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	pump_move_time_mix((unsigned int)(fg_pgstGvar->iiPumpMixVolume));
}


/*********************************************************************************/
/*! \fn    static void fcommand __ARGS(( void))
     \brief Aspira Liquido

Esegue 1 atomo:
\arg 1. La POMPA aspira quanto scritto in V (var globale per MixVolum)
 */
/*********************************************************************************/
static void LVfcommand __ARGS(( void))
{
#ifdef DEBUG
	giiRelSpr = (pgstGvar->iiPumpMixVolume);
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_FILL");
	gpstExta->piiRelstp[giiNa]  = giiRelSpr;
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	pump_move_time_mix( (unsigned int)(fg_pgstGvar->iiPumpMixVolume));
}

/*********************************************************************************/
/*! \fn    static void gcommand __ARGS(( void))
     \brief Scambia Volume di liquido

Esegue 3 atomi:
\arg 1. Muove la TORRE in posizione 4
\arg 2. Aspira dalla POMPA di quanto segnalato in protocollo
\arg 3. Muove la TORRE in posizione 1
 */
/*********************************************************************************/
static void LVgcommand __ARGS(( void))
{
	unsigned int uiApp;

	uiApp = (unsigned int)table_volume(fg_tc_argptr);
	//__Time to move to k4
	position_tower(4);
	//__Pump movement time
#ifdef DEBUG
	giiRelSpr = (int)(uiApp);
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_FILL");
	gpstExta->piiRelstp[giiNa]  = giiRelSpr;
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	pump_move_time_mix(uiApp);
	position_tower(1);
}


/*********************************************************************************/
/*! \fn    static void LVhcommand __ARGS(( void))
     \brief Definisce velocita' della pompa per WASH

Questo comando non ha effetti sull'esecuzione del protocollo, ne' atomi o
tempi.
 */
/*********************************************************************************/
static void LVhcommand __ARGS(( void))
{
	fg_pgstGvar->iiPumpWashSpeed = table_speed( fg_tc_argptr );
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
}


/*********************************************************************************/
/*! \fn    static void icommand __ARGS(( void))
     \brief TRAY IN
 */
/*********************************************************************************/
#ifdef TIMECALC_BEFORE20111108
static void LVicommand __ARGS(( void))
{
#ifdef DEBUG
	//  giiAbsStep[1] -= wellspace;
	movetray(-wellspace);
#endif
	calctCmdIO();
	fg_giiAPozTray--;
	fg_giiAStepTray -= fg_wellspace;
}
#else

static void LVicommand __ARGS(( void))
{
	int iiDlpInt;

	iiDlpInt = 1;
	if (TC_old == 1 && fg_giiDlp)//giiDlp	// check of TimeCalcOLD on Loops
		iiDlpInt = fg_giiDlp; //giiDlp;
#ifdef DEBUG
	//  giiAbsStep[1] -= wellspace;
	movetray(-wellspace);
#endif
	calctCmdIO();

	fg_giiAPozTray  -= iiDlpInt;
	fg_giiAStepTray -= fg_wellspace * iiDlpInt;
}

#endif

/*********************************************************************************/
/*! \fn    static void LVjcommand __ARGS(( void))
     \brief Start attesa che Scanner termina quando vuole

Questo comando genera una tabella a parte su .exttime
 */
/*********************************************************************************/
static void LVjcommand __ARGS(( void))
{
	fg_giiNj = tc_parse_num(fg_tc_argptr, 1);
	switch(fg_giiNj)
	{
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		{
			if (fg_iiEsecU == TRUE)
				fg_tc_error = TC_ERR_JAFU;
		}
		case 7:
		{
	#ifdef DEBUG
			strcpy(gpstExta->pccNatm[giiNa], "CMD");
			gpstExta->piiRelstp[giiNa]  = 0;
			gpstExta->piiTypecom[giiNa] = 0;
	#endif
		}break;
		default:
		{
			fg_tc_error = TC_ERR_BAD_J;
		}break;
	}
}



/*********************************************************************************/
/*! \fn    static void LVkcommand __ARGS(( void))
     \brief Muove la TORRE

Muove la torre in una delle 5 posizioni note.
 */
/*********************************************************************************/
static void LVkcommand __ARGS(( void))
{
	int k;

	k = tc_parse_num(fg_tc_argptr, 1);
	position_tower((unsigned int)k);
}


/*********************************************************************************/
/*! \fn    static void LVlcommand __ARGS(( void))
     \brief Definisce distanza tra due well

Questo comando non ha effetti sull'esecuzione del protocollo, ne' atomi o
tempi.
 */
/*********************************************************************************/
static void LVlcommand __ARGS(( void))
{
	int iiI;

	fg_giiComIO = -1;
	fg_wellspace = tc_parse_num(fg_tc_argptr, 3);
	for (iiI = 0; iiI < LENpiiPacc; iiI++)
	{
		if(fg_wellspace == fg_piiPacc[iiI])
			break;
	}
	if (iiI < LENpiiPacc)
		fg_giiComIO = iiI;
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
}


/*********************************************************************************/
/*! \fn    static void mcommand __ARGS(( void))
     \brief Realizza MIX

Realizza il  MIX con volumi e velocita' fissate rispettivamente nelle var globali
V ed S. Il MIX e' ripetuto per un numero di volte passato dal primo parametro
dopo M. Il sottociclo e' costituito da 4 atomi:

\arg 1. POMPA - AtomTime = tempo di aspirazione con motore pompa
\arg 2. WAIT  - AtomTime = tempo di attesa con liquido dentro SPR
\arg 3. POMPA - AtomTime = tempo di svuotamento
\arg 4. WAIT  - AtomTime = tempo di attesa dopo svuotamento

a cui si deve sommare un DELAY totale sul comando
 */
/*********************************************************************************/
static void LVmcommand __ARGS(( void))
{
	long   llMtime;

	llMtime  = ((long)(table_time((fg_tc_argptr + 1))) * 1000l);
	// Aspira
#ifdef DEBUG
	giiRelSpr = pgstGvar->iiPumpMixVolume;
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_FILL");
	gpstExta->piiRelstp[giiNa]  = giiRelSpr;
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	// gia' dal II giro in questo punto si arriva da una attesa di 1 secondo
	fg_llCorpomp = -1L;
	pump_move_time_mix((unsigned int)(fg_pgstGvar->iiPumpMixVolume));
	// Attende
	calcTime(llMtime);
	// Eroga
#ifdef DEBUG
	giiRelSpr = -(pgstGvar->iiPumpMixVolume);
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_EMPTY");
	gpstExta->piiRelstp[giiNa]  = giiRelSpr;
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	// effettua la correzione del tempo di movimento della pompa che verr� eseguito
	// dopo l' attesa. Se il tempo di attesa e' inferiore a 3 secondi la pompa sara'
	// piu' veloce di 1 msec, altrimenti e' piu' veloce di 2 msec
	if (llMtime < 3000L)
		fg_llCorpomp = -1L;
	else
		fg_llCorpomp = -2L;
	pump_move_time_mix((unsigned int)(fg_pgstGvar->iiPumpMixVolume));
	// Attende 1 secondo
	calcTime(1000);
	// Ripete il ciclo
	fg_giiInlp  = table_cycles(fg_tc_argptr);
}




/*********************************************************************************/
/*! \fn    static void ncommand __ARGS(( void))
     \brief End of 'do loop' command string
 */
/*********************************************************************************/
static void LVncommand __ARGS(( void))
{
	if (!fg_giiDlp)
		fg_tc_error = TC_ERR_NCOM;
	fg_giiDlp = 0;
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
}


/*********************************************************************************/
/*! \fn    static void ocommand __ARGS(( void))
     \brief Move tray out one well interval

Tray OUT from VIDAS.
 */
/*********************************************************************************/
#ifdef TIMECALC_BEFORE20111108
static void LVocommand __ARGS(( void))
{
#ifdef DEBUG
	movetray(wellspace);
#endif
	calctCmdIO();
	fg_giiAStepTray += fg_wellspace;
	fg_giiAPozTray++;
}
#else
static void LVocommand __ARGS(( void))
{
	int iiDlpInt;

	iiDlpInt = 1;
	if (TC_old == 1 && fg_giiDlp)	// // check of TimeCalcOLD on Loops
		iiDlpInt = fg_giiDlp;
#ifdef DEBUG
	movetray(wellspace);
#endif
	calctCmdIO();
	fg_giiAPozTray  += iiDlpInt;
	fg_giiAStepTray += fg_wellspace * iiDlpInt;
}
#endif

/*********************************************************************************/
/*! \fn    static void pcommand __ARGS(( void))
     \brief drain SPR with specified volume (PUT Liquid)

Esegue 2 atomi:
\arg 1. Muove la TORRE in posizione 4
\arg 2. Muove la POMPA di tanti passi quanto e' il volume da espellere

 */
/*********************************************************************************/
static void LVpcommand __ARGS(( void))
{
	unsigned int uiApp;

	uiApp = table_volume(fg_tc_argptr);
	// Time to move to k4 ( 0 if there)
	position_tower( 4 );
#ifdef DEBUG
	giiRelSpr = (-1) * (int)(uiApp);
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_EMPTY");
	gpstExta->piiRelstp[giiNa]  = giiRelSpr;
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	// Time to drain selected volume
	pump_move_time_mix(uiApp);
}



/*********************************************************************************/
/*! \fn    static void LVqcommand __ARGS(( void))
     \brief Definisce volume di WASH

Questo comando non ha effetti sull'esecuzione del protocollo, ne' atomi o
tempi.
 */
/*********************************************************************************/
static void LVqcommand __ARGS(( void))
{
	int iiIndc;

	fg_pgstGvar->iiPumpWashVolume = table_volume( fg_tc_argptr );
	iiIndc = calcchar(fg_tc_argptr);
	//  if (iiIndc < 11)
	//   tc_error = TC_ERR_5uL;
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
}


/*********************************************************************************/
/*! \fn    static void rcommand __ARGS(( void))
     \brief Prepare incubator to run assay.

Esegue il posizionamento dei motori in modo da essere pronto a cominciare assay.
 */
/*********************************************************************************/
static void LVrcommand __ARGS(( void))
{
	fg_giiAk         = 1;
	fg_giiAPozTray   = 0;
	fg_giiAStepTray  = -5;
	fg_iiEsecU       = FALSE;
	fg_llCorpomp     = 0L;
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "RESET");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif

	//improvement of checking index memory
	if (fg_giiNa>=NMAXATOM)
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NAMAX);
		return;
	}
	///////////
	fg_gpuiTma[fg_giiNa]              = DLYRESET;
	fg_gpuiTea[fg_giiNa]              = DLYRESET;
	incrNatom();
}

/*********************************************************************************/
/*! \fn    static void LVscommand __ARGS(( void))
     \brief Definisce velocita' di MIX

Questo comando non ha effetti sull'esecuzione del protocollo, ne' atomi o
tempi.
 */
/*********************************************************************************/
static void LVscommand __ARGS(( void))
{
	fg_pgstGvar->iiPumpMixSpeed = table_speed( fg_tc_argptr );
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
}


/*********************************************************************************/
/*! \fn    static void tcommand __ARGS(( void))
     \brief wait selected time in seconds

Setta un DELAY fisso.
 */
/*********************************************************************************/
static void LVtcommand __ARGS(( void))
{
	long llMt;          // millisecondi di attesa

	llMt = ((long)(table_time(fg_tc_argptr)) * 1000l);
	calcTime(llMt);
}

/*********************************************************************************/
/*! \fn    static void ucommand __ARGS(( void))
     \brief end of assay - prepare incubator for cleanout

Unload ovvero Chiudi il protocollo. Esegue atomi:
\arg 1. TOWER - Muove la torre nella posizione 0
\arg 2. TRAY  - Muove la Tray in home con t fisso(0.6)
\arg 3. TRAY  - Tray  fully OUT
\arg 4. PUMP  - Pompa in home
 */
/*********************************************************************************/
static void LVucommand __ARGS(( void))
{
#ifdef DEBUG
	int iiSvws;
	float ffTe;
	float ffTm;

	// sequenza di comandi "K0Z3L660O"
	// 1 - Torre in K0
	position_tower(0);
	// 2 - Tray in home (comando Z3)
	CalcTE(giiAStepTray + 5, VELTRAYE, &gpuiTea[giiNa]);
	// Calcola il tempo misurato utilizzando la retta di correlazione
	ffTe = (float)(gpuiTea[giiNa]);
	ffTm = ffTe * ffMtray + ffNtray;
	gpuiTma[giiNa] = (unsigned int)(ffTm);
	strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TR);
	movetray(-(giiAStepTray + 5));
	giiAPozTray  = 0;
	giiAStepTray = -5;
	incrNatom();
	// 3 - Tray in A (Simula L660 O)
	iiSvws    = wellspace;
	wellspace = 660;
	movetray(wellspace);
	calctCmdIO();
	giiAStepTray += wellspace;
	giiAPozTray   = WELL_CUVETTE;
	wellspace     = iiSvws;
	strcpy(gpstExta->pccNatm[giiNa], "COM. U");
#else
  //improvement of checking index memory
  if (fg_giiNa>=NMAXATOM)
  {
	HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NAMAX);
	return;
  }
  ////////////////
	fg_gpuiTma[fg_giiNa]  = DLYUCOMT;
	fg_gpuiTea[fg_giiNa]  = DLYUCOMT;
	incrNatom();
#endif
	fg_giiAk           = 0;
	fg_giiAPozTray     = 0;
	fg_giiAStepTray    = -5;
	fg_iiEsecU         = TRUE;
}

/*********************************************************************************/
/*! \fn    static void LVvcommand __ARGS(( void))
     \brief Definisce volume di MIX

Questo comando non ha effetti sull'esecuzione del protocollo, ne' atomi o
tempi.
 */
/*********************************************************************************/
static void LVvcommand __ARGS(( void))
{
	int iiIndc;

	fg_pgstGvar->iiPumpMixVolume = table_volume(fg_tc_argptr);
	iiIndc = calcchar(fg_tc_argptr);
	//  if (iiIndc < 11)
	//   tc_error = TC_ERR_5uL;
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "CMD");
	gpstExta->piiRelstp[giiNa]  = 0;
	gpstExta->piiTypecom[giiNa] = 0;
#endif
}


/*********************************************************************************/
/*! \fn    static void wcommand __ARGS(( void))
     \brief WASH command

Cycle washvolume in SPR selected number of cycles with selected full time in seconds
E' identico al comando 'M' tranne che per i parametri relativi al volume e alla
velocita' della pompa e gli offset di delay.Il sottociclo e' costituito da 4 atomi:

\arg 1. POMPA - AtomTime = tempo di aspirazione con motore pompa
\arg 2. WAIT  - AtomTime = tempo di attesa con liquido dentro SPR
\arg 3. POMPA - AtomTime = tempo di svuotamento
\arg 4. WAIT  - AtomTime = tempo di attesa dopo svuotamento
 */
/*********************************************************************************/
static void LVwcommand __ARGS(( void))
{
	long   llMtime;

	llMtime  = ((long)(table_time((fg_tc_argptr + 1))) * 1000l);
	// Aspira
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_FILL");
	gpstExta->piiRelstp[giiNa]  = pgstGvar->iiPumpWashVolume;
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	// gia' dal II giro in questo punto si arriva da una attesa di 1 secondo
	fg_llCorpomp = -1L;
	pump_move_time_wash( (unsigned int)(fg_pgstGvar->iiPumpWashVolume));
	// Attende
	calcTime(llMtime);
	// Eroga
#ifdef DEBUG
	strcpy(gpstExta->pccNatm[giiNa], "PUMP_EMPTY");
	gpstExta->piiRelstp[giiNa]  = -(pgstGvar->iiPumpWashVolume);
	gpstExta->piiTypecom[giiNa] = 3;
#endif
	// effettua la correzione del tempo di movimento della pompa che verr� eseguito
	// dopo l' attesa. Se il tempo di attesa e' inferiore a 3 secondi la pompa sara'
	// piu' veloce di 1 msec, altrimenti e' piu' veloce di 2 msec
	if (llMtime < 3000L)
		fg_llCorpomp = -1L;
	else
		fg_llCorpomp = -2L;
	pump_move_time_wash( (unsigned int)(fg_pgstGvar->iiPumpWashVolume));
	// Attende 1 secondo
	calcTime(1000);
	// Ripete il ciclo
	fg_giiInlp  = table_cycles(fg_tc_argptr);
}



/*********************************************************************************/
/*! \fn    static void xcommand __ARGS(( void))
     \brief Go to selected well and double-punch foil

Esegue gli spostamenti del tray (con double punch) fino alla well impostata.
Se necessario alza la torre, si muove sulla well di destinazione, ed esegue
il doppio foro. Chiama la xy_boogie()
Atomi:
\arg 1. TOWER - Sposta la torre in posiz 1 per liberare TRAY
\arg 2. TRAY  - Se tray nn e' sopra well giusta, lo sposta
\arg 3. TOWER - Manda la torre in posiz 3
\arg 4. TOWER - Riporta la torre in posiz 1
\arg 5. TRAY  - Muove il TRAY di un offset per il DoublePunch
\arg 6. TOWER - Abbassa la Torre in 4 (dentro la well)
\arg 7. DELAY - Conteggia un ritardo finale di 0.1 sec

 */
/*********************************************************************************/
static void LVxcommand __ARGS(( void))
{
	int iiTray;

	iiTray = table_volume( fg_tc_argptr );
	if (iiTray < 0 || iiTray > 10)
	{
		fg_tc_error = TC_ERR_NTRAY;
		return;
	}
	///////////////////
	//improvement of checking index memory
	if ((fg_giiAPozTray<0)||(fg_giiAPozTray>MAX_POSTRAYWELL))
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NTRAY);
		return;
	}
	/////////////
	if (!iiTray)
	{
		// Torre in K1
		position_tower(1);
		// tray in iiX
		fg_gpuiTma[fg_giiNa] = fg_llTmtray[fg_giiAPozTray][iiTray];
		fg_gpuiTea[fg_giiNa] = fg_llTetray[fg_giiAPozTray][iiTray];
#ifdef DEBUG
		strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TR);
		movetray((iiTray - giiAPozTray) * wellspace);
#endif
		fg_giiAStepTray += (iiTray - fg_giiAPozTray) * fg_wellspace;
		incrNatom();
	}
	else
	{
		// Torre in K1
		position_tower(1);
		// Tray in vent della cella c
		fg_gpuiTma[fg_giiNa] = fg_llTmtray_vent[fg_giiAPozTray][iiTray];
		fg_gpuiTea[fg_giiNa] = fg_llTetray_vent[fg_giiAPozTray][iiTray];
#ifdef DEBUG
		strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TR);
		movetray((((iiTray - giiAPozTray) * wellspace) - 20));
		strcpy(gpstExta->pccNatm[giiNa], "TRAY_VENT");
		gpstExta->pffVela[giiNa] = VELTRAYE;
		gpstExta->piiTypecom[giiNa] = 2;
#endif
		fg_giiAStepTray += ((iiTray - fg_giiAPozTray) * fg_wellspace - 20);
		incrNatom();
		// Torre in K3 (esgue vent)
		position_tower(3);
		// Torre in K1
		position_tower(1);
		// Tray centrato su c (20 step)
		fg_gpuiTma[fg_giiNa] = fg_llTmtray_vent[iiTray][iiTray];
		fg_gpuiTea[fg_giiNa] = fg_llTetray_vent[iiTray][iiTray];
#ifdef DEBUG
		strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TR);
		gpstExta->piiRelstp[giiNa]  = 20;
		strcpy(gpstExta->pccNatm[giiNa], "TRAY_CENTER");
		gpstExta->pffVela[giiNa] = VELTRAYE;
		gpstExta->piiTypecom[giiNa] = 2;
#endif
		fg_giiAStepTray += 20;
		incrNatom();
		// Torre in K4
		position_tower(4);
	}
	fg_giiAPozTray = iiTray;
}


/*********************************************************************************/
/*! \fn    static void ycommand __ARGS(( void))
     \brief Go to selected well and single - punch foil

Esegue gli spostamenti del tray (con single punch) fino alla well impostata.
Se necessario alza la torre, si muove sulla well di destinazione, ed esegue
il foro. Chiama la xy_boogie()
Atomi:
\arg 1. TOWER - Sposta la torre in posiz 1 per liberare TRAY
\arg 2. TRAY  - Se tray nn e' sopra well giusta, lo sposta
\arg 3. TOWER - Abbassa la Torre in 4 (dentro la well)
\arg 4. DELAY - Conteggia un ritardo finale di 0.1 sec

 */
/*********************************************************************************/
static void LVycommand __ARGS(( void))
{
	int iiTray;

	// Torre in K1
	position_tower(1);

	// tray in iiX
	iiTray = table_volume( fg_tc_argptr );
	if (iiTray < 0 || iiTray > 10)
	{
		fg_tc_error = TC_ERR_NTRAY;
		return;
	}
	/////////////////
	//improvement of checking index memory
	if ((fg_giiAPozTray<0)||(fg_giiAPozTray>MAX_POSTRAYWELL))
	{
		HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NTRAY);
		return;
	}
	///////////////
	fg_gpuiTma[fg_giiNa] = fg_llTmtray[fg_giiAPozTray][iiTray];
	fg_gpuiTea[fg_giiNa] = fg_llTetray[fg_giiAPozTray][iiTray];
#ifdef DEBUG
	strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TR);
	movetray((iiTray - giiAPozTray) * wellspace);
#endif
	fg_giiAStepTray += (iiTray - fg_giiAPozTray) * fg_wellspace;
	incrNatom();
	// Torre in K4
	position_tower(4);
	fg_giiAPozTray = iiTray;
}

/*********************************************************************************/
/*! \fn    static void zcommand __ARGS(( void))
     \brief Seek the home location of the selected axis

Esegue il reset del motore indicato dal parametro successivo a Z con un singolo
atomo.
 */
/*********************************************************************************/
static void LVzcommand __ARGS(( void))
{
	static float ffTe;
	static float ffTm;
	int   iiSaveVel;

	ffTe = 0;
	switch(tc_parse_num( fg_tc_argptr, 1 ))
	{
		// tower a home
		case 1:
		{
			position_tower(2);
		}break;
		// Pompa a home
		case 2:
		{
			#ifdef DEBUG
				giiRelSpr = (300);
				strcpy(gpstExta->pccNatm[giiNa], "PUMP_FILL");
				gpstExta->piiRelstp[giiNa]  = giiRelSpr;
				gpstExta->piiTypecom[giiNa] = 3;
			#endif
			iiSaveVel = fg_pgstGvar->iiPumpMixSpeed; // save actual speed
			fg_pgstGvar->iiPumpMixSpeed = 60;        // set speed home procedure
			pump_move_time_mix(278);              // 300 step is 278uL
			fg_pgstGvar->iiPumpMixSpeed = iiSaveVel; // restore speed
		}break;
		// Tray a home
		case 3:
		{
			//           gpuiTma[giiNa] = llTmZ3[giiAtray];
			//           gpuiTea[giiNa] = llTeZ3[giiAtray];
			// Calcola il tempo teorico
			//improvement of checking index memory
			if (fg_giiNa>=NMAXATOM)
			{
				HaltTaskTCalc(__FILE__,__LINE__,TC_ERR_NAMAX);
				return;
			}
			////////////////
			CalcTE(fg_giiAStepTray + 5, VELTRAYE, &fg_gpuiTea[fg_giiNa]);
			// Calcola il tempo misurato utilizzando la retta di correlazione
			ffTe = (float)(fg_gpuiTea[fg_giiNa]);
			ffTm = ffTe * fg_ffMtray + fg_ffNtray;
			fg_gpuiTma[fg_giiNa] = ((unsigned int)(ffTm) + Z_DELAY_MES);					// MODIFIED ON 1st Feb 2011 just to create a #define used in protocol.c too
			#ifdef DEBUG
				strcpy(gpccNatom[giiNumComD - 1][giiNa], STR_TR);
				movetray(-(giiAStepTray + 5));
			#endif
			fg_giiAPozTray  = 0;
			fg_giiAStepTray = -5;
			incrNatom();
		}break;

		default:
		{
			//Error, bad z number!
			fg_tc_error = TC_ERR_BAD_Z;
		}break;
	}
}

#ifdef DEBUG
/*********************************************************************************/
/*! \fn    static void movetray(int iiNStepTray)
     \brief Preapara i dati per il file exttime
     \ iiNpoz = Numero di pozzetti da muoversi
 */
/*********************************************************************************/
static void movetray(int iiNst)
{

	//  giiAbsStep[1] -= wellspace;
	if (iiNst < 0)
		strcpy(gpstExta->pccNatm[giiNa], "TRAY_IN");
	else
		strcpy(gpstExta->pccNatm[giiNa], "TRAY_OUT");
	gpstExta->piiRelstp[giiNa]  = iiNst;
	gpstExta->piiTypecom[giiNa] = 2;
	gpstExta->pffVela[giiNa] = VELTRAYE;
}
#endif


#define jCmd  ('J' - 'A')
#define dCmd  ('D' - 'A')
#define nCmd  ('N' - 'A')

/*********************************************************************************/
/*! \fn    err = TimeCalcJ(const char *Locals, unsigned  int *Jgap)
     \brief Calcola il tempo fra due J
 */
/*********************************************************************************/
int TimeCalcJ(char *Locals, unsigned  int *Jgap)
{
	int          protoCmdOffset = 1;	// command pointer, skip the header '&'
	int          lastJID = -1;          // Ultimo J sul quale e' stato eseguito il calcolo
	STR_Atomi    protoCmdData;
	unsigned int timeSinceLastJ = 0;	// tempo misurato fra due J
	int	protoLoopStartOffset = -1;
	int	currentLoopCycles = 0;


	if( fg_tc_error != TC_OPEN )
	{
		fg_tc_error = TC_ERR_NOOPEN;
		return fg_tc_error;
	}

	while(1)
	{
		int atom;
		int cmdTotTime = 0;
		int nextCmd;

		#if REPORT_DBG==1
			char c = *(Locals + protoCmdOffset);
		#endif
		TimeCommand(Locals + protoCmdOffset, &protoCmdData);

		if( fg_tc_error != 0 )
			break;	// proto end or error

		nextCmd = protoCmdOffset + protoCmdData.iiNarg;		// next cmd if a loop end doesn't get back

		#if REPORT_DBG==1
			printf("%d: (%d)[%d]%c = { ", (int)timeSinceLastJ, (int)currentLoopCycles,  (int)protoCmdOffset,  c);
		#endif

		for( atom = 0; atom < protoCmdData.iiNa; ++atom )
		{
			#if REPORT_DBG==1
				printf("%d ", (int)protoCmdData.puiTma[atom]);
			#endif
			cmdTotTime += protoCmdData.puiTma[atom];
		}
		cmdTotTime *= fg_giiInlp;
		timeSinceLastJ += cmdTotTime;

		#if REPORT_DBG==1
			printf("}%d\n", (int)cmdTotTime);
		#endif

		switch( fg_giiNcom )
		{
			case jCmd:	// J found
			{
				int JinTheMiddle;
				if( fg_giiDlp )	// J cmd is not allowed inside a loop
				{
					fg_tc_error = TC_ERR_DNELJ;
					break;
				}

				for( JinTheMiddle = lastJID + 1; JinTheMiddle < fg_giiNj; ++JinTheMiddle )	// reset not found Js
				{
					Jgap[ JinTheMiddle ] = 0;
				}

				lastJID = fg_giiNj;
				Jgap[ lastJID ] = timeSinceLastJ;				// setup found J
				timeSinceLastJ = 0;
			}break;

			case dCmd:	// Loop start found
			{
				if( currentLoopCycles == 0 )
				{
					// first loop -> set the loop counter and loop start offset
					currentLoopCycles = fg_giiDlp;
					protoLoopStartOffset = protoCmdOffset;
				}
			}break;

			case nCmd:	// loop end found
			{
				currentLoopCycles--;
				if( currentLoopCycles == 0 )
				{
					// loop finished
					protoLoopStartOffset = -1;
				}
				else
				{
					nextCmd = protoLoopStartOffset;
				}

			}break;

			default: 
			{
				/*nothing to do*/ 
			}break;
		}
		protoCmdOffset = nextCmd;

		#if REPORT_DBG==1
			fflush(stdout);
		#endif
	}

	/////////////////////////////////////////////
	#if REPORT_JGAP_TIME==1
	{
		int jdx;
		for (jdx=0;jdx<8;jdx++)
		{
			printf("Jgap[%d]=%d\n",jdx,Jgap[jdx]);
		}
	}
	#endif
	return fg_tc_error;
}


/*###############################################################################*/
/***********************/
/* ENDOFFILE 20190109  */
/***********************/
