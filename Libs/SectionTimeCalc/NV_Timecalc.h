/*********************************************************************************/
/*!  \file  NV_Timecalc.h
     \brief Calcolo dei tempi degli atomi

Questo modulo effettua i tempi di esecuzione dei movimenti da parte della scheda
incubator del Vidas.
Per la pompa i tempi sono calcolati utilizzando una tabella, per la torre e il tray
parte dei movimenti sono tabellati e parte dei movimenti sono calcolati utilizzando
una retta di correlazione che trasforma il tempo teorico in tempo misurato.
Tempo teorico  = Tempo teorico di esecuzione di un movimento
Tempo misurato = Tempo impiegato dall' incubator per effettuare un movimento
Atomo          = Movimento elementare eseguito dall' incubator
Comando        = Singolo comando inviato dalla scanner all' incubator (file .P)
    \author  Laboratori Victoria
    \date    14_settembre_2010
*/
/*********************************************************************************/
#ifndef _H__NV_TIMECALC_

#define _H__NV_TIMECALC_
//#define VERTIMECALC "01.01.004"   // Versione modulo NV_Timecalc
//  Calcoli tempi eseguiti da tabella
//  Manca la gestione di alcuni comandi: B, N, D, etc.

//#define VERTIMECALC "01.01.005"   // Versione modulo NV_Timecalc
// Aggiornamenti effettuati:
//   1 - Eliminare gli warning
//   2 - Ripulito il codice da funzioni inutilizzate
//   3 - Messo la #define DEBUG per effettuare la compilazione condizionata
//       per il debug.
// Da fare:
//     - Modificare il passaggio dei parametri della funzione TimeCommand()
//       secondo le specifiche discusse fra Francesco e Paolo
//     - Aggiornare la tabella dei tempi della pompa per volumi inferiori a 5uL
//     - Attivare i comandi B, D, N
//     - Per i movimenti del Tray e del Tower, aggiungere una retta di
//       correlazione dalla quale effettuare i calcoli per i punti non tabellati.

//#define VERTIMECALC "01.01.006"   // Versione modulo NV_Timecalc
// Aggiornamenti effettuati:
//    1 - Aggiornamento sotto Debug parte di codice utilizzato da visualizzatore
//    2 - Sistemate i valori di default delle variabili globali quando si fa la
//        open.
//#define VERTIMECALC "01.01.007"   // Versione modulo NV_Timecalc
// Aggiornamenti effettuati:
//    1 - Sistemato comando Z3
//#define VERTIMECALC "01.01.008"   // Versione modulo NV_Timecalc
// Aggiornamenti effettuati:
//    1 - Sistemato baco senza DEBUG (giiNcom = i)
//#define VERTIMECALC "01.01.009"   // Versione modulo NV_Timecalc
// Aggiornamenti effettuati:
//    1 - Sistemato bachi dei tempi effettivi
//#define VERTIMECALC "01.01.012"   // Versione modulo NV_Timecalc
//////////////////////////////////////////////////////////////////		
//	MAD 01.02.000: 20170405 NefroCheck Project
//	fixed bug on loops prediction
//	20190109 : added following defines
//		TC_ERR_RPUMP.TC_ERR_CPUMP.TC_ERR_IDXRAMP
#define VERTIMECALC "01.02.000"   // Versione modulo NV_Timecalc

/*###############################################################################*/
/*********************************************************************************/
/*                            INCLUDE                                            */
/*********************************************************************************/

/*********************************************************************************/
/*                            DEFINE                                             */
/*********************************************************************************/
/* If this is an ANSI compiler, set things up to allow for
 * prototypes in headers.  This is to accomodate those headers which
 * might still have to retain compatability with K&R 'C'.
 */
#ifndef __ARGS
#if __STDC__ || __ZTC__
#define __ARGS(a)   a
#else
#define __ARGS(a)   ()
#endif
#endif

//#define DEBUG                     // vero se effettuiamo il debug

#ifndef NUM_CMDS
#define NUM_CMDS                   26
#endif

#ifndef NUM_SELECTS
#define NUM_SELECTS                36
#endif


// Error from the timecalc routine.
#define TC_PRTEND                   -1  // siamo giunti a fine protocollo
#define TC_OPEN                     0
#define TC_ERR_TIME_ARG             1
#define TC_ERR_SPEED_ARG            2
#define TC_ERR_BAD_CMD              3
#define TC_ERR_BAD_J                5
#define TC_ERR_BAD_Z                6
#define TC_ERR_GLOBAL_DELIM         7
#define TC_ERR_LOCAL_DELIM          8
#define TC_CLOSE                    9   // (*) non eseguo comando perche' non e' stato aperto timecalc
                                        //     eseguire TimeOpen()
#define TC_ERR_OPEN                10   // (*) errore di apertura eseguire la TimeClose() prima della TimeOpen(...)
#define TC_ERR_NOOPEN              11   // (*) si vuole eseguire una funzione senza aver eseguito la TimeOpen(...),
                                        // eseguire la TimeClose() e poi la TimeOpen(...)
#define TC_ERR_BCOM                12   // comando B nel protocollo non accettato
#define TC_ERR_CCOM                13   // comando C nel protocollo non accettato
#define TC_ERR_SPEED               14   // speed non e' un valore accettato 20, 40.... 320, 360
#define TC_ERR_VOL                 15   // Volume non e' un valore accettato 0, 1 ...603, 649
#define TC_ERR_NAMAX               16   // Numero atomi eccede il massimo
#define TC_ERR_LCOM                17   // Si accetta solo L066 e L071
#define TC_ERR_NTRAY               18   // Numero Tray non compreso fra 0 e 10
#define TC_ERR_DCOM                19   // Comando D non accettato
#define TC_ERR_NCOM                20   // Comando N non accettato
#define TC_ERR_5uL                 21   // Volumi sotto 5 uL non accettati
#define TC_ERR_DNELJ               22   // Comando J all'interno di un ciclo loop
#define TC_ERR_PTOWER              23   // Richiesta posizione tower errata (non compresa fra 0 e 5)
#define TC_ERR_ACOM                24   // comando A nel protocollo non accettato
#define TC_ERR_JAFU                25   // comando J 0 - 4 dopo il comando U
//01.02.000
#define TC_ERR_RPUMP			   30	// indice calcolato row pump
#define TC_ERR_CPUMP			   31	// indice calcolato col pump
#define TC_ERR_IDXRAMP			   32	// indice calcolato rampa errato

#define NMAXJGAP                    8
#define NMAXATOM                   10   // Numero massimo di atomi per comando

#define STR_TW   "Tower"
#define STR_PM   "Pump "
#define STR_TR   "Vasso"
#define STR_ERR  "ERROR"
#define STR_W    "WAIT "
#define STR_T    "Time "
#define STR_CMD  "CMD  "



#define NON     0
#define STW     1
#define SPM     2
#define STR     3
#define ERR     4
#define C_W     5
#define C_T     6
#define CCC     7

#define RESET_ON    1
#define RESET_OFF   0


#define NUM_READINGS      8
#define MAX_ATOMI      5000  // Numero massimo di atomi per comando
#define VELMAXRAMP      360  // Velocita' massima della rampa
#define VELTRAYN        400  // Velocita' tray
#define VELTOWERUPN     400  // Velocita' tower in salita
#define VELTOWERDWNN    360  // Velocita' tower in discesa
#define NSTEPRAMPA       13  // N. step della rampa
// Velocita' effettive
#define VELTOWERUPE     395.224
#define VELTOWERDOWNE   356.255
// #define VELTRAYE        356.570			

#define VELTRAYE        395.570

// #define TC_ERR_DNELJ             22   // Comando J all'interno di un ciclo loop
// #define TC_ERR_PTOWER         23   // Richiesta posizione tower errata (non compresa fra 0 e 5)
// #define TC_ERR_ACOM              24   // comando A nel protocollo non accettato


#define Z_DELAY_MES	6					// define created by bmx on 1st Feb 2011

/*********************************************************************************/
/*           Define per la supervisor                                            */
/*********************************************************************************/
// #define DLYRESET        	15016 		// Time R command		// DEBUG TO BE COMMENTED
//#define DLYRESET        	13606  	// Time R command		// DEBUG TO BE UNCOMMENTED

#define DLYRESET            14016   // Time R command for home before start-proto 20110928

// #define DLYUCOMT        5000  		// Time U command
#define DLYUCOMT        	15153  	// Time U command
#define DLYSRTRDMIN        0   		// Ritardo minimo  inizio lettura (in msec)
#define DLYSRTRDMAX        0   		// Ritardo massimo inizio lettura (in msec)
#define TMRDMIN         9000   		// Durata  minima  lettura (in msec)
#define TMRDMAX        12000   		// Durata  massima lettura (in msec)

/*********************************************************************************/
/*                            TYPES                                              */
/*********************************************************************************/
typedef struct
{
    void (*func) __ARGS((void));   //!< Punatatore a funzione corrispondente
                                   //!< al comando
                                   //!<
    unsigned char argcount     ;   //!< Numero di dati passabili con comando
                                   //!<
    unsigned int  numatomi     ;   //!< Massimo # di atomi previsti per comando
                                   //!<
} CommandTable;

typedef  struct
{
  int   iiPumpWashSpeed;            //!< [mircosec / Step ] velocit� da utilizzare
                                    //!< per il comando di wash pompa
  int   iiPumpMixSpeed;             //!< [mircosec / Step ] velocit� da utilizzare
                                    //!< per il comando di Mix pompa
  int   iiPumpWashVolume;           //!< [Step] Volume  da utilizzare
                                    //!< per il comando di wash pompa
  int   iiPumpMixVolume;            //!< [Step] Volume  da utilizzare
                                    //!< per il comando di Mix  pompa
} STR_GlobalVar ;

typedef  struct
{
  int   iiCal;                      //!< Flag di segnalazione che non si usa il calcolo dei tempi
                                    //!< ma si utilizza tempi letti da tabelle fissi
                                    //!< nnNocalc = 1 ? utilizzo tempi da tabella
  int   iiEsec;                     //!< bbEsec = 1 ? stiamo effettuando i calcoli per la section
  int     piiRampatower[20];        //!< [mircosec / Step ] velocit� da utilizzare
                                    //!< per le rampe torre
  int     piiRampapompa[20];        //!< [mircosec / Step ] velocit� da utilizzare
                                    //!< per le rampe pompa
  int     piiRampatray[20];         //!< [mircosec / Step ] velocit� da utilizzare
                                    //!< per le rampe tray
  int     piiVeltower[4];           //!< velocita� [mircosec / Step ]   torre
  int     piiVelpompa[36];          //!< velocita� [mircosec / Step ]   pompa
  int     piiVeltray[4];            //!< velocita� [mircosec / Step ]   tray
  int     piiPostower[10];          //!< posizione  assolute torre in step
  int     piiPospompa[40];          //!< posizione  assolute pompa in step
  int     piiPostray[24];           //!< posizione  assolute tray in step
} STR_GlobalInfo;

typedef  struct
{
  unsigned int puiTea[NMAXATOM];    //!< Tempo effettivo singolo atomo
  unsigned int puiTma[NMAXATOM];    //!< Tempo misurato singolo atomo
  int          iiNarg;              //!< Numero di argomenti di seguito al comando
  int          iiNa;                //!< Numero di atomi che compongono il comando
} STR_Atomi;

typedef  struct
{
  int   iiVel[NSTEPRAMPA];          //!< Velocita'
  float ffMsec[NSTEPRAMPA];         //!< Durata rampa fino a quel passo di velocita'
} STR_Rampa;

/*********************************************************************************/
/*                            PUBLIC FUNCTION                                    */
/*********************************************************************************/
/*********************************************************************************/
/*                            PUBLIC VARIABLE                                    */
/*********************************************************************************/
#ifdef DEBUG
#define     NMAXDLP  100                      // Numero massimo comando all' interno
                                              // di un comando D loop
extern int  giiNcom[NMAXDLP];                 // Numero comando (0...  25)
extern char gpccNatom[NMAXDLP][NMAXATOM][10]; // Array di nomi degli atomi del comando
extern int  giiPerr;                          // Parametro dell' errore
extern char gpccNcom[NMAXDLP][10];            // Nome del comando completo di parametri
extern int  giiInlp;                          // Numero di loop di ripetizione degi atomi comandi M e W
extern int  giiAPozTray;                      // Posizione attuale Tray
extern int  giiAStepTray;                     // Posizione assoluta in step del Tray
extern int  wellspace;                        // L - reagent tray well interval
extern int  giiAk;                            // Attuale posizione della torre
extern int  giiDlp;                           // N. di loop del comando D
                                              // 0 ==> non siamo dentro una fase di loop
extern int  giiNumComD;                       // N. di comando dentro il loop del comando D
extern const int piiKn[5];                    // Posizione della torre
extern       int giiK5;                       // posizione iniziale di K5 modificata dal comando B
#endif

/*********************************************************************************/
/*                            PUBLIC FUNCTION                                    */
/*********************************************************************************/
int    TimeCalcJ   (char *Locals, unsigned  int *Jgap                           );
int    TimeCommand(char *p, STR_Atomi *pstrAtm                                  );
void   TimeVer     (char * versione                                             );
int    TimeOpen    (STR_GlobalVar * strGlobalVars, /*STR_GlobalInfo * strGloInfo,*/
                    char *pccGlob                                               );
void   TimeClose   (void                                                        );

#endif /* _H__NV_TIMECALC_ */

