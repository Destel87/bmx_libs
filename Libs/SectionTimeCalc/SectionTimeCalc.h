//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SectionTimeCalc.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the SectionTimeCalc class.
//! @details
//!
//*!****************************************************************************

#ifndef SECTIONTIMECALC_H
#define SECTIONTIMECALC_H


class SectionTimeCalc
{

	public:

		/*! *************************************************************************************************
		 * @brief SectionTimeCalc contructor
		 * **************************************************************************************************
		 */
		SectionTimeCalc();

		/*! *************************************************************************************************
		 * @brief processProtocol wrapper function to process the protocl string extracting the Reading times
		 * @param strLocals the Locals protocol string
		 * @param strGlobals the Globals protocol string
		 * @param JgapsRel the pointer to the array where the times will be stored
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool processProtocol(char * strLocals, char * strGlobals, unsigned int * JgapsRel);

};

#endif // SECTIONTIMECALC_H
