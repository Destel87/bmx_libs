//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SectionTimeCalc.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the SectionTimeCalc class.
//! @details
//!
//*!****************************************************************************

#include <string>

#include "SectionTimeCalc.h"
#include "NV_Timecalc.h"

#define	SIZE_OF_PROTOCOL_STRING	(1024 + 8)


SectionTimeCalc::SectionTimeCalc()
{
}

bool SectionTimeCalc::processProtocol(char * strLocals, char * strGlobals, unsigned int * JgapsRel)
{
	STR_GlobalVar strGlobalVars;
	char strProtocol[SIZE_OF_PROTOCOL_STRING];
	bool bRet = false;

	sprintf(strProtocol, "*%s*", strGlobals);
	if(TimeOpen(&strGlobalVars, strProtocol) == TC_OPEN)
	{
		sprintf(strProtocol, "&%s&", strLocals);
		if(TimeCalcJ(strProtocol, JgapsRel) == TC_PRTEND)
		{
			// processing completed correctly
			bRet = true;
		}
	}
	TimeClose();
	return bRet;
}
