//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    StaticSingleton.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the StaticSingleton class.
//! @details
//!
//*!****************************************************************************

#ifndef STATICSINGLETON_H
#define STATICSINGLETON_H

#include <stdlib.h>

template<typename Instance>
class StaticSingleton
{
#ifdef STATICSINGLETON_INSTANTIATOR

		class Instantiator
		{
			public:
				Instantiator() { getInstance(); }
				~Instantiator() {}
		};
		static Instantiator m_instantiator;
#endif

	public:
#ifdef STATICSINGLETON_INSTANTIATOR
		StaticSingleton() { &m_instantiator; }
#else
		StaticSingleton() {}
#endif
		virtual ~StaticSingleton() {}
		static Instance* getInstance()	{ return m_pInstance == 0 ? m_pInstance = new Instance() : m_pInstance; }

    private:
        static Instance* m_pInstance;

};

template<typename Instance>
Instance* StaticSingleton<Instance>::m_pInstance = 0;

#ifdef STATICSINGLETON_INSTANTIATOR
template<typename Instance>
typename StaticSingleton<Instance>::Instantiator StaticSingleton<Instance>::m_instantiator;
#endif

#endif // STATICSINGLETON_H
