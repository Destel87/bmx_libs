//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Log.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the Log class.
//! @details
//!
//*!****************************************************************************

#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdarg.h>

/*! ***********************************************************************************************
 * MACROS
 * ************************************************************************************************
 */
//#define IS_INTEGRITY

/*! ***********************************************************************************************
 * DEFINES
 * ************************************************************************************************
 */
#define LOG_VERSION "1.0.0"
// The following log levels are taken from the Linux syslog, maybe the first two won't never be used
/*! @def system emergency */
#define LOG_EMERG			0
/*! @def system alert */
#define LOG_ALERT			1
/*! @def critical error impossibile to recover */
#define LOG_CRIT			2
/*! @def generic error that could cause a system failure */
#define LOG_ERR				3
/*! @def Warning, there has been a recovered error or an anomaly */
#define LOG_WARNING			4
/*! @def System correctly works but there's been something strange */
#define LOG_NOTICE			5
/*! @def info on the program execution */
#define LOG_INFO			6
/*! @def debug messages */
#define LOG_DEBUG			7
/*! @def more debug messages (maybe too many) */
#define LOG_DEBUG_PARANOIC	8
/*! @def even more debug messages (surely too many) */
#define LOG_DEBUG_IPER_PARANOIC	9

#define MAX_TMP_BUFFER_SIZE 1024

/*! ***********************************************************************************************
 * @class Log
 * @brief classe wrapper per le varie funzioni della libreria log
 *
 *  * @file Log.h
 * @brief Queste funzioni permettono di eseguire il log su un file.
 *  E' possibile specificare il livello di log ch si desidera avere,
 *  tipicamente piu' e' alto il livello e maggiore e' il numero di
 *  messaggi scritti. <br>
 *  La libreria propone una serie di livelli di log ispirati al syslog
 *  dei sistemi linux ma possono anche essere definiti dall'utente. <br>
 *  La libreria prevede anche il <b>roll</b> dei files di log. Quando un
 *  file di log raggiunge una dimensione prefissata viene eseguito un roll.
 *  L'operazione di roll consiste nel rinominare il file aggiungendo
 *  l'estensione .1, rinominare il precedente file con estensione .1 in un
 *  file con estensione .2, e cosi' via fino all'ultimo file che viene
 *  sovrascritto (perdendo definitivamente quello precedente).
 * ************************************************************************************************
 */
class Log
{
	public:

		/*! ***************************************************************************************
		 * void constructor
		 * Initializes with default values the internal members of the class
		 * ****************************************************************************************
		 */
		Log();
		/*! ***************************************************************************************
		 * Default destructor
		 * Free resources
		 * ****************************************************************************************
		 */
		virtual ~Log();
		/*!
         * @fn void log(int level, const char fmt[], ...)
		 * @brief Esegue il log di un messaggio passato tramite argomenti
		 * @param level livello di log del messaggio che si sta scrivendo
		 * @param fmt formato degli argomenti del messaggio
		 */
		void log(int level, const char fmt[], ...);
		/*!
         * @fn void logMessage(int level, const char* message)
		 * @brief Esegue il log di un messaggio passato come stringa
		 * @param level livello di log del messaggio che si sta scrivendo
		 * @param message stringa da scrivere sul log. Un messaggio troppo lungo (1024 bytes)
		 *  verra' troncato
		 */
		void logMessage(int level, const char* message);
		/*!
         * @fn void getFile(char * sFileName, int nMaxFileNameSize)
		 * @brief recupera il nome del file di log
         * @param sFileName stringa su cui verra' scritto il nome del file
         * @param nMaxFileNameSize dimensione della stringa name
		 */
		void getFile(char* sFileName, int nMaxFileNameSize);
		/*!
         * @fn int getLevel()
		 * @brief recupera il nome del file di log
		 * @return il livello di log attuale
		 */
		int getLevel(void);
		/*!
         * @fn bool setFile(const char* sFileName)
		 * @brief imposta il nome del file di log
         * @param sFileName da impostare (se vale 'stdout' scrive sullo standard output)
         * @return restituisce true se la libreria puo' usare il file specificato
         *  come file di log e false altrimenti (ad es. se non ha i permessi di scrittura)
		 */
		bool setFile(const char* sFileName);
		/*!
         * @fn void setAppName(const char* sAppName)
		 * @brief imposta il nome dell'applicazione che esegue il log
         * @param sAppName ome dell'applicazione che esegue il log
		 */
		void setAppName(const char* sAppName);
		/*!
         * @fn void setDebug(int status)
		 * @brief Imposta lo stato di debug (1) o meno (0)
		 * @param status stato a cui impostare il debug
		 */
		void setDebug(int status);
		/*!
         * @fn void setLevel(int nLevel)
		 * @brief Imposta il livello di log
         * @param nLevel livello da impostare
		 */
		void setLevel(int nLevel);
		/*!
         * @fn void setMaxNumFiles(int nNumFiles)
		 * @brief Imposta il numero massimo di files su cui eseguire il roll
         * @param nNumFiles massimo numero di files di roll da mantenere
		 */
		void setMaxNumFiles(int nNumFiles);
		/*!
         * @fn void setUseMsecInTimestamp(int nUseMsec)
		 * @brief Indica di usare anche i millisecondi o meno mentre
		 *  scrive i logs
         * @param nUseMsec indica di usare i millisecondi (1) o meno (0)
		 */
		void setUseMsecInTimestamp(int nUseMsec = 1);
		/*!
         * @fn void setMaxFileSizeInBytes(int nNumBytes)
		 * @brief imposta la massima dimensione che puo' avere un file (oltre
		 *  questa dimensione si esegue il roll)
         * @param nNumBytes dimensione massima di un file(in bytes)
		 */
		void setMaxFileSizeInBytes(int nNumBytes);

		/*! ***************************************************************************************
         * @brief Enables or disables log messages
		 * @param	bEnable if set to true enables log messages, otherwise disables log messages
		 * ****************************************************************************************
		 */
		void enable(bool bEnable);

    private:

        char m_sLogFile[MAX_TMP_BUFFER_SIZE]; //!< Name of the log file (if set to 'stdout' it writes on the standard output)
        char m_sAppName[MAX_TMP_BUFFER_SIZE]; //!< Name of the application that is using the class
        int m_nLogLevel; //!< the log level
        int m_nLogDebug; //!< enable (1) or disable (0) the debug mode
        int m_nUseMsec; //!< enable (1) or disable (0) the usage of milliseconds in the timestamps
        unsigned int m_uMaxFileSizeInBytes; //!< max log file size before rolling
        unsigned int m_uMaxNumFiles; //!< maximum number of files for rolling
        void* m_pLockData; //!< pointer to a structure used for the lock
        bool m_bEnable;

};

#endif //LOG_H
