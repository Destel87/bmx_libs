//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Loggable.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the Loggable class.
//! @details
//!
//*!****************************************************************************

#include "Loggable.h"

Loggable::Loggable()
{
	m_pLogger = NULL;
}

Loggable::~Loggable()
{
	// Nothing to be done here yet
}

void Loggable::setLogger(Log* pLogger)
{
	m_pLogger = pLogger;
}

void Loggable::setLogger(Log& rLogger)
{
	m_pLogger = &rLogger;
}

Log* Loggable::getLogger()
{
	return m_pLogger;
}

void Loggable::log(int level, const char* format, ...)
{
	if (m_pLogger == NULL)
	{
		return;
	}
	char message[4096];
	va_list ap;
	va_start(ap, format);
	vsnprintf(message, 4095, format, ap);
	va_end(ap);
	m_pLogger->logMessage(level, message);
}
