//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Log.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the Log class.
//! @details
//!
//*!****************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>


#include "Log.h"

#ifdef DEBUG
#define DEFAULT_DEBUG_STATUS 1
#else
#define DEFAULT_DEBUG_STATUS 0
#endif

#define DEFAULT_LEVEL	LOG_ERR
#define MAX_NUM_FILES	5
#define MAX_FILE_SIZE	1024 * 1024
#define MAX_MSG_SIZE	4096
#define MAX_STR_SIZE    128

#define USE_LOCK
#ifdef USE_LOCK
#include <pthread.h>
#endif


/*! ***********************************************************************************************
 *  Local functions
 * ************************************************************************************************
 */
static void get_current_time(struct timeval *pTv)
{
	gettimeofday(pTv, NULL);
}


static void log_create_mutex(void** pLockData)
{
	#ifdef USE_LOCK
	pthread_mutex_t* mutex;
	mutex = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(mutex, NULL);
	*pLockData = (void*)mutex;
	#endif	// USE_LOCK
}

static void log_delete_mutex(void* pLockData)
{
	#ifdef USE_LOCK
	pthread_mutex_t* mutex;
	if (pLockData == NULL)
	{
		return;
	}
	mutex = (pthread_mutex_t*)pLockData;
	pthread_mutex_destroy(mutex);
	free(pLockData);
	pLockData = NULL;
	#endif	// USE_LOCK
}

static void log_lock(void* pLockData)
{
	#ifdef USE_LOCK
	pthread_mutex_t* mutex;
	if (pLockData == NULL)
	{
		return;
	}
	mutex = (pthread_mutex_t*)pLockData;
	pthread_mutex_lock(mutex);
	#endif
}

static void log_unlock(void* pLockData)
{
	#ifdef USE_LOCK
	pthread_mutex_t* mutex;
	if (pLockData == NULL)
	{
		return;
	}
	mutex = (pthread_mutex_t*)pLockData;
	pthread_mutex_unlock(mutex);
	#endif	// USE_LOCK
}


static void roll_if_needed(char* log_file, unsigned int max_file_size, unsigned int max_num_files)
{
	struct stat filestatus;
	unsigned int i, size;
	char oldname[MAX_MSG_SIZE], newname[MAX_MSG_SIZE];

	if ( stat(log_file, &filestatus) != 0 )
	{
		#ifdef DEBUG
		printf("Impossible to retrieve status of <%s>\n", log_file);
		#endif
		return;
	}
	size = (unsigned int)filestatus.st_size;

    if (size < max_file_size)
	{
		// rolling is not needed
		return;
	}

    for (i = max_num_files - 1; i > 1; i--)
	{
		sprintf(newname, "%s.%d", log_file, i);
        sprintf(oldname, "%s.%d", log_file, i - 1);
		if ( stat(oldname, &filestatus) != 0 )
		{
			continue;
		}
		if ( stat(newname, &filestatus) == 0 )
		{
			unlink(newname);
		}
		rename(oldname, newname);
	}
	sprintf(newname, "%s.1", log_file);
	if ( stat(newname, &filestatus) == 0 )
	{
		unlink(newname);
	}
	rename(log_file, newname);
}


/*! ***********************************************************************************************
 *  Class implementation
 * ************************************************************************************************
 */
Log::Log()
{
	log_create_mutex(&m_pLockData);
	m_nLogDebug = DEFAULT_DEBUG_STATUS;
	m_nLogLevel = DEFAULT_LEVEL;
	strcpy(m_sLogFile, "");
	m_uMaxFileSizeInBytes = MAX_FILE_SIZE;
	m_uMaxNumFiles = MAX_NUM_FILES;
	m_nUseMsec = 1;
	strcpy(m_sAppName, "");
	m_bEnable = true;
	#if defined IS_INTEGRITY
	CTime t;
	t.InitTimeManager();
	t.UpdateLocalDateRTC();
	#endif
}

Log::~Log()
{
	log_delete_mutex(m_pLockData);
	m_nLogDebug = DEFAULT_DEBUG_STATUS;
	m_nLogLevel = DEFAULT_LEVEL;
	strcpy(m_sLogFile, "");
	m_uMaxFileSizeInBytes = MAX_FILE_SIZE;
	m_uMaxNumFiles = MAX_NUM_FILES;
	strcpy(m_sAppName, "");
}

void Log::log(int level, const char fmt[], ...)
{
	if ( !m_bEnable ) return;

    char message[MAX_MSG_SIZE];
	va_list ap;

	va_start(ap, fmt);
    vsnprintf(message, MAX_MSG_SIZE - 1, fmt, ap);
	va_end(ap);
	logMessage(level, message);
}

void Log::logMessage(int level, const char* message)
{
	if ( !m_bEnable ) return;

	FILE* fp;
	char logmsg[MAX_MSG_SIZE];
    char time_msg[MAX_TMP_BUFFER_SIZE];
    char name_msg[MAX_TMP_BUFFER_SIZE] = "";
    char level_msg[MAX_STR_SIZE];
	int debug;
	int file_is_stdout;

	#if defined IS_INTEGRITY
	CTime t;
	string sDate;
	string sTime;
	t.GetDate(sDate);
	t.GetTime(sTime);
	#else
	struct timeval curr_time;
	time_t curr_sec;
	int curr_msec;
	#endif

	debug = 0;
	file_is_stdout = 0;

	log_lock(m_pLockData);
	if (m_nLogLevel < level)
	{
		log_unlock(m_pLockData);
		return;
	}

	#if defined IS_INTEGRITY
	if ( strcmp(m_sLog_file, "console") == 0 )
	#else
	if ( strcmp(m_sLogFile, "stdout") == 0 )
	#endif
	{
		file_is_stdout = 1;
	}

	debug = m_nLogDebug;

	if (strlen(m_sLogFile) == 0)
	{
		log_unlock(m_pLockData);
		return;
	}

	#if defined IS_INTEGRITY
        snprintf(time_msg, MAX_TMP_BUFFER_SIZE - 1, "%s-%s", sDate.c_str(), sTime.c_str());
	#else
	get_current_time(&curr_time);
	curr_sec = curr_time.tv_sec;
	curr_msec = curr_time.tv_usec / 1000;
	if ( m_nUseMsec )
	{
		sprintf(time_msg, "%.19s.%03d", ctime(&curr_sec), curr_msec);
	}
	else
	{
		sprintf(time_msg, "%.19s", ctime(&curr_sec));
	}
	#endif

	// Inserts, eventually, the name of the application
	if (strlen(m_sAppName) > 0)
	{
        if(snprintf(name_msg, MAX_TMP_BUFFER_SIZE - 1, " (%s)", m_sAppName) < 0)
            return;
	}
	if ( level > 18 )
	{
        if(snprintf(level_msg, MAX_STR_SIZE - 1, "[PAR_#]") < 0)
            return;
	}
	else if(level > 8)
	{
        if(snprintf(level_msg, MAX_STR_SIZE - 1,"[PAR_%d]", level - 8) < 0)
            return;
	}
	else
	{
        int ret = 0;
		switch ( level )
		{
			case 0:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[EMERG]");
				break;
			case 1:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[ALERT]");
				break;
			case 2:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[CRIT_]");
				break;
			case 3:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[ERROR]");
				break;
			case 4:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[WARN_]");
				break;
			case 5:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[NOTIC]");
				break;
			case 6:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[INFO_]");
				break;
			case 7:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[DEBUG]");
				break;
			case 8:
                ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[PARAN]");
				break;
			default:
				if(level >= 0)
                    ret = snprintf(level_msg, MAX_STR_SIZE - 1, "[#####]");
				break;
		}
        if(ret < 0)
            return;
	}

	// Builds the complete message
    if(snprintf(logmsg, MAX_MSG_SIZE, "%s%s %s %s ", time_msg, name_msg, level_msg, message) < 0)
    {
        return;
    }

	// Writes on file
	if ( !file_is_stdout )
	{
		fp = fopen(m_sLogFile, "a+");
		if ( fp == NULL )
		{
			log_unlock(m_pLockData);
			return;
		}
		fprintf(fp, "%s\n", logmsg);
		fflush(fp);
		fclose(fp);

		// If the file is not the stdout, checks whether to perform the rolling
		roll_if_needed(m_sLogFile, m_uMaxFileSizeInBytes, m_uMaxNumFiles);
	}

	// in any case print also on stdout
	#ifdef IS_INTEGRITY
	fprintf(console, "%s\n", logmsg);
	#else
	fprintf(stdout, "%s\n", logmsg);
	fflush(stdout);
	#endif

	log_unlock(m_pLockData);
	if (debug)
	{
		printf("%s%s %s %s\n", time_msg, name_msg, level_msg, message);
	}
	return;
}

void Log::getFile(char* sFileName, int nMaxFileNameSize)
{
	log_lock(m_pLockData);
	strncpy(sFileName, m_sLogFile, nMaxFileNameSize);
	log_unlock(m_pLockData);
}

int Log::getLevel(void)
{
	int ret;
	log_lock(m_pLockData);
	ret = m_nLogLevel;
	log_unlock(m_pLockData);
	return ret;
}

bool Log::setFile(const char* sFileName)
{
	FILE* fp;
	struct stat filestatus;
	int rc = 0;

	log_lock(m_pLockData);
        strncpy(m_sLogFile, sFileName, MAX_TMP_BUFFER_SIZE);

	#if defined IS_INTEGRITY
	if ( strcmp(name, "console") != 0 )
	#else
	if ( strcmp(sFileName, "stdout") != 0 )
	#endif
	{
		// Check if the file exists
		if ( stat(sFileName, &filestatus) != 0 )
		{
			// File does not exist try to create it
			fp = fopen(sFileName, "w");
			if (fp != NULL)
			{
				fclose(fp);
			}
			else
			{
				rc = -1;
			}
		}
	}
	log_unlock(m_pLockData);
	return rc;
}

void Log::setAppName(const char* sAppName)
{
	log_lock(m_pLockData);
        strncpy(m_sAppName, sAppName, MAX_TMP_BUFFER_SIZE);
	log_unlock(m_pLockData);
}

void Log::setDebug(int status)
{
	log_lock(m_pLockData);
	m_nLogDebug = status;
	log_unlock(m_pLockData);
}

void Log::setLevel(int nLevel)
{
	log_lock(m_pLockData);
	m_nLogLevel = nLevel;
	log_unlock(m_pLockData);
}

void Log::setMaxNumFiles(int nNumFiles)
{
	log_lock(m_pLockData);
	m_uMaxNumFiles = nNumFiles;
	log_unlock(m_pLockData);
}

void Log::setUseMsecInTimestamp(int nUseMsec)
{
	log_lock(m_pLockData);
	m_nUseMsec = nUseMsec;
	log_unlock(m_pLockData);
}

void Log::setMaxFileSizeInBytes(int nNumBytes)
{
	log_lock(m_pLockData);
	m_uMaxFileSizeInBytes = nNumBytes;
	log_unlock(m_pLockData);
}

void Log::enable(bool bEnable)
{
	m_bEnable = bEnable;
}
