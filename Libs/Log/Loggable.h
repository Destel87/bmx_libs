//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Loggable.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the Loggable class.
//! @details
//!
//*!****************************************************************************

#ifndef LOGGABLE_H
#define LOGGABLE_H

#include "Log.h"
 /*!
 * @class CLogLoggable
 * @brief classe base per tutte quelle classi a cui si desidera passare un CLog
 *	dall'esterno
 */
class Loggable
{

	public:

		/*! ***************************************************************************************
		 * void constructor
		 * Initializes with default values the internal members of the class
		 * ****************************************************************************************
		 */
		Loggable();

		/*! ***************************************************************************************
		 * Default destructor
		 * ****************************************************************************************
		 */
		virtual ~Loggable();

		/*! ***************************************************************************************
		 * Sets the logger on which log messages are executed
		 * @param pLogger pointer to an object of type Log
		 * ****************************************************************************************
		 */
		void setLogger(Log* pLogger);

		/*! ***************************************************************************************
		 * Sets the logger on which log messages are executed
		 * @param rLogger reference to an object of type Log
		 * ****************************************************************************************
		 */
		void setLogger(Log& rLogger);

		/*! ***************************************************************************************
		 * Retrieves the logger on which log messages are executed
		 * @return pointer to the logger used by this class
		 * ****************************************************************************************
		 */
		Log* getLogger();

    protected:

		/*! ***************************************************************************************
		 * Write a log message by calling the Log::logMessage() method
		 * ****************************************************************************************
		 */
		void log(int level, const char* format, ...);

    protected:

        Log* m_pLogger;

};

#endif // LOGGABLE_H
