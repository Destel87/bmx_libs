//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    I2CInterface.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the I2CInterface class.
//! @details
//!
//*!****************************************************************************

#ifndef I2CINTERFACE_H
#define I2CINTERFACE_H

#include <stdint.h>

#include "Loggable.h"
#include "Mutex.h"


/*! ***********************************************************************************************
 * MACROS
 * ************************************************************************************************
 */
#define I2CINTERFACE_VERSION	"1.0.0"
#define I2C_MAX_NAME_SIZE		1024
#define I2C_MAX_TX_BUFFER_SIZE	1024

#define I2C_RDWR_RETRY_OP		100	//Retry for read and write operations


class I2CInterface : public Loggable
{
	public:

		/*! ***************************************************************************************
		 * void constructor
		 * ****************************************************************************************
		 */
		I2CInterface();

		/*! ***************************************************************************************
		 * default destructor
		 * ****************************************************************************************
		 */
		virtual ~I2CInterface();

		/*! ***************************************************************************************
		 * Initializes the class
		 * Tries to open the file and, upon success, retrieves the bus functionalities and set the
		 * device address. After this operation has succeded, the user can perform read/write
		 * operations.
         * @param	sBusFileName name of the file that represents the interface with the low level
		 *			driver (e.g. /dev/i2c-0)
		 * @param	bMustSwapAddress
         * @param   bTenBitAddress
		 * @retval	true upon successful initialization, false otherwise
		 * ****************************************************************************************
		 */
		bool init( const char* sBusFileName, bool bMustSwapAddress = false, bool bTenBitAddress = false);

		bool setAddress(int32_t slAddress);

		void reset(void);

		void formatRegAddr( uint8_t* pRegAddrBuff, uint32_t nRegAddr, int32_t nRegAddrLen );

        bool multiReadGeneral( int32_t slDeviceAddress, uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint32_t nRegAddr,
                               int32_t nRegAddrLen);

        bool multiWriteGeneral( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint32_t nRegAddr,
                                int32_t nRegAddrLen, bool bPrintErr = true);

		bool multiRead( int32_t slDeviceAddress, uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint8_t nRegAddr);

		bool multiRead( int32_t slDeviceAddress, uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint16_t nRegAddr);

		bool multiWrite( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint8_t nRegAddr);

		bool multiWrite( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint16_t nRegAddr);

		bool writeRegister( int32_t slDeviceAddress, uint8_t nRegAddr, uint8_t nData);

		bool writeRegister( int32_t slDeviceAddress, uint16_t nRegAddr, uint8_t nData);

		bool writeRegisterGeneral( int32_t slDeviceAddress, uint8_t nData, uint32_t nRegAddr, int32_t nRegAddrLen);

		bool readRegister( int32_t slDeviceAddress, uint8_t nRegAddr, uint8_t* nData);

		bool readRegister( int32_t slDeviceAddress, uint16_t nRegAddr, uint8_t* nData);

		bool readRegisterGeneral( int32_t slDeviceAddress, uint32_t nRegAddr, int32_t nRegAddrLen, uint8_t* nData);

		bool singleWriteGeneral( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen );

    private:

        bool m_bConfigOK;
        char m_sBusFileName[I2C_MAX_NAME_SIZE];	// Device file name, e.g. /dev/i2c-0
        int32_t m_nFD;							// Device file discriptor
        int32_t m_nAddress;						// Device address
        bool m_bTenBitAddress;					// Device address
        bool m_bMustSwapAddress;
        Mutex m_nMutexWrite;					// Provides protection during write operations
        Mutex m_nMutexRead;						// Provides protection during read operations
        uint8_t m_pTxBuffer[I2C_MAX_TX_BUFFER_SIZE];

};

#endif // I2CINTERFACE_H
