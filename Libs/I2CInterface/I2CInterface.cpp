//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    I2CInterface.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implmentation for the I2CInterface class.
//! @details
//!
//*!****************************************************************************

#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <limits>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "I2CInterface.h"

/*! ***********************************************************************************************
 * STATIC FUNCTIONS
 * ************************************************************************************************
 */
bool check_if_file_exists(const char* sFilename)
{
	int32_t nFd;
	nFd = open(sFilename, O_WRONLY);
	if ( nFd < 0 )
	{
		return false;
	}
	else
	{
		close(nFd);
		return true;
	}
}

/*! ***********************************************************************************************
 * CLASS IMPLEMENTATION
 * ************************************************************************************************
 */

I2CInterface::I2CInterface()
{
	m_bConfigOK = false;
	m_sBusFileName[0] = 0;
	m_nFD = -1;
	m_bMustSwapAddress = false;
}

I2CInterface::~I2CInterface()
{
	reset();
}

bool I2CInterface::init(const char *sBusFileName, bool bMustSwapAddress , bool bTenBitAddress)
{
	m_bConfigOK = false;
	if (!check_if_file_exists(sBusFileName))
	{
		log(LOG_ERR, "Cannot open file %s", sBusFileName);
		return false;
	}

	m_nFD = open(sBusFileName, O_RDWR);
	if ( m_nFD < 0 )
	{
		log(LOG_ERR, "I2CDevice: can't' open file <%s>", sBusFileName);
		return false;
	}
	snprintf(m_sBusFileName, 1023, "%s", sBusFileName);

	// Retrieve bus functionalities
	unsigned long funcs;
	if (ioctl(m_nFD, I2C_FUNCS, &funcs) < 0)
	{
		log(LOG_ERR, "I2CDevice: cannot retrieve bus functionalities (%s)", strerror(errno));
		close(m_nFD);
		m_nFD = -1;
		return false;
	}

	m_bMustSwapAddress = bMustSwapAddress;
	m_bTenBitAddress	= bTenBitAddress;

	return true;
}

bool I2CInterface::setAddress(int32_t slAddress)
{
	if ( m_nFD < 0 )
	{
		log(LOG_ERR, "I2CInterface: file <%s> is not open", m_sBusFileName);
		m_bConfigOK = false;
		return false;
	}
	if ( slAddress != INT32_MAX )
	{
		if ( ioctl(m_nFD, I2C_SLAVE, slAddress) < 0 )
		{
			log(LOG_ERR, "I2CInterface: cannot set slave address %d (%s)", slAddress, strerror(errno));
			m_bConfigOK = false;
			return false;
		}
		m_nAddress = slAddress;
		m_bConfigOK = true;
		log(LOG_DEBUG_PARANOIC, "I2CInterface: slave address %d correctly set", m_nAddress);
	}
	return m_bConfigOK;
}

void I2CInterface::reset(void)
{
	m_bConfigOK = false;
	if ( m_nFD >= 0 )
	{
		if ( close(m_nFD) != 0 )
			log(LOG_ERR, "I2CInterface: unable to close file descriptor of device %s", m_sBusFileName);
		else
			log(LOG_INFO, "I2CInterface: file descriptor of device %s correctly closed", m_sBusFileName);
	}
	m_nFD = -1;
	m_nAddress = INT32_MAX;
}

void I2CInterface::formatRegAddr( uint8_t* pRegAddrBuff, uint32_t nRegAddr, int32_t nRegAddrLen )
{
	for ( int32_t i=0; i<(int32_t)sizeof(uint32_t); i++)
		pRegAddrBuff[i] = 0;

	switch ( nRegAddrLen )
	{
		case 1:
			pRegAddrBuff[0] = nRegAddr & 0xFF;
		break;

		case 2:
			pRegAddrBuff[0] = nRegAddr >> 8;
			pRegAddrBuff[1] = nRegAddr & 0xFF;
		break;

		case 3:
			pRegAddrBuff[0] = nRegAddr >> 16;
			pRegAddrBuff[1] = nRegAddr >> 8;
			pRegAddrBuff[2] = nRegAddr & 0xFF;
		break;

		case 4:
			pRegAddrBuff[0] = nRegAddr >> 24;
			pRegAddrBuff[1] = nRegAddr >> 16;
			pRegAddrBuff[2] = nRegAddr >> 8;
			pRegAddrBuff[3] = nRegAddr & 0xFF;
		break;

		default:
		break;
	}

	if( m_bMustSwapAddress )
	{
		uint8_t uTmp  = 0;
		switch ( nRegAddrLen )
		{
			case 1:
			break;

			case 2:
				uTmp = pRegAddrBuff[0];
				pRegAddrBuff[0] = pRegAddrBuff[1];
				pRegAddrBuff[1] = uTmp;
			break;

			case 3:
			break;

			case 4:
				uTmp = pRegAddrBuff[0];
				pRegAddrBuff[0] = pRegAddrBuff[3];
				pRegAddrBuff[3] = uTmp;
				uTmp = pRegAddrBuff[1];
				pRegAddrBuff[1] = pRegAddrBuff[2];
				pRegAddrBuff[2] = uTmp;
			break;
		}
	}
}

bool I2CInterface::multiReadGeneral( int32_t slDeviceAddress, uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint32_t nRegAddr, int32_t nRegAddrLen)
{
	setAddress(slDeviceAddress);

	if ( m_bConfigOK == false )
		return false;

	uint8_t pRegAddrBuff[4];
	formatRegAddr(pRegAddrBuff, nRegAddr, nRegAddrLen);

	uint16_t readFlags = 0;
	if (m_bTenBitAddress)
		readFlags = I2C_M_TEN;

	m_nMutexRead.lock();

	struct i2c_msg msgs[2];
	msgs[0].addr = static_cast<uint16_t>(slDeviceAddress);
	msgs[0].flags = 0;
	msgs[0].len = static_cast<uint16_t>(nRegAddrLen);
	msgs[0].buf = (uint8_t*)pRegAddrBuff;

	msgs[1].addr = static_cast<uint16_t>(slDeviceAddress);
	msgs[1].flags = I2C_M_RD | readFlags;
	msgs[1].len = static_cast<uint16_t>(nReadBufferLen);
	msgs[1].buf = (uint8_t*)pReadBuffer;

	struct i2c_rdwr_ioctl_data data;
	data.msgs = msgs;
	data.nmsgs = 2;

	int32_t nRet;
	uint32_t ulCnt = 0;
	do
	{
		nRet = ioctl(m_nFD, I2C_RDWR, &data);
		ulCnt++;
	} while ( (( nRet < 0 ) && ( errno == EREMOTEIO || errno == EIO )) && ulCnt<I2C_RDWR_RETRY_OP);

	m_nMutexRead.unlock();

	if ( nRet < 0 )
	{
		log(LOG_ERR, "I2CDevice: cannot read data");
		return false;
	}
	else
	{
		log(LOG_DEBUG_PARANOIC, "I2CDevice: ioctl read reported %d", nRet);
	}
	return true;
}

bool I2CInterface::multiWriteGeneral( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint32_t nRegAddr, int32_t nRegAddrLen, bool bPrintErr)
{
	if ( nWriteBufferLen > I2C_MAX_TX_BUFFER_SIZE )
	{
		log(LOG_ERR, "I2CInterface::multiWriteGeneral: data size exceeded maximum size");
		return false;
	}

	setAddress(slDeviceAddress);

	if ( m_bConfigOK == false )
		return false;

	uint8_t pRegAddrBuff[4];
	formatRegAddr(pRegAddrBuff, nRegAddr, nRegAddrLen);

	uint16_t writeFlags = 0;
	if (m_bTenBitAddress)
		writeFlags = I2C_M_TEN;

	m_nMutexWrite.lock();

	for (int i=0; i<(int)nRegAddrLen; i++)
	{
		m_pTxBuffer[i] = pRegAddrBuff[i];
	}

	for (int i=0; i<(int)nWriteBufferLen; i++)
	{
		m_pTxBuffer[i+nRegAddrLen] = pWriteBuffer[i];
	}

	struct i2c_msg msgs;
	msgs.addr = static_cast<uint16_t>(slDeviceAddress);
	msgs.flags = 0 | writeFlags;
	msgs.len = static_cast<uint16_t>(nRegAddrLen+nWriteBufferLen);
	msgs.buf = m_pTxBuffer;

	struct i2c_rdwr_ioctl_data data;
	data.msgs = &msgs;
	data.nmsgs = 1;

	int32_t nRet;
	uint32_t ulCnt = 0;
	do
	{
		nRet = ioctl(m_nFD, I2C_RDWR, &data);
		ulCnt++;
	} while ( (( nRet < 0 ) && ( errno == EREMOTEIO || errno == EIO )) && ulCnt<I2C_RDWR_RETRY_OP);

	m_nMutexWrite.unlock();

	if ( nRet < 0 )
	{
        if ( bPrintErr )
        {
            log(LOG_ERR, "I2CDevice: cannot write data");
        }
		return false;
	}
	else
	{
		log(LOG_DEBUG_PARANOIC, "I2CDevice: ioctl write reported %d", nRet);
	}
	return true;
}

bool I2CInterface::multiRead( int32_t slDeviceAddress, uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint8_t nRegAddr)
{
	return multiReadGeneral( slDeviceAddress, pReadBuffer, nReadBufferLen, nRegAddr, 1);
}

bool I2CInterface::multiRead( int32_t slDeviceAddress, uint8_t *pReadBuffer, uint32_t nReadBufferLen, uint16_t nRegAddr)
{
	return multiReadGeneral( slDeviceAddress, pReadBuffer, nReadBufferLen, nRegAddr, 2);
}

bool I2CInterface::multiWrite( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint8_t nRegAddr)
{
	return multiWriteGeneral( slDeviceAddress, pWriteBuffer, nWriteBufferLen, nRegAddr, 1);
}

bool I2CInterface::multiWrite( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen, uint16_t nRegAddr)
{
	return multiWriteGeneral( slDeviceAddress, pWriteBuffer, nWriteBufferLen, nRegAddr, 2);
}

bool I2CInterface::writeRegister( int32_t slDeviceAddress, uint8_t nRegAddr, uint8_t nData)
{
	return writeRegisterGeneral( slDeviceAddress, nData, nRegAddr, 1);
}

bool I2CInterface::writeRegister( int32_t slDeviceAddress, uint16_t nRegAddr, uint8_t nData)
{
	return writeRegisterGeneral( slDeviceAddress, nData, nRegAddr, 2);
}

bool I2CInterface::writeRegisterGeneral( int32_t slDeviceAddress, uint8_t nData, uint32_t nRegAddr, int32_t nRegAddrLen)
{
	return multiWriteGeneral( slDeviceAddress, &nData, 1, nRegAddr, nRegAddrLen);
}

bool I2CInterface::readRegister( int32_t slDeviceAddress, uint8_t nRegAddr, uint8_t* nData)
{
	return readRegisterGeneral( slDeviceAddress, nRegAddr, 1, nData);
}

bool I2CInterface::readRegister( int32_t slDeviceAddress, uint16_t nRegAddr, uint8_t* nData)
{
	return readRegisterGeneral( slDeviceAddress, nRegAddr, 2, nData);
}

bool I2CInterface::readRegisterGeneral( int32_t slDeviceAddress, uint32_t nRegAddr, int32_t nRegAddrLen, uint8_t* nData)
{
	return multiReadGeneral( slDeviceAddress, nData, 1, nRegAddr, nRegAddrLen);
}

bool I2CInterface::singleWriteGeneral( int32_t slDeviceAddress, uint8_t *pWriteBuffer, uint32_t nWriteBufferLen )
{
	setAddress(slDeviceAddress);

	if ( m_bConfigOK == false )
		return false;

	int32_t nRet = write(m_nFD, pWriteBuffer, nWriteBufferLen);

	if ( nRet != (int32_t)nWriteBufferLen )
	{
		log(LOG_ERR, "I2CDevice: cannot write data");
		return false;
	}
	else
	{
		log(LOG_DEBUG_PARANOIC, "I2CDevice: write reported %d", nRet);
	}
    return true;
}
