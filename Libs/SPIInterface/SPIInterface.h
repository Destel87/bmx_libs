//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SPIInterface.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the SPIInterface class.
//! @details
//!
//*!****************************************************************************

#ifndef SPIDEVICE_H
#define SPIDEVICE_H

#include "Loggable.h"
#include "Mutex.h"
#include <stdint.h>
#include <string>

using namespace std;

class SPIInterface : public Loggable
{

	public:

		/*! ***************************************************************************************
		 * void constructor
		 * ****************************************************************************************
		 */
		SPIInterface();


		/*! ***************************************************************************************
		 * default destructor
		 * ****************************************************************************************
		 */
		virtual ~SPIInterface();


		/*! ***************************************************************************************
		 * Initializes the class
		 * @param sBusFileName name of the file that represents the interface with the low level
		 *		  driver (e.g. "/dev/spidev1.0")
		 * @param nMode spi mode
		 * @param nSpeedHz spi speed
		 * @param nBitsPerWord
		 * @retval	true upon successful initialization, false otherwise
		 * ****************************************************************************************
		 */
		bool init(string sBusFileName, uint32_t nMode, uint32_t nSpeedHz, uint8_t nBitsPerWord);

		/*! ***************************************************************************************
		 * Close the interface with the driver, after this function is called the init() method
		 * shall be called again
		 * ****************************************************************************************
		 */
		void reset(void);


		/*! ***************************************************************************************
		 * Initializes a spi_ioc_transfer structure with the specified tx and and rx buffers and
		 * their size
		 * @param	pXfer pointer to a spi_ioc_transfer structure to be initialized
		 * @param	pTxBuffer the tx buffer
		 * @param	pRxBuffer the rx buffer
		 * @param	nBuffersLengthNumBytes the tx and rx buffer length in byte
		 * @retval	true if every transfer has been correctly performed, false otherwise
		 * ****************************************************************************************
		 */
		void prepareTransfer(struct spi_ioc_transfer *pXfer, uint8_t *pTxBuffer, uint8_t *pRxBuffer, size_t nBuffersLengthNumBytes);


		/*! ***************************************************************************************
		 * Performs a specified number of SPI transfers (at the moment maximum is 2)
		 * @param	pXfers pointer to an array of spi_ioc_transfer structures each of which
		 *			contains the data for a transfer to be performed
         * @param	nNumTransfers the number of transfer to perform (i.e. the xfers array size)
		 * @retval	true if every transfer has been correctly performed, false otherwise
		 * ****************************************************************************************
		 */
		bool transfer(struct spi_ioc_transfer *pXfers, int nNumTransfers);


		/*! ***************************************************************************************
		 * Performs a single transfer allowing the user to specify directly
		 * the tx, the rx buffers and their length
		 * @param	pTxBuffer the tx buffer
		 * @param	pRxBuffer the rx buffer
		 * @param	nBuffersLengthNumBytes the tx and rx buffer length in bytes
		 * @retval	true if the transfer has been correctly performed, false otherwise
		 * ****************************************************************************************
		 */
		bool singleTransfer(uint8_t *pTxBuffer, uint8_t *pRxBuffer, size_t nBuffersLengthNumBytes);

		string getSPIname() { return m_sBusFileName; }

    private:

        bool	 m_bConfigOK;
        string	 m_sBusFileName;			// Device file name, e.g. /dev/spidev1.0
        int32_t	 m_nFD;					// Device file discriptor
        uint32_t m_nMode;
        uint32_t m_nSpeedHz;
        uint8_t  m_nBitsPerWord;
        Mutex	 m_nMutex;

};

#endif // SPIDEVICE_H
