//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    SPIInterface.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the SPIInterface class.
//! @details
//!
//*!****************************************************************************

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <linux/spi/spidev.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <cstring>
#include <assert.h>

#include "SPIInterface.h"


/*! ***********************************************************************************************
 * STATIC FUNCTIONS
 * ************************************************************************************************
 */
bool checkIfFileExists(const char* sFilename)
{
	int32_t nFd;
	nFd = open(sFilename, O_WRONLY);

	if ( nFd < 0 )
	{
		return false;
	}
	else
	{
		close(nFd);
		return true;
	}
}


/*! ***********************************************************************************************
 * CLASS IMPLEMENTATION
 * ************************************************************************************************
 */
SPIInterface::SPIInterface()
{
	m_bConfigOK = false;
	m_sBusFileName.clear();
	m_nFD = -1;
	m_nMode = UINT32_MAX;
	m_nSpeedHz = UINT32_MAX;
	m_nBitsPerWord = UINT8_MAX;
}

SPIInterface::~SPIInterface()
{
	reset();
}


bool SPIInterface::init(string sBusFileName, uint32_t nMode, uint32_t nSpeedHz, uint8_t nBitsPerWord)
{
	m_bConfigOK = false;
	if (!checkIfFileExists(sBusFileName.c_str()))
	{
		log(LOG_ERR, "SPIDevice: file %s does not exist", sBusFileName.c_str());
		return false;
	}

	// Try to open bus file name
	m_nFD = open(sBusFileName.c_str(), O_RDWR);
	if ( m_nFD < 0 )
	{
		log(LOG_ERR, "SPIDevice: unable to open file <%s>", sBusFileName.c_str());
		return false;
	}
	m_sBusFileName = sBusFileName;

	log(LOG_INFO, "SPIDevice: file <%s> correctly opened", sBusFileName.c_str());

	// Set spi mode
	int ret = ioctl(m_nFD, SPI_IOC_WR_MODE32, &nMode);
	if ( ret == -1 )
	{
		log(LOG_ERR, "SPIDevice: can't set spi mode");
		reset();
		return false;
	}
	uint32_t nTmp = UINT32_MAX;
	ret = ioctl(m_nFD, SPI_IOC_RD_MODE32, &nTmp);
	if (ret == -1)
	{
		log(LOG_ERR, "SPIDevice: can't get spi mode");
		reset();
		return false;
	}
	log(LOG_INFO, "SPIDevice: set SPI mode to %#x", nTmp);
	m_nMode = nMode;

	// Set bits per word
	ret = ioctl(m_nFD, SPI_IOC_WR_BITS_PER_WORD, &nBitsPerWord);
	if ( ret == -1 )
	{
		log(LOG_ERR, "SPIDevice: can't set bits per word");
		reset();
		return false;
	}
	uint8_t nTmp2 = UINT8_MAX;
	ret = ioctl(m_nFD, SPI_IOC_RD_BITS_PER_WORD, &nTmp2);
	if ( ret == -1 )
	{
		log(LOG_ERR, "SPIDevice: can't get bits per word");
		reset();
		return false;
	}
	log(LOG_INFO, "SPIDevice: set bit per word to %d", nTmp2);
	m_nBitsPerWord = nBitsPerWord;

	// Set max speed hz
	ret = ioctl(m_nFD, SPI_IOC_WR_MAX_SPEED_HZ, &nSpeedHz);
	if ( ret == -1 )
	{
		log(LOG_ERR, "SPIDevice: can't set bus speed");
		reset();
		return false;
	}
	ret = ioctl(m_nFD, SPI_IOC_RD_MAX_SPEED_HZ, &nTmp);
	if ( ret == -1 )
	{
		log(LOG_ERR, "SPIDevice: can't get bus speed");
		reset();
		return false;
	}
	log(LOG_INFO, "SPIDevice: set bus speed to %d Hz", nTmp);
	m_nSpeedHz = nSpeedHz;

	m_bConfigOK = true;
	return true;

}

void SPIInterface::reset(void)
{
	if ( m_nFD >= 0 )
		close(m_nFD);
	m_bConfigOK = false;
	m_sBusFileName.clear();
	m_nFD = -1;
	m_nMode = UINT32_MAX;
	m_nSpeedHz = UINT32_MAX;
	m_nBitsPerWord = UINT8_MAX;
}

void SPIInterface::prepareTransfer(struct spi_ioc_transfer *pXfer, uint8_t *pTxBuffer, uint8_t *pRxBuffer, size_t nBuffersLengthNumBytes)
{
	memset(pXfer, 0, sizeof(*pXfer));
	pXfer->tx_buf = (__u64)pTxBuffer;
	pXfer->rx_buf = (__u64)pRxBuffer;
	pXfer->len = (__u32)nBuffersLengthNumBytes;
	pXfer->delay_usecs = (__u16)1;
	pXfer->speed_hz = m_nSpeedHz;
	pXfer->bits_per_word = m_nBitsPerWord;
}

bool SPIInterface::singleTransfer(uint8_t *pTxBuffer, uint8_t *pRxBuffer, size_t nBuffersLengthNumBytes)
{
	if ( !m_bConfigOK ) return false;
	struct spi_ioc_transfer tr;
	prepareTransfer(&tr, pTxBuffer, pRxBuffer, nBuffersLengthNumBytes);
	return transfer(&tr, 1);
}

bool SPIInterface::transfer(struct spi_ioc_transfer *pXfers, int nNumTransfers)
{
	if ( !m_bConfigOK ) return false;
	bool bRetValue = true;
	int nExpectedTransfers = 0;
	int nPerformedTransfers = INT32_MAX;

	for ( int i = 0; i < nNumTransfers; i++ )
	{
		nExpectedTransfers += pXfers[i].len;
	}

	m_nMutex.lock();
	switch ( nNumTransfers )
	{
		case 1:
			nPerformedTransfers = ioctl(m_nFD, SPI_IOC_MESSAGE(1), pXfers);
			break;
		case 2:
			nPerformedTransfers = ioctl(m_nFD, SPI_IOC_MESSAGE(2), pXfers);
			break;
		default:
			log(LOG_ERR, "SPIDevice: invalid number of transfers");
			bRetValue = false;
			break;
	}
	m_nMutex.unlock();

	if ( nPerformedTransfers != nExpectedTransfers )
	{
		log(LOG_ERR, "SPIDevice: can't send spi messages");
		bRetValue = false;
	}
	return bRetValue;
}



