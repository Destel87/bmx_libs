//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TARUtilities.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the TARUtilities class.
//! @details
//!
//*!****************************************************************************

#ifndef TARUTILITIES_H
#define TARUTILITIES_H

#include <vector>
#include <string>
#include <sys/stat.h>

#include "Log.h"
#include "TARUtilitiesCommonInclude.h"

#define TAR_CONTAINER_INFO	"tar_container_file.txt"


using namespace std;

class TARutilities
{
	public:

		TARutilities();

		TARutilities(Log* pLogger);

		virtual ~TARutilities();

		bool setLogger(Log* pLogger);

		/*! ***************************************************************************************
		 * @brief  createArchive method used to create a tar archive file [sArchiveFileName] from
		 *		   the list of file name [vsArchiveFileList]
		 * @param  vsArchiveFileList list of fileName
		 * @param  sArchiveFileName name of archive file
		 * @return true in case of success, false otherwise
		 * ****************************************************************************************
		 */
		bool createArchive(vector<string>& vsArchiveFileList, string& sArchiveFileName);

		/*! ***************************************************************************************
		 * @brief  openArchive method used to create a directory named sTarFileName (without .tar
		 *		   extension), containing all the file inside the tar file named sTarFileName
		 * @param  sTarFileName archive file name
		 * @return true in case of success, false otherwise
		 * ****************************************************************************************
		 */
		bool openArchive(string& sTarFileName);

		/*! ***************************************************************************************
		 * @brief  viewArchive method used to visualize the list of file contain inside the tar
		 *		   archive file. Create in a folder named sTarFileName (without .tar extension) a
		 *		   .txt file with the list of files inside.
		 * @param  sTarFileName tar file name
		 * @return true in case of success, false otherwise
		 * ****************************************************************************************
		 */
		bool viewArchive(string& sTarFileName);

    protected:

        Log*		m_pLogger;
        static int	m_iTmpBuf[16384 / sizeof(int)];
        bool		m_bConfigOk;

    private:

		bool tarCommand(int liElemNum, char** argv);
		bool doTarCmd(int liElemNum, char** ppArgv, void* scratch, size_t scratchSize);
		void initStructTarOpt(structTarOpt* opt);
		bool addFileToTarOpt(structTarOpt* opt, const char* rgFileName);
		void freeTarOpt(structTarOpt* opt);
		bool expandFilesTarOpt(structTarOpt* opt);
		bool openTarIoForRead(structTarIo* io, const char* pFileName, void* buf, size_t size);
		uTarBlock* readBlockTarIo(structTarIo* io);
		bool refillTarIo(structTarIo* io);
		bool decodeHeader(structTarOpt* opt, uTarBlock* hdr, struct stat* statBuf);
		unsigned int decodeOctal(const char* pcStr, int liLen);
		unsigned int computeChecksum(uTarBlock* hdr);
		bool copyTarIoToFd(structTarIo* io, int liFd, size_t size);
		uTarBlock* getReadBlocksTarIo(structTarIo* io, int* pNum);
		bool skipTarIo(structTarIo* io, size_t size);
		bool extractTarFile(structTarOpt* opt, structTarIo* io, const char* pcWhere);
		bool closeTarIo(structTarIo* io);
		bool flushTarIo(structTarIo* io);
		bool openTarIoForWrite(structTarIo* io, const char* pcFileName, void* pBuf, size_t size);
		bool createTarFile(structTarOpt* opt, structTarIo* io);
		bool insertFileOrDir(structTarOpt* opt, structTarIo* io, const char* pcFileName);
		bool encodeHeader(uTarBlock* hdr, const struct stat* statBuf, const char* pcFileName);
		bool encodeOctal(char* pStr, int liLen, unsigned int uliVal);
		bool writeTarIoBlock(structTarIo* io, uTarBlock* block);
		bool copyTarIoFromFd(structTarIo* io, int liFd, size_t size);
		uTarBlock* getTarIoWriteBlocks(structTarIo* io, int* piNum);
};

#endif // TARUTILITIES_H
