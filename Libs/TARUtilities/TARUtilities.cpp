//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TARUtilities.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the TARUtilities class.
//! @details
//!
//*!****************************************************************************

#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <dirent.h>

#include "TARUtilities.h"


#define MAX_FILES	128
#define NOOP		(void)0


int TARutilities::m_iTmpBuf[16384 / sizeof(int)];

TARutilities::TARutilities()
{
	m_pLogger = 0;
	m_bConfigOk = false;
}

TARutilities::TARutilities(Log* pLogger)
{
	m_bConfigOk = false;

	if ( pLogger != nullptr )
	{
		m_pLogger = pLogger;
		m_bConfigOk = true;
	}
}

TARutilities::~TARutilities()
{

}

bool TARutilities::setLogger(Log* pLogger)
{
	m_bConfigOk = false;
	if ( pLogger == 0 )	return false;

	m_pLogger = pLogger;
	m_bConfigOk = true;

	return true;
}

bool TARutilities::createArchive(vector<string>& vsArchiveFileList, string& sArchiveFileName)
{
	if ( ! m_bConfigOk )	return false;

	if ( vsArchiveFileList.size() == 0 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::createArchive: empty archive file list");
		return false;
	}
	if ( sArchiveFileName.length() == 0 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::createArchive: empty archive file name");
		return false;
	}

	char* prgTarArgv[MAX_FILES] = {};
	int liParametersCount = 0;

	// prgTarArgv is an array of pointer to 1) command to execute, 2) .tar filename,
	// 3...n) names of the file inside .tar archive
	prgTarArgv[0] = (char*)"-cvf";
	prgTarArgv[1] = (char*)sArchiveFileName.c_str();

	while ( vsArchiveFileList.size()-liParametersCount )
	{
		prgTarArgv[liParametersCount+2] = (char*)vsArchiveFileList[liParametersCount].c_str();
		liParametersCount++;
	}

	if ( ! tarCommand(liParametersCount+2, prgTarArgv) )
	{
		return false;
	}

	return true;
}

bool TARutilities::openArchive(string& sTarFileName)
{
	if ( ! m_bConfigOk )	return false;

	if ( sTarFileName.length() == 0 ) return -1;

	char* prgTarArgv[MAX_FILES] = {};
	int liParametersCount = 2;

	// prgTarArgv is an array of pointer to 1) command to execute, 2) .tar filename,
	// 3...n) names of the file inside .tar archive
	prgTarArgv[0] = (char*)"-xvf";
	prgTarArgv[1] = (char*)sTarFileName.c_str();

	if ( ! tarCommand(liParametersCount,prgTarArgv) )
	{
		return false;
	}

	return true;
}

bool TARutilities::viewArchive(string& sTarFileName)
{
	if ( ! m_bConfigOk )	return false;

	if ( sTarFileName.length() == 0 ) return -1;

	char* prgTarArgv[MAX_FILES] = {};
	int liParametersCount = 2;

	prgTarArgv[0] = (char*)"-tvf";
	prgTarArgv[1] = (char*)sTarFileName.c_str();

	if ( ! tarCommand(liParametersCount,prgTarArgv) )
	{
		return false;
	}

	return true;
}

bool TARutilities::tarCommand(int liElemNum, char** argv)
{
	if ( argv == 0 )		return false;

	bool ret;
	int liBuffSize = sizeof(m_iTmpBuf);
	ret = doTarCmd(liElemNum, argv, &m_iTmpBuf, liBuffSize);

	return ret;
}

bool TARutilities::doTarCmd(int liElemNum, char** ppArgv, void* scratch, size_t scratchSize)
{
	if ( ppArgv == 0 )		return false;
	if ( scratch == 0 )		return false;

	structTarOpt opt;
	structTarIo io;
	memset(&io, '\0', sizeof(io));
	initStructTarOpt(&opt);

	if ( liElemNum == 0)
	{
		m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: error using tar [cxtvfp] tarfile [files]");
		return false;
	}

	bool bFirstArg = true;
	while ( ppArgv[0] )
	{
		const char* pcArg = ppArgv[0];

		if ( pcArg[0] == '-' )
		{
			pcArg++;
		}
		else if ( liElemNum && bFirstArg )
		{
			NOOP;
		}
		else
		{
			break;
		}

		bFirstArg = false;
		while ( pcArg[0] )
		{
			switch ( pcArg[0] )
			{
				case 'c':	opt.bCreate = true;		break;
				case 'x':	opt.bExtract = true;	break;
				case 't':	opt.bTell = true;		break;
				case 'v':	opt.bVerbose = true;	break;
				case 'f':	opt.bFile = true;		break;
				case 'p':	opt.bPreserve = true;   break;

				case 'z':
					m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: unsupported option %c", pcArg[0]);
					return false;
				break;

				default:
					m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: unknown option %c", pcArg[0]);
					return false;
				break;
			}
			pcArg++;
		}
		liElemNum--;
		ppArgv++;
	}

	const char* pTarFileName = 0;

	if ( opt.bFile )
	{
		if ( liElemNum == 0 )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: too few arguments");
			return false;
		}
		pTarFileName = ppArgv[0];
		liElemNum--;
		ppArgv++;
	}
	else
	{
		m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: 'f' is the only supported mode of operation");
		return false;
	}

	while (liElemNum)
	{
		if ( addFileToTarOpt(&opt, *ppArgv) == false )
		{
			freeTarOpt(&opt);
			return false;
		}

		liElemNum--;
		ppArgv++;
	}

	if ( 0 < liElemNum )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: too many arguments");
		freeTarOpt(&opt);
		return false;
	}

	if ( (opt.bExtract + opt.bCreate + opt.bTell) != 1 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: exactly one of 'c', 'x', or 't' must be specified");
		freeTarOpt(&opt);
		return false;
	}

	// check if is correct.. this should be for integrity only
	if ( expandFilesTarOpt(&opt) == false )
	{
		freeTarOpt(&opt);
		return false;
	}

	string sTarDirectoryName("");
	bool bRet = true;

	if ( opt.bExtract || opt.bTell )
	{
		bRet = openTarIoForRead(&io, pTarFileName, scratch, scratchSize);
		if ( bRet )
		{
			sTarDirectoryName = pTarFileName;

			size_t found = sTarDirectoryName.find(".tar");
			if ( found != std::string::npos )
			{
				sTarDirectoryName.erase(found,4);
				mkdir(sTarDirectoryName.c_str(),S_IRWXU|S_IRWXG|S_IRWXO);
				bRet = extractTarFile(&opt, &io,sTarDirectoryName.c_str());
			}
			else
			{
				bRet = false;
			}
		}
		if ( bRet == true )
		{
			bRet = closeTarIo(&io);
		}
	}
	else
		if  ( opt.bCreate )
		{
			bRet = openTarIoForWrite(&io, pTarFileName, scratch, scratchSize);
			if ( bRet == true )
			{
				bRet = createTarFile(&opt, &io);
			}
			if ( bRet == true )
			{
				bRet = closeTarIo(&io);
			}
		}
		else
		{
			m_pLogger->log(LOG_ERR, "TARutilities::doTarCmd: unsupported mode of operation");
			freeTarOpt(&opt);
			return false;
		}

	freeTarOpt(&opt);
	return bRet;
}

void TARutilities::initStructTarOpt(structTarOpt* opt)
{
	if ( opt )
	{
		memset(opt, '\0', sizeof(opt[0]));
	}
}

bool TARutilities::addFileToTarOpt(structTarOpt* opt, const char* rgFileName)
{
	if ( opt == 0 )			return false;
	if ( rgFileName == 0 )	return false;

	if ( opt->liNumFiles == opt->liMaxFiles )
	{
		int liNewMaxFiles = 2*opt->liMaxFiles + 3;
		const char** ppNewFiles = (const char **)realloc(opt->ppFiles, liNewMaxFiles*sizeof(opt->ppFiles[0]));

		if ( ppNewFiles == 0 )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::addFileToTarOpt: too many args");
			return false;
		}
		opt->liMaxFiles = liNewMaxFiles;
		opt->ppFiles = ppNewFiles;
	}
	opt->ppFiles[opt->liNumFiles++] = rgFileName;
	return true;
}

void TARutilities::freeTarOpt(structTarOpt* opt)
{
	if ( opt == 0 )	return;

	if ( opt->ppFiles )
	{
		free(opt->ppFiles);
		memset(opt, '\0', sizeof(opt[0]));
	}
}

bool TARutilities::expandFilesTarOpt(structTarOpt* opt)
{
	if ( opt == 0 )		return false;

	for (int i = 0; i < opt->liNumFiles; i++)
	{
		int liGlobArg = GLOB_NOMAGIC;
		const char* pcPathName = opt->ppFiles[i];
		if ( opt->bDidGlob )
		{
			liGlobArg |= GLOB_APPEND;
		}
		else
		{
			opt->bDidGlob = true;
		}

		if ( glob(pcPathName, liGlobArg, 0, &opt->globData) == GLOB_NOMATCH )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::expandFilesTarOpt: tar %s no match", pcPathName);
			return false;
		}
		if ( opt->globData.gl_pathc == 0 )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::expandFilesTarOpt: no such file or directory");
			return false;
		}
	}

	opt->liNumFiles = 0;
	for (uint16_t i = 0; i < opt->globData.gl_pathc; ++i)
	{
		addFileToTarOpt(opt, opt->globData.gl_pathv[i]);
	}

	return true;
}

bool TARutilities::openTarIoForRead(structTarIo* io, const char* pFileName, void* buf, size_t size)
{
	if ( io == 0 )			return false;
	if ( buf == 0 )			return false;
	if ( pFileName == 0 )	return false;

	// base name of the archive
	io->base = (uTarBlock*)buf;
	io->rptr = io->wptr = io->base;
	io->end = io->base + ( size / sizeof(io->base[0]) );
	io->size = ( io->end - io->base ) * sizeof(io->base[0]);
	io->bIsWrite = false;
	io->bGotError = false;

	io->liFd = open(pFileName, O_RDONLY);
	if ( io->liFd == -1 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::openTarIoForRead: error opening file");
		io->bGotError = true;
		return false;
	}

	return true;
}

bool TARutilities::extractTarFile(structTarOpt* opt, structTarIo* io, const char* pcWhere)
{
	if ( io == 0 )			return false;
	if ( opt == 0 )			return false;
	if ( pcWhere == 0 )	return false;

	uTarBlock* hdr;
	FILE* fileTarContainer;
	int liRet;
	char rgcTarPathName[TARNAMESIZE];

	memset(rgcTarPathName, 0x00,TARNAMESIZE);

	if ( opt->liNumFiles )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::extractTarFile: selective extraction is not supported");
		return false;
	}

	string sTarContainerFileName;
	sTarContainerFileName += pcWhere;
	sTarContainerFileName += "/";
	sTarContainerFileName += TAR_CONTAINER_INFO;

	fileTarContainer = fopen(sTarContainerFileName.c_str(), "w+");

	liRet = chdir(pcWhere);
	if ( liRet == -1 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::extractTarFile: nable to change dir to %s", pcWhere);
		return false;
	}

	while (1)
	{
		struct stat statBuf;
		hdr = readBlockTarIo(io);
		snprintf(rgcTarPathName, TARNAMESIZE, "%s/%s", pcWhere, hdr->cName);

		if ( hdr == 0 )
		{
			break;
		}
		if ( ! decodeHeader(opt, hdr, &statBuf) )
		{
			return false;
		}

		if (statBuf.st_mode)
		{
			/* indicates not empty of dir */
			if ( opt->bVerbose )
			{
				m_pLogger->log(LOG_INFO, "TARutilities::extractTarFile: path is %s", rgcTarPathName);
				fprintf(fileTarContainer, "%s\n", rgcTarPathName);
			}
			if ( opt->bExtract )
			{
				bool bRet;
				int liFd;

				liFd = creat(hdr->cName, statBuf.st_mode);
				if (liFd == -1)
				{
					m_pLogger->log(LOG_ERR, "TARutilities::extractTarFile: unable to create and open file (%s)", hdr->cName);
					return false;
				}

				bRet = copyTarIoToFd(io, liFd, statBuf.st_size);
				/* hdr is not valid after the previous call */
				hdr = 0;
				if ( bRet && opt->bPreserve )
				{
					struct timeval utime[2];
					utime[0].tv_sec = statBuf.st_atime;
					utime[0].tv_usec = 0;
					utime[1].tv_sec = statBuf.st_mtime;
					utime[1].tv_usec = 0;
					if ( futimes(liFd, &(utime[0])) == -1 )
					{
						m_pLogger->log(LOG_ERR, "TARutilities::extractTarFile: error setting file times");
						bRet = false;
					}
				}

				if ( bRet && opt->bPreserve )
				{
					if ( fchown(liFd, statBuf.st_uid, statBuf.st_gid) == -1 )
					{
						m_pLogger->log(LOG_ERR, "TARutilities::extractTarFile: error setting file owner");
						bRet = false;
					}
				}
				if ( close(liFd) == -1 )
				{
					m_pLogger->log(LOG_ERR, "TARutilities::extractTarFile: error closing file");
					bRet = false;
				}

				if ( bRet == false )
				{
					return false;
				}
			}
			else
			{
				if ( statBuf.st_size )
				{
					if ( ! skipTarIo(io, statBuf.st_size) )
					{
						return false;
					}
				}
			}
		}
	}

	liRet = chdir("/home/root");
	if ( liRet )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::extractTarFile: unable to change dir to %s", pcWhere);
		return false;
	}

	fclose(fileTarContainer);

	return !(io->bGotError);
}

uTarBlock* TARutilities::readBlockTarIo(structTarIo* io)
{
	if ( io == 0 )	return 0;

	if ( io->rptr == io->wptr )
	{
		if ( ! refillTarIo(io) )
		{
			return 0;
		}
	}
	return io->rptr++;
}

bool TARutilities::refillTarIo(structTarIo* io)
{
	if ( io == 0 )	return false;

	int liBytesRead, liPaddingBytes;
	liBytesRead = read(io->liFd, io->base, io->size);

	if ( liBytesRead == -1 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::refillTarIo: error reading file");
		io->bGotError = true;
		return false;
	}
	if ( liBytesRead == 0 )	return false;

	io->rptr = io->base;
	io->wptr = io->rptr + ((liBytesRead + sizeof(io->base[0]) - 1) / sizeof(io->base[0]));
	liPaddingBytes = ((io->wptr - io->rptr) * sizeof(io->base[0])) - liBytesRead;

	if ( liPaddingBytes )
	{
		memset(((char*)io->rptr)+liBytesRead, '\0', liPaddingBytes);
	}
	return true;
}

bool TARutilities::decodeHeader(structTarOpt* opt, uTarBlock* hdr, struct stat* statBuf)
{
	if ( opt == 0 )			return false;
	if ( hdr == 0 )			return false;
	if ( statBuf == 0 )		return false;

	char* cPath = &(hdr->cName[0]);
	unsigned int liCheckSum;

	memset(statBuf, '\0', sizeof(statBuf[0]));

	if (cPath[0] == '\0')
	{
		/* empty entry */
		return true;
	}

	{ // Limit variable existance only here
		char* cSlash = cPath;
		while ( (cSlash = strchr(cSlash, '/')) != 0 )
		{
			if ( opt->bExtract )
			{
				struct stat thisStatBuf;
				cSlash[0] = '\0';
				if ( stat (cPath, &thisStatBuf) == -1 )
				{
					if ( mkdir(cPath, 0777) == -1 )
					{
						return false;
					}
				}
				else if ( ! S_ISDIR(thisStatBuf.st_mode) )
				{
					return false;
				}
				cSlash[0] = '/';
			}
			cSlash++;

			if ( cSlash[0] == '\0' )
			{
				/* just a directory */
				return true;
			}
		}
	}

	assert(cPath[0] != '\0');

	statBuf->st_mode  = decodeOctal(&(hdr->cMode[0]), sizeof(hdr->cMode));
	statBuf->st_uid	  = decodeOctal(&(hdr->cUid[0]), sizeof(hdr->cUid));
	statBuf->st_gid   = decodeOctal(&(hdr->cGid[0]), sizeof(hdr->cGid));
	statBuf->st_size  = decodeOctal(&(hdr->cSize[0]), sizeof(hdr->cSize));
	statBuf->st_mtime = decodeOctal(&(hdr->cMtime[0]), sizeof(hdr->cMtime));

	liCheckSum = decodeOctal(&(hdr->cCheckSum[0]), sizeof(hdr->cCheckSum));

	if ( opt->bPreserve == false )
	{
		statBuf->st_mode &= (S_ISVTX | S_IRWXU | S_IRWXG | S_IRWXO);
		statBuf->st_mode &= ~(S_ISUID | S_ISGID);
	}
	statBuf->st_atime = statBuf->st_ctime = statBuf->st_mtime;

	if ( computeChecksum(hdr) != liCheckSum)
	{
		m_pLogger->log(LOG_ERR, "TARutilities::decodeHeader:bad checksum for: %s\n", hdr->cName);
		return false;
	}

	return true;
}

unsigned int TARutilities::decodeOctal(const char* pcStr, int liLen)
{
	if ( pcStr == 0 )		return 0;	// dummy value

	unsigned int uliVal = 0;
	const char* pcEnd = (pcStr + liLen);

	while ( (pcStr < pcEnd) && ((pcStr[0] == ' ') || (pcStr[0] == '0')) )
	{
		pcStr++;
	}

	while ( (pcStr < pcEnd) && (('0' <= pcStr[0]) && (pcStr[0] <= '7')) )
	{
		uliVal = (uliVal << 3);
		uliVal |= (pcStr[0] - '0');
		pcStr++;
	}

	return uliVal;
}

unsigned int TARutilities::computeChecksum(uTarBlock* hdr)
{
	if ( hdr == 0 )		return 0;	// dummy value

	unsigned int ulival = 256;
	const char *pcPtr, *pcEnd;

	pcPtr = (const char *)hdr;
	pcEnd = (const char *)&hdr->cCheckSum;

	while (pcPtr < pcEnd)
	{
		ulival += (*(pcPtr++) & 0xff);
	}

	pcPtr += sizeof(hdr->cCheckSum);
	pcEnd = (const char *)(hdr+1);

	while (pcPtr < pcEnd)
	{
		ulival += (*(pcPtr++) & 0xff);
	}

	return ulival;
}

bool TARutilities::copyTarIoToFd(structTarIo* io, int liFd, size_t size)
{
	if ( io == 0 )		return false;

	uTarBlock* blocks;

	while (size)
	{
		int liBlocksNum;
		size_t liBytesToWrite, liBytesWritten;
		blocks = getReadBlocksTarIo(io, &liBlocksNum);
		if ( blocks == 0 )
		{
			break;
		}
		liBytesToWrite = liBlocksNum * sizeof(blocks[0]);

		if ( size < liBytesToWrite )
		{
			liBytesToWrite = size;
		}
		liBytesWritten = write(liFd, blocks, liBytesToWrite);
		if ( liBytesWritten != liBytesToWrite )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::CopyToFdTarIo: write failes");
			io->bGotError = true;
			break;
		}
		size -= liBytesWritten;
		io->rptr += (liBytesWritten + sizeof(blocks[0]) - 1) / sizeof(blocks[0]);
	}

	return ( ! io->bGotError );
}

uTarBlock* TARutilities::getReadBlocksTarIo(structTarIo* io, int* pNum)
{
	if ( io == 0 )			return 0;
	if ( pNum == 0 )		return 0;

	if ( io->rptr == io->wptr )
	{
		if ( ! refillTarIo(io) )
		{
			*pNum = 0;
			return 0;
		}
	}
	*pNum = (io->wptr - io->rptr);

	return io->rptr;
}

bool TARutilities::skipTarIo(structTarIo* io, size_t size)
{
	if ( io == 0 )			return false;

	int ntr = (size + sizeof(io->base[0]) - 1) / sizeof(io->base[0]);
	int nr = (io->wptr - io->rptr);

	if ( ntr < nr )
	{
		io->rptr += ntr;
		return true;
	}

	io->rptr += nr;
	assert( io->rptr == io->wptr );
	ntr -= nr;

	if ( lseek(io->liFd, ntr*sizeof(io->base[0]), SEEK_CUR) == -1)
	{
		m_pLogger->log(LOG_ERR, "TARutilities::skipTarIo: error seeking in file");
		io->bGotError = true;
		return false;
	}

	return true;
}

bool TARutilities::closeTarIo(structTarIo* io)
{
	if ( io == 0 )			return false;

	int liRes;

	if ( io->bIsWrite)
	{
		flushTarIo(io);
	}

	liRes = close(io->liFd);

	return ( liRes == -1 ) ? false : true;
}

bool TARutilities::flushTarIo(structTarIo* io)
{
	if ( io == 0 )			return false;

	int liBytesToWrite, liBytesWritten;
	liBytesToWrite = (io->wptr - io->base) * sizeof(io->base[0]);
	liBytesWritten = write(io->liFd, io->base, liBytesToWrite);

	if ( liBytesWritten == -1 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::flushTarIo: error writing");
		io->bGotError = true;
		return false;
	}

	if ( ! liBytesWritten )
	{
		return false;
	}

	if ( liBytesWritten != liBytesToWrite )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::flushTarIo: partial write");
		io->bGotError = true;
		return false;
	}

	io->wptr = io->base;
	return true;
}

bool TARutilities::openTarIoForWrite(structTarIo* io, const char* pcFileName, void* pBuf,	size_t size)
{
	if ( io == 0 )					return false;
	if ( pcFileName == 0 )			return false;
	if ( pBuf == 0 )				return false;

	io->base = (uTarBlock*)pBuf;
	io->rptr = io->wptr = io->base;
	io->end = io->base + (size / sizeof(io->base[0]));
	io->size = (io->end - io->base) * sizeof(io->base[0]);
	io->bIsWrite = true;
	io->bGotError = false;

	io->liFd = open(pcFileName, (O_WRONLY | O_CREAT | O_TRUNC), 0666);
	if (io->liFd == -1)
	{
		m_pLogger->log(LOG_ERR, "TARutilities::openTarIoForWrite: error opening %s", pcFileName);
		return false;
	}
	return true;
}

bool TARutilities::createTarFile(structTarOpt* opt, structTarIo* io)
{
	if ( opt == 0 )					return false;
	if ( io == 0 )					return false;

	if ( opt->liNumFiles == 0 )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::createTarFile: no files specified for creation");
		return false;
	}

	for (int i = 0; i < opt->liNumFiles; i++)
	{
		const char * pcFileName = opt->ppFiles[i];

		if ( ! insertFileOrDir(opt, io, pcFileName) )
		{
			return false;
		}
	}

	return true;
}

bool TARutilities::insertFileOrDir(structTarOpt* opt, structTarIo* io, const char* pcFileName)
{
	if ( opt == 0 )					return false;
	if ( io == 0 )					return false;
	if ( pcFileName == 0 )					return false;

	struct stat statBuf;

	if ( lstat(pcFileName, &statBuf) == -1)
	{
		m_pLogger->log(LOG_ERR, "TARutilities::insertFileOrDir: unable to get %s attributes", pcFileName);
		return false;
	}

	if ( S_ISLNK(statBuf.st_mode) )
	{
		m_pLogger->log(LOG_ERR, "TARutilities::insertFileOrDir: links not supported");
		return false;
	}
	else if ( S_ISDIR(statBuf.st_mode) )
	{
		DIR* dir;
		struct dirent* entry;
		int liFileNameLen;

		liFileNameLen = strlen(pcFileName);
		dir = opendir(pcFileName);
		if ( dir == 0 )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::insertFileOrDir: unable to open directory");
			return false;
		}

		while ( (entry = readdir(dir)) != 0 )
		{
			/* skip "." and ".." */
			if ( entry->d_name[0] == '.' )
			{
				if ( entry->d_name[1] == '\0' )
				{
					continue;
				}
				if ( (entry->d_name[1] == '.') && (entry->d_name[2] == '\0') )
				{
					continue;
				}
			}
			{
				char* pcEntName;
				pcEntName = (char*)malloc(liFileNameLen+1+strlen(entry->d_name)+1);

				if ( pcEntName )
				{
					bool bRes;
					strcpy(pcEntName, pcFileName);
					strcpy(pcEntName + liFileNameLen, "/");
					strcpy(pcEntName + liFileNameLen + 1, entry->d_name);
					bRes = insertFileOrDir(opt, io, pcEntName);
					free(pcEntName);
					if ( ! bRes )
					{
						return false;
					}
				}
				else
				{
					m_pLogger->log(LOG_ERR, "TARutilities::insertFileOrDir: failed new()");
					return false;
				}
			}
		}
		closedir(dir);
	}
	else if ( S_ISREG(statBuf.st_mode) )
	{
		uTarBlock hdr;
		int liFd;
		bool bRes;

		if ( ! encodeHeader(&hdr, &statBuf, pcFileName) )
		{
			return false;
		}

		liFd = open(pcFileName, O_RDONLY);
		if ( liFd == -1 )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::insertFileOrDir: unable to open %s", pcFileName);
			return false;
		}

		if ( opt->bVerbose )
		{
			m_pLogger->log(LOG_INFO, "TARutilities::insertFileOrDir: filename is %s", pcFileName);
		}

		bRes = writeTarIoBlock(io, &hdr);
		if ( (bRes && copyTarIoFromFd(io, liFd, statBuf.st_size)) == false )
		{
			bRes = false;
		}

		if ( close(liFd) == -1 )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::insertFileOrDir: error closing file descriptor");
			bRes = false;
		}
		if (bRes == false)		return false;
	}
	else
	{
		m_pLogger->log(LOG_ERR, "TARutilities::insertFileOrDir: unknown file type: %s\n", pcFileName);
		return false;
	}

	return true;
}

bool TARutilities::encodeHeader(uTarBlock* hdr, const struct stat* statBuf, const char* pcFileName)
{
	if ( hdr == 0 )					return false;
	if ( statBuf == 0 )				return false;
	if ( pcFileName == 0 )			return false;

	unsigned int uliCheckSum;

	memset(hdr, '\0', sizeof(hdr[0]));

	strncpy(hdr->cName, pcFileName, sizeof(hdr->cName));
	encodeOctal(&(hdr->cMode[0]), sizeof(hdr->cMode), statBuf->st_mode);
	encodeOctal(&(hdr->cUid[0]), sizeof(hdr->cUid), statBuf->st_uid);
	encodeOctal(&(hdr->cGid[0]), sizeof(hdr->cGid), statBuf->st_gid);
	encodeOctal(&(hdr->cSize[0]), sizeof(hdr->cSize), statBuf->st_size);
	encodeOctal(&(hdr->cMtime[0]), sizeof(hdr->cMtime), statBuf->st_mtime);

	uliCheckSum = computeChecksum(hdr);
	encodeOctal(&(hdr->cCheckSum[0]), sizeof(hdr->cCheckSum), uliCheckSum);

	return true;
}

bool TARutilities::encodeOctal(char* pStr, int liLen, unsigned int uliVal)
{
	if ( pStr == 0 )					return false;

	char* cPtr = (pStr + liLen - 1);

	*(cPtr--) = '\0';

	while ( uliVal && (cPtr >= pStr) )
	{
		*(cPtr--) = '0' + (uliVal & 0x7);
		uliVal = (uliVal >> 3);
	}

	while ( pStr <= cPtr )
	{
		*(cPtr--) = '0';
	}

	return (uliVal == 0);
}

bool TARutilities::writeTarIoBlock(structTarIo* io, uTarBlock* block)
{
	if ( io == 0 )					return false;

	if ( io->wptr == io->end )
	{
		if ( ! flushTarIo(io) )
		{
			return false;
		}
	}

	memcpy(io->wptr, block, sizeof(block[0]));
	io->wptr++;
	return true;
}

bool TARutilities::copyTarIoFromFd(structTarIo* io, int liFd, size_t size)
{
	if ( io == 0 )					return false;

	uTarBlock* blocks;

	while ( size )
	{
		int liBlocksNum;
		size_t liBytesToRead, liBytesRead, liPaddingBytes;
		blocks = getTarIoWriteBlocks(io, &liBlocksNum);
		if ( blocks == 0 )
		{
			break;
		}

		liBytesToRead = ( liBlocksNum * sizeof(blocks[0]) );
		if ( size < liBytesToRead )
		{
			liBytesToRead = size;
		}

		liBytesRead = read(liFd, blocks, liBytesToRead);
		if ( liBytesRead != liBytesToRead )
		{
			m_pLogger->log(LOG_ERR, "TARutilities::copyTarIoFromFd: read failed");
			io->bGotError = true;
			break;
		}
		size -= liBytesRead;
		liPaddingBytes = ( liBytesRead + sizeof(io->base[0]) - 1 ) / sizeof(io->base[0]);
		liPaddingBytes *= sizeof(io->base[0]);
		liPaddingBytes -= liBytesRead;
		if ( liPaddingBytes )
		{
			assert ( size == 0 );
			memset(((char*)io->wptr) + liBytesRead, '\0', liPaddingBytes);
		}

		io->wptr += ((liBytesRead + liPaddingBytes) / sizeof(blocks[0]));
	}

	return ( ! io->bGotError );
}

uTarBlock* TARutilities::getTarIoWriteBlocks(structTarIo* io, int* piNum)
{
	if ( io == 0 )					return 0;
	if ( piNum == 0 )				return 0;

	if (io->wptr == io->end)
	{
		if ( ! flushTarIo(io) )
		{
			*piNum = 0;
			return 0;
		}
	}
	*piNum = (io->end - io->wptr);
	return io->wptr;
}
