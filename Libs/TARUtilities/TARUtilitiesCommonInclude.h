//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    TARUtilitiesCommonInclude.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the defines for the TARUtilities lib.
//! @details
//!
//*!****************************************************************************

#ifndef TARUTILITIESCOMMONINCLUDE_H
#define TARUTILITIESCOMMONINCLUDE_H

#include <stddef.h>
#include <glob.h>

#define TARNAMESIZE 100

typedef union
{
	unsigned char data[512];
	struct
	{
		char cName[TARNAMESIZE];
		char cMode[8];
		char cUid[8];
		char cGid[8];
		char cSize[12];
		char cMtime[12];
		char cCheckSum[8];
		bool bHasLinks;
		char cIsLinkTo[TARNAMESIZE];
		char cExtno[4];
		char cExtotal[4];
		char cEfsize[12];
		bool bWasCompressed;
	};
} uTarBlock;

typedef struct
{
	int liFd;
	uTarBlock* base;
	uTarBlock* rptr;
	uTarBlock* wptr;
	uTarBlock* end;
	size_t size;
	bool bIsWrite;
	bool bGotError;
} structTarIo;

typedef struct
{
	bool bExtract;			/* "-x" */
	bool bCreate;			/* "-c" */
	bool bTell;				/* "-t" */
	bool bVerbose;			/* "-v" */
	bool bFile;				/* "-f" */
	bool bPreserve;			/* "-p" */
	int liNumFiles;
	int liMaxFiles;
	const char** ppFiles;

	// these should be only for integrity...
	glob_t globData;
	bool bDidGlob;
} structTarOpt;

#endif // TARUTILITIESCOMMONINCLUDE_H
