QT          -= core gui
TARGET      = TARUtilities
TEMPLATE    = lib
CONFIG      += staticlib

QMAKE_CXXFLAGS_DEBUG -= -O1
QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG -= -O3
QMAKE_CXXFLAGS_DEBUG += -O0


SOURCES +=  $$PWD/TARUtilities.cpp \

HEADERS += \
    $$PWD/TARUtilities.h \
    $$PWD/TARUtilitiesCommonInclude.h \


# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a
