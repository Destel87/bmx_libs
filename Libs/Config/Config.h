//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Config.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the Config class.
//! @details
//!
//*!****************************************************************************

#ifndef CONFIG_H
#define CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>

#define CONFIG_VERSION "1.0.0"


/*! ***********************************************************************************************
 * @file Config.h
 * @brief
 * <h2>COME UTILIZZARE LA LIBRERIA</h2>
 *
 * per utilizzare la libreria di configurazione si devono individuare i parametri
 * da memorizzare ed attribuire a ciascuno di essi un indice <b>univoco</b>. <br>
 * ad esempio si possono usare delle define come segue: <br>
 * <code>
 * #define CONFIG_NAME			0     <br>
 * #define CONFIG_THRESHOLD		1     <br>
 * #define CONFIG_FRAME_NUMBER	2     <br>
 * #define CONFIG_PREFIX			3     <br>
 * </code>
 * <br>
 * oppure una enumerazione come la seguente: <br>
 * <code>
 *	enum {						<br>
 * 		CONFIG_NAME,			<br>
 * 		CONFIG_THRESHOLD,		<br>
 * 		CONFIG_FRAME_NUMBER,	<br>
 * 		CONFIG_PREFIX,			<br>
 *	};
 * </code>
 * <br>
 * una volta che e' sato costruito un indice per ogni chiave si costruisce un array
 * di chiavi da cercare sul file di configurazione. Ogni elemento di questo array
 * e' costituito da una struttura di tipo config_key_t; in questa struttura
 * sono indicati:
 * <ul>
 *   <li> l'indice stabilito prima per quel parametro
 *   <li> la stringa che identifica il parametro nel file di configurazione
 *   <li> la sezione del file di configurazione in cui si trova il parametro
 *   <li> il tipo del parametro (intero, float, ecc.) secondo le define sopra
 *   <li> il valore di default espresso come stringa
 *   <li> un flag (con valore DEFAULT_ACCEPTED oppure DEFAULT_UNACCEPTED) che indica
 *   se i valori di default sono accettabili o se deve necessariamente essere letto
 *   un valore dal file di configurazione
 * </ul>
 * un esempio di dichiarazionedi tale array e' il seguente: <br>
 * <code>
 *	config_key_t keys[] = {  <br>
 *		{ CONFIG_NAME, "NAME", "GENERAL", T_string, "mirko.pnt", DEFAULT_ACCEPTED}, <br>
 *		{ CONFIG_THRESHOLD, "THRESHOLD", "GENERAL", T_float, "0.1", DEFAULT_UNACCEPTED}, <br>
 *		{ CONFIG_FRAME_NUMBER, "FRAME_NUM", "GENERAL", T_int, "1"}, <br>
 *		{ CONFIG_PREFIX, "PREFIX", "GENERAL", T_char, "c"}, <br>
 *	}; <br>
 * </code>
 * a questo punto e' possibile inizializzare la struttura di configurazione
 * con una chiamata del tipo <br>
 * <code>
 *	config_t* config;   <br>
 *	int num_key;   <br>
 *	num_key = sizeof(keys) / sizeof(config_key_t);   <br>
 *	config = config_init("file.cfg", keys, 4);   <br>
 * </code>   <br>
 * se la struttura viene allocata correttamente e' possibile recuperare i parametri
 * con le funzioni config_get_char, config_get_int, ecc. specificando l'indice
 * univoco del parametro
 *
 * <hr>
 *
 * La classe <i>CTlabConfig</i> dichiarata sotto e' visibile (grazie ad una #ifdef) solo se
 * il presente file viene incluso in un file c++.
 * le dichiarazioni e le funzioni sono del tutto analoghe alle funzioni c
 * la classe non e' altro che una interfaccia che richiama le funzioni c
 * ************************************************************************************************
 */


// Tipi di dato consentiti

/*!
* \def T_unused indica un tipo di dati sconosciuto
*/
#define T_unused 0
/*!
* \def T_int indica un tipo di dati intero con segno 
*      (eventualmente scritto in esadecimale)
*/
#define T_int   1
/*!
* \def T_float indica un tipo di dati float con segno 
*/
#define T_float 2
/*!
* \def T_string indica un tipo di dati string cioe' un
*      buffer di caratteri terminato da \0
*/
#define T_string 3
/*!
* \def T_string indica un tipo di dati string cioe' un
*      buffer di caratteri terminato da l valore 0
*/
#define T_char 4


// indica se per un parametro si puo' accettare un valore di default
#define DEFAULT_ACCEPTED 1
#define DEFAULT_UNACCEPTED 0
#define DEFAULT_IGNORE -1

/*!
* @struct config_key struttura della chiave della tabella di configurazione
* @typedef config_key_t chiave della tabella di configurazione
*/
typedef struct config_key {
	int m_index; //!< indice all'interno del file
    char *m_key_string; //!< valore da ricercare nel file
    char *m_section; //!< sezione in cui ricercare il valore
    int m_type;//!< tipo della variabile (intero, stringa, ecc.)
    char* m_default_value;//!< valore di default espresso come stringa
    int m_default_accepted;//!< indica se per si puo' accettare un valore di default	
} config_key_t;

/*
 * Struttura per memorizzare i parametri letti una tantum,
 * ed indipendetemente da quelli previsti.
 */
struct config_param {
	char *m_key;
	char *m_value;
	char *m_section;
	struct config_param *m_next;
};

/*!
* @struct config struttura principale, contiene la tabella di configurazione
*  e le informazioni sul file di configurazione
* @typedef config_t tipo di dati config
*/
typedef struct config {
	char m_config_file[1024]; //!< nome del file di configurazione
	struct _config_entry * m_entries; //!< vettore con tutte le entries con i valori
	int m_num_entries; //!< dimensione del vettore con le entries
	int m_is_debug; //!< la config e' in modalita' debug
	struct config_param *m_params_head;
} config_t;

int setconfigvalue(config_key_t* config_keys, int index, int num_keys, int value);



/*!
* @fn config_t* config_init(config_key_t* keys, int num_keys, const char* filename)
* @brief alloca ed inizializza la struttura
* @param keys puntatore ad un array di elementi contenenti le chiavi da cercare sul file
*			ed i valori di default (vedi sopra)
* @param num_keys dimensione dell'array di chiavi
* @param filename nome del file di configurazione (se vale null si prende il valore di default)
*			 "config.txt"
* @sa config_init_with_default_check
* @return un puntatore alla struttura allocata e NULL in caso di errore
*/
config_t* config_init(config_key_t* keys, int num_keys, const char* filename);

/*!
* @fn config_t* config_init_with_default_check(config_key_t* keys, int num_keys,
						   				const char* filename, int check_default)
* @brief alloca ed inizializza la struttura con una eventuale esclusione del
*  check dei valori di default
* @param keys puntatore ad un array di elementi contenenti le chiavi da cercare sul file
*			ed i valori di default (vedi sopra)
* @param num_keys dimensione dell'array di chiavi
* @param filename nome del file di configurazione (se vale null si prende il valore di default)
*			 "config.txt"
* @param check_default se vale 0 non viene effettuato il check dei valori di default
* @sa config_init
* @return un puntatore alla struttura allocata e NULL in caso di errore
*/
config_t* config_init_with_default_check(config_key_t* keys, int num_keys,
						   				const char* filename, int check_default);

/*!
* @fn int config_check_default_values(config_t* config, config_key_t* keys, int num_keys)
* @brief esegue il controllo dei valori di default
* @param config: puntatore alla struttura di configurazione
* @param keys:	puntatore ad un array di elementi contenenti le chiavi da cercare sul file
*			ed i valori di default (vedi sopra)
* @param num_keys: dimensione dell'array di chiavi
* @return restituisce 0 se il controllo ha successo e -1 altrimenti
*/
int config_check_default_values(config_t* config, config_key_t* keys, int num_keys);

/*!
* @fn int config_free(config_t* config)
* @brief libera la struttura e la memoria allocate da config_init
* @param config: puntatore alla struttura di configurazione
* @sa config_init
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_free(config_t* config);

/*!
* @fn void config_set_debug(config_t* config, int debug_val)
* @brief imposta la modalita' di debug
* @param config puntatore alla struttura di configurazione
* @param debug_val vale 0 per disabilitare il debug ed 1 per abilitarlo
*/
void config_set_debug(config_t* config, int debug_val);

/*!
* @fn int config_read_file(config_t* config, const char* filename)
* @brief legge il file di configurazione
* @param config: puntatore alla struttura di configurazione
* @param filename: nome del file da cui leggere, se e' NULL si prende il nome indicato
*			 dentro la struttura config
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_read_file(config_t* config, const char* filename);

/*!
* @fn int config_write_file(config_t* config, const char* filename)
* @brief scrive il file di configurazione
* @param config: puntatore alla struttura di configurazione
* @param filename: nome del file da cui leggere, se e' NULL si prende il nome indicato
*			 dentro la struttura config
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_write_file(config_t* config, const char* filename);

/*!
* @fn char config_get_char(config_t* config, int index)
* @brief  recupera un char
* @param config struttura di configurazione
* @param index indice del parametro (secondo quanto indicato con l'array delle chiavi
*		  durante l'inizializzazione della struttura)
* @return restituisce il parametro
*/
char config_get_char(config_t* config, int index);
char config_get_param_char(config_t* config, const char *section,
		const char *key, char def);

/*!
* @fn int config_get_int(config_t* config, int index)
* @brief recupera un int
* @param config struttura di configurazione
* @param index indice del parametro (secondo quanto indicato con l'array delle chiavi
*		  durante l'inizializzazione della struttura)
* @return restituisce il parametro
*/
int config_get_int(config_t* config, int index);
int config_get_param_int(config_t* config, const char *section,
		const char *key, int def);

/*!
* @fn float config_get_float(config_t* config, int index)
* @brief recupera un float
* @param config struttura di configurazione
* @param index indice del parametro (secondo quanto indicato con l'array delle chiavi
*		  durante l'inizializzazione della struttura)
* @return restituisce il parametro
*/
float config_get_float(config_t* config, int index);
float config_get_param_float(config_t* config, const char *section,
		const char *key, float def);

/*!
* @fn char* config_get_string(config_t* config, int index)
* @brief recupera una stringa
* @param config struttura di configurazione
* @param index indice del parametro (secondo quanto indicato con l'array delle chiavi
* 		  durante l'inizializzazione della struttura)
* @return restituisce il puntatore ad una stringa allocata dentro la struttura config
*/
char* config_get_string(config_t* config, int index);
char* config_get_param_string(config_t* config, const char *section,
		const char *key, const char *def);

/*!
* @fn int config_set_char(config_t* config, int index, const char c)
* @brief  modifica un char
* @param config struttura di configurazione
* @param index indice del parametro (secondo quanto indicato con l'array delle chiavi
*		  durante l'inizializzazione della struttura)
* @param c nuovo valore
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_set_char(config_t* config, int index, const char c);

/*!
* @fn int config_set_int(config_t* config, int index, const int i)
* @brief modifica un int
* @param config struttura di configurazione
* @param index indice del parametro (secondo quanto indicato con l'array delle chiavi
*		  durante l'inizializzazione della struttura)
* @param i nuovo valore
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_set_int(config_t* config, int index, const int i);

/*!
* @fn int config_set_float(config_t* config, int index, const float f)
* @brief modifica un float
* @param  config struttura di configurazione
* @param  index indice del parametro (secondo quanto indicato con l'array delle chiavi
*		  durante l'inizializzazione della struttura)
* @param  f nuovo valore
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_set_float(config_t* config, int index, const float f);

/*!
* @fn int config_set_string(config_t* config, int index, const char* string)
* @brief modifica una stringa
* @param config struttura di configurazione
* @param index indice del parametro (secondo quanto indicato con l'array delle chiavi
*		  durante l'inizializzazione della struttura)
* @param string nuovo valore
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_set_string(config_t* config, int index, const char* string);


/*!
* @fn void config_print_params(config_t* config, FILE * fp_out)
* @brief scrive i parametri su un file
* @param  config struttura di configurazione
* @param  fp_out puntatore al file dove vongono stampati i parametri
*		(se e' NULL si stampa su stdout)
*/
void config_print_params(config_t* config, FILE * fp_out);


/*!
* @fn void config_print_last_error(FILE * fp)
* @brief scrive su un file l'ultimo errore avvenuto
* @param  fp: puntatore al file dove vongono stampati i parametri 
*		(se e' NULL si stampa su stdout)
*/
void config_print_last_error(FILE * fp);

/*!
* @fn void config_get_last_error(char* string, int size)
* @brief scrive su una stringa l'ultimo errore avvenuto
* @param string (out): stringa su cui si stampa il messaggio
* @param size (in): dimensione della stringa
*/
void config_get_last_error(char* string, int size);

/*!
* @fn config_build_xml(const char* filename)
* @brief costruisce un buffer con i valori di configurazione formattati in xml
* @param filename: nome del file di configurazione
* @param head: tag head dell'xml
* @return un buffer con l'xml desiderato (<i> Deve essere deallocato dall'utente con una free </i>)
*/
char* config_build_xml(const char* filename, const char* head);

/*!
* @fn int config_file_set_param(const char* filename, const char* option, const char* value, const char* section)
* @brief modifica un valore in un file di configurazione, se il valore non e' presente lo aggiunge
* @param filename: nome del file di configurazione
* @param option: nome della chiave di configurazione
* @param value: nome del valore da inserire
* @param section: nome della sezione in cui inserire il valore (se NULL viene usata la sezione GENERAL)
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_file_set_param(const char* filename, const char* option, const char* value, const char* section);

/*!
* @fn int config_file_del_param(const char* filename, const char* option, const char* section)
* @brief elimina una voce da un file di configurazione
* @param filename: nome del file di configurazione
* @param option: nome della chiave di configurazione
* @param section: nome della sezione in cui inserire il valore (se NULL viene usata la sezione GENERAL)
* @return restituisce 0 in caso di successo e -1 in caso di errore
*/
int config_file_del_param(const char* filename, const char* option, const char* section);

/*!
* @fn int config_file_exist_param(const char* filename, const char* section)
* @brief controlla se esiste il parametro specificato nella sezione specificata
* @param filename: nome del file di configurazione
* @param option: nome della chiave di configurazione
* @param section: nome della sezione in viene effettuato il controllo
* @return restituisce 0 se esiste il parametro e -1 se non esiste. Ritorna -1 anche in caso di errore
*/
//int config_file_exist_param(const char* filename, const char* option, const char* section);
int config_file_exist_param(char* filename, char* option, char* section);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
// Implementazione della classe CTlabConfig

/*!
* @class CTlabConfig
* @brief questa classe e' un wrapper per poter usare le funzioni della config
*  in un codice c++ in maniera piu' semplice da usare
*/
class Config {
	config_t* m_pConfig;
public:
	/*!
	* costruttore di default
	*/
    Config() { m_pConfig = NULL;};
	/*!
	* costruttore con parametri. RIchiama la init
	* @sa Init
	*/
	Config(config_key_t* keys, int num_keys, const char* filename = NULL, bool check_default=true)
        { m_pConfig = NULL; Init(keys, num_keys, filename, check_default);};
    ~Config() { Free(); };
	/*!
	* @sa config_init
	* @sa config_init_with_default_check
	*/
	bool Init(config_key_t* keys, int num_keys, const char* filename = NULL, bool check_default=true)
	{
		if (m_pConfig != NULL) {
			config_free(m_pConfig);
		}
		m_pConfig = config_init_with_default_check(keys, num_keys, filename, (check_default)? 1 : 0 );
		return (m_pConfig != NULL);
	};
	
	/*!
	* @sa config_set_debug
	*/
	void SetDebug (bool value)
	{
		config_set_debug(m_pConfig, (int) value);
	}

	/*!
	* @sa config_check_default_values
	*/
	bool CheckDefault(config_key_t* keys, int num_keys)
	{
		if (m_pConfig == NULL) {
			return false;
		}
		return (config_check_default_values(m_pConfig, keys, num_keys) == 0);
    };

	/*!
	* @sa config_free
	*/
    void Free() { if(m_pConfig != NULL) config_free(m_pConfig); m_pConfig = NULL; };

	/*!
	* @sa config_read_file
	*/
	bool ReadFile(const char* filename = NULL)
        { return( config_read_file(m_pConfig, filename) == 0); };

	/*!
	* @sa config_write_file
	*/
	bool WriteFile(const char* filename = NULL)
        { return( config_write_file(m_pConfig, filename) == 0); };

	/*!
	* @sa config_get_char
	*/
    char GetChar(int ind) { return config_get_char(m_pConfig, ind);};
	char GetParamChar(const char *section, const char *key, const char def) {
		return config_get_param_char(m_pConfig,section,key,def);
	}
	/*!
	* @sa config_get_int
	*/
    int GetInt(int ind) { return config_get_int(m_pConfig, ind);};
	int GetParamInt(const char *section, const char *key, int def) {
			return config_get_param_int(m_pConfig,section,key,def);
	}
	/*!
	* @sa config_get_float
	*/
    float GetFloat(int ind) { return config_get_float(m_pConfig, ind);};
	int GetParamFloat(const char *section, const char *key, float def) {
			return config_get_param_float(m_pConfig,section,key,def);
	}
	/*!
	* @sa config_get_string
	*/
    char* GetString(int ind) { return config_get_string(m_pConfig, ind);};
	char* GetParamString(const char *section, const char *key, const char *def) {
		return config_get_param_string(m_pConfig,section,key,def);
	}
	/*!
	* @sa config_set_char
	*/
	bool SetChar(int ind, const char c)
        { return (config_set_char(m_pConfig, ind, c) == 0);};
	/*!
	* @sa config_set_int
	*/
	bool SetInt(int ind, const int i)
        { return (config_set_int(m_pConfig, ind, i) == 0);};
	/*!
	* @sa config_set_float
	*/
	bool SetFloat(int ind, const float f)
        { return (config_set_float(m_pConfig, ind, f) == 0);};
	/*!
	* @sa config_set_string
	*/
	bool SetString(int ind, const char* string)
        { return (config_set_string(m_pConfig, ind, string) == 0);};
	/*!
	* Recupera il nome del file di configurazione
	* @param name buffer dove verra' scritto il nome del file di configurazione
	* @return true in caso di esecuzione corretta e false altrimenti
	*/
	bool GetConfigFileName(char* name)
        {if (m_pConfig == NULL) return false; strcpy(name, m_pConfig->m_config_file); return true;};
	/*!
	* @sa config_print_params
	*/
    void PrintParams(FILE * fp_out = NULL) {config_print_params(m_pConfig, fp_out);};
	/*!
	 * Recupera un puntatore alla lista con i parametri letti dal file
	 * @return il puntatore alla lista o NULL se la lista e' vuota o non inizializzata
	 */
	struct config_param* GetParamList() {
		if (m_pConfig == NULL) {
			return NULL;
		}
		return m_pConfig->m_params_head;
	}
	/*!
	* @sa config_print_last_error
	*/
    static void PrintLastError(FILE * fp_out = NULL) {config_print_last_error(fp_out);};
	/*!
	* @sa config_get_last_error
	*/
    static void GetLastError(char* string, int size) {config_get_last_error(string, size);};
	/*!
	* @sa config_build_xml
	*/
    static char* BuildXML(const char* filename, const char *head = "config") { return config_build_xml(filename, head); };
	/*!
	* @sa config_file_set_param
	*/
	static bool FileSetParam(const char* filename, const char* option, const char* value, const char* section = NULL)
        { return (config_file_set_param(filename, option, value, section) == 0); };
	/*!
	* @sa config_file_del_param
	*/
	static bool FileDelParam(const char* filename, const char* option, const char* section = NULL)
        { return (config_file_del_param(filename, option, section) == 0); };
	/*!
	* @sa config_file_exist_param
	*/
	//static bool FileExistParam(const char* filename, const char* option, const char* section = NULL)
	static bool FileExistParam(char* filename, char* option, char* section = NULL)
        { return (config_file_exist_param(filename, option, section) == 0); };
};

#endif // __cplusplus
#endif	// CONFIG_H
