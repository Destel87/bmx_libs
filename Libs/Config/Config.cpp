//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    Config.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the Config class.
//! @details
//!
//*!****************************************************************************

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>

#ifdef WIN32
//#define stat _stat
//#define rename _rename
#else
#include <unistd.h>
#endif

#include "Config.h"

#define MAX_STR_LEN 1024

#define DEFAULT_XML_HEADER "biomerieux"


//------------------------------------------------
//
//  Strutture interne alla libreria
//
//------------------------------------------------
// valore della variabile
typedef union _config_value
{
	int	m_int_value;
	char* m_string_value;
	char m_char_value;
	float m_float_value;
} config_value_t;

// entry del vettore all'interno della struttura
typedef struct _config_entry
{
	// valore
	config_value_t m_value;
	// valore da ricercare nel file
	char *m_key_string;
	// sezione in cui ricercare il valore
	char *m_section;
	// tipo della variabile
    int m_type;
	// flag che indica se la entry ha il valore di default o meno
	int m_is_default;
} config_entry_t;




//------------------------------------------------
//  Variabili globali locali
//------------------------------------------------
static char g_last_error[MAX_STR_LEN] = "";

//------------------------------------------------
//  Funzioni locali
//------------------------------------------------

// controlla se una stringa contiene un numero
int is_number(char* string)
{
	unsigned int i;
	for (i = 0; i < strlen(string); i++) {
		if ( (string[i] < '0') || (string[i] > '9') ) {
			return -1;
		}
	}
	return 0;
}


// esegue una fprintf
static void my_fprintf(FILE* fp, const char* fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
#ifdef _WIN32
	if ( (fp == stdout) || (fp == stderr) ){
		vprintf(fmt, ap);
	} else {
		vfprintf(fp, fmt, ap);
	}
#else
	vfprintf(fp, fmt, ap);
#endif
	va_end(ap);
}

// toglie gli spazi all'inizio ed alla fine di una stringa
static void trim_string(char* string)
{
    char tmp[MAX_STR_LEN];
	unsigned int i;

	// toglie gli spazi all'inizio
	i = 0;
	while (1) {
		if (string[i] == 0) {
			// ha finito la stringa: ritorna
			return;
		}
		if ( (string[i] != ' ') && (string[i] != '\t') ){
			// ha trovato un carattere
			break;
		}
		i++;
	}
	// copia la stringa temporanea
	strcpy(tmp, string + i);

	// toglie gli spazi dalla fine della stringa
	i = (unsigned int)strlen(tmp) -1;
    while (1)
    {
        if (i == 0)
        {
			// ha finito la stringa
			break;
		}
        if ( (string[i] != ' ') && (string[i] != '\t') )
        {
			// ha trovato un carattere
			break;
		}
		tmp[i] = 0;
		i--;
	}
	strcpy(string, tmp);
}

// esegue una atoi anche se ho un esadecimale
static int my_atoi(char* string)
{
	char* hex;
	int ret, pow;
	unsigned int i;

    if (strlen(string) < 2)
    {
		// troppo corto per essere un esadecimale
		// controlla che sia un numero
        if (is_number(string) != 0)
        {
			//printf("%s non e' un numero\n", string);
			return 0;
		}
		//printf("%s e' un numero\n", string);
		return atoi(string);
	}

	// controlla se e' un esadecimale
    if ( (string[1] != 'x') && (string[1] != 'X') )
    {
		// e' in base 10
		//printf("%s e' base 10\n", string);
		return atoi(string);
	}
	// dovrebbe essere un esadecimale
    if (string[0] != '0')
    {
		// il primo carattere non e' uno 0: non torna qualcosa
		//printf("%s e' un esadecimale senza 0 all'inizio\n", string);
		return 0;
	}
	
	//printf("%s e' un esadecimale\n", string);

	// recupera l'inizio del valore esadecimale
    hex = string + 2;
	// costruisce il valore finale
	ret = 0;
	pow = 1;
    for (i = 1; i <= strlen(hex); i++)
    {
		int val;
		char c;
		c = hex[strlen(hex) - i];
		// esegue la conversione hex -> bin
		val = 0;
        if ( (c >= '0') && (c <= '9') )
        {
			val = c - '0';
        }
        else if ( (c >= 'A') && (c <= 'F') )
        {
			val = c - 'A' + 10;
        }
        else if ( (c >= 'a') && (c <= 'f') )
        {
			val = c - 'a' + 10;
		}
		// aggiorna il valore finale
		ret += (val * pow);
		//printf("%d) c=%c \t  pow = %d \t  val = %d \t ret = %d\n", i, c, pow, val, ret);
		// incrementa il valore del moltiplicatore
		pow *= 16;
	}
	return ret;
}


// routine che esegue la fgets per files di testo in windows
// o in unix indifferentemente (fgets universale)
static char *fgets_u(char *string, int size, FILE * fp)
{
    char *cp;
    cp = fgets(string, size, fp);
    if (cp == NULL)
    {
		return NULL;
	}
    cp = strchr(string, '\r');
    if (cp != NULL)
    {
		*cp = 0;
	}
    return string;
}


static void SetVal(config_t* config, char *option, char *value, char *section)
{
    int i;
	int size;
    char section_and_name[MAX_STR_LEN];
	
	size = config->m_num_entries;

    for (i = 0; i < size; i++)
    {
		int found;
	
        if (config->m_entries[i].m_type == T_unused)
        {
			// chiave non impostata
			continue;
		}
		found = 0;
		
		// 1) Prova nella maniera classica
		if ( (strcmp(config->m_entries[i].m_section, section) == 0) && 
            (strcmp(config->m_entries[i].m_key_string, option) == 0) )
        {
			found = 1;
		}
		// 2) Prova a vedere se la forma della chiave e' SEZIONE.NOME
        if ( !found && (strcmp(section, "GENERAL") == 0) )
        {
            snprintf(section_and_name, (MAX_STR_LEN - 1), "%s.%s", config->m_entries[i].m_section, config->m_entries[i].m_key_string);
            if (strcmp(option, section_and_name) == 0)
            {
				//printf("DEBUG FOUND %s\n", option);
				found = 1;
			}
		}
		
		// se non sono nella sezione voluta passo alla entry successiva
        if ( !found )
        {
			continue;
		}

		// se arriva qui ha trovato una entry che va bene
		// a seconda del tipo di variabile imposta il dato
        switch (config->m_entries[i].m_type)
        {

		case T_char:
			config->m_entries[i].m_value.m_char_value = value[0];
			config->m_entries[i].m_is_default = 0; // imposta il valore come non piu' di default
			break;

		case T_int:
			// se value ha lunghezza zero allora voglio che il parametro sia DEFAULT
            if (strlen(value) > 0)
            {
				config->m_entries[i].m_value.m_int_value = my_atoi(value);
				config->m_entries[i].m_is_default = 0; // imposta il valore come non piu' di default
			}
			break;

		case T_float:
            if (strlen(value) > 0)
            {
				config->m_entries[i].m_value.m_float_value = (float) atof(value);
				config->m_entries[i].m_is_default = 0; // imposta il valore come non piu' di default
			}
			break;

		case T_string:
            if (config->m_entries[i].m_value.m_string_value != NULL)
            {
				free(config->m_entries[i].m_value.m_string_value);
			}
			config->m_entries[i].m_value.m_string_value = strdup(value);
			config->m_entries[i].m_is_default = 0; // imposta il valore come non piu' di default
			break;
		}

		// NON esce dal ciclo perche' lo stesso nome potrebbe comparire piu'
		// volte (anche se ha poco senso ...)
		//break;
    }
}

static void SetEntry(config_key_t* key, config_entry_t* entry)
{
	entry->m_key_string = strdup(key->m_key_string);
	entry->m_section = strdup(key->m_section);
	// controlla se la chiave e' da ignorare:
    if (key->m_default_accepted == DEFAULT_IGNORE)
    {
		entry->m_type = T_unused;
		return;
	}
	
	entry->m_type = key->m_type;

	// imposta il valore di default
    switch (entry->m_type)
    {
	case T_char:
		entry->m_value.m_char_value = key->m_default_value[0];
		break;
	case T_int:
		entry->m_value.m_int_value = my_atoi(key->m_default_value);
		break;
	case T_float:
		entry->m_value.m_float_value = (float)atof(key->m_default_value);
		break;
	case T_string:
		entry->m_value.m_string_value = strdup(key->m_default_value);
		break;
	}
	// imposta il flag che indica che e' un valore di default
	entry->m_is_default = 1;
}

static void write_single_entry(config_t* config, int index, FILE* fp)
{
	// a seconda del tipo di variabile imposta il dato
    switch (config->m_entries[index].m_type)
    {

	case T_char:
		my_fprintf(fp, "%s=%c\n",
			config->m_entries[index].m_key_string,
			config->m_entries[index].m_value.m_char_value);
		break;

	case T_int:
		my_fprintf(fp, "%s=%d\n",
			config->m_entries[index].m_key_string,
			config->m_entries[index].m_value.m_int_value);
		break;

	case T_float:
		my_fprintf(fp, "%s=%f\n",
			config->m_entries[index].m_key_string,
			config->m_entries[index].m_value.m_float_value);
		break;

	case T_string:
		my_fprintf(fp, "%s=%s\n",
			config->m_entries[index].m_key_string,
			config->m_entries[index].m_value.m_string_value);
		break;
	}
}

static void write_all_entries(config_t* config, int* written, FILE* fp)
{
	config_entry_t* entries;
	int num_entries, i;
	char current_section[MAX_STR_LEN] = "GENERAL";

	entries = config->m_entries;
	num_entries = config->m_num_entries;

    for (i = 0; i < num_entries; i++)
    {
        if (entries[i].m_type == T_unused)
        {
			// chiave non impostata
			continue;
		}
		
		// controlla se l'entry in posizione i deve essere scritta
		if (written[i] != 0)
        {
			continue;
        }
		// controlla se la sezione della entry corrisponde a quella attuale
        if (strcmp(current_section, entries[i].m_section) != 0)
        {
			// crea una nuova sezione
			my_fprintf(fp, "\n[%s]\n", entries[i].m_section);
			strcpy(current_section, entries[i].m_section);
		}
		// scrive l'entry
		write_single_entry(config, i, fp);

		// imposta l'entry come scritta
		written[i] = 1;
	}
}

static int find_entry_index(config_t* config, const char* key_string,
							const char* section)
{
	int i;
	int index = -1;

	assert(config != NULL);

    for (i = 0; i < config->m_num_entries; i++)
    {
        if ( (strcmp(config->m_entries[i].m_key_string, key_string) == 0) &&
             (strcmp(config->m_entries[i].m_section, section)==0) )
        {
			index = i;
			break;
		}
	}

	return index;
}


static int config_read_file_plain(config_t* config, const char* filename)
{
	FILE* fp;
	char section[MAX_STR_LEN] = "GENERAL"; // sezione corrente
	char string[MAX_STR_LEN];
	char* cp;
	char option[MAX_STR_LEN], value[MAX_STR_LEN];

	fp = fopen(filename, "r");
    if (fp == NULL)
    {
		sprintf(g_last_error, "Impossible to open file <%s>", filename);
		return -1;
	}

    while (!feof(fp))
    {
		if (fgets_u(string, MAX_STR_LEN, fp) == NULL)
        {
			break;
        }
		// toglie la fine della riga al valore
		cp = strchr(string, '\r');
        if (cp != NULL)
        {
			*cp = 0;
		}
		cp = strchr(string, '\n');
		if (cp != NULL) {
			*cp = 0;
		}

		if (string[0] == '#')
        {
			continue;		// e' un commento
        }

        if (string[0] == '[')
        {
			// stiamo entrando in una sezione
			cp = strchr(string, ']');
			// controllo se la parentesi si chiude
			if (cp == NULL)
            {
				continue; // passa alla riga successiva
            }
			// faccio terminare la stringa
			*cp = 0;
			// copio la sezione corrente
			strcpy(section, &string[1]);

			// passa alla riga successiva
			continue;
		}

		// toglie un eventuale commento a meta' riga
		cp = strchr(string, '#');
        if (cp != NULL)
        {
			*cp = 0;
		}

		// cerca lo spazio
		cp = strchr(string, '=');
        if (cp == NULL)
        {
			// cerca un tab
			cp = strchr(string, ' ');
            if (cp == NULL)
            {
				// cerca un =
				cp = strchr(string, '\t');
			}
		}
		// controlla se ha trovato qualcosa
        if (cp == NULL)
        {
			//errore: la riga non contiene parametri
			continue;
		}

		// copia il nome
		*cp = 0;
		strcpy(option, string);

        if (strlen(option) == 0)
        {
			continue;
		}

		// cerca l'inizio del valore
		cp++;
		// scorre la stringa (nota: il seguente ciclo finisce sicuramente perche'
		// ci sara' alla fine uno \0)
        while ( (*cp == ' ') || (*cp == '\t') || (*cp == '='))
        {
			cp++;
		}
		strcpy(value, cp);

		// toglie gli spazi all'inizio ed alla fine
		trim_string(option);
		trim_string(value);

		// Aggiunge il paramtro trovato alla lista
		struct config_param *cur_param = config->m_params_head;
        while(cur_param->m_next != NULL)
        {
			cur_param = cur_param->m_next;
		}
		struct config_param *new_param =
				(struct config_param *)malloc(sizeof(struct config_param));
		cur_param->m_next = new_param;
        new_param->m_key = strdup(option);
        new_param->m_value = strdup(value);
        new_param->m_section = strdup(section);
        new_param->m_next = NULL;

		// imposta il valore dell'opzione a seconda di quanto
		// impostato nella keytable
		SetVal(config, option, value, section);
    }

	fclose(fp);

    return 0;
}


static int config_write_file_plain(config_t* config, const char* filename)
{
    char name[MAX_STR_LEN];
	char tmp_name[MAX_STR_LEN];
	FILE* fp;
    FILE * fp_orig;
	char section[MAX_STR_LEN] = "GENERAL"; // sezione corrente
	char string[MAX_STR_LEN], tmp_string[MAX_STR_LEN];
	char* cp;
	char option[MAX_STR_LEN], value[MAX_STR_LEN];
	struct stat filestatus;
	int i;
    int *written, *writtenFree; // vettore che indica quale entry e' gia' stata scritta

	assert(config != NULL);

	written = (int*)calloc(config->m_num_entries, sizeof(int));
    if(written == NULL) return -1;

    writtenFree = written;

    if ( (filename != NULL) && (strlen(filename) > 0) )
    {
        if (strlen(filename) > (MAX_STR_LEN - 1))
        {
			sprintf(g_last_error, "Impossible to open file with a name %d bytes long (max=1023)", strlen(filename));
			return -1;
		}
		strcpy(name, filename);
    }
    else
    {
		strcpy(name, config->m_config_file);
	}

	// controlla se il file esiste
    if (stat(name, &filestatus) != 0)
    {
		// il file non esiste: si crea un file nuovo
		fp = fopen(name, "w");
        if (fp == NULL)
        {
            free(writtenFree);
            if(snprintf(g_last_error, MAX_STR_LEN, "Impossible to open file <%s>", name) < 0)
            {
                return -1;
            }
			return -1;
		}
		write_all_entries(config, written, fp);
		fclose(fp);
        free(writtenFree);
		return 0;
	}

	// esiste gia' un file di configurazione precedente, si leggono i commenti
	// e le altre cose da quel file e le si scrivono in un file temporaneo
	// con i nuovi valori, alla fine si fara' un rename

	fp_orig = fopen(name, "r");
    if (fp_orig == NULL)
    {
        free(writtenFree);
        if(snprintf(g_last_error, MAX_STR_LEN, "Impossible to open file <%s>", name) < 0)
        {
            return -1;
        }
		return -1;
	}

    if(snprintf(tmp_name, MAX_STR_LEN, "%s.tmp_config_filename.tmp", name) < 0)
    {
        return -1;
    }

    fp = fopen(tmp_name, "w");
    if (fp == NULL)
    {
        free(writtenFree);
		fclose(fp_orig);
		return -1;
	}


    while (!feof(fp_orig))
    {
		if (fgets_u(string, MAX_STR_LEN, fp_orig) == NULL)
        {
			break;
        }

        if (string[0] == '#')
        {
			// scrive sul nuovo file
			my_fprintf(fp, "%s", string);
			continue;		// e' un commento
		}

        if (string[0] == '[')
        {
			strcpy(tmp_string, string);
			// stiamo entrando in una sezione
			cp = strchr(tmp_string, ']');
			// controllo se la parentesi si chiude
            if (cp == NULL)
            {
				// scrive sul nuovo file
				my_fprintf(fp, "%s", string);
				continue; // passa alla riga successiva
			}
			// faccio terminare la stringa
			*cp = 0;
			// copio la sezione corrente
			strcpy(section, &tmp_string[1]);

			// scrive sul nuovo file
			my_fprintf(fp, "%s", string);

			// passa alla riga successiva
			continue;
		}

		cp = strchr(string, '#');
		if (cp != NULL)
        {
			*cp = 0;
        }

		// copia la riga su una stringa temporanea
		strcpy(tmp_string, string);

		// elimina il fine riga
		cp = strchr(tmp_string, '\r');
        if (cp != NULL)
        {
			*cp = 0;
		}
		cp = strchr(tmp_string, '\n');
        if (cp != NULL)
        {
			*cp = 0;
		}

		// toglie spazi all'inizio ed alla fine
		trim_string(tmp_string);

		// cerca l'uguale
		cp = strchr(tmp_string, '=');
        if (cp == NULL)
        {
			// cerca lo spazio
			cp = strchr(tmp_string, ' ');
            if (cp == NULL)
            {
				// cerca un tab
				cp = strchr(tmp_string, '\t');
			}
		}
		// controlla se ha trovato qualcosa
        if (cp == NULL)
        {
			//errore: la riga non contiene parametri
			continue;
		}

		// copia il nome
		*cp = 0;
		strcpy(option, tmp_string);

        if (strlen(option) == 0)
        {
			continue;
		}

		// cerca l'inizio del valore
		cp++;
		// scorre la stringa (nota: il seguente ciclo finisce sicuramente perche'
		// ci sara' alla fine uno \0)
        while ( (*cp == ' ') || (*cp == '\t') || (*cp == '='))
        {
			cp++;
		}
		strcpy(value, cp);

		// toglie gli spazi all'inizio ed alla fine
		trim_string(option);
		trim_string(value);

		// cerca nella struttura la chiave letta
		i = find_entry_index(config, option, section);
        if (i < 0)
        {
			// non e' stata trovata alcuna entry: non si copia nel nuovo file
			continue;
		}
		//scrive nel file la nuova entry
		write_single_entry(config, i, fp);

		// indica che l'entry corrente e' stata scritta
		written[i] = 1;
    }

	// scrive eventuali entries non scritte
	write_all_entries(config, written, fp);

	fclose(fp);
	fclose(fp_orig);

	// rinomina il file
	unlink(name);
	rename(tmp_name, name);

	// libera la ram
    free(writtenFree);

    return 0;
}

static void append_string(char** src, int* src_size, const char* string)
{

	if ( (strlen(*src) + strlen(string)) > (size_t)*src_size ) {
		// si deve eseguire una reallocazione
		char* tmp, *old;
		int new_size;
		// alloca di nuovo la stringa
		new_size = (*src_size) * 2 + strlen(string);
		tmp = (char *)malloc(new_size);
		// copia i dati sulla nuova stringa
		strcpy(tmp, *src);
		*src_size = new_size;
		// assegna il nuovo valore
		old = *src;
		*src = tmp;
		// elimina il vecchio
		free(old);
	}
	strcat(*src, string);
}

static void append_char(char** src, int* src_size, char c)
{
	char string[2];
	string[0] = c;
	string[1] = 0;
	append_string(src, src_size, string);
}

//static void remove_chars(char* string, unsigned int num_chars)
//{
//	unsigned int size = (unsigned int)strlen(string);
//	if (num_chars < size) {
//		string[size - num_chars] = 0;
//	} else {
//		string[0] = 0;
//	}
//}

// stati della ricerca
#define START_TAG_SEARCH 0
#define START_TAG 1
#define INSIDE_TAG 2
#define SUSPECT_END_TAG 3
#define END_TAG 4

// Legge l'header xml in un buffer
// buffer (in): buffer con i dati
// buffer_size (in): dimensione del buffer
// ritorna numero di caratteri processati oppure -1
// NOTA: per ora l'header non viene parserizzato.
static int read_header(char* buffer, unsigned int buffer_size)
{
	char c;
	unsigned int buffer_pos;
	char* tmp;

	buffer_pos = 0;
	/*
	*	1) Cerca l'inizio del tag header
	*/
    while ( buffer_pos < buffer_size )
    {
		// legge un carattere
		c = buffer[buffer_pos];
		buffer_pos++;
		// sostituisce un tab con uno spazio
        if ( (c == '\t') || (c == '\r') || (c == '\n') )
        {
			continue;
		}
        if (c == '<')
        {
			// ha trovato l'inizio dell'header
			break;
		}
	}
    if (buffer_pos >= buffer_size)
    {
		return -1;
	}
	
	/*
	*	2) Controlla l'header
	*/
	tmp = buffer + buffer_pos;
	if ( strncmp(tmp, "?xml", 4) != 0 ) {
		return -1;
	}
	buffer_pos += 4;

	/*
	*	1) Cerca la fine del tag header
	*/
    while ( buffer_pos < buffer_size )
    {
		// legge un carattere
		c = buffer[buffer_pos];
		buffer_pos++;
		// sostituisce un tab con uno spazio
        if ( (c == '\t') || (c == '\r') || (c == '\n') )
        {
			continue;
		}
        if (c == '>')
        {
			// ha trovato la fine dell'header
			break;
		}
	}
    if (buffer_pos >= buffer_size)
    {
		return -1;
	}

	return buffer_pos;
}

/*
 * Trova il contenuto di un tag contenuto in un buffer
 * buffer (in): buffer con i dati
 * buffer_size (in): dimensione del buffer
 * processed_chars (out): numero di caratteri processati
 * tag_name (out): nome del tag trovato
 * tag_attribute (out): eventuale attributo del tag
 * tag_value_ret (out): buffer con il contenuto del tag
 * tag_value_size_ret (out): dimensione del buffer dei risultati
 */
static int find_tag(char* buffer, int buffer_size,
		int* processed_chars,
		char* tag_name, char* tag_attribute,
		char** tag_value_ret, int* tag_value_size_ret)
{
	char start_tag[MAX_STR_LEN];
	char end_tag[MAX_STR_LEN];

	// buffer che contiene il tag
    char * tag_value, *tag_value_free;
	// dimensione del tag
	int tag_value_size;
	// dimensione del buffer che contiene il tag
	int tag_value_buf_size;

	char* tmp_buffer;
	unsigned int tmp_buffer_size;

	unsigned int first_char, last_char;

	char c;
	unsigned int buffer_pos;


	int status;
	// valori per i commenti
	char last_chars[4];
	//int prev_status = -1;
	
	strcpy(tag_name, "");
	strcpy(tag_attribute, "");

	tag_value_buf_size = MAX_STR_LEN;
	tag_value = (char *)calloc(tag_value_buf_size, sizeof(char));
    if (tag_value == NULL)
    {
		return -1;
	}
    tag_value_free = tag_value;
	tag_value_size = 0;

	buffer_pos = 0;
	status = START_TAG_SEARCH;
    c = last_chars[0] = last_chars[1] = last_chars[2] = last_chars[3] = 0;


    while ( (status != END_TAG) && ((int)buffer_pos < buffer_size) )
    {
		// scorre il buffer con i caratteri precedenti
		last_chars[0] = last_chars[1];
		last_chars[1] = last_chars[2];
		last_chars[2] = c;

		// legge un carattere
		c = buffer[buffer_pos];
		buffer_pos++;
		// sostituisce un tab con uno spazio
        if ( (c == '\t') || (c == '\r') || (c == '\n') )
        {
			c = ' ';
		}

		switch (status) {

		case START_TAG_SEARCH:
			{
				// scorre alla ricerca del tag
                if (c == '<')
                {
					// inizia il tag
					status = START_TAG;
					strcpy(start_tag, "");
				}
				break;
			}

		case START_TAG:
			{
                if (c == '>')
                {
                    // fine del tag di start
                    // controlla se ci sono atributi
                    char* cp;
                    cp = strchr(start_tag, ' ');
                    if (cp != NULL)
                    {
                            // c'e' un attributo
                            *cp = 0;
                            strcpy(tag_name, start_tag);
                            cp++;
                            while (*cp == ' ')
                            {
                                    cp++;
                            }
                            strcpy(tag_attribute, cp);
                    }
                    else
                    {
                            // non ci sono attributi
                            strcpy(tag_name, start_tag);
                    }
                    // cambia lo stato
                    status = INSIDE_TAG;
                }
                else
                {
                    // aggiunge il carattere
                    strncat(start_tag, &c, 1);
				}
				break;
			}

		case INSIDE_TAG:
			{
                if (c == '<')
                {
					// e' finito il valore
					status = SUSPECT_END_TAG;
					strcpy(end_tag, "");
                }
                else
                {
					append_char(&tag_value, &tag_value_buf_size, c);
					tag_value_size++;
				}
				break;
			}

		case SUSPECT_END_TAG:
			{
				// controlla se e' il tag giusto
                if ( (c == '>') && (strcmp(end_tag + 1, start_tag) == 0) )
                {
					// il tag e' finito
					status = END_TAG;
                }
                else
                {
					// aggiunge il carattere appena letto
                                        strncat(end_tag, &c, 1);
					// per il confronto aggiunge 1 in quanto
					// alla fine del tag c'e' un /
                    if (strlen(end_tag + 1) > strlen(start_tag))
                    {
						// non era la fine del tag voluto
						status = INSIDE_TAG;
						append_string(&tag_value, &tag_value_buf_size, "<");
						tag_value_size += 2;
                        append_string(&tag_value, &tag_value_buf_size, end_tag);
						tag_value_size += (unsigned int)strlen(end_tag);
					}
				}
				break;
			}

		}
	}
	
    if (status != END_TAG)
    {
        if (tag_value_free != NULL)
        {
            free(tag_value_free);
		}
		return -1;
	}

	//
	// rimuove gli spazi iniziali e/o finali
	//
	// cerca il primo carattere
    for (first_char = 0; first_char < (unsigned int)tag_value_buf_size; first_char++)
    {
        if ( tag_value[first_char] != ' ')
        {
			break;
		}
	}
	// cerca l'ultimo carattere
	for (last_char = (tag_value_size - 1); last_char > 0; last_char--) {
		if ( (tag_value[last_char] != ' ') &&
			 (tag_value[last_char] != 0) ) {
			break;
		}
	}
	// alloca un nuovo buffer
	tmp_buffer_size = (last_char - first_char) + 2;
	tmp_buffer = (char *)malloc(tmp_buffer_size);
    if (tmp_buffer == NULL)
    {
        free(tag_value_free);
		return -1;
	}

	// copia i dati sul buffer
	memcpy(tmp_buffer, tag_value + first_char, tmp_buffer_size);
	tmp_buffer[tmp_buffer_size - 1] = 0;

	// elimina l'altro buffer
    free(tag_value_free);

	// assegna i nuovi valori
	tag_value = tmp_buffer;
	tag_value_buf_size = tmp_buffer_size;

    // cupia i valori sui buffers di uscita
	*tag_value_ret = tag_value;
	*tag_value_size_ret = tag_value_buf_size;
	*processed_chars = buffer_pos;

	return 0;
}

/*
 * Rimuove i commenti da un buffer di caratteri
 */
#define INSIDE_COMMENT 100
#define SEARCH_COMMENT 101
static void remove_comments(char** buf, int* buf_size)
{
	char* buffer;
	unsigned int buffer_size;

    char * tmp_buffer, * tmp_buffer_free;
	unsigned int tmp_buffer_pos;

	unsigned int buffer_pos;

	int status;
	
	buffer = *buf;
	buffer_size = *buf_size;

	tmp_buffer = (char *)calloc(buffer_size, sizeof(char));
    if (tmp_buffer == NULL)
    {
		return;
	}

    tmp_buffer_free = tmp_buffer;
	buffer_pos = tmp_buffer_pos = 0;
	status = SEARCH_COMMENT;


    while ( buffer_pos < buffer_size )
    {
        switch (status)
        {

		case SEARCH_COMMENT:
			{
				if ( (buffer[buffer_pos] == '<') &&
					 (buffer_size - buffer_pos > 5) &&
					 (buffer[buffer_pos + 1] == '!') &&
					 (buffer[buffer_pos + 2] == '-') &&
					 (buffer[buffer_pos + 3] == '-') ) {
					// e' un commento
					status = INSIDE_COMMENT;
				} else {
					// copia i dati
					tmp_buffer[tmp_buffer_pos] = buffer[buffer_pos];
					tmp_buffer_pos++;
				}

				break;
			}
	
		case INSIDE_COMMENT:
			{
				if ( (buffer[buffer_pos] == '>') &&
					 (buffer[buffer_pos - 1] == '-') && 
					 (buffer[buffer_pos - 2] == '-') ) {
						// fine del commento
					status = SEARCH_COMMENT;
				}
				break;
			}
		}

		// incerementa il buffer
		buffer_pos++;
	}

	// copia i dati sul buffer
	free(*buf);
	*buf_size = tmp_buffer_pos;
	*buf = (char *)malloc(tmp_buffer_pos);
    if (*buf != NULL)
    {
		memcpy(*buf, tmp_buffer, tmp_buffer_pos);
        free(tmp_buffer_free);
	}
}

/*
 * Parserizza una sezione
 * config (in/out): struttura con i dati di configurazione
 * section_name (in): nome della sessione
 * data_buffer (in): buffer con i dati contenuti nel tag della sessione
 * data_buffer_size (in): dimensione del buffer con i dati
 * data_buffer_offset (in): offset del buffer con la sezione
 * processed_chars (out): numero di caratteri processati
 */
static int parse_section_xml(config_t* config,
			char* section_name,
			char* data_buffer,
			unsigned int data_buffer_size,
			unsigned int data_buffer_offset,
			unsigned int* processed_chars)
{
	int rc;
	unsigned int total_processed;
	char tag_name[MAX_STR_LEN], tag_attribute[MAX_STR_LEN];
	char* tag_value = NULL;
	int tag_value_size;

	char* section_buffer = data_buffer + data_buffer_offset;
	unsigned int section_buffer_size = data_buffer_size;

	total_processed = 0;
	while (total_processed < section_buffer_size-1) {
		char* buffer;
		unsigned int buffer_size;
		int processed;

		buffer = section_buffer + total_processed;
		buffer_size = section_buffer_size - total_processed;

		rc = find_tag(buffer, buffer_size,
			&processed,
			tag_name, tag_attribute,
			&tag_value, &tag_value_size);
		if (rc < 0) {
			return -1;
		}
		total_processed += processed;
		// Adesso inserisce il valore trovato nella struttura config
		// .....
		// .....
		// .....
		// imposta il valore dell'opzione a seconda di quanto
		// impostato nella keytable
		SetVal(config, tag_name, tag_value, section_name);

		// libera la memoria del tag
		free(tag_value);
		tag_value = NULL;
	}
	*processed_chars = total_processed;
	return 0;
}

static int parse_all_section_xml(config_t* config,
			char* data_buffer, 
			unsigned int data_buffer_size,
			unsigned int data_buffer_offset)
{
	int rc;
	int total_processed, processed_chars;
	char tag_name[MAX_STR_LEN], tag_attribute[MAX_STR_LEN];
	unsigned int offset = data_buffer_offset;
	char* tag_value;
	int tag_value_size;

	// scorre le varie sezioni
	total_processed = 0;
	while (total_processed < (int)data_buffer_size - 1) {
		char* buffer;
		unsigned int size;
		char section_name[MAX_STR_LEN], string[MAX_STR_LEN];
		unsigned int processed;

		// assegna il buffer
		buffer = data_buffer + offset;
		size = data_buffer_size - offset;

		// revupera il buffer della sezione
		rc = find_tag(buffer, size, &processed_chars,
			tag_name, tag_attribute, &tag_value, &tag_value_size);
		if (rc < 0) {
			break;
		}
		// incrementa l'offset
		offset += processed_chars;
		// incrementa il numero di caratteri processati
		total_processed += processed_chars;

		//
		// controlla la sezione
		//
		if (strcmp(tag_name, "section") != 0) {
			// non era una sezione valida: passa alla successiva
			free(tag_value);
			continue;
		}
		//cerca il nome della sezione
		strcpy(string, tag_attribute);
		strcpy(section_name, "GENERAL");
		if (strlen(string) != 0) {
			// cerca il nome
			char* cp;
			cp = strstr(string, "name");
			if (cp != NULL) {
				char *start, *end;
				start = strchr(cp, '"');
				if (start != NULL) {
					end = strchr(start+1, '"');
					if (end != NULL) {
						// ha trovato il nome della sezione
						*end = 0;
						strcpy(section_name, start + 1);
					}
				}
			}
		}
		// parserizza il contenuto della sezione
		rc = parse_section_xml(config, section_name,
							tag_value, tag_value_size, 0,
							&processed);
		// libera la memoria
		free(tag_value);
	}

	return 0;
}


static int config_read_file_xml(config_t* config, const char* filename)
{
    char name[MAX_STR_LEN - 100];
	FILE* fp;
	char string[MAX_STR_LEN];
	char* file_buffer;
	int file_buffer_size;
	char* data_buffer;
	int data_buffer_size;
	
	int processed_chars;
	char tag_attribute[MAX_STR_LEN];
	char* tag_value;
	int tag_value_size;

	char main_tag[MAX_STR_LEN];

	int rc;

	assert(config != NULL);

    if ( (filename != NULL) && (strlen(filename) > 0) )
    {
        if (strlen(filename) > (MAX_STR_LEN - 1))
        {
			sprintf(g_last_error, "Impossible to write a file with a name %d bytes long (max=1023)", strlen(filename));
			return -1;
		}
		strcpy(name, filename);
    }
    else
    {
		strcpy(name, config->m_config_file);
	}


	fp = fopen(name, "r");
    if (fp == NULL)
    {
        snprintf(g_last_error, MAX_STR_LEN, "Impossible to open file <%s>", name);
		return -1;
	}

	// mette il contenuto del file in un buffer
	file_buffer = (char *)calloc(MAX_STR_LEN, sizeof(char));
    if (file_buffer == NULL)
    {
		fclose(fp);
		free(file_buffer);
		return -1;
	}
	file_buffer_size = MAX_STR_LEN;

    while (!feof(fp))
    {
        if (fgets_u(string, MAX_STR_LEN, fp) == NULL)
        {
			break;
		}
		append_string(&file_buffer, &file_buffer_size, string);	
	}

	fclose(fp);

	// rimuove i commenti
	remove_comments(&file_buffer, &file_buffer_size);
    if (file_buffer == NULL)
    {
		// errore di memoria all'interno della funzione
		return -1;
	}

	// legge l'header
	rc = read_header(file_buffer, file_buffer_size);
    if (rc == -1)
    {
		free(file_buffer);
		return -1;
	}

	// imposta le dimensioni del buffer
	data_buffer = file_buffer + rc;
	data_buffer_size = file_buffer_size - rc;

	// recupera il contenuto del tag principale
	rc = find_tag(data_buffer, data_buffer_size,
					&processed_chars,
					main_tag, tag_attribute,
					&tag_value, &tag_value_size);
    if (rc == -1)
    {
		free(file_buffer);
		return -1;
	}

	// dealloca il buffer con tutto il file: da adesso si usa il contenuto
	// del tag principale
	free(file_buffer);

	// controlla se c'e' una sezione
    if (strncmp(tag_value, "<section", strlen("<section")) == 0)
    {
		// c'e' almeno una sezione: le scorre tutte
		rc = parse_all_section_xml(config, tag_value, tag_value_size, 0);
    }
    else
    {
		unsigned int processed;
		// non ci sono sezioni: si prende quella di default "GENERAL"
		rc = parse_section_xml(config, (char*)"GENERAL",
					tag_value, tag_value_size, 0,
					&processed);
	}

	// libera la memoria allocata 
	free(tag_value);
    return 0;
}


static void write_single_entry_xml(config_t* config, int index, FILE* fp)
{
	char value[MAX_STR_LEN];

	// a seconda del tipo di variabile imposta il dato
	switch (config->m_entries[index].m_type) {

	case T_char:
		sprintf(value,"%c", config->m_entries[index].m_value.m_char_value);
		break;

	case T_int:
		sprintf(value,"%d", config->m_entries[index].m_value.m_int_value);
		break;
		
	case T_float:
		sprintf(value,"%f", config->m_entries[index].m_value.m_float_value);
		break;

	case T_string:
		sprintf(value,"%s", config->m_entries[index].m_value.m_string_value);
		break;
	default:
		return;
	}

	my_fprintf(fp, "<%s> %s </%s>\n",
			config->m_entries[index].m_key_string, value,
			config->m_entries[index].m_key_string);
}


static int write_all_entries_xml(config_t*config, FILE* fp, char* header)
{
	config_entry_t* entries;
	int num_entries, i;
	char current_section[MAX_STR_LEN] = "\0";
	sprintf(current_section, "%s", "GENERAL");
	if (header == NULL) {
		header = (char*)DEFAULT_XML_HEADER;
	}

	entries = config->m_entries;
	num_entries = config->m_num_entries;

	my_fprintf(fp, "<%s>\n", header);

	// apre la sessione general
	my_fprintf(fp, "<section name=\"GENERAL\" >\n");

	for (i = 0; i < num_entries; i++) {
		if (entries[i].m_type == T_unused) {
			// chiave non impostata
			continue;
		}
		// controlla se la sezione della entry corrisponde a quella attuale
		if (strcmp(current_section, entries[i].m_section) != 0) {
			// chiude la precedente sezione
			my_fprintf(fp, "</section>\n");
			// crea una nuova sezione
			my_fprintf(fp, "<section name=\"%s\" >\n", entries[i].m_section);
			strcpy(current_section, entries[i].m_section);
		}
		// scrive l'entry
		write_single_entry_xml(config, i, fp);
	}
	
	// chiude la sezione aperta
	my_fprintf(fp, "</section>\n");

	my_fprintf(fp, "</%s>\n", header);

	return 0;
}


static int config_write_file_xml(config_t* config, const char* filename)
{
    char name[MAX_STR_LEN - 24];
	FILE* fp;
	char section[MAX_STR_LEN] = "\0";
	sprintf(section, "%s", "GENERAL");
	assert(config != NULL);


    if ( (filename != NULL) && (strlen(filename) > 0) )
    {
        if (strlen(filename) > (MAX_STR_LEN - 1))
        {
            sprintf(g_last_error, "Impossible to write a file with a name %d bytes long (max=1000)", strlen(filename));
			return -1;
		}
		strcpy(name, filename);
	} else {
		strcpy(name, config->m_config_file);
	}

	// il file non esiste: si crea un file nuovo
	fp = fopen(name, "w");
    if (fp == NULL)
    {
        snprintf(g_last_error, MAX_STR_LEN, "Failed open file <%s>", name);
		return -1;
	}

	// scrive l'header
	my_fprintf(fp, "<?xml version='1.0' ?>\n");

	// scrive i dati sul file
	write_all_entries_xml(config, fp, NULL);

	fclose(fp);
	return 0;
}

//------------------------------------------------
//
//  Funzioni di libreria
//
//------------------------------------------------

int setconfigvalue(config_key_t* config_keys, int index, int num_keys, int value)
{
	int i = 0;
	for (i=0;i<num_keys; i++){
		if (config_keys[i].m_index == index){
			config_keys[i].m_default_accepted = value;
			return 1;
		}
	}
	return 0;
}

void config_print_params(config_t* config, FILE * fp_out)
{
    int i;
	
	if ( fp_out == NULL ) {
		fp_out = stdout;
		//fp_out = console;
	}

    for (i = 0; i < config->m_num_entries; i++) {
		if (config->m_entries[i].m_type == T_unused) {
			// chiave non impostata
			continue;
		}
		
		// a seconda del tipo di variabile imposta il dato
		switch (config->m_entries[i].m_type) {

		case T_char:
			my_fprintf(fp_out, "\t[%s]\t%s = %c\n",
				config->m_entries[i].m_section,
				config->m_entries[i].m_key_string,
				config->m_entries[i].m_value.m_char_value);
			break;

		case T_int:
			my_fprintf(fp_out, "\t[%s]\t%s = %d\n",
				config->m_entries[i].m_section,
				config->m_entries[i].m_key_string,
				config->m_entries[i].m_value.m_int_value);
			break;

		case T_float:
			my_fprintf(fp_out, "\t[%s]\t%s = %f\n",
				config->m_entries[i].m_section,
				config->m_entries[i].m_key_string,
				config->m_entries[i].m_value.m_float_value);
			break;

		case T_string:
			my_fprintf(fp_out, "\t[%s]\t%s = '%s'\n",
				config->m_entries[i].m_section,
				config->m_entries[i].m_key_string,
				config->m_entries[i].m_value.m_string_value);
			break;
		}
    }
}


void config_print_last_error(FILE * fp)
{
	if (fp == NULL) {
		fp = stdout;
	}
	my_fprintf(fp, "%s\n", g_last_error);
	fflush(fp);
}

void config_get_last_error(char* string, int size)
{
	strncpy(string, g_last_error, size);
}

char* config_build_xml(const char* filename, const char* head)
{
	//append_string(char** src, unsigned int* src_size, char* string)
    char *res, section[MAX_STR_LEN], my_head[MAX_STR_LEN];
	int res_size;
	FILE* fp;

	strcpy(section, "GENERAL");
	strcpy(my_head, head);

    res_size = MAX_STR_LEN;
	// alloca una prima stringa
	res = (char*)malloc(res_size);
    if (res == NULL)
    {
		sprintf(g_last_error, "Memory error");
		return NULL;
	}
	// apre il file
	fp = fopen(filename, "r");
    if ( fp == NULL)
    {
		free(res);
		sprintf(g_last_error, "Impossible to open file <%s>", filename);
		return NULL;
	}
	// scrive l'intestazione xml
	strcpy(res, "<?xml version='1.0' encoding='UTF-8' standalone='no'?>\r\n");
	// scrive l'head
	append_string(&res, &res_size, "<");
	append_string(&res, &res_size, my_head);
	append_string(&res, &res_size, ">\r\n");

	// scrive la prima sezione (GENERAL)
	append_string(&res, &res_size, "\t<section name =\"");
	append_string(&res, &res_size, section);
	append_string(&res, &res_size, "\">\r\n");

	// scorre il file e costruisce l'xml
    while ( !feof(fp) )
    {
        char line[MAX_STR_LEN], option[MAX_STR_LEN], value[MAX_STR_LEN], *cp;

		// legge una riga
        if (fgets_u(line, MAX_STR_LEN, fp) == NULL) {
			break;
		}
		// elimina il fine riga
		cp = strchr(line, '\r');
		if (cp != NULL) {
			*cp = 0;
		}
		cp = strchr(line, '\n');
		if (cp != NULL) {
			*cp = 0;
		}
		// toglie i commenti
		cp = strchr(line, '#');
		if (cp != NULL) {
			*cp = 0;
		}
		// toglie spazi all'inizio ed alla fine
		trim_string(line);
		// se dopo queste operazioni la stringa e' vuota passa alla riga successiva
		if (strlen(line) == 0) {
			continue;
		}

		if (line[0] == '[') {
			// stiamo entrando in una sezione
			cp = strchr(line, ']');
			// controllo se la parentesi si chiude
			if (cp == NULL)
				continue; // passa alla riga successiva
			// faccio terminare la stringa
			*cp = 0;

			// copio la sezione corrente
			strcpy(section, &line[1]);
			
			// chiude la sezione precedente
			append_string(&res, &res_size, "\t</section>\r\n");
			// apre la nuova sezione
			append_string(&res, &res_size, "\t<section name =\"");
			append_string(&res, &res_size, section);
			append_string(&res, &res_size, "\">\r\n");

			// passa alla riga successiva
			continue;
		}

		// cerca lo spazio
		cp = strchr(line, '=');
		if (cp == NULL) {
			// cerca un tab
			cp = strchr(line, ' ');
			if (cp == NULL) {
				// cerca un =
				cp = strchr(line, '\t');
			}
		}
		// controlla se ha trovato qualcosa
		if (cp == NULL) {
			//errore: la riga non contiene parametri
			continue;
		}

		// copia il nome
		*cp = 0;
		strcpy(option, line);

		if (strlen(option) == 0) {
			continue;
		}

		// cerca l'inizio del valore
		cp++;
		// scorre la stringa (nota: il seguente ciclo finisce sicuramente perche'
		// ci sara' alla fine uno \0)
		while ( (*cp == ' ') || (*cp == '\t') || (*cp == '=')) {
			cp++;
		}
		strcpy(value, cp);

		// toglie gli spazi all'inizio ed alla fine
		trim_string(option);
		trim_string(value);

		// scrive l'xml del valore trovato
		append_string(&res, &res_size, "\t\t<");
		append_string(&res, &res_size, option);
		append_string(&res, &res_size, ">");
		append_string(&res, &res_size, value);
		append_string(&res, &res_size, "</");
		append_string(&res, &res_size, option);
		append_string(&res, &res_size, ">\r\n");
	}
	// chiude il file
	fclose(fp);

	// scrive la fine della sezione
	append_string(&res, &res_size, "\t</section>\r\n");

	// scrive la fine del tag head
	append_string(&res, &res_size, "</");
	append_string(&res, &res_size, my_head);
	append_string(&res, &res_size, ">\r\n");

	return res;
}


int config_file_set_param(const char* filename, const char* option, const char* value, const char* section)
{
	FILE *fp_orig, *fp_new;
    char curr_section[MAX_STR_LEN], temp_filename[MAX_STR_LEN - 24];
	int value_changed;
    char key_section[MAX_STR_LEN - 24], option_extended[MAX_STR_LEN];
	int found_sections;
	
    if ( (section == NULL) || (strlen(section) == 0) )
    {
		strcpy(key_section, "GENERAL");
    }
    else
    {
        snprintf(key_section, MAX_STR_LEN, "%s", section);
	}
	// costruisce una opzione con il nuovo formato
    snprintf(option_extended, MAX_STR_LEN, "%s.%s", key_section, option);

	value_changed = 0;
	strcpy(curr_section, "GENERAL");

	// apre il file di origine
	fp_orig = fopen(filename, "r");
    if ( fp_orig == NULL)
    {
        snprintf(g_last_error, MAX_STR_LEN, "Impossible to open file <%s>", filename);
		return -1;
	}
	// apre un file temporaneo
    snprintf(temp_filename, MAX_STR_LEN, "%s.tmp", filename);
	fp_new = fopen(temp_filename, "w");
    if ( fp_new == NULL)
    {
		fclose(fp_orig);
        if(snprintf(g_last_error, MAX_STR_LEN, "Impossible to open file <%s>", temp_filename) < 0)
        {
            return -1;
        }
		return -1;
	}

	// scorre il file
	found_sections = 0;
    while ( !feof(fp_orig) )
    {
        char line[MAX_STR_LEN], line_tmp[MAX_STR_LEN], curr_option[MAX_STR_LEN];
        char curr_value[MAX_STR_LEN], *cp, sep;

		// legge una riga
        if (fgets_u(line, MAX_STR_LEN, fp_orig) == NULL)
        {
			break;
		}
		// elimina il fine riga
		cp = strchr(line, '\r');
        if (cp != NULL)
        {
			*cp = 0;
		}
		cp = strchr(line, '\n');
        if (cp != NULL)
        {
			*cp = 0;
		}
		// toglie spazi all'inizio ed alla fine
		trim_string(line);

		// se dopo queste operazioni la stringa e' vuota o inizia con un commento passa alla riga successiva
        if ( (strlen(line) == 0) || (line[0] == '#'))
        {
			fprintf(fp_new, "%s\n", line);
			continue;
		}
		
		// copia la riga su una stringa temporanea
		strcpy(line_tmp, line);
		
		// elimina eventuali commenti a meta' riga
		cp = strchr(line_tmp, '#');
        if (cp != NULL)
        {
			*cp = 0;
		}

        if (line_tmp[0] == '[')
        {
			// stiamo entrando in una sezione
			cp = strchr(line_tmp, ']');
			// controllo se la parentesi si chiude
			if (cp == NULL)
            {
				continue; // passa alla riga successiva
            }
			// faccio terminare la stringa
			*cp = 0;

			// copio la sezione corrente
			strcpy(curr_section, &line_tmp[1]);

			fprintf(fp_new, "%s\n", line);
			found_sections = 1;

			// passa alla riga successiva
			continue;
		}
		
		// se si arriva qui significa che la riga corrente non e' un header di sezione
        if ( strncmp(option_extended, line, strlen(option_extended)) != 0 )
        {
            if (strcmp(curr_section, key_section) != 0)
            {
				// siamo in una sezione diversa da quella in cui vogliamo cmabiare i dati: inutile
				// andare avanti
				fprintf(fp_new, "%s\n", line);
				continue;
			}
		}

		// cerca l'uguale
		sep = '=';
		cp = strchr(line_tmp, '=');
        if (cp == NULL)
        {
			// cerca lo spazio
			sep = ' ';
			cp = strchr(line_tmp, ' ');
            if (cp == NULL)
            {
				// cerca un tab
				sep = '\t';
				cp = strchr(line_tmp, '\t');
			}
		}
		// controlla se ha trovato qualcosa
        if (cp == NULL)
        {
			//errore: la riga non contiene parametri
			continue;
		}

		// copia il nome
		*cp = 0;
		strcpy(curr_option, line_tmp);

        if (strlen(curr_option) == 0)
        {
			continue;
		}

		// cerca l'inizio del valore
		cp++;
		// scorre la stringa (nota: il seguente ciclo finisce sicuramente perche'
		// ci sara' alla fine uno \0)
        while ( (*cp == ' ') || (*cp == '\t') || (*cp == '='))
        {
			cp++;
		}
		strcpy(curr_value, cp);

		// toglie gli spazi all'inizio ed alla fine
		trim_string(curr_option);
		trim_string(curr_value);
		
		// se option coincide col nuovo valore lo cambia, altrimenti riscrive la riga
        if (strcmp(curr_option, option_extended) == 0)
        {
			fprintf(fp_new, "%s%c%s\n", option_extended, sep, value);
			value_changed = 1;
        }
        else if (strcmp(curr_option, option) == 0)
        {
			fprintf(fp_new, "%s%c%s\n", option, sep, value);
			value_changed = 1;
        }
        else
        {
			fprintf(fp_new, "%s\n", line);
		}
	}
	// chiude il file originale
	fclose(fp_orig);
    if (value_changed == 0)
    {
		// il valore non e' stato cambiato: dobbiamo aggiungerlo
		// controlla se deve aggiungere anche una sezione
        if ( found_sections == 0 )
        {
			// e' un file senza sezioni
            if ( (section == NULL) || (strlen(section) == 0) )
            {
				fprintf(fp_new, "%s=%s\n", option, value);
            }
            else
            {
				fprintf(fp_new, "%s.%s=%s\n", section, option, value);
			}
        }
        else
        {
            if (strcmp(curr_section, key_section) != 0)
            {
				fprintf(fp_new, "\n");
				fprintf(fp_new, "[%s]\n", key_section);
			}
			fprintf(fp_new, "%s=%s\n", option, value);
		}
	}
    // chiude il file temporaneo
	fclose(fp_new);
	
	// rinomina il file temporaneo su quello originale:
	// prioma elimina quello vecchio
	unlink(filename);
    if ( rename(temp_filename, filename) != 0)
    {
        if(snprintf(g_last_error, MAX_STR_LEN, "Failed rename file <%s> to <%s>", temp_filename, filename) < 0)
        {
            return -1;
        }
		return -1;
	}
	return 0;
}


int config_file_del_param(const char* filename, const char* option, const char* section)
{
	FILE *fp_orig, *fp_new;
    char curr_section[MAX_STR_LEN], temp_filename[MAX_STR_LEN];
    char key_section[MAX_STR_LEN], option_extended[MAX_STR_LEN];
	
	if ( (section == NULL) || (strlen(section) == 0) )
	{
		strcpy(key_section, "GENERAL");
	}
	else
	{
        snprintf(key_section, MAX_STR_LEN, "%s", section);
	}

	strcpy(curr_section, "GENERAL");
	
	// costruisce una opzione con il nuovo formato
    if(snprintf(option_extended, MAX_STR_LEN, "%s.%s", key_section, option) < 0)
    {
        return -1;
    }
	
	// apre il file di origine
	fp_orig = fopen(filename, "r");
	if ( fp_orig == NULL)
	{
        snprintf(g_last_error, MAX_STR_LEN, "Impossible to open file <%s>", filename);
		return -1;
	}
	// apre un file temporaneo
    snprintf(temp_filename, MAX_STR_LEN, "%s.tmp", filename);
	fp_new = fopen(temp_filename, "w");
	if ( fp_new == NULL )
	{
		fclose(fp_orig);
        if(snprintf(g_last_error, MAX_STR_LEN, "Impossible to open file <%s>", temp_filename) < 0)
        {
            return -1;
        }
		return -1;
	}

	// scorre il file e costruisce l'xml
	while ( !feof(fp_orig) )
	{
        char line[MAX_STR_LEN], line_tmp[MAX_STR_LEN], curr_option[MAX_STR_LEN], *cp;

		// legge una riga
        if (fgets_u(line, MAX_STR_LEN, fp_orig) == NULL)
		{
			break;
		}
		// elimina il fine riga
		cp = strchr(line, '\r');
		if (cp != NULL)
		{
			*cp = 0;
		}
		cp = strchr(line, '\n');
		if (cp != NULL)
		{
			*cp = 0;
		}
		// toglie spazi all'inizio ed alla fine
		trim_string(line);

		// se dopo queste operazioni la stringa e' vuota o inizia con un commento passa alla riga successiva
		if ( (strlen(line) == 0) || (line[0] == '#') )
		{
			fprintf(fp_new, "%s\n", line);
			continue;
		}
		
		// copia la riga su una stringa temporanea
		strcpy(line_tmp, line);
		
		// elimina eventuali commenti a meta' riga
		cp = strchr(line_tmp, '#');
		if (cp != NULL)
		{
			*cp = 0;
		}
		
		if (line_tmp[0] == '[')
		{
			// stiamo entrando in una sezione
			cp = strchr(line_tmp, ']');
			// controllo se la parentesi si chiude
			if (cp == NULL)
            {
				continue; // passa alla riga successiva
            }
			// faccio terminare la stringa
			*cp = 0;

			// copio la sezione corrente
			strcpy(curr_section, &line_tmp[1]);

			fprintf(fp_new, "%s\n", line);

			// passa alla riga successiva
			continue;
		}
		
		// se si arriva qui significa che la riga corrente non e' un header di sezione
		if ( strncmp(option_extended, line, strlen(option_extended)) != 0 )
		{
			if (strcmp(curr_section, key_section) != 0)
			{
				// siamo in una sezione diversa da quella in cui vogliamo cmabiare i dati: inutile andare avanti
				fprintf(fp_new, "%s\n", line);
				continue;
			}
		}

		// cerca l'uguale
		//sep = '=';
		cp = strchr(line_tmp, '=');
		if (cp == NULL)
		{
			// cerca lo spazio
			//sep = ' ';
			cp = strchr(line_tmp, ' ');
			if (cp == NULL)
			{
				// cerca un tab
				//sep = '\t';
				cp = strchr(line_tmp, '\t');
			}
		}
		// controlla se ha trovato qualcosa
		if (cp == NULL)
		{
			//errore: la riga non contiene parametri
			continue;
		}

		// copia il nome
		*cp = 0;
		strcpy(curr_option, line_tmp);

		if (strlen(curr_option) == 0)
		{
			continue;
		}

		// toglie gli spazi all'inizio ed alla fine
		trim_string(curr_option);
		
		// se option non coincide col nuovo valore riscrive la riga altrimenti
		// non fa niente e quindi elimina il valore
		if (strcmp(curr_option, option) == 0)
		{
			continue;
		}
		if (strcmp(curr_option, option_extended) == 0 )
		{
			continue;
		}
		
		// se arriva qui puo' scrivere la riga
		fprintf(fp_new, "%s\n", line);
	}
	// chiude il file originale
	fclose(fp_orig);

	// chiude il file temporaneo
	fclose(fp_new);
	
	// rinomina il file temporaneo su quello originale prima elimina quello vecchio
	unlink(filename);
	if ( rename(temp_filename, filename) != 0 )
	{
        if(snprintf(g_last_error, MAX_STR_LEN, "Impossible to rename file <%s> in <%s>", temp_filename, filename) < 0)
        {
            return -1;
        }
		return -1;
	}

	return 0;
}


config_t* config_init(config_key_t* keys, int num_keys, const char* filename)
{
	return config_init_with_default_check(keys, num_keys, filename, 1);
}


config_t* config_init_with_default_check(config_key_t* keys, int num_keys,
                                            const char* filename, int check_default)
{
	config_t* config;
	int i, j, dim;

	config = (config_t*)calloc(sizeof(config_t), 1);
    if (config == NULL)
    {
		sprintf(g_last_error, "Memory error, impossible to allocate %d bytes", sizeof(config_t));
		return NULL;
	}

	// copia il nome
    if ( (filename != NULL) && (strlen(filename) > 0) )
    {
        if (strlen(filename) > (MAX_STR_LEN - 1))
        {
			sprintf(g_last_error, "Impossible to write a file with a name %d bytes long (max=1023)", strlen(filename));
			free(config);
			return NULL;
		}
		strcpy(config->m_config_file, filename);
    }
    else
    {
		strcpy(config->m_config_file, "");
	}

	// adesso costruisce le strutture per le variabili
	// esegue un primo ciclo per cercare la chiave piu' alta
	dim = 0;
    if(num_keys!=0 && keys != NULL)
    {
        for(i = 0; i < num_keys; i++)
        {
            if(keys[i].m_index > dim)
            {
				dim = keys[i].m_index;
			}
            if(keys[i].m_index < 0)
            {
				// errore
				free(config);
				return NULL;
			}
		}
		dim++;
	}
	config->m_num_entries = dim;

    if(num_keys != 0 && keys != NULL)
    {
		// alloca il vettore con le variabili
		config->m_entries = (_config_entry*)calloc(sizeof(config_entry_t), dim);
        if (config->m_entries == NULL)
        {
			free(config);
			sprintf(g_last_error, "Memory error, impossibile to allocate %d bytes", sizeof(config_entry_t) * dim);
			return NULL;
		}

		// imposta tutte le chiavi come non usate
        for (i = 0; i < config->m_num_entries; i++)
        {
			config->m_entries[i].m_type = T_unused;
		}

		// imposta le chiavi ed i valori di default
        for(i = 0; i < num_keys; i++)
        {
			j = keys[i].m_index;
			SetEntry( &(keys[i]), &(config->m_entries[j]) );
		}
	}

	// Inizia l'head della lista dei parametri
	config->m_params_head=NULL;
	config->m_params_head=(struct config_param *)malloc(sizeof(struct config_param));
    if ( config->m_params_head == NULL)
    {
		sprintf(g_last_error, "Impossible to allocate memory for params\n");
		config_free(config);
		return NULL;
	}
    config->m_params_head->m_value = NULL;
    config->m_params_head->m_key = NULL;
    config->m_params_head->m_section = NULL;
    config->m_params_head->m_next = NULL;

	// legge il file di configurazione (se presente)
    if ( (filename != NULL) && (strlen(filename) > 0) )
    {
        if ( config_read_file(config, NULL) < 0)
        {
            config_free(config);
            if(snprintf(g_last_error, MAX_STR_LEN, "Impossible to read from file <%s>\n", config->m_config_file) < 0)
            {
                return NULL;
            }
			return NULL;
		}
	}

    if (check_default != 0 && (num_keys!=0 && keys != NULL))
    {
        if ( config_check_default_values(config, keys, num_keys) != 0 )
        {
			fflush(stdout);
			config_free(config);
			return NULL;
		}
	}

	// Ritorna il config inizializzato
	return config;
}

int config_check_default_values(config_t* config, config_key_t* keys, int num_keys)
{
	int i, index;
	
	// controlla che le chiavi con DEFAULT_UNACCEPTED non abbiano il valore di default
    for (i = 0; i < num_keys; i++)
    {
		// recupera l'indice della chiave i-esima
		index = keys[i].m_index;
		//printf("DEBUG: index = %d  config->m_entries[%d].m_key_string='%s'\n", index, index, config->m_entries[index].m_key_string);
		// controlla se la entry in posizione index e' soggetta al controllo sui default ed
		// in tal caso se il valore e' stato impostato o meno
		if( (keys[i].m_default_accepted == DEFAULT_UNACCEPTED) &&
            (config->m_entries[index].m_is_default == 1) )
        {
			sprintf(g_last_error, "Param '%s' (in section '%s') cannot have default value. Suggested value: <%s>",
            keys[i].m_key_string, keys[i].m_section, keys[i].m_default_value);
			return -1;
		}
		//printf("DEBUG: index = %d  FINE\n", index);
	}
	return 0;
}

int config_free(config_t* config)
{
	int i;
	config_entry_t * entry;

	if (config == NULL) {
		return -1;
	}
	// elimina l'area di memoria delle varie chiavi
    for (i = 0; i < config->m_num_entries; i++)
    {
		entry = &(config->m_entries[i]);
		free(entry->m_section);
		free(entry->m_key_string);
        if (entry->m_type == T_string)
        {
			// libera la ram delle stringhe
			free(entry->m_value.m_string_value);
		}
	}
	free(config->m_entries);

	struct config_param *head = config->m_params_head;
	struct config_param *cur = NULL;
    if(head != NULL)
    {
		cur = head->m_next;
		while(cur != NULL) {
            head->m_next = cur->m_next;
            if(cur->m_section != NULL)
            {
				free(cur->m_section);
			}
            if(cur->m_key != NULL)
            {
				free(cur->m_key);
			}
            if(cur->m_value != NULL)
            {
				free(cur->m_value);
			}
			free(cur);
			cur = head->m_next;
		}
		free(head);
	}

	// libera la struttura
	free(config);

	return 0;
}

void config_set_debug(config_t* config, int debug_val)
{
	config->m_is_debug = debug_val;
}

// a seconda dell'estensione legge un file xml o un file di testo semplice
int config_read_file(config_t* config, const char* filename)
{
    char name[MAX_STR_LEN];
	char* ext;
	int ret_val;

	assert(config != NULL);

    if ( (filename != NULL) && (strlen(filename) > 0) )
    {
        if (strlen(filename) > (MAX_STR_LEN - 1))
        {
			sprintf(g_last_error, "Impossible to write a file with a name %d bytes long (max=1023)", strlen(filename));
			return -1;
		}
		strcpy(name, filename);
    }
    else
    {
		strcpy(name, config->m_config_file);
	}

    if ( strlen(name) == 0 )
    {
		sprintf(g_last_error, "File name not specified");
		return -1;
	}

	ext = strrchr(name, '.');

    if ( (ext != NULL) && (strcmp(ext, ".xml") == 0) )
    {
		ret_val = config_read_file_xml(config, name);
    }
    else
    {
		ret_val = config_read_file_plain(config, name);
	}

	return ret_val;
}

int config_write_file(config_t* config, const char* filename)
{
    char name[MAX_STR_LEN];
	char* ext;
	int ret_val;

	assert(config != NULL);

    if ( (filename != NULL) && (strlen(filename) > 0) )
    {
        if (strlen(filename) > (MAX_STR_LEN - 1))
        {
			sprintf(g_last_error, "Impossible to write a file with a name %d bytes long (max=1023)", strlen(filename));
			return -1;
		}
		strcpy(name, filename);
    }
    else
    {
		strcpy(name, config->m_config_file);
	}

	ext = strrchr(name, '.');
    if ( (ext != NULL) && (strcmp(ext, ".xml") == 0) )
    {
		ret_val = config_write_file_xml(config, name);
    }
    else
    {
		ret_val = config_write_file_plain(config, name);
	}
	return ret_val;
}

// legge un char
char config_get_char(config_t* config, int index)
{
	assert(config != NULL);
	assert(index < config->m_num_entries);
	assert(config->m_entries[index].m_type == T_char);

    if ( config->m_is_debug && (config->m_entries[index].m_type == T_unused))
    {
		my_fprintf(stderr, "Key [%s] %s cannot be used\n", config->m_entries[index].m_section, config->m_entries[index].m_key_string);
		return 0;
	}
	return config->m_entries[index].m_value.m_char_value;
}

char config_get_param_char(config_t* config, const char *section,
		const char *key, char def)
{
	char *s = config_get_param_string(config,section,key,NULL);
    if(s == NULL || strlen(s) == 0)
    {
		return def;
    }
    else
    {
		return s[0];
	}
}

// legge un int
int config_get_int(config_t* config, int index)
{
	assert(config != NULL);
	assert(index < config->m_num_entries);
	assert(config->m_entries[index].m_type == T_int);
    if ( config->m_is_debug && (config->m_entries[index].m_type == T_unused))
    {
		my_fprintf(stderr, "Key [%s] %s cannot be used\n", config->m_entries[index].m_section, config->m_entries[index].m_key_string);
		return 0;
	}
	return config->m_entries[index].m_value.m_int_value;
}

int config_get_param_int(config_t* config, const char *section,
		const char *key, int def)
{
	char *s = config_get_param_string(config,section,key,NULL);
    if(s == NULL)
    {
		return def;
    }
    else
    {
		return my_atoi(s);
	}
}

// legge un float
float config_get_float(config_t* config, int index)
{
	assert(config != NULL);
	assert(index < config->m_num_entries);
	assert(config->m_entries[index].m_type == T_float);
    if ( config->m_is_debug && (config->m_entries[index].m_type == T_unused))
    {
		my_fprintf(stderr, "Key [%s] %s cannot be used\n", config->m_entries[index].m_section, config->m_entries[index].m_key_string);
		return 0;
	}
	return config->m_entries[index].m_value.m_float_value;
}

float config_get_param_float(config_t* config, const char *section,
		const char *key, float def)
{
	char *s = config_get_param_string(config,section,key,NULL);
    if(s == NULL)
    {
        return def;
    }
    else
    {
        return (float)atof(s);
    }
}

// legge una stringa
char* config_get_string(config_t* config, int index)
{
	assert(config != NULL);
	assert(index < config->m_num_entries);
	assert(config->m_entries[index].m_type == T_string);
	if ( config->m_is_debug && (config->m_entries[index].m_type == T_unused))
	{
		my_fprintf(stderr, "Key [%s] %s cannot be used\n", config->m_entries[index].m_section, config->m_entries[index].m_key_string);
		//return (char*)"";
		return strdup("");
	}
	return config->m_entries[index].m_value.m_string_value;
}

char *config_get_param_string(config_t* config, const char *section,
		const char *key, const char *def)
{
	struct config_param *cur = config->m_params_head->m_next;
    if(config == NULL || section == NULL || key == NULL)
    {
		cur = NULL;
	}
    while(cur != NULL)
    {
        if(cur->m_section != NULL && cur->m_key != NULL)
        {
            char key_composed[MAX_STR_LEN];

			// modalita' classica
            if ( strcmp(cur->m_section,section)==0 && strcmp(cur->m_key,key) == 0)
            {
				return cur->m_value;
			} 
			// modalita' a properties
            if ( strcmp(section, "") == 0)
            {
                strncpy(key_composed, key, (MAX_STR_LEN - 1));
            }
            else
            {
                snprintf(key_composed, (MAX_STR_LEN - 1), "%s.%s", section, key);
			}
            if ( strcmp(cur->m_section,"GENERAL")==0 && strcmp(cur->m_key,key_composed) == 0)
            {
				return cur->m_value;
			}
		}
		// passa al prossimo
		cur = cur->m_next;
	}
	return (char *)def;
}

int config_set_char(config_t* config, int index, const char c)
{
	assert(config != NULL);
	if(index >= config->m_num_entries)		return -1;
	if(config->m_entries[index].m_type != T_char)		return -1;
	config->m_entries[index].m_value.m_char_value = c;
	return 0;
}

int config_set_int(config_t* config, int index, const int i)
{
	assert(config != NULL);
	if(index >= config->m_num_entries)
		return -1;
	if(config->m_entries[index].m_type != T_int)
		return -1;
	config->m_entries[index].m_value.m_int_value = i;
	return 0;
}

int config_set_float(config_t* config, int index, const float f)
{
	assert(config != NULL);
	if(index >= config->m_num_entries)
		return -1;
	if(config->m_entries[index].m_type != T_float)
		return -1;
	config->m_entries[index].m_value.m_float_value = f;
	return 0;
}

int config_set_string(config_t* config, int index, const char* string)
{
	assert(config != NULL);
	if(index >= config->m_num_entries)
		return -1;
	if(config->m_entries[index].m_type != T_string)
		return -1;
	if(config->m_entries[index].m_value.m_string_value != NULL) {
		free(config->m_entries[index].m_value.m_string_value);
	}
	config->m_entries[index].m_value.m_string_value = strdup(string);
	return 0;
}

//int config_file_exist_param(const char* filename, const char* option, const char* section)
int config_file_exist_param(char* filename, char* option, char* section)
{
	config_key_t keys[] =
	{
			{ 1, option, section, T_string, (char*)"", DEFAULT_UNACCEPTED},
	};
	config_t *handle = config_init(keys, 1,filename);
	if (handle == NULL) {
		return -1;
	} else {
		config_free(handle);
		return 0;
	}
}
