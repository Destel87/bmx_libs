//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    DataCollector.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the DataCollector class.
//! @details
//!
//*!****************************************************************************

#ifndef DATACOLLECTOR_H
#define DATACOLLECTOR_H

#include <string>
#include <list>
#include <fstream>
#include "Loggable.h"
#include "TimeMeter.h"
#include "Mutex.h"

using namespace std;

typedef struct
{
	string m_sId;
	uint32_t m_nCounter;
	uint32_t m_nNumBits;
	TimeMeter m_timeMeter;
	bool m_bResult;
} dataElem;

class DataCollector : public Loggable
{

	public:

		/*! ***************************************************************************************
		 * Constructor
         * @param sDataCollectorId	an identifier for the container
		 * @param bDoCheck	before inserting a data element in the list performs a check if not
		 *					already inserted
		 * ****************************************************************************************
		 */
		DataCollector(string sDataCollectorId, bool bDoCheck = false);

        DataCollector(bool bDoCheck = false);

		void setId(string sId) { m_sId.assign(sId); }

		/*! ***************************************************************************************
		 * Default destructor
		 * ****************************************************************************************
		 */
		virtual ~DataCollector();

		/*! ***************************************************************************************
		 * Frees resources
		 * ****************************************************************************************
		 */
		void clear(void);

		/*! ***************************************************************************************
		 * Inserts a new data element in the list with proper parameters and save its initial
		 * timestamp
		 * @param sDataElementId	a string that identifies the data element
         * @param nDataElementCounter	an integer that identifies the data element
		 * @param nNumBits			the number of bits involved in the transaction represented
		 *							by the data element inserted in the list
		 * @return true if the insertion succeded, false otherwise
		 * ****************************************************************************************
		 */
		bool start(string sDataElementId, uint32_t nDataElementCounter, uint32_t nNumBits);

		/*! ***************************************************************************************
		 * Inserts a new data element in the back of the list with no use of parameters
		 * @param nNumBits			the number of bits involved in the transaction represented
		 *							by the data element inserted in the list
		 * @return true if the insertion succeded, false otherwise
		 * ****************************************************************************************
		 */
		bool start(uint32_t nNumBits);

		/*! ***************************************************************************************
		 * Finalizes the data element of the list identified by the parameters, saves
		 * inside it the actual timestamp and the operation outcome if specified
		 * @param sDataElementId	a string that identifies the data element
		 * @param nDataElementId	an integer that identifies the data element
		 * @param bOperationResult	a boolean the represents the outcome of the operation associa
		 *							ted with the data element
		 * ****************************************************************************************
		 */
		bool stop(string sDataElementId, uint32_t nDataElementId, bool bOperationResult = true);

		/*! ***************************************************************************************
		 * Finalizes the last data element of the list, saving in it the actual timestamp
		 * and the operation outcome if specified
		 * @param bOperationResult	a boolean the represents the outcome of the operation associa
		 *							ted with the data element
		 * ****************************************************************************************
		 */
		bool stop(bool bOperationResult = true);

		/*! ***************************************************************************************
		 * Prints (using the logger) the whole list of data elements and their content
		 * ****************************************************************************************
		 */
		void print(void);

		/*! ***************************************************************************************
		 * Searches in the list the data elements with the minimum duration and returns the
		 * minimum duration in microseconds
		 * @return	the minimum duration in microseconds among all the data elements
		 * ****************************************************************************************
		 */
		uint64_t getMinDurationUSec(void) const;

		/*! ***************************************************************************************
		 * Searches in the list the data elements with the maximum duration and returns the
		 * maximum duration in microseconds
		 * @return	the maximum duration in microseconds among all the data elements
		 * ****************************************************************************************
		 */
		uint64_t getMaxDurationUSec(void) const;

		/*! ***************************************************************************************
		 * Performs the sum calculation of the durations of every data element and returns it
		 * @return	the whole duration of the data collector in microseconds
		 * ****************************************************************************************
		 */
		uint64_t getTotalDurationUSec(void) const;

		/*! ***************************************************************************************
		 * Performs the calculation of the average of the durations of all the data elements
		 * and returns it
		 * @return	the average duration of all the data elements in microseconds
		 * ****************************************************************************************
		 */
		float getAvgDurationUsec(void) const;

		/*! ***************************************************************************************
		 * Returns the total number of bytes collected, given by the sum of the bytes of each
		 * data element
		 * @return the sum of the numBytes field of each data element
		 * ****************************************************************************************
		 */
		uint64_t getTotalNumBytes(void) const;

		/*! ***************************************************************************************
		 * Returns the total number of elements in the list, i.e. the total number of data
		 * elements
		 * @return the number of data elements in the list
		 * ****************************************************************************************
		 */
		uint64_t getTotalNumOperations(void) const;

		/*! ***************************************************************************************
		 * Counts and returns the number of the data elements that have the m_bResult field set to
		 * true, i.e. the number of succesful operations in the list
		 * elements
		 * @return number of data elements in the list that have the m_bResult field set to true
		 * ****************************************************************************************
		 */
		uint64_t getSuccessfulTotalNumOperations(void) const;

		/*! ***************************************************************************************
		 * Returns the bits per second of the specified data element, if 0 is specified returns the
		 * bytes per second calculated on the whole data elements list
		 * @param nDataElementId	the identifier of the data element on which the data rate is
		 *							calculated
		 * @return the bits per seconds calculated on the specified parameter
		 * ****************************************************************************************
		 */
		float getBitsPerSec(uint32_t nDataElementId = 0) const;

		/*! ***************************************************************************************
		 * Returns the bytes per second of the specified data element, if 0 is specified returns
		 * the bytes per second calculated on the whole data elements list
		 * @param nDataElementId	the identifier of the data element on which the data rate is
		 *							calculated
		 * @return the bytes per seconds calculated on the specified parameter
		 * ****************************************************************************************
		 */
		float getBytesPerSec(uint32_t nDataElementId = 0) const;

		/*! ***************************************************************************************
		 * Returns the Kbytes per second of the specified data element, if 0 is specified returns
		 * the Kbytes per second calculated on the whole data elements list
		 * @param nDataElementId	the identifier of the data element on which the data rate is
		 *							calculated
		 * @return the KBytes per seconds calculated on the specified parameter
		 * ****************************************************************************************
		 */
		float getKBytesPerSec(uint32_t nDataElementId = 0) const;

		/*! ***************************************************************************************
		 * Returns the ratio between the sum of the bytes of the dataElements theh have the
		 * m_bResult fìeld set to true over the total time duration of the data collector, i.e.
		 * the data throughput of the collected data in bytes per seconds
		 * @return the data throughput of the collected data in bytes per seconds
		 * ****************************************************************************************
		 */
		float getDataThroughputBytesPerSec(void) const;

		/*! ***************************************************************************************
		 * Returns the ratio between the sum of the bytes of the dataElements theh have the
		 * m_bResult fìeld set to true over the total time duration of the data collector, i.e.
		 * the data throughput of the collected data in KBytes per seconds
		 * @return the data throughput of the collected data in KBytes per seconds
		 * ****************************************************************************************
		 */
		float getDataThroughputKBytesPerSec(void) const;

		/*! ***************************************************************************************
		 * Returns the duration variance of all the dataElements of the list
		 * @return the duration variance of the collected data in micro seconds
		 * ****************************************************************************************
		 */
		float getVarianceDurationUSec(void) const;

		/*! ***************************************************************************************
		 * Returns the duration standard deviation of all the dataElements of the list
		 * @return the duration standard deviation of the collected data in micro seconds
		 * ****************************************************************************************
		 */
		float getStandardDeviationDurationUSec(void) const;

		/*! ***************************************************************************************
		 * Operator << to write data statistics to an ostream
		 * ****************************************************************************************
		 */
		friend ostream& operator<<(ostream& out, DataCollector& dc);
        friend ostream& operator<<(ostream& out, DataCollector* pDc);

        /*! ***************************************************************************************
         * Returns the cvs Header string
         * @return the cvs Header string
         * ****************************************************************************************
         */
        string getCsvHeaderString(void) const;

        /*! ***************************************************************************************
         * Returns the statistics print status flag
         * @return the statistics print status flag
         * ****************************************************************************************
         */
        bool getStatisticsPrinted(void);

        /*! ***************************************************************************************
         * Sets the statistics print status flag
         * @param bPrintIsDone the statistics print status flag
         * ****************************************************************************************
         */
        void setStatisticsPrinted(bool bPrintIsDone);

    private:

        string m_sId;
        list<dataElem> m_pDataElements;
        bool m_bDoCheck;
        uint64_t m_nCounter;
        Mutex m_Mutex;
        bool m_bStatisticsHaveBeenPrinted;

};

#endif // DATACOLLECTOR_H
