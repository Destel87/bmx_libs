QT -= core gui
TARGET = DataCollector
TEMPLATE = lib
CONFIG += staticlib

SOURCES += DataCollector.cpp
HEADERS += DataCollector.h

# internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../TimeStamp
DEPENDPATH += $$PWD/../TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../TimeStamp/libTimeStamp.a

unix:!macx: LIBS += -L$$OUT_PWD/../Log/ -lLog
INCLUDEPATH += $$PWD/../Log
DEPENDPATH += $$PWD/../Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../Thread/ -lThread
INCLUDEPATH += $$PWD/../Thread
DEPENDPATH += $$PWD/../Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Thread/libThread.a
