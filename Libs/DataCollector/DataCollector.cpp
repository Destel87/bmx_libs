//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    DataCollector.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the DataCollector class.
//! @details
//!
//*!****************************************************************************

#include <time.h>
#include <math.h>

#include "TimeStamp.h"
#include "DataCollector.h"


static inline long long int timespec_to_ns(const struct timespec *ts)
{
	return ((long long int) ts->tv_sec * NSEC_PER_SEC) + ts->tv_nsec;
}


DataCollector::DataCollector(string sDataCollectorId, bool bDoCheck)
{
	m_sId.assign(sDataCollectorId);
	clear();
	m_bDoCheck = bDoCheck;
	m_nCounter = 0;
	m_bStatisticsHaveBeenPrinted = false;
}

DataCollector::DataCollector(bool bDoCheck)
{
	m_sId.clear();
	clear();
	m_bDoCheck = bDoCheck;
	m_nCounter = 0;
	m_bStatisticsHaveBeenPrinted = false;
}

DataCollector::~DataCollector()
{
	clear();
}

void DataCollector::clear(void)
{
	m_pDataElements.clear();
}

bool DataCollector::start(string sDataElementId, uint32_t nDataElementCounter, uint32_t nNumBits)
{
	m_Mutex.lock();
	if ( m_bDoCheck )
	{
		list<dataElem>::iterator it;
		for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
		{
			if ( ( it->m_nCounter == nDataElementCounter ) && ( it->m_sId.compare(sDataElementId) == 0 ) )
			{
				m_Mutex.unlock();
				log(LOG_WARNING, "DataCollector: trying to insert element[%s:%d] already present",
					sDataElementId.c_str(), nDataElementCounter);
				return false;
			}
		}
	}
	dataElem newDataElem;
	newDataElem.m_sId.assign(sDataElementId);
	newDataElem.m_nCounter = nDataElementCounter;
	newDataElem.m_nNumBits = nNumBits;
	m_pDataElements.push_back(newDataElem);
	dataElem& pNewDataElem = m_pDataElements.back();
	pNewDataElem.m_timeMeter.start();
	m_Mutex.unlock();
	log(LOG_DEBUG_PARANOIC, "DataCollector: start element[%s:%d] numBytes=%d t=%llu",
		pNewDataElem.m_sId.c_str(), pNewDataElem.m_nCounter, pNewDataElem.m_nNumBits, pNewDataElem.m_timeMeter.getStart());
	return true;
}

bool DataCollector::start(uint32_t nNumBits)
{
	m_Mutex.lock();
	m_nCounter++;
	dataElem newDataElem;
	newDataElem.m_sId.assign(m_sId);
	newDataElem.m_nCounter = m_nCounter;
	newDataElem.m_nNumBits = nNumBits;
	m_pDataElements.push_back(newDataElem);
	dataElem& pNewDataElem = m_pDataElements.back();
	pNewDataElem.m_timeMeter.start();
	m_Mutex.unlock();
	log(LOG_DEBUG_PARANOIC, "DataCollector: start element[%s:%d] numBytes=%d t=%llu",
		pNewDataElem.m_sId.c_str(), pNewDataElem.m_nCounter, pNewDataElem.m_nNumBits, pNewDataElem.m_timeMeter.getStart());
	return true;
}

bool DataCollector::stop(string sDataElementId, uint32_t nDataElementId, bool bOperationResult)
{
	struct timespec endTime;
	clock_gettime(CLOCK_MONOTONIC, &endTime);

	bool bRetValue = false;

	m_Mutex.lock();
	dataElem& pLast = m_pDataElements.back();
	if ( m_bDoCheck )
	{
		list<dataElem>::iterator it;
		for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
		{
			if ( ( it->m_nCounter == nDataElementId ) && ( it->m_sId.compare(sDataElementId) == 0 ) )
			{
				pLast = *it;
				bRetValue = true;
				break;
			}
		}
	}

	if ( pLast.m_nCounter == nDataElementId )
	{
		log(LOG_DEBUG_PARANOIC, "DataCollector: stop for item with id=%d", nDataElementId);
		pLast.m_timeMeter.setEnd(&endTime);
		pLast.m_bResult = bOperationResult;
		bRetValue = true;
	}
	else
	{
		log(LOG_WARNING, "DataCollector: trying to stop item with id=%d that is not present", nDataElementId);
	}
	m_Mutex.unlock();
	return bRetValue;

}

bool DataCollector::stop(bool bOperationResult)
{
	struct timespec endTime;
	clock_gettime(CLOCK_MONOTONIC, &endTime);
	bool bRetValue = false;

	m_Mutex.lock();
	dataElem& pLast = m_pDataElements.back();
	if ( pLast.m_nCounter == m_nCounter )
	{
		pLast.m_timeMeter.setEnd(&endTime);
		pLast.m_bResult = bOperationResult;
		bRetValue = true;
	}
	m_Mutex.unlock();

	// Debug print
	if ( bRetValue == true )
		log(LOG_DEBUG_PARANOIC, "DataCollector: stop for item with id=%d", m_nCounter);
	else
		log(LOG_WARNING, "DataCollector: trying to stop item with id=%d that is not present", m_nCounter);

	return bRetValue;
}

void DataCollector::print(void)
{
	log(LOG_INFO, "DataCollector <%s> elements:", m_sId.c_str());
	list<dataElem>::iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		struct timespec t = it->m_timeMeter.getStart();
		int64_t nStart = timespec_to_ns(&t);
		t = it->m_timeMeter.getEnd();
		int64_t nEnd = timespec_to_ns(&t);
		log(LOG_INFO, "    Elem[%s;%d]: start=%lld end=%lld duration=%llu nsec",
			it->m_sId.c_str(), it->m_nCounter, nStart, nEnd, it->m_timeMeter.getDurationNanoSec());
	}
}

uint64_t DataCollector::getMinDurationUSec(void) const
{
	uint64_t nMinDuration = UINT64_MAX;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		uint64_t nMinDurationTmp = it->m_timeMeter.getDurationNanoSec();
		if (  nMinDurationTmp < nMinDuration )
		{
			nMinDuration = it->m_timeMeter.getDurationNanoSec();
		}
	}
	nMinDuration /= NSEC_PER_USEC;

	return nMinDuration;
}

uint64_t DataCollector::getMaxDurationUSec(void) const
{
	uint64_t nMaxDuration = 0;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		if ( it->m_timeMeter.getDurationNanoSec() > nMaxDuration )
			nMaxDuration = it->m_timeMeter.getDurationNanoSec();
	}
	nMaxDuration /= NSEC_PER_USEC;
	return nMaxDuration;
}

uint64_t DataCollector::getTotalDurationUSec(void) const
{
	uint64_t nTotalDuration = 0;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		nTotalDuration += it->m_timeMeter.getDurationNanoSec();
	}
	nTotalDuration /= NSEC_PER_USEC;
	return nTotalDuration;
}

float DataCollector::getAvgDurationUsec(void) const
{
	float nAvgDuration = 0;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		nAvgDuration += it->m_timeMeter.getDurationNanoSec();
	}
	nAvgDuration /= m_pDataElements.size();
	nAvgDuration /= NSEC_PER_USEC;
	return nAvgDuration;
}

uint64_t DataCollector::getTotalNumBytes(void) const
{
	uint64_t nTotalNumBytes = 0;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		nTotalNumBytes += it->m_nNumBits;
	}
	nTotalNumBytes /= 8;
	return nTotalNumBytes;
}

uint64_t DataCollector::getTotalNumOperations(void) const
{
	return (uint64_t)m_pDataElements.size();
}

uint64_t DataCollector::getSuccessfulTotalNumOperations(void) const
{
	uint64_t nSuccesfulTotalNumOperations = 0;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		if (it->m_bResult == true) nSuccesfulTotalNumOperations++;
	}
	return nSuccesfulTotalNumOperations;
}

float DataCollector::getBitsPerSec(uint32_t nDataElementId) const
{
	return (getBytesPerSec(nDataElementId) * 8);
}

float DataCollector::getBytesPerSec(uint32_t nDataElementId) const
{
	float fBytesPerSec = .0;
	float fNumBytes = .0;
	float fDurationSec = .0;
	if ( nDataElementId == 0 )
	{
		fNumBytes = (float)getTotalNumBytes();
		fDurationSec = (float)getTotalDurationUSec();
		fDurationSec /= (float)USEC_PER_SEC;
	}
	else if ( nDataElementId > 0 )
	{
		list<dataElem>::const_iterator it;
		for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
		{
			if ( it->m_nCounter == nDataElementId )
			{
				fNumBytes = (float)(it->m_nNumBits) / 8;
				fDurationSec = (float)it->m_timeMeter.getDurationNanoSec();
				fDurationSec /= (float)NSEC_PER_SEC;
			}
		}
	}
	fBytesPerSec = fNumBytes / fDurationSec;
	return fBytesPerSec;
}

float DataCollector::getKBytesPerSec(uint32_t nDataElementId) const
{
	return (getBytesPerSec(nDataElementId)/1000);
}

float DataCollector::getDataThroughputBytesPerSec(void) const
{
	float fTotalDurationSec = (float)getTotalDurationUSec() / USEC_PER_SEC;
	float fNumBytes = .0;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		if ( it->m_bResult == true )
		{
			fNumBytes += (((float)it->m_nNumBits)/8);
		}
	}
	float fDataThroughput = fNumBytes / fTotalDurationSec;
	return fDataThroughput;
}

float DataCollector::getDataThroughputKBytesPerSec(void) const
{
	return (getDataThroughputBytesPerSec()/1000);
}

float DataCollector::getVarianceDurationUSec(void) const
{
	float fDurationMeanUsec = getAvgDurationUsec();
	float fDurationVarianceUsec = 0;
	int nNumSamples = 0;
	list<dataElem>::const_iterator it;
	for ( it = m_pDataElements.begin(); it != m_pDataElements.end(); ++it )
	{
		float fTmp = it->m_timeMeter.getDurationNanoSec()/NSEC_PER_USEC;
		fTmp -= fDurationMeanUsec;
		fTmp = pow(fTmp, 2);
		fDurationVarianceUsec += fTmp;
		nNumSamples++;
	}
	if (nNumSamples > 1)
		fDurationVarianceUsec /= (nNumSamples-1);
	return fDurationVarianceUsec;
}

float DataCollector::getStandardDeviationDurationUSec(void) const
{
	return sqrt(getVarianceDurationUSec());
}

string DataCollector::getCsvHeaderString(void) const
{
	string sRetValue;
	sRetValue.clear();
	sRetValue.append("collectionId;");
	sRetValue.append("date;");
	sRetValue.append("numOperations;");
	sRetValue.append("numSuccessfulOperations;");
	sRetValue.append("totalDuration[uSec];");
	sRetValue.append("minDuration[uSec];");
	sRetValue.append("maxDuration[uSec];");
	sRetValue.append("avgDuration[uSec];");
	sRetValue.append("totalNumBytes[bytes];");
	sRetValue.append("dataRate[KBytes/sec];");
	sRetValue.append("dataThroughPut[KBytes/sec];");
	sRetValue.append("varDuration[uSec];");
	sRetValue.append("stdDuration[uSec];");

	return sRetValue;
}

ostream& operator<<(ostream& out, DataCollector& dc)
{

	// collectionId;Dyyyymmdd_Hhhmmss_Mxxx;numOperations;numSuccessfulOperations;
	// totalDurationUSec;minDurationUSec;maxDurationUSec;avgDurationUSec;totalNumBytes;
	// dataRate;dataThroughPut;varDurationUSec;stdDurationUSec
	TimeStamp date;
	char sDate[32] = "\0";
	date.getDateStringFromTimestamp(sDate);

	uint64_t nTotOperations = dc.getTotalNumOperations();
	uint64_t nTotSuccesfulOperations = dc.getSuccessfulTotalNumOperations();
	uint64_t nTotDurationUSec = dc.getTotalDurationUSec();

	uint64_t nMinDurationUSec = dc.getMinDurationUSec();
	uint64_t nMaxDurationUSec = dc.getMaxDurationUSec();
	float nAvgDurationUSec = dc.getAvgDurationUsec();

	uint64_t nTotalNumBytes = dc.getTotalNumBytes();
	float fKBytesPerSec = dc.getKBytesPerSec();
	float fDataThroughputKBytesPerSec = dc.getDataThroughputKBytesPerSec();

	double nDurationVarUSec = dc.getVarianceDurationUSec();
	double nDurationStdUSec = dc.getStandardDeviationDurationUSec();


	out << dc.m_sId << ";"
		<< sDate << ";"
		<< nTotOperations << ";"
		<< nTotSuccesfulOperations << ";"
		<< nTotDurationUSec << ";"
		<< nMinDurationUSec << ";"
		<< nMaxDurationUSec << ";"
		<< nAvgDurationUSec << ";"
		<< nTotalNumBytes << ";"
		<< fKBytesPerSec << ";"
		<< fDataThroughputKBytesPerSec << ";"
		<< nDurationVarUSec << ";"
		<< nDurationStdUSec << ";" ;

	dc.setStatisticsPrinted(true);

	return out;

}

ostream& operator<<(ostream& out, DataCollector* pDc)
{
	// collectionId;Dyyyymmdd_Hhhmmss_Mxxx;numOperations;numSuccessfulOperations;
	// totalDurationUSec;minDurationUSec;maxDurationUSec;avgDurationUSec;totalNumBytes;
	// dataRate;dataThroughPut;varDurationUSec;stdDurationUSec
	TimeStamp date;
	char sDate[32] = "\0";
	date.getDateStringFromTimestamp(sDate);

	uint64_t nTotOperations = pDc->getTotalNumOperations();
	uint64_t nTotSuccesfulOperations = pDc->getSuccessfulTotalNumOperations();
	uint64_t nTotDurationUSec = pDc->getTotalDurationUSec();

	uint64_t nMinDurationUSec = pDc->getMinDurationUSec();
	uint64_t nMaxDurationUSec = pDc->getMaxDurationUSec();
	float nAvgDurationUSec = pDc->getAvgDurationUsec();

	uint64_t nTotalNumBytes = pDc->getTotalNumBytes();
	float fKBytesPerSec = pDc->getKBytesPerSec();
	float fDataThroughputKBytesPerSec = pDc->getDataThroughputKBytesPerSec();

	double nDurationVarUSec = pDc->getVarianceDurationUSec();
	double nDurationStdUSec = pDc->getStandardDeviationDurationUSec();


	out << pDc->m_sId << ";"
		<< sDate << ";"
		<< nTotOperations << ";"
		<< nTotSuccesfulOperations << ";"
		<< nTotDurationUSec << ";"
		<< nMinDurationUSec << ";"
		<< nMaxDurationUSec << ";"
		<< nAvgDurationUSec << ";"
		<< nTotalNumBytes << ";"
		<< fKBytesPerSec << ";"
		<< fDataThroughputKBytesPerSec << ";"
		<< nDurationVarUSec << ";"
		<< nDurationStdUSec << ";" ;

	pDc->setStatisticsPrinted(true);

	return out;
}

bool DataCollector::getStatisticsPrinted(void)
{
	return m_bStatisticsHaveBeenPrinted;
}

void DataCollector::setStatisticsPrinted(bool bPrintIsDone)
{
	m_bStatisticsHaveBeenPrinted = bPrintIsDone;
}
