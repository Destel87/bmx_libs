//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerConnection.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the WebServerConnection class.
//! @details
//!
//*!****************************************************************************

#ifndef WEBSERVER_CONNECTION_H
#define WEBSERVER_CONNECTION_H

#include <string>
#include <vector>
#include <map>
#include <sstream>
#include "Mutex.h"

#include "WebServerInclude.h"

typedef struct _WebServerConnectionStringPair
{
	char name[128];
	char value[2048];
	struct _WebServerConnectionStringPair* next;
} WebServerConnectionStringPair_t;

/*! *******************************************************************************************************************
* @struct	WebServerConnection_t
* @short	Contains all the connection data and the socket used for the communication
*			This class is used to read data passed by the client and to send the response
* *********************************************************************************************************************
*/
typedef struct
{
	// client socket descriptor obtained from the accept() method
	int m_liClientSocketDescriptor;
	// flag that indicates if the header has been sent
	int m_liHeaderSent;
	// requested file (with parameters)
	char m_sFileName[4096];
	// requested page
	char m_sPageName[1024];
	// request method (POST or GET)
	char m_sRequestMethod[4096];
	// values contained in the header of the request
	WebServerConnectionStringPair_t* m_pRequestHeaderLines;
	// client address
	char m_sClientAddr[1048];
	// client port
	unsigned short int m_nClientPort; // NOTE: IN HOST BYTE ORDER
	// received parameters
	WebServerConnectionStringPair_t* m_pParams;
	// server name
	char m_sServerName[1024];
	// mime type of the response
	char m_sMimeType[1024];
	// lines to add to the header of the response
	WebServerConnectionStringPair_t* m_pHeaderLines;

	// string containing the full body of the HTTP request
	// used for POST requests for any content-type different from text/html
	char* m_sRequestBody;
	// length of the string m_sRequestBody, used for calloc() at instantiation
	unsigned int uliBodyLength;

	// cache to send
	unsigned char* m_rgucCache;
	// size of the cache
	unsigned int m_uliCacheSize;
	// size of data in the cache
	unsigned int m_uliCacheDataSize;

	// parametri per invio di dati in multipart
	//boundary di una frame
	char m_sBoundary[1024];
	// numero di buffer spediti
	unsigned int m_nBoundaryCounter;
} WebServerConnection_t;



/*!
* @fn void WebServerConnection_Init()
* @brief WebServerConnection_Init	allocates and initializes the connection
* @param pConn pointer to the connection
*/
WebServerConnection_t* WebServerConnection_Init();

/*!
* @fn void WebServerConnection_Release(EngWebSrvConnection_t* pConn)
* @brief inizializza una connessione azzerando tutti i parametri: NOTA BENE
* se esistono buffers allocati NON vengono rilasciati
* @param pConn puntatore alla connessione
*/
void WebServerConnection_Release(WebServerConnection_t* pConn);

/*!
* @fn void WebServerConnection_IsOpen(EngWebSrvConnection_t* pConn)
* @brief controlla se una connessione e' aperta o meno
* @param pConn la connessione
* @return 1 se la connessione e' aperta e 0 altrimenti
*/
int WebServerConnection_IsOpen(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_Duplicate
* @brief Duplica i dati di una connessione trasferendo il socket. Con questa funzione
* viene allocata una nuova connessione su cui vengono copiati tutti i dati. <br>
* Il socket viene assegnato alla nuova connessione e quindi ogni operazione
* di scrittura sulla connessione corrente fallira'
* @param pConn la connessione
* @return la nuova connessione
*/
WebServerConnection_t* WebServerConnection_Duplicate(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_CloseSocketSimple
* @brief chiude il socket senza eseguire lo shutdown: questa operazione DEVE essere eseguita
* SOLTANTO in caso di fork sul processo padre
* @param pConn la connessione
*/
void WebServerConnection_CloseSocketSimple(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_Reset
* @brief resetta la connessione chiudendo il socket e svuotando la cache
* @param pConn la connessione
*/
void WebServerConnection_Reset(WebServerConnection_t* pConn);

/*!
* Recupera un parametro a partire dal nome
* @param pConn la connessione
* @param name nome del parametro da cercare
* @return se trova il parametro ritorna il suo valore, altrimenti NULL
*/
const char* WebServerConnection_FindParam(WebServerConnection_t* pConn, const char* name);

/*!
* @fn WebServerConnection_GetParams
* @brief Recupera un vettore con tutti i parametri della connessione. I parametri sono
* memorizzati come coppie nome - valore.
* @param pConn la connessione
* @return una lista con con le coppie di parametri. Se non ci sono parametri ritorna NULL
*/
WebServerConnectionStringPair_t* WebServerConnection_GetParams(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_GetPage
* @brief Recupera il nome della pagina richiesta dal client
* @param pConn la connessione
* @return il nome della pagina
*/
const char* WebServerConnection_GetPage(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_GetFullPath
* @brief Recupera il file richiesto dal client. Il file e' composto dalla
* pagina richiesta piu' tutti gli eventuali parametri passati in GET
* @return il nome del file
*/
const char* WebServerConnection_GetFullPath(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_FindRequestHeader
* Recupera un header della richiesta a partire dal nome
* @param pConn la connessione
* @param name nome dell'header da cercare
* @return se trova l'header ritorna il suo valore, altrimenti NULL
*/
const char* WebServerConnection_FindRequestHeader(WebServerConnection_t* pConn, const char* name);

/*!
* @fn WebServerConnection_GetRequestHeaders
* @brief Recupera un vettore con tutti gli headers della richiesta. Gli headers sono
* memorizzati come coppie nome - valore.
* @param pConn la connessione
* @return una lista con con le coppie di headers. Se non ci sono parametri ritorna NULL
*/
WebServerConnectionStringPair_t* WebServerConnection_GetRequestHeaders(WebServerConnection_t* pConn);

/*!
* Recupera il metodo della richiesta (POST, GET, etc.)
* @param pConn la connessione
* @return se trova l'header ritorna il suo valore, altrimenti NULL
*/
const char* WebServerConnection_GetRequestMethod(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_GetUserAgent
* @brief Recupera l'user agent del client
* @param pConn la connessione
* @return il nome dell'user agent
*/
const char* WebServerConnection_GetUserAgent(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_GetRequestHost
* @brief Recupera l'host richiesto dal client
* @param pConn la connessione
* @return il nome dell'host
*/
const char* WebServerConnection_GetRequestHost(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_GetClientAddr
* @brief Recupera l'indirizzo ip usato dal client per la connessione
* @param pConn la connessione
* @return indirizzo ip del client
*/
const char* WebServerConnection_GetClientAddr(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_GetRequestBody
* @brief Recupera il body ottenuto dal client durante la connessione
* @param pConn la connessione
* @return il body della request
*/
const char* WebServerConnection_GetRequestBody(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_SetClientAddr
* @brief Imposta l'indirizzo ip usato dal client per la connessione
* @param pConn la connessione
* @param sClientAddr indirizzo del client recuperato dopo aver ricevuto la connessione
* @return
*/
void WebServerConnection_SetClientAddr(WebServerConnection_t* pConn, const char* sClientAddr);

/*!
* @fn WebServerConnection_GetClientPort
* @brief Recupera la porta usata dal client per la connessione
* @param pConn la connessione
* @return la porta del client
*/
unsigned short int WebServerConnection_GetClientPort(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_SetClientPort
* @brief Imposta la porta usata dal client per la connessione
* @param pConn la connessione
* @param usiClientPort porta del client su cui il server e' in ascolto
* @return
*/
void WebServerConnection_SetClientPort(WebServerConnection_t* pConn, unsigned short int usiClientPort);

/*!
* @fn WebServerConnection_SetServerName
* @brief Imposta il nome del server per la connessione
* @param pConn la connessione
* @param sServerName nome del server per la connessione
* @return
*/
void WebServerConnection_SetServerName(WebServerConnection_t* pConn, const char* sServerName);

/*!
* @fn WebServerConnection_SetMimeType
* @brief Imposta il mime type che verra' usato per inviare la risposta al
* client
* @param pConn la connessione
* @param type il mime type desiderato
*/
void WebServerConnection_SetMimeType(WebServerConnection_t* pConn, const char* type);

/*!
* @fn WebServerConnection_SetBoundary(EngWebSrvConnection_t* pConn, char* boundary)
* @brief Imposta il boundary che verra' usato per inviare la risposta in multipart al
* client
* @param pConn la connessione
* @param boundary il boundary desiderato
*/
void WebServerConnection_SetBoundary(WebServerConnection_t* pConn, const char* boundary);

/*!
* @fn WebServerConnection_DisableCache
* @brief disabilita l'utilizzo della cache
* @param pConn la connessione
*/
void WebServerConnection_DisableCache(WebServerConnection_t* pConn);

/*!
* @fn WebServerConnection_EnableCache
* @brief abilita l'utilizzo della cache
* @param pConn la connessione
* @param nCacheSize dimensione della cache (se vale -1 usa il valore di default)
*/
void WebServerConnection_EnableCache(WebServerConnection_t* pConn, int nCacheSize);



int WebServerConnection_SendBuffer(WebServerConnection_t* pConn, int response_status, unsigned char* buffer, unsigned int buffer_size, char* error);
int WebServerConnection_SendString(WebServerConnection_t* pConn, int response_status, const char* str, char* error);
/*!
* @fn WebServerConnection_SendBuffer
* @brief Invia un buffer al client e poi chiude la connessione. Una volta chiusa
*	la connessione non sara' piu' possibile inviare altri dati
* @param pConn la connessione
* @param buffer buffer da inviare al client
* @param buffer_size dimensione del buffer da inviare
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return 0 in caso di esecuzione corretta ed 1 altrimenti
*/
int WebServerConnection_SendBufferWithOk200(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error);

/*!
* Invia una stringa al client e poi chiude la connessione. Una volta chiusa
* la connessione non sara' piu' possibile inviare altri dati
* @param pConn la connessione
* @param str stringa da inviare al client
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return true in caso di esecuzione corretta e false altrimenti
*/
int WebServerConnection_SendStringWithOk200(WebServerConnection_t* pConn, const char* str, char* error);

/*!
* @fn WebServerConnection_WriteBuffer
* @brief Invia un buffer al client senza chiudere la connessione.
* @param pConn la connessione
* @param buffer buffer da inviare al client
* @param buffer_size dimensione del buffer da inviare
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return 0 in caso di esecuzione corretta ed 1 altrimenti
*/
int WebServerConnection_WriteBuffer(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error);

/*!
* @fn WebServerConnection_WriteString
* Invia una stringa al client e poi chiude la connessione.
* @param pConn la connessione
* @param str stringa da inviare al client
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return 0 in caso di esecuzione corretta ed 1 altrimenti
*/
int WebServerConnection_WriteString(WebServerConnection_t* pConn, const char* str, char* error);

/*!
* @fn WebServerConnection_WriteElement(EngWebSrvConnection_t* pConn,
		unsigned char* buffer, unsigned int buffer_size, char* error)
* @brief Invia un buffer al client in multipart.
* @param pConn la connessione
* @param buffer buffer con l'elemento da inviare al client
* @param size dimensione dell'elemento da inviare
* @param sMimeType mime type del buffer inviato
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return 0 in caso di esecuzione corretta ed 1 altrimenti
*/
int WebServerConnection_WriteElement(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int size, const char* sMimeType, char* error);

/*!
* @fn WebServerConnection_SendNotFound
* @brief Invia al client una risposta http con codice di errore 404 (<b>Not found</b>)
*  ed in seguito chiude la connessione
* @param pConn la connessione
* @param buffer buffer da inviare al client (tale buffer viene tipicamente
*  visualizzato dal browser, se non viene inviato il browser usa una pagina
*  di default)
* @param buffer_size dimensione del buffer suddetto
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return 0 in caso di esecuzione corretta ed 1 altrimenti
*/
int WebServerConnection_SendNotFound(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error);

/*!
* @fn WebServerConnection_SendRedirect
* @brief Invia al client una risposta http con codice di 303 (<b>Redirect</b>)
*	ed in seguito chiude la connessione
* @param pConn la connessione
* @param url url verso cui eseguire il redirect
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return true in caso di esecuzione corretta e false altrimenti
*/
int WebServerConnection_SendRedirect(WebServerConnection_t* pConn, const char* url, char* error);

/*!
* @fn WebServerConnection_SendInternalError
* @brief Invia al client una risposta html con codice di errore 505 (<b>Internal Server
* Error </b>) ed in seguito chiude la connessione
* @param pConn la connessione
* @param buffer buffer da inviare al client (tale buffer viene tipicamente
*  visualizzato dal browser, se non viene inviato il browser usa una pagina
*  di default)
* @param buffer_size dimensione del buffer suddetto
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return 0 in caso di esecuzione corretta ed 1 altrimenti
*/
int WebServerConnection_SendInternalError(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error);

/*!
* @fn WebServerConnection_AddHeaderLine
* @brief Aggiunge una riga da scrivere nell'header html dopo quelli "classici"
* @param pConn la connessione
* @param line riga da aggiungere nell'header
*/
void WebServerConnection_AddHeaderLine(WebServerConnection_t* pConn, const char* line);

/*!
* @fn WebServerConnection_AddHeader
* @brief Aggiunge una header html da inserire dopo quelli "classici"
* @param pConn la connessione
* @param name nome dell'header
* @param value valore dell'header
*/
void WebServerConnection_AddHeader(WebServerConnection_t* pConn, const char* name, const char* value);

/*!
* @fn WebServerConnection_Flush
* @brief Svuota la cache e la scrive sul socket
* @param pConn la connessione
* @param error puntatore ad una stringa dove viene scritto un
*  eventuale messaggio di errore
* @return 0 in caso di esecuzione corretta ed 1 altrimenti
*/
int WebServerConnection_Flush(WebServerConnection_t* pConn, char* error);


void WebServerConnection_SetSocket(WebServerConnection_t* pConn, int sock);
int WebServerConnection_GetHeader(WebServerConnection_t* pConn, char* error);


class WebServerConnection
{

    public:
        WebServerConnection();
        WebServerConnection(WebServerConnection_t* pConn, bool external_connection);
        ~WebServerConnection();
        void Reset();
        void CloseSocketSimple();
        bool IsOpen();
        bool Flush(char* error = NULL);
        void SetSocket(int sock);
        int GetHeader(char* error = NULL);
        std::string FindParam(std::string name);
        std::vector< std::pair<std::string, std::string> > GetParams();
        std::string GetRequestMethod();
        std::string GetPage();
        std::string GetFullPath();
        std::string GetUserAgent();
        std::string GetRequestHost();
        std::string FindRequestHeader(std::string name);
        std::vector< std::pair<std::string, std::string> > GetRequestHeaders();
        std::string GetClientAddr();
        std::string GetRequestBody();
        void SetClientAddr(std::string sClientAddr);
        unsigned short int GetClientPort();
        void SetClientPort(unsigned short int usiClientPort);
        void SetServerName(std::string sServerName);
        void SetMimeType(std::string type) ;
        void SetBoundary(std::string boundary) ;
        void DisableCache();
        void EnableCache(int nCacheSize = -1);
        bool SendBuffer(unsigned char* buffer, unsigned int buffer_size, char* error = NULL);
        //bool SendString(std::string str, char* error = NULL);
        bool SendString(std::string str, int response_status = HTTP_RET_CODE_OK, char* error = NULL);
        bool WriteBuffer(unsigned char* buffer, unsigned int buffer_size, char* error = NULL);
        bool WriteString(std::string str, char* error = NULL);
        bool SendNotFound(unsigned char* buffer = NULL, unsigned int buffer_size = 0, char* error = NULL);
        bool SendRedirect(std::string url, char* error = NULL);
        bool SendInternalError(unsigned char* buffer = NULL, unsigned int buffer_size = 0, char* error = NULL);
        void AddHeaderLine(std::string line);
        void AddHeader(std::string name, std::string value) ;
        WebServerConnection* Duplicate();
        WebServerConnection_t* getInternalData(void);

	protected:
		WebServerConnection(WebServerConnection_t* pConn);

    protected:
        WebServerConnection_t* m_pConn;
        bool m_bExternalConn;

    private:
        friend class WebServer;

};

#endif //WEBSERVER_CONNECTION_H
