//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerInclude.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the general functions for the WebServer lib.
//! @details
//!
//*!****************************************************************************

#include "WebServerInclude.h"
#include <ctype.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <cstdio>
#include <errno.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>

int hexToInt(int liCharAsHexValue)
{
	liCharAsHexValue = toupper(liCharAsHexValue);
	liCharAsHexValue = liCharAsHexValue - '0';
	if ( liCharAsHexValue > 9 ) liCharAsHexValue = liCharAsHexValue - 'A' + '9' + 1;
	return liCharAsHexValue;
}

int urlDecode(char* sUrl)
{
	char* s0 = sUrl;
	char* t = sUrl;
	int n = strlen(sUrl);
	while ( n > 0 )
	{
		int c = *sUrl++;
		if ( ( c == '%' ) && ( n > 2 ) )
		{
			c = *sUrl++;
			int c1 = c;
			c = *sUrl++;
			c = 16*hexToInt(c1) + hexToInt(c);
			n -= 2;
		}
		*t++ = c;
		n--;
	}
	*t = 0;
	return t - s0;
}

void setFlags(int liFD, int liFlags)
{
	int liRetVal = fcntl(liFD, F_GETFL, 0);
	if ( liRetVal  < 0 )
		return;
	liRetVal |= liFlags;
	if ( fcntl(liFD, F_SETFL, liRetVal) < 0 )
		return;
}

void errorPrintf(char* sError, const char*fmt, ...)
{
	va_list ap;
	if (sError == NULL)
	{
		return;
	}
	va_start(ap, fmt);
	vsprintf(sError, fmt, ap);
	va_end(ap);
	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("%s\n", error);
	#endif
}

int getSocketError()
{
	return errno;
}

const char* getSocketErrorMessage(int liMsgNumber)
{
	return strerror(liMsgNumber);
}

int tcpWrite(int liSocketDescriptor, unsigned char* rgucBuffer, unsigned int uliBufferSize, char* sError)
{

	unsigned int uliTotalSize = uliBufferSize;
	unsigned int uliSentBytes = 0;

	while ( uliSentBytes < uliTotalSize )
	{
		unsigned char* pucTmpBuffer = rgucBuffer + uliSentBytes;
		int liRemainingBytesToSend = (int)uliTotalSize - (int)uliSentBytes;
		int liTmpSize = SOCKET_STEP_SIZE;
		if ( liRemainingBytesToSend <= liTmpSize )
		{
			liTmpSize = liRemainingBytesToSend;
		}
		int liRetVal = send(liSocketDescriptor, (char*)pucTmpBuffer, liTmpSize, 0);
		unsigned int uliEagainCnt = 0;
		if ( ( liRetVal == -1 ) && ( errno == EAGAIN ) )
		{
			uliEagainCnt++;
			usleep(1000);
			if ( uliEagainCnt > SOCKET_WRITE_TIMEOUT_MSEC )
			{
				errorPrintf(sError, "Error code from send %d", liRetVal);
				return 1;
			}
			continue;
		}
		uliEagainCnt = 0;
		if ( liRetVal < 0 )
		{
			int liErrorCode = getSocketError();
			if ( errno == EPIPE )
			{
				errorPrintf(sError, "Connection closed by the client");
			}
			else
			{
				errorPrintf(sError, "Error from send (%s)", getSocketErrorMessage(liErrorCode));
			}
			return 1;
		}
		uliSentBytes += liRetVal;
		//printf("Written on socket %d bytes (%d / %d)\n", rc, sent_bytes, total_size);
	}
	//printf("Written on socket %d bytes in total\n", buffer_size);
	return 0;
}

int tcpRead(int liSocketDescriptor, unsigned char* rgucBuffer, unsigned int uliBufferSize, int liTimeoutMsec, char* sError)
{
	if ( liTimeoutMsec >= 0 )
	{
		struct timeval tv;
		fd_set rfds;
		FD_ZERO(&rfds);
		FD_SET(liSocketDescriptor, &rfds);
		tv.tv_sec = liTimeoutMsec / 1000;
		tv.tv_usec = (liTimeoutMsec % 1000) * 1000;
		int liRetVal = select(liSocketDescriptor + 1, &rfds, NULL, NULL, &tv);
		if ( liRetVal == -1 )
		{
			errorPrintf(sError, "Error from select()");
			return -1;
		}
		else if ( liRetVal == 0 )
		{
			// no data
			return 0;
		}
		if ( !FD_ISSET(liSocketDescriptor, &rfds))
		{
			return 0;
		}
	}
	int liReadBytes = recv(liSocketDescriptor, (char*)rgucBuffer, uliBufferSize, MSG_NOSIGNAL);
	if ( liReadBytes < 0 )
	{
		int liErrorCode = getSocketError();
		if ( liErrorCode == EAGAIN )
		{
			return 0;
		}
		else if ( liErrorCode == EPIPE )
		{
			errorPrintf(sError, "recv: client closed the connection (header 1)");
			return -1;
		}
		errorPrintf(sError, "error from recv (%s)", getSocketErrorMessage(liErrorCode));
		return -1;
	}
	if ( liReadBytes == 0 )
	{
		errorPrintf(sError, "recv: client closed the connection (header 2)");
		return 0;
	}
	return liReadBytes;
}

double getTimer(void)
{
	struct timeval  tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + (tv.tv_usec / 1e6);
}

void usiToString(unsigned short int usiInputVal, char* sOutputString)
{
	sprintf(sOutputString, "%d", (int)usiInputVal);
}

void substituteChar(char* sStringToParse, const char cActualCharValue, const char cNewCharValue)
{
	for ( unsigned int i = 0; i < strlen(sStringToParse); i++ )
	{
		if ( sStringToParse[i] == cActualCharValue )
		{
			sStringToParse[i] = cNewCharValue;
		}
	}
}

int setCloexecFlag(int liFD, int liValue)
{
	if ( liFD == -1 )
	{
		return -1;
	}
	int liFlags = fcntl(liFD, F_GETFD, 0);
	// Set just the flag we want to set
	if ( liValue != 0 )
	{
		liFlags |= FD_CLOEXEC;
	}
	else
	{
		liFlags &= ~FD_CLOEXEC;
	}
	// Store modified flag word in the descriptor.
	return fcntl(liFD, F_SETFD, liFlags);
}

void getRfc1123Time(char* sOutputTimeString)
{
	struct timeval tv;
	struct tm *tms;
	gettimeofday(&tv, NULL);
	tms = localtime((time_t*)&tv.tv_sec); // Make sure tz is set the first time...
	strftime(sOutputTimeString, 64, "%a, %d %b %Y %H:%M:%S GMT%z", tms);
}

void buildCurrentTime(char* sOutputTimeString)
{
	struct timeval tv;
	struct tm *tms, loc_time;
	gettimeofday(&tv, NULL);
	tms = localtime((time_t*)&tv.tv_sec);
	loc_time = *tms;
	strftime(sOutputTimeString, 64, "%a, %d %b %Y %H:%M:%S GMT%z", &loc_time);
}
