//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServer.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the WebServer class.
//! @details
//!
//*!****************************************************************************

#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>
#include <iostream>

#include "WebServer.h"
#include "WebServerInclude.h"

bool TestProcessConnection(WebServerConnection* pConn, void* counter)
{
	char error[1024];

	int* pCounter = (int*)counter;
	(*pCounter)++;

	std::string sMethod = pConn->GetRequestMethod();
	std::vector< std::pair<std::string, std::string> > pParams = pConn->GetParams();
	std::vector< std::pair<std::string, std::string> > pRequestHeaders = pConn->GetRequestHeaders();
	std::string sPage = pConn->GetPage();
	std::string sClientAddr = pConn->GetClientAddr();
	unsigned short int usiClientPort = pConn->GetClientPort();
	std::string sRequestHost = pConn->GetRequestHost();
	std::string sUserAgent = pConn->GetUserAgent();
	std::vector<std::pair<std::string, std::string> >::iterator it;

	std::stringstream os;
	// html body start
	os << "<html>";
	os << "<body>";
	{

		// Server info
		os << "<hr>";
		{
			os << "Server version: " << WebServer::getVersion();
			os << "<br>";
		}

		// Connection info
		os << "<hr>"		<< std::endl;
		{
			os << "Method:         " << sMethod			<< "<br>";
			os << "Client addr:    " << sClientAddr		<< "<br>";
			os << "Client port:    " << usiClientPort	<< "<br>";
			os << "Host:           " << sRequestHost	<< "<br>";
			os << "User Agent:     " << sUserAgent		<< "<br>";
			os << "Connection no.: " << (*pCounter)		<< "<br>";
		}

		// Headers and parameters received
		os << "<hr>";
		os << "<table border=0>";
		{
			os << "<table border=1>";
			{

				// First row start
				os << "<tr>";
				{
					// First row: cell 00
					os << "<td>";
					os << "&nbsp";
					os << "Page";
					os << "</td>";

					// First row: cell 01
					os << "<td>";
					os << "&nbsp";
					os << sPage;
					os << "</td>";
				}
				// First row end
				os << "</tr>";

				// Second row start
				os << "<tr>";
				{
					// Second row: cell 10
					os << "<td>";
					os << "&nbsp";
					os << "Parameters";
					os << "</td>";

					// Second row: cell 11
					os << "<td>";
					os << "<table border=0>";
					for ( it = pParams.begin(); it != pParams.end(); it++ )
					{

						os << "<tr>";

						os << "<td>";
						os << (*it).first;
						os << "</td>";

						os << "<td>";
						os << "&nbsp=&nbsp";
						os << "</td>";

						os << "<td>";
						os << (*it).second;
						os << "</td>";

						os << "</tr>";

					}
					os << "</table>";
					os << "</td>";
				}
				// Second row end
				os << "</tr>";

				// Third row start
				os << "<tr>";
				{
					// Third row: cell 20
					os << "<td>";
					os << "&nbsp";
					os << "Request headers";
					os << "&nbsp";
					os << "</td>";

					// Third row: cell 21
					os << "<td>";
					os << "<table border=1 frame=hsides>";
					for ( it = pRequestHeaders.begin(); it != pRequestHeaders.end(); it++ )
					{

						os << "<tr>";

						os << "<td>";
						os << "&nbsp";
						os << (*it).first;
						os << "&nbsp";
						os << "</td>";

						os << "<td>";
						os << "&nbsp";
						os << (*it).second;
						os << "&nbsp";
						os << "</td>";

						os << "</tr>";

					}
					os << "</table>";
					os << "</td>";
				}
				// Third row end
				os << "</tr>";
			}
			os << "</table>";
		}
		os << "</table>";
	}
	// html body end
	os << "</body>";
	os << "</html>";

	std::string sOutStr = os.str();
	pConn->SetMimeType(WEBSERVER_MIME_TEXT_HTML);

	if ( !pConn->SendString(sOutStr, HTTP_RET_CODE_OK, error) )
	{
		std::cout << error << endl;
		return false;
	}
	std::string sCurrTime = WebServer::currentTime();
	std::cout << sCurrTime << " (CnTest) [INFO_] received connection from " << sClientAddr << ":" << usiClientPort << std::endl;
	return true;
}

/*! *******************************************************************************************************************
  * Signal handling
  * *******************************************************************************************************************
  */
static int sliPipeSignalIgnore = 0;

void pipeSignalInit()
{
	// the first time we get here indicates to ignore SIGPIPE
	if ( !sliPipeSignalIgnore )
	{
		sliPipeSignalIgnore = 1;
		signal(SIGPIPE, SIG_IGN);
	}
}

/*! *******************************************************************************************************************
  * WebServer class implementation
  * *******************************************************************************************************************
  */
void WebServer::closeSocket(void)
{
	if ( m_liServerSocketDescriptor > 0 )
	{
		close(m_liServerSocketDescriptor);
	}
	m_liServerSocketDescriptor = -1;
}

bool WebServer::openSocket(char* sError)
{

	closeSocket();

	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::openSocket: start\n"); fflush(stdout);
	#endif
	m_liServerSocketDescriptor = (int)socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if ( m_liServerSocketDescriptor < 0 )
	{
		errorPrintf(sError, "error during main socket creation");
		return false;
	}
	int liSocketFlags = 1;
	int liRetValue = setsockopt(m_liServerSocketDescriptor, SOL_SOCKET, SO_REUSEADDR, (const char*)&liSocketFlags, sizeof(int));
	if ( liRetValue < 0 )
	{
		closeSocket();
		errorPrintf(sError, "setsockopt failed");
		return false;
	}

	struct sockaddr_in sin;
	sin.sin_family = AF_INET;
	if ( strcmp(m_strListenAddress.c_str(), "") == 0 )
	{
		sin.sin_addr.s_addr = INADDR_ANY;
	}
	else
	{
		sin.sin_addr.s_addr = inet_addr(m_strListenAddress.c_str());
	}
	sin.sin_port = htons(m_usiListenPort);

	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::openSocket: binding main socket to address and port\n"); fflush(stdout);
	#endif
	liRetValue = bind(m_liServerSocketDescriptor, (struct sockaddr *)&sin, sizeof(sin));
	if ( liRetValue < 0 )
	{
		closeSocket();
		errorPrintf(sError, "bind failed");
		return false;
	}
	setCloexecFlag(m_liServerSocketDescriptor, 1);

	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::openSocket: main socket correctly opened\n"); fflush(stdout);
	#endif

	return true;

}

bool WebServer::acceptConnection(WebServerConnection* pConn)
{
	struct sockaddr_in cliaddr;
	socklen_t uliSockaddrInSize = sizeof(cliaddr);
	int liClientSocketDescriptor = (int)accept(m_liServerSocketDescriptor, (struct sockaddr *)&cliaddr, &uliSockaddrInSize);
	if ( liClientSocketDescriptor < 0 )
	{
		return false;
	}

	setCloexecFlag(liClientSocketDescriptor, 1);

	char sClientAddress[128];
	strncpy(sClientAddress, inet_ntoa(cliaddr.sin_addr), 127);
	int liClientPort = ntohs(cliaddr.sin_port);
	setFlags(liClientSocketDescriptor, O_NONBLOCK);

	// set the client socket
	pConn->SetSocket(liClientSocketDescriptor);

	// set the client data
	string sClientAddr(sClientAddress);
	pConn->SetClientAddr(sClientAddr);
	pConn->SetClientPort((unsigned short int)liClientPort);

	// set the server name
	pConn->SetServerName(m_strServerName);

	return true;
}

WebServer::WebServer()
{
	m_strListenAddress[0] = '\0';
	m_usiListenPort = 0;
	m_liServerSocketDescriptor = 0;
	m_strServerName[0] = '\0';
	m_uliSupportedNumClients = WEBSERVER_DEFAULT_NUM_CLIENTS;
}

WebServer::WebServer(unsigned short int usiListenPort)
{
	init("", usiListenPort);
}

WebServer::WebServer(string strListenAddress, unsigned short int usiListenPort)
{
	init(strListenAddress, usiListenPort);
}

WebServer::~WebServer()
{
	// Nothing to be done here yet...
}

void WebServer::init(string strListenAddress, unsigned short int usiListenPort, unsigned int uliMaxNumClients)
{
	m_strListenAddress.assign(strListenAddress);
	m_usiListenPort = usiListenPort;
	m_liServerSocketDescriptor  = -1;
	m_strServerName.assign(WEBSERVER_DEFAULT_NAME);
	m_strServerName += "/";
	m_strServerName += WEBSERVER_VERSION;
	if ( uliMaxNumClients == 0 )
	{
		m_uliSupportedNumClients = 1;
	}
	else if ( uliMaxNumClients > WEBSERVER_MAX_NUM_CLIENTS )
	{
		m_uliSupportedNumClients = WEBSERVER_MAX_NUM_CLIENTS;
	}
	else
	{
		m_uliSupportedNumClients = uliMaxNumClients;
	}
	// Initialize signal handlers
	pipeSignalInit();
}

const char* WebServer::getVersion()
{
	return WEBSERVER_VERSION;
}

string WebServer::getListenWebAddress(void)
{
	return m_strListenAddress;
}

void WebServer::setListenWebAddress(string strWebAddress)
{
	m_strListenAddress.clear();
	m_strListenAddress.assign(strWebAddress);
}

unsigned short int WebServer::getListenWebPort(void)
{
	return m_usiListenPort;
}

void WebServer::setListenWebPort(unsigned short int usiWebPort)
{
	m_usiListenPort = usiWebPort;
}

string WebServer::getServerName()
{
	return m_strServerName;
}

void WebServer::setServerName(std::string sServerName)
{
	m_strServerName.clear();
	m_strServerName.assign(sServerName);
}

bool WebServer::isActive()
{
	return ( m_liServerSocketDescriptor > 0 ) ? true : false;
}

void WebServer::setSupportedNumClients(unsigned int uliSupportedNumClients)
{
	m_uliSupportedNumClients = uliSupportedNumClients;
}

bool WebServer::startListening(string sListenAddress, unsigned short int usiListenPort, char* sError)
{
	setListenWebAddress(sListenAddress);
	setListenWebPort(usiListenPort);
	return startListening(sError);
}

bool WebServer::startListening(unsigned short int usiListenPort, char* sError)
{
	setListenWebPort(usiListenPort);
	return startListening(sError);
}

bool WebServer::startListening(char* sError)
{

	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::startListening: opening main socket\n"); fflush(stdout);
	#endif
	if ( openSocket(sError) == false )
	{
		return false;
	}

	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::startListening: main socket correctly opened\n"); fflush(stdout);
	#endif

	if ( listen(m_liServerSocketDescriptor, m_uliSupportedNumClients) < 0 )
	{
		int err = getSocketError();
		closeSocket();
		errorPrintf(sError, "failed listen() (%s)", getSocketErrorMessage(err));
		return false;
	}

	setFlags(m_liServerSocketDescriptor, O_NONBLOCK);

	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::startListening: listen() on main socket done\n"); fflush(stdout);
	#endif

	return true;

}

void WebServer::stopListening()
{
	closeSocket();
	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::stopListening: closeSocket done\n"); fflush(stdout);
	#endif
}

bool WebServer::waitConnection(WebServerConnection* pConnection, int liTimeoutMilliSeconds)
{
	if ( pConnection == NULL )
	{
		return false;
	}
	pConnection->Reset();

	#ifdef WEBSERVER_ENABLE_DEBUG
	printf("WebServer::waitConnection: now accepting requests\n"); fflush(stdout);
	#endif

	// Prepares a HTTP connection, if timeout is specified select() is executed
	if ( liTimeoutMilliSeconds >= 0 )
	{
		fd_set rfds;
		FD_ZERO(&rfds);
		FD_SET(m_liServerSocketDescriptor, &rfds);
		struct timeval tv;
		tv.tv_sec = liTimeoutMilliSeconds / 1000;
		tv.tv_usec = (liTimeoutMilliSeconds % 1000) * 1000;
		int rc = select(m_liServerSocketDescriptor + 1, &rfds, NULL, NULL, &tv);
		if ( rc == -1 )		// error, check ERRNO
		{
			return false;
		}
		else if ( rc == 0 )	// timeout with no requests
		{
			return false;
		}
		if ( !FD_ISSET(m_liServerSocketDescriptor, &rfds) )
		{
			return false;
		}
	}

	bool bAccepted = acceptConnection(pConnection);
	if ( !bAccepted )
	{
		pConnection->Reset();
		return false;
	}
	int liRetValueFromGetHeader = pConnection->GetHeader(NULL);
	if ( liRetValueFromGetHeader != 0 )
	{
		#ifdef WEBSERVER_ENABLE_DEBUG
		printf("WebServer::waitConnection: error retrieving headers\n"); fflush(stdout);
		#endif
		pConnection->Reset();
		return false;
	}

	return true;
}

bool WebServer::waitConnection(WebServerConnection& rConnection, int liTimeoutMilliSeconds)
{
	return waitConnection(&rConnection, liTimeoutMilliSeconds);
}

std::string WebServer::currentTime(void)
{
	char sTime[64];
	struct timeval curr_time;
	time_t curr_sec;
	int liMsec;
	gettimeofday(&curr_time, NULL);
	curr_sec = curr_time.tv_sec;
	liMsec = curr_time.tv_usec / 1000;
	sprintf(sTime, "%.19s.%03d", ctime(&curr_sec), liMsec);
	return std::string(sTime);
}
