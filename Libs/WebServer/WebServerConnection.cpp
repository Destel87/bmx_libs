//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerConnection.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the WebServerConnection class.
//! @details
//!
//*!****************************************************************************

#include "WebServerConnection.h"
#include "WebServerInclude.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>

/*! *******************************************************************************************************************
 * Internal functions for WebServerConnection_t library
 * ********************************************************************************************************************
 */
static WebServerConnectionStringPair_t* DuplicateStringPairs(WebServerConnectionStringPair_t* oldList)
{
	WebServerConnectionStringPair_t *ret, *prev;

	ret = NULL;
	prev = NULL;
	while ( oldList != NULL )
	{
		WebServerConnectionStringPair_t* newPair;
		// allocates a new element
		newPair = (WebServerConnectionStringPair_t*)calloc(sizeof(WebServerConnectionStringPair_t), 1);
		// copies data
		strcpy(newPair->name, oldList->name);
		strcpy(newPair->value, oldList->value);
		newPair->next = NULL;
		// parse old list
		oldList = oldList->next;
		// parse new list
		if ( prev != NULL )
		{
			prev->next = newPair;
		}
		prev = newPair;
		if ( ret != NULL )
		{
			ret = newPair;
		}
	}

	return ret;
}

static void FreeStringPairs(WebServerConnectionStringPair_t* list)
{
	while ( list != NULL )
	{
		WebServerConnectionStringPair_t* tmp;
		tmp = list;
		list = list->next;
		free(tmp);
	}
}

char* FindStringInStringPairs(WebServerConnectionStringPair_t* list, const char* name)
{
	while ( list != NULL )
	{
		if ( strcmp(list->name, name) == 0 )
		{
			return list->value;
		}
		list = list->next;
	}
	// parameter not found
	return NULL;
}

static void WebServerConnection_InsertParam(WebServerConnection_t* pConn, const char* name, const char* value)
{
	if ( pConn == NULL )
		return;
	WebServerConnectionStringPair_t* newPair;
	// allocates a new element
	newPair = (WebServerConnectionStringPair_t*)calloc(sizeof(WebServerConnectionStringPair_t), 1);
	// copies data in the new element
	strncpy(newPair->name, name, 127);
	strncpy(newPair->value, value, 2047);
	// insert int the top of the list
	newPair->next = pConn->m_pParams;
	pConn->m_pParams = newPair;
}

static void WebServerConnection_InsertRequestHeader(WebServerConnection_t* pConn, const char* name, const char* value)
{
	if ( pConn == NULL )
		return;

	WebServerConnectionStringPair_t* newPair;
	// allocates a new element
	newPair = (WebServerConnectionStringPair_t*)calloc(sizeof(WebServerConnectionStringPair_t), 1);
	// copies data in the new element
	strcpy(newPair->name, name);
	strcpy(newPair->value, value);
	// delete possible : from name
	if (newPair->name[strlen(newPair->name) - 1] == ':')
	{
		newPair->name[strlen(newPair->name) - 1] = 0;
	}
	// insert in the top of the list
	newPair->next = pConn->m_pRequestHeaderLines;
	pConn->m_pRequestHeaderLines = newPair;
}

static void WebServerConnection_ParseQuery(WebServerConnection_t* pConn, char* query)
{
	if ( pConn == NULL )
		return;

	char* query_tmp;
	//char* cp;
	char* offset;

	query_tmp = strdup(query);

	offset = query_tmp;

	while ( strlen(offset) > 0 )
	{
		char tmp[1024], name[1024], value[1024];
		char *cp;
		// parse parameters
		cp = strchr(offset, '&');
		if ( cp != NULL )
		{
			*cp = 0;
			strcpy(tmp, offset);
			offset = cp + 1;
		}
		else
		{
			// parameters are over
			strcpy(tmp, offset);
			//offset = ""; // TODO
			offset[0] = '\0';
		}
		//printf("tmp = %s\n", tmp);
		// separates name from value
		cp = strchr(tmp, '=');
		if (cp != NULL)
		{
			*cp = 0;
			strcpy(name, &tmp[0]);
			strcpy(value, cp + 1);
			//printf("name = %s     value = %s\n", name, value);
			urlDecode(name);
			urlDecode(value);
			WebServerConnection_InsertParam(pConn, name, value);
		}
	}

	free(query_tmp);
}

static void WebServerConnection_SaveBody(WebServerConnection_t* pConn, char* sBodyBuffer, unsigned int uliContentLength)
{
	if ( pConn == NULL )		return;

	SAFE_FREE(pConn->m_sRequestBody);
	pConn->uliBodyLength = (unsigned int)strlen(sBodyBuffer);

    if ( uliContentLength != pConn->uliBodyLength )
    {
		printf("Warning, content-length=%u, bodBufferLength=%u\n", uliContentLength, pConn->uliBodyLength);
    }

	if ( pConn->uliBodyLength > 0 )
	{
		pConn->m_sRequestBody = strndup(sBodyBuffer, pConn->uliBodyLength);
	}
}

int WebServerConnection_GetHeader(WebServerConnection_t* pConn, char* error)
{
	if ( pConn == NULL )
		return -1;

	const char* end_of_line = "\r\n";
	int rc, offset, eol_size;
	//char method[16];
	char line_buffer[4096]; // buffer che contiene una riga
	int buflen = 4095;

	offset = 0;
	eol_size = (int)strlen(end_of_line);

	while (1)
	{
		char *cp;
		int timeout = 5000;

		// Reads a character
		rc = tcpRead(pConn->m_liClientSocketDescriptor, (unsigned char*)&line_buffer[offset], 1, timeout, error);
		if ( rc < 0 )
		{
			// error, exit
			line_buffer[offset] = '\0';
			return -1;
		}
		else if ( rc == 0 )
		{
			// end of reading, terminate string
			line_buffer[offset] = '\0';
			return offset;
		}
		//printf("%c", buffer[offset]);
		offset++;
		if ( offset >= buflen )
		{
			// end of available buffer
			return -1;
		}
		// terminate string
		line_buffer[offset] = '\0';
		// find the end of  the header
		if ( offset < eol_size )
		{
			// current line not completed, continue reading
			continue;
		}
		// find the end of line
		cp = &line_buffer[offset - eol_size];
		if ( strcmp(cp, end_of_line) != 0 )
		{
			// line has not ended, continue reading
			continue;
		}
		// if we get here the line is completed so delete the \r\n sequence
		*cp = 0;
		//printf("%s\n", line_buffer);
		// if the line is empty the header has been completely parsed
		if ( strcmp(line_buffer, "") == 0 )
		{
			break;
		}
		// parse the line
		if ( ( strncmp("GET ", line_buffer, 4)  == 0 ) || ( strncmp("POST ", line_buffer, 5) == 0 ) )
		{
			// this line contains the GET or POST request
			char *cp1, *query;
			char str[4096];
			cp = strchr(line_buffer, ' ');
			if ( cp != NULL )
			{
				*cp = 0;
			}
            sprintf(pConn->m_sRequestMethod, "%s", line_buffer);
			cp++;
			// delete the space character after the name
			cp1 = strchr(cp, ' ');
			if ( cp1 != NULL )
			{
				*cp1 = 0;
			}
			// copy the name
			strcpy(pConn->m_sFileName, cp);
			// delete a possible absolute url from the name of the file
			if ( strncmp(cp, "http://", 7) == 0 )
			{
				strcpy(pConn->m_sFileName, strchr(&cp[7], '/'));
			}
			// searches the page name in the filename
			strncpy(str, pConn->m_sFileName, 4095);
			cp = strchr(str, '?');
			if ( cp == NULL )
			{
				// there are no params
				strcpy(pConn->m_sPageName, str);
				urlDecode(pConn->m_sPageName);
				//query = "";
				query = NULL;
				query = (char*)calloc(1, sizeof(char));
			}
			else
			{
				*cp = 0;
				strcpy(pConn->m_sPageName, str);
				urlDecode(pConn->m_sPageName);
				query = cp + 1;
			}
			// parse the query
			WebServerConnection_ParseQuery(pConn, query);
		}
		else
		{
			cp = strchr(line_buffer, ' ');
			if (cp != NULL)
			{
				*cp = 0;
				WebServerConnection_InsertRequestHeader(pConn, line_buffer, cp+1);
			}
		}
		// reset the line to start a new one in the next loop
		offset = 0;
	}
	urlDecode(pConn->m_sFileName);

	// in the case of POST request a buffer must be read
	if ( strcmp(pConn->m_sRequestMethod, "POST") == 0 )
	{

		// cfind content type and content lenght
		const char* sContentType;
		const char* sContentLength;

		sContentType = WebServerConnection_FindRequestHeader(pConn, "Content-Type");
		sContentLength = WebServerConnection_FindRequestHeader(pConn, "Content-Length");
		if ( sContentLength != NULL )
		{
            unsigned char *buffer, *bufferFree;
			int size;

			size = atoi(sContentLength);
			if (size > 0)
			{
				int remaining;
				int timeout = 5000;

				offset = 0;
                buffer = (unsigned char*)calloc(size + 1, 1);
				if (buffer == NULL)
				{
					return -1;
				}
				remaining = size;
                bufferFree = buffer;
				while (remaining > 0)
				{
					rc = tcpRead(pConn->m_liClientSocketDescriptor, buffer + offset, remaining, timeout, error);
					if (rc <= 0)
					{
						break;
					}
					remaining -= rc;
                    offset += rc;
				}
				buffer[size] = 0;
				//printf("%s\n", (char*)buffer);fflush(stdout);
				urlDecode((char*)buffer);

				// if content type is of text/html we insert couples of type name=value
				// otherwise the full body request is saved in the connection as it is
				if ( (sContentType != NULL) && (strcmp(sContentType, WEBSERVER_MIME_TEXT_HTML) == 0) )
				{
					WebServerConnection_ParseQuery(pConn, (char*)buffer);
				}
				else
				{
					WebServerConnection_SaveBody(pConn, (char*)buffer, size);
				}
                free(bufferFree);
			}
		}
	}
	return 0;
}

static void WebServerConnection_CloseSocket(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return;

	WebServerConnection_Flush(pConn, NULL);
	if ( pConn->m_liClientSocketDescriptor != -1 )
	{
		shutdown(pConn->m_liClientSocketDescriptor, 2);
		close(pConn->m_liClientSocketDescriptor);
		pConn->m_liClientSocketDescriptor = -1;
	}

}

void WebServerConnection_SetSocket(WebServerConnection_t* pConn, int sock)
{
	if ( pConn == NULL )
		return;

	if ( pConn->m_liClientSocketDescriptor != -1 ) {
		close(pConn->m_liClientSocketDescriptor);
	}
	pConn->m_liClientSocketDescriptor = sock;
}

static void WebServerConnection_MakeHttpHeader(WebServerConnection_t* pConn, int type, int len, char* header)
{
	if ( pConn == NULL )
		return;

	char now[128], tmp[1024];
	WebServerConnectionStringPair_t* list;

	strcpy(header, rfc2616_protocol);
	switch ( type )
	{
		case HTTP_RET_CODE_OK:
			strcat(header, rfc2616_status_200);
			break;
		case HTTP_RET_CODE_CREATED:
			strcat(header, rfc2616_status_201);
			break;
		case HTTP_RET_CODE_ACCEPTED:
			strcat(header, rfc2616_status_202);
			break;
		case HTTP_RET_CODE_INTERNAL_ERR:
			strcat(header, rfc2616_status_505);
			break;
		case HTTP_RET_CODE_SEE_OTHER:
			strcat(header, rfc2616_status_303);
			break;
		default:
			strcat(header, rfc2616_status_404);
			break;
	}
	strcat(header, "\r\n");
	//printf("pConn->m_sServerName = %s\n", pConn->m_sServerName);
	buildCurrentTime(now);
    if(snprintf(tmp, 1024 - 1, "%s%s\r\n", rfc2616_server, pConn->m_sServerName) < 0) return;
	strcat(header, tmp);
    if(snprintf(tmp, 1024 - 1, "%s%s\r\n", rfc2616_date, now) < 0) return;
	strcat(header, tmp);

	if ( strcmp(pConn->m_sMimeType, WEBSERVER_MIME_TEXT_MULTIPART) == 0)
	{
        if(snprintf(tmp, 1024 - 1, "%s%s; boundary=%s\r\n", rfc2616_content_type, pConn->m_sMimeType, pConn->m_sBoundary) < 0) return;
	}
	else
	{
        if(snprintf(tmp, 1024 - 1, "%s%s\r\n", rfc2616_content_type, pConn->m_sMimeType) < 0) return;
	}

	strcat(header, tmp);
    if(snprintf(tmp, 1024 - 1, "%s%s\r\n", rfc2616_last_modified, now) < 0) return;
	strcat(header, tmp);
	if ( len > 0 )
	{
        if(snprintf(tmp, 1024 - 1, "%s%d\r\n", rfc2616_content_length, len) < 0) return;
		strcat(header, tmp);
	}
    if(snprintf(tmp, 1024 - 1, "%s%s\r\n", rfc2616_expires, now) < 0) return;
	strcat(header, tmp);

	// add possible further headers
	list = pConn->m_pHeaderLines;
	while( list != NULL )
	{
		if (strlen(list->name) == 0)
		{
            if(snprintf(tmp, 1024 - 1, "%s\r\n", list->value) < 0) return;
			strcat(header, tmp);
		}
		else
		{
            if(snprintf(tmp, 1024 - 1, "%s: %s\r\n", list->name, list->value) < 0) return;
			strcat(header, tmp);
		}
		list = list->next;
	}
	// write the end of the header
	strcat(header, rfc2616_header_end);
}

static int WebServerConnection_DoWriteBuffer(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( pConn == NULL )
		return 1;

	// eventualmente scrive prima in cache
	unsigned int nCacheFree, nWritten, nRemain;
	unsigned char* offset;

	nCacheFree = pConn->m_uliCacheSize - pConn->m_uliCacheDataSize;

	if ( (pConn->m_rgucCache != NULL) && (nCacheFree > buffer_size) )
	{
		// simply write in cache
		memcpy(pConn->m_rgucCache + pConn->m_uliCacheDataSize, buffer, buffer_size);
		pConn->m_uliCacheDataSize += buffer_size;
		return 0;
	}
	// altrimenti deve scrivere prima la cache e poi i dati:
	// la cosa piu' semplice (anche se leggermente piu' lenta) e'
	// riempire la cache
	if ( pConn->m_rgucCache != NULL )
	{
		memcpy(pConn->m_rgucCache + pConn->m_uliCacheDataSize, buffer, nCacheFree);
		pConn->m_uliCacheDataSize += nCacheFree;
		// poi svuota la cache
        if ( WebServerConnection_Flush(pConn, error) != 0 )
        {
			return 1;
		}
		nWritten = nCacheFree;
	}
	else
	{
		nWritten = 0;
	}

	// adesso scrive tutto il resto a blocchi grandi quanto la
	// dimensione della cache (tranne l'ultimo blocco
	// che andra' in cache)
	offset = buffer + nWritten;
	nRemain = buffer_size - nWritten;
	while (nRemain > pConn->m_uliCacheSize)
	{
		if ( tcpWrite(pConn->m_liClientSocketDescriptor, offset, pConn->m_uliCacheSize, error) != 0 )
		{
			return 1;
		}
		// aggiorna
		nWritten += pConn->m_uliCacheSize;
		nRemain = buffer_size - nWritten;
		offset = buffer + nWritten;
	}

	// alla fine copia in cache una eventuale rimanenza
	if ( pConn->m_rgucCache != NULL )
	{
		memcpy(pConn->m_rgucCache, offset, nRemain);
		pConn->m_uliCacheDataSize = nRemain;
	}
	else
	{
		if ( tcpWrite(pConn->m_liClientSocketDescriptor, offset, nRemain, error) != 0 )
		{
			return 1;
		}
	}

	return 0;
}

static int WebServerConnection_WriteHttpHeader(WebServerConnection_t* pConn, int type, unsigned int data_size, char* error)
{
	if ( pConn == NULL )
		return 1;

	char header_buf[1024];
	int header_len;

	// se l'header era gia' stato inviato non fa niente
	if ( pConn->m_liHeaderSent )
	{
		return 0;
	}
	WebServerConnection_MakeHttpHeader(pConn, type, data_size, header_buf);

	header_len = strlen(header_buf);
    if ( header_len == 0 )
	{
		errorPrintf(error, "Errore di memoria");
		return 1;
	}
	if ( WebServerConnection_DoWriteBuffer(pConn, (unsigned char*)header_buf, header_len, error) != 0 )
	{
		return 1;
	}
	pConn->m_liHeaderSent = 1;
	return 0;
}

static int WebServerConnection_WriteElementHeader(WebServerConnection_t* pConn, unsigned int size, const char* sMimeType, char* error)
{
	if ( pConn == NULL )
		return 1;

	char header[1024], tmp[1024];

    //sprintf(header, "");
	header[0] = '\0';
	// boundary
    if(snprintf(tmp, 1023, "--%s\r\n", pConn->m_sBoundary) < 0) return 1;
	strcat(header, tmp);
	// frame size
    if(snprintf(tmp, 1023, "Content-length: %d\r\n", size) < 0) return 1;
    strcat(header, tmp);
	// frame type
    if(snprintf(tmp, 1023, "Content-type: %s\r\n", sMimeType) < 0) return 1;
	strcat(header, tmp);
	// end of the header
	strncat(header, "\r\n", 1023);
	return WebServerConnection_WriteString(pConn, header, error);
}


/*! *******************************************************************************************************************
 * Implementation of WebServerConnection_t library
 * ********************************************************************************************************************
 */
WebServerConnection_t* WebServerConnection_Init()
{
	WebServerConnection_t* pConn;
	pConn = (WebServerConnection_t*)calloc(1, sizeof(WebServerConnection_t));
    if ( pConn == 0)
    {
		return NULL;
    }

	pConn->m_liClientSocketDescriptor = -1;
	pConn->m_uliCacheSize = CACHE_SIZE;
	if ( pConn->m_uliCacheSize > 0 )
	{
		pConn->m_rgucCache = (unsigned char*)calloc(1, pConn->m_uliCacheSize);
		if ( pConn->m_rgucCache == NULL )
		{
			pConn->m_uliCacheSize = 0;
		}
	}
	else
	{
		pConn->m_rgucCache = NULL;
		pConn->m_uliCacheSize = 0;
	}
	pConn->m_uliCacheDataSize = 0;
	pConn->m_pRequestHeaderLines = NULL;
	pConn->m_pParams = NULL;
	pConn->m_pHeaderLines = NULL;

	WebServerConnection_Reset(pConn);

	return pConn;
}

void WebServerConnection_Release(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )		return;

	WebServerConnection_Reset(pConn);
	SAFE_FREE(pConn->m_rgucCache);
	free(pConn);
}

void WebServerConnection_Reset(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )		return;

	WebServerConnection_CloseSocket(pConn);

	pConn->m_liHeaderSent = 0;

	strcpy(pConn->m_sFileName, "");
	strcpy(pConn->m_sPageName, "");
	strcpy(pConn->m_sRequestMethod, "");

	FreeStringPairs(pConn->m_pRequestHeaderLines);
	pConn->m_pRequestHeaderLines = NULL;

	strcpy(pConn->m_sClientAddr, "");
	pConn->m_nClientPort = 0;

	FreeStringPairs(pConn->m_pParams);
	pConn->m_pParams = NULL;

	sprintf(pConn->m_sServerName, "%s/%s",	WEBSERVER_DEFAULT_NAME, WEBSERVER_VERSION);

	strcpy(pConn->m_sMimeType, WEBSERVER_MIME_TEXT_HTML);

	FreeStringPairs(pConn->m_pHeaderLines);
	pConn->m_pHeaderLines = NULL;

	SAFE_FREE(pConn->m_sRequestBody);
	pConn->uliBodyLength = 0;

	SAFE_FREE(pConn->m_rgucCache);
	pConn->m_uliCacheSize = CACHE_SIZE;
	pConn->m_uliCacheDataSize = 0;

	strcpy(pConn->m_sBoundary, "myboundary");
	pConn->m_nBoundaryCounter = 0;

}

int WebServerConnection_IsOpen(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return 0;
	return ((pConn->m_liClientSocketDescriptor > 0) ? 1 : 0);
}

WebServerConnection_t* WebServerConnection_Duplicate(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;

	WebServerConnection_t* pNewConn;

	pNewConn = WebServerConnection_Init();
	if (pNewConn == NULL) {
		return NULL;
	}

	pNewConn->m_liHeaderSent = pConn->m_liHeaderSent;
	strcpy(pNewConn->m_sFileName, pConn->m_sFileName);
	strcpy(pNewConn->m_sPageName, pConn->m_sPageName);
	strcpy(pNewConn->m_sClientAddr, pConn->m_sClientAddr);
	pNewConn->m_pRequestHeaderLines = DuplicateStringPairs(pConn->m_pRequestHeaderLines);
	strcpy(pNewConn->m_sServerName, pConn->m_sServerName);
	strcpy(pNewConn->m_sRequestMethod, pConn->m_sRequestMethod);
	pNewConn->m_pParams = DuplicateStringPairs(pConn->m_pParams);
	strcpy(pNewConn->m_sMimeType, pConn->m_sMimeType);
	pNewConn->m_uliCacheDataSize = pConn->m_uliCacheDataSize;
	pNewConn->m_pHeaderLines = DuplicateStringPairs(pConn->m_pHeaderLines);
	strcpy(pNewConn->m_sServerName, pConn->m_sServerName);
	if (pConn->m_rgucCache == NULL)
	{
		pNewConn->m_rgucCache = NULL;
		pNewConn->m_uliCacheSize = 0;
		pNewConn->m_uliCacheDataSize = 0;
	}
	else
	{
		if (pNewConn->m_uliCacheSize != pConn->m_uliCacheSize)
		{
			// deve riallocare
			SAFE_FREE(pNewConn->m_rgucCache);
			pNewConn->m_rgucCache = (unsigned char*)calloc(pConn->m_uliCacheSize, 1);
		}
		if (pNewConn->m_rgucCache == NULL)
		{
			pNewConn->m_uliCacheSize = 0;
			pNewConn->m_uliCacheDataSize = 0;
		}
		else
		{
			// copia i dati
			memcpy(pNewConn->m_rgucCache, pConn->m_rgucCache, pConn->m_uliCacheDataSize);
			pNewConn->m_uliCacheDataSize = pConn->m_uliCacheDataSize;
		}
	}
	if ( pConn->m_liClientSocketDescriptor != -1 )
	{
		pNewConn->m_liClientSocketDescriptor = pConn->m_liClientSocketDescriptor;
		// siccome il socket e' aperto nella nuova connessione
		// quello corrente viene considerato chiuso
		pConn->m_liClientSocketDescriptor = -1;
	}

	pNewConn->m_nBoundaryCounter = pConn->m_nBoundaryCounter;
	strcpy(pNewConn->m_sBoundary, pConn->m_sBoundary);

	return pNewConn;
}

void WebServerConnection_CloseSocketSimple(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return;
	if ( pConn->m_liClientSocketDescriptor != -1 )
	{
		close(pConn->m_liClientSocketDescriptor);
		pConn->m_liClientSocketDescriptor = -1;
	}
}

void WebServerConnection_AddHeaderLine(WebServerConnection_t* pConn, const char* line)
{
	if ( pConn == NULL )
		return;
	WebServerConnectionStringPair_t *newPair;
	// allocates a new element
	newPair = (WebServerConnectionStringPair_t*)calloc(sizeof(WebServerConnectionStringPair_t), 1);
	// use only value field
	strncpy(newPair->value, line, 1023);
	// insert in the top of the list
	newPair->next = pConn->m_pHeaderLines;
	pConn->m_pHeaderLines = newPair;
}

void WebServerConnection_AddHeader(WebServerConnection_t* pConn, const char* name, const char* value)
{
	if ( pConn == NULL )
		return;

	WebServerConnectionStringPair_t *newPair;
	// allocates a new element
	newPair = (WebServerConnectionStringPair_t*)calloc(sizeof(WebServerConnectionStringPair_t), 1);
	if ( !newPair )
		return;
	// insert data
	strncpy(newPair->name, name, 1023);
	strncpy(newPair->value, value, 1023);
	// insert in the top of the list
	newPair->next = pConn->m_pHeaderLines;
	pConn->m_pHeaderLines = newPair;
}

const char* WebServerConnection_FindRequestHeader(WebServerConnection_t* pConn, const char* name)
{
	if ( pConn == NULL )
		return NULL;
	if ( name == NULL )
		return NULL;
	WebServerConnectionStringPair_t* pHeaderLines = pConn->m_pRequestHeaderLines;
	if ( pHeaderLines != NULL )
		return FindStringInStringPairs(pHeaderLines, name);
	else
		return NULL;
}

WebServerConnectionStringPair_t* WebServerConnection_GetRequestHeaders(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return pConn->m_pRequestHeaderLines;
}

const char* WebServerConnection_FindParam(WebServerConnection_t* pConn, const char* name)
{
	if ( pConn == NULL )
		return NULL;
	if ( name == NULL )
		return NULL;
	return FindStringInStringPairs(pConn->m_pParams, name);
}

WebServerConnectionStringPair_t* WebServerConnection_GetParams(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return pConn->m_pParams;
}

const char* WebServerConnection_GetPage(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return pConn->m_sPageName;
}

const char* WebServerConnection_GetFullPath(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return pConn->m_sFileName;
}

const char* WebServerConnection_GetUserAgent(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return WebServerConnection_FindRequestHeader(pConn, "User-Agent");
}

const char* WebServerConnection_GetRequestHost(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return WebServerConnection_FindRequestHeader(pConn, "Host");
}

const char* WebServerConnection_GetRequestMethod(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return pConn->m_sRequestMethod;
}

const char* WebServerConnection_GetClientAddr(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return pConn->m_sClientAddr;
}

const char* WebServerConnection_GetRequestBody(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return NULL;
	return pConn->m_sRequestBody;
}

void WebServerConnection_SetClientAddr(WebServerConnection_t* pConn, const char* sClientAddr)
{
	if ( pConn == NULL )
		return;
	strncpy(pConn->m_sClientAddr, sClientAddr, 1047);
}

unsigned short int WebServerConnection_GetClientPort(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return 0;
	return pConn->m_nClientPort;
}

void WebServerConnection_SetClientPort(WebServerConnection_t* pConn, unsigned short int usiClientPort)
{
	if ( pConn == NULL )
		return;
	pConn->m_nClientPort = usiClientPort;
}

void WebServerConnection_SetServerName(WebServerConnection_t* pConn, const char* sServerName)
{
	if ( pConn == NULL )
		return;
	strncpy(pConn->m_sServerName, sServerName, 1023);
}

void WebServerConnection_SetMimeType(WebServerConnection_t* pConn, const char* type)
{
	if ( pConn == NULL )
		return;
	strncpy(pConn->m_sMimeType, type, 1023);
}

void WebServerConnection_SetBoundary(WebServerConnection_t* pConn, const char* boundary)
{
	if ( pConn == NULL )
		return;
	strncpy(pConn->m_sBoundary, boundary, 1023);
}

void WebServerConnection_DisableCache(WebServerConnection_t* pConn)
{
	if ( pConn == NULL )
		return;
	WebServerConnection_Flush(pConn, NULL);
	SAFE_FREE(pConn->m_rgucCache);
}

void WebServerConnection_EnableCache(WebServerConnection_t* pConn, int nCacheSize)
{
	if ( pConn == NULL )
		return;
	WebServerConnection_DisableCache(pConn);
	if (nCacheSize < 0)
	{
		nCacheSize = CACHE_SIZE;
	}
	pConn->m_uliCacheSize = nCacheSize;
	pConn->m_rgucCache = (unsigned char*)calloc(1, pConn->m_uliCacheSize);
}

int WebServerConnection_SendNotFound(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( pConn == NULL )
		return 1;
	if ( pConn->m_liHeaderSent )
	{
		// if a header has already been sent, it cannot send another one
		errorPrintf(error, "Header already sent");
		return 1;
	}
	// write header
	if ( WebServerConnection_WriteHttpHeader(pConn, 404, buffer_size, error) != 0 )
	{
		return 1;
	}

	// write buffer (if present)
	if ( ( buffer != NULL ) && ( buffer_size > 0 ) )
	{
		return WebServerConnection_DoWriteBuffer(pConn, buffer, buffer_size, error);
	}
	return 1;
}

int WebServerConnection_SendRedirect(WebServerConnection_t* pConn, const char* url, char* error)
{
	if ( pConn == NULL )
		return 1;
	if (pConn->m_liHeaderSent)
	{
		// if a header has already been sent, it cannot send another one
		errorPrintf(error, "Header already sent");
		return 1;
	}
	if ( strcmp(url, "") == 0 )
	{
		errorPrintf(error, "Url non accettabile");
		return 1;
	}
	// add another header with redirect command
	WebServerConnection_AddHeader(pConn, "Location", url);
	// write header
	if ( WebServerConnection_WriteHttpHeader(pConn, 303, 0, error) != 0)
	{
		return 1;
	}
	return 0;
}

int WebServerConnection_SendInternalError(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( pConn == NULL )
		return 1;

	if ( pConn->m_liHeaderSent )
	{
		// if a header has already been sent, it cannot send another one
		errorPrintf(error, "Header already sent");
		return 1;
	}
	// write header
	if ( WebServerConnection_WriteHttpHeader(pConn, 505, buffer_size, error) != 0 )
	{
		return 1;
	}
	// write buffer (if present)
	if ( (buffer != NULL) && (buffer_size > 0) )
	{
		return WebServerConnection_DoWriteBuffer(pConn, buffer, buffer_size, error);
	}
	return 0;
}



int WebServerConnection_SendBuffer(WebServerConnection_t* pConn, int response_status, unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( pConn == NULL )
		return 1;

	int ret;
	#ifdef WEBSERVER_CONNECTION_ENABLE_DEBUG
	printf("SendBuffer - start\n"); fflush(stdout);
	#endif

	// Write header
	if ( ! pConn->m_liHeaderSent )
	{
		if ( WebServerConnection_WriteHttpHeader(pConn, response_status, buffer_size, error) != 0 )
		{
			return 1;
		}
	}
	// Writes buffer
	ret = WebServerConnection_DoWriteBuffer(pConn, buffer, buffer_size, error);
	WebServerConnection_CloseSocket(pConn);

	#ifdef WEBSERVER_CONNECTION_ENABLE_DEBUG
	printf("SendBuffer - EngWebSrvConnection_DoWriteBuffer completed with code %d\n", ret); fflush(stdout);
	#endif

	return ret;
}

int WebServerConnection_SendString(WebServerConnection_t* pConn, int response_status, const char* str, char* error)
{
	if ( pConn == NULL )
		return 1;
	int len;
	len = strlen(str);
	return WebServerConnection_SendBuffer(pConn, response_status, (unsigned char*)str, len, error);
}
int WebServerConnection_SendBufferWithOk200(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( pConn == NULL )
		return 1;

	int ret;
	#ifdef WEBSERVER_CONNECTION_ENABLE_DEBUG
	printf("SendBuffer - start\n"); fflush(stdout);
	#endif

	// Write header
	if ( ! pConn->m_liHeaderSent )
	{
		if ( WebServerConnection_WriteHttpHeader(pConn, HTTP_RET_CODE_OK, buffer_size, error) != 0 )
		{
			return 1;
		}
	}
	// Writes buffer
	ret = WebServerConnection_DoWriteBuffer(pConn, buffer, buffer_size, error);
	WebServerConnection_CloseSocket(pConn);

	#ifdef WEBSERVER_CONNECTION_ENABLE_DEBUG
	printf("SendBuffer - EngWebSrvConnection_DoWriteBuffer completed with code %d\n", ret); fflush(stdout);
	#endif

	return ret;
}

int WebServerConnection_SendStringWithOk200(WebServerConnection_t* pConn, const char* str, char* error)
{
	if ( pConn == NULL )
		return 1;
	int len;
	len = strlen(str);
	return WebServerConnection_SendBufferWithOk200(pConn, (unsigned char*)str, len, error);
}

int WebServerConnection_WriteBuffer(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int buffer_size, char* error)
{

	if ( pConn == NULL )
		return 1;

	#ifdef WEBSERVER_CONNECTION_ENABLE_DEBUG
	printf("WriteBuffer - start\n"); fflush(stdout);
	#endif
	// Write header
	if ( !pConn->m_liHeaderSent )
	{
		if ( WebServerConnection_WriteHttpHeader(pConn, HTTP_RET_CODE_OK, 0, error) != 0)
		{
			return 1;
		}
	}
	// Write buffer
	return WebServerConnection_DoWriteBuffer(pConn, buffer, buffer_size, error);
}

int WebServerConnection_WriteString(WebServerConnection_t* pConn, const char* str, char* error)
{
	if ( pConn == NULL )
		return 1;
	int len;
	len = strlen(str);
	return WebServerConnection_WriteBuffer(pConn, (unsigned char*)str, len, error);
}

int WebServerConnection_WriteElement(WebServerConnection_t* pConn, unsigned char* buffer, unsigned int size, const char* sMimeType, char* error)
{
	if ( pConn == NULL )
		return 1;

	if ( WebServerConnection_IsOpen(pConn) != 1 )
	{
		errorPrintf(error, "Connection not initialized");
		return 1;
	}

	// Writes the buffer header
	if ( WebServerConnection_WriteElementHeader(pConn, size, sMimeType, error) != 0 )
	{
		return 1;
	}
	// Writes the buffer
	if (  WebServerConnection_WriteBuffer(pConn, buffer, size, error) != 0 )
	{
		return 1;
	}

	// Append the closing sequence \r\n
	if ( WebServerConnection_WriteString(pConn, "\r\n", error) != 0 )
	{
		return 1;
	}

	// Increments the numbers of buffers sent
	pConn->m_nBoundaryCounter++;
	pConn->m_nBoundaryCounter = (pConn->m_nBoundaryCounter % 1000000);

	return 0;
}

int WebServerConnection_Flush(WebServerConnection_t* pConn, char* error)
{
	if ( pConn == NULL )
		return 1;

	int ret;
	if ( pConn->m_rgucCache == NULL)
	{
		errorPrintf(error, "Impossible to flush an empty cache");
		return 1;
	}
	ret = 1;
	if (pConn->m_uliCacheDataSize > 0)
	{
		ret = tcpWrite(pConn->m_liClientSocketDescriptor, pConn->m_rgucCache, pConn->m_uliCacheDataSize, error);
	}
	pConn->m_uliCacheDataSize = 0;
	return ret;
}


/*! *******************************************************************************************************************
 * WebServerConnection class implementation
 * ********************************************************************************************************************
 */
WebServerConnection::WebServerConnection()
{
	m_pConn = WebServerConnection_Init();
	m_bExternalConn = false;
}

WebServerConnection::WebServerConnection(WebServerConnection_t* pConn)
{
	m_pConn = pConn;
	m_bExternalConn = false;
}

WebServerConnection::WebServerConnection(WebServerConnection_t* pConn, bool external_connection)
{
	m_pConn = pConn;
	m_bExternalConn = external_connection;
}

WebServerConnection::~WebServerConnection()
{
	if ( !m_bExternalConn )
	{
		WebServerConnection_Release(m_pConn);
	}
	m_pConn = NULL;
}

void WebServerConnection::Reset()
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_Reset(m_pConn);
}

void WebServerConnection::CloseSocketSimple()
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_CloseSocketSimple(m_pConn);
}

bool WebServerConnection::IsOpen()
{
	if ( m_pConn == NULL )
		return false;
	return ( WebServerConnection_IsOpen(m_pConn) == 1 );
}

bool WebServerConnection::Flush(char* error)
{
	if ( m_pConn == NULL )
		return false;
	return ( WebServerConnection_Flush(m_pConn, error) == 0 );
}

void WebServerConnection::SetSocket(int sock)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_SetSocket(m_pConn, sock);
}

int WebServerConnection::GetHeader(char* error)
{
	int nRetValue = -1;
	if ( m_pConn == NULL )
		return nRetValue;
	nRetValue = WebServerConnection_GetHeader(m_pConn, error);
	return nRetValue;
}

std::string WebServerConnection::FindParam(std::string name)
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;

	const char* sParam = WebServerConnection_FindParam(m_pConn, name.c_str());
	if ( sParam == NULL )
	{
		return sRetValue;
	}
	sRetValue.assign(sParam);
	return sRetValue;
}

std::vector< std::pair<std::string, std::string> > WebServerConnection::GetParams()
{
	std::vector< std::pair<std::string, std::string> > ret;
	ret.clear();
	if ( m_pConn == NULL )
		return ret;
	WebServerConnectionStringPair_t* params = WebServerConnection_GetParams(m_pConn);
	while ( params != NULL )
	{
		ret.push_back(std::make_pair(std::string(params->name), std::string(params->value)));
		params = params->next;
	}
	return ret;
}

std::string WebServerConnection::GetRequestMethod()
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_GetRequestMethod(m_pConn);
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

std::string WebServerConnection::GetPage()
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_GetPage(m_pConn);
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

std::string WebServerConnection::GetFullPath()
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_GetFullPath(m_pConn);
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

std::string WebServerConnection::GetUserAgent()
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_GetUserAgent(m_pConn);
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

std::string WebServerConnection::GetRequestHost()
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_GetRequestHost(m_pConn);
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

std::string WebServerConnection::FindRequestHeader(std::string name)
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	if ( name.length() <= 0 )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_FindRequestHeader(m_pConn, name.c_str());
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

std::vector< std::pair<std::string, std::string> > WebServerConnection::GetRequestHeaders()
{
	std::vector< std::pair<std::string, std::string> > ret;
	ret.clear();
	if ( m_pConn == NULL )
		return ret;
	WebServerConnectionStringPair_t* headers = WebServerConnection_GetRequestHeaders(m_pConn);
	while ( headers != NULL )
	{
		ret.push_back(std::make_pair(std::string(headers->name), std::string(headers->value)));
		headers = headers->next;
	}
	return ret;
}

std::string WebServerConnection::GetClientAddr()
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_GetClientAddr(m_pConn);
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

std::string WebServerConnection::GetRequestBody()
{
	std::string sRetValue("");
	if ( m_pConn == NULL )
		return sRetValue;
	const char* sRetValueCString = WebServerConnection_GetRequestBody(m_pConn);
	if ( sRetValueCString != NULL )
		sRetValue.assign(sRetValueCString);
	return sRetValue;
}

void WebServerConnection::SetClientAddr(std::string sClientAddr)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_SetClientAddr(m_pConn, sClientAddr.c_str());
}

unsigned short int WebServerConnection::GetClientPort()
{
	if ( m_pConn == NULL )
		return 0;
	return WebServerConnection_GetClientPort(m_pConn);
}

void WebServerConnection::SetClientPort(unsigned short int usiClientPort)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_SetClientPort(m_pConn, usiClientPort);
}

void WebServerConnection::SetServerName(std::string sServerName)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_SetServerName(m_pConn, sServerName.c_str());
}

void WebServerConnection::SetMimeType(std::string type)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_SetMimeType(m_pConn, type.c_str());
}

void WebServerConnection::SetBoundary(std::string boundary)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_SetBoundary(m_pConn, boundary.c_str());
}

void WebServerConnection::DisableCache()
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_DisableCache(m_pConn);
}

void WebServerConnection::EnableCache(int nCacheSize)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_EnableCache(m_pConn, nCacheSize);
}

bool WebServerConnection::SendBuffer(unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( m_pConn == NULL )
		return false;
	if ( buffer == NULL )
		return false;
	return ( WebServerConnection_SendBufferWithOk200(m_pConn, buffer, buffer_size, error) == 0 );
}

bool WebServerConnection::SendString(std::string str, int response_status, char* error)
{
	if ( m_pConn == NULL )
		return false;
	if ( str.length() == 0 )
		return false;
	if ( response_status != HTTP_RET_CODE_OK )
	{
		return ( WebServerConnection_SendString(m_pConn, response_status, str.c_str(), error) == 0 );
	}
	else
	{
		return ( WebServerConnection_SendStringWithOk200(m_pConn, str.c_str(), error) == 0 );
	}
}

bool WebServerConnection::WriteBuffer(unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( m_pConn == NULL )
		return false;
	if ( buffer == NULL )
		return false;
	return ( WebServerConnection_WriteBuffer(m_pConn, buffer, buffer_size, error) == 0 );
}

bool WebServerConnection::WriteString(std::string str, char* error)
{
	if ( m_pConn == NULL )
		return false;
	if ( str.length() == 0 )
		return false;
	return ( WebServerConnection_WriteString(m_pConn, str.c_str(), error) == 0 );
}

bool WebServerConnection::SendNotFound(unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( m_pConn == NULL )
		return false;
	return ( WebServerConnection_SendNotFound(m_pConn, buffer, buffer_size, error) == 0 );
}

bool WebServerConnection::SendRedirect(std::string url, char* error)
{
	if ( m_pConn == NULL )
		return false;
	if ( url.length() == 0 )
		return false;
	return ( WebServerConnection_SendRedirect(m_pConn, url.c_str(), error) == 0 );
}

bool WebServerConnection::SendInternalError(unsigned char* buffer, unsigned int buffer_size, char* error)
{
	if ( m_pConn == NULL )
		return false;
	if ( ( buffer == NULL ) || ( buffer_size == 0 ) )
		return false;
	return ( WebServerConnection_SendInternalError(m_pConn, buffer, buffer_size, error) == 0 );
}

void WebServerConnection::AddHeaderLine(std::string line)
{
	if ( m_pConn == NULL )
		return;
	if ( line.length() == 0 )
		return;
	WebServerConnection_AddHeaderLine(m_pConn, line.c_str());
}

void WebServerConnection::AddHeader(std::string name, std::string value)
{
	if ( m_pConn == NULL )
		return;
	WebServerConnection_AddHeader(m_pConn, name.c_str(), value.c_str());
}

WebServerConnection* WebServerConnection::Duplicate()
{
	WebServerConnection* pRetValue = 0;
	pRetValue = new WebServerConnection(WebServerConnection_Duplicate(m_pConn));
	return pRetValue;
}

WebServerConnection_t* WebServerConnection::getInternalData(void)
{
	return m_pConn;
}
