//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerDaemon.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the WebServerDaemon class.
//! @details
//!
//*!****************************************************************************

#ifndef WEBSERVERDAEMON_H
#define WEBSERVERDAEMON_H

#include <string>

#include "Thread.h"
#include "Loggable.h"
#include "WebServer.h"

using namespace std;

/*! *******************************************************************************************************************
 *	Function used to process connections
 * ********************************************************************************************************************
 */
typedef bool (*WebServerDaemonProcFunc)(WebServerConnection* pConnection, void* pHandler);

/*! *******************************************************************************************************************
 * @brief	The WebServerDaemon class
 *			This thread contains a web server that is listening on some address and port and serves a HTTP connection
 *			per time. As soon as the connection is received a WebserverConnection member is properly filled with all
 *			headers and body received and gets passed to a function, said connection processor, to perform actions
 *			on it. The connection processor function shall be implemented somewhere else and passed to this daemon.
 *			As soon as the connection processor exits, the current connection is closed and the daemon is ready
 *			to serve the next one. A NULL value for the connection processor can be specified and, in this case, a
 *			"HTTP 404 not found" is sent to the client.
 * @note	This class is now a single thread that supports only one connection per time.
 *			For future use it can be expanded as a multiconnection processor, i.e. it can be used as a thread manager
 *			that initializes m_uliSupportedNumClients threads.
 *			Every thread should implement the same thread body as this one providing the management of its connection.
 * ********************************************************************************************************************
 */
class WebServerDaemon : public Thread, public Loggable
{

	public:

		/*! ***********************************************************************************************************
		 * @brief WebServerDaemon	void constructor, reset internal members
		 * ************************************************************************************************************
		 */
		WebServerDaemon();

		/*! ***********************************************************************************************************
		 * @brief ~WebServerDaemon default destructor
		 * ************************************************************************************************************
		 */
		virtual ~WebServerDaemon();

		/*! ***********************************************************************************************************
		 * @brief init					initialization function that must be called before the web server daemon is
		 *								started
		 * @param strListenAddress		the web address on which the server will be listening
		 * @param usiListenPort			the web port on which the server will be listening
		 * @param uliSupportedClients	number of parallel connections managed, at the moment only 1 is accepted
		 * @return						true upon succesfull initialization, false otherwise
		 * ************************************************************************************************************
		 */
		bool init(string strListenAddress, uint16_t usiListenPort, uint32_t uliSupportedClients = WEBSERVER_DEFAULT_NUM_CLIENTS);

		/*! ***********************************************************************************************************
		 * @brief setConnectionProcessorFunction	initializes the function pointer to be called after a connection
		 *											is received
		 * @param pConnProcessorFunc				the function to be executed after the connection is received,
		 *											NULL can be specified, and, in this case, a HTTP 404 is sent
		 *											to the client
		 * ************************************************************************************************************
		 */
		void setConnectionProcessorFunction(WebServerDaemonProcFunc pConnProcessorFunc);

		/*! ***********************************************************************************************************
		 * @brief setProcessorData	optional pointer to a data structure passed to the connection processor
		 *							function. The connection processor function must know what the data structure is
		 *							aimed to.
		 * @param pProcessorData	pointer to a data structure to be optionally used by the connection processor
		 * ************************************************************************************************************
		 */
		void setProcessorData(void* pProcessorData);

	protected:

		int workerThread();
		void beforeWorkerThread();
		void afterWorkerThread();


    private:

        bool m_bConfigOk;								// flag that indicates the configuration status
        uint16_t m_usiListenPort;						// server listen port
        string m_strListenAddress;						// server listen address
        uint32_t m_uliSupportedNumClients;				// maximum number of parallel clients, for future use
        WebServerDaemonProcFunc m_pConnProcessorFunc;	// function to call upon connection reception
        void* m_pProcessorData;							// additional data to be passed to the connection processor
        WebServer m_webServer;							// the web server that listens and serves connections
        WebServerConnection m_connection;

};

#endif // WEBSERVERDAEMON_H
