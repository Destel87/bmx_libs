//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerInclude.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the defines for the WebServer lib.
//! @details
//!
//*!****************************************************************************

#ifndef WEBSERVER_INCLUDE_H
#define WEBSERVER_INCLUDE_H

/*! *******************************************************************************************************************
 * WEBSERVER definitions
 * ********************************************************************************************************************
 */
#define WEBSERVER_VERSION					"1.0.1"
#define WEBSERVER_DEFAULT_NAME				"BioMérieux Modular Master Board HTTP Server"
#define WEBSERVER_MAX_NUM_CLIENTS			10UL
#define WEBSERVER_DEFAULT_NUM_CLIENTS		1UL

#define WEBSERVER_MIME_TEXT_HTML			"text/html"
#define WEBSERVER_MIME_TEXT_XML				"text/xml"
#define WEBSERVER_MIME_IMAGE_JPEG			"image/jpeg"
#define WEBSERVER_MIME_TEXT_MULTIPART		"multipart/x-mixed-replace"
#define WEBSERVER_MIME_APP_XML				"application/xml"
//#define WEBSERVER_ENABLE_DEBUG
//#define WEBSERVER_CONNECTION_ENABLE_DEBUG

#ifndef O_NONBLOCK
#define O_NONBLOCK							1
#endif

#define SOCKET_STEP_SIZE					1024	// max buffer size for a single write on the socket
#define SOCKET_WRITE_TIMEOUT_MSEC			5000	// max write timeout on socket
#define CACHE_SIZE							1024	// cache size

/*! *******************************************************************************************************************
 * MACROS
 * ********************************************************************************************************************
 */
#define SAFE_DELETE(p)					if( (p) != 0 ) {delete (p); (p) = 0;}
#define SAFE_DELETE_ARRAY(p)			if( (p) != 0 ) {delete [] (p); (p) = 0;}
#define SAFE_FREE(p)					if( (p) != 0 ) {free(p); p = 0;}
#define closesocket(s)					close(s)

/*! *******************************************************************************************************************
 * HTTP definitions
 * ********************************************************************************************************************
 */
#define rfc2616_protocol				"HTTP/1.0 "
#define rfc2616_status_200				"200 OK"
#define rfc2616_status_201				"201 CREATED"
#define rfc2616_status_202				"202 ACCEPTED"
#define rfc2616_status_404				"404 Not Found"
#define rfc2616_status_303				"303 See Other"
#define rfc2616_status_505				"505 Internal Server error"
#define rfc2616_server					"Server: "
#define rfc2616_date					"Date: "
#define rfc2616_content_type			"Content-type: "
#define rfc2616_last_modified			"Last-modified: "
#define rfc2616_content_length			"Content-length: "
#define rfc2616_expires					"Expires: "
#define rfc2616_header_end				"\r\n"

#define HTTP_RET_CODE_OK				200
#define HTTP_RET_CODE_CREATED			201
#define HTTP_RET_CODE_ACCEPTED			202
#define HTTP_RET_CODE_NOT_FOUND			404
#define HTTP_RET_CODE_SEE_OTHER			303
#define HTTP_RET_CODE_INTERNAL_ERR		505



/*! *******************************************************************************************************************
 * Function prototypes
 * ********************************************************************************************************************
 */
int hexToInt(int liCharAsHexValue);
int urlDecode(char *sUrl);
void setFlags(int liFD, int liFlags);
void errorPrintf(char* sError, const char*fmt, ...);
int getSocketError();
const char *getSocketErrorMessage(int liMsgNumber);
int tcpWrite(int liSocketDescriptor, unsigned char* rgucBuffer, unsigned int uliBufferSize, char* sError);
int tcpRead(int sock, unsigned char* buffer, unsigned int buflen, int liTimeoutMsec, char* sError);
double getTimer(void);
void usiToString(unsigned short int usiInputVal, char* sOutputString);
void substituteChar(char* sStringToParse, const char cActualCharValue, const char cNewCharValue);
int setCloexecFlag (int liFD, int liValue);
void getRfc1123Time(char* sOutputTimeString);
void buildCurrentTime(char* sOutputTimeString);

#endif // WEBSERVER_INCLUDE_H
