//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerMultipartConnection.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the WebServerMultipartConnection class.
//! @details
//!
//*!****************************************************************************

#include "WebServerMultipartConnection.h"


WebServerMultipartConnection::WebServerMultipartConnection()
{

}

WebServerMultipartConnection::~WebServerMultipartConnection()
{

}

bool WebServerMultipartConnection::writeElement(unsigned char* rgucBuffer, int liBufferSize, char* sMimeType, char* sError)
{
	return ( WebServerConnection_WriteElement(m_pConn, rgucBuffer, liBufferSize, sMimeType, sError) == 0 );
}

void WebServerMultipartConnection::setBoundary(const char* sBoundary)
{
	WebServerConnection_SetBoundary(m_pConn, sBoundary);
}
