//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerMultipartConnection.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the WebServerMultipartConnection class.
//! @details
//!
//*!****************************************************************************

#ifndef WEBSERVER_MULTIPART_CONNECTION_H
#define WEBSERVER_MULTIPART_CONNECTION_H

#include "WebServer.h"

class WebServerMultipartConnection : public WebServerConnection
{
	public:

		/*! ***********************************************************************************************************
		 * @brief WebServerMultipartConnection void constructor
		 * ***********************************************************************************************************
		 */
		WebServerMultipartConnection();

		/*! ***********************************************************************************************************
		 * @brief ~WebServerMultipartConnection default destructor
		 * ************************************************************************************************************
		 */
		virtual ~WebServerMultipartConnection();

		/*! ***********************************************************************************************************
		 * @brief WriteElement	send a slice of a multipart element
		 * @param rgucBuffer	the buffer to send
		 * @param liBufferSize	the buffer size
		 * @param sMimeType		the mime-type of the element
         * @param sError [out]	a string reporting the error, if it occurres
		 * @return				true if the slice has been correctly sent, false otherwise
		 * ************************************************************************************************************
		 */
		bool writeElement(unsigned char* rgucBuffer, int liBufferSize, char* sMimeType, char* sError = NULL);

		/*! ***********************************************************************************************************
		 * @brief SetBoundary	set the frame boundary
         * @param sBoundary		a string representing the frame boundary
		 * ************************************************************************************************************
		 */
		void setBoundary(const char* sBoundary);
};


#endif // WEBSERVER_MULTIPART_CONNECTION_H
