QT			-= core gui
TARGET		= WebServer
TEMPLATE	= lib
CONFIG		+= staticlib

# internal libraries inclusion
INCLUDEPATH += $$PWD/../Log
INCLUDEPATH += $$PWD/../Thread

HEADERS += \
			WebServerInclude.h \
			WebServerConnection.h \
			WebServer.h \
			WebServerMultipartConnection.h \
			WebServerDaemon.h

SOURCES += \
			WebServerInclude.cpp \
			WebServerConnection.cpp \
			WebServer.cpp \
			WebServerMultipartConnection.cpp \
			WebServerDaemon.cpp

DISTFILES +=
