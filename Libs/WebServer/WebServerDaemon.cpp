//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServerDaemon.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the implementation for the WebServerDaemon class.
//! @details
//!
//*!****************************************************************************

#include "WebServerDaemon.h"


WebServerDaemon::WebServerDaemon()
{
	m_bConfigOk = false;
	m_usiListenPort = 0;
	m_strListenAddress.clear();
	m_uliSupportedNumClients = WEBSERVER_DEFAULT_NUM_CLIENTS;
	m_pConnProcessorFunc = 0;
	m_pProcessorData = 0;
}

WebServerDaemon::~WebServerDaemon()
{

}

bool WebServerDaemon::init(string strListenAddress, uint16_t usiListenPort, uint32_t uliSupportedClients)
{
	m_bConfigOk = false;
	if ( usiListenPort == 0 )
	{
		return false;
	}
	m_strListenAddress.assign(strListenAddress);
	m_usiListenPort = usiListenPort;

	if ( uliSupportedClients == 0 )
	{
		m_uliSupportedNumClients = 1;
	}
	else if ( uliSupportedClients > WEBSERVER_MAX_NUM_CLIENTS )
	{
		m_uliSupportedNumClients = WEBSERVER_MAX_NUM_CLIENTS;
	}
	else
	{
		m_uliSupportedNumClients = uliSupportedClients;
	}

	m_webServer.init(m_strListenAddress, m_usiListenPort, m_uliSupportedNumClients);
	m_bConfigOk = true;
	return true;
}

void WebServerDaemon::setConnectionProcessorFunction(WebServerDaemonProcFunc pConnProcessorFunc)
{
	m_pConnProcessorFunc = pConnProcessorFunc;
}

void WebServerDaemon::setProcessorData(void* pProcessorData)
{
	m_pProcessorData = pProcessorData;
}

int WebServerDaemon::workerThread()
{
	if ( m_bConfigOk == false )
	{
		log(LOG_ALERT, "WebServerDaemon::thread: bad configuration, exit");
		return 1;
	}

	// Start listening
	char sError[1024];
	if ( m_webServer.startListening(sError) == false )
	{
		log(LOG_ERR,
			"WebServerDaemon::thread: failed to start listening on %s:%d (%s), exit",
			m_strListenAddress.c_str(), m_usiListenPort, sError);
		return 1;
	}


	/*! ***************************************************************************************************************
	 * Main cycle
	 * ****************************************************************************************************************
	 */
	m_connection.Reset();
	log(LOG_INFO, "WebServerDaemon::thread: listening for connections on %s:%hu, supported clients %lu",
		m_strListenAddress.c_str(), m_usiListenPort, m_uliSupportedNumClients);

	while ( isRunning() )
	{
		// wait for a connection
		if ( m_webServer.waitConnection(&m_connection, 100) == false )
		{
			continue;
		}
		log(LOG_DEBUG, "WebServerDaemon::thread: received a connection");

		// process connection
		if ( m_pConnProcessorFunc != NULL )
		{

			bool bProcessRetValue = (m_pConnProcessorFunc)(&m_connection, m_pProcessorData);
			if ( !bProcessRetValue )
			{
				log(LOG_DEBUG_PARANOIC+1, "WebServerDaemon::thread: process connection failure");
			}
		}
		else
		{
			m_connection.SendNotFound();
		}

		// reset connection
		m_connection.Reset();
	}

	// Stop the server
	log(LOG_INFO, "WebServerDaemon::thread: stop listening");
	m_webServer.stopListening();

	// Exits
	log(LOG_INFO, "WebServerDaemon::thread: exit");
	return 0;
}

void WebServerDaemon::beforeWorkerThread()
{
	// Do some check before starting the thread
	if ( m_usiListenPort == 0 )
	{
		m_bConfigOk = false;
	}
	if ( m_bConfigOk == false )
	{
		log(LOG_ALERT, "WebServerDaemon::thread: bad configuration");
		return;
	}

	m_bConfigOk = true;
	log(LOG_INFO, "WebServerDaemon::thread: server correctly configured");
	m_webServer.setListenWebAddress(m_strListenAddress);
	m_webServer.setListenWebPort(m_usiListenPort);
}

void WebServerDaemon::afterWorkerThread()
{

}
