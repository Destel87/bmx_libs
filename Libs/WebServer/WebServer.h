//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    WebServer.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the WebServer class.
//! @details
//!
//*!****************************************************************************

#ifndef WEBSERVER_H
#define WEBSERVER_H

#include "WebServerInclude.h"
#include "WebServerConnection.h"
#include "Loggable.h"

using namespace std;

/*! *******************************************************************************************************************
 * @brief TestProcessConnection	function to test the connection that sends back to the client the headers and
 *								body and parameters received
 * @param pConn					pointer to a connection object filled with data received by the client
 * @param counter				a counter to track the number of connections received
 * @return						true if the response has been correctly sent back to the client
 * ********************************************************************************************************************
 */
bool TestProcessConnection(WebServerConnection* pConn, void* counter);

/*! *******************************************************************************************************************
 * @brief	The WebServer class
 *			Implements a web server. This class contains a socket waiting for connections and initializes an
 *			object of type WebServerConnection for every received connection
 * ********************************************************************************************************************
 */
class WebServer : public Loggable
{

	public:

		/*! ***********************************************************************************************************
		 * @brief WebServer				void constructor intializes internal members to 0
		 * ************************************************************************************************************
		 */
		WebServer();

		/*! ***********************************************************************************************************
		 * @brief WebServer				initializes the web server to listen on the specified port and all
		 *								possible addresses
		 * @param usiListenPort			the web port on which the server will be listening
		 * ************************************************************************************************************
		 */
		WebServer(unsigned short int usiListenPort);

		/*! ***********************************************************************************************************
		 * @brief WebServer				initializes the web server to listen on the specified port and address
		 * @param strListenAddress		the web address on which the server will be listening
		 * @param usiListenPort			the web port on which the server will be listening
		 * ************************************************************************************************************
		 */
		WebServer(string strListenAddress, unsigned short int usiListenPort);

		/*! ***********************************************************************************************************
		 * @brief ~WebServer			default destructor
		 * ************************************************************************************************************
		 */
		virtual ~WebServer();

		/*! ***********************************************************************************************************
		 * @brief init					initializes the web server to listen on the specified port and address but
		 *								does not open the main socket
		 * @param strListenAddress		the web address on which the server will be listening
         * @param usiListenPort			the web port on which the server will be listening
		 * @param uliMaxNumClients		maximum number of supported parallel connections
		 * ************************************************************************************************************
		 */
		void init(string strListenAddress, unsigned short int usiListenPort, unsigned int uliMaxNumClients = WEBSERVER_DEFAULT_NUM_CLIENTS);

		/*! ***********************************************************************************************************
		 * @brief getVersion			retrieves the version of this library
		 * @return						a string in the form XX.YY.ZZ
		 * ************************************************************************************************************
		 */
		static const char* getVersion();

		/*! ***********************************************************************************************************
		 * @brief getListenWebAddress	retrieves the web address on which the server is listening
		 * @return						a string containing the web address on which the server is listening
		 * ************************************************************************************************************
		 */
		string getListenWebAddress(void);

		/*! ***********************************************************************************************************
		 * @brief setListenWebAddress	set the web address on which the server is listening
         * @param strWebAddress			the web address on which the server is listening, can be an empty string
		 *								to listen to all possible addresses
		 * @note						if this function is called after startListening() it has no effect
		 * ************************************************************************************************************
		 */
		void setListenWebAddress(string strWebAddress);

		/*! ***********************************************************************************************************
		 * @brief getListenWebPort		retrieves the web port on which the server is listening
		 * @return						a number containing the web port on which the server is listening
		 * ************************************************************************************************************
		 */
		unsigned short int getListenWebPort(void);

		/*! ***********************************************************************************************************
		 * @brief setListenWebPort		set the web port on which the server is listening
		 * @param usiWebPort			the web port on which the server is listening
		 * @note						if this function is called after startListening() it has no effect
		 * ************************************************************************************************************
		 */
		void setListenWebPort(unsigned short int usiWebPort);

		/*! ***********************************************************************************************************
		 * @brief getServerName			retrieves the server name that is sent in the HTTP header of the response
		 * @return						a string containing the server name
		 * ************************************************************************************************************
		 */
		string getServerName();

		/*! ***********************************************************************************************************
		 * @brief setServerName			set the server name that will be sent in the HTTP header of the response
         * @param sServerName			a string containing the server name
		 * ************************************************************************************************************
		 */
		void setServerName(string sServerName);

		/*! ***********************************************************************************************************
		 * @brief isActive				check whether the server is listening or not
		 * @return						true if the server is listening, false otherwise
		 * ************************************************************************************************************
		 */
		bool isActive();

		/*! ***********************************************************************************************************
         * @brief setSupportedNumClients		set the maximum number of connections that the server can accept.
		 *								This value will	be used in the startListening method
         * @param uliSupportedNumClients		maximum number of supported connections
		 * ************************************************************************************************************
		 */
		void setSupportedNumClients(unsigned int uliSupportedNumClients);

		/*! ***********************************************************************************************************
		 * @brief startListening		start listening to requests on the specified address and port
		 * @param sListenAddress		the web address on which the server is listening, can be an empty string
		 *								to listen to all the addresses
		 * @param usiListenPort			the web port on which the server is listening
		 * @param sError				a string containing a possible error message
		 * @return						true upon succesfull execution of operations on socket, false otherwise
		 * ************************************************************************************************************
		 */
		bool startListening(string sListenAddress, unsigned short int usiListenPort, char* sError = NULL);

		/*! ***********************************************************************************************************
		 * @brief startListening		start listening to requests on the address previously configured with the
		 *								setListenWebAddress and the specified port passed as parameter
         * @param usiListenPort			the web port on which the server is listening
		 * @param sError				a string containing a possible error message
		 * @return						true upon succesfull execution of operations on socket, false otherwise
		 * ************************************************************************************************************
		 */
		bool startListening(unsigned short int usiListenPort, char* sError = NULL);

		/*! ***********************************************************************************************************
		 * @brief startListening		start listening to requests on the address and port previously configured
		 *								with the setListenWebAddress and setListenWebPort methods
		 * @param sError				a string containing a possible error message
		 * @return						true upon succesfull execution of operations on socket, false otherwise
		 * ************************************************************************************************************
		 */
		bool startListening(char* sError = NULL);

		/*! ***********************************************************************************************************
		 * @brief stopListening			stop listening to requests by closing the socket
		 * ************************************************************************************************************
		 */
		void stopListening();

		/*! ***********************************************************************************************************
		 * @brief WaitConnection		waits for a connection by a client; if a timeout is specified, this method
		 *								exits at the end of the waiting time if no connection has been received. If
		 *								a timeout of -1 is specified the function gets locked until a connection is
		 *								received
		 * @param pConnection			pointer to an object of type WebserverConnection that will be filled out with
		 *								the received data in the connection and the socket
		 * @param liTimeoutMilliSeconds	waiting timeout in milliseconds
		 * @return						true if a connection has been received before the timeout, false upon timeout
		 *								or some error
		 * ************************************************************************************************************
		 */
		bool waitConnection(WebServerConnection* pConnection, int liTimeoutMilliSeconds = -1);

		/*! ***********************************************************************************************************
		 * @brief WaitConnection		waits for a connection by a client; if a timeout is specified, this method
		 *								exits at the end of the waiting time if no connection has been received. If
		 *								a timeout of -1 is specified the function gets locked until a connection is
		 *								received
		 * @param rConnection			reference to an object of type WebserverConnection that will be filled out
		 *								with the received data in the connection and the socket
		 * @param liTimeoutMilliSeconds	waiting timeout in milliseconds
		 * @return						true if a connection has been received before the timeout, false upon timeout
		 *								or some error
		 * ************************************************************************************************************
		 */
		bool waitConnection(WebServerConnection& rConnection, int liTimeoutMilliSeconds = -1);

		/*! ***********************************************************************************************************
		 * @brief currentTime			retrieves the current system time and formats a date string of type
		 *								%day, %day-num %month %Y %H:%M:%S GMT%z
		 * @return						current system time
		 * ************************************************************************************************************
		 */
		static string currentTime(void);

    private:

        void closeSocket(void);
        bool openSocket(char* sError);
        bool acceptConnection(WebServerConnection* pConn);

    private:

        string m_strListenAddress;				// listen address
        unsigned short int m_usiListenPort;		// listen port
        int m_liServerSocketDescriptor;			// socket descriptor on which the listen() is performed
        string m_strServerName;					// name of the server
        unsigned int m_uliSupportedNumClients;	// maximum number of parallel clients
};

#endif //WEBSERVER_H
