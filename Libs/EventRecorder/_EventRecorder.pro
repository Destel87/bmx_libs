QT          -= core gui
TARGET      = Log
TEMPLATE    = lib
CONFIG      += staticlib

QMAKE_CXXFLAGS_DEBUG -= -O1
QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG -= -O3
QMAKE_CXXFLAGS_DEBUG += -O0

HEADERS += \
	$$PWD/Log.h \ 
	$$PWD/Loggable.h

SOURCES += \
	$$PWD/Log.cpp \
	$$PWD/Loggable.cpp
