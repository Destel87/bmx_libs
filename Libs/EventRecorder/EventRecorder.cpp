//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    EventRecorder.cpp
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the EventRecorder class.
//! @details
//!
//*!****************************************************************************

#include <string>
#include <iostream>
#include <fstream>
#include <mutex>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include "EventRecorder.h"


#define RECORDER_SETUP_FILE	"recorderSetup.cfg"

#define RECORDER_EVENTS_FILE	"recEvents"

#define RECORDEREXTENSION_FILE	".log"

#define RECORDER_SEPARATOR		";"

#define RECORDER_FILE_MAX_SIZE		100000
#define RECORDER_FILE_MAX_NUMBER	10

#define RECORDER_HEADER			"timestamp;tag;information"

static bool rollIfNeeded(const char* log_file, unsigned int max_file_size);

static const char * recorderHeader[RECORDER_NUM_INDEX] =
{
	"[PRESSURE]",
	"[ERROR]",
	"[READING]",
	"[EVENT]",
	"[STATE]",
	"[COMMAND]",
	"[VOLTAGES]",
	"[FAN]",
	"[TEMPERATURE]"
};

static std::mutex m_mutexRecorder;

EventRecorder::EventRecorder()
{
	m_recorderIndex = 0;
	m_bEnable = false;
    m_strFolderName.clear();
	m_strFileName.clear();
}

EventRecorder::~EventRecorder()
{
}

bool EventRecorder::initEventRecorder(bool bEnable, std::string folderName)
{
	m_bEnable = bEnable;
    m_strFolderName.assign(folderName);

	// if the recorder is disabled no need to read configuration file
	if(m_bEnable == false) return false;

	std::string strRead, strKey, strIndex;
	std::ifstream inFile;

    std::string strRecorderFile;
    strRecorderFile.assign(m_strFolderName);
    strRecorderFile.append(RECORDER_SETUP_FILE);

    inFile.open(strRecorderFile);
	if(inFile.fail())
	{
		//File does not exist code here
        printf("EventRecorder::conf file not present %s", strRecorderFile.c_str());
	}
	else
	{
		while(!inFile.eof())
		{
			getline(inFile, strRead);
			if(!strRead.empty())
			{
				// lines beginning with # are comments
				if(strRead.front() != '#')
				{
					size_t posStr = 0;
					if((posStr = strRead.find("=")) != std::string::npos)
					{
						// extract the values
						posStr = strRead.find("=");
						strIndex = strRead.substr(posStr + 1);
						strKey = strRead.substr(0, posStr);
						if(strKey.find("Index") != std::string::npos)
						{
							// current index of the event file name
							m_recorderIndex = atoi(strIndex.c_str());
							break;
						}
					}
				}
			}
		}
		inFile.close();
	}

	bool bNewFile = false;

	if(m_recorderIndex <= 0)
	{
		// default value
		m_recorderIndex = 1;
		bNewFile = true;
	}

	assignFileNames();

	if(bNewFile)
	{
		// if a index is not set start from 1 resetting the file
		writeHeaders();
		saveIndex();
	}

	// record the instrument startup
	std::vector<std::string> vectorMsg;
	vectorMsg.push_back("Startup");
	writeEvent(RECORDER_EVENTS_INDEX,  &vectorMsg);

	return true;
}

bool EventRecorder::stopEventRecorder()
{
	// if the recorder is disabled no need to read configuration file
	if(m_bEnable == false) return false;

	// record the instrument shutdown
	std::vector<std::string> vectorMsg;
	vectorMsg.push_back("Shutdown");
	writeEvent(RECORDER_EVENTS_INDEX,  &vectorMsg);

	saveIndex();

	return true;
}

void EventRecorder::assignFileNames()
{
    m_strFileName.assign(m_strFolderName);
    m_strFileName.append(RECORDER_EVENTS_FILE);
	m_strFileName.append(std::to_string(m_recorderIndex));
	m_strFileName.append(RECORDEREXTENSION_FILE);
}

bool EventRecorder::writeHeaders()
{
	FILE * fp;

	fp = fopen(m_strFileName.c_str(), "w");
	if ( fp == NULL )
	{
		return false;
	}
	fprintf(fp, "%s\n", RECORDER_HEADER);
	fflush(fp);
	fclose(fp);
	return true;
}

bool EventRecorder::writeEvent(int event, std::vector<std::string> * pVectorMsg)
{
	if(m_bEnable == false) return true;

	if(event > RECORDER_NUM_INDEX) return false;

	FILE * fp;
/*
	char time_msg[1024];
	struct timeval curr_time;
	time_t curr_sec;

	gettimeofday(&curr_time, NULL);
	curr_sec = curr_time.tv_sec;

	sprintf(time_msg, "%.19s", ctime(&curr_sec));
*/
	char time_msg[32];

	time_t t = time(NULL);
	struct tm timeNow = *localtime(&t);

	sprintf(time_msg, "%04d-%02d-%02dT%02d:%02d:%02d",
		timeNow.tm_year + 1900, timeNow.tm_mon + 1, timeNow.tm_mday,
		timeNow.tm_hour, timeNow.tm_min, timeNow.tm_sec);

	m_mutexRecorder.lock();

	fp = fopen(m_strFileName.c_str(), "a+");
	if ( fp == NULL )
	{
		m_mutexRecorder.unlock();
		return false;
	}

	// build the string to be written
	std::string outputString;

	// first value is always the timestamp
	outputString.append(time_msg);
	outputString.append(RECORDER_SEPARATOR);

	// second value is the tag to identify the event
	outputString.append(recorderHeader[event]);
	outputString.append(RECORDER_SEPARATOR);

	for(uint8_t counter = 0; counter < pVectorMsg->size(); counter++)
	{
		outputString.append(pVectorMsg->at(counter));
        if(counter != (pVectorMsg->size() - 1))
		{
			outputString.append(RECORDER_SEPARATOR);
		}
	}
    outputString.append("\r\n");

    fprintf(fp, "%s", outputString.c_str());
	fflush(fp);
	fclose(fp);

	// is the max size reached ? if so update the counter and initialize new files
    if(rollIfNeeded(m_strFileName.c_str(), RECORDER_FILE_MAX_SIZE))
	{
		if(++m_recorderIndex >= RECORDER_FILE_MAX_NUMBER)
		{
			// recirculate the index
			m_recorderIndex = 1;
		}
		assignFileNames();
		writeHeaders();
		saveIndex();
	}

	m_mutexRecorder.unlock();
	return true;
}

void EventRecorder::saveIndex()
{
	FILE* fp;

    std::string strRecorderFile;
    strRecorderFile.assign(m_strFolderName);
    strRecorderFile.append(RECORDER_SETUP_FILE);

    fp = fopen(strRecorderFile.c_str(), "w");
	if ( fp == NULL )
	{
		return;
	}
	fprintf(fp, "Index = %d\n", m_recorderIndex);
	fflush(fp);
	fclose(fp);
}

static bool rollIfNeeded(const char* log_file, unsigned int max_file_size)
{
	struct stat filestatus;
	unsigned int size;

	if ( stat(log_file, &filestatus) != 0 )
	{
		return false;
	}

	size = (unsigned int)filestatus.st_size;
	if (size < max_file_size)
	{
		// rolling is not needed
		return false;
	}

	return true;
}

