#-------------------------------------------------
#
# Project created by QtCreator 2020-02-11T19:08:07
#
#-------------------------------------------------

QT       -= core gui

TARGET = EventRecorder
TEMPLATE = lib
CONFIG      += staticlib

QMAKE_CXXFLAGS_DEBUG -= -O1
QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG -= -O3
QMAKE_CXXFLAGS_DEBUG += -O0

DEFINES += EVENTRECORDER_LIBRARY

SOURCES += EventRecorder.cpp

HEADERS += \
    EventRecorder.h

