//*!****************************************************************************
//! @copyright
//! bioMerieux Italy - Confidential & proprietary intellectual property
//! Copyright (C) 2020 bioMerieux, Inc. This program is the property of
//! bioMerieux, Inc, its contents are proprietary information and no part of it
//! is to be disclosed to anyone except employees of bioMerieux, Inc or as
//! explicitly agreed in writing with a Statement of Non-Disclosure.
//!
//! @file    EventRecorder.h
//!
//! @author  BmxIta FW dept
//!
//! @brief   Contains the declaration for the EventRecorder class.
//! @details
//!
//*!****************************************************************************

#ifndef EVENTRECORDER_H
#define EVENTRECORDER_H

#include <vector>
#include <string>

enum
{
	RECORDER_PRESSURE_INDEX = 0,
    RECORDER_ERRORS_INDEX,
    RECORDER_READINGS_INDEX,
    RECORDER_EVENTS_INDEX,
    RECORDER_STATE_INDEX,
    RECORDER_COMMANDS_INDEX,
    RECORDER_VOLTAGES_INDEX,
    RECORDER_FAN_INDEX,
    RECORDER_TEMPERATURE_INDEX,
    RECORDER_NUM_INDEX
};

/*!
 * @class EventRecorder
 * @brief interface class to log events on instrument: Errors, Pressure, Readings, Events
 */
class EventRecorder
{

	public:

		/*! ***************************************************************************************
         * @brief void constructor
		 * Initializes with default values the internal members of the class
		 * ****************************************************************************************
		 */
        EventRecorder();

		/*! ***************************************************************************************
		 * Default destructor
		 * ****************************************************************************************
		 */
		virtual ~EventRecorder();

		/*! ***************************************************************************************
		 * @brief initEventRecorder read from configuration file the current recorder index
		 * @param bEnable true if the recorder is enabled, false otherwise
         * @param folderName the folder where the files are stored
         * @return true if file is present false otherwise
		 * ****************************************************************************************
		 */
        bool initEventRecorder(bool bEnable, std::string folderName);

		/*! ***************************************************************************************
		 * @brief stopEventRecorder write to configuration file the current recorder index
		 * @return true if file is present false otherwise
		 * ****************************************************************************************
		 */
		bool stopEventRecorder();

		/*! ***************************************************************************************
		 * @brief writeEvent write to the events file corresponding to event the formatted string
		 *	composed by all elements of the vector
		 * @param event the event (-> file) identifier according to the enum
		 * @param pVectorMsg the pointer to a string vector to be written
		 * @return true if succesfull
		 * ****************************************************************************************
		 */
		bool writeEvent(int event, std::vector<std::string> * pVectorMsg);

	protected:

	private :

		/*! ***************************************************************************************
		 * @brief assignFileNames sets the events file name according to the valid index
		 * @return true if file is present fals otherwise
		 * ****************************************************************************************
		 */
		void assignFileNames();

		/*! ***************************************************************************************
		 * @brief writeHeaders when a new file is created the corresponding header is written
		 * @return true if succesfull, false otherwise
		 * ****************************************************************************************
		 */
		bool writeHeaders();

		/*! ***************************************************************************************
		 * @brief saveIndex save on configuration file the index of log currently active
		 * ****************************************************************************************
		 */
		void saveIndex();

    protected:

        int	m_recorderIndex;
        bool	m_bEnable;

        std::string m_strFolderName;
        std::string m_strFileName;

};

#endif // EVENTRECORDER_H
