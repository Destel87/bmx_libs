TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# system libraries inclusion
QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread

# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a

# sources and target
SOURCES += TestMessageThread.cpp
TARGET = TestMessageThread

# deployment directives
target.path = /home/root
INSTALLS += target
