#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "MessageThread.h"

#define msleep(ms) usleep(ms*1000)

class TestMessageThread : public MessageThread
{

	public:

		static const unsigned int TEXT_MSG_TYPE = MSG_THREAD_USER + 1;
		static const unsigned int NUM_MSG_TYPE = MSG_THREAD_USER + 2;


	protected:

		void parseMsg(unsigned int type, void* param1, int param2)
		{
			if (type == TEXT_MSG_TYPE)
			{
				char* text = (char*)param1;
				printf("Text message '%s'\n", text); fflush(stdout);
				delete text;
			}
			else if (type == NUM_MSG_TYPE)
			{
				printf("Numeric message '%d'\n", param2); fflush(stdout);
			}
		}

		void purgeMsg(unsigned int type, void* param1, int param2)
		{
			if (type == TEXT_MSG_TYPE)
			{
				char* text = (char*)param1;
				printf("Clean text '%s'\n", text); fflush(stdout);
				delete text;
				msleep(2);
			}
			else if (type == NUM_MSG_TYPE)
			{
				printf("Clean number '%d'\n", param2); fflush(stdout);
			}
			else
			{
				printf("Purge message of type '%d'\n", type); fflush(stdout);
			}
		}
};

int main()
{
	TestMessageThread thread;
	int i;

	printf("Thread version %s\n", Thread::getVersion() );

	if ( !thread.init() )
	{
		printf("Impossible to start thread\n"); fflush(stdout);
		return 1;
	}

	for (i = 0; i < 1000; i++)
	{
		char* text = new char[8192];
		sprintf(text, "Cicle number %d", i);
		// Send message of type TEXT
		//printf("Sending message '%s' from main\n", text); fflush(stdout);
		if ( !thread.sendMsg(TestMessageThread::TEXT_MSG_TYPE, text, 0) )
		{
			printf("Impossible to send message number %d\n", i); fflush(stdout);
			delete text;
		}
		// invia un messaggio di tipo NUM
		//printf("Sending numeric message '%d' from main\n", i); fflush(stdout);
		if ( !thread.sendMsg(TestMessageThread::NUM_MSG_TYPE, NULL, i) )
		{
			printf("Impossible to send numeric message %d\n", i); fflush(stdout);
		}
		// Waits for a while
		msleep(10);
	}

	printf("Resources release\n"); fflush(stdout);
	thread.destroy(true);

	return 0;
}


