#include <iostream>
#include <signal.h>
#include <stdlib.h>
#include "USBInterface.h"

#define BUFFERSIZE			1024
#define TIMEOUT_MAX_MSEC	1000

using namespace std;
static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
			printf("\nReceived SIGINT\n");
			exit(1);
			break;
		case SIGKILL:
			printf("\nReceived SIGKILL\n");
			bKeepRunning = false;
			break;
	}
}


int main()
{
	/* Signal manager */
	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);

	USBInterface USBdevice;

	USBdevice.initInterface(USBdevice.getDefaultUSBaddress(), 8, "N", 0, 0);

	if ( USBdevice.openInterface() != 0 )
	{
		return -1;
	}

	unsigned char rgBuffer[BUFFERSIZE];
	unsigned int uliBufferSize = BUFFERSIZE;
	int liOperationResult = 0;
	int liTimeoutMsec = TIMEOUT_MAX_MSEC;

	while ( bKeepRunning )
	{
		int nBytesRead = USBdevice.readBufferWithTimeout(rgBuffer, uliBufferSize, liOperationResult, liTimeoutMsec);
		if ( nBytesRead > 0 )
		{
			rgBuffer[nBytesRead] = '\0';
			printf("TestSerial: read %d bytes, buff =\"%s\"\n", nBytesRead, rgBuffer);
		}
		else
		{
			printf("TestSerial: read reported errno=%d\n", liOperationResult);
		}
		usleep(10);
	}

	if ( USBdevice.closeInterface() != 0 )
	{
		return -1;
	}


	return 0;
}


