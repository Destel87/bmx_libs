TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3
QMAKE_CXXFLAGS += -O0

#QMAKE_CXXFLAGS +=  -pthread
LIBS += -pthread


unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/USBInterface/ -lUSBInterface
INCLUDEPATH += $$PWD/../../Libs/USBInterface
DEPENDPATH += $$PWD/../../Libs/USBInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/USBInterface/libUSBInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a


# sources and target
SOURCES += TestUSB.cpp
TARGET = TestUSB

# deployment directives
target.path = /home/root
INSTALLS += target



