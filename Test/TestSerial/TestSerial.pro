TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/SerialInterface/ -lSerialInterface
INCLUDEPATH += $$PWD/../../Libs/SerialInterface
DEPENDPATH += $$PWD/../../Libs/SerialInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/SerialInterface/libSerialInterface.a

# sources and target
SOURCES += TestSerial.cpp

TARGET = TestSerial

# deployment directives
target.path = /home/root
INSTALLS += target

