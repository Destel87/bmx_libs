#include <signal.h>
#include <stdlib.h>

#include "SerialInterface.h"

#define SERIAL_INTERFACE_FILENAME	"/dev/ttymxc1"
#define BUFFERSIZE					1024
#define BAUDRATE					115200
#define TIMEOUT_MAX_MSEC			1000


using namespace std;
static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
			printf("\nReceived SIGINT\n");
			exit(1);
			break;
		case SIGKILL:
			printf("\nReceived SIGKILL\n");
			bKeepRunning = false;
			break;
	}
}



int main(void)
{

	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);

	SerialInterface serial;
	string fileName = "/dev/ttymxc1";

	serial.initInterface(fileName, BAUDRATE, 8, "N", 0, 0);

	if ( serial.openInterface() != 0 )
	{
		return -1;
	}

	unsigned char rgBuffer[BUFFERSIZE];
	unsigned int uliBufferSize = BUFFERSIZE;
	int liOperationResult = 0;
	int liTimeoutMsec = TIMEOUT_MAX_MSEC;

	while ( bKeepRunning )
	{
		int nBytesRead = serial.readBufferWithTimeout(rgBuffer, uliBufferSize, liOperationResult, liTimeoutMsec);
		if ( nBytesRead > 0 )
		{
			rgBuffer[nBytesRead] = '\0';
			printf("TestSerial: read %d bytes, buff =\"%s\"\n", nBytesRead, rgBuffer);
		}
		else
		{
			printf("TestSerial: read reported errno=%d\n", liOperationResult);
		}
		usleep(10);
	}

	if ( serial.closeInterface() != 0 )
	{
		return -1;
	}

	return 0;

}
