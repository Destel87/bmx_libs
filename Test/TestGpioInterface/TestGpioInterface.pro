TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -g
QMAKE_CXXFLAGS += -O0
QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3

# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/GpioInterface/ -lGpioInterface
INCLUDEPATH += $$PWD/../../Libs/GpioInterface
DEPENDPATH += $$PWD/../../Libs/GpioInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/GpioInterface/libGpioInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../Libs/Config
DEPENDPATH += $$PWD/../../Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Config/libConfig.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a

# sources and target
SOURCES += TestGpioInterface.cpp
TARGET = TestGpioInterface

# deployment directives
target.path = /home/root

conf.path = /home/root
conf.files = gpio_config.txt
conf.files += gpio_config_interrupt.txt

INSTALLS = target
INSTALLS += conf

DISTFILES += \
    gpio_config.txt \
    gpio_config_interrupt.txt
