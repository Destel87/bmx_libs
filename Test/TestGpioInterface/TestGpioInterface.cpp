#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "GpioInterface.h"

#define msleep(ms) usleep(ms*1000)

#define LOG_LEVEL		LOG_DEBUG
#define LOG_FILE		"stdout"
#define PROGRAM_NAME	"TestLog"



#define HAS_INTERRUPT_ON_GPIO_1

#ifdef HAS_INTERRUPT_ON_GPIO_1
// This file contains configuration for the app to listen to an interrupt on gpio 1
// and react to it by setting and resetting gpio 2
#define CONFIG_FILENAME	"gpio_config_interrupt.txt"
#else
// This file contains configuration for the app to generate two square waves on
// gpio 1 and gpio 2
#define CONFIG_FILENAME	"gpio_config.txt"
#endif

static int keepRunning = 1;

void intHandler(int)
{
	keepRunning = 0;
}


int main(void)
{

	Log logger;
	logger.setLevel(LOG_LEVEL);
	logger.setFile(LOG_FILE);
	logger.setAppName(PROGRAM_NAME);
	logger.log(LOG_INFO, "TestGpioInterface start");

	GpioInterface gpioInterface;
	gpioInterface.setLogger(&logger);
	gpioInterface.init(CONFIG_FILENAME);
	gpioInterface.printConfiguration();

	int i = 0;
	signal(SIGINT, intHandler);
	while ( keepRunning )
	{
#ifdef HAS_INTERRUPT_ON_GPIO_1
		if ( gpioInterface.waiForIrq(1) )
		{
			gpioInterface.writeValue(2, 1);
			msleep(100);
			gpioInterface.writeValue(2, 0);
		}
#else
		gpioInterface.writeValue(1, 1);
		msleep(100);
		gpioInterface.writeValue(2, 1);
		msleep(100);
		gpioInterface.writeValue(1, 0);
		msleep(100);
		gpioInterface.writeValue(2, 0);
		msleep(100);
#endif
		logger.log(LOG_INFO, "TestGpioInterface cicle %d", ++i);
	}

#ifdef HAS_INTERRUPT_ON_GPIO_1
	gpioInterface.writeValue(2, 0);
#endif
	gpioInterface.writeValue(1, 0);

	logger.log(LOG_INFO, "TestGpioInterface end of cicles");

	return 0;
}



