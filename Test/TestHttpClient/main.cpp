#include <iostream>

#include "HttpConnection.h"


#define BASE_URL		"http://10.105.192.106/api/instrument/v1/processCommand"
#define XML_BUF_P1		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
#define XML_BUF_P2		"<vminst:HELLO	xmlns:vminst=\"http://biomerieux.com/vidas/instrument/protocol/v1/vminst\" "
#define XML_BUF_P3		"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
#define XML_BUF_P4		"Usage=\"SERVICE\"/>"
using namespace std;

int main(void)
{
	HttpConnection conn("");
	conn.appendHeader("header1", "value1");
	string strUrl(BASE_URL);
	string strXmlBuffer(XML_BUF_P1);
	strXmlBuffer.append(XML_BUF_P2);
	strXmlBuffer.append(XML_BUF_P3);
	strXmlBuffer.append(XML_BUF_P4);
	HttpResponse r = conn.post(strUrl, strXmlBuffer);
	return 0;
}
