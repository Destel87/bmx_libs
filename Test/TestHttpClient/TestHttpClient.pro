TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt core gui

#internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/WebClient/ -lHttpClient
INCLUDEPATH += $$PWD/../../Libs/HttpClient
DEPENDPATH += $$PWD/../../Libs/HttpClient
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/HttpClient/libHttpClient.a

SOURCES += main.cpp

#unix:!macx: LIBS += -lcurl

# deployment directives
target.path = /home/root
INSTALLS += target


