#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "MessageThread.h"

#define msleep(ms) usleep(ms*1000)

#define TIMEOUT 10


class TestThread : public Thread
{

	protected:

		int workerThread()
		{
			int counter = 0;
			while ( isRunning() )
			{
				msleep(500);
				printf("Thread cycle %d\n", counter); fflush(stdout);
				counter++;
			}
			return 0;
		}

		void beforeWorkerThread()
		{
			printf("Start of thread\n"); fflush(stdout);
		}

		void afterWorkerThread()
		{
			printf("End of thread\n"); fflush(stdout);
		}

};

int main()
{

	TestThread thread;

	printf("Thread version %s\n", Thread::getVersion() );

	if ( !thread.startThread() )
	{
		printf("Impossible to start thread\n"); fflush(stdout);
		return 1;
	}

	msleep(TIMEOUT * 1000);

	printf("Resources release\n"); fflush(stdout);
	thread.stopThread();

	return 0;

}



