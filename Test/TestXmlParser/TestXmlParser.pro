TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    DOMPrintErrorHandler.cpp \
    DOMPrintFilter.cpp \
    DOMTreeErrorReporter.cpp
LIBS += -pthread
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/XmlParser/ -lXmlParser
INCLUDEPATH += $$PWD/../../Libs/XmlParser
DEPENDPATH += $$PWD/../../Libs/XmlParser
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/XmlParser/libXmlParser.a

INCLUDEPATH += $$PWD/../../Libs/XmlParser/XercesSrc


# deployment directives
target.path = /home/root
INSTALLS += target

HEADERS += \
    DOMPrintErrorHandler.hpp \
    DOMPrintFilter.hpp \
    DOMTreeErrorReporter.hpp

DISTFILES += \
    login.xml \
    personal.xml

xml_files.path = $$target.path
xml_files.files = \
	login.xml \
	personal.xml
INSTALLS += xml_files
