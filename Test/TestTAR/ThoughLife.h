#ifndef THOUGHLIFE_H
#define THOUGHLIFE_H


static std::vector <std::string> vstrThughLife =
{
".",
".",
".                                            @@@@@@@+ @@   @@ @@+    @@    ++++ ",
".                ++++++++++++                @@@@@@@+ @@   @@ @@+    @@   @@@@@@+",
".            +++++++++++++++++++             +++@@+++ @@   @@ @@+    @@  @@@++@@+",
".          +++++++++++++++++++++++              @@    @@   @@ @@+    @@ @@@    +  ",
".        +++++++++++++++++++++++++++            @@    @@   @@ @@+    @@ @@         ",
".       +++++++++++++++++++++++++++++           @@    @@@@@@@ @@+    @@ @@   @@@@@+",
".      +++++++++++++++++++++++++++++++          @@    @@@@@@@ @@+    @@ @@   @@@@@+",
".     +++++++++++++++++++++++++++++++++         @@    @@   @@ +@+   +@@ @@+     @@  ",
".     +++++++++++++++++++++++++++++++++         @@    @@   @@  @@+ +@@+ +@@+  +@@+  ",
".                   +++++                       @@    @@   @@  +@@@@@@   +@@@@@@@   ",
".                                               @@    @@   @@   +@@@+       @@@@+    ",
".                      ++                                                           ",
".                     +++                                                           ",
".  +                +++++                +",
".  +                +++++                +",
".  ++              +++++++              ++   ",
".  +++            ++++++++++           +++          @@     @@+ @@@@@@ @@@@@@           ",
".  +++++       +++++++++++++++       +++++          @@     @@+ @@@@@@ @@@@@@           ",
".  +++++++++++++++++++++++++++++++++++++++          @@     @@+ @@     @@               ",
".  +++++++++++++++++++++++++++ @++++++++++          @@     @@+ @@     @@               ",
".  ++++++++++++++++++++++++++ ++++++++++++          @@     @@+ @@@@@  @@@@@            ",
".   +++++++++++++++  +++  +@++++++++++++++          @@     @@+ @@@@@  @@@@@",
".     ++++++++++++++++++++@@@@@++++++++++           @@     @@+ @@+++  @@+++",
".        +++++++++++++++++@@@@@@+++++++             @@     @@+ @@     @@",
".          +++++++++++++++@@@@@@++                  @@++++ @@+ @@     @@++++",
".                   +++++  @@@@@@                   @@@@@@ @@+ @@     @@@@@@",
".                          +@@@@@@+                 @@@@@@ @@+ @@     @@@@@@",
".                            +@@@@@@+",
".                             +@@@@@@+",
".                              +@@@@@@+",
".                                @@@@@@+",
".                                 +@@@@+",
".                                   @@@+",
".                                     ++",
".",
"."
};


#endif // THOUGHLIFE_H
