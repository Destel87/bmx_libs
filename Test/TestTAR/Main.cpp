#include <iostream>

#include "TARUtilities.h"
#include "ThoughLife.h"
#include "Log.h"

#define ESC		27


int main()
{
	Log m_Log;
	m_Log.enable(true);
	m_Log.setLevel(7);
	m_Log.setMaxNumFiles(1000);
	m_Log.setFile("stdout");

	TARutilities myTar;
	myTar.setLogger(&m_Log);

	vector<string> vsArchiveFileList;
	string sFoo("");
	int liRes;
	char sCmd[4];

	while(1)
	{
		printf("\n--< FOLDER MANAGER CMD LIST >--\n\n");
		printf("a - Create archive\n");
		printf("b - Open archive\n");
		printf("c - view archive\n");

		scanf("%s", sCmd);
		char c;

		switch (sCmd[0])
		{
			case 'a':
				printf("Please insert the archive filename:\n");
				sFoo.clear();
				cin >> sFoo;
				liRes = myTar.createArchive(vsArchiveFileList, sFoo);
				if ( liRes )
				{
					printf("archive %s created with success.\n", sFoo.c_str());
					break;
				}
				printf("Unable to create %s.\n", sFoo.c_str());
			break;

			case 'b':
				printf("Please insert the archive filename:\n");
				sFoo.clear();
				cin >> sFoo;
				liRes = myTar.openArchive(sFoo);
				if ( liRes )
				{
					printf("archive %s opened with success.\n", sFoo.c_str());
					break;
				}
				printf("Unable to open %s.\n", sFoo.c_str());
			break;

			case 'c':
				printf("Please insert the archive filename:\n");
				sFoo.clear();
				cin >> sFoo;
				liRes = myTar.viewArchive(sFoo);
				if ( liRes )
				{
					printf("archive %s viewed with success.\n", sFoo.c_str());
					break;
				}
				printf("Unable to view %s.\n", sFoo.c_str());
			break;

			default:
				printf("Wrong key pressed. I'm sorry.\n");
			break;

		}

		printf("\nPress ESC to exit the loop or enter to continue.\n");

		int8_t exit = 1;
		// This is needed to make the cycle work, do not delete it.
		c = cin.get();
		c = 0;
		while(exit)
		{
			c = cin.get();
			if ( c == ESC || c == '\n')
				exit = 0;
		}


		if ( c == ESC )
			break;
	}

	for ( auto s:vstrThughLife)
		cout << "\r" << s << endl;

	printf("Tar Utilities test completed with success.\n");

	return 0;
}
