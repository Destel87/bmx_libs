TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3
QMAKE_CXXFLAGS += -O0


unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/TARUtilities/ -lTARUtilities
INCLUDEPATH += $$PWD/../../Libs/TARUtilities
DEPENDPATH += $$PWD/../../Libs/TARUtilities
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/TARUtilities/libTARUtilities.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a


SOURCES += Main.cpp 
TARGET = TestTAR

# deployment directives
target.path = /home/root
INSTALLS += target

HEADERS += \
    ThoughLife.h





