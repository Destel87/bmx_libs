#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "WebServer.h"
#include "WebServerMultipartConnection.h"
#include "WebServerDaemon.h"
#include "Log.h"

//#define TEST_WEB_SERVER 1
#define TEST_WEB_SERVER_DAEMON 1
//#define TEST_MULTIPART_CONNECTION 1

#define msleep(ms)		usleep(ms*1000)
#define WEB_ADDRESS		""
#define WEB_PORT		8050
#define NUM_CLIENTS		8
#define IMG_DIR			"/home/root"
#define	LOG_FILE		"webserver.log"
#define UPTIME_SECONDS	60*60		// 1 hour uptime

using namespace std;

bool TestProcessConnection(WebServerConnection* pConn, void* counter)
{
	char error[1024];

	int* pCounter = (int*)counter;
	(*pCounter)++;

	std::string sMethod = pConn->GetRequestMethod();
	std::vector< std::pair<std::string, std::string> > pParams = pConn->GetParams();
	std::vector< std::pair<std::string, std::string> > pRequestHeaders = pConn->GetRequestHeaders();
	std::string sPage = pConn->GetPage();
	std::string sClientAddr = pConn->GetClientAddr();
	unsigned short int usiClientPort = pConn->GetClientPort();
	std::string sRequestHost = pConn->GetRequestHost();
	std::string sUserAgent = pConn->GetUserAgent();
	std::vector<std::pair<std::string, std::string> >::iterator it;

	std::stringstream os;
	// html body start
	os << "<html>";
	os << "<body>";
	{

		// Server info
		os << "<hr>";
		{
			os << "Server version: " << WebServer::getVersion();
			os << "<br>";
		}

		// Connection info
		os << "<hr>"		<< std::endl;
		{
			os << "Method:         " << sMethod			<< "<br>";
			os << "Client addr:    " << sClientAddr		<< "<br>";
			os << "Client port:    " << usiClientPort	<< "<br>";
			os << "Host:           " << sRequestHost	<< "<br>";
			os << "User Agent:     " << sUserAgent		<< "<br>";
			os << "Connection no.: " << (*pCounter)		<< "<br>";
		}

		// Headers and parameters received
		os << "<hr>";
		os << "<table border=0>";
		{
			os << "<table border=1>";
			{

				// First row start
				os << "<tr>";
				{
					// First row: cell 00
					os << "<td>";
					os << "&nbsp";
					os << "Page";
					os << "</td>";

					// First row: cell 01
					os << "<td>";
					os << "&nbsp";
					os << sPage;
					os << "</td>";
				}
				// First row end
				os << "</tr>";

				// Second row start
				os << "<tr>";
				{
					// Second row: cell 10
					os << "<td>";
					os << "&nbsp";
					os << "Parameters";
					os << "</td>";

					// Second row: cell 11
					os << "<td>";
					os << "<table border=0>";
					for ( it = pParams.begin(); it != pParams.end(); it++ )
					{

						os << "<tr>";

						os << "<td>";
						os << (*it).first;
						os << "</td>";

						os << "<td>";
						os << "&nbsp=&nbsp";
						os << "</td>";

						os << "<td>";
						os << (*it).second;
						os << "</td>";

						os << "</tr>";

					}
					os << "</table>";
					os << "</td>";
				}
				// Second row end
				os << "</tr>";

				// Third row start
				os << "<tr>";
				{
					// Third row: cell 20
					os << "<td>";
					os << "&nbsp";
					os << "Request headers";
					os << "&nbsp";
					os << "</td>";

					// Third row: cell 21
					os << "<td>";
					os << "<table border=1 frame=hsides>";
					for ( it = pRequestHeaders.begin(); it != pRequestHeaders.end(); it++ )
					{

						os << "<tr>";

						os << "<td>";
						os << "&nbsp";
						os << (*it).first;
						os << "&nbsp";
						os << "</td>";

						os << "<td>";
						os << "&nbsp";
						os << (*it).second;
						os << "&nbsp";
						os << "</td>";

						os << "</tr>";

					}
					os << "</table>";
					os << "</td>";
				}
				// Third row end
				os << "</tr>";
			}
			os << "</table>";
		}
		os << "</table>";
	}
	// html body end
	os << "</body>";
	os << "</html>";

	std::string sOutStr = os.str();
	pConn->SetMimeType(WEBSERVER_MIME_TEXT_HTML);

	if ( !pConn->SendString(sOutStr, error) )
	{
		cout << error << endl;
		return false;
	}
	std:: string sCurrTime = WebServer::currentTime();
	std::cout << sCurrTime << " : received connection from " << sClientAddr << ":" << usiClientPort << std::endl;
	return true;
}

int main(void)
{

	Log logger;
	logger.setLevel(7);
	logger.setFile(LOG_FILE);
	logger.setAppName("TEST_WEB_SERVER");

	/* ****************************************************************************************************************
	 * MAIN cycle
	 * ****************************************************************************************************************
	 */
	#ifdef TEST_WEB_SERVER

	char sError[1024];
	int liCounter = 0;
	WebServerConnection conn;
	WebServer srv(WEB_ADDRESS, WEB_PORT);
	srv.setLogger(&logger);

	logger.log(LOG_INFO, "TEST_WEB_SERVER MAIN: opening socket");
	if ( !srv.startListening(sError) )
	{
		logger.log(LOG_ERR, "TEST_WEB_SERVER MAIN: error from startListening(%s)", sError);
		return 1;
	}

	logger.log(LOG_INFO, "TEST_WEB_SERVER MAIN: start listening for connections");
	while ( true )
	{
		if ( !srv.waitConnection(conn, 100) )
		{
			continue;
		}
		TestProcessConnection(&conn, &liCounter);
	}
	srv.stopListening();

	#elif TEST_WEB_SERVER_DAEMON

	int liCounter = 0;
	WebServerDaemon daemon;
	daemon.setLogger(&logger);
	daemon.init(WEB_ADDRESS, WEB_PORT, NUM_CLIENTS);
	daemon.setConnectionProcessorFunction(TestProcessConnection);
	daemon.setProcessorData((void*)&liCounter);

	if ( !daemon.startThread() )
	{
		logger.log(LOG_ERR, "MAIN: unable to start WebServerDaemon");
		return 1;
	}

	int liUptimeSeconds = UPTIME_SECONDS;
	logger.log(LOG_ERR, "MAIN: WebServerDaemon started, it will be active for %d sec", liUptimeSeconds);
	while ( liUptimeSeconds >= 0 )
	{
		sleep(1);
		liUptimeSeconds--;
	}
	daemon.stopThread();
	logger.log(LOG_ERR, "MAIN: WebServerDaemon stopped. Exit");

#elif TEST_MULTIPART_CONNECTION

	char sError[1024];
	int liCounter = 0;

	WebServerMultipartConnection conn;
	WebServer srv(WEB_ADDRESS, WEB_PORT);

	logger.log(LOG_INFO, "TEST_WEB_SERVER_MULTIPART MAIN: opening socket");
	if ( !srv.startListening(sError) )
	{
		logger.log(LOG_ERR, "TEST_WEB_SERVER MAIN: error from startListening(%s)", sError);
		return 1;
	}

	logger.log(LOG_INFO, "TEST_WEB_SERVER MAIN: start listening for connections");
	while ( true )
	{
		if ( !srv.waitConnection(conn, 100) )
		{
			continue;
		}

		while ( true )
		{
			liCounter++;
			char sImgName[1024];
			sprintf(sImgName, "%s/%05d.jpg", IMG_DIR, liCounter);

			struct stat fileStatus;
			if ( stat(sImgName, &fileStatus) != 0)
			{
				liCounter = 1;
				sprintf(sImgName, "%s/%05d.jpg", IMG_DIR, liCounter);
				if (stat(sImgName, &fileStatus) != 0)
				{
					logger.log(LOG_ERR, "TEST_WEB_SERVER MAIN: impossible to find \"%s\" closing connection", sImgName);
					break;
				}
			}
			conn.SetMimeType(WEBSERVER_MIME_TEXT_MULTIPART);
			conn.DisableCache();

			FILE* pImageFileDescriptor = fopen(sImgName, "rb");
			if ( pImageFileDescriptor == 0 )
			{
				logger.log(LOG_ERR, "TEST_WEB_SERVER MAIN: impossible to open \"%s\" closing connection", sImgName);
				break;
			}
			int liImgSize = (int)fileStatus.st_size;
			unsigned char* rgucImgBuf = new unsigned char[liImgSize];
			fread(rgucImgBuf, liImgSize, 1, pImageFileDescriptor);
			fclose(pImageFileDescriptor);

			if ( !conn.writeElement(rgucImgBuf, liImgSize, (char*)"image/jpeg", sError) )
			{
				logger.log(LOG_ERR, "TEST_WEB_SERVER MAIN: impossible to send \"%s\" closing connection", sImgName);
				break;
			}
			logger.log(LOG_ERR, "TEST_WEB_SERVER MAIN: sent \"%s\"", sImgName);
			usleep(100000);
		}
	}
	srv.stopListening();

	#endif

	return 0;
}
