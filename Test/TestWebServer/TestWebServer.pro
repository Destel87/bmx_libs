TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

LIBS += -pthread

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/WebServer/ -lWebServer
INCLUDEPATH += $$PWD/../../Libs/WebServer
DEPENDPATH += $$PWD/../../Libs/WebServer
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/WebServer/libWebServer.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a


# deployment directives
target.path = /home/root
INSTALLS += target

config.path = /etc/apache2/
config.files += httpd.conf
INSTALLS += config

DISTFILES += \
    img/00001.jpg \
    img/00002.jpg \
    img/00003.jpg \
    img/00004.jpg \
    img/00005.jpg \
    httpd.conf

images.path = $$target.path
images.files = \
	img/00001.jpg \
	img/00002.jpg \
	img/00003.jpg \
	img/00004.jpg \
	img/00005.jpg
INSTALLS += images

