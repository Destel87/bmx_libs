#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "Log.h"

#define msleep(ms) usleep(ms*1000)

#define LOG_LEVEL		LOG_DEBUG
#define LOG_FILE		"stdout"
#define PROGRAM_NAME	"TestLog"

int main(void)
{
	Log logger;
	logger.setLevel(LOG_LEVEL);
	logger.setFile(LOG_FILE);
	logger.setAppName(PROGRAM_NAME);
	logger.log(LOG_INFO, "##### TestLog start #####");
	for(int i = 1; i<=20; i++)
	{
		logger.log(LOG_DEBUG, "Log line number %d", i);
		msleep(100);
	}
	logger.log(LOG_INFO, "##### TestLog exit #####");
	return 0;
}



