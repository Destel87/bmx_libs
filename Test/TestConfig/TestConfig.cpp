#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "Config.h"


#define CONFIG_FILE_NAME			"config.txt"

/* ************************************************************************************************
 * CONFIG
 * ************************************************************************************************
 */
#define CFG_INT_EXAMPLE_ID								0
#define CFG_INT_EXAMPLE_KEY_NAME						"cfg.int.name"
#define CFG_INT_EXAMPLE_DEFAULT_VALUE					"32"

#define CFG_FLOAT_EXAMPLE_ID							1
#define CFG_FLOAT_EXAMPLE_KEY_NAME						"cfg.float.name"
#define CFG_FLOAT_EXAMPLE_DEFAULT_VALUE					"3.14"

#define CFG_STRING_EXAMPLE_ID							2
#define CFG_STRING_EXAMPLE_KEY_NAME						"cfg.string.name"
#define CFG_STRING_EXAMPLE_DEFAULT_VALUE				"Use the force, Luke."


int main(void)
{

	Config* Configurator = new Config();

	printf("TestConfig start\n");

	config_key_t config_keys[] =
	{
		{ CFG_INT_EXAMPLE_ID,		(char*)CFG_INT_EXAMPLE_KEY_NAME,	(char*)"GENERAL",	T_int,		(char*)CFG_INT_EXAMPLE_DEFAULT_VALUE,		DEFAULT_ACCEPTED },
		{ CFG_FLOAT_EXAMPLE_ID,		(char*)CFG_FLOAT_EXAMPLE_KEY_NAME,	(char*)"GENERAL",	T_float,	(char*)CFG_FLOAT_EXAMPLE_DEFAULT_VALUE,		DEFAULT_ACCEPTED },
		{ CFG_STRING_EXAMPLE_ID,	(char*)CFG_STRING_EXAMPLE_KEY_NAME,	(char*)"GENERAL",	T_string,	(char*)CFG_STRING_EXAMPLE_DEFAULT_VALUE,	DEFAULT_ACCEPTED }
	};

	int num_keys = sizeof(config_keys) / sizeof(config_key_t);

	bool bInitOk = Configurator->Init(config_keys, num_keys, CONFIG_FILE_NAME);
	if ( bInitOk == false )
	{
		printf("TestConfig: read error, check that file <%s> exists\n", CONFIG_FILE_NAME);
		Configurator->Init(config_keys, num_keys, NULL);
	}
	else
	{
		printf("TestConfig: read file <%s> success\n", CONFIG_FILE_NAME);
	}

	printf("TestConfig: PARAMETERS\n");
	Configurator->PrintParams();

	int nIntExample = Configurator->GetInt(CFG_INT_EXAMPLE_ID);
	float fFloatExample = Configurator->GetFloat(CFG_FLOAT_EXAMPLE_ID);
	char sStringExample[256] = "";
	snprintf(sStringExample, 255, "%s", Configurator->GetString(CFG_STRING_EXAMPLE_ID));

	printf("TestConfig: nIntExample=%d\n", nIntExample);
	printf("TestConfig: fFloatExample=%f\n", fFloatExample);
	printf("TestConfig: sStringExample=%s\n", sStringExample);

	printf("TestConfig exit\n");

	return 0;
}



