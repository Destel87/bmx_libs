TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../Libs/Config
DEPENDPATH += $$PWD/../../Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Config/libConfig.a

# sources and target
SOURCES += TestConfig.cpp
TARGET = TestConfig

# deployment directives
target.path = /home/root

conf.path = /home/root
conf.files = config.txt

INSTALLS = target
INSTALLS += conf

DISTFILES += \
	config.txt

