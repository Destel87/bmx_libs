#include <unistd.h>
#include <iostream>
#include <sstream>

#include "SPITestIncludes.h"
#include "Config.h"
#include "Log.h"
#include "IS25LQFlashMemory.h"
#include "DataCollector.h"

using namespace std;

/*! ***********************************************************************************************
 * MAIN THREAD
 * ************************************************************************************************
 */
int main(void)
{

	/* ********************************************************************************************
	 * SPI FLASH MEMORY INITIALIZATION
	 * ********************************************************************************************
	 */
	IS25LQFlashMemory spiFlashMemory;
	
	if ( !spiFlashMemory.init(SPI_BUS_FILE_NAME, SPI_FLASH_MEMORY_MODE, SPI_FLASH_MEMORY_SPEED_HZ, SPI_FLASH_MEMORY_BITSPERWORD) )
	{
		cout << "Unable to initialize SPI device" << endl;
		return -1;
	}
	
	spiFlashMemory.resetChip();
	usleep(100);
	uint32_t jedec = spiFlashMemory.readJedec();
	printf("SPI device correctly initialized with jedec: 0x%08X\n", jedec);

	/* ********************************************************************************************
	 * SETUP THE EXPERIMENT
	 * ********************************************************************************************
	 */
	uint16_t nStartAddress = SPI_FLASH_MEMORY_START_ADDRESS;
	int32_t nTransferSize = SPI_FLASH_MEMORY_TRANSFER_SIZE_BYTES;

	uint8_t pTxBuffer[nTransferSize];
	uint8_t pRxBuffer[nTransferSize];

	if ( nTransferSize > 0 )
	{
		for (int i=0; i<nTransferSize; i++)
		{
			pTxBuffer[i] = (uint8_t)i;
			pRxBuffer[i] = 0;
		}
	}

	int32_t nNumIterations = SPI_FLASH_MEMORY_NUM_ITERATIONS;
	bool bPerformCheck = false;
	if ( SPI_FLASH_PERFORM_CHECK != 0 )
	{
		bPerformCheck = true;
	}

	/* ********************************************************************************************
	 * SETUP DATA COLLECTOR
	 * ********************************************************************************************
	 */
	string sDataCollectorIdWrite("SPI_Write");
	DataCollector dcWrite(sDataCollectorIdWrite);

	string sDataCollectorIdRead("SPI_Read");
	DataCollector dcRead(sDataCollectorIdRead);

	/* ********************************************************************************************
	 * RUN THE EXPERIMENT
	 * ********************************************************************************************
	 */
	cout << "Test started" << endl;
	bool bResult = true;
	int nTransferSizeWriteBits = ( nTransferSize + 9 ) * 8;
	int nTransferSizeReadBits = ( nTransferSize + 4 ) * 8;
	for ( int i=0; i<nNumIterations; i++ )
	{
		bResult = true;

		spiFlashMemory.sectorErase(nStartAddress);

		dcWrite.start( nTransferSizeWriteBits );
		spiFlashMemory.programPage(pTxBuffer, nTransferSize, nStartAddress);
		dcWrite.stop(bResult);

		dcRead.start( nTransferSizeReadBits );
		spiFlashMemory.readData(pRxBuffer, nTransferSize, nStartAddress);
		if ( ( bPerformCheck == true ) && ( memcmp(pTxBuffer, pRxBuffer, nTransferSize ) != 0 ) )
		{
				bResult = false;
		}
		dcRead.stop(bResult);

	}
	cout << "Test finished" << endl;

	/* ********************************************************************************************
	 * COMPUTE RESULTS
	 * ********************************************************************************************
	 */
	if ( SPI_STATISTICS_ENABLE != 0 )
	{

		ostringstream osStatisticsWrite;
		osStatisticsWrite << dcWrite;

		ofstream fout;
		fout.open(SPI_STATISTICS_FILE_NAME, ios_base::out | ios_base::binary | ios_base::ate | ios_base::app);
		fout << dcWrite << endl;
		fout.close();

		ostringstream osStatisticsRead;
		osStatisticsRead << dcRead;

		fout.open(SPI_STATISTICS_FILE_NAME, ios_base::out | ios_base::binary | ios_base::ate | ios_base::app);
		fout << dcRead << endl;
		fout.close();

	}

	return 0;

}
