#ifndef SPITESTINCLUDES_H
#define SPITESTINCLUDES_H

/* ************************************************************************************************
 * CONFIGURATION KEYS
 * ************************************************************************************************
 */


#define SPI_BUS_FILE_NAME						"/dev/spidev2.0"



#define SPI_FLASH_MEMORY_MODE					0 
#define SPI_FLASH_MEMORY_SPEED_HZ				1000000
#define SPI_FLASH_MEMORY_BITSPERWORD			8
#define SPI_FLASH_MEMORY_START_ADDRESS			0
#define SPI_FLASH_MEMORY_TRANSFER_SIZE_BYTES	256
#define SPI_FLASH_MEMORY_NUM_ITERATIONS			100
#define SPI_FLASH_PERFORM_CHECK					1

#define SPI_STATISTICS_ENABLE					1
#define SPI_STATISTICS_FILE_NAME				"stats_spi.csv"


#endif // SPITESTINCLUDES_H
