#ifndef IS25LQFLASHMEMORY_H
#define IS25LQFLASHMEMORY_H

#include "SPIInterface.h"

#define	IS25LQ_OP_RD		0x03 // Read Data Bytes from Memory at Normal Read Mode
#define	IS25LQ_OP_FR		0x0B // Read Data Bytes from Memory at Fast Read Mode
#define	IS25LQ_OP_FRDIO		0xBB // Fast Read Dual I/O SPI 104MHz
#define	IS25LQ_OP_FRDO		0x3B // Fast Read Dual Output SPI 104MHz
#define	IS25LQ_OP_FRQIO		0xEB // Fast Read Quad I/O SPI 104MHz
#define	IS25LQ_OP_FRQO		0x6B // Fast Read Quad Output SPI 104MHz
#define	IS25LQ_OP_PP		0x02 // Page Program Data Bytes into Memory SPI 104MHz
#define	IS25LQ_OP_PPQ		0x32 // Also 0x38 Page Program Data Bytes into Memory with Quad Interface SPI 104MHz
#define	IS25LQ_OP_SER		0xD7 // Also 0x20 Sector Erase 4KB
#define	IS25LQ_OP_BER32		0x52 // Block Erase 32KB
#define	IS25LQ_OP_BER64 	0xD8 // Block Erase 64KB
#define	IS25LQ_OP_CER1		0xC7 // 60h Chip Erase
#define	IS25LQ_OP_WREN		0x06 // Write Enable
#define	IS25LQ_OP_WRDI		0x04 // Write Disable
#define	IS25LQ_OP_RDSR		0x05 // Read Status Register
#define	IS25LQ_OP_WRSR		0x01 // Write Status Register
#define	IS25LQ_OP_RDFR		0x48 // Read Function Register
#define	IS25LQ_OP_WRFR		0x42 // Write Function Register
#define	IS25LQ_OP_PERSUS	0x75 // B0h Suspend during the Program/Erase
#define	IS25LQ_OP_PERRSM	0x7A // 30h Resume Program/Erase
#define	IS25LQ_OP_DP		0xB9 // Deep Power Down Mode
#define	IS25LQ_OP_RDID		0xAB // Read Manufacturer and Product ID/Release Deep Power Down
#define	IS25LQ_OP_RDUID		0x4B // Read Unique ID Number
#define	IS25LQ_OP_RDJDID	0x9F // Read Manufacturer and Product ID by JEDEC ID Command
#define	IS25LQ_OP_RDMDID	0x90 // Read Manufacturer and Device ID
#define	IS25LQ_OP_RDSFDP	0x5A // SFDP Read
#define	IS25LQ_OP_RSTEN		0x66 // Software Reset Enable
#define	IS25LQ_OP_RST		0x99 // Reset
#define	IS25LQ_OP_IRP		0x62 // Program Information Row
#define	IS25LQ_OP_IRRD		0x68 // Read Information Row
#define	IS25LQ_OP_SECUNLOCK 0x26 // Sector Unlock
#define	IS25LQ_OP_SECLOCK	0x24 // Sector Lock

class IS25LQFlashMemory : public SPIInterface
{
	public:
		IS25LQFlashMemory();
		virtual ~IS25LQFlashMemory();
		void resetChip(void);
		uint32_t readJedec(void);
		uint8_t readStatusRegister(void);
		bool writeEnable(void);
		bool writeDisable(void);
		void programPage(uint8_t *data, size_t len, size_t addr);
		void readData(uint8_t *out, size_t len, size_t addr);
		void sectorErase(size_t addr);
};

#endif // IS25LQFLASHMEMORY_H
