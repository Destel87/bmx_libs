TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3
QMAKE_CXXFLAGS += -O0

# system libraries inclusion
LIBS += -pthread

# internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/DataCollector/ -lDataCollector
INCLUDEPATH += $$PWD/../../Libs/DataCollector
DEPENDPATH += $$PWD/../../Libs/DataCollector
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/DataCollector/libDataCollector.a


unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/SPIInterface/ -lSPIInterface
INCLUDEPATH += $$PWD/../../Libs/SPIInterface
DEPENDPATH += $$PWD/../../Libs/SPIInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/SPIInterface/libSPIInterface.a


unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a


unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../Libs/Config
DEPENDPATH += $$PWD/../../Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Config/libConfig.a


unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../Libs/TimeStamp
DEPENDPATH += $$PWD/../../Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/TimeStamp/libTimeStamp.a


unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a


SOURCES += TestSPI.cpp \
    IS25LQFlashMemory.cpp
TARGET = TestSPI

# deployment directives
target.path = /home/root
INSTALLS += target

HEADERS += \
    IS25LQFlashMemory.h \
    SPITestIncludes.h


