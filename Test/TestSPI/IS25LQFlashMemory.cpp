#include "IS25LQFlashMemory.h"

#include <linux/spi/spidev.h>


IS25LQFlashMemory::IS25LQFlashMemory()
{

}

IS25LQFlashMemory::~IS25LQFlashMemory()
{

}

void IS25LQFlashMemory::resetChip(void)
{
	uint8_t rsten = IS25LQ_OP_RSTEN;
	uint8_t rst = IS25LQ_OP_RST;
	singleTransfer(&rsten, NULL, 1);
	singleTransfer(&rst, NULL, 1);
}

uint32_t IS25LQFlashMemory::readJedec(void)
{
	uint8_t tx[4] = { IS25LQ_OP_RDJDID, 0x00, 0x00, 0x00 };
	uint8_t rx[4];
	singleTransfer(tx, rx, 4);
	return (rx[1] << 16) | (rx[2] << 8) | rx[3];
}

uint8_t IS25LQFlashMemory::readStatusRegister()
{
	uint8_t tx[2] = { IS25LQ_OP_RDSR, 0x00 };
	uint8_t rx[2] = { 0x00, 0x00 };
	singleTransfer(tx, rx, 2);
	return rx[1];
}

bool IS25LQFlashMemory::writeEnable()
{
	uint8_t tx = IS25LQ_OP_WREN;
	uint8_t statusReg;
	singleTransfer(&tx, NULL, 1);
	statusReg = readStatusRegister();
	if ( statusReg & 0x02 )
		return true;
	else
		return false;
}

bool IS25LQFlashMemory::writeDisable()
{
	uint8_t tx = IS25LQ_OP_WRDI;
	uint8_t statusReg;
	singleTransfer(&tx, NULL, 1);
	statusReg = readStatusRegister();
	if ( statusReg & 0x02 )
		return false;
	else
		return true;
}

void IS25LQFlashMemory::sectorErase(size_t addr)
{
	uint8_t cmd[4] = { IS25LQ_OP_SER, (uint8_t)(addr >> 16), (uint8_t)(addr >> 8), (uint8_t)addr };
	uint8_t sr;
	writeEnable();
	singleTransfer(cmd, NULL, 4);
	// Wait for the erase operation to complete
	do {
		sr = readStatusRegister();
	} while (sr & 0x01);
}

void IS25LQFlashMemory::programPage(uint8_t *data, size_t len, size_t addr)
{
	struct spi_ioc_transfer tr[2];
	uint8_t cmd[4] = { IS25LQ_OP_PP, (uint8_t)(addr >> 16), (uint8_t)(addr >> 8), (uint8_t)addr };
	uint8_t sr;
	prepareTransfer(&tr[0], cmd, NULL, 4);
	prepareTransfer(&tr[1], data, NULL, len);
	writeEnable();
	transfer(tr, 2);
	// Wait for the write operation to complete
	do {
		sr = readStatusRegister();
	} while (sr & 0x01);
}

void IS25LQFlashMemory::readData(uint8_t *out, size_t len, size_t addr)
{
	struct spi_ioc_transfer tr[2];
	uint8_t rd_cmd[4] = { IS25LQ_OP_RD, (uint8_t)(addr >> 16), (uint8_t)(addr >> 8), (uint8_t)addr };
	prepareTransfer(&tr[0], rd_cmd, NULL, 4);
	prepareTransfer(&tr[1], NULL, out, len);
	transfer(tr, 2);
}
