#include <sys/types.h>
#include <signal.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "Process.h"

#define DIR_PROGRAM "/bin/ls"

#define MAX_CMD_SIZE 8192

int main(int argc, char** argv)
{
	Process process;
	char cmd[MAX_CMD_SIZE] = "";
	int i;
	
	sprintf(cmd, "%s ", DIR_PROGRAM);

	// Builds the command line
	for (i = 1; i < argc; i++)
	{
		if ( (strlen(cmd) + strlen(argv[i])) > MAX_CMD_SIZE )
		{
			printf("Command line is too long\n");
			return 1;
		}
		strcat(cmd, argv[i]);
		if ( i != ( argc - 1 ) )
		{
			strcat(cmd, " ");
		}
		printf("Added <%s> to the command line\n", argv[i]);
	}

	printf("Executes process <%s>\n", cmd);
	if ( !process.init(cmd) )
	{
		printf("Impossible to initialize the process\n");
		return 1;
	}
	if ( !process.start() )
	{
		printf("Impossible to start the process\n");
		return 1;
	}
	// Wait the end of the process for 5 sec
	if ( !process.waitProcess(5000, 100) )
	{
		printf("Process not ended in 5 seconds\n");
		process.stop();
		return 1;
	}
	process.stop();
	printf("Process exited normally with code %u\n", process.getRetCode());
	return 0;

}
