TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/ProcessManager/ -lProcessManager
INCLUDEPATH += $$PWD/../../Libs/ProcessManager
DEPENDPATH += $$PWD/../../Libs/ProcessManager
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/ProcessManager/libProcessManager.a

# sources and target
SOURCES += TestProcessManager.cpp
TARGET = TestProcessManager

# deployment directives
target.path = /home/root
INSTALLS += target
