# Defines compilation directives for example test programs of the libs
# Set variable value to ON if the desired test project shall be included in the compiler directives
TEST_THREAD = OFF		# to test the Thread library
TEST_MESSAGE_THREAD = OFF	# to test the MessageThread library
TEST_LOG = OFF			# to test the Log library
TEST_CONFIG = OFF		# to test the Config library
TEST_PROCESS_MANAGER = OFF	# to test the ProcessManager library
TEST_GPIO_INTERFACE = OFF	# to test the GpioInterface library
TEST_TIME_STAMP = OFF		# to test the TimeStamp library
TEST_DATA_COLLECTOR = OFF	# to test the DataCollector library
TEST_SHARED_MEMORY = OFF	# to test the Posix SharedMemory mechanism
TEST_MESSAGE_QUEUE = OFF	# to test the Posix Message Queue mechanism
TEST_SCHEDULER = OFF		# to test the Scheduler Module
TEST_SERIAL = OFF		# to test the SerialDevice library
TEST_SPI = OFF			# to test the SPIDevice library
TEST_USB = OFF			# to test the USBInterface library
TEST_HTTPCLIENT = OFF		# to test the WebClient library
TEST_WEBSERVER = OFF		# to test the WebServer library
TEST_XMLPARSER = OFF		# to test the XmlParser library
TEST_I2C = OFF			# to test the I2cDevice library
TEST_STATE_MACHINE = OFF	# to test the StateMachine library
TEST_TAR = OFF                  # to test the TARUtilities library


TEMPLATE = subdirs


contains( TEST_THREAD, ON ) {
	message("Thread library test app is being compiled")
    SUBDIRS += TestThread
}

contains( TEST_MESSAGE_THREAD, ON ) {
	message("Message thread library test app is being compiled")
    SUBDIRS += TestMessageThread
}

contains( TEST_LOG, ON ) {
	message("Log library test app is being compiled")
	SUBDIRS += TestLog
}

contains( TEST_CONFIG, ON ) {
	message("Config library test app is being compiled")
	SUBDIRS += TestConfig
}

contains( TEST_PROCESS_MANAGER, ON ) {
	message("Process Manager library test app is being compiled")
	SUBDIRS += TestProcessManager
}

contains( TEST_GPIO_INTERFACE, ON ) {
	message("GpioInterface library test app is being compiled")
	SUBDIRS += TestGpioInterface
}

contains( TEST_TIME_STAMP, ON ) {
	message("TimeStamp library test app is being compiled")
	SUBDIRS += TestTimeStamp
}

contains( TEST_DATA_COLLECTOR, ON ) {
	message("Data collector test app is being compiled")
	SUBDIRS += TestDataCollector
}

contains( TEST_SHARED_MEMORY, ON ) {
	message("Shared memory test app is being compiled")
	SUBDIRS += TestSharedMemory
}

contains( TEST_MESSAGE_QUEUE, ON ) {
	message("Message queue test app is being compiled")
	SUBDIRS += TestMessageQueue
}

contains( TEST_SCHEDULER, ON ) {
	message("Scheduler test app is being compiled")
	SUBDIRS += TestScheduler
}

contains( TEST_SERIAL, ON ) {
	message("Serial communication test app is being compiled")
	SUBDIRS += TestSerial
}

contains( TEST_SPI, ON ) {
	message("SPI communication test app is being compiled")
	SUBDIRS += TestSPI
}

contains( TEST_USB, ON ) {
	message("USB communication test app is being compiled")
	SUBDIRS += TestUSB
}

contains( TEST_HTTPCLIENT, ON ) {
	message("Web client test app is being compiled")
        SUBDIRS += TestHttpClient
}

contains( TEST_WEBSERVER, ON ) {
	message("Web server test app is being compiled")
	SUBDIRS += TestWebServer
}

contains( TEST_XMLPARSER, ON ) {
	message("Xml parser test app is being compiled")
	SUBDIRS += TestXmlParser
}

contains( TEST_I2C, ON ) {
	 message("I2CTest app is being compiled")
	 SUBDIRS += TestI2C
}

contains( TEST_STATE_MACHINE, ON ) {
	 message("StateMachineTest app is being compiled")
	 SUBDIRS += TestStateMachine
}

contains( TEST_TAR, ON ) {
         message("TarTest app is being compiled")
         SUBDIRS += TestTAR
}

isEmpty(SUBDIRS) {
	message("Empty Dirlist")
}

message("Subdirs = $$SUBDIRS")    

