TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/StateMachine/ -lStateMachine
INCLUDEPATH += $$PWD/../../Libs/StateMachine
DEPENDPATH += $$PWD/../../Libs/StateMachine
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/StateMachine/libStateMachine.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a

HEADERS += \
			TestStateMachineInclude.h

SOURCES += main.cpp

TARGET = TestStateMachine

# deployment directives
target.path = /home/root
INSTALLS += target


