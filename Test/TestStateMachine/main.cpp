#include <iostream>

#include "TransitionTableEntry.h"
#include "TestStateMachineInclude.h"
#include "TransitionTable.h"
#include "StateMachine.h"

using namespace std;

int main(void)
{

	/* ****************************************************************************************************************
	 * Builds the transition table tree
	 * ****************************************************************************************************************
	 */
	// SECTION A
	TransitionTable* pSectA_TT = new TransitionTable(SECT_A_SM_ID);
	TransitionTableEntry* pSectA_R0 = new TransitionTableEntry;
	pSectA_R0->setId(SECT_A_TRANS_TABLE_ROW_00_ID);
	pSectA_R0->setStartState(SECT_TRANS_TABLE_ROW_00_SS);
	pSectA_R0->setTargetState(SECT_TRANS_TABLE_ROW_00_TS);
	pSectA_TT->addRow(pSectA_R0);
	TransitionTableEntry* pSectA_R1 = new TransitionTableEntry;
	pSectA_R1->setId(SECT_A_TRANS_TABLE_ROW_01_ID);
	pSectA_R1->setStartState(SECT_TRANS_TABLE_ROW_01_SS);
	pSectA_R1->setTargetState(SECT_TRANS_TABLE_ROW_01_TS);
	pSectA_TT->addRow(pSectA_R1);
	TransitionTableEntry* pSectA_R2 = new TransitionTableEntry;
	pSectA_R2->setId(SECT_A_TRANS_TABLE_ROW_02_ID);
	pSectA_R2->setStartState(SECT_TRANS_TABLE_ROW_02_SS);
	pSectA_R2->setTargetState(SECT_TRANS_TABLE_ROW_02_TS);
	pSectA_TT->addRow(pSectA_R2);
	TransitionTableEntry* pSectA_R3 = new TransitionTableEntry;
	pSectA_R3->setId(SECT_A_TRANS_TABLE_ROW_03_ID);
	pSectA_R3->setStartState(SECT_TRANS_TABLE_ROW_03_SS);
	pSectA_R3->setTargetState(SECT_TRANS_TABLE_ROW_03_TS);
	pSectA_TT->addRow(pSectA_R3);

	// SECTION B
	TransitionTable* pSectB_TT = new TransitionTable(SECT_B_SM_ID);
	TransitionTableEntry* pSectB_R0 = new TransitionTableEntry;
	pSectB_R0->setId(SECT_B_TRANS_TABLE_ROW_00_ID);
	pSectB_R0->setStartState(SECT_TRANS_TABLE_ROW_00_SS);
	pSectB_R0->setTargetState(SECT_TRANS_TABLE_ROW_00_TS);
	pSectB_TT->addRow(pSectB_R0);
	TransitionTableEntry* pSectB_R1 = new TransitionTableEntry;
	pSectB_R1->setId(SECT_B_TRANS_TABLE_ROW_01_ID);
	pSectB_R1->setStartState(SECT_TRANS_TABLE_ROW_01_SS);
	pSectB_R1->setTargetState(SECT_TRANS_TABLE_ROW_01_TS);
	pSectB_TT->addRow(pSectB_R1);
	TransitionTableEntry* pSectB_R2 = new TransitionTableEntry;
	pSectB_R2->setId(SECT_B_TRANS_TABLE_ROW_02_ID);
	pSectB_R2->setStartState(SECT_TRANS_TABLE_ROW_02_SS);
	pSectB_R2->setTargetState(SECT_TRANS_TABLE_ROW_02_TS);
	pSectB_TT->addRow(pSectB_R2);
	TransitionTableEntry* pSectB_R3 = new TransitionTableEntry;
	pSectB_R3->setId(SECT_B_TRANS_TABLE_ROW_03_ID);
	pSectB_R3->setStartState(SECT_TRANS_TABLE_ROW_03_SS);
	pSectB_R3->setTargetState(SECT_TRANS_TABLE_ROW_03_TS);
	pSectB_TT->addRow(pSectB_R3);

	// MODULE
	TransitionTable* pMod_TT = new TransitionTable(MODULE_SM_ID);
	TransitionTableEntry* pMod_TT_R0 = new TransitionTableEntry;
	pMod_TT_R0->setId(MOD_TRANS_TABLE_ROW_00_ID);
	pMod_TT_R0->setStartState(MOD_TRANS_TABLE_ROW_00_SS);
	pMod_TT_R0->setTargetState(MOD_TRANS_TABLE_ROW_00_TS);
	pMod_TT->addRow(pMod_TT_R0);
	TransitionTableEntry* pMod_TT_R1 = new TransitionTableEntry;
	pMod_TT_R1->setId(MOD_TRANS_TABLE_ROW_01_ID);
	pMod_TT_R1->setStartState(MOD_TRANS_TABLE_ROW_01_SS);
	pMod_TT_R1->setTargetState(MOD_TRANS_TABLE_ROW_01_TS);
	pMod_TT->addRow(pMod_TT_R1);
	TransitionTableEntry* pMod_TT_R2 = new TransitionTableEntry;
	pMod_TT_R2->setId(MOD_TRANS_TABLE_ROW_02_ID);
	pMod_TT_R2->setStartState(MOD_TRANS_TABLE_ROW_02_SS);
	pMod_TT_R2->setTargetState(MOD_TRANS_TABLE_ROW_02_TS);
	pMod_TT->addRow(pMod_TT_R2);
	TransitionTableEntry* pMod_TT_R3 = new TransitionTableEntry;
	pMod_TT_R3->setId(MOD_TRANS_TABLE_ROW_03_ID);
	pMod_TT_R3->setStartState(MOD_TRANS_TABLE_ROW_03_SS);
	pMod_TT_R3->setTargetState(MOD_TRANS_TABLE_ROW_03_TS);
	pMod_TT->addRow(pMod_TT_R3);
	TransitionTableEntry* pMod_TT_R4 = new TransitionTableEntry;
	pMod_TT_R4->setId(MOD_TRANS_TABLE_ROW_04_ID);
	pMod_TT_R4->setStartState(MOD_TRANS_TABLE_ROW_04_SS);
	pMod_TT_R4->setTargetState(MOD_TRANS_TABLE_ROW_04_TS);
	pMod_TT->addRow(pMod_TT_R4);
	TransitionTableEntry* pMod_TT_R5 = new TransitionTableEntry;
	pMod_TT_R5->setId(MOD_TRANS_TABLE_ROW_05_ID);
	pMod_TT_R5->setStartState(MOD_TRANS_TABLE_ROW_05_SS);
	pMod_TT_R5->setTargetState(MOD_TRANS_TABLE_ROW_05_TS);
	pMod_TT->addRow(pMod_TT_R5);

	// Add children
	//pMod_TT->addChild(pSectA_TT);
	//pMod_TT->addChild(pSectB_TT);

	/* ****************************************************************************************************************
	 * Builds the state machine
	 * ****************************************************************************************************************
	 */
	// SECTION A
	StateMachine* pSectA_SM = new StateMachine(SECT_A_SM_ID, SECT_A_DEFAULT_INITIAL_STATE);
	pSectA_SM->setTransitionTable(pSectA_TT);

	// SECTION B
	StateMachine* pSectB_SM = new StateMachine(SECT_B_SM_ID, SECT_B_DEFAULT_INITIAL_STATE);
	pSectB_SM->setTransitionTable(pSectB_TT);

	// MODULE
	StateMachine* pMod_SM_ACTUAL = new StateMachine(MODULE_SM_ID, MODULE_DEFAULT_INITIAL_STATE, true);
	pMod_SM_ACTUAL->setTransitionTable(pMod_TT);
	pMod_SM_ACTUAL->addChild(pSectA_SM);
	pMod_SM_ACTUAL->addChild(pSectB_SM);

	cout << "BUILT STATE MACHINE, INITIAL VALUE:" << endl;
	pMod_SM_ACTUAL->print();
	cout << "-----------------------------------" << endl;

	/* ****************************************************************************************************************
	 * Now we build a transition from INIT -> IDLE of every leaf
	 * ****************************************************************************************************************
	 */
	StateMachine* pMod_SM_TRANS = new StateMachine(MODULE_SM_ID, MODULE_STATE_IDLE);
	StateMachine* pSA_SM_TRANS = new StateMachine(SECT_A_SM_ID, SECT_STATE_IDLE);
	pMod_SM_TRANS->addChild(pSA_SM_TRANS);
	StateMachine* pSB_SM_TRANS = new StateMachine(SECT_B_SM_ID, SECT_STATE_IDLE);
	pMod_SM_TRANS->addChild(pSB_SM_TRANS);

	cout << "FIRST ROUND ------------------------------------------" << endl;
	cout << "PERFORMING THE FOLLOWING TRANSITION" << endl;
	pMod_SM_TRANS->print();
	cout << "-----------------------------------" << endl;

	//int liRetValue = pMod_SM_ACTUAL->check(pMod_SM_TRANS);
	int liRetValue = pMod_SM_ACTUAL->checkAndSet(pMod_SM_TRANS);
	if ( liRetValue == TRANSITION_ALLOWED )
	{
		cout << "TRANSITION reported " << liRetValue << " SUCCESS, NEW VALUE:" << endl;
		//pMod_SM_ACTUAL->set(pMod_SM_TRANS);
		pMod_SM_ACTUAL->print();
	}
	else
	{
		cout << "TRANSITION reported " << liRetValue << " FAILURE" << endl;
	}
	cout << "------------------------------------------------------" << endl << endl;
	delete(pMod_SM_TRANS);

	/* ****************************************************************************************************************
	 * Now we build a transition from IDLE -> PROTOCOL on Section B
	 * ****************************************************************************************************************
	 */
	if ( liRetValue == TRANSITION_ALLOWED )
	{
		StateMachine* pMod_SM_TRANS = new StateMachine(MODULE_SM_ID, MODULE_STATE_PROTOCOL);
		StateMachine* pSB_SM_TRANS = new StateMachine(SECT_B_SM_ID, SECT_STATE_PROTOCOL);
		pMod_SM_TRANS->addChild(pSB_SM_TRANS);
		cout << "SECOND ROUND -----------------------------------------" << endl;
		cout << "PERFORMING THE FOLLOWING TRANSITION" << endl;
		pMod_SM_TRANS->print();
		cout << "-----------------------------------" << endl;

		liRetValue = pMod_SM_ACTUAL->checkAndSet(pMod_SM_TRANS);
		if ( liRetValue == TRANSITION_ALLOWED )
		{
			cout << "TRANSITION reported " << liRetValue << " SUCCESS, NEW VALUE:" << endl;
			//pMod_SM_ACTUAL->set(pMod_SM_TRANS);
			pMod_SM_ACTUAL->print();
		}
		else
		{
			cout << "TRANSITION reported " << liRetValue << " FAILURE" << endl;
		}
		cout << "------------------------------------------------------" << endl << endl;
		delete(pMod_SM_TRANS);
	}

	/* ****************************************************************************************************************
	 * Now we build a transition from IDLE -> PROTOCOL on Section A
	 * ****************************************************************************************************************
	 */
	if ( liRetValue == TRANSITION_ALLOWED )
	{
		StateMachine* pMod_SM_TRANS = new StateMachine(MODULE_SM_ID, MODULE_STATE_PROTOCOL);
		StateMachine* pSA_SM_TRANS = new StateMachine(SECT_A_SM_ID, SECT_STATE_PROTOCOL);
		pMod_SM_TRANS->addChild(pSA_SM_TRANS);
		cout << "THIRD ROUND ------------------------------------------" << endl;
		cout << "PERFORMING THE FOLLOWING TRANSITION" << endl;
		pMod_SM_TRANS->print();
		cout << "-----------------------------------" << endl;

		liRetValue = pMod_SM_ACTUAL->checkAndSet(pMod_SM_TRANS);
		if ( liRetValue == TRANSITION_ALLOWED )
		{
			cout << "TRANSITION reported " << liRetValue << " SUCCESS, NEW VALUE:" << endl;
			//pMod_SM_ACTUAL->set(pMod_SM_TRANS);
			pMod_SM_ACTUAL->print();
		}
		else
		{
			cout << "TRANSITION reported " << liRetValue << " FAILURE" << endl;
		}
		cout << "------------------------------------------------------" << endl << endl;
		delete(pMod_SM_TRANS);
	}

	/* ****************************************************************************************************************
	 * Now we build a transition from IDLE -> PROTOCOL on Section A
	 * ****************************************************************************************************************
	 */
	if ( liRetValue == TRANSITION_ALLOWED )
	{
		StateMachine* pMod_SM_TRANS = new StateMachine(MODULE_SM_ID, MODULE_STATE_IDLE);
		StateMachine* pSA_SM_TRANS = new StateMachine(SECT_A_SM_ID, SECT_STATE_IDLE);
		pMod_SM_TRANS->addChild(pSA_SM_TRANS);
		StateMachine* pSB_SM_TRANS = new StateMachine(SECT_B_SM_ID, SECT_STATE_IDLE);
		pMod_SM_TRANS->addChild(pSB_SM_TRANS);
		cout << "FOURTH ROUND -----------------------------------------" << endl;
		cout << "PERFORMING THE FOLLOWING TRANSITION" << endl;
		pMod_SM_TRANS->print();
		cout << "-----------------------------------" << endl;

		liRetValue = pMod_SM_ACTUAL->checkAndSet(pMod_SM_TRANS);
		if ( liRetValue == TRANSITION_ALLOWED )
		{
			cout << "TRANSITION reported " << liRetValue << " SUCCESS, NEW VALUE:" << endl;
			//pMod_SM_ACTUAL->set(pMod_SM_TRANS);
			pMod_SM_ACTUAL->print();
		}
		else
		{
			cout << "TRANSITION reported " << liRetValue << " FAILURE" << endl;
		}
		cout << "------------------------------------------------------" << endl << endl;
		delete(pMod_SM_TRANS);
	}

	/* ****************************************************************************************************************
	 * Clear resources
	 * ****************************************************************************************************************
	 */
	delete(pMod_SM_ACTUAL);
	delete(pMod_TT);
	delete(pSectB_TT); // to be explicitly destroyed because not added to pMod_TT
	delete(pSectA_TT); // to be explicitly destroyed because not added to pMod_TT

	return 0;

}
