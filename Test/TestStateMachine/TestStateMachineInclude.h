#ifndef TESTSTATEMACHINEINCLUDE_H
#define TESTSTATEMACHINEINCLUDE_H

/* ********************************************************************************************************************
 * Sections State Machine Defines
 * ********************************************************************************************************************
 */
// Section State Machine States
#define SECT_STATE_POWERUP_STRING			"POWERUP"
#define SECT_STATE_POWERUP					1
#define SECT_STATE_INIT_STRING				"INIT"
#define SECT_STATE_INIT						2
#define SECT_STATE_IDLE_STRING				"IDLE"
#define SECT_STATE_IDLE						3
#define SECT_STATE_PROTOCOL_STRING			"PROTOCOL"
#define SECT_STATE_PROTOCOL					4

// Section Transition Table
#define SECT_TRANS_TABLE_ROW_00_SS			SECT_STATE_POWERUP
#define SECT_TRANS_TABLE_ROW_00_TS			SECT_STATE_INIT
#define SECT_TRANS_TABLE_ROW_01_SS			SECT_STATE_INIT
#define SECT_TRANS_TABLE_ROW_01_TS			SECT_STATE_IDLE
#define SECT_TRANS_TABLE_ROW_02_SS			SECT_STATE_IDLE
#define SECT_TRANS_TABLE_ROW_02_TS			SECT_STATE_PROTOCOL
#define SECT_TRANS_TABLE_ROW_03_SS			SECT_STATE_PROTOCOL
#define SECT_TRANS_TABLE_ROW_03_TS			SECT_STATE_IDLE

// Section A State Machine
#define SECT_A_SM_ID						"SECTION_A"
#define SECT_A_DEFAULT_INITIAL_STATE		SECT_STATE_INIT
#define SECT_A_TRANS_TABLE_ROW_00_ID		"SA_R0"
#define SECT_A_TRANS_TABLE_ROW_01_ID		"SA_R1"
#define SECT_A_TRANS_TABLE_ROW_02_ID		"SA_R2"
#define SECT_A_TRANS_TABLE_ROW_03_ID		"SA_R3"

// Section B State Machine
#define SECT_B_SM_ID						"SECTION_B"
#define SECT_B_DEFAULT_INITIAL_STATE		SECT_STATE_INIT
#define SECT_B_TRANS_TABLE_ROW_00_ID		"SB_R0"
#define SECT_B_TRANS_TABLE_ROW_01_ID		"SB_R1"
#define SECT_B_TRANS_TABLE_ROW_02_ID		"SB_R2"
#define SECT_B_TRANS_TABLE_ROW_03_ID		"SB_R3"

/* ********************************************************************************************************************
 * Module State Machine Defines
 * ********************************************************************************************************************
 */
// Module State Machine
#define MODULE_SM_ID						"MODULE"
#define MODULE_STATE_POWERUP_STRING			"POWERUP"
#define MOD_STATE_POWERUP					1
#define MODULE_STATE_INIT_STRING			"INIT"
#define MODULE_STATE_INIT					2
#define MODULE_STATE_IDLE_STRING			"IDLE"
#define MODULE_STATE_IDLE					3
#define MODULE_STATE_PROTOCOL_STRING		"PROTOCOL"
#define MODULE_STATE_PROTOCOL				4
#define MODULE_DEFAULT_INITIAL_STATE		MODULE_STATE_INIT

// Module Transition Table
#define MOD_TRANS_TABLE_ROW_00_ID			"M_R0"
#define MOD_TRANS_TABLE_ROW_00_SS			MOD_STATE_POWERUP
#define MOD_TRANS_TABLE_ROW_00_TS			MODULE_STATE_INIT
#define MOD_TRANS_TABLE_ROW_01_ID			"M_R1"
#define MOD_TRANS_TABLE_ROW_01_SS			MODULE_STATE_INIT
#define MOD_TRANS_TABLE_ROW_01_TS			MODULE_STATE_IDLE
#define MOD_TRANS_TABLE_ROW_02_ID			"M_R2"
#define MOD_TRANS_TABLE_ROW_02_SS			MODULE_STATE_IDLE
#define MOD_TRANS_TABLE_ROW_02_TS			MODULE_STATE_PROTOCOL
#define MOD_TRANS_TABLE_ROW_03_ID			"M_R3"
#define MOD_TRANS_TABLE_ROW_03_SS			MODULE_STATE_PROTOCOL
#define MOD_TRANS_TABLE_ROW_03_TS			MODULE_STATE_PROTOCOL
#define MOD_TRANS_TABLE_ROW_04_ID			"M_R4"
#define MOD_TRANS_TABLE_ROW_04_SS			MODULE_STATE_PROTOCOL
#define MOD_TRANS_TABLE_ROW_04_TS			MODULE_STATE_PROTOCOL
#define MOD_TRANS_TABLE_ROW_05_ID			"M_R5"
#define MOD_TRANS_TABLE_ROW_05_SS			MODULE_STATE_PROTOCOL
#define MOD_TRANS_TABLE_ROW_05_TS			MODULE_STATE_IDLE

#endif // TESTSTATEMACHINEINCLUDE_H
