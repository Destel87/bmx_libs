/*! *******************************************************************************************************************
 * @file	TestMessageQueueServer.cpp
 * @author	Gionatan Caradonna
 * @date	2019/03/14
 * @sa		TestMessageQueueClient.cpp
 * ********************************************************************************************************************
 */
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include "MessageQueue.h"
#include "TestMessageQueueInclude.h"

void error(char* sMsg)
{
	perror(sMsg);
	exit(1);
}

static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...");
				exit(1);
			}
		break;
	}
}

int main(void)
{

	MessageQueue m;

	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);

	/*! *******************************************************************************************
	 * Create receive message queue
	 * ********************************************************************************************
	 */
	bool bRet = m.create(TEST_MQ_SERVER_QUEUE_NAME, O_RDONLY, TEST_MQ_MAX_MESSAGES, TEST_MQ_MAX_MSG_SIZE);
	if ( bRet == false )
	{
		printf("SERVER: Unable to create message queue. Exit.\n");
		return 1;
	}


	/*! ***************************************************************************************************************
	  *	MESSAGE QUEUE PRE EMPTYING
	  * This server can have been respawned, so before starting the normal real-time service
	  * it is necessary to perform a cleaning of the remained buffers
	  * ***************************************************************************************************************
	  */
	bRet = m.empty();
	if ( bRet == false )
	{
		printf("SERVER: Unable to PRE-EMPTY message queue. Exit.\n");
		return 1;
	}

	/*! ***************************************************************************************************************
	 * MAIN cycle
	 * ****************************************************************************************************************
	 */
	char sInBuffer[TEST_MQ_MSG_BUFFER_SIZE];
	printf("SERVER: Start main cycle\n"); fflush(stdout);
	while ( bKeepRunning )
	{
		// Get the oldest message with highest priority
		unsigned int nMsgPriority;
		int nNumBytesReceived = m.receive(sInBuffer, TEST_MQ_MSG_BUFFER_SIZE, &nMsgPriority, 10000);
		if ( nNumBytesReceived == -1 )
		{
			if ( errno == ETIMEDOUT )
			{
				printf((char*)"SERVER: TIME-OUT on mq_timedreceive\n"); fflush(stdout);
			}
			else
			{
				printf((char*)"SERVER: error from mq_timedreceive()\n"); fflush(stdout);
			}
			if ( bKeepRunning == true )
				continue;
			else
				break;
		}
		printf((char*)"SERVER: <--- message received <%s> (prio=%u)\n", sInBuffer, nMsgPriority); fflush(stdout);

	}

	/*! ***************************************************************************************************************
	 * Close and unlink
	 * ****************************************************************************************************************
	 */
	bRet = m.closeAndUnlink();
	if ( bRet == false )
	{
		error((char*)"SERVER: error from mq_unlink()");
	}
	else
	{
		printf((char*)"SERVER: receive message queue unlinked.\n"); fflush(stdout);
	}
	return 0;

}
