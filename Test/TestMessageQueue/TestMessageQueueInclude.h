/*! *******************************************************************************************************************
 * @file	TestMessageQueueInclude.h
 * @author	Gionatan Caradonna
 * @date	2019/03/14
 * @sa		TestMessageQueueServer.cpp TestMessageQueueClient.cpp
 * ********************************************************************************************************************
 */

#define TEST_MQ_SERVER_QUEUE_NAME			"/mq-testmq-server-queue"
#define TEST_MQ_SERVER_QUEUE_PERMISSIONS	0660
#define TEST_MQ_MAX_MESSAGES				16
#define TEST_MQ_MAX_MSG_SIZE				256
#define TEST_MQ_MSG_BUFFER_SIZE				TEST_MQ_MAX_MSG_SIZE + 16


