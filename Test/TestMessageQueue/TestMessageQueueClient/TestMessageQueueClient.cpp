/*! *******************************************************************************************************************
 * @file	TestMessageQueueClient.cpp
 * @author	Gionatan Caradonna
 * @date	2019/03/14
 * @sa		TestMessageQueueServer.cpp
 * ********************************************************************************************************************
 */
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "MessageQueue.h"
#include "TestMessageQueueInclude.h"

void error(char* sMsg)
{
	perror(sMsg);
	exit(1);
}

static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...");
				exit(1);
			}
		break;
	}
}

int main(void)
{
	char sError[1024] = "\0";

	MessageQueue m;

	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);

	/*! *******************************************************************************************
	 * Open message queue for sending messages to the server
	 * ********************************************************************************************
	 */
	bool bRet = m.open(TEST_MQ_SERVER_QUEUE_NAME, O_WRONLY);
	if ( bRet == false )
	{
		sprintf(sError, "CLIENT: unable to open server SEND QUEUE <%s>", TEST_MQ_SERVER_QUEUE_NAME);
		error(sError);
	}
	printf((char*)"CLIENT: SEND QUEUE <%s> opened\n", TEST_MQ_SERVER_QUEUE_NAME);


	/*! ***************************************************************************************************************
	 * MAIN cycle
	 * ****************************************************************************************************************
	 */
	char sTempBuf[TEST_MQ_MAX_MSG_SIZE];
	char sOutBuf[TEST_MQ_MAX_MSG_SIZE];
	printf((char*)"CLIENT: please type a message to send to the server or \"exit\" to quit: "); fflush(stdout);
	unsigned int nMsgPriority = 0;
	while ( fgets(sTempBuf, TEST_MQ_MAX_MSG_SIZE, stdin) )
	{

		// Remove newline from string
		int nTempBufLength = strlen(sTempBuf);
		if ( sTempBuf[nTempBufLength-1] == '\n' )
		{
			sTempBuf[nTempBufLength-1] = '\0';
		}

		if ( strcmp(sTempBuf, "exit") == 0 )
			break;

		char* pPrioStr = 0;
		pPrioStr = strstr(sTempBuf,"prio=");
		if ( pPrioStr != 0 )
		{
			int n = atoi(&sTempBuf[5]);
			if ( ( n >= 0 ) && ( n <= sysconf(_SC_MQ_PRIO_MAX)-1 ) )
			{
				nMsgPriority = (unsigned int)n;
				printf((char*)"CLIENT: set priority to %u\n", nMsgPriority); fflush(stdout);
			}
			else
			{
				printf((char*)"CLIENT: specified priority out-of-range (%d)\n", n); fflush(stdout);
			}

			printf((char*)"CLIENT: please type a message to send to the server or \"exit\" to quit: "); fflush(stdout);
			continue;
		}

		time_t now = time(NULL);
		char* sTS = ctime(&now);
		int len = strlen(sTS);
		sTS[len-1] = '\0';
		snprintf(sOutBuf, TEST_MQ_MAX_MSG_SIZE, "%d: %s %s", getpid(), sTS, sTempBuf);

		// send message to server the name of the message queue on which we want receive messages
		if ( m.send(sOutBuf, strlen(sOutBuf)+1, nMsgPriority, 10000) == false )
		{
			perror("CLIENT: unable to send message to server");
			continue;
		}

		printf((char*)"CLIENT: please type a message to send to the server or \"exit\" to quit: "); fflush(stdout);

	}

	/*! ***************************************************************************************************************
	 * Close
	 * ****************************************************************************************************************
	 */
	bRet = m.close();
	if ( bRet == false )
	{
		error((char*)"CLIENT: unable to close queue");

	}

	printf((char*)"CLIENT: close\n");

	return 0;

}
