TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../../Libs/IPC/ -lIPC
INCLUDEPATH += $$PWD/../../../Libs/IPC
DEPENDPATH += $$PWD/../../../Libs/IPC
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../../Libs/IPC/libIPC.a

unix:!macx: LIBS += -L$$OUT_PWD/../../../Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../../Libs/TimeStamp
DEPENDPATH += $$PWD/../../../Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../../Libs/TimeStamp/libTimeStamp.a

LIBS += -pthread -lrt

INCLUDEPATH += $$PWD/../
HEADERS += ../TestMessageQueueInclude.h
SOURCES += TestMessageQueueClient.cpp

TARGET = TestMessageQueueClient

# deployment directives
target.path = /home/root
INSTALLS += target
