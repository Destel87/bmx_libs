#include <iostream>
#include <fstream>
#include <sstream>

#include "Config.h"
#include "TimeWait.h"
#include "DataCollector.h"

#define OUTPUT_FILE     "output.csv"


using namespace std;

int main(void)
{
    bool result = false;
    DataCollector Data("S0");
    TimeWait tWait;

    // 3 Data to be collected

    Data.start("S0", 0, 200);
    tWait.waitMicroSeconds(125000);
    Data.stop("S0", 0, result);

    Data.start("S0", 1, 100);
    tWait.waitMicroSeconds(125000);
    Data.stop("S0", 1, result);

    Data.start("S0", 2, 200);
    tWait.waitMicroSeconds(125000);
    Data.stop("S0", 2, result);

    // To test << operator overloading
    cout << Data << endl;


    //Print data .csv file.
    Log logger;
    ofstream fout;

    ostringstream osStatisticsWrite;
    osStatisticsWrite << Data << endl;
    string sStatisticsWrite = osStatisticsWrite.str();
    logger.log(LOG_INFO, "Write stats: %s", Data.getCsvHeaderString().c_str());
    logger.log(LOG_INFO, "            %s", sStatisticsWrite.c_str());
	fout.open(OUTPUT_FILE, ios_base::out | ios_base::binary | ios_base::ate | ios_base::app);
    fout << Data << endl;
    fout.close();

    return 0;
}
