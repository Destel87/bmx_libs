TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -pthread

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/DataCollector/ -lDataCollector
INCLUDEPATH += $$PWD/../../Libs/DataCollector
DEPENDPATH += $$PWD/../../Libs/DataCollector
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/DataCollector/libDataCollector.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../Libs/TimeStamp
DEPENDPATH += $$PWD/../../Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/TimeStamp/libTimeStamp.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../Libs/Config
DEPENDPATH += $$PWD/../../Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Config/libConfig.a


SOURCES += TestDataCollector.cpp
TARGET = TestDataCollector

# deployment directives
target.path = /home/root
INSTALLS += target
