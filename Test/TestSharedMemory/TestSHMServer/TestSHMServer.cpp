/*! *******************************************************************************************************************
 * @file	TestSHMServer.cpp
 * @author	Gionatan Caradonna
 * @date	2019/03/08
 * @details	The System Logger process creates a POSIX shared memory object and maps it to its address space.
 *			Clients also map the shared memory object to their address spaces.
 *			When a client wants to log a message, it creates a string in the format <client pid><timestamp><message>
 *			and writes the string in the shared memory object.
 *			The logger reads strings from the shared memory object, one by one,
 *			and writes them in a log file in chronological order.
 * @sa		TestSHMClient.cpp
 * ********************************************************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <signal.h>
#include <iostream>
#include "TestSHMInclude.h"
#include "Semaphore.h"

void error(char* sMsg)
{
	perror(sMsg);
	exit(1);
}

static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...");
				exit(1);
			}
		break;
	}
}

int main(void)
{
	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);

	/*! ***************************************************************************************************************
	 * Open log file
	 * ****************************************************************************************************************
	 */
	char sError[1024] = "\0";
	int nLogFileFileDescriptor = open(TEST_SHM_LOG_FILE_NAME, O_CREAT | O_WRONLY | O_APPEND | O_SYNC, 0666);
	if ( nLogFileFileDescriptor == -1 )
	{
		sprintf(sError, "SERVER: unable to open file <%s>", TEST_SHM_LOG_FILE_NAME);
		error(sError);
	}
	else
	{
		printf((char*)"SERVER: log file %s correctly opened\n", TEST_SHM_LOG_FILE_NAME); fflush(stdout);
	}

	/*! ***************************************************************************************************************
	 * Create shared memory and set size
	 * ****************************************************************************************************************
	 */
	off_t nSharedMemorySizeBytes = sizeof(shared_memory);
	int nSharedMemoryFileDescriptor = shm_open(TEST_SHM_SHM_MESSAGE_CONTAINER_NAME, O_RDWR, 0644);
	if ( nSharedMemoryFileDescriptor == -1 )
	{

		// create shared memory
		printf((char*)"SERVER: SHARED MEMORY <%s> not present, so create it\n", TEST_SHM_SHM_MESSAGE_CONTAINER_NAME);
		nSharedMemoryFileDescriptor = shm_open(TEST_SHM_SHM_MESSAGE_CONTAINER_NAME, O_RDWR | O_CREAT, 0644);
		if ( nSharedMemoryFileDescriptor == -1 )
		{
			sprintf(sError, "SERVER: unable to create SHARED MEMORY <%s>", TEST_SHM_SHM_MESSAGE_CONTAINER_NAME);
			error(sError);
		}

		// set shared memory size
		if ( ftruncate(nSharedMemoryFileDescriptor, nSharedMemorySizeBytes) == -1 )
		{
			sprintf(sError, "SERVER: unable to set size for shared memory <%s>", TEST_SHM_SHM_MESSAGE_CONTAINER_NAME);
			error(sError);
		}
		else
		{
			printf((char*)"SERVER: set shared memory size to %d bytes\n", (int)nSharedMemorySizeBytes); fflush(stdout);
		}

	}
	else
	{
		printf((char*)"SERVER: SHARED MEMORY <%s> (fd=%d) was already present\n",
			   TEST_SHM_SHM_MESSAGE_CONTAINER_NAME, nSharedMemoryFileDescriptor); fflush(stdout);
	}

	/*! ***************************************************************************************************************
	 * Map shared memory to this virtual address space
	 * ****************************************************************************************************************
	 */
	shared_memory sharedMemoryLocalMap;
	memset((void*)&(sharedMemoryLocalMap), 0, nSharedMemorySizeBytes);
	shared_memory* pSHM = (shared_memory*)mmap(	NULL,							// the kernel chooses the page-aligned address at which to create the mapping
												nSharedMemorySizeBytes,			// the length of the mapping (which must be greater than 0)
												PROT_READ | PROT_WRITE,			// pages can be read/written, PROT_EXEC and PROT_NONE make no sense
												MAP_SHARED,						// Share this mapping with other processes
												nSharedMemoryFileDescriptor,	// obtained above, see client size
												0 );							// no particular flags are specified
	if ( pSHM == MAP_FAILED )
	{
		sprintf(sError, "SERVER: unable to locally MAP the shared memory");
		error(sError);
	}
	else
	{
		printf((char*)"SERVER: local MAPPING of shared memory correctly performed\n"); fflush(stdout);
	}

	/*! ***************************************************************************************************************
	 * Initialize counting semaphore of remaining available buffers.
	 * Initial value = TEST_SHM_MAX_NUM_BUFFERS.
	 * ****************************************************************************************************************
	 */
	int nSemBufferCounterValue = 0;
	Semaphore semBufferCounter;
	string semBufferCounterName(TEST_SHM_SEM_SEM_BUFFER_COUNT_NAME);
	bool b = semBufferCounter.create(semBufferCounterName, TEST_SHM_MAX_NUM_BUFFERS, false, 0644);
	if ( b == false )
	{
		sprintf(sError, "SERVER: unable to create sem COUNTER <%s>", TEST_SHM_SEM_SEM_BUFFER_COUNT_NAME);
		error(sError);
	}
	else
	{
		nSemBufferCounterValue = semBufferCounter.getValue();
		printf((char*)"SERVER: sem COUNTER <%s> created with %d buffers left\n",
			   TEST_SHM_SEM_SEM_BUFFER_COUNT_NAME, nSemBufferCounterValue); fflush(stdout);
	}

	/*! ***************************************************************************************************************
	 * Initialize spool signal semaphore.
	 * It gets increased (so to unlock this server that is waiting on it) when some client pushes a message.
	 * in the shared memory.
	 * Initial value = 0.
	 * ****************************************************************************************************************
	 */
	int nSemSpoolSignalValue = 0;
	Semaphore semSpoolSignal;
	string semSpoolSignalName(TEST_SHM_SEM_SPOOL_SIGNAL_NAME);
	b = semSpoolSignal.create(semSpoolSignalName, 0, false, 0644);
	if ( b == false )
	{
		sprintf(sError, "SERVER: unable to create sem SPOOL SIGNAL <%s>", TEST_SHM_SEM_SPOOL_SIGNAL_NAME);
		error(sError);
	}
	else
	{
		nSemSpoolSignalValue = semSpoolSignal.getValue();
		printf((char*)"SERVER: sem SPOOL SIGNAL <%s> created with %d buffers left\n",
			   TEST_SHM_SEM_SPOOL_SIGNAL_NAME, nSemSpoolSignalValue); fflush(stdout);
	}

	/*! ***************************************************************************************************************
	 * Mutex to protect the buffer configured on the shared memoy.
	 * It has 2 functionalities:
	 *  -	protects the buffer mutual excluding more than one client accessing to it
	 *	-	provides a safe initialization of the client: i.e. the first operation performed by a client
	 *		is an attempt to open this semaphore; if not completely initialized, the client exits
	 *		avoiding to access a shared memory not yet configured.
	 * Initial value 0.
	 * ****************************************************************************************************************
	 */
	Semaphore semBufferMutex;
	string semBufferMutexName(TEST_SHM_SEM_BUFFER_MUTEX_NAME);
	b = semBufferMutex.create(semBufferMutexName, 0, false, 0644);
	if ( b == false )
	{
		sprintf(sError, "SERVER: unable to create sem MUTEX <%s>", TEST_SHM_SEM_BUFFER_MUTEX_NAME);
		error(sError);
	}

	/*! ***************************************************************************************************************
	 * Initialization complete.
	 * Now we can set pSemBufferMutex to 1 to indicate shared memory segment is available.
	 * ****************************************************************************************************************
	 */
	int nSemBufferMutexValue = 0;
	if ( semBufferMutex.release() < 0 )
	{
		error((char*)"SERVER: sem_post: mutex_sem");
	}
	else
	{
		nSemBufferMutexValue = semBufferMutex.getValue();
		printf((char*)"SERVER: mutex_sem_safe_init released v=%d -> start...\n", nSemBufferMutexValue); fflush(stdout);
	}

	/*! ***************************************************************************************************************
	  *	BUFFER PRE EMPTYING
	  * This server can have been respawned, so before starting the normal real-time service
	  * it is necessary to perform a cleaning of the remained buffers
	  * ***************************************************************************************************************
	  */
	char sBuf[256];
	while (1)
	{
		nSemBufferCounterValue = semBufferCounter.getValue();
		if ( nSemBufferCounterValue >= TEST_SHM_MAX_NUM_BUFFERS )
		{
			printf((char*)"SERVER: PRE-EMPTYING, buffer is empty, start normal cycle\n");
			break;
		}
		else
		{

			printf((char*)"SERVER: PRE-EMPTYING, copying buffer at index %d\n", nSemBufferCounterValue); fflush(stdout);

			// Copy buffer here
			snprintf(sBuf, TEST_SHM_MAX_BUFFER_LENGTH-1, pSHM->sBuf[nSemBufferCounterValue]);
			int d = write(nLogFileFileDescriptor, sBuf, strlen(sBuf));
			if ( d != (int)strlen(sBuf) )
			{
				error((char*)"SERVER: PRE-EMPTYING, error writing on logfile");
			}
			else
			{
				printf((char*)"SERVER: PRE-EMPTYING, appended buffer <%s> (length=%d)\n", sBuf, d); fflush(stdout);
			}

			// Contents of one buffer has been copied. One more buffer is available for use by producers.
			if ( semBufferCounter.release() < 0 )
			{
				error((char*)"SERVER: PRE-EMPTYING, error from sem_post() on semBufferCounter");
			}
			else
			{
				nSemBufferCounterValue = semBufferCounter.getValue();
				printf((char*)"SERVER: PRE-EMPTYING, %d buffer left in the queue\n", nSemBufferCounterValue); fflush(stdout);
			}

			// Release also the spool semaphore
			if ( semSpoolSignal.wait() < 0 )
			{
				error((char*)"SERVER: PRE-EMPTYING, error from sem_wait() on pSemSpoolSignal");
			}
			else
			{
				nSemSpoolSignalValue =  semSpoolSignal.getValue();
				printf((char*)"SERVER: PRE-EMPTYING, nSemSpoolSignalValue=%d\n", nSemSpoolSignalValue); fflush(stdout);
			}

		}

	}


	/*! ***************************************************************************************************************
	 * MAIN cycle
	 * ****************************************************************************************************************
	 */
	while ( bKeepRunning )
	{

		int iSpoolWaitTimeMsec = 10 * 1000;
		int iSemWaitRetValue = semSpoolSignal.wait(iSpoolWaitTimeMsec);
		if ( iSemWaitRetValue == SEM_TIMEOUT )
		{
			printf((char*)"SERVER: TIME-OUT on semSpoolSignal val=%d\n", semSpoolSignal.getValue()); fflush(stdout);
			if ( bKeepRunning == true )
				continue;
			else
				break;
		}
		else if ( iSemWaitRetValue == SEM_FAILURE )
		{
			printf((char*)"SERVER: error from sem_wait() on semSpoolSignal\n"); fflush(stdout);
			if ( bKeepRunning == true )
				continue;
			else
				break;
		}
		nSemSpoolSignalValue = semSpoolSignal.getValue();
		printf((char*)"SERVER: <-- received spool signal nSemSpoolSignalValue=%d\n", nSemSpoolSignalValue); fflush(stdout);

		// Retrieve the index of the buffer to empty
		int nBufferIndexToEmpty = semBufferCounter.getValue();
		printf((char*)"SERVER: copying buffer at index %d\n", nBufferIndexToEmpty); fflush(stdout);

		// Copy buffer here
		snprintf(sBuf, TEST_SHM_MAX_BUFFER_LENGTH-1, pSHM->sBuf[nBufferIndexToEmpty]);

		// Write the string to file
		int d =  write(nLogFileFileDescriptor, sBuf, strlen(sBuf));
		if ( d != (int)strlen(sBuf) )
		{
			error((char*)"write: logfile");
		}
		else
		{
			printf((char*)"SERVER: appended buffer <%s> (length=%d)\n", sBuf, d); fflush(stdout);
		}

		// Content of one buffer has been copied. One more buffer is available for use by producers.
		if ( semBufferCounter.release() < 0 )
		{
			error((char*)"SERVER: error from sem_post() on semBufferCounter");
		}
		else
		{
			nSemBufferCounterValue = semBufferCounter.getValue();
			printf((char*)"SERVER: released pSemBufferCounter %d buffer left in the queue\n", nSemBufferCounterValue); fflush(stdout);
		}

	}

	/*! ***************************************************************************************************************
	 * Close
	 * ****************************************************************************************************************
	 */
	int d = munmap(pSHM, nSharedMemorySizeBytes);
	if ( d == -1 )
	{
		error ((char*)"SERVER: error from munmap()");
	}
	else
	{
		printf((char*)"SERVER: shared memory unmapped. Close.\n"); fflush(stdout);
	}

	return 0;

}
