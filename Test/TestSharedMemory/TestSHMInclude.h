/*! *******************************************************************************************************************
 * @file	TestSHMInclude.h
 * @author	Gionatan Caradonna
 * @date	2019/03/08
 * @details	The System Logger process creates a POSIX shared memory object and maps it to its address space.
 *			Clients also map the shared memory object to their address spaces.
 *			When a client wants to log a message, it creates a string in the format <client pid><timestamp><message>
 *			and writes the string in the shared memory object.
 *			The logger reads strings from the shared memory object, one by one,
 *			and writes them in a log file in chronological order.
 * @sa		TestSHMServer.cpp TestSHMClient.cpp
 * ********************************************************************************************************************
 */

#define TEST_SHM_LOG_FILE_NAME					"test_shm.log"
#define TEST_SHM_SEM_BUFFER_MUTEX_NAME			"/sem-testshm-buffer-mutex"
#define TEST_SHM_SEM_SEM_BUFFER_COUNT_NAME		"/sem-testshm-buffer-counter"
#define TEST_SHM_SEM_SPOOL_SIGNAL_NAME			"/sem-testshm-spool-signal"
#define TEST_SHM_SHM_MESSAGE_CONTAINER_NAME		"/shm-testshm-messages_container"
#define	TEST_SHM_MAX_NUM_BUFFERS				16
#define	TEST_SHM_MAX_BUFFER_LENGTH				256

typedef struct
{
	char sBuf[TEST_SHM_MAX_NUM_BUFFERS][TEST_SHM_MAX_BUFFER_LENGTH];
} shared_memory;
