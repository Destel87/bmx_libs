TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -pthread -lrt
INCLUDEPATH += $$PWD/../

#internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../../Libs/Thread
DEPENDPATH += $$PWD/../../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../../Libs/Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../../../Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../../Libs/TimeStamp
DEPENDPATH += $$PWD/../../../Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../../Libs/TimeStamp/libTimeStamp.a

HEADERS += ../TestSHMInclude.h
SOURCES += TestSHMClient.cpp

TARGET = TestSHMClient

# deployment directives
target.path = /home/root
INSTALLS += target


