/*! *******************************************************************************************************************
 * @file	TestSHMClient.cpp
 * @author	Gionatan Caradonna
 * @date	2019/03/08
 * @details	The System Logger process creates a POSIX shared memory object and maps it to its address space.
 *			Clients also map the shared memory object to their address spaces.
 *			When a client wants to log a message, it creates a string in the format <client pid><timestamp><message>
 *			and writes the string in the shared memory object.
 *			The logger reads strings from the shared memory object, one by one,
 *			and writes them in a log file in chronological order.
 * @sa		TestSHMServer.cpp
 * ********************************************************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <signal.h>
#include "Semaphore.h"
#include "TestSHMInclude.h"
#include "TimeStamp.h"

void error (char *msg)
{
	perror (msg);
	exit (1);
}

static bool bKeepRunning = true;
//static int inBck;
void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...\n"); fflush(stdout);
				//inBck = dup(0);
				//fclose(stdin);
				exit(1);
			}
		break;
	}
}


int main (void)
{

	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);

	/*! ***************************************************************************************************************
	 * Get the shared memory buffer mutex that should be already registered by the server.
	 * If not exits because the server has had some initialization problem...
	 * ****************************************************************************************************************
	 */
	char sError[1024] = "\0";
	Semaphore semBufferMutex;
	string semBufferMutexName(TEST_SHM_SEM_BUFFER_MUTEX_NAME);
	bool b = semBufferMutex.open(semBufferMutexName);
	if ( b == false )
	{
		sprintf(sError, "CLIENT: unable to open MUTEX <%s>", TEST_SHM_SEM_BUFFER_MUTEX_NAME);
		error(sError);
	}

	/*! ***************************************************************************************************************
	 * Map shared memory (registered by the server) to this virtual address space
	 * ****************************************************************************************************************
	 */
	int nSharedMemoryFileDescriptor = shm_open(TEST_SHM_SHM_MESSAGE_CONTAINER_NAME, O_RDWR, 0);
	if ( nSharedMemoryFileDescriptor == -1 )
	{
		sprintf(sError, "CLIENT: unable to open SHARED MEMORY <%s>", TEST_SHM_SHM_MESSAGE_CONTAINER_NAME);
		error(sError);
	}
	else
	{
		printf((char*)"CLIENT: SHARED MEMORY <%s> (fd=%d) correctly opened\n", TEST_SHM_SHM_MESSAGE_CONTAINER_NAME, nSharedMemoryFileDescriptor);
	}

	/*! ***************************************************************************************************************
	 * Map shared memory to this virtual address space
	 * ****************************************************************************************************************
	 */
	off_t nSharedMemorySizeBytes = sizeof(shared_memory);
	shared_memory sharedMemoryLocalMap;
	memset((void*)&(sharedMemoryLocalMap), 0, nSharedMemorySizeBytes);
	shared_memory* pSHM = (shared_memory*)mmap(	NULL,
												nSharedMemorySizeBytes,
												PROT_READ | PROT_WRITE,
												MAP_SHARED,
												nSharedMemoryFileDescriptor,
												0 );
	if ( pSHM == MAP_FAILED )
	{
		sprintf(sError, "SERVER: unable to locally MAP the shared memory");
		error(sError);
	}
	else
	{
		printf((char*)"CLIENT: local MAPPING of shared memory correctly performed\n");
	}

	/*! ***************************************************************************************************************
	 * Retrieve counting semaphore of remaining available buffers.
	 * ****************************************************************************************************************
	 */
	int nSemBufferCounterValue = 0;
	Semaphore semBufferCounter;
	string semBufferCounterName(TEST_SHM_SEM_SEM_BUFFER_COUNT_NAME);
	b = semBufferCounter.open(semBufferCounterName);
	if ( b == false )
	{
		sprintf(sError, "CLIENT: unable to open COUNTER <%s>", TEST_SHM_SEM_SEM_BUFFER_COUNT_NAME);
		error(sError);
	}
	else
	{
		nSemBufferCounterValue = semBufferCounter.getValue();
		printf((char*)"CLIENT: sem COUNTER <%s> opened with %d buffers available\n",
			   TEST_SHM_SEM_SEM_BUFFER_COUNT_NAME, nSemBufferCounterValue);
	}
	semBufferCounter.enableErrorPrint(true);

	/*! ***************************************************************************************************************
	 * Initialize spool signal semaphore.
	 * It gets increased (so to unlock the server that is waiting on it) when this client pushes a message.
	 * Initial value = 0.
	 * ****************************************************************************************************************
	 */
	int nSemSpoolSignalValue = 0;
	Semaphore semSpoolSignal;
	string semSpoolSignalName(TEST_SHM_SEM_SPOOL_SIGNAL_NAME);
	b = semSpoolSignal.open(semSpoolSignalName);
	if ( b == false )
	{
		sprintf(sError, "CLIENT: unable to open sem SPOOL <%s>", TEST_SHM_SEM_SPOOL_SIGNAL_NAME);
		error(sError);
	}
	else
	{
		nSemSpoolSignalValue = semSpoolSignal.getValue();
		printf((char*)"CLIENT: sem SPOOL <%s> opened with value %d\n", TEST_SHM_SEM_SPOOL_SIGNAL_NAME, nSemSpoolSignalValue);
	}

	/*! ***************************************************************************************************************
	 * MAIN cycle
	 * ****************************************************************************************************************
	 */
	char sBuf[200];
	printf((char*)"Please type a message: ");
	while ( bKeepRunning )
	{
		while ( fgets(sBuf, 198, stdin ) )
		{
			// Remove newline from string
			int nBufLength = strlen(sBuf);
			for ( int i=0; i<nBufLength; i++ )
			{
				if ( ( sBuf[i] == '\r' ) || ( sBuf[i] == '\n') )
				{
					sBuf[i] = '\0';
					break;
				}
			}


			// Get a buffer from the shared memory
			int nBufferIndexToFill = 0;
			if ( semBufferCounter.wait() < 0 )
			{
				error((char*)"CLIENT: error from sem_wait() on semBufferCounter");
			}
			else
			{
				nBufferIndexToFill = semBufferCounter.getValue();
				printf((char*)"CLIENT: buffer obtained buffer at index %d\n", nBufferIndexToFill);
			}

			/*! ***************************************************************************************************************
			* Start of critical section
			* There might be multiple producers.
			* We must ensure that only one producer uses buffer_index at a time.
			* ****************************************************************************************************************
			*/
			// Protect buffer
			if ( semBufferMutex.wait() == -1 )
			{
				error((char*)"CLIENT: error from sem_wait() on semBufferCounter");
			}

			// Copy buffer at the index retrieved before
			TimeStamp now;
			char sNow[256];
			now.getDateStringFromTimestamp(sNow);
			snprintf(pSHM->sBuf[nBufferIndexToFill], TEST_SHM_MAX_BUFFER_LENGTH, "%d: %s %s", getpid(), sNow, sBuf);

			// Un-protect buffer
			if ( semBufferMutex.release() == -1 )
			{
				error((char*)"CLIENT: error from sem_post() on semBufferCounter");
			}

			/*! ***************************************************************************************************************
			* End of Critical section
			* Now send a message to the server that a new message is ready to be pop-ed
			* ****************************************************************************************************************
			*/
			// Tell spooler that there is a string to print, V(spool_signal_sem)
			if ( semSpoolSignal.release() < 0 )
			{
				error ((char*)"CLIENT: error from sem_post() on semSpoolSignal");
			}
			else
			{
				printf((char*)"CLIENT: sent spool signal -->\n");
			}

			printf((char*)"CLIENT: Please type a message: ");

		}

	}
//	printf("Test\n");
//	dup2(inBck, 0);
//	char chr = getchar();

	/*! ***************************************************************************************************************
	 * Close
	 * ****************************************************************************************************************
	 */
	int d = munmap(pSHM, nSharedMemorySizeBytes);
	if ( d == -1 )
	{
		error ((char*)"CLIENT: error from munmap()");
	}
	else
	{
		printf((char*)"CLIENT: shared memory unmapped. Close.\n");
	}

	exit(0);

}

