TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -pthread

# internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Scheduler/ -lScheduler
INCLUDEPATH += $$PWD/../../Libs/Scheduler
DEPENDPATH += $$PWD/../../Libs/Scheduler
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Scheduler/libScheduler.a
INCLUDEPATH += $$PWD/../../Libs/Scheduler/Scheduler

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/StaticSingleton/ -lStaticSingleton
INCLUDEPATH += $$PWD/../../Libs/StaticSingleton
DEPENDPATH += $$PWD/../../Libs/StaticSingleton
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/StaticSingleton/libStaticSingleton.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../Libs/TimeStamp
DEPENDPATH += $$PWD/../../Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/TimeStamp/libTimeStamp.a

# sources and target
SOURCES += TestScheduler.cpp

TARGET = TestScheduler

# deployment directives
target.path = /home/root
INSTALLS += target




