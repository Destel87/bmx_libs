#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include "SchedulerThread.h"
#include "TimeWait.h"
#include "Log.h"

#define LOG_LEVEL		LOG_DEBUG_PARANOIC
#define LOG_FILE		"stdout"
#define PROGRAM_NAME	"TestScheduler"

static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...");
				exit(1);
			}
		break;
	}
}

int main(void)
{

	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);

	Log logger;
	logger.setLevel(LOG_LEVEL);
	logger.setFile(LOG_FILE);
	logger.setAppName(PROGRAM_NAME);
	logger.log(LOG_INFO, "Scheduler main start...");

	SchedulerThread::getInstance()->setLogger(&logger);

	if ( ! SchedulerThread::getInstance()->startThread() )
	{
		logger.log(LOG_ERR, "Scheduler cannot be run");
	}

	TimeWait w;
	w.waitSeconds(1);

	/*! ***************************************************************************************************************
	 * Schedule 2 dummy events
	 * ****************************************************************************************************************
	 */
	t_ErrorData ErrorData;
	ErrorData.iErrorCode = SCHED_SUCCESS;
	int iFailureCode = SCHED_ERROR;
	logger.log(LOG_INFO, "MAIN: start scheduling two dummy events");
	SchedulerThread::getInstance()->problemInit(ErrorData);
	if ( ErrorData.iErrorCode != 0 )
	{
		SchedulerThread::getInstance()->closeAndAbort();
		logger.log(LOG_ERR, "MAIN: Scheduler initialization error :%d", ErrorData.iErrorCode);
	}
	else
	{
		uint16_t usiSectionId = SECTION_A_ID;
		struct precedence SLastPrec;
		SLastPrec = SchedulerThread::getInstance()->getLastPrec();
		iFailureCode = SchedulerThread::getInstance()->addDummy_1(usiSectionId, SLastPrec.section_id, SLastPrec.slot_id, ErrorData, false, true);
		if ( iFailureCode == SCHED_SUCCESS )
		{
			SLastPrec = SchedulerThread::getInstance()->getLastPrec();
			iFailureCode = SchedulerThread::getInstance()->addDummy_2(usiSectionId, SLastPrec.section_id, SLastPrec.slot_id, ErrorData, false, true);
		}
		else
		{
			logger.log(LOG_ERR, "MAIN: Scheduler dummy_2 scheduling error :%d", ErrorData.iErrorCode);
		}
		if ( iFailureCode == SCHED_SUCCESS )
		{
			SchedulerThread::getInstance()->closeAndRun(ErrorData);
		}
		else
		{
			logger.log(LOG_ERR, "MAIN: Scheduler dummy_2 scheduling error :%d", ErrorData.iErrorCode);
		}
	}
	logger.log(LOG_INFO, "MAIN: finished scheduling two dummy events");

	while ( bKeepRunning )
	{
		w.waitSeconds(1);
	}

	SchedulerThread::getInstance()->stopThread();

	SchedulerThread::getInstance()->~SchedulerThread();

	logger.log(LOG_INFO, "MAIN: exits");

	return 0;

}



