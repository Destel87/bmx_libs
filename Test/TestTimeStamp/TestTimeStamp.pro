TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#Uncomment to enable TIME_WAIT_TEST
DEFINES += TIME_WAIT_TEST

LIBS += -pthread

# private libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../Libs/TimeStamp
DEPENDPATH += $$PWD/../../Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/TimeStamp/libTimeStamp.a

# sources and target
SOURCES += \
    TestTimeStamp.cpp

TARGET = TestTimeStamp

# deployment directives
target.path = /home/root
INSTALLS += target






