#include <iostream>
#include <semaphore.h>
#include <unistd.h>
#include <inttypes.h>

#include "TimeStamp.h"
#include "TimeMeter.h"
#include "TimeWait.h"

#define NUM 2000

using namespace std;

void measureWithTimeMeter(int time, int id_unit);
void measureWithTimeStamp(int time, int id_unit);

int main(void)
{
    bool repeat = true;
    int timerUsed = 0;

    while (repeat)
    {
        (timerUsed==0)? measureWithTimeMeter(125000, 2): measureWithTimeStamp(125000, 2);

        printf("If you want to repeat it again with TimeMeter press '1'."
               "\nIf you want to repeat it again with TimeStamp press '2'."
               "\nElse press Esc.\n");

        char key;
        cin >> key;
        switch(key)
        {
            //Esc is 27 in ASCII code
            case 27:
                repeat = false;
                break;

            case '1':
                timerUsed = 0;
                break;

            case '2':
                timerUsed = 1;
                break;

            default:
                break;
        }
    }

    return 0;
}


void measureWithTimeMeter(int time, int id_unit)
{
    TimeWait tWait;

    TimeMeter timer;
    timer.start();

#ifdef TIME_WAIT_TEST
    if (id_unit==0)
        tWait.waitSeconds(time);
    else if (id_unit == 1)
        tWait.waitMilliSeconds(time);
    else tWait.waitMicroSeconds(time);
#else
    usleep(125000);
#endif

    timer.stop();
    printf("Total duration: %" PRIu64 "us\n", timer.getDurationNanoSec()/1000);
    printf("Timer exceeded of: %" PRIu64 "us\n", (timer.getDurationNanoSec()/1000) - time);

}


void measureWithTimeStamp(int time, int id_unit)
{
    TimeWait tWait;

    TimeStamp tsNow;
    char timeNow[256] = "\0";
    tsNow.getDateStringFromTimestamp(timeNow);


    cout << "**********************NOW**********************" << endl ;

    cout << "Date: " << timeNow << endl;
    cout << "sec_now: " << tsNow.getSecsFromTimestamp() << endl;
    cout << "msec_now: " << tsNow.getMilliSecsFromTimestamp() << endl;
    cout << "usec_now: " << tsNow.getMicroSecsFromTimestamp() << endl;
    cout << "nsec_now: " << tsNow.getNanoSecsFromTimestamp() << endl << endl;

#ifdef TIME_WAIT_TEST
    if (id_unit==0)
        tWait.waitSeconds(time);
    else if (id_unit == 1)
        tWait.waitMilliSeconds(time);
    else tWait.waitMicroSeconds(time);
#else
    usleep(125000);
#endif

    TimeStamp tsAfter;
    char timeAfter[256] = "\0";
    tsAfter.getDateStringFromTimestamp(timeAfter);

    cout << "****************AFTER 125000us****************" << endl;

    cout << "Date: " << timeAfter << endl;
    cout << "sec_after: " << tsAfter.getSecsFromTimestamp() << endl;
    cout << "msec_after: " << tsAfter.getMilliSecsFromTimestamp() << endl;
    cout << "usec_after: " << tsAfter.getMicroSecsFromTimestamp() << endl;
    cout << "nsec_after: " << tsAfter.getNanoSecsFromTimestamp() << endl << endl;

    cout << "******************DIFFERENCES******************" << endl;

    // be careful variables UNSIGNED
    cout << "Difference s: " << tsAfter.getSecsFromTimestamp() - tsNow.getSecsFromTimestamp() << endl;
    cout << "Difference ms: " << tsAfter.getMilliSecsFromTimestamp() - tsNow.getMilliSecsFromTimestamp() << endl;
    cout << "Difference us: " << tsAfter.getMicroSecsFromTimestamp() - tsNow.getMicroSecsFromTimestamp() << endl;
    cout << "Difference ns: " << tsAfter.getNanoSecsFromTimestamp() - tsNow.getNanoSecsFromTimestamp() << endl << endl;
}
