TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O3
QMAKE_CXXFLAGS += -O0

# system libraries inclusion
LIBS += -lpthread -lrt

# internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/I2CInterface/ -lI2CInterface
INCLUDEPATH += $$PWD/../../Libs/I2CInterface
DEPENDPATH += $$PWD/../../Libs/I2CInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/I2CInterface/libI2CInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/I2CDevice/ -lI2CDevice
INCLUDEPATH += $$PWD/../../Libs/I2CDevice
DEPENDPATH += $$PWD/../../Libs/I2CDevice
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/I2CDevice/libI2CDevice.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../Libs/Log
DEPENDPATH += $$PWD/../../Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../Libs/Thread
DEPENDPATH += $$PWD/../../Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../Libs/Thread/libThread.a


# sources and target
SOURCES += main.cpp

TARGET = TestI2C

# deployment directives
target.path = /home/root
INSTALLS += target






