#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include "I2CInterface.h"
#include "Log.h"

#define I2C_TEST_EEPROM_DEVICE_ADDRESS				0x51
#define I2C_TEST_EEPROM_START_ADDRESS				0x00
#define I2C_TEST_EEPROM_NUM_ITERATIONS				0x01
#define	I2C_TEST_EEPROM_TRANSFER_SIZE_BYTES			128
#define I2C_TEST_DIM_BUFF							256

using namespace std;

int main()
{
	/* ********************************************************************************************
	 * LOGGER INITIALIZATION
	 * ********************************************************************************************
	 */
	Log logger;
	logger.enable(true);
	logger.setAppName("I2CTest");
	logger.setLevel(6);
	logger.setFile("stdout");
	logger.setMaxNumFiles(1);
	logger.log(LOG_INFO, "MainExec::initLogger: done");

	/* ********************************************************************************************
	 * I2C INTERFACE INITIALIZATION
	 * ********************************************************************************************
	 */
	I2CInterface i2cDevice;
	i2cDevice.setLogger(&logger);
	if ( !i2cDevice.init("/dev/i2c-2", false, true) )
	{
		logger.log(LOG_ERR, "Unable to initialize I2C device");
		return 1;
	}
	logger.log(LOG_INFO, "I2C device correctly initialized");


	/* ********************************************************************************************
	 * SETUP THE EXPERIMENT
	 * ********************************************************************************************
	 */
	int32_t		slDeviceAddress = I2C_TEST_EEPROM_DEVICE_ADDRESS;

	uint16_t	nStartAddress = (uint16_t)I2C_TEST_EEPROM_START_ADDRESS;
	int32_t		nTransferSize = I2C_TEST_EEPROM_TRANSFER_SIZE_BYTES;

	uint8_t		pTxBuffer[I2C_TEST_DIM_BUFF];
	uint8_t		pRxBuffer[I2C_TEST_DIM_BUFF];

	memset(pTxBuffer, 0, I2C_TEST_DIM_BUFF);
	memset(pRxBuffer, 0, I2C_TEST_DIM_BUFF);

	if ( nTransferSize > 0 )
	{
		for (int i=0; i<nTransferSize; i++)
		{
			pTxBuffer[i] = (uint8_t)i;
			pRxBuffer[i] = 0;
		}
	}

	int32_t nNumIterations = I2C_TEST_EEPROM_NUM_ITERATIONS;

	/* ********************************************************************************************
	 * RUN THE EXPERIMENT
	 * ********************************************************************************************
	 */
//	bool bResult = false;
	logger.log(LOG_INFO, "Test started");
	for (int i=0; i<nNumIterations; i++)
	{
//		bResult = true;

		//WRITE
		i2cDevice.multiWrite( slDeviceAddress, pTxBuffer, nTransferSize, nStartAddress);

		//READ
		i2cDevice.multiRead( slDeviceAddress, pRxBuffer, nTransferSize, nStartAddress);
	}
	logger.log(LOG_INFO, "Test finished");


	/* ********************************************************************************************
	 * FREE RESOURCES
	 * ********************************************************************************************
	 */
	logger.log(LOG_INFO, "Release resources and close"); fflush(stdout);
	return 0;
}
